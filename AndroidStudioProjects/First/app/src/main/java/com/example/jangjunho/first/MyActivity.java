package com.example.jangjunho.first;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MyActivity extends Activity {

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String SHARED_PREF = "test";
    public static final String PROPERTY_GCM_ID = "gcm_id";
    public static final String PROJECT_NUMBER = "1059689398777";

    private GoogleCloudMessaging mGcm;

    @InjectView(R.id.tv_msg)
    TextView tvMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        if (isGoogleServiceAvailable()) {
            mGcm = GoogleCloudMessaging.getInstance(this);
            final String gcmId = getGcmID();
            if (gcmId.isEmpty()) {
                new AsyncTask<Void, Void, String>() {

                    @Override
                    protected String doInBackground(Void... params) {
                        GoogleCloudMessaging instance = GoogleCloudMessaging.getInstance(MyActivity.this);
                        String regId = "";
                        try {
                            regId = instance.register(PROJECT_NUMBER);

                            android.util.Log.e("regId", regId);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return regId;
                    }

                    @Override
                    protected void onPostExecute(String regId) {

                        // 이 값을 서버에 등록하기.
                        setGcmID(regId);

                        // 이 값을 프리퍼런스에 저장하기.
//                        storeGcmID(regId);

                    }
                }.execute();
            } else {
                Toast.makeText(this,gcmId,Toast.LENGTH_LONG).show();
                android.util.Log.e("STORE_GCM_ID", gcmId);
            }
        }


        ButterKnife.inject(this);

    }

    private void setGcmID(String gcmId) {
        RequestParams reqParams = new RequestParams();

        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        reqParams.put("user_idx",android_id);
        reqParams.put("gcm_id",gcmId);

        AsyncHttpClient client = new AsyncHttpClient();

        client.post("http://14.63.161.94/gcm/register.php", reqParams, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String content) {
                Log.e("register",content);
            }
        });
    }

    private void storeGcmID(String gcmId) {
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PROPERTY_GCM_ID, gcmId);
        editor.apply();
    }

    private String getGcmID() {
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        return pref.getString(PROPERTY_GCM_ID,"");

    }

    /**
     *
     * @return 구글 서비스가 어베일어블 한지 확인.
     */
    private Boolean isGoogleServiceAvailable() {
        int resCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resCode)) {
                GooglePlayServicesUtil.getErrorDialog(resCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        } else {
            return true;
        }
    }


    @OnClick(R.id.btn_send) void sendMsg() {

    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
