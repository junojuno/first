<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
?>

<!-- footerwrap -->
<div id="footerwrap">
	<div id='footer'>
		<div class='sns_set'>
		<a href='http://www.facebook.com/wegeneration/' target='_blank'><img src='/images/common/icon_fb.png' style='margin-right: 5px' title='위젠 페이스북' /></a>
		<a href='http://www.twitter.com/wegenkr/' target='_blank'><img src='/images/common/icon_tw.png' style='margin-right: 5px' title='위젠 트위터' /></a>
		<a href='http://www.youtube.com/wegenkr' target='_blank'><img src='/images/common/icon_youtube.png' style='margin-right: 5px' title='위젠 유튜브 채널' /></a>
		</div>

		<menu>
			<a href='/provision'><li>이용약관</li></a>
			<a href='/privacy-policy'><li>개인정보 취급방침</li></a>
			<a href='/about/inquiry'><li>1:1 문의</li></a>
		</menu>
		<div style='clear: both'></div>
	</div>
</div>
<!-- /footerwrap -->

<!-- copyrightwrap -->
<div id="copyrightwrap">
	<div id="copyright">
			<div class='copy_img'>
				<img src="/images/common/copyright.png" style='width:100%;'/>
			</div>	
			위제너레이션 | (135-845) 서울특별시 서초구 양재2동 362-2 국일빌딩 301호 | 대표 : 홍기대 <br>
			사업자등록번호 : 120-87-82097 | 통신판매업신고번호 : 제2012-서울강남-01625 | 개인정보관리책임자 : 홍기대 <br>
			대표전화번호 :  <?=$default[de_admin_company_tel]?> | 팩스 :  <?=$default[de_admin_company_fax]?> | 이메일 : wegen@wegen.kr  <br><br>
			<strong>COPYRIGHTⓒ 2012 WEGENERATION. ALL RIGHTS RESERVED.</strong>
	</div>
</div>
<!-- /copyrightwrap -->

</body>

</html>