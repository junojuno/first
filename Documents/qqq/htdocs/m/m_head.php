<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

sec_session_start();

if(!login_check($mysqli)) {
   if(isset($_COOKIE[wegen_id]) && isset($_COOKIE[wegen_pw])) {
      login($_COOKIE[wegen_id],$_COOKIE[wegen_pw],$mysqli);
   }
}

if ($_loginrequired && login_check($mysqli) != true) {
	header("Location: /m/login_m.php?url=".urlencode($_SERVER[REQUEST_URI]));
	exit;
}
$tablet_size = $tablet_size ? $tablet_size : "1.0";
$settings[type] = ($settings[type]) ? $settings[type] : 'website';
//$settings[url] = ($settings[url]) ? $settings[url] : "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
$settings[url] = ($settings[url]) ? $settings[url] : "http://wegen.kr" . $_SERVER['REQUEST_URI'];
$settings[title_page] = ($settings[title] == '') ? '위제너레이션' : $settings[title].' - 위제너레이션';
$settings[image] = ($settings[image]) ? $settings[image] : 'http://wegen.kr/images/common/logo_facebook.jpg';
$settings[desc] = ($settings[desc]) ? $settings[desc] : '스타와 함께 하는 즐거운 기부, 위제너레이션';
?>
<!DOCTYPE html>
<html>

<head>
<title><?=$settings[title_page]?></title>
<meta name="viewport" content="user-scalable=no, initial-scale=<?=$tablet_size?>, maximum-scale=<?=$tablet_size?>, minimum-scale=<?=$tablet_size?>, width=device-width" />
<meta property="fb:app_id" content="211534425639026" />
<meta property='og:title' content='<?=$settings[title]?>' />
<meta property='og:type' content='<?=$settings[type]?>' />
<meta property='og:url' content='<?=$settings[url]?>' />
<meta property='og:image' content='<?=$settings[image]?>' />
<meta property='og:description' content='<?=$settings[desc]?>' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<link rel='stylesheet' type='text/css' href='/css/m/style.css' />
<!-- <link rel='stylesheet' type='text/css' href='http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css'/> -->

<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script>
   var js_file = '<?=file_exists(JS_ROOT.str_replace('.php','.js', SCRIPT_NAME)) ? '/js'.str_replace('.php','.js',SCRIPT_NAME) : false ?>';
   var http_host = '<?=$_SERVER[HTTP_HOST]?>';
   var mb_no = '<?=$_SESSION[user_no]?>';
   var it_id = '<?=$_REQUEST[it_id]?>';
   var _debug = '<?=$_debug? 1 : 0?>';
</script>
<script type='text/javascript' src='/js/scripts.js'></script>
<!-- <script src='http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js'></script> -->
<script type='text/javascript'>
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-34058829-1']);
_gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

$(document).ready(function() {
	
	
});
</script>
</head>

<body>
<div id='fb-root'></div>
<!-- header -->
<div id='header'>
	<input type="hidden" name="ismobile" value="<?=$isMobile ? '1' : '0'?>"/>
	<div class='left'>
		<a href='/'>
			<img class='logo' src='/images/m/logo.png' style='width:100%;'/>
			<span class='bottom_logo'>Causes worth taking action</span>
		</a>
	</div>
	<div class='right'>
		<? if (login_check($mysqli)) { ?>
			<a href='/my'><span class='clickable'>마이페이지</span></a> | 
			<a href='/logout.php'><span class='clickable'>로그아웃</span></a> |
		<? } else { ?>
			<a href='/m/login_m.php?url=<?=urlencode($_GET[url] ? $_GET[url] : $_SERVER[REQUEST_URI])?>'><span class='m_login_txt clickable'>로그인</span></a> | 
			<a href='/register'><span class='clickable'>회원가입</span></a> | 
		<? } ?>
		<a href='http://wegen.kr/'><span class='clickable'>메인으로</span></a>
	</div>
</div>
<!-- header end -->

</body>
