<?php
	$_required = true;
	include '../config.php';
	
	include './m_head.php';

	if ($_SESSION[user_no]) {
		header('Location: '.urldecode($_GET[url]));
	}
?>
<script type='text/javascript' src='/js/encrypt.js'></script>
<script>
$(document).ready(function() {
	$('input[name=uid]').focusin(function() {
		if(!$(this).val() || $(this).val() == '이메일 주소') {
			$(this).val('').removeClass('label');
		}
	}).focusout(function() {
		if(!$(this).val()) {
			$(this).val('이메일 주소').addClass('label');
		}
	});

	$('input[name=password_pseudo]').focusin(function() {
		$(this).hide();
		$('input[name=p]').show();
		$('input[name=p]').focus();
	});

	$('input[name=p]').focusout(function() {
		if(!$(this).val()) {
			$('input[name=p]').hide();
			$('input[name=password_pseudo]').show();
		}
	}).keydown(function(e) {
		if (e.which == '13')
			$('input[name=loginBtn]').trigger('click');
				
	});

	$("input[name=loginBtn]").click(function() {
		if(checkLoginForm()) {
			processLogin("<?=urlencode($_GET[url] ? $_GET[url] : '')?>");
		}
	});
	$(".facebook_btn").click(function() {
		window.open("/facebook/", "loginFB");
	});

	$(".register_btn").click(function() {
		location.href="/register?url=<?=urlencode($_GET['url'])?>";
	});

});

</script>

<div id='content'>
	<div class='login_m'>
		<p class='title'>로그인</p>
		
		<div class='info_input'>				
			<!-- <form name="loginForm" action='/login.php' method="POST">  -->
				<table style='width:100%;'>
					<tr>
						<td>
							<input tabindex="1" type="email" name='uid' class='textbox label' value='이메일 주소' />
						</td>
						<td class='login_btn' rowspan='2'>
							<input tabindex="3" type='image' name='loginBtn' src='/images/m/btn_dologin.png'/>
						</td>
					</tr>
					<tr>
						<td>
							<input tabindex="2" type="text" name='password_pseudo' class='textbox label' value='비밀번호'/>
							<input type="password" name='p' class='textbox' style='display:none;'/>
						</td>
					</tr>
				</table>
			<!--  </form>  -->
		</div>
		
		<div class='facebook_btn clickable'>
			<img src='/images/m/btn_fblogin.png'/>
		</div>
		

		<div class='register_btn clickable'>
			<img src='/images/m/btn_register.png'/>
		</div>

		
		<p class='bottom'>아이디나 비밀번호가 기억나지 않으세요?<br><br>
		컴퓨터에서 위젠(wegen.kr)에 연결하시면 아이디,
		비밀번호 찾기를 하실 수 있습니다.</p>
	</div>
</div>

<?php
	include './m_tail.php';
?>
