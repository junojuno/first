<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;
$it_id = $_GET[it_id];

$_required = true;
include '../../config.php';

//if(!$isMobile || $isTablet) {
	header("Location: /campaign/{$_GET[it_id]}");
	exit;
//}

$sql = "SELECT idx, it_id FROM 
			(SELECT @idx:=@idx+1 AS idx, it_id FROM ".DB_CAMPAIGNS.", (SELECT @idx:=-1) i
			 ORDER BY it_startdate DESC) t";
$it_ids = sql_query($sql);
$it_ids_num = mysql_num_rows($it_ids);

$first = 0; $last = 0;
for($i =0; $row = sql_fetch_array($it_ids); $i++) {
	if($i == 0 && $row['it_id'] == $it_id) {
		$first = 1;
		$row = sql_fetch_array($it_ids);
		$next_it_id = $row['it_id'];
		break;
	} else if($i == $it_ids_num-1 && $row['it_id'] == $it_id ) {
		$last = 1;
		$row = sql_fetch_array($it_ids);
		$prev_it_id = $row['it_id'];
		break;
	} else if( $row['it_id'] == $it_id) {
		$row = sql_fetch_array($it_ids);
		$next_it_id = $row['it_id'];
		break;	
	} else {
		$prev_it_id = $row['it_id'];
	}
}


$sql = "SELECT *,
			(SELECT SUM(od_amount) - SUM(pay_remain)
			FROM ".DB_ORDERS." o
			WHERE o.it_id = i.it_id)
		AS it_funded
		FROM ".DB_CAMPAIGNS." i
		LEFT JOIN ".DB_PARTNERS." p ON (i.it_partner = p.pt_no)
		WHERE i.it_id = '$it_id' ";
$it = sql_fetch($sql);
if($it[it_isPublic] == 0) {
   alert('잘못된 캠페인 코드입니다.');
   exit;
}

$hzh = $it[it_id] == '1371778995' ? true : false;
$isVol = ($it[type] == '2') ? true : false;

switch($it_id) {
   case $SPECIAL[cam_school] :
      $draw_type = $DRAW_GAUGE[cam_school];
      break;
   case $SPECIAL[cw1] :
   case $SPECIAL[cw2] :
      $draw_type = $DRAW_GAUGE[cw];
      $type_cw = true;
      break;
   case $SPECIAL[kara_fan]:
      $draw_type = $DRAW_GAUGE[kara_fan];
      break;
   default:
      $draw_type = ($isVol) ? $DRAW_GAUGE[volunteer] : $DRAW_GAUGE[normal];
}

if ($isVol) {
	$req = sql_fetch("SELECT COUNT(od_id) AS cnt FROM wegen_volunteer WHERE it_id = '$it_id' ");
	$req = $req[cnt];
}

$sql = "SELECT *
		FROM ".DB_POSTSCRIPTS."
		WHERE wr_campaign = '$it_id'
		AND wr_category = '1'
		";
$ps = sql_query($sql);
$pstotal = mysql_num_rows($ps);
$pstotal = ($pstotal > 0) ? " ($pstotal)" : false;

$sql = "SELECT *
		FROM ".DB_POSTSCRIPTS."
		WHERE wr_campaign = '$it_id'
		AND wr_category = '2'
		";
$ev = sql_query($sql);
$evtotal = mysql_num_rows($ev);
$evtotal = ($evtotal > 0) ? " ($evtotal)" : false;

$settings[type] = 'wegenapp:campaign';
$settings[title] = $it[it_name];
$settings[desc] = $it[it_shortdesc];
$settings[image] = 'http://wegen.kr/data/campaign/'.$it[it_id].'/list.jpg';

include '../m_head.php';

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	$param = $_GET[view] ? $_GET[view] : 'detail';
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = '$param', param2 = '$it_id', referer = '$_SERVER[HTTP_REFERER]' ");
}

$status = ($it[it_funded] >= $it[it_target]) ? '성공' : '완료';
if ($it[it_isEnd] == 0) {
	$diff = round((strtotime($it[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
	$status = ($diff < 0) ? '완료' : "D-".$diff;
	if ($diff < 0) {
		$ismain = ($status == '완료') ? ", it_isMain = '0' " : false;
		mysql_query("UPDATE ".DB_CAMPAIGNS." SET it_isEnd = '1' $ismain WHERE it_id = '$it_id' ");
	}
}

if (!$it[it_id]) alert("잘못된 접근입니다.");

if($it[it_id] == '1371778995' ) {
	echo "<script>window.open('/data/campaign/1371778995/matches.jpg','','width=900,height=643,resizable=no, scrollbars=no, status=no ')</script>";
}

// <img src='/data/partners/$it[pt_logo]' style='max-width: 100%;' /> -> <img src='/data/partners/$it[pt_logo]' style='width: 100%;' /> 변경
// 2013. 08. 13.
// 진입명
?>
<div id='content'>
	<div class='campaign_detail'>		
		<div class='top'>
			<span id='campaign_detail_prev' class='direction left clickable'> < </span>
			<span id='campaign_detail_next' class='direction right clickable'> > </span>
		</div>
		
		<div class='content'> 
			<div class='content_unit title'>
				<?=$it['it_name']?>
			</div>
			
			<div class='content_unit'>
				<? if($it['it_youtube']) :?>
					<div id='movieFrame_<?=$_GET['it_id']?>'></div>
					<script type='text/javascript'>
					$.getScript('https://www.youtube.com/iframe_api');
					function onYouTubeIframeAPIReady() {
						var p = new YT.Player('movieFrame_<?=$_GET['it_id']?>',{
							videoId:'<?=$it['it_youtube']?>',
							width:'100%',
							height:'300',
			//				origin:'http://wegen.kr',
							playerVars:{showinfo: 0}
						});
					}
					</script>
				<? else : ?>
					<img src='/data/campaign/<?=$it[it_id]?>/inner.jpg' style='width:100%;' />
				<? endif; ?>
			</div>
			
			<div class='content_unit' style='text-align:left;'>
				<a href='/m/campaign/<?=$_GET['it_id']?>/donate'><img class='donate_btn' src='<?=$hzh ? "/images/m/lol.png" : '/images/m/btn_donate.png'?>'/></a>
				<?=$hzh ? "<br/><span style='color:red;font:10pt NanumGothic;'>참가신청을 위해서는 페이스북/트위터 로그인이나<br/>위젠계정으로 로그인이 필요합니다.</span>" : false ?>
			</div>
         <? if($it[it_id] != $SPECIAL[snoop]) { ?>
			<div class='content_unit' style=''>
				<div class='gauge_layout'>
					<h3 style='margin: 0px; border: none'>
                  <? 
                  if($isVol || $it['type'] == 100 || $it['type'] == 101 || $it['type'] == 102 ) {
                     print '참여인원';
                  } else if($it[it_id] == '1371778995') {
                     print '참가팀수';	
                  } else if($type_cw) {
                     print '판매수량';
                  } else if($it[it_id] == $SPECIAL[kara_fan]) {
                     echo '참여인원';
                  }else {
                     print '모금액';	
                  }
                  ?>
               </h3>
					<? 
					if($it[it_id] == '1371778995') {
                  $sql = "SELECT mb_no FROM ".DB_ORDERS." WHERE it_id='1371778995' AND od_amount='20000'";
                  $res = sql_query($sql);
                  $total = @mysql_num_rows($res); 
                  drawGauge($total, 64, true, false, 2);
					} else if($it[it_id] == $SPECIAL[solar]) {
						drawGauge($it[it_funded],50000000);
						print "<br/><h3 style='margin: 0px; border: none'>한국동서발전, 유니슨 4배매칭 기부</h3>";
						$funded = $it[it_funded]*4;
						$targeted = 200000000;
						$percent = @floor($funded * 100 / $targeted);
						$percent = ($funded > 0 && $percent == 0) ? 1 : $percent;
						$width = ($percent >= 100) ? 100 : $percent;
						$funded = number_format($funded);
						$targeted = number_format($targeted);
						$unit = '원';
						
						print "
						<div class='gauge'>
						<div class='progress' style='width: ${width}%;background-color:orange;'></div>
						<div class='percent'>${percent}%</div>
						</div>";
						print "
						<span class='funded'>${funded}${unit}</span>
						<span class='targeted'>${targeted}${unit}</span>
						<div style='clear: left'></div>";
						
					} else if($it['type'] == 100 || $it['type'] == 101 || $it['type'] == 102 ) {
						$c = sql_fetch("SELECT (count(*)) as count FROM ".DB_ORDERS." WHERE it_id='{$it['it_id']}'");
						$percent = $c['count'];
						$width = ($percent >= 100) ? 100 : $percent;
						$unit = '명';
						$mny = $it['it_funded']*1+10000000;
						$mny = number_format($mny);
						print "
						<div class='gauge'>
						<div class='progress' style='width: ${width}%'></div>
						<div class='percent'>${percent}%</div>
						</div>";
						print "
						<div style='text-align:center;'>
						<div class='funded' style='display:inline-block;float:left;'>${c['count']}${unit}</div>
						<div style='font: bold 11pt Arial, NanumGothic;display:inline-block;margin-top:5px;'>{$mny}원</div>
						<div class='funded' style='display:inline-block;float:right;'>100${unit}</div>
						</div>
						<div style='clear: left'></div>";
               } else if ($it[it_id] == $SPECIAL[cam_hanta_bada])  {
                  drawGauge($it[it_funded], $it[it_target]);
                  print "<br/><h3 style='margin: 0px; border: none'>한국타이어, 5배 매칭 기부</h3>";
                  $funded = $it[it_funded]*5;
                  $targeted = 10000000;
                  $funded = $funded >= $targeted ? $targeted : $funded;
                  $percent = @ceil($funded *100 / $targeted);
                  $percent = ($percent == 100 && $funded < $targeted ) ? 99 : $percent;
                  $width = ($percent >= 100) ? 100 : $percent;
                  $funded = number_format($funded);
                  $targeted = number_format($targeted);
                  $unit = '원';
                  
                  print "
                  <div class='gauge'>
                  <div class='progress' style='width: ${width}%;background-color:orange;'></div>
                  <div class='percent'>${percent}%</div>
                  </div>";
                  print "
                  <span class='funded'>${funded}${unit}</span>
                  <span class='targeted'>${targeted}${unit}</span>
                  <div style='clear: left'></div>";
               } else {
                  switch($draw_type) {
                     case $DRAW_GAUGE[cw]:
                        drawGauge($it[it_funded], $it[it_target], true, false, $DRAW_GAUGE[cw]);
                        break;
                     case $DRAW_GAUGE[kara_fan]:
                        drawGauge($it[it_funded]/5000, $it[it_target]/5000, true, false, $DRAW_GAUGE[kara_fan]);
                        break;
                     default:
                        ($isVol) ? drawGauge($req, $it[it_target], true, false, 1) : drawGauge($it[it_funded],$it[it_target], true, false, $draw_type); 
                        break;
                  }
					} 
					?>
				</div>
			</div>
			<? } ?>
			<div class='content_unit bottom'>
				<div class='menu'>
					<ul>
						<? if($weisVol) : ?>
						<li class='campaign_menu_0 selected line' style='width:49%;'>상세 내용</li>
						<li class='campaign_menu_1' style='width:49%;'>봉사 후기</li>
						<? else : ?>
						<li class='campaign_menu_0 selected line'>상세 내용</li>
						<li class='campaign_menu_1 line'>전달 후기</li>
						<li class='campaign_menu_2'>이벤트 후기</li>
						<? endif;?>
					</ul>
					<input type='hidden' name='current_menu' value='0'/>
				</div>
				
				
				<div class='swipe'>
					<div class='swipe-wrap'>
						<div class='menu_swipe_0'>
                  <? include WEB_ROOT.'/module/detail_auction.php'; ?>
						<?
						$customizedDetail = '../../module/detail.'.$it_id.'.php';
						if (file_exists($customizedDetail)) {
							include $customizedDetail;
						} else {
						?>
						<img src='/data/campaign/<?=$it[it_id]?>/detail.jpg' style='width: 100%'/>
                  <? } ?>
						</div>
						
						<div class='menu_swipe_1'>
						<table>
							<?
								if (!$pstotal) print "<tr><td class='nothing'>아직 등록된 후기가 없습니다.<br/>캠페인 완료 후 후기가 등록됩니다.</td></tr>";
								for ($i = 0; $data = sql_fetch_array($ps); $i++) {
							?>
								<tr>
									<td class='title'><?=$data[wr_subject]?></td>
									<td class='date'><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])));?></td>
								</tr>
								<tr>
									<td colspan='2' class='redactor_editor' style='padding-bottom: 50px'><?=$data[wr_content]?></td>
								</tr>
							<?
								}
							?>
						</table>
						
						</div>
						
						<? if(!$isVol) { ?>
						<div class='menu_swipe_2'>
						<table>
							<?
								if (!$evtotal) print "<tr><td class='nothing'>아직 등록된 후기가 없습니다.<br/>캠페인 완료 후 후기가 등록됩니다.</td></tr>";
								for ($i = 0; $data = sql_fetch_array($ev); $i++) {
							?>
								<tr>
									<td class='title'><?=$data[wr_subject]?></td>
									<td class='date'><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])));?></td>
								</tr>
								<tr>
									<td colspan='2' class='redactor_editor' style='padding-bottom: 50px'><?=$data[wr_content]?></td>
								</tr>
							<?
								}
							?>
						</table>
						</div>
						<? } ?>
					</div>
				</div>
				<script>setCampaignMenuSwipe();</script>
			</div>
			
			<? if($it[it_id] == $SPECIAL[solar]) { ?>
			<div class='content_unit bottom'>
				<iframe id="player" type="text/html" width="100%" height="300"
				  src="http://www.youtube.com/embed/mK-5SfcYuoE?enablejsapi=1"
				  frameborder="0"></iframe>
				
			</div>
			<?php } ?>
			
		</div>
	</div>
</div>

<? include '../m_tail.php'; ?>

<script>
$(document).ready(function() {
	if(1 == <?=$first?>) {
		$('#campaign_detail_prev').css('display', 'none');
		$('#campaign_detail_next').click(function() {
			location.href='./<?=$next_it_id?>';
		});
		                                     	
	} else if(1 == <?=$last?>) {
		$('#campaign_detail_next').css('display', 'none');
		$('#campaign_detail_prev').click(function() {
			location.href='./<?=$prev_it_id?>';
		});
	} else {
		$('#campaign_detail_next').click(function() {
			location.href='./<?=$next_it_id?>';
		});
		
		$('#campaign_detail_prev').click(function() {
			location.href='./<?=$prev_it_id?>';
		});	
	}
});

											

</script>
