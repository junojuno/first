<?php
   if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;
   
   $_required = true;
   include '../../config.php';
   
   if(!$isMobile) {
      header("Location: /campaign/{$_GET[it_id]}/donate");
      exit;
   }
   
   /* kcp와 통신후 kcp 서버에서 전송되는 결제 요청 정보 */
   
   $req_tx          = $_POST[ "req_tx"         ]; // 요청 종류
   $res_cd          = $_POST[ "res_cd"         ]; // 응답 코드
   
   if(isset($_POST["res_cd"]) && $_POST["res_cd"] != "3000" && $_POST["res_cd"] != "3001") {
      $isKCP = true;
   } else {
      $isKCP = false;
   }
   
   $tran_cd         = $_POST[ "tran_cd"        ]; // 트랜잭션 코드
   $ordr_idxx       = $_POST[ "ordr_idxx"      ]; // 쇼핑몰 주문번호
   $good_name       = $_POST[ "good_name"      ]; // 상품명
   $good_mny        = $_POST[ "good_mny"       ]; // 결제 총금액
   $buyr_name       = $_POST[ "buyr_name"      ]; // 주문자명
   $buyr_tel1       = $_POST[ "buyr_tel1"      ]; // 주문자 전화번호
   $buyr_tel2       = $_POST[ "buyr_tel2"      ]; // 주문자 핸드폰 번호
   $buyr_mail       = $_POST[ "buyr_mail"      ]; // 주문자 E-mail 주소
   $use_pay_method  = $_POST[ "use_pay_method" ]; // 결제 방법
   $enc_info        = iconv("UTF-8","euc-kr",$_POST[ "enc_info"       ]); // 암호화 정보
   $enc_data        = iconv("UTF-8","euc-kr",$_POST[ "enc_data"       ]); // 암호화 데이터
   
   /*
    * 기타 파라메터 추가 부분 - Start -
   */
   $param_opt_1     = $_POST[ "param_opt_1"    ]; // 주소 data
   $param_opt_2     = $_POST[ "param_opt_2"    ]; // 이벤트 data
   $param_opt_3     = $_POST[ "param_opt_3"    ]; // 영수증 data

   /*
    * 기타 파라메터 추가 부분 - End -
   */
   
   if($isKCP) {
      $param_1_arr = explode("|", $param_opt_1);
      $param_2_arr = explode("|", $param_opt_2);
      $param_3_arr = explode("|", $param_opt_3);
   }
   
   $tablet_size      = $isTablet ? "1.25" : "1.0";

   $_loginrequired = true;
   include '../m_head.php';
   
   $row = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id='$_GET[it_id]' AND type!=2");
   
   if(!$row[it_id]) { alert("캠페인 정보가 없습니다.","$g4[path]"); }
   if ($row[it_isEnd] == '1') { alert("이미 종료된 캠페인입니다."); }
   if($row[it_isPublic] == 0) {
      alert('잘못된 캠페인 코드입니다.');
      exit;
   }
   $isOpenCampaign = $row[it_isOpenCampaign] == 1 ? true : false;
   
   $it_opt1 = explode('|', $row[it_currency]);
   $totalopt = count($it_opt1)-1;

   $hzh = $row[it_id] == '1371778995' ? 1 : 0;
   $dikpung = $row[it_id] == '1372190258' ? 1 : 0;
   $blockb = $row[it_id] == '1371992245' ? 1 : 0;
   $solar = $row[it_id] == $SPECIAL[solar] ? 1 : 0;
   $sandara = $row['it_id'] == '1372929550' ? 1 : 0;
   $kara_fan = $row[it_id] == $SPECIAL[kara_fan] ? true : false;

   if($hzh == 1) {
      $sql = "SELECT mb_no FROM ".DB_ORDERS." WHERE it_id='1371778995' ANd od_amount='20000'";
      $res = sql_query($sql);
      $num = @mysql_num_rows($res);
      if($num == 64) {
         alert('참가팀수가 다 찼습니다');
         exit;
      }
   }
   if($kara_fan) {
      $ticket_chk = sql_fetch("SELECT COUNT(od_id) AS donate_num 
                  FROM ".DB_ORDERS." a 
                  WHERE a.it_id = '$row[it_id]'");
      if($ticket_chk[donate_num] >= $row[it_target]/5000) {
         alert('남아있는 티켓이 없습니다 T^T');
         exit;
      }
      $donate_chk = sql_fetch("SELECT od_id FROM ".DB_ORDERS." WHERE it_id = '$row[it_id]' AND mb_no = $_SESSION[user_no]");
      if($donate_chk[od_id]) {
         alert('이미 티켓을 구매하셨습니다. (1인 1매)');
         exit;
      }
   }
   if($row['it_id'] == $SPECIAL[cw1] || $row['it_id'] == $SPECIAL[cw2]) {
      $sql = "SELECT IFNULL(SUM(od_amount), 0) as funded,
         (SELECT IFNULL(SUM(round(od_amount/15000, 0)),0) FROM ".DB_ORDERS." WHERE it_id='$row[it_id]' 
            AND substr(reward,1,1) = '1'
            AND IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1 ) as goods_num_1,
         (SELECT IFNULL(SUM(round(od_amount/15000,0)),0) FROM ".DB_ORDERS." WHERE it_id='$row[it_id]'
            AND substr(reward,1,1) = '2'
            AND IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1 ) as goods_num_2
         FROM ".DB_ORDERS." WHERE it_id='$row[it_id]' and IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1";
      $data = sql_fetch($sql);
      if($data[funded] >= $row[it_target]) {
         alert("재고가 없습니다 T^T");
         exit;
      }
      $type_cw = true;
      $goods_num = ($row[it_target] - $data[funded]) / 15000;
      if($row['it_id'] == $SPECIAL[cw1]) {
         $goods_1 = '1.봄의은하(압화)';
         $goods_2 = '2.브라운(기본)';
      } else {
         $goods_1 = "1.꽃밭에서(WHITE)";
         $goods_2 = "2.바램(BLACK)";
      }
      $goods_1_num = 50-(int)$data[goods_num_1];
      $cw_selected_1 = $goods_1_num == 0 ? false : 'selected';
      $goods_2_num = 50-(int)$data[goods_num_2];
      $cw_selected_2 = $cw_selected_1 ? false : 'selected';
   }
   // ACTIVITY LOG
   if ($_SESSION[user_no]) {
      mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'donate', param2 = '$row[it_id]', referer = '$_SERVER[HTTP_REFERER]' ");
   }
?>
<script>var http_host = "<?=$_SERVER[HTTP_HOST]?>";</script>
<script type="text/javascript" src="/module/kcp_mobile/approval_key.js"></script>
<script>
   
    /* kcp web 결제창 호출 (변경불가)*/
   function call_pay_form() {
      var v_frm = document.donationForm;

      layer_cont_obj   = document.getElementById("content");
      layer_pay_obj = document.getElementById("layer_pay");

      document.getElementById("fb-root").style.display="none";
      document.getElementById("header").style.display = "none";
      document.getElementById("footerwrap").style.display="none";
      document.getElementById("copyrightwrap").style.display="none";
      layer_cont_obj.style.display = "none";
      layer_pay_obj.style.display = "block";
      v_frm.target = "frm_pay";

      if(v_frm.encoding_trans.value == "UTF-8") {
         v_frm.action = PayUrl.substring(0,PayUrl.lastIndexOf("/")) + "/jsp/encodingFilter/encodingFilter.jsp";
         v_frm.PayUrl.value = PayUrl;
      }

      if(v_frm.Ret_URL.value == "") {
         /* Ret_URL값은 현 페이지의 URL 입니다. */
         alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다.");
         return false;
      } else {
         v_frm.submit();
      }
   }


    /* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청*/
   function chk_pay() {
      /*kcp 결제서버에서 가맹점 주문페이지로 폼값을 보내기위한 설정(변경불가)*/
      self.name = "tar_opener";

      var pay_form = document.donationForm;

      if (pay_form.res_cd.value == "3001" ) {
         alert("사용자가 취소하였습니다.");
         pay_form.res_cd.value = "";
         return false;
      } else if (pay_form.res_cd.value == "3000" ) {
         alert("30만원 이상 결제 할수 없습니다.");
         pay_form.res_cd.value = "";
         return false;
      }

      if (pay_form.enc_data.value != "" && pay_form.enc_info.value != "" && pay_form.tran_cd.value !="" ) {
         $('input[name=it_id]').val("<?=$param_1_arr[0]?>");
         $('input[name=od_zip1]').val("<?=$param_1_arr[1]?>");
         $('input[name=od_zip2]').val("<?=$param_1_arr[2]?>");
         $('input[name=od_addr1]').val("<?=$param_1_arr[3]?>");
         $('input[name=od_addr2]').val("<?=$param_1_arr[4]?>");

         var param_2_branch = "<?=$param_2_arr[0]?>";

         if( "1" == param_2_branch ) {
            $('input[name=od_isEvent]').filter("input[value="+"<?=$param_2_arr[1]?>"+"]").attr("checked", "checked");
         } else if( "2" == param_2_branch ) {
            $('input[name=reward_req]').val("<?=$param_2_arr[1]?>");
         } else if( "3" == param_2_branch ) {
            $('input[name=od_isEvent]').filter("input[value="+"<?=$param_2_arr[1]?>"+"]").attr("checked", "checked");
            $('input[name=reward_req]').val("<?=$param_2_arr[2]?>");
         }

         var receipt_val = "<?=$param_3_arr[0]?>";
         $('input[name=isReceipt]').filter("input[value="+receipt_val+"]").attr("checked", "checked");
         
         if( "1" == receipt_val ) {
            $('input[name=realname]').val("<?=$param_3_arr[1]?>");
            $('input[name=serial1]').val("<?=$param_3_arr[2]?>");
            $('input[name=serial2]').val("<?=$param_3_arr[3]?>");
         } else if( "2" == receipt_val) {
            $('input[name=permit1]').val("<?=$param_3_arr[1]?>");
            $('input[name=permit2]').val("<?=$param_3_arr[2]?>");
            $('input[name=permit3]').val("<?=$param_3_arr[3]?>");
            $('input[name=permitimage]').val("<?=$param_3_arr[4]?>");
         }
            
         pay_form.target = "";
         pay_form.action = "/campaign/pp_ax_hub.php";
         $("form#donationForm").attr("enctype", "multipart/form-data");
         pay_form.submit();
      } else {
         return false;
      }
   }


   function kcp_runPlugin(form) {
      var RetVal = false;
      if(0 == <?=$hzh?>) {
         <? if ($kara_fan) { ?>
         $('input[name=good_mny]').val('5000');
         <? } else if($type_cw)  { ?>
         var gnum = $('input[name=cw_goods_num]').val()*1;
         var lnum = $('select[name=reward_req]').val() == '<?=$goods_1?>' ? <?=$goods_1_num?> : <?=$goods_2_num?>;
         if(gnum > lnum) {
            qAlert('재고 수량보다 많습니다. (현재 재고량 : '+lnum+' 개)');
            $('input[name=cw_goods_num]').val('1');
            $('#cw_price').html('17,500');
            $('input[name=od_price]').val('17500');
            return false;
         }
         $('input[name=good_mny]').val($('input[name=od_price]').val());
      <? } else { ?>
         var p = checkRadioValue(document.donationForm.od_price);
         if (!p) {
            qAlert('후원하실 금액을 선택해 주세요.');
            $('html, body').animate({
               scrollTop: $(".donate_price").offset().top - ($(".donate_price").height()/2)
            }, 300);
            return false;
         }
         if (p == 'user_input') {
            p = $.trim($('input[name=user_input]').val());
            if (!p || parseInt(p) == 0) {
               qAlert('후원을 희망하는 금액을 입력해 주세요.');
               $('html, body').animate({
                  scrollTop: $(".donate_price tr:last").offset().top - ($(".donate_price").height()/2)
               }, 300);
               
               return false;
            }
            else if (p < 1000) {
               qAlert('후원은 최소 1,000원 이상부터 가능합니다.');
               $('html, body').animate({
                  scrollTop: $(".donate_price tr:last").offset().top - ($(".donate_price").height()/2)
               }, 300);
               return false;
            }
         }
         $('input[name=good_mny]').val(p);
      <? } ?>
      }else {
         if(!$('input[name=lol_team_name]').val()) {
            qAlert('팀명을 입력해주세요');
            $('input[name=lol_team_name]').focus();
            return false;
         }
         if(!$('input[name=lol_name_1]').val()) {
            qAlert('대표자 이름을 적어주세요');
            $('input[name=lol_name_1]').focus();
            return false;
         }
         
         if(!$('input[name=lol_num_1]').val()) {
            qAlert('대표자 번호를 적어주세요');
            $('input[name=lol_name_1]').focus();
            return false;
         }
         $('#lol_table').append("<input type='hidden' name='reward_req'/>");
         var lol = $('input[name=lol_team_name]').val() + '|' +
               $('input[name=lol_name_1]').val() +','+ $('input[name=lol_num_1]').val();

         $('input[name=reward_req]').val(lol);
         $('input[name=good_mny]').val('20000');
      }

      var m = checkRadioValue(document.donationForm.od_method);
      if (!m) {
         qAlert('결제방법을 선택해 주세요.');
         $('html, body').animate({
            scrollTop: $(".od_method").offset().top - ($(".od_method").height()/2)
         }, 300);
         return false;
      }

      if (!$.trim($('input[name=buyr_name]').val())) {
         qAlert('후원자 이름을 입력해 주세요.');
         $('input[name=buyr_name]').focus();
         return false;
      }

      var hps = ['input[name=od_hp1]', 'input[name=od_hp2]', 'input[name=od_hp3]'];
      for (var i = 0; i < 3; i++)   {
         if (!$.trim($(hps[i]).val())) {
            qAlert('후원자 연락처를 입력해 주세요.');
            $(hps[i]).focus();
            return false;
         }
      }
      $('input[name=buyr_tel1]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());

      if (!$.trim($('input[name=buyr_mail]').val())) {
         qAlert('후원자 이메일 주소를 입력해 주세요.');
         $('input[name=buyr_mail]').focus();
         return false;
      }

      if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
         qAlert('이메일 주소가 올바르지 않습니다.');
         $('input[name=buyr_mail]').val('').focus();
         return false;
      }

      if (!$.trim($('input[name=od_zip1]').val())) {
         qAlert('우편번호 검색창을 통해 주소를 입력해 주세요.');
         $('html, body').animate({
            scrollTop: $(".account_info .zipcode-finder").offset().top - ($(".account_info").height()/2)
         }, 300);
         return false;
      }

      if (!$.trim($('input[name=od_addr2]').val()) || $('input[name=od_addr2]').val() == '상세주소 입력') {
         qAlert('상세주소를 입력해 주세요.');
         $('html, body').animate({
            scrollTop: $("input[name=od_addr2]").offset().top - ($(".account_info").height()/2)
         }, 300);
         return false;
      }
      var param_1_data = "<?=$row[it_id]?>"+ "|" + 
               $('input[name=od_zip1]').val() + "|" + $('input[name=od_zip2]').val() + "|" +
               $('input[name=od_addr1]').val() + "|" + $('input[name=od_addr2]').val();
      $("input[name=param_opt_1]").val(param_1_data);

      var param_2_data = 0;
    <?
    $sql = "SELECT * FROM ".DB_FUNDRAISERS."
         WHERE it_id = '$row[it_id]'
         ";
    $result = sql_query($sql);
    $total = mysql_num_rows($result);
    if ($total > 0 && !$kara_fan) {
       if( $hzh == 0) {
   ?>
      if (!checkRadioValue(document.donationForm.od_isEvent)) {
         qAlert('이벤트 참여 여부를 선택해 주세요.');
         $('html, body').animate({
            scrollTop: $(".event").offset().top - ($(".event").height()/2)
         }, 300);
         return false;
      }
    <? } else { ?>
      $('#lol_table').append("<input type='hidden' name='od_isEvent'/>");
      $('input[name=od_isEvent]').val('24');
    <? } ?>
      param_2_data = 1;
    <? } ?>
    
      if($('input[name=reward_req]').val()) {
         param_2_data = param_2_data == 1 ? 3 : 2;
      }

      if(param_2_data == 1) {
         param_2_data += "|" + $("input[name=od_isEvent]").val();
      } else if( param_2_data == 2 ) { 
         var reward_req_val = '<?=$row[it_id]?>' == '<?=$SPECIAL[cam_school]?>' ? $('input[name=reward_req]:checked').val() : $('input[name=reward_req]').val();
         param_2_data += "|" + reward_req_val;
      } else if( param_2_data == 3 ) {
         param_2_data += "|" + $("input[name=od_isEvent]").val() + "|" + $('input[name=reward_req]').val();
      }
      $("input[name=param_opt_2]").val(param_2_data);
      
    <? if ($row[it_isReceipt] == 1) : ?>
      var isReceipt = checkRadioValue(document.donationForm.isReceipt);
      var receipt_data = isReceipt + "|";
      if (isReceipt == 1) {
         if (!$.trim($('input[name=realname]').val())) {
            qAlert('영수증 발행을 위해 실명을 입력해 주세요.');
            return false;
         }
         if (!$.trim($('input[name=serial1]').val()) || !$.trim($('input[name=serial2]').val())) {
            qAlert('영수증 발행을 위해 주민등록번호를 입력해 주세요.');
            return false;
         }
         receipt_data += $.trim($('input[name=realname]').val()) + "|" + $.trim($('input[name=serial1]').val()) + "|"
                  + $.trim($('input[name=serial2]').val());
      }
      if (isReceipt == 2) {

         if (!$.trim($('input[name=permit1]').val()) || !$.trim($('input[name=permit2]').val()) || !$.trim($('input[name=permit3]').val())) {
            qAlert('영수증 발행을 위해 사업자등록번호를 입력해 주세요.');
            return false;
         }

         var ext = $('input[name=permitimage]').val().split('.').pop().toLowerCase();
         if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            qAlert('사업자등록증 이미지가 올바른 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
            return false;
         }
         receipt_data += $.trim($('input[name=permit1]').val()) + "|" + $.trim($('input[name=permit2]').val()) + "|" 
                  + $.trim($('input[name=permit3]').val()) + "|" + $('input[name=permitimage]').val();
      }
      
      $("input[name=param_opt_3]").val(receipt_data);
    <? endif; ?>
      if(m == "100000000000") { //card
         $('input[name=pay_method]').val('CARD');
         $('input[name=ActionResult]').val('card');
         RetVal = true;
      } else if(m == "001000000000") { //acc
         $('input[name=pay_method]').val('VCNT');
         $('input[name=ActionResult]').val('vcnt');
         RetVal = true;
      } else if(m == "000010000000") { //mob
         $('input[name=pay_method]').val('MOBX');
         $('input[name=ActionResult]').val('mobx');
         RetVal = true;
      }
      $('input[name=use_pay_method]').val(m);

      if(m== 'coin') {
         document.donationForm.action = '/campaign/proc_coin.php';
         document.donationForm.method = 'post';
         $('form[name=donationForm]').submit();
         return true;
      } else {
         kcp_AJAX();
         RetVal = true;
      }

      
      return RetVal ;
    }


    $(document).ready(function() {
      chk_pay();
      $('input[name=isReceipt]').change(function() {
         switch($(this).val()) {
         case '0':
            $('tr.ppForm, tr.pcForm').hide();
            break;
         case '1':
            $('tr.ppForm').show();
            $('tr.pcForm').hide();
            break;
         case '2':
            $('tr.ppForm').hide();
            $('tr.pcForm').show();
            break;
         }
      });

      $('input[name=buyr_phone2]').keyup(function() {
         if($(this).val().length == 4) {
            $('input[name=buyr_phone3]').focus();
         } 
      });
      
      $('input[name=serial1]').keyup(function() {
         if($(this).val().length == 6) {
            $('input[name=serial2]').focus();
         }
      });

      $('input[name=permit1]').keyup(function() {
         if($(this).val().length == 3) {
            $('input[name=permit2]').focus();
         }
      });

      $('input[name=permit2]').keyup(function() {
         if($(this).val().length == 2) {
            $('input[name=permit3]').focus();
         }
      });
<? if($type_cw) { ?>
      $('#cw_mod_btn').click(function() {
         var gnum = $('input[name=cw_goods_num]').val()*1;
         var lnum = $('select[name=reward_req]').val() == '<?=$goods_1?>' ? <?=$goods_1_num?> : <?=$goods_2_num?>;
         if(gnum > lnum) {
            qAlert('재고 수량보다 많습니다. (현재 재고량 : '+lnum+' 개)');
            $('input[name=cw_goods_num]').val('1');
            $('#cw_price').html('17,500');
            $('input[name=od_price]').val('17500');
            return false;
         }
         $('input[name=od_price]').val(''+(gnum*15000+2500));
         var reg = /\B(?=(\d{3})+(?!\d))/g
         $('#cw_price').html((gnum*15000+2500).toString().replace(reg, ","));
      });
<? } ?> 
    });
    
</script>


<script type='text/javascript' src='/js/zipsearch.js'></script>

<div id="layer_pay" style="position:absolute;width:100%;height:100%;z-index:1;display:none;">
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
        <tr width="100%" height="100%">
            <td style="height:inherit;">
                <iframe name="frm_pay"  frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"></iframe>
            </td>
        </tr>
    </table>
</div>

<div id='content'>
   <div class="donate_m">
      <p class='title'>후원하기</p>
      
      <form name='donationForm' method='post'>
         <h4><? if($type_cw) print '배송정보 입력'; else print '개인정보 입력';?></h4>
         <?
         $info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
         $contact = explode('-', $info[mb_contact]);
         ?>
         <table class='account_info' style=''>
            <tr>
               <td class='left_col' >이름</td>
               <td class='right_col'>
                  <input type='text' name='buyr_name' value="<?= $isKCP ? $buyr_name : $_SESSION[username]?>" style='width:50%;'>
               </td>
            </tr>
            <tr>
               <td class='left_col'>전화번호</td>
               <td class='right_col'>
                  <input type='number' name="od_hp1" value="<?=trim($contact[0])?>" max='9999' style='width:16%'> - 
                  <input type='number' name="od_hp2" value="<?=trim($contact[1])?>" max='9999' style='width:16%'> - 
                  <input type='number' name="od_hp3" value="<?=trim($contact[2])?>" max='9999' style='width:16%'>
               </td>
            </tr>
            <tr>
               <td class='left_col'>이메일</td>
               <td class='right_col'>
                  <input type='email' name='buyr_mail' value='<?= $isKCP ? $buyr_mail : $info[mb_email]?>' style='width: 82%;' />
               </td>
            </tr>
            <tr>
               <td class='left_col' >주소<? if($hzh == 1) print "<br/><br/><span style='color:red;font:8pt NanumGothic;'>대회관련 상품이 배송될 주소이니 정확히 기재해주세요</span>";?></td>
               <td class='right_col'>
                  <div class='zipcode-finder'>
                     <input type='text' id="dongName" style='width:60%;'/>
                     <input type='button' class='zipcode-search' value='검색' />
                     <div class="zipcode-search-result"></div>
                  </div>
                  <div id='addr'>
                     <input type='number' name="od_zip1" value="<?=$info[mb_zip1]?>" max='999' style="width: 12%;" readonly /> -
                     <input type='number' name="od_zip2" value="<?=$info[mb_zip2]?>" max='999' style="width: 12%;" readonly />
                     <input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 82%' readonly /><br/>
                     <input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 82%' />
                  </div>
               </td>
            </tr>
         </table>
         
         
         <h4>
         <? if($hzh == 1 ) { 
               print "팀 참가비 : 20,000원<br/><span style='color:#CCC;font:9pt NanumGothic;'><span style='color:red'>5명 팀</span>으로만 참가가 가능하고 팀 대표의 연락처는 꼭 작성해주시기바랍니다.</span>";}
            else if($type_cw){
               print '결제금액'; 
            } else { print "후원금액";}?>
         </h4>
      
         <? if($kara_fan) { ?>
            <table class='kara_ticket'>
               <tr>
                  <td>KARA 자선 팬미팅</td>
                  <td>5,000원</td>
                  <td>1매</td>
               </tr>
               <tr>
                  <td colspan='2'>TOTAL</td>
                  <td>5,000원</td>
               </tr>
            </table>
            <input type='hidden' name='od_price' value='5000'>

         <? } else if($type_cw) { ?>
            <table class='account_info' style='width:100%;'>
               <tr>
                  <td class='left_col'>상품종류</td>
                  <td class='right_col'><select name='reward_req'>
                     <option value='<?=$goods_1?>' <?=$cw_selected_1?> ><?="$goods_1 (남은갯수 : $goods_1_num 개)"?></option>
                     <option value='<?=$goods_2?>' <?=$cw_selected_2?> ><?="$goods_2 (남은갯수 : $goods_2_num 개)"?></option>
                  </select></td>
               </tr>
               <tr>
                  <td class='left_col'>판매가</td>
                  <td class='right_col'><span id='cw_price'>17,500</span>원 (배송비 2,500원 포함 가격)<input type='hidden' name='od_price' value='17500'></td>
               </tr>
               <tr>
                  <td class='left_col'>수량</td>
                  <td class='right_col'>
                     <input type="text" class='numonly' name='cw_goods_num' style='width:40px;' value='1'>
                     <button type='button' id='cw_mod_btn' style='width:44px;'>수정</button>
                  </td>
               </tr>
            </table>
         <? } else if($solar ==1) { ?>
            <table id='donate_price' class='donate_price'>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='10000' id='p1'></td>
                  <td class="right_col">
                     <label for='p1'>
                        <div class='left'>10,000원</div>
                     </label>
                     
                  </td>
               </tr>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='30000' id='p2'></td>
                  <td class="right_col">
                     <label for='p2'>
                        <div class='left'>30,000원</div> 
                     </label>
                  </td>
               </tr>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='50000' id='p3'></td>
                  <td class="right_col">
                     <label for='p3'>
                        <div class='left'>50,000원</div>
                     </label>
                  </td>
               </tr>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='150000' id='p4'></td>
                  <td class="right_col">
                     <label for='p4'>
                        <div class='left'>150,000원</div>
                     </label>
                  </td>
               </tr>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='300000' id='p5'></td>
                  <td class="right_col">
                     <label for='p5'>
                        <div class='left'>300,000원</div>
                     </label>
                  </td>
               </tr>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='5000000' id='p6'></td>
                  <td class="right_col">
                     <label for='p6'>
                        <div class='left'>500,000원</div>
                     </label>
                  </td>
               </tr>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='user_input' id='p7'></td>
                  <td class="right_col">
                     <label for='p7'>
                        <div class='left'>직접입력</div>
                        <div class='right'><input type='number' name='user_input' max='99999999'/> 원</div>
                     </label>
                  </td>
               </tr>
            </table>
         <? } else if($hzh == 1) { ?>
            <div style='width:100%;text-align:left;padding-left:2px;padding-top:10px;'>
               <strong>팀명 : </strong> <input type='text' name='lol_team_name'/> <br/>
            </div>
            <table id='lol_table' style='width:100%;'>
               <tr>
                  <th style='width:20px;'></th>
                  <th style='padding:0;'>이름</th>
                  <th style='padding:0;'>전화번호</th>
               </tr>
               <tr>
                  <td style='text-align:center;width:20px;'>1<br/><span style='color:red;'>(대표)</span></td>
                  <td><input type='text' name='lol_name_1'/></td>
                  <td><input type='number' name='lol_num_1'/></td>
               </tr>
                  
            </table>
         <? } else { ?>
            <table id='donate_price' class='donate_price'>
               <?
               for ($i = 0; $i < count($it_opt1); $i++) {
                  $it_opt = explode(';', $it_opt1[$i]);
                  $it_opt = $it_opt[0];
                  if ($it_opt != 'user_input') {
               ?>
               <tr>
               <td class="left_col"><input type='radio' name='od_price' value='<?=$it_opt?>' id='p<?=$i?>'></td>
                  <td class="right_col">
                  <label for='p<?=$i?>'>
                        <div class='left'><?=number_format($it_opt)?>원</div>
                        <div class='right'>
                           <img src='/images/m/donation_ico_<?=$it_opt?>.gif'/>
                        </div>
                     </label>
                     
                  </td>
               </tr>
               <? }} ?>
               <tr>
                  <td class="left_col"><input type='radio' name='od_price' value='user_input' id='p7'></td>
                  <td class="right_col">
                     <label for='p7'>
                        <div class='left'>직접입력</div>
                        <div class='right'><input type='number' name='user_input' max='999999'/> 원</div>
                     </label>
                  </td>
               </tr>
            </table>
         <? } ?>
         <h4>결제 방법</h4>
         <ul class='od_method'>
            <li>
               <input type='radio' name='od_method' value='000010000000' id='mo' checked />
               <label for='mo' style="margin-left:3px">휴대폰</label>
            </li>
            <li>
               <input type='radio' name='od_method' value='100000000000' id='cd' />
               <label for='cd' style="margin-left:3px">신용카드 <span style='font-size:11px'>(모바일ISP나 Paypin 설치가 필요합니다)</span></label>
            </li>
            <li>
               <input type='radio' name='od_method' value='001000000000' id='vc' />
               <label for='vc' style="margin-left:3px">가상계좌(무통장입금) <span style='font-size:11px'></span></label>
            </li>
            <li>
               <input type='radio' name='od_method' value='coin' id='co'>
               <label for='co' style="margin-left:3px">코인 <span style='font-size:11px'></span></label>
            </li>
         </ul>
         
         
         <?
         $sql = "SELECT * FROM ".DB_FUNDRAISERS."
               WHERE it_id = '$row[it_id]'
               ";
         $result = sql_query($sql);
         $total = mysql_num_rows($result);
         
         if ($total > 0 && !$kara_fan) {
            if($hzh ==0 ) {
            ?>
         
            <h4>이벤트 참여 여부 선택</h4>
            <ul class='event'>
               <?
               for ($i = 0; $fr = sql_fetch_array($result); $i++) {
                  $fr[fr_desc] = nl2br($fr[fr_desc]);
               ?>
               <li>
                  <input type='radio' name='od_isEvent' value="<?=$fr[no]?>" id='e<?=$i?>'/>
                  <label for='e<?=$i?>' title='<?=$fr[fr_desc]?>'><strong><?=$fr[fr_name]?></strong>님의 펀드레이저 이벤트에 참여합니다.</label>
               </li>
               <? } ?>
               <li>
                  <input type='radio' name='od_isEvent' id='ef'/>
                  <label for='ef'>이벤트에 참여하지 않겠습니다.</label>
               </li>
            </ul>
            
            <div class='additional'>
                  <p>위젠은 기부만을 희망하시는 회원님의 모금액에는 일체 수수료를 부과하지 않습니다.<br/>
               이를 통해 100% 기부를 지속적으로 가능케 하려고 합니다.</p>
               <p>기부와 더불어 유명인사와 함께하는 펀드레이저 이벤트에 참여를 희망하시는 회원님의<br/>
               모금액에는 저녁식사 등 이벤트 활동비를 고려하여 총 20%의 운영수수료를 부과합니다.</p>
               <p>이벤트는 추첨으로 진행되며 기부액과 참여도를 고려하여 확률이 가산됩니다.</p>
            </div>
         
         <? }
         } else { ?>
               <input type='hidden' name='od_isEvent' value='없음' />
         <? } ?>
         
         <?
            if($row[it_id] == $SPECIAL[cam_school]) { ?>
      <h4 style='margin-top:20px'>수혜자 선택</h4>
      <ul>
         <li><input type="radio" name='reward_req' value="1" id='cs1' checked>
            <label for='cs1'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_1.jpg' target='_blank' title="<div style='overflow: hidden; max-height:800px'><img src='/images/campaign/cam_school_detail_1.jpg' style='width: 100%' /></div>">
               신하영 (9세) 지적장애 3급
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="2" id='cs2'>
            <label for='cs2'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_2.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_2.jpg' style='width: 100%' /></div>">
               김규리 (13세) 뇌성마비 3급
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="3" id='cs3'>
            <label for='cs3'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_3.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_3.jpg' style='width: 100%' /></div>">
               전혜민 (16세) 연골무형성증
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="4" id='cs4'>
            <label for='cs4'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_4.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_4.jpg' style='width: 100%' /></div>">
               박선영 (10세) 뇌병변 1급
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="5" id='cs5'>
            <label for='cs5'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_5.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_5.jpg' style='width: 100%' /></div>">
               김민규 (14세) 연골무형성증
            </span></label>
         </li>
      </ul>
            

         <? }
            if ($row[it_reward_fromer]) :
         ?>
         <h4>리워드 선택</h4>
         <table class='reward_req'>
            <tr>
               <td class='left_col'>요청사항</td>
               <td class='right_col'>
                  <input type='text' name='reward_req' class='reward_input' style='width:85%'/>
               </td>
            </tr>
         </table>
         <div class='additional'><p><?=nl2br($row[it_reward_fromer])?></p></div>
         <?
         // Added by 2013.08.07
         $additional_reward_image = "/data/campaign/{$_GET['it_id']}/reward.jpg";
         if(file_exists("..{$additional_reward_image}")) {
            echo "<img src='{$additional_reward_image}' style='width:100%;'>";
         }
         ?>
         <?php endif;?>
         
         <h4 style='<?=$hzh == 1 ? 'display:none;' : false ?>'>기부금 영수증 신청</h4>
         <? if ($row[it_isReceipt] == 1) : ?>
         <ul class='receipt' id='donationReceipt' style='<?=$hzh == 1 ? 'display:none;' : false ?>'>
            <li><input type='radio' name='isReceipt' value='0' id='pn' checked /><label for='pn'>미신청</label></li>
            <li><input type='radio' name='isReceipt' value='1' id='pp' /><label for='pp'>개인</label></li>
            <li><input type='radio' name='isReceipt' value='2' id='pc' /><label for='pc'>사업자</label></li>
         </ul>
         
         <div style='clear: both;<?=$hzh == 1 ? 'display:none;' : false ?>'></div>
      
         <table class='receipt' style='<?=$hzh == 1 ? 'display:none;' : false ?>'>
            <tr class='ppForm' style='display: none'>
               <td class='left_col'>실명</td>
               <td class='right_col'>
                  <input type='text' name='realname' style='width:50%;' />
               </td>
            </tr>
            <tr class='ppForm' style='display: none'>
               <td class='left_col'>주민등록번호</td>
               <td class='right_col'>
                  <input type='number' name='serial1' max='999999' style='width: 23%;' /> - 
                  <input type='number' name='serial2' max='9999999' style='width: 23%;' />
               </td>
            </tr>
            <tr class='pcForm' style='display: none'>
               <td class='left_col'>사업자등록번호</td>
               <td class='right_col'>
                  <input type='number' name='permit1' max='999' style='width: 15%;' /> - 
                  <input type='number' name='permit2' max='99'  style='width: 10%;' /> - 
                  <input type='number' name='permit3' max='99999' style='width: 25%;' />
               </td>
            </tr>
            <tr class='pcForm' style='display: none'>
               <td class='left_col'>등록증 사본</td>
               <td class='right_col'>
                  <input type='file' name='permitimage' style='width: 100%; text-align: center' />
               </td>
            </tr>
         </table>
         
         <? else : ?>
            <ul class='receipt' id='donationReceipt'>
               <li style='width: 100%; color: silver'>본 캠페인은 기부금 영수증 발급이 불가능합니다.</li>
            </ul>
         <? endif; ?>
         
         <div class='additional' style='<?=$hzh == 1 ? 'display:none;' : false ?>' >
            <p>영수증 발행은 해당 캠페인의 지원 단체에 요청되며, 기부금 영수증은 캠페인이 완료된 후 해당 단체를 통해 일괄 발송됩니다.<br/>
            입력하신 회원님의 실명 정보와 주민등록번호는 영수증 발행 이의외 목적으로는 사용되지 않으며, 목적 달성 즉시 파기됩니다. 입력하신 정보가 정확하지 않을 경우 영수증 발급이 어려울 수 있습니다.</p>
         </div>
         
         <div class='additional'>
            <p>입력하신 개인정보는 캠페인 진행 상황 및 이벤트 당첨 알림, 리워드 발송 등의 목적으로만<br/>
            사용되며, <a href='/privacy-policy'><strong>개인정보 취급방침</strong></a>에 의거해 보호됩니다.</p>
         </div>
         
         <input type='button' value='<?=$hzh == 1 ? '참가신청' : '후원하기'?>' class='submit' onclick="kcp_runPlugin(this.form);" />
         
         <input type='hidden' name='ordr_idxx'     value='<?= $isKCP ? $ordr_idxx : date('ymdHis').rand(100, 999)?>' /> 
         <input type='hidden' name='it_id'         value='<?=$row[it_id]?>' />
         <input type='hidden' name='itemid'			value='<?=$_GET[it_id]?>' />
         <input type='hidden' name='pay_method'    value='' />
         <input type='hidden' name='buyr_tel1'     value='<?= $isKCP ? $buyr_tel1 : ""; ?> '/>
         <input type='hidden' name='buyr_tel2'     value='<?= $isKCP ? $buyr_tel2 : ""; ?>' />
         <input type='hidden' name='good_name'     value='<?= $isKCP ? $good_name : $row[it_name];?>' />
         <input type='hidden' name='good_mny'      value='<?= $isKCP ? $good_mny : "" ;?>'/>
         <input type='hidden' name='req_tx'        value='pay' />
         <input type='hidden' name='site_cd'       value="<?=$g_conf_site_cd?>" />
         <input type='hidden' name='site_key'      value="<?=$g_conf_site_key?>" />
         <input type='hidden' name='shop_name'     value="<?=$g_conf_site_name?>" />
         <input type='hidden' name='quotaopt'      value='12' />
         <input type='hidden' name='currency'      value='410'/>
         <input type="hidden" name='Ret_URL'       value="http://<?=$_SERVER[HTTP_HOST]?>/m/campaign/<?=$row[it_id]?>/donate">
         <input type="hidden" name='param_opt_1'    value="<?=$param_opt_1?>"/>
         <input type="hidden" name='param_opt_2'    value="<?=$param_opt_2?>"/>
         <input type="hidden" name='param_opt_3'    value="<?=$param_opt_3?>"/>
         <input type="hidden" name='escw_used'    value="N">
         <input type='hidden' name='connect_type'  value='MOBILE' />
         <input type="hidden" name="encoding_trans" value="UTF-8">
         <input type="hidden" name="PayUrl">
         
         <!-- PLUGIN 설정 (변경 불가) -->
         <input type='hidden' name='ActionResult'  value=''>
         <input type="hidden" name='approval_key'  id="approval">
         <input type='hidden' name='module_type'      value='01' />
         <input type='hidden' name='epnt_issu'     value='' />
         <input type='hidden' name='res_cd'        value='<?=$res_cd?>'/>
         <input type='hidden' name='res_msg'       value='' />
         <input type='hidden' name='tno'           value='' />
         <input type='hidden' name='trace_no'      value='' />
         <input type='hidden' name='enc_info'      value='<?=$enc_info?>' />
         <input type='hidden' name='enc_data'      value='<?=$enc_data?>' />
         <input type='hidden' name='ret_pay_method'   value='' />
         <input type='hidden' name='tran_cd'       value='<?=$tran_cd?>' />
         <input type='hidden' name='bank_name'     value='' />
         <input type='hidden' name='bank_issu'     value='' />
         <input type='hidden' name='use_pay_method'   value='<?=$use_pay_method?>' />
         <input type='hidden' name='cash_tsdtime'  value='' />
         <input type='hidden' name='cash_yn'       value='' />
         <input type='hidden' name='cash_authno'      value='' />
         <input type='hidden' name='cash_tr_code'  value='' />
         <input type='hidden' name='cash_id_info'  value='' />
         <input type="hidden" name="vcnt_expire_term" value="3"/>
         
         <!-- 제공 기간 설정 0:일회성 1:기간설정(ex 1:2012010120120131) -->
         <input type='hidden' name='good_expr'     value='0' />
         <input type='hidden' name='site_logo'     value='' />
         <input type='hidden' name='skin_indx'     value='1' />
         <?
         /*
         신용카드사 삭제
         <input type='hidden' name="not_used_card" value="CCPH:CCSS:CCKE:CCHM:CCSH:CCLO:CCLG:CCJB:CCHN:CCCH"/>
         사용카드 설정 여부 (통합결제창 노출 유무)
         <input type='hidden' name="used_card_YN"value="Y"/>
         사용카드 설정 (해당 카드만 결제창에 보이게 설정. used_card_YN 값이 Y일때 적용)
         <input type='hidden' name="used_card"value="CCBC:CCKM:CCSS"/>              
         해외카드 구분 (해외비자, 해외마스터, 해외JCB로 구분하여 표시)
         <input type='hidden' name="used_card_CCXX"value="Y"/>  
         결제창 영문 표시
         <input type='hidden' name='eng_flag'  value='Y'>
         현금영수증 등록창 출력 여부
         현금영수증 사용시 KCP 상점관리자 페이지에서 현금영수증 사용 동의 필요
         */
         ?>
         <input type='hidden' name='disp_tax_yn'      value='N' />
         <? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
         <input type='hidden' name='token'         value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
      </form>
   </div>
</div>

<?php include '../m_tail.php';?>
