<?
$_required = true;
include 'config.php';
sec_session_start();
include 'module/class.upload.php';

$password = $_POST['p'];
if($password == '' || $_POST[email] == '' || $_POST[username] == '') {
   alert('비정상적인 접근입니다.');
   exit;
}

$check_mb = sql_fetch("SELECT mb_no FROM ".DB_MEMBERS." WHERE mb_id = '$_POST[email]'");
if($check_mb[mb_no]) {
   alert('이미 존재하는 아이디입니다.');
   exit;
}

$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
$password = hash('sha512', $password.$random_salt);

if($stmt = $mysqli->prepare("INSERT INTO wegen_member SET 
                     mb_id = ?, 
                     mb_password = ?,
                     mb_name = ?,
                     mb_email = ?,
                     mb_contact = ?,
                     mb_zip1 = ?,
                     mb_zip2 = ?,
                     mb_addr1 = ?,
                     mb_addr2 = ?,
                     mb_agreesms = ?,
                     mb_agreemail = ?,
                     salt = ?")) {
   $stmt->bind_param('sssssssssiis', $_POST[email], $password, $_POST[username], $_POST[email], $_POST[mb_contact], 
                     $_POST[mb_zip1], $_POST[mb_zip2], $_POST[mb_addr1], $_POST[mb_addr2], $_POST[agree_sms] = (int)$_POST[agree_sms], $_POST[agree_mail] = (int)$_POST[agree_mail], $random_salt);
   $register_res = $stmt->execute();
   $mb_no = $stmt->insert_id;
} else {
   $register_res = false;
}
/*
sql_query("INSERT INTO ".DB_MEMBERS." SET 
      mb_id = '$_POST[email]',
      mb_password = '$password',
      mb_name = '$_POST[username]',
      mb_email = '$_POST[email]',
      mb_contact = '$_POST[mb_contact]',
      mb_zip1 = '$_POST[mb_zip1]',
      mb_zip2 = '$_POST[mb_zip2]',
      mb_addr1 = '$_POST[mb_addr1]',
      mb_addr2 = '$_POST[mb_addr2]',
      mb_agreesms = '$_POST[agree_sms]',
      mb_agreemail = '$_POST[agree_mail]',
      salt = '$random_salt'
      ");
$mb_no = mysql_insert_id();
*/

if ($_FILES['profilepicture']) {
	$handle = new upload($_FILES['profilepicture']);
	if ($handle->uploaded) {
		$handle->forbidden				= array('application/*');
		$handle->allowed				= array('image/*');
		$handle->image_convert			= 'jpg';
		$handle->file_new_name_body		= 'profile_'.$mb_no;
		$handle->image_resize			= true;
		$handle->image_ratio_crop		= true;
		$handle->image_x				= 100;
		$handle->image_y				= 100;
		$handle->process('data/member/');

		if ($handle->processed) {
			$handle->clean();
			mysql_query("UPDATE ".DB_MEMBERS." SET mb_icon = 'profile_$mb_no.jpg' WHERE mb_no = '$mb_no' ");
		} else {
			echo 'error : ' . $handle->error;
		}
	}
}

// ACTIVITY LOG
//mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '0', mb_no = '$check[no]', param1 = 'register', param2 = 'wegen', referer = '$_SERVER[HTTP_REFERER]' ");
?>
<html>
<head>
   <meta charset="UTF-8">
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<title>회원가입</title>
</head>
<body>

<?php
if ($post[type] == 'sns') print "<script>opener.reload(); self.close();</script>";
else if($register_res) {
	// 회원가입 후 '회원 가입이 완료되었습니다' 라는 팝업 생성
	login($_POST['email'], $_POST['p'],$mysqli);?>
   <script>
   $(document).ready(function() {
      $.ajax({
         url:'/module/sendsms.php',
         type:'POST',
         data:{
            sms_type:'join',
            mb_no:'<?=$mb_no?>'
         },
         cache:false,
         success: function(t) {
         }
      });
      $.ajax({
         url:'/module/sendmail.php',
         type:'POST',
         data:{
            mail_type:'join',
            mb_no:'<?=$mb_no?>'
         },
         cache:false,
         success: function(t) {
         }
      });
      
      alert('회원 가입이 완료되었습니다');
      window.location.href = '<?=$_POST['url'] != '' ? $_POST['url'] : '/'?>';
   });
      
   </script>
   <?
} else {
   alert('오류가 발생했습니다');
}
?>

</body>
</html>
