<?php
   if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
      header("HTTP/1.0 404 Not Found");
      exit;
   }

   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   sec_session_start();
   
   $ret = array();
   if(!$_SESSION[user_no] || !".$_POST[a_id].") {
      $ret[show_status] = 'failed';
      echo json_encode($ret);
      exit;
   }

   if(!$_POST[bid_amount] || !$_POST[mb_name] || !$_POST[mb_contact] || !$_POST[mb_zip1] || !$_POST[mb_zip2] || !$_POST[mb_addr1] || !$_POST[mb_addr2] ) {
      $ret[show_status] = 'failed';
      echo json_encode($ret);
      exit;
   }

   $auc_info = sql_fetch("SELECT * FROM ".DB_AUCTION." WHERE auc_id = '".$_POST[a_id]."'");
      
   if(strtotime($auc_info[auc_enddate])-time() < 0) {
      $ret[show_status] = 'failed';
      $ret[show_msg] = 'err_1';
      $ret[msg]= "SELECT * FROM ".DB_AUCTION." WHERE auc_id = '".$_POST[a_id]."'";
      echo json_encode($ret);
      exit;
   }

   $chk = sql_fetch("SELECT * FROM ".DB_BIDDING." a
      WHERE a.auc_id = '".$_POST[a_id]."' AND a.mb_no = '$_SESSION[user_no]'");
   if(!is_numeric($_POST[bid_amount])) {
      $ret[show_status] = 'failed';
      echo json_encode($ret);
      exit;
   }
   if((int)$_POST[bid_amount] < $chk[auc_min]) {
      $ret[show_status] = 'failed';
      $ret[show_msg] = 'err_2';
      $ret[show_val1] = $chk[auc_min];
      echo json_encode($ret);
      exit;
   }

   if($chk[bid_id]) {
      if((int)$_POST[bid_amount] % 1000 != 0) {
         $ret[show_status] = 'failed';
         $ret[show_msg] = 'err_3';
         echo json_encode($ret);
         exit;
      }
      if((int)$_POST[bid_amount] < $chk[bid_amount]) {
         $ret[show_status] = 'failed';
         $ret[show_msg] = 'err_4';
         echo json_encode($ret);
         exit;
      }
   }
   $chk = sql_fetch("SELECT MAX(bid_amount) as max_amount FROM ".DB_BIDDING." WHERE auc_id = '".$_POST[a_id]."'");
   if($chk[max_amount] >= $_POST[bid_amount]) {
      $ret[show_status] = 'failed';
      $ret[show_msg] = 'err_5';
      $ret[show_val1] = number_format($chk[max_amount]);
      echo json_encode($ret);
      exit;
   }

   sql_query("DELETE FROM ".DB_BIDDING." WHERE auc_id = '".$_POST[a_id]."' AND mb_no = '$_SESSION[user_no]'");


   $res = sql_query("INSERT INTO ".DB_BIDDING." SET 
         mb_no = '$_SESSION[user_no]',
         bid_amount = '$_POST[bid_amount]',
         auc_id = '".$_POST[a_id]."',
         bid_name = '$_POST[mb_name]',
         bid_contact = '$_POST[mb_contact]',
         bid_zip1 = '$_POST[mb_zip1]',
         bid_zip2 = '$_POST[mb_zip2]',
         bid_addr1 = '$_POST[mb_addr1]',
         bid_addr2 = '$_POST[mb_addr2]'
         ");
   if(!$res) {
      $ret[show_status] = 'failed333';
      echo json_encode($res);
      exit;
   } 
   $id = mysql_insert_id();
   $info = sql_fetch("SELECT bid_time, 
         (SELECT COUNT(bid_id) + 1 FROM ".DB_BIDDING." WHERE auc_id = '".$_POST[a_id]."' AND bid_amount > a.bid_amount OR IF(bid_amount = a.bid_amount, IF(bid_time < a.bid_time, 1, 0), 0) = 1 ) AS rank 
         FROM ".DB_BIDDING." a WHERE a.bid_id = '$id'");
   $ret[bid_time] = $info[bid_time];
   $ret[bid_rank] = $info[rank];
   $ret[show_status] = 'success';
   $ret[tester] = "SELECT bid_time, 
         (SELECT COUNT(bid_id) + 1 FROM ".DB_BIDDING." WHERE auc_id = '".$_POST[a_id]."' AND bid_amount > a.bid_amount OR IF(bid_amount = a.bid_amount, IF(bid_time < a.bid_time, 1, 0), 0) = 1 ) AS rank 
         FROM ".DB_BIDDING." a WHERE a.bid_id = '$id'";

   sql_query("UPDATE ".DB_MEMBERS." SET 
         mb_name = '$_POST[mb_name]',
         mb_contact = '$_POST[mb_contact]',
         mb_zip1 = '$_POST[mb_zip1]',
         mb_zip2 = '$_POST[mb_zip2]',
         mb_addr1 = '$_POST[mb_addr1]',
         mb_addr2 = '$_POST[mb_addr2]',
         mb_email = '$_POST[mb_email]' 
         WHERE mb_no = '$_SESSION[user_no]'");

   echo json_encode($ret);
   exit;
?>
