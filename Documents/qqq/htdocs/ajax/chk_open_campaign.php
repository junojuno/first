<?php
   if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
      header("HTTP/1.0 404 Not Found");
      exit;
   }

   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   sec_session_start();
   
   $ret = array();
   if($_POST[logined_before_click] == '1') {
      $it_id = $_POST[it_id];
      if (!preg_match("/^[0-9]{10}$/", $it_id) || !$_SESSION[user_no]) {
         $ret[show_status] = "failed1".$_SESSION[user_no];
         echo json_encode($ret);
         exit;
      } 
      $chk = sql_fetch("SELECT a.* FROM ".DB_ORDERS." a 
            WHERE a.it_id = '$it_id' AND a.od_method='무료응모' AND a.mb_no = $_SESSION[user_no]");
            
      if($chk[od_id]) {
         $ret[show_status] = 'before_err_1';
         echo json_encode($ret);
         exit;
      } else {
         $ret[show_status] = 'before_success';
         echo json_encode($ret);
         exit;
      }
   }

   if($_POST[mb_name] == '' || $_POST[mb_email] == '' || $_POST[mb_contact] == '' || $_POST[it_id] == '' || $_POST[open_msg] == '')  {
      $ret[show_status] = 'failed';
      echo json_encode($ret);
      exit;
   }

   $contact = $_POST[mb_contact];
   $email = $_POST[mb_email];
   $it_id = $_POST[it_id];
   if (!preg_match("/^[0-9]{10}$/", $it_id)) {
      $ret[show_status] = 'failed';
      echo json_encode($ret);
      exit;
   } 

   $mb_no = $_SESSION[user_no] ? " OR a.mb_no = ".$_SESSION[user_no] : '';
   if($_POST[fb_account] == '1') {
      include $_SERVER[DOCUMENT_ROOT].'/module/facebook/setting.php';
      $user = $facebook->getUser();
      if($user) {
         try {
            $user_profile = $facebook->api('/me');
         }
         catch (FacebookApiException $e) {
            $ret[show_status] = 'get user profile error';
            echo json_encode($ret);
            exit;
         }
         $pwdstr = hash('sha512', $user_profile[id].'tryFaceBOOKsessionLOGin');
         $membercheck = login('fb@'.$user_profile[id], $pwdstr, $mysqli);
         if ($membercheck !== '0009') {
            $mb_no = " OR a.mb_no = ".$_SESSION[user_no];
         }
      } 
   }

   $chk = sql_fetch("SELECT a.*, b.mb_email FROM ".DB_ORDERS." a 
         LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
         WHERE a.it_id = '$it_id' AND a.od_method='무료응모' AND (od_hp = '$contact' OR b.mb_email = '$email' $mb_no)");
         
   if($chk[od_id]) {
      $ret[show_status] = 'err_1';
      if($_POST[fb_account] == '1') $ret[show_status] = 'fb_err_1';
      echo json_encode($ret);
      exit;
   } else {
      $ret[show_status] = 'success';
      echo json_encode($ret);
      exit;
   }
?>
