<?php
   if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
      header("HTTP/1.0 404 Not Found");
      exit;
   }

   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   sec_session_start();
   
   $ret = array();
   if($_POST[mb_name] == '' || $_POST[mb_email] == '' || $_POST[mb_contact] == '' || $_POST[it_id] == '' || $_POST[open_msg] == '')  {
      $ret[show_status] = 'failed';
      echo json_encode($ret);
      exit;
   }
   $mb_name = $_POST[mb_name];
   $mb_email = $_POST[mb_email];
   $mb_contact = $_POST[mb_contact];
   $it_id = $_POST[it_id];
   $open_msg = $_POST[open_msg];
   
   if($_SESSION[user_no]) {
      $mb_no = $_SESSION[user_no];
   } else {
      include $_SERVER[DOCUMENT_ROOT].'/module/facebook/setting.php';
      $user = $facebook->getUser();
      if($user) {
         try {
            $user_profile = $facebook->api('/me');
         }
         catch (FacebookApiException $e) {
            $ret[show_status] = 'get user profile error';
            echo json_encode($ret);
            exit;
         }
         $pwdstr = hash('sha512', $user_profile[id].'tryFaceBOOKsessionLOGin');
         $membercheck = login('fb@'.$user_profile[id], $pwdstr, $mysqli);
         if ($membercheck === '0009') {
            $password = hash('sha512', $user_profile[id].'tryFaceBOOKsessionLOGin');
            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $password = hash('sha512', $password.$random_salt);
            $fbid = 'fb@'.$user_profile[id];

            if (!$user_profile[email]) $user_profile[email] = 'asd@asd.com';

            if ($stmt = $mysqli->prepare("INSERT INTO wegen_member (mb_id, mb_name, mb_password, mb_email, salt) VALUES (?, ?, ?, ?, ?)")) {    
               $stmt->bind_param('sssss', $fbid, $user_profile[name], $password, $user_profile[email], $random_salt); 
               $stmt->execute();

               $mb_no = $stmt->insert_id;
            }
            // and login again
            login('fb@'.$user_profile[id], $pwdstr, $mysqli);
         }

         $mb_no = $_SESSION[user_no];
      } else {
         $ret[show_status] = 'facebook_not_logged';
         echo json_encode($ret);
         exit;
      }
   }

   if($mb_no) {
      sql_query("UPDATE ".DB_MEMBERS." SET 
         mb_name = '$mb_name',
         mb_email = '$mb_email',
         mb_contact = '$mb_contact'
      WHERE mb_no = $mb_no");

      $ordr_idxx = date('ymdHis').rand(100, 999);
      $cust_ip	= getenv("REMOTE_ADDR");
      $od_time = date('Y-m-d H:i:s');

      $res =  sql_query("INSERT INTO ".DB_ORDERS." SET 
            od_id = '$ordr_idxx',
            it_id = '$it_id',
            mb_no = '$mb_no',
            od_name = '$mb_name',
            od_hp = '$mb_contact',
            od_ip = '$cust_ip',
            od_time = '$od_time',
            od_method = '무료응모',
            od_amount = '100',
            pay_remain = '100',
            var_1 = '무료응모'
            ");
      if($res) {
         $ret[show_status] = 'success';
         echo json_encode($ret);
         exit;
      }
   }

   $ret[show_status] = 'failed';
   echo json_encode($ret);
   exit;
