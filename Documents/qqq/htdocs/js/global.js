var serverName = 'wegen.kr';
$(document).ready(function() {
   function n(e) {
      var t = $(this).data("qtip"), n = 3e3;
      if (t.get("show.persistent") === true) {
         return
      }
      clearTimeout(t.timer);
      if (e.type !== "mouseover") {
         t.timer = setTimeout(t.hide, n)
      }
   }
   var fb_appId = _debug == '1' ? "408565205831331" : "211534425639026";
   FB.init({
      appId : fb_appId,
      status : true,
      cookie : true,
      xfbml : true
   });
   $.fn.qtip.defaults.position.my = "bottom center";
   $.fn.qtip.defaults.position.at = "top center";
   $.fn.qtip.defaults.style.classes = "qtip-bootstrap";
   $.fn.pressEnter = function(e) {
      return this.each(function() {
         $(this).bind("enterPress", e);
         $(this).keyup(function(e) {
            if (e.keyCode == 13)
               $(this).trigger("enterPress")
         });
      });
   };
   var e = [ "campaign", "givex", "about", "request", "postscript", "partners" ];
   for ( var t = 0; t < e.length; t++) {
      if (document.URL.replace("http://", "").split("/")[1]
            .indexOf(e[t]) != -1) {
         $("#menu a").eq(t).addClass("on");
      }
   }
   $('[data-href]').css('cursor','pointer');
   $('[data-href]').click(function(){
      if($(this).data('nw')) {
         window.open($(this).data('href'));
      } else {
         location.href = $(this).data('href');
      }
   });
   var recent_enter = false;
   $('.recent .list').mouseenter(function() {
      recent_enter = true;
   }).mouseleave(function() {
      recent_enter = false;
   });
   $('.recent').mouseenter(function(){
      $('.recent .list').slideDown(800);
   }).mouseleave(function() {
      if(!recent_enter) {
         $('.recent .list').slideUp(400);
      }
   });
   $("#slides").slidesjs();
   if($('.js_slides').length > 0) {
      var wdh = $(window).width()*1 < 980 ? '980' : $(window).width();
      $('.js_slides li').css('width', wdh);
      $(window).resize(function() { 
         var wdh = $(window).width()*1 < 980 ? '980' : $(window).width();
         $('.js_slides li').css('width', wdh);
      });
      $('.js_slides').jcarousel({
         wrap:'circular',
         animation:{
            duration:400,
            easing:'linear'
         }
      }).jcarouselAutoscroll({
         interval:5000,
         target:'+=1',
         autostart:true
      });
      $('.main_top .next').jcarouselControl({
         carousel:$('.js_slides'),
         target:'+=1'
      });
      $('.main_top .prev').jcarouselControl({
         carousel:$('.js_slides'),
         target:'-=1'
      });
      $('.js-slides-pagination').on('jcarouselpagination:active','li', function() {
         $(this).addClass('active');
      }).on('jcarouselpagination:inactive','li', function() {
         $(this).removeClass('active');
      }).jcarouselPagination({
         carousel:$('.js_slides'),
         item:function(page) {
            return '<li></li>';
         }
      });
   }

   if($('.main_scrolling').length > 0) {
      $('.main_scrolling.fundraiser .scroll_content, .main_scrolling.sponsor .scroll_content, .main_scrolling.partner .scroll_content').jcarousel({
         animation:{
            duration:400
         },
         wrap:'circular'
      }).jcarouselAutoscroll({
         interval:2300,
         target:'+=1',
         autostart:true
      });
      $('.main_scrolling.fundraiser .next').jcarouselControl({
         carousel:$('.main_scrolling.fundraiser .scroll_content'),
         target:'+=1'
      });
      $('.main_scrolling.fundraiser .prev').jcarouselControl({
         carousel:$('.main_scrolling.fundraiser .scroll_content'),
         target:'-=1'
      });
      $('.main_scrolling.sponsor .next').jcarouselControl({
         carousel:$('.main_scrolling.sponsor .scroll_content'),
         target:'+=1'
      });
      $('.main_scrolling.sponsor .prev').jcarouselControl({
         carousel:$('.main_scrolling.sponsor .scroll_content'),
         target:'-=1'
      });
      $('.main_scrolling.partner .next').jcarouselControl({
         carousel:$('.main_scrolling.partner .scroll_content'),
         target:'+=1'
      });
      $('.main_scrolling.partner .prev').jcarouselControl({
         carousel:$('.main_scrolling.partner .scroll_content'),
         target:'-=1'
      });
   }

   $("#loginCombo li").hover(function() {
      $(this).css("background-position", "bottom");
   }, function() {
      $(this).css("background-position", "top");
   });
   $(".btn_login_facebook").click(function() {
      window.open("/facebook/");
   });
   $(".btn_login_twitter").click(function() {
      window.open("/twitter/", "loginTW", "width=500, height=250");
   });

   $(".btn_login").click(function() {
      if($('#header input[name=ismobile]').val() == 1) {
         location.href='/m/login_m.php?url='+$('#loginLayer input[name=url]').val();
      } else {
         $("#movieFrame").hide();
         createLayer($("#loginLayer"));
         if ($(this).attr("url")) {
            $("input[name=url]").val(encodeURIComponent($(this).attr("url")));
            $("#regLink").attr("href","/register?url="+encodeURIComponent($(this).attr("url")));
         }
         $('input[name=uid]').focus();
      }
   });
   
   $("#loginLayer input[name=p]").keydown(function(e) {
      if (e.which == '13') {
         if(checkLoginForm()) {
            processLogin($('#loginLayer input[name=url]').val());
         }
      }
   });
   
   $("#loginLayer input[name=p]").focusout(function(e){
      if(!$("#loginLayer input[name=p]").val()) {
         $("#loginLayer input[name=p]").hide();
         $("#loginLayer input[name=password_pseudo").show();
      }
   });

   $("#loginLayer .btn_close").click(function() {
      $("#movieFrame").show();
      destroyLayer($("#loginLayer"));
   });
   
   $("#coinDonateLayer .btn_close").click(function() {
      $("#movieFrame").show();
      destroyLayer($("#coinDonateLayer"));
   });

   $("#invite_email .btn_close").click(function() {
      destroyLayer($("#invite_email"));
   });
   $("#myInfoBox #myBadge li").css("height", $("#myInfoBox #myBadge li").eq(0).width() + "px");
   $(".campaignCell").hover(function() {
      $(this).stop().animate({
         borderColor : "#E30000"
      }, 250, function() {
      });
   }, function() {
      $(this).stop().animate({
         borderColor : "#E2E2DF"
      }, 250);
   });
   $(".gauge").each(function() {
      var e = $(".percent", this).width() - ($(this).width() - $(".progress", this).width());
      if (e > 0) {
         var t = $(".percent", this).html();
         var n = Math.ceil(e / Math.round($(".percent", this).width() / t.length));
         $(".percent", this).html(
            "<span style='color: white'>"
             + t.substr(0, n)
             + "</span>" + t.substr(n));
      }
   });
   $("#campaignTab li").click(function() {
      $(this).parent().children().removeClass("on");
      $(this).addClass("on");
   });
   $("#shares li").hover(function() {
      $(this).css("background-position", "bottom");
   }, function() {
      $(this).css("background-position", "top");
   });
   $(".btn_facebook").click(function() {
      postToFeed();
   });
   $(".btn_twitter").click(function() {
      postToTweet();
   });
   $("#shares .btn_comment").click(function() {
      $("html, body").animate({
         scrollTop : $("#comment").offset().top
      }, 500);
   });
   $(".btn_submitcmt").click(function() {
      var en = $("input[name=is_en]") != null && $("input[name=is_en]").val() == '1' ? 1 : 0;
      if (!$.trim($("textarea[name=cmt]").val())) {
         var tmp_msg = en == 1 ? "Please input comment." : "코멘트 내용을 입력해주세요.";
         qAlert(tmp_msg);
         return false;
      }
      var e = $("input[name=viewport]").val();
      var t = e == "result" ? $("input[name=category]").val() : checkRadioValue(document.commentForm.category);
      $.ajax({
         type : "POST",
         url : "/campaign/ajax.comment.php",
         data : {
            category : t,
            it_id : $("input[name=it_id]").val(),
            cmt : $("textarea[name=cmt]").val(),
            is_en : en,
            viewport : e
         },
         cache : false,
         success : function(n) {
            if (e == "result") {
               $("#smiley" + t).addClass("dropped");
               var com_msg1 = en == 1 ? "Comment Complete!" : "응원 완료!";
               var com_msg2 = en == 1 ? "The Comment has been registered. Thank you!" : "응원 코멘트가 등록되었습니다. 감사합니다!";
               $("#subtext" + t).css("color","#FF5B5B").html(com_msg1);
               alert(com_msg2);
            } else {
               $("#commentList").prepend(n);
               $("#commentList .commentCell:first").hide().fadeIn("slow");
            }
            $("textarea[name=cmt]").val("");
         }
      });
   });
   $(".btn_submitreply").click(function() {
      var e = $(this);
      if (!$.trim($("textarea[name=r_cmt]").val())) {
         qAlert("답글 내용을 입력해주세요.");
         return false
      }
      $.ajax({
         type : "POST",
         url : "/campaign/ajax.comment.php",
         data : {
            it_id : $("input[name=it_id]").val(),
            cmt_id : $("input[name=cmt_id]").val(),
            cmt : $("textarea[name=r_cmt]").val()
         },
         cache : false,
         success : function(t) {
            $(e).parent().closest(".commentCell").after(t);
            $(".fresh").hide().fadeIn("slow").removeClass("fresh");
            $("textarea[name=r_cmt]").val("");
            $("#replybox").hide();
         }
      });
   });
   $(".modify").click(function() {
      document.location.href = "/my/modify";
   });
   $("[title]").qtip();
   $(".numonly").keydown(function(e) {
      if (e.keyCode == 46 || e.keyCode == 8
            || e.keyCode == 9
            || e.keyCode == 27
            || e.keyCode == 13
            || e.keyCode == 37
            || e.keyCode == 39
            || e.keyCode == 65
            && e.ctrlKey === true) {
         return
      } else if (e.shiftKey
            || (e.keyCode < 48 || e.keyCode > 57)
            && (e.keyCode < 96 || e.keyCode > 105)) {
         e.preventDefault();
      }
   });
   window.qModal = function(e, t) {
      $(document.body).qtip({
         id : "modal",
         content : {
            text : t,
            title : {
               text : e,
               button : true
            }
         },
         position : {
            my : "center",
            at : "center",
            target : $(window)
         },
         show : {
            event : "click",
            solo : true,
            modal : true
         },
         hide : false
      });
   };
   window.qAlert = function(e) {
      $(document.body).qtip({
         content : {
            text : e,
            title : {
               text : "<img src='/images/common/icon_alert.png' style='vertical-align: middle' />오류",
               button : false
            }
         },
         position : {
            my : "center",
            at : "center",
            target : $(window)
         },
         show : {
            event : false,
            ready : true,
            effect : function() {
               $(this).stop(0, 1).fadeIn(250);
            },
            delay : 0,
            persistent : false,
            solo : true
         },
         hide : {
            event : false,
            effect : function(e) {
               $(this).stop(0, 1).fadeOut(250).queue(function() {
                  e.destroy();
               });
            }
         },
         style : {
            classes : "jalert qtip-calert",
            tip : false
         },
         events : {
            render : function(e, t) {
               n.call(t.elements.tooltip,e);
            }
         }
      }).removeData("qtip");
      $(document).delegate(".qtip.jalert","mouseover mouseout", n);
   };
   window.qGrowl = function(e, t, r) {
      t = typeof t !== "undefined" ? t : false;
      r = typeof r !== "undefined" ? r : false;
      var i = $(".qtip.jgrowl:visible:last");
      $(document.body).qtip({
         content : {
            text : e,
            title : {
               text : t,
               button : t ? true : false
            }
         },
         position : {
            my : "bottom right",
            at : (i.length ? "top" : "bottom") + " right",
            target : i.length ? i : $(window),
            adjust : {
               y : -10,
               x : i.length ? 0 : -10
            },
            effect : function(e, t) {
               $(this).animate(t, {
                  duration : 200,
                  queue : false
               });
               e.cache.finalPos = t
            }
         },
         show : {
            event : false,
            ready : true,
            effect : function() {
               $(this).stop(0, 1).fadeIn(400)
            },
            delay : 0,
            persistent : r
         },
         hide : {
            event : false,
            effect : function(e) {
               $(this).stop(0, 1).fadeOut(400).queue(function() {
                  e.destroy();
                  updateGrowls();
               });
            }
         },
         style : {
            width : 300,
            classes : "jgrowl qtip-bootstrap",
            tip : false
         },
         events : {
            render : function(e, t) {
               n.call(t.elements.tooltip, e);
               $("button.ok", t.elements.content).click(t.hide);
               $("button.btn_sharebadge",t.elements.content).click(t.hide);
            },
            hide : function(e, t) {
               var n = $(".grantBadge", t.elements.content).attr("category");
               var r = $(".grantBadge", t.elements.content).attr("param1");
               var i = $(".grantBadge", t.elements.content).attr("param2");
               if (n) {
                  $.ajax({
                     type : "POST",
                     url : "/ajax.badge.php",
                     data : {
                        category : n,
                        param1 : r,
                        param2 : i
                     },
                     cache : false,
                     success : function(e) {
                     }
                  });
               }
            }
         }
      }).removeData("qtip");
   };
   window.updateGrowls = function() {
      var e = $(".qtip.jgrowl"), 
          t = e.outerWidth(), 
          n = e.outerHeight(), 
          r = e.eq(0).qtip("option","position.adjust.y"), 
          i;
      e.each(function(e) {
         var s = $(this).data("qtip");
         s.options.position.target = !e ? $(window) : [i.left + t,  i.top - n * (e - 1) - Math.abs(r * (e - 1)) ];
         s.options.position.adjust.x = !e ? -10 : 0;
         s.set("position.at", "bottom right");
         if (!e) {
            i = s.cache.finalPos;
         }
      });
   }
});
$(document).on("click",	".btn_sharebadge",function() {
   var e = $(".grantBadge").attr("category");
   var t = [ "뱃지 이름", "개척자", "종결자", "메신저", "히어로", "럭키가이/럭키걸",
         "홈런", "100% 달성", "VIP", "PERFECT" ];
   var n = [ "", "각 캠페인에 처음으로 기부하신 회원님께 드리는 뱃지입니다.",
         "각 캠페인에 마지막으로 기부하신 회원님께 드리는 뱃지입니다.",
         "각 캠페인을 SNS로 공유해주신 회원님께 드리는 뱃지입니다.",
         "캠페인에 최고액을 후원해주신 회원님께 드리는 뱃지입니다.",
         "유명인사 이벤트에 당첨되신 회원님께 드리는 뱃지입니다.",
         "목표액이 달성된 캠페인에 추가로 기부하신 회원님께 드리는 뱃지입니다.",
         "후원하신 캠페인이 목표 모금액을 달성 시 드리는 뱃지입니다.",
         "총 100만원 이상 후원하신 회원님께 드리는 뱃지입니다.",
         "후원 내역을 공유하고 응원코멘트를 모두 작성하신 회원님께 드리는 뱃지입니다." ];
   var r = "http://" + serverName + "/images/common/badges/" + e + "_on.png";
   var i = {
      method : "feed",
      link : "http://wegen.kr/",
      picture : r,
      name : $("#userName").html() + "님이 '" + t[e] + "' 뱃지를 획득하셨습니다.",
      caption : "스타와 함께 하는 즐거운 기부, 위젠",
      description : "'" + t[e] + "' 뱃지는 " + n[e]
   };
   FB.ui(i);
});

$(document).on("click", ".btn_reply", function() {
	$("#replybox").show().appendTo($(this).parent().closest(".commentCell"));
	$("input[name=cmt_id]").val($(this).attr("cmt"));
});

$(document).on("click", ".btn_delete", function() {
	var e = $(this);
	if (confirm("이 코멘트를 삭제하시겠습니까?")) {
		$.ajax({
			type : "POST",
			url : "/campaign/ajax.comment.php",
			data : {
				mode : "delete",
				it_id : $(this).attr("item"),
				cmt_id : $(this).attr("cmt")
			},
			cache : false,
			success : function(t) {
				if (t == "0000") {
					$(e).parent().closest("div").fadeOut("slow", function() {
						$(this).remove();
					})
				}
			}
		});
	}
});

function settingLayer(obj) {
   $(obj).overlay({
      top:'center',
      left:'center',
      mask:{
         color:'#000',
         opacity:0.7
      },
      speed:200
   });
}
function createLayer(str) {
	$(str).show().addClass('centering').css('margin-top',
			'-' + $(str).height() / 2 + 'px').css('margin-left',
			'-' + $(str).width() / 2 + 'px');
	$('body').append($(document.createElement('div')).addClass('layer').append(str));
   $(".layer").hide().fadeIn("fast");
   $(".layer").click(function() {
      $("#movieFrame").hide();
      destroyLayer($(str));
      if($(str).data('refresh')) {
         location.reload();
      }
   });
   $(str).click(function(e) {
      e.stopPropagation();
   });
}

function destroyLayer(str) {
	$(str).hide().prependTo($('body'));
   $('.layer').fadeOut("fast",function() {
      $(this).remove();
   });
}

function procShare(it_id, via, param) {
   var is_en = $('input[name=is_en]') != null && $('input[name=is_en]').val() == '1' ? 1 : 0;
	$.ajax({
      type : 'POST',
      asyn : true,
      url : '/campaign/ajax.share.php',
      data : {
         'it_id' : it_id,
         'via' : via,
         'post_id' : param,
         'is_en' : is_en
      },
      dataType : 'text',
      success : function(result) {
         if(is_en == 0 ) {
            result = result.split(':');
            if (result[0] != '0007' && result[0] != '0009') {
               qGrowl('성공적으로 공유글을 남겼습니다. 감사합니다!', '공유 성공', true);
            }
            switch (result[0]) {
            case '0000':
               qGrowl("<div class='grantBadge' category='3' param1='"
                  + result[1]
                  + "' param2='"
                  + result[2]
                  + "'><img src='/images/common/badges/3_on.png'/></div><p>축하합니다!</p><p>위제너레이션 캠페인을 공유해주셔서<br/>&quot;<strong>메신저</strong>&quot; 뱃지를 획득하셨습니다</p><p style='text-align: center'><button class='btn_sharebadge' badgecat='"
                  + cat
                  + "'>공유하기</button><button class='ok'>확인</button></p>"
               ,'뱃지 획득 알림', true);
            }
            $('#share_' + via).html(parseInt($('#share_' + via).html()) + 1);
         }
      }
   });
}

// functions for request form validation
function checkRequestForm() {
	if (!$.trim($('input[name=req_name]').val())) {
		qAlert('단체명을 입력해 주세요.');
		$('input[name=req_name]').focus();
		return false;
	}

	if (!$.trim($('textarea[name=req_about]').val())) {
		qAlert('단체 소개를 입력해 주세요.');
		$('textarea[name=req_about]').focus();
		return false;
	}

	if (!$.trim($('input[name=req_charge]').val())) {
		qAlert('담당자명을 입력해 주세요.');
		$('input[name=req_charge]').focus();
		return false;
	}

	if (!$.trim($('input[name=req_hp1]').val())
			|| !$.trim($('input[name=req_hp2]').val())
			|| !$('input[name=req_hp3]').val()) {
		qAlert('담당자 연락처를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=req_mail]').val())) {
		qAlert('담당자 이메일 주소를 입력해 주세요.');
		$('input[name=req_mail]').focus();
		return false;
	}

	if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($(
			'input[name=req_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=req_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('textarea[name=req_desc]').val())) {
		qAlert('캠페인 내용 설명을 입력해 주세요.');
		$('textarea[name=req_desc]').focus();
		return false;
	}

	if (!$.trim($('input[name=req_target]').val())) {
		qAlert('목표로 하는 모금액을 입력해 주세요.');
		$('input[name=req_target]').focus();
		return false;
	}

	if (!$.trim($('textarea[name=req_reward]').val())) {
		qAlert('제공 가능한 리워드를 입력해 주세요.');
		$('textarea[name=req_reward]').focus();
		return false;
	}

	return true;
}

function checkTalentForm() {

	if (!$.trim($('select[name=tal_category]').val())) {
		qAlert('신청분야를 선택해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=tal_name]').val())) {
		qAlert('신청자명을 입력해 주세요.');
		$('input[name=tal_name]').focus();
		return false;
	}

	if (!$.trim($('textarea[name=tal_about]').val())) {
		qAlert('자기 소개를 입력해 주세요.');
		$('textarea[name=tal_about]').focus();
		return false;
	}

	if (!$.trim($('input[name=tal_hp1]').val())
			|| !$.trim($('input[name=tal_hp2]').val())
			|| !$('input[name=tal_hp3]').val()) {
		qAlert('신청자 연락처를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=tal_mail]').val())) {
		qAlert('신청자 이메일 주소를 입력해 주세요.');
		$('input[name=tal_mail]').focus();
		return false;
	}

	if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($(
			'input[name=tal_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=tal_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('textarea[name=tal_desc]').val())) {
		qAlert('희망하는 재능기부의 내용을 입력해 주세요.');
		$('textarea[name=tal_desc]').focus();
		return false;
	}

	return true;
}

function checkInquiryForm() {

	if (!$.trim($('input[name=inq_name]').val())) {
		qAlert('문의자명을 입력해 주세요.');
		$('input[name=inq_name]').focus();
		return false;
	}

	if (!$.trim($('input[name=inq_subject]').val())) {
		qAlert('제목을 입력해 주세요.');
		$('input[name=inq_subject]').focus();
		return false;
	}

	if (!$.trim($('input[name=inq_mail]').val())) {
		qAlert('이메일 주소를 입력해 주세요.');
		$('input[name=inq_mail]').focus();
		return false;
	}

	if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($(
			'input[name=inq_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=inq_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('textarea[name=inq_desc]').val())) {
		qAlert('문의 내용을 입력해 주세요.');
		$('textarea[name=inq_desc]').focus();
		return false;
	}

	return true;
}

function checkRadioValue(radioObj) {
	var l = radioObj.length;
	if (l == undefined) {
		if (l.checked) {
			return radioObj.value;
		} else
			return;
	}
	for ( var i = 0; i < l; i++) {
		if (radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
}

function checkLoginForm() {
	if (!$.trim($('input[name=uid]').val())
			|| $('input[name=uid]').val() == '이메일 주소'
			|| !/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($(
					'input[name=uid]').val()))) {
		$('input[name=uid]').animate({
			borderColor : '#E30000'
		}, 500).delay(500).animate({
			borderColor : '#DDDDDD'
		}, 500);
		qAlert('이메일 입력을 확인해주세요.');
		return false;
	}

	if (!$.trim($('input[name=p]').val())) {
		$('input[name=p], input[name=password_pseudo]').animate({
			borderColor : '#E30000'
		}, 500).delay(500).animate({
			borderColor : '#DDDDDD'
		}, 500);
		qAlert('비밀번호 입력을 확인해주세요.');
		return false;
	}
	/*
	$('#loginForm').append(
			$(document.createElement("input")).attr('name', 'p').attr('type',
					'hidden')
			// .val(hex_sha512($('input[name=password]').val()))
			.val($('input[name=password]').val()));
	$('input[name=password]').val('');

	document.loginForm.submit();
	*/
	return true;
}

function addLabel(target, str, ispassword) {
	ispassword = (typeof ispassword !== 'undefined') ? ispassword : false;
	// if (ispassword) $(target).attr('type', 'text');

	if (!$(target).val())
		$(target).val(str).addClass('label');

	$(target).focusin(function() {
      if (!$(target).val() || $(target).val() == str) {
         if (ispassword) {
            /*
            $(document.createElement('input')).attr('type',
                  'password').attr('name', ispassword).appendTo(
                  $(target).parent().get(0)).addClass('text')
                  .keydown(function(e) {
                     console.log('enter pressed');
                     if (e.which == '13')
                        checkLoginForm();
                  }).focus(); 
            */
            $(target).hide();
            $('#loginLayer input[name=p]').show();
            $('#loginLayer input[name=p]').focus();
         } else {
            $(target).val('').removeClass('label');				
         }
      }
   }).focusout(function() {
      if (!ispassword && !$(target).val()) {
         // if (ispassword) $(target).attr('type', 'text');
         $(target).val(str).addClass('label');
      }
   });
}

function procBadge(cat, title) {
	qGrowl("<div class='grantBadge' category='"
         + cat
         + "'><img src='/images/common/badges/"
         + cat
         + "_on.png'/></div><p>&quot;<strong>"
         + title
         + "</strong>&quot; 뱃지를 획득하셨습니다</p><p style='text-align: center'><button class='btn_sharebadge' badgecat='"
         + cat + "'>공유하기</button><button class='ok'>확인</button></p>",
   '뱃지 알림', true);
}

function moveFocus(maxLen, from, to) {
	var len = from.value.length;
	if (len == maxLen) {
		to.focus();
	}
}

function setCampaignMenuSwipe() {
	var menu_swipe = Swipe($('.campaign_detail .content .swipe')[0], {
		continuous : false,
		callback : function(index, element) {
			$('.campaign_detail .content .menu ul li.selected').removeClass('selected');
			switch (index) {
            case 0:
               $('.campaign_detail .content .swipe-wrap').css('height',	$('.campaign_detail .content .menu_swipe_0').height()+'px');
               $('.campaign_detail .content .menu .campaign_menu_0').addClass('selected');
               $('.campaign_detail .content .menu input[name=current_menu]').val('0');
               break;
            case 1:
               $('.campaign_detail .content .swipe-wrap').css('height', $('.campaign_detail .content .menu_swipe_1').height()+'px');
               $('.campaign_detail .content .menu .campaign_menu_1').addClass('selected');
               $('.campaign_detail .content input[name=current_menu]').val('1');
               break;
            case 2:
               $('.campaign_detail .content .swipe-wrap').css('height', $('.campaign_detail .content .menu_swipe_2').height()+'px');
               $('.campaign_detail .content .menu .campaign_menu_2').addClass('selected');
               $('.campaign_detail .content input[name=current_menu]').val('2');
               break;
			}
		}
	});
	$('.campaign_detail .content .menu .campaign_menu_0').click(function() {
		switch ($('.campaign_detail .content input[name=current_menu]').val()) {
         case '1':
            menu_swipe.prev();
            break;
         case '2':
            menu_swipe.prev();
            menu_swipe.prev();
            break;
		}
	});
	$('.campaign_detail .content .menu .campaign_menu_1').click(function() {
		switch ($('.campaign_detail .content input[name=current_menu]').val()) {
         case '0':
            menu_swipe.next();
            break;
         case '2':
            menu_swipe.prev();
            break;
		}
	});
	$('.campaign_detail .content .menu .campaign_menu_2').click(function() {
		switch ($('.campaign_detail .content input[name=current_menu]').val()) {
         case '0':
            menu_swipe.next();
            menu_swipe.next();
            break;
         case '1':
            menu_swipe.next();
            break;
		}
	});
}

function processLogin(url) {
	$.ajax({
		type : "POST",
		url : "/processLogin.php",
		data : {
			uid : $("input[name=uid]").val(),
			p : hex_sha512($('input[name=p]').val())
		},
		cache : false,
		success : function(t) {
			if(t=='failed') {
				qAlert('이메일 또는 비밀번호가 잘못되었습니다.');
			} else {
				if(url) {
					location.href=decodeURIComponent(url);
				} else {
					location.href='/';
				}
			}
		}
	});
}
function js_send_email(post_data) {
   $.ajax({
      url:'/module/sendmail.php',
      type:'POST',
      data:post_data,
      cache:false,
      success: function(t) {
         console.log(t);
      }
   });
}

function js_send_sms(post_data) {
   $.ajax({
      url:'/module/sendsms.php',
      type:'POST',
      data:post_data,
      cache:false,
      success: function(t) {
         console.log(t);
      }
   });
}

function ajaxRequest(type, post_data, idx) {
   var req_url = '';
   switch(type) {
      case 'request_bid':
         req_url = '/ajax/request.bid.php';
         break;
   } 
   $.ajax({
      url: req_url,
      type:'POST',
      data: post_data,
      cache:false,
      success:function(res) {
         res = JSON.parse(res);
         alert(res.bid_rank);
         switch(type) {
            case 'request_bid':
               if(res.show_status =='success') {
                  var my_rank = res.bid_rank*1;
                  var my_time = res.bid_time;
                  var my_name = post_data.mb_name.substr(0,1)+"***";

                  if($('.list tr[data-chkme=1]').length > 0) {
                     var past_rank = $('.list tr[data-chkme=1]').data('num')*1;
                     var flag = false;
                     $('.list tr[data-num]').each(function() {
                        if($(this).data('num')*1 == past_rank) {
                           $(this).remove();
                        }
                        if($(this).data('num')*1 > past_rank ) {
                           var upd = $(this).data('num')*1 - 1;
                           $(this).attr('data-num',upd);
                           $(this).data('num',''+upd);
                           $('.list tr[data-num='+upd+'] td:first').html(upd);
                        }
                     });
                  }

                  if($('.list tr[data-num=0]').length > 0 || my_rank == 1) {
                     $('.list tr[data-num='+(my_rank-1)+']').remove();
                     $('.list tr[data-num]').each(function() {
                        var upd = $(this).data('num')*1 + 1;
                        $(this).attr('data-num',''+upd);
                        $(this).data('num',''+upd);
                        $('.list tr[data-num='+upd+'] td:first').html(upd);
                     });
                     $('.list tr[class=top_row'+idx+']').after("<tr data-num='"+my_rank+"' data-chkme='1'>"+
                        "<td>"+my_rank+"</td>"+
                        "<td>"+my_name+"</td>"+
                        "<td>"+number_format(post_data.bid_amount)+"</td>"+
                        "<td>"+my_time+"</td>"+
                        "</tr>");
                     if(my_rank == 1) {
                        $('._2_right_ctt'+idx).html(number_format(post_data.bid_amount));
                        
                     } 
                  } else {
                     $('.list tr[data-num='+(my_rank-1)+']').after("<tr data-num='"+my_rank+"' data-chkme='1'>"+
                        "<td>"+my_rank+"</td>"+
                        "<td>"+my_name+"</td>"+
                        "<td>"+number_format(post_data.bid_amount)+"</td>"+
                        "<td>"+my_time+"</td>"+
                        "</tr>");
                     $('.list tr[data-num]').each(function() {
                        if($(this).data('num')*1 >= my_rank) {
                           if($(this).data('chkme') == '1') return;
                           var upd = $(this).data('num')*1 + 1;
                           $(this).attr('data-num',''+upd);
                           $(this).data('num',''+upd);
                           $('.list tr[data-num='+upd+'] td:first').html(upd);
                        }
                     });
                  }
                  var now = $('.content_bid').scrollTop();
                  var st = $('.list tr[data-chkme=1]').offset().top - $('.content_bid').offset().top + now - 30;

                  $('.content_bid').animate({
                     scrollTop: st 
                  },500);
                  $('.list tr[data-chkme=1]').focus();
                  $('.list tr[data-chkme=1]').hide().fadeIn('slow');
                  $('input[name=bid_amount]').val('');

                  if(res.bid_rank == '1') {
                     js_send_sms({sms_type:'bid_new_rank'});
                  }
               } else {
                  if(res.show_msg) {
                     switch(res.show_msg) {
                        case 'err_1':
                           alert(res.msg);
                           if($('input[name=ismobile]').val() == '1') {
                              alert('경매가 종료되었습니다.');
                           } else {
                              qAlert('경매가 종료되었습니다.');
                           }
                           break;
                        case 'err_2':
                           if($('input[name=ismobile]').val() == '1') {
                              alert('이 캠페인의 최소입찰금액은 '+number_format(res.var_1)+'원 입니다.');
                           } else {
                              qAlert('이 캠페인의 최소입찰금액은 '+number_format(res.var_1)+'원 입니다.');
                           }
                           $('input[name=bid_amount]').focus();

                           break;
                        case 'err_3':
                           if($('input[name=ismobile]').val() == '1') {
                              alert('천원 단위의 금액만 입찰이 가능합니다.');
                           } else {
                              qAlert('천원 단위의 금액만 입찰이 가능합니다.');
                           }
                           $('input[name=bid_amount]').focus();
                           
                           break;
                        case 'err_4':
                           if($('input[name=ismobile]').val() == '1') {
                              alert('입찰했던 금액보다 낮게 입찰할 수 없습니다.');
                           } else {
                              qAlert('입찰했던 금액보다 낮게 입찰할 수 없습니다.');
                           }
                           $('input[name=bid_amount]').focus();
                           break;
                        case 'err_5':
                           if($('input[name=ismobile]').val() == '1') {
                              alert('최고 입찰금액 '+res.show_val1+'원 보다 많아야합니다.');
                           } else {
                              qAlert('최고 입찰금액 '+res.show_val1+'원 보다 많아야합니다.');
                           }
                           $('input[name=bid_amount]').focus();
                           break;

                     }
                  } else {
                     if($('input[name=ismobile]').val() == '1') {
                        alert('오류가 발생했습니다. 다시 시도해주세요.');
                     } else {
                        qAlert('오류가 발생했습니다. 다시 시도해주세요.');
                     }
                  }
               }
               break;
         }
      }
   });

}

function phone_check(phone) {
   var regExp = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$/;
   if(!phone) {
      return false;
   } else if (!regExp.test(phone)) {
      return false;
   }
   return true;
}
function email_check(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function number_format(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

