var scriptLoader = {
	require: function(src) {
      document.write("<script type='text/javascript' src='"+src+"'></script>");
	},
	load: function() {
		var scriptTags = document.getElementsByTagName("script");
		for (var i = 0; i < scriptTags.length; i++) {
			if (scriptTags[i].src && scriptTags[i].src.match(/scripts\.js$/)) {
				var path = scriptTags[i].src.replace(/scripts\.js$/,'');
            this.require(path + 'jquery.min.js');
            this.require(path + 'jquery.plugin.min.js');
				this.require(path + 'jquery.qtip.min.js');
            this.require(path + 'jquery.animate-colors-min.js');
				this.require(path + 'jquery.slides.min.js');
				this.require(path + 'swipe.js');
            this.require(path + 'jquery.jcarousel.min.js');
				this.require(path + 'global.js');
            if(js_file) this.require(js_file);
				this.require('http://connect.facebook.net/en_US/all.js');
				this.require('https://platform.twitter.com/widgets.js');
				break;
			}
		}
      
	}
}
scriptLoader.load();
