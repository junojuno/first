$(document).ready(function() {
   $('input[type=text]').addClass('text');
   $('form[name=PGIOForm] .submit').click(function() {
      var p = checkRadioValue(document.PGIOForm.od_price);
      var custom = $('input[name=connect_type]').val() + '|' + $('input[name=ordr_idxx]').val();
      if (!p) {
         qAlert('Please Select amount to donate.');
         return false;
      }
      if (p == 'user_input') {
         p = $.trim($('input[name=user_input]').val());
         if (!p || parseInt(p) == 0) {
            qAlert('Please Input amount to donate.');
            return false;
         }
         /*
         else if (p*1 < $('#donationPrice #p0').val()*1 ) {
            qAlert('Amount should be equal to or over '+$('label[for=p0]').html()+' dollars');
            return false;
         }
         */
      }
      $('input[name=unitprice]').val(p);

      if($('input[name=reward_req]') != null && $('input[name=reward_req]').val() != '') {
         custom += '|'+ $('input[name=reward_req]').val();
      }
      $('input[name=custom]').val(custom);

      if($('select[name=paymethod] option:selected').val() == '') {
         qAlert('Select Paymethod');
         $('select[name=paymethod]').focus();
         return false;
      }

      if (!$.trim($('input[name=mb_name]').val())) {
         qAlert('Please Input your name.');
         $('input[name=mb_name]').focus();
         return false;
      }
      $('input[name=receipttoname]').val($('input[name=mb_name]').val());

      if (!$.trim($('input[name=night_phone_b]').val())) {
         qAlert('Please Input your telephone number.');
         $('input[name=night_phone_b]').focus();
         return false;
      }

      if (!$.trim($('input[name=email]').val())) {
         qAlert('Please Input your email.');
         $('input[name=email]').focus();
         return false;
      }

      if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
         qAlert('Wrong your email format');
         $('input[name=email]').val('').focus();
         return false;
      }
      $('input[name=receipttoemail]').val($('input[name=email]').val());
      
      if(!$.trim($('input[name=address1]').val())) {
         qAlert('Please Input your address.');
         $('input[name=address1]').val('').focus();
         return false;
      }

      $.ajax({
         type : "POST",
         url : "/en/campaign/ajax.update.php",
         data : {
            name : $('input[name=mb_name]').val(),
            email : $('input[name=email]').val(),
            contact : $('input[name=night_phone_b]').val(),
            address1 : $('input[name=address1]').val(),
            address2 : $('input[name=address2]').val(),
            zip : $('input[name=zip]').val()
         },
         cache : false,
         success : function(n) {
            if(n=='success') {
               doTransaction(document.PGIOForm);
            } else {
               alert('Please Retry again.');
            }
         }
      });
      return false;
   });
});

// paygate
function getPGIOresult() {
   var replycode = document.PGIOForm.elements['replycode'].value;
   var replyMsg = document.PGIOForm.elements['replyMsg'].value;
   if(replycode == '0000') {
      $('form[name=PGIOForm]').attr('action', '/en/campaign/result');
      $('form[name=PGIOForm]').submit();
   } else {
      alert(replycode+' '+replyMsg);
   }
}
