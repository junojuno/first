var share_btn_click = false;
var from_fb_link_oc = from_fb_link_oc ? from_fb_link_oc : false;

$(document).ready(function() {
   $('.open_share_btn').click(function() {
      if(mb_no) {
         var pd = {logined_before_click:'1', it_id:it_id};
         chkOpenCampaign({logined_before_click:'1', it_id:it_id});
      } else {
         createLayer($('.open_campaign_layout'));
      }
   });
   $('.open_campaign_layout .close_btn').click(function() {
      destroyLayer($('.open_campaign_layout'));
   });
   if(from_fb_link_oc) {
      createLayer($('.open_campaign_layout'));
   }
   $('[type=text]').addClass('text');
   $('.open_campaign_layout .share_fb_btn').click(function() {
      var post_data = {};
      if(!$('input[name=open_name]').val()) {
         qAlert('이름을 입력해주세요');
         $('input[name=open_name]').focus();
         return false;
      }
      post_data.mb_name = $('input[name=open_name]').val();

      if(!$('input[name=open_hp1]').val()) {
         qAlert('전화번호를 입력해주세요');
         $('input[name=open_hp1]').focus();
         return false;
      }
      if(!$('input[name=open_hp2]').val()) {
         qAlert('전화번호를 입력해주세요');
         $('input[name=open_hp2]').focus();
         return false;
      }
      if(!$('input[name=open_hp3]').val()) {
         qAlert('전화번호를 입력해주세요');
         $('input[name=open_hp3]').focus();
         return false;
      }
      var mb_contact = $('input[name=open_hp1]').val()+'-'+$('input[name=open_hp2]').val()+'-'+$('input[name=open_hp3]').val();
      if(!phone_check(mb_contact)) {
         qAlert('전화번호 형식이 잘못되었습니다.');
         $('input[name=open_hp1]').focus();
         return false;
      }
      post_data.mb_contact = mb_contact;

      if(!$('input[name=open_email]').val()) {
         qAlert('이메일을 입력해주세요.');
         $('input[name=open_email]').focus();
         return false;
      }
      if(!email_check($('input[name=open_email]').val())) {
         qAlert('이메일 형식을 확인해주세요.');
         $('input[name=open_email]').focus();
         return false;
      }
      post_data.mb_email = $('input[name=open_email]').val();

      if(!$('textarea[name=open_msg]').val()) {
         qAlert('응원 메세지를 입력해주세요.');
         $('textarea[name=open_msg]').focus();
         return false;
      }
      post_data.open_msg = $('textarea[name=open_msg]').val();
      post_data.it_id = $('input[name=open_it_id]').val();
      if(!share_btn_click) {
         share_btn_click = true;
         chkOpenCampaign(post_data);
      }
   });
   if($('.detail_right_top').height() < $('.campaign_summary').height() + $('.recent_donor').height()) {
      function recent_donor_animation() {
         var up_name = $('.recent_donor .list li:last').text();
         $('.recent_donor .list li:last').remove();
         $('.recent_donor .list').prepend('<li style="height:0px;">'+up_name+'</li>');
         $('.recent_donor .list li:first').animate({height:'19px'}, 600);
      }
      setInterval(recent_donor_animation, 1900);
   }
   
   $('.open_campaign_finish .close_btn').click(function() {
      destroyLayer($('.open_campaign_finish'));
   });
   $('.open_campaign_finish .donate_btn img').click(function() {
      location.href = '/campaign/'+it_id+'/donate';
   });
});

function chkOpenCampaign(pd) {
   $.ajax({
      type : 'POST',
      url : '/ajax/chk_open_campaign.php',
      data : pd, 
      success : function(res) {
         res = JSON.parse(res);
         switch(res.show_status) {
            case 'failed' :
               alert('오류가 발생하였습니다.');
               share_btn_click = false;
               break;
            case 'err_1' :
               alert('이미 같은 이메일 또는 연락처로 무료 응모를 하였습니다.');
               share_btn_click = false;
               break;
            case 'before_err_1':
            case 'fb_err_1':
               alert('이미 무료 응모를 하셨습니다.');
               share_btn_click = false;
               location.reload();
               break;
            case 'before_success':
               createLayer($('.open_campaign_layout'));
               break;
            case 'success':
               if(pd.fb_account == '1') {
                  function callback(response) {
                     if(response) {
                        requestOpenShared(pd);
                     }
                  }
                  share_btn_click = false;
                  FB.ui(pd.share.obj, callback);
               } else {
                  openCampaignShare(pd);
               }
               break;
         }
      },
      error : function() {
         qAlert('예기치못한 오류가 발생하였습니다. 계속 오류가 발생할 시 1:1 문의로 연락 부탁드립니다.');
         share_btn_click = false;
      }
   });
}

function requestOpenShared(pd) {
   $.ajax({
      type: 'POST',
      url : '/ajax/open_campaign_shared.php',
      data : pd,
      success : function(res) {
         console.log('shared res : '+ res);
         res = JSON.parse(res);
         switch(res.show_status) {
            case 'success' :
               destroyLayer($('.open_campaign_layout'));
               createLayer($('.open_campaign_finish'));
               $.ajax({
                  type : "POST",
                  url : "/campaign/ajax.comment.php",
                  data : {
                     category : 1,
                     it_id : pd.it_id,
                     cmt : pd.open_msg,
                     is_en : 1,
                     viewport : '무료응모'
                  },
                  cache : false,
                  success : function(n) {
                     console.log('comment : '+ n);
                     $("#commentList").prepend(n);
                     $("#commentList .commentCell:first").hide().fadeIn("slow");
                     $("textarea[name=open_msg]").val("");
                  }
               });
               break;
            case 'failed' :
            default:
               alert('오류가 발생했습니다.');
               share_btn_click = false;
               break;

         }
      },
      error : function() {
         qAlert('예기치못한 오류가 발생하였습니다. 계속 오류가 발생할 시 1:1 문의로 연락 부탁드립니다.');
         share_btn_click = false;
      }
   })
}
