<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no];
$mode = $_REQUEST[mode];
$title = $mode ==='edit' ? '수정' : '등록';

if ($mode ==='insert') {
   $data = sql_fetch("SELECT * FROM ".DB_FUNDRAISERS." WHERE no = $no");
} else {
   $data = sql_fetch("SELECT * FROM ".DB_FUNDRAISERS_EN." WHERE no = $no");
}
?>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>펀드레이저(영문) <?=$title?></h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='fundraiser_en'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<input type='hidden' name='old_it_id' value='<?=$data[it_id]?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
   <th style='width: 120px'>펀드레이저명</th>
   <td style='padding-left: 10px'>
   <input type='text' name='fr_name' value='<?=$data[fr_name]?>' style='width: 95%' />
   </td>
</tr>
<tr>
   <th style='width: 120px'>홈페이지</th>
   <td style='padding-left: 10px'>
   <input type='text' name='fr_homepage' value='<?=$data[fr_homepage]?>' style='width: 95%' />
   </td>
</tr>
<? $noimg = "<span style='color: red; margin-right: 10px'>이미지 없음</span>"; ?>
<tr>
   <th style='width: 120px'>사진</th>
   <td style='padding-left: 10px'>
   <? if ($mode == 'edit') print (file_exists('../data/en/partners/'.$data[fr_logo])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/en/partners/$data[fr_logo]' target='_blank' title=\"<img src='/data/en/partners/$data[fr_logo]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
   <input type='file' name='fr_logo' style='width: 300px' />
   </td>
</tr>

<tr>
   <th style='width:120px'>선택 가능한 캠페인</th>
   <td style='padding-left:10px'>
      <select name="it_id">
         <option value=''>선택안함</option>
         <?
         $sql = "SELECT * FROM ".DB_CAMPAIGNS_EN." WHERE it_isEnd != 1 AND (it_fundraiser = 0 OR it_fundraiser = $no) ORDER BY it_id DESC";
         $result = sql_query($sql);
         for ($i = 0; $campaign = sql_fetch_array($result); $i++) :
            $selected = ($campaign[it_fundraiser] == $data['no']) ? ' selected' : false;
         ?>
         <option value='<?=$campaign[it_id]?>' <?=$selected?>><?=$campaign[it_name]?></option>
         <? endfor; ?>
      </select>
   </td>
</tr>

<tr>
   <th style='width:120px'>스타/멘토 이벤트</th>
   <td style='padding-left:10px'>
      <textarea name="fr_desc" class="text" style="width:95%;height:64px;" ><?=$data[fr_desc]?></textarea>
   </td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' />
</div>
</form>

</div>