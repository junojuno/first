<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no] ? $_REQUEST[no] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no != '') {
	$data = sql_fetch("SELECT * FROM ".DB_FUNDRAISERS." WHERE no = $no");
	$data2 = sql_fetch("SELECT it_name FROM ".DB_CAMPAIGNS." WHERE it_id = '$data[it_id]'");
}
?>

<script>
$(document).ready(function() {
	$('#btn-delete').click(function() {
		if (confirm('정말 삭제하시겠습니까?')) {
			$('input[name=mode]').val('delete');
			document.form.submit();
		}
	});
});
</script>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>펀드레이저 <?=$title?></h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='fundraiser'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<input type='hidden' name='old_it_id' value='<?=$data[it_id]?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>펀드레이저명</th>
	<td style='padding-left: 10px'>
	<input type='text' name='fr_name' value='<?=$data[fr_name]?>' style='width: 95%' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>홈페이지</th>
	<td style='padding-left: 10px'>
	<input type='text' name='fr_homepage' value='<?=$data[fr_homepage]?>' style='width: 95%' />
	</td>
</tr>
<? $noimg = "<span style='color: red; margin-right: 10px'>이미지 없음</span>"; ?>
<tr>
   <th style='width: 120px'>프로필 사진<span class='help' title='상세페이지'></span></th>
	<td style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/partners/'.$data[fr_logo])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/partners/$data[fr_logo]' target='_blank' title=\"<img src='/data/partners/$data[fr_logo]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='fr_logo' style='width: 300px' />
	</td>
</tr>
<tr>
   <th style='width: 120px'>메인용 사진<span class='help' title='정사각형사이즈'></span></th>
	<td style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/partners/'.$data[fr_logo_main])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/partners/$data[fr_logo_main]' target='_blank' title=\"<img src='/data/partners/$data[fr_logo_main]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='fr_logo_main' style='width: 300px' />
	</td>
</tr>
   

<tr>
   <th style='width:120px'>선택 가능한 캠페인</th>
   <td style='padding-left:10px'>
      <select name="it_id">
         <option value=''>선택안함</option>
         <?
         $added = $no ? "OR it_fundraiser = $no" : false;
         $sql = "SELECT * FROM ".DB_CAMPAIGNS." WHERE (it_fundraiser = 0 {$added}) ORDER BY it_id DESC";
         $result = sql_query($sql);
         for ($i = 0; $campaign = sql_fetch_array($result); $i++) :
            $selected = ($campaign[it_id] == $data[it_id]) ? ' selected' : false;
         ?>
         <option value='<?=$campaign[it_id]?>' <?=$selected?>><?=$campaign[it_name]?></option>
         <? endfor; ?>
      </select>
   </td>
</tr>

<tr>
	<th style='width:120px'>스타/멘토 이벤트</th>
	<td style='padding-left:10px'>
		<textarea name="fr_desc" class="text" style="width:95%;height:64px;" ><?=$data[fr_desc]?></textarea>
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' />
<? if ($mode == 'edit') : ?>
<input id='btn-delete' type='button' class='submit' value='펀드레이저 삭제' style='margin-left: 30px' />
<? endif; ?>
</div>
</form>

</div>
