<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'category';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'asc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = $_REQUEST[count] ? $_REQUEST[count] : 30;
$limit = ($page - 1) * $count;
$cate = $_REQUEST[s] ? $_REQUEST[s] - 1 : false;
$where = $_REQUEST[s] ? " WHERE category = '$cate' " : false;

$result = sql_fetch("SELECT COUNT(no) AS cnt FROM wegen_guide ".$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

$categories = array('위제너레이션', '계정설정', '후원하기', '이벤트/리워드', '정기후원', '뱃지/레벨', '캠페인신청', '기타');

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>위젠 가이드 관리</h2>

<div>
<a href='./guide.form.php'><button style='float: right' class='button'>가이드 추가</button></a>
<?
print ($cate === false) ? "<strong>전체보기</strong> |\n" : "<a href='./guide.php'>전체보기</a> |\n";
for ($i = 0; $i < count($categories); $i++) {
	$j = $i +1;
	print ($cate === $i) ? "<strong>".$categories[$i]."</strong>" : "<a href='./guide.php?s=".$j."'>".$categories[$i]."</a>";
	if ($i+1 != count($categories)) print " |\n";
}
?>
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 70px'>카테고리</th>
	<th style='width: 300px'>질문</th>
	<th>답변</th>
	<th style='width: 70px'>출력순서</th>
</tr>
<?
$sql = "SELECT * FROM wegen_guide $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$answer = (mb_strlen($data[answer], 'UTF-8') > 50) ? mb_substr($data[answer], 0, 50, 'UTF-8').'...' : $data[answer];
	$listorder = ($data[listorder] == '0') ? '미지정' : $data[listorder];
?>
<tr data-href='./guide.form.php?no=<?=$data[no]?>'>
	<td style='font-size: 10px; text-align: center'><?=$categories[$data[category]]?></td>
	<td><?=$data[question]?></td>
	<td><?=$answer?></td>
	<td style='font-size: 10px; text-align: center'><?=$listorder?></td>
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal' class='nav'>»</a> ";
?>
</span>

</div>