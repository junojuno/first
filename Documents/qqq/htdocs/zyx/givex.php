<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'od_id';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = (!$_REQUEST[count]) ? (!$_REQUEST[s]) ? 30 : 999 : $_REQUEST[count];
$limit = ($page - 1) * $count;

$where = ($_REQUEST[s]) ? " AND preferdate = '$_REQUEST[s]' " : false;

$result = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_REGULARPAYMENT." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no WHERE isFirst = 1 ".$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>정기후원 관리</h2>

<div style='height: 30px'>
<!--<a href='#'><button style='float: right' class='button'>내역 추가</button></a>-->
<a href='excel.php?section=givex'><button class='button' style='float: right; width: auto'>전체 목록을 엑셀로 저장</button></a>
<?
$categories = array('10', '25');
print (!$_REQUEST[s]) ? "<strong>전체보기</strong> |\n" : "<a href='./givex.php'>전체보기</a> |\n";
for ($i = 0; $i < count($categories); $i++) {
	print ($_REQUEST[s] === $categories[$i]) ? "<strong>".$categories[$i]."일 결제</strong>" : "<a href='./givex.php?s=".$categories[$i]."'>".$categories[$i]."일 결제</a>";
	if ($i+1 != count($categories)) print " |\n";
}
?>
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>신청일자</th>
	<th style='width: 150px'>회원명</th>
	<th style='width: 100px'>후원액수</th>
	<th style='width: 100px'>결제희망일</th>
	<th style='width: 100px'>연락처</th>
	<th style='width: 200px'>주소</th>
	<th style='width: 100px'>결제처리<br/><span style='font: 11px NanumGothic'>(<?=date('Y')?>년 <?=date('m')?>월)</span></th>
   <th style='width: 100px'>삭제</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_REGULARPAYMENT." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id WHERE isFirst = 1 and isCancel = 0 $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$zip = $data[mb_zip1] ? '('.$data[mb_zip1].'-'.$data[mb_zip2].') ' : false;

	$started = substr(array_pop(array_reverse(explode(' ', $data[od_time]))), 0, -3);
	$now = date('Y-m');
	$check = sql_fetch("SELECT od_time FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$data[mb_no]' AND od_time LIKE '%".$now."%' ORDER BY od_time DESC LIMIT 1");
	$proc = ($check[od_time]) ? "<span style='color: blue'>결제완료</span>" : "<span class='procPayment' data-date='".$data[preferdate]."' data-id='".$data[mb_no]."'>결제</span>";
   $del = "<button type='button' class='procDelete' data-id='".$data[mb_no]."'>삭제</button>";
	?>
<tr>
	<td style='font-size: 10px; text-align: center'><?=array_pop(array_reverse(explode(' ', $data[od_time])))?></td>
	<td style=''><?=$data[mb_name]?> </td>
	<td style='font-size: 11px; text-align: right; padding-right: 10px'><?=number_format($data[od_amount])?>원<br/>(<?=$data[od_method]?>)</td>
	<td style='text-align: center'><?=$data[preferdate]?>일</td>
	<td style='font-size: 10px; text-align: center'><?=$data[mb_contact]?></td>
	<td style='font-size: 11px'><?=$zip?><?=$data[mb_addr1]?> <?=$data[mb_addr2]?></td>
	<td style='text-align: center'><?=$proc?></td>
   <td style='text-align: center'><?=$del?></td>
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>»</a> ";
?>
</span>

<form name='form' id='paymentForm' method='post' action='./pp_cli_hub.php'>
   <input type='hidden' name='no' />
</form>

<form name='form' id='deleteForm' method='post' action='do.php'>
   <input type='hidden' name='section' value='givex'>
   <input type='hidden' name='mode' value='delete'>
   <input type='hidden' name='no'>
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('.procPayment').click(function() {
		var p = $(this).attr('data-date');
		var t = $.datepicker.formatDate('dd', new Date());
		if (p != t) {
			if (!confirm('이 회원의 정기결제 희망일은 매월 ' + p + '일이며, 오늘은 ' + t + '일입니다.\n그래도 결제를 진행하겠습니까?')) {
				return false;
			}
		}
		$('input[name=no]').val($(this).attr('data-id'));
		$('#paymentForm').submit();
	});
   $('.procDelete').click(function() {
      if(!confirm('이 회원의 정기후원을 취소합니다.\n정말 취소하시겠습니까?')) {
         return false;
      }
      $('input[name=no]').val($(this).attr('data-id'));
      $('#deleteForm').submit();
   });
});
</script>
