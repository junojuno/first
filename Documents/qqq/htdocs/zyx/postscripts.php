<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'wr_datetime';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = $_REQUEST[count] ? $_REQUEST[count] : 30;
$limit = ($page - 1) * $count;

$result = sql_fetch("SELECT COUNT(wr_id) AS cnt FROM ".DB_POSTSCRIPTS.$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>캠페인 후기 관리</h2>

<div style='text-align: right'>
<a href='./postscripts.form.php'><button style='float: right' class='button'>글 추가</button></a>
&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 80px'>구분</th>
	<th>제목</th>
	<th style='width: 300px'>캠페인명</th>
	<th style='width: 100px'>작성일</th>
	<th style='width: 100px'>후기알림메일</th>
	<th style='width: 100px'>후기알림문자</th>
</tr>
<?
$sql = "SELECT *, p.sent_mail AS p_sent_mail, p.sent_sms AS p_sent_sms FROM ".DB_POSTSCRIPTS." p LEFT JOIN ".DB_CAMPAIGNS." c ON c.it_id = p.wr_campaign $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :

   $send_mail = $data[p_sent_mail] == 0 ? "<button data-wrid='$data[wr_id]' class='btn btn-primary btn-small send_mail' type='button'>보내기</button>" : 
      ($data[p_sent_mail] == 1 ? "보내는 중.." : "<span style='color:red'>보냈음</span>");
   $send_sms = $data[p_sent_sms] == 0 ? "<button data-wrid='$data[wr_id]' class='btn btn-primary btn-small send_sms' type='button'>보내기</button>" : 
      ($data[p_sent_sms] == 1 ? "보내는 중.." : "<span style='color:red'>보냈음</span>");
?>
<tr> 
	<td data-href='./postscripts.form.php?no=<?=$data[wr_id]?>' style='font-size: 10px; text-align: center'><? print ($data[wr_category] == 1) ? '전달후기' : '이벤트 후기'; ?></td>
	<td data-href='./postscripts.form.php?no=<?=$data[wr_id]?>' style='padding-left: 15px'><?=$data[wr_subject]?></td>
	<td data-href='./postscripts.form.php?no=<?=$data[wr_id]?>'><?=$data[it_name]?></td>
	<td data-href='./postscripts.form.php?no=<?=$data[wr_id]?>' style='font-size: 10px; text-align: center'><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])))?></td>
   <td data-kind='email' data-wrid='<?=$data[wr_id]?>' style='text-align:center;'><?=$send_mail?></td>
   <td data-kind='sms' data-wrid='<?=$data[wr_id]?>' style='text-align:center;'><?=$send_sms?></td>
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal' class='nav'>»</a> ";
?>
</span>

</div>
<script>
$(document).ready(function() {
   $(".send_mail").click(function() {
      if(!confirm("기부자들에게 메일을 보내시겠습니까?")) {
         return false;
      }
      $("td[data-kind=email][data-wrid="+$(this).data('wrid')+"]").html("보내는 중..");
      js_send_email({mail_type:"upload_postscript", wr_id:$(this).data("wrid")});
   });
   $(".send_sms").click(function() {
      if(!confirm("기부자들에게 문자를 보내시겠습니까?")) {
         return false;
      }
      $("td[data-kind=sms][data-wrid="+$(this).data('wrid')+"]").html("보내는 중..");
      js_send_sms({sms_type:"upload_postscript", wr_id:$(this).data("wrid")});
   });
});
</script>
