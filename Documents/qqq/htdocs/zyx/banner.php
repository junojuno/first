<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'bn_priority';
$no;
$mode;
$bn_type = array('공지 배너', '캠페인 배너');
?>

<script>
$(document).ready(function() {
	$('.btn-delete').click(function() {
		if (confirm('정말 삭제하시겠습니까?')) {
			$('input[name=mode]').val('delete');
			$('input[name=no]').val($(this).val());
			document.form.submit();
		}
	});
	$('.btn-up').click(function() {
		var Row = "#"+$(this).parent().parent().attr('id');
		var prevRow = "#"+$(this).parent().parent().prev().attr('id');
		temp = parseInt($(this).val());
		if(prevRow == undefined){
			return false;
		}
		$(Row).after($(prevRow));
		return false;
	});
	$('.btn-down').click(function() {
		var Row = "#"+$(this).parent().parent().attr('id');
		var nextRow = "#"+$(this).parent().parent().next().attr('id');
		temp = parseInt($(this).val());
		if(nextRow == undefined){
			return false;
		}
		$(Row).before($(nextRow));
		return false;
	});
	$('.btn-button').click(function(){
		if( confirm('수정하시겠습니까?')){
			$('input[name=mode]').val('orderEdit');
			document.form.submit();
		}
	});

});
</script>
<div style='padding: 0px 20px 100px 175px'>
	<h2>메인 배너 관리</h2>
<form name='form' action="do.php" method="POST">
<input type='hidden' name='section' value='banner'/>
<input type='hidden' name='mode' value='<?=$mode?>' />
<input type='hidden' name='no' value='<?=$no?>'/>
<div>
	<input type="button" class="btn-button" value="저장" style=" float: right">&nbsp;
	<a href='./banner.form.php'><button type='button' style='float: right'>배너 추가</button></a>&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>

	<th style='width: 100px'>순서</th>
	<th style='width: 100px'>종류</th>
	<th style='width: 150px'>이미지</th>
	<th>배너 명</th>
	<th style='width: 150px;'>편집</th>
	<th style='width: 150px;'>공개/비공개</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_BANNER." $where ORDER BY $order";
$result = sql_query($sql);
$total = mysql_num_rows($result);
for ($i = 0; $i < $total; $i++) :

mysql_data_seek($result, $i);
$data = mysql_fetch_array($result);
$bn_up = "<button class='btn-up' value='$i'>up</button> ";
$bn_down = "<button class='btn-down' value='$i'>down</button> ";
$bn_edit = "<button type='button' class='btn-edit' onclick=\"location.href='./banner.form.php?no={$data['bn_no']}&mode=edit'\">수정</button> ";
$bn_delete = "<button class='btn-delete' value='$i'>삭제</button> ";

$tri = array();
$tri[$i] = $i;
?>

<tr id='tr_<?=$i?>'>
	<input type='hidden' name='bn_no[]' value='<?=$data[bn_no]?>' >
	<td style='font-size: 10px; text-align: center'><?=$bn_up?><?=$bn_down?></td>
	<td style='font-size: 10px; text-align: center'><?=$bn_type[$data[bn_type]]?></td>
	<td style='text-align: center'>
	<? if( $data[bn_type] == 0 ) {?>
	<img src='/data/banner/<?=$data[bn_image]?>' style='width: 120px; height: 60px; outline: solid 1px silver; margin: 10px 0px' /></td>
	<? } else {?>
	<img src='/data/campaign/<?=$data[bn_image]?>/banner.jpg' style='width: 120px; height: 60px; outline: solid 1px silver; margin: 10px 0px' /></td>
	<? } ?>
	<td style='padding-left: 10px'><?=$data[bn_name]?></td>
   	<td style='text-align:center;'><?=$bn_edit?><?=$bn_delete?></td>
   	<td style='text-align:center;'>
   	<input type='radio' name='bn_public[<?=$i?>]' value="1" <? print ($data[bn_public] == '1') ? 'checked' : false; ?> />
   	<input type='radio' name='bn_public[<?=$i?>]' value="0" <? print ($data[bn_public] == '0') ? 'checked' : false; ?> />
	</td>
</tr>

<? endfor; ?>
</table>

</form>
</div>

