<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no] ? $_REQUEST[no] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no) {
	$data = sql_fetch("SELECT * FROM ".DB_NOTICE." WHERE id = '$no' ");
}

?>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>공지사항 <?=$title?></h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='notice'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<link rel='stylesheet' href='/css/redactor.css' />
<script src='/js/redactor.min.js'></script>
<script>
$(document).ready(function() {
	$('textarea[name=content]').redactor({
      imageUploadExtraParams: {from:'notice'},
		imageUpload: "ajax.upload.php",
		imageUploadCallback: function(image, json) {
			console.log(image);
		}
	});
	$('#btn-delete').click(function() {
		if (confirm('정말 삭제하시겠습니까?')) {
			$('input[name=mode]').val('delete');
			document.form.submit();
		}
	});
});
</script>
<table cellpadding='0' cellspacing='0' style=' border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>사이트</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='radio' name='site_code' value='A001' checked />DSP ZONE
	</td>
</tr>
<tr>
	<th style='width: 120px'>구분</th>
	<td colspan='5' style='padding-left: 10px'>
   <input type='radio' name='type' value='1' <?=$data[type] == 1 || !$data[type] ? 'checked' : false?> />NOTICE
	<input type='radio' name='type' value='2' <?=$data[type] == 2 ? 'checked' : false?> />나눔
	</td>
</tr>
<tr>
	<th style='width: 120px'>제목</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='title' style='width: 95%' value='<?=$data[title]?>' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>내용</th>
	<td colspan='5' style='padding-left: 10px;width:600px;'>
	<textarea name='content' class='text' style='width: 600px; line-height: 20px'><?=$data[content]?></textarea>
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='확인' class='submit' />
<input id='btn-delete' type='button' class='submit' value='이 글 삭제' style='margin-left: 30px' />
</div>
</form>

</div>

