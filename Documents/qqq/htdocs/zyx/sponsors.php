<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'sp_no';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>스폰서 관리</h2>

<div>
<a href='./sponsors.form.php'><button style='float: right; width: auto' class='button'>스폰서 추가</button></a>
&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 50px'>ID</th>
	<th style='width: 150px'>로고</th>
	<th style='width: 150px'>스폰서명</th>
	<th style='width: 150px'>스폰서 홈페이지</th>
	<th>스폰서 설명</th>
</tr>
<?
$sql = "SELECT * FROM wegen_sponsor $where ORDER BY $order $sort";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$desc = (mb_strlen($data[sp_desc], 'UTF-8') > 120) ? mb_substr($data[sp_desc], 0, 120, 'UTF-8').'...' : $data[sp_desc];
?>
<tr data-href='./sponsors.form.php?no=<?=$data[sp_no]?>'>
	<td style='font-size: 10px; text-align: center'><?=$data[sp_no]?></td>
	<td style='text-align: center'><img src='/data/partners/<?=$data[sp_logo]?>' style='width: 120px; height: 55px; outline: solid 1px silver; margin: 10px 0px' /></td>
	<td style='padding-left: 10px'><?=$data[sp_name]?></td>
	<td style='padding-left: 10px'><?=$data[sp_homepage]?></td>
	<td style='padding-left: 10px'><?=$desc?></td>
</tr>
<? endfor; ?>
</table>

</div>