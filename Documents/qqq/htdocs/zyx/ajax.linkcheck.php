<?php
   if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
      header("HTTP/1.0 404 Not Found");
      exit;
   }

   $_required = true;
   include '../config.php';

   if(!isset($_REQUEST[path])) {
      print "failed";
      exit;
   }

   $paths = $_REQUEST[path];
   $res = sql_query("SELECT path FROM ".DB_LINKS);
   for($i =0; $row = sql_fetch_array($res);$i++){
      $db_links[$i] = $row[path];
   }

   for($i =0; $i < count($paths); $i++) {
      if(file_exists("../".$paths[$i])) {
         if(array_search($paths[$i],$db_links) === false) {
            print $paths[$i];
            exit;
         }
      }
   }

   print "success";
?>
