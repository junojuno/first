<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST["no"] ? $_REQUEST["no"] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no) {
	// * -> 컬럼 이름으로 변환
	// 2013. 08. 10.
	// 진입명
	$data = sql_fetch("SELECT sp_logo, sp_name, sp_homepage, sp_desc FROM wegen_sponsor WHERE sp_no = '$no' ");
	// $data = sql_fetch("SELECT * FROM wegen_sponsor WHERE sp_no = '$no' ");
}
?>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#btn-delete').click(function() {
		if (confirm('정말 삭제하시겠습니까?')) {
			$('input[name=mode]').val('delete');
			// 명시되지 않은 폼을 변경하라는 명령을 명시하여 변경하게 함
			// 2013. 08. 10.
			// 진입명
			$('#thisform').submit();
			// document.form.submit();
		}
	});
});
</script>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>스폰서 <?=$title?></h2>

<!-- 
태그 엘리먼트의 id 속성과 name 속성 변경

2013. 08. 10.
진입명
-->
<form id="thisform" name='thisform' enctype='multipart/form-data' method='post' action='do.php'>
<!-- <form name='form' enctype='multipart/form-data' method='post' action='do.php'> -->
<input type='hidden' name='section' value='sponsor'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>스폰서명</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='sp_name' value='<?=$data[sp_name]?>' style='width: 95%' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>홈페이지</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='sp_homepage' value='<?=$data[sp_homepage]?>' style='width: 95%' />
	</td>
</tr>
<? $noimg = "<span style='color: red; margin-right: 10px'>이미지 없음</span>"; ?>
<tr>
	<th style='width: 120px'>로고</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/partners/'.$data["sp_logo"])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/partners/$data[sp_logo]' target='_blank' title=\"<img src='/data/partners/$data[sp_logo]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='sp_logo' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>스폰서 설명</th>
	<td colspan='5' style='padding-left: 10px'>
	<textarea name='sp_desc' rows='10' style='width: 95%'><?=$data["sp_desc"]?></textarea>
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' />
<? if ($mode == 'edit') : ?>
<input id='btn-delete' type='button' class='submit' value='스폰서 삭제' style='margin-left: 30px' />
<? endif; ?>
</div>
</form>

</div>