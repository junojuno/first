<?php
	$_required = true;
	include "../config.php";
	include "../module/_head.admin.php";
	
	$sql = "SELECT * FROM ".DB_INFO;
	$info = sql_fetch($sql);

   $sql = "SELECT * FROM ".DB_LINKS;
   $links = sql_query($sql);
?>

<script>
$(document).ready(function() {
   var http_host = "<?=$_SERVER[HTTP_HOST]?>";
	$('#info_form .button').click(function() {
		if(!$('input[name=info_account]').val()) {
			alert('계좌정보를 입력해주세요.');
			$('input[name=info=account]').focus();
			return false;
		}

		if(confirm('수정하시겠습니까?')) {
			$('#info_form').submit();
		}
	});

   $('#link_form .button').click(function() {
      if($(".path").length == 0) {
         alert("추가할 링크가 없습니다.");
         return false;
      }
      var flag = false;
      var paths = [];
      $(".path").each(function() {
         if(flag) return false;
         var check = $(this).val();
         if(!check.match(/^[\w]+([\/][\w]+)?$/)) {
            console.log(check);
            alert("URL 포맷이 일치하지 않습니다.");
            flag = true;
            $(this).focus();
         } else {
            paths.push(check);
         }
      });
      if(flag) return false;
      flag = false; 
      $(".link").each(function() {
         if($(this).val() == '') {
            alert("링크를 입력해주세요");
            flag = true;
            $(this).focus();
         }
      });
      if(flag) return false;
      console.log(paths);
      $.ajax({
         type: "POST",
         url: "./ajax.linkcheck.php",
         data: {path: paths}
      }).done(function(data){
         if(data == 'failed') {
            alert('알수없는 에러가 발생하였습니다. 다시시도해주세요.');
            return false;
         } else if(data != 'success') {
            alert('해당 링크는 사용하실수 없습니다.\n이미 존재하는 링크 http://'+http_host+'/'+data);
            return false;
         }
         if(confirm('수정하시겠습니까?')) {
            $('#link_form').submit();
         }
      });

   });


   var row_id = 0;
   $('#link_add').click(function(){
      row_id++;
      $('table.link_table').append("<tr id='new"+row_id+"'><td><input type='hidden' name='ids[]' value='0'>http://"+http_host+"/<input type='text' class='path text' name='paths[]' style='width:300px;'></td><td><input type='text' class='link text' name='links[]' style='width:300px;'></td><td><button type='button' class='new_delete' data-id='"+row_id+"'>-</button></td></tr>");
      $('button.new_delete').click(function(){
         $('tr#new'+$(this).data("id")).remove(); 
         return false;
      });
   });
});
</script>

<div style='padding: 0px 20px 100px 175px'>

<h2>위젠 정보 관리</h2>

<div style="margin-bottom:32px;">
	<form id='info_form' action='do.php' method="POST">
		<input type='hidden' name='section' value='wegeninfo'>
      <input type='hidden' name='mode' value='info_table'>
		<table style='width:620px; border:solid 1px #DDDDDD;'>
			<tr>
				<th style='width:97px;'>계좌정보</th>
				<td style='padding-left:10px;'><input type='text' name='info_account' class='text' style='width:300px;' value='<?=$info[account]?>'/></td>
			</tr>
         <tr>
            <th style='width:97px;'>메인페이스북url<span class='help' title="페이스북 해당 포스트 게시글 퍼가기 누른 후 위에 html 태그중 ...a href='여기에있는주소'... 를 복사해서 여기에 집어넣으면 됨"></span></th>
            <td style='padding-left:10px;'><input type='text' name='info_main_url' class='text' style='width:300px;' value='<?=$info[main_fb_url]?>'/></td>
         </tr>
			<tr>
				<td colspan='2' style='text-align:right;'>
					<input type='button' class='button' value='수정'/> 
				</td>
			</tr>
		</table>
	</form>
</div>

<h2>링크 생성<span class='help' title='custom link 말고 기존에 사용하고있는 url은 입력하면 안됩니다. (ex : http://wegen.kr/facebook (페이스북 로그인url), http://wegen.kr/twitter) '></span></h2>

<div>
   <form id="link_form" action="do.php" method="POST">
      <input type='hidden' name='section' value='wegeninfo'>
      <input type='hidden' name='mode' value='link_table'>
      <button type="button" id="link_add">+</button>
      <table class='link_table' style='width:900px;border:solid 1px #DDD;text-align:center;'>
         <tr>
            <th>URL<span class='help' title="알파벳과 _ 문자만 가능하며 맨끝에 '/' 는 생략해주세요"></span></th>
            <th>Redirect 링크</th>
            <th style='width:42px;'>삭제</th>
         </tr>
      <?
      while($row = sql_fetch_array($links)) {
      ?>
         <tr>
            <td>http://<?=$_SERVER[HTTP_HOST]?>/<input type='text' class='path' name="paths[]" value='<?=$row[path]?>' style="width:300px;"></td>
            <td><input type="hidden" name="ids[]" value="<?=$row[id]?>"><input type='text' class='link'  name="links[]" value='<?=$row[link]?>' style="width:300px;"></td>
            <td><input type="checkbox" name="delete_<?=$row[id]?>" value="<?=$row[id]?>"></td>
         </tr>
      <?
      }
      ?>
      </table>
      <input type="button" class="button" value="링크 저장" style="margin:0;margin-top:10px;">
   </form>
</div>

</div>
