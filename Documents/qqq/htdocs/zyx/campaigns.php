<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'it_id';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>캠페인 관리</h2>

<div>
<a href='./campaigns.form.php'><button style='float: right' class='button'>캠페인 추가</button></a>
<a href='./auc_campaigns.form.php'><button style='float: right' class='button'>경매캠페인 추가</button></a>
	<form name='form' method='post' action='do.php'>
	<input type='hidden' name='section' value='highlight' />
	<input type='hidden' name='mode' value='edit' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>
	하이라이트 캠페인
	<select name='it_id'>
	<?
	$sql = "SELECT * FROM ".DB_CAMPAIGNS." WHERE it_isEnd != '1' ORDER BY it_id DESC";
	$result = sql_query($sql);

	for ($i = 0; $data = sql_fetch_array($result); $i++) :
		$selected = ($data[it_isHighlighted] == 1) ? ' selected' : false;
	?>
	<option value='<?=$data[it_id]?>' <?=$selected?>><?=$data[it_name]?></option>
	<? endfor; ?>
	<input type='submit' value='설정' />
	</select>
</form>
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 100px'>캠페인코드</th>
	<th>캠페인명</th>
	<th style='width: 100px'>시작일</th>
	<th style='width: 100px'>종료일</th>
	<th style='width: 70px'>참여자 수</th>
	<th style='width: 100px'>후원총액</th>
	<th style='width: 70px'>진행여부</th>
	<th style='width: 70px'>공개여부</th>
   <th style='width: 70px'>성공메일</th>
   <th style='width: 70px'>성공문자</th>
   <th style='width: 70px'>영문버전</th>
</tr>
<?
$sql = "SELECT i.*, (SELECT sum(od_amount)-sum(pay_remain) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) AS it_funded
FROM ".DB_CAMPAIGNS." i $where ORDER BY $order $sort";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :

	$isVol = ($data[type] == 2) ? true : false;

	$csql = $isVol ? "SELECT COUNT(DISTINCT mb_no) AS cnt FROM wegen_volunteer WHERE it_id = '$data[it_id]' " : "SELECT COUNT(DISTINCT mb_no) AS cnt, SUM(od_amount) AS amt FROM ".DB_ORDERS." WHERE it_id = '$data[it_id]' ";
	$count = sql_fetch($csql);
	$status = ($count[amt] >= $data[it_target]) ? "<span style='color: blue'>성공</span>" : "<span style='color: red'>완료</span>";
	if ($data[it_isEnd] == 0) {
		$diff = round((strtotime($data[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
		$status = ($diff < 0) ? "<span style='color: red'>완료</span>" : "진행중<br/><span style='font-size: 10px'>(D-".$diff.")</span>"; 
	}
	$isPublic = ($data[it_isPublic] == 1) ? '공개' : '비공개';
   $isEn = ($data['is_en'] == 1) ? 
      "<button class='btn btn-warning btn-small' type='button' onclick=\"location.href='./campaigns.form.en.php?it_id={$data['it_id']}&mode=edit'\">수정</button>" : 
      "<button class='btn btn-primary btn-small' type='button' onclick=\"location.href='./campaigns.form.en.php?it_id={$data['it_id']}&mode=insert'\">추가</button>";

   if($data[it_funded] >=$data[it_target]) {
      $success_mail = $data[sent_mail] == 0 ? "<button data-itid='$data[it_id]' class='btn btn-primary btn-small send_mail' type='button'>보내기</button>" : 
         ($data[sent_mail] == 1 ? "보내는 중.." : "<span style='color:red'>보냈음</span>");
      $success_sms = $data[sent_sms] == 0 ? "<button data-itid='$data[it_id]' class='btn btn-primary btn-small send_sms' type='button'>보내기</button>" : 
         ($data[sent_sms] == 1 ? "보내는 중.." : "<span style='color:red'>보냈음</span>");
   } else {
      $success_mail = "<span style='color:lightgray;'>성공ㄴㄴ</span>";
      $success_sms = "<span style='color:lightgray;'>성공ㄴㄴ</span>";
   }
?>
<tr>
	<td style='font-size: 10px; text-align: center'><?=$data[it_id]?></td>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='padding-left: 10px'><? print $isVol ? '<strong>[봉사활동]</strong> ' : false; ?><?=$data[it_name]?></td>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='font-size: 11px; text-align: center'><?=date('Y-m-d', strtotime($data[it_startdate]))?></td>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='font-size: 11px; text-align: center'><?=date('Y-m-d', strtotime($data[it_enddate]))?></td>
	<? if ($isVol) : ?>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='text-align: center'><?=$count[cnt]?></td>
	<? else : ?>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='text-align: center'><a href='donation.php?search=campaign&s=<?=$data[it_id]?>'><?=$count[cnt]?></a></td>
	<? endif; ?>
	<? if ($isVol) : ?>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='font-size: 11px; text-align: center'></td>
	<? else : ?>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='font-size: 11px; text-align: right; padding-right: 10px'><a href='donation.php?search=campaign&s=<?=$data[it_id]?>'><?=number_format($count[amt]);?>원</a></td>
	<? endif; ?>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='text-align: center'><?=$status?></td>
	<td data-href='./campaigns.form.php?it_id=<?=$data[it_id]?>' style='text-align: center'><?=$isPublic?></td>
   <td style='text-align: center;' data-kind='email' data-itid='<?=$data[it_id]?>'><?=$success_mail?></td>
   <td style='text-align: center;' data-kind='sms' data-itid='<?=$data[it_id]?>'><?=$success_sms?></td>
   <td style='text-align: center;'><?=$isEn?></td>
</tr>
<? endfor; ?>
</table>

</div>

<script>
$(document).ready(function() {
   $(".send_mail").click(function() {
      if(!confirm("기부자들에게 메일을 보내시겠습니까?")) {
         return false;
      }
      $("td[data-kind=email][data-itid="+$(this).data('itid')+"]").html("보내는 중..");
      js_send_email({mail_type:"success_campaign", it_id:$(this).data("itid")});
   });
   $(".send_sms").click(function() {
      if(!confirm("기부자들에게 문자를 보내시겠습니까?")) {
         return false;
      }
      $("td[data-kind=sms][data-itid="+$(this).data('itid')+"]").html("보내는 중..");
      js_send_sms({sms_type:"success_campaign", it_id:$(this).data("itid")});
   });
});
</script>
