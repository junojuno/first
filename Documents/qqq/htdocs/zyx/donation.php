<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'od_time';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = (!$_REQUEST[count]) ? (!$_REQUEST[s]) ? 30 : 999 : $_REQUEST[count];
$limit = ($page - 1) * $count;

if ($_REQUEST[s]) {
	switch ($_REQUEST[search]) {
		case 'campaign' :
         $where = " WHERE o.it_id = '$_REQUEST[s]' and o.od_amount - o.pay_remain > 0 ";
			$code = "$('#search').val('campaign'); $('#campaign').show().val('$_REQUEST[s]'); $('#searchbox').hide();";
			break;
		case 'date' :
         $where = " WHERE o.od_time LIKE '%".$_REQUEST[s]."%' and o.od_amount - o.pay_remain > 0 ";
			$code = "$('#search').val('date'); $('#searchbox').show(); $('#searchstring').val('$_REQUEST[s]');";
			break;
		case 'member' :
         $where = " WHERE o.mb_no = '$_REQUEST[s]' OR m.mb_name LIKE '%".$_REQUEST[s]."%' and o.od_amount-o.pay_remain > 0 ";
			$code = "$('#search').val('member'); $('#searchbox').show(); $('#searchstring').val('$_REQUEST[s]');";
			break;
		case 'cancel' :
			$where = " WHERE o.od_amount <= 0";
			$code = "$('#search').val('cancel');";
			break;
		default :
         $where = "WHERE o.od_amount - o.pay_remain > 0";
			break;
	}
}

$result = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_ORDERS." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no ".$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

if($_REQUEST[s] == $SPECIAL[kara_fan]) $kara_fan = true;
?>

<div style='padding: 0px 20px 100px 175px'>

<h2>후원내역 관리</h2>

<div style='height: 30px'>
<button id='btnDelete' style='float: right' class='button' onclick="javascript: deleteDonations();">기부금 취소</button>
<a href="addDonation.php"><button style='float: right' class='button-red'>기부금 등록</button></a>
<a href='excel.php?section=donate&search=<?=$_REQUEST[search]?>&s=<?=$_REQUEST[s]?>'><button class='button' style='float: right; width: auto'>현재 목록을 엑셀로 저장</button></a>
<a href="excel.php?section=donate&kara_fan=1"><button style='float: right' class='button-red'>카라 명단뽑기</button></a>
<select id='search' name='search'>
<option value=''>조건별 정렬</option>
<option value='campaign'>캠페인별</option>
<option value='date'>후원일자별</option>
<option value='member'>회원별</option>
<option value='cancel'>취소내역</option>
</select>
<script>
$(document).ready(function() {
	<?=$code?>
	$('#search').change(function() {
		if ($('#search option:selected').val() == 'campaign') {
			$('#campaign').show();
			$('#searchbox').hide();
		} else if ($('#search option:selected').val() == 'cancel') {
			document.location.href = './donation.php?search=cancel&s=cancel';
		} else if (!$('#search option:selected').val()) {
			$('#campaign').hide();
			$('#searchbox').hide();
		} else {
			$('#campaign').hide();
			$('#searchbox').show();
		}
	});

	$('#campaign').change(function() {
		document.location.href = './donation.php?search=campaign&s=' + $('#campaign option:selected').val();
	});

	$('#searchbtn').click(function() {
		document.location.href = './donation.php?search=' + $('#search option:selected').val() + '&s=' + $('#searchstring').val();
	});
	
	$('#btnDelete').click(function () {
		if ($("input[name=od_ids\\[\\]]:checked").length < 1) {
			alert('선택된 내역이 없습니다.');
			return;
		}
		
		if (confirm('후원내역이 삭제됩니다.')) {
			$('#thisform').submit();
			return;
		}
	});
});
</script>

<select id='campaign' name='campaign' style='display: none'>
<option value=''></option>
<?
$sql = "SELECT * FROM ".DB_CAMPAIGNS." ORDER BY it_id DESC";
$result = sql_query($sql);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
?>
<option value='<?=$data[it_id]?>'><?=$data[it_name]?></option>
<? endfor; ?>
</select>

<span id='searchbox' style='display: none'>
<input id='searchstring' type='text' name='s' />
<button id='searchbtn'>검색</button>
</span>

</div>

<form id="thisform" name='thisform' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='donation'/>
<input type='hidden' name='mode' value='delete'/>
<input type='hidden' name='on' value='0'/>
<?
$sql = "SELECT * FROM ".DB_ORDERS." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);
?>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 50px'></th>
	<th style='width: 80px'>후원코드</th>
	<th style='width: 80px'>후원일자</th>
	<th style='width: 150px'>회원명</th>
	<th style='width: 300px;'>캠페인명</th>
	<th style='width: 82px'>접속수단</th>
	<th style='width: 90px'>후원금액</th>
	<th style='width: 100px'>결제수단</th>
	<th style='width: 70px'>이벤트</th>
	<th style='width: 200px'>리워드</th>
	<th style='width: 100px'>연락처</th>
	<? if(isset($_GET['search']) && $_GET['search'] == 'campaign' && isset($_GET['s']) && ($_GET['s'] == '1372771180' || $_GET['s'] == '1372609576' || $_GET['s'] == '1372573236') ) { ?>
		<th style='width:100px;'>사번</th>
	<? } ?>
	<th style='width: 100px'>이메일</th>
	<th style='width: 200px'>주소</th>
</tr>


<?
for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$isEvent = ($data[od_isEvent] != '없음') ? ($data[od_isEvent] != '미참여') ? "<span style='color: blue'>참여</span>" : "<span style='color: red'>미참여</span>" : '-';
	if (!$data[od_isEvent]) $isEvent = "<span style='color: red'>미참여</span>";
	$check = sql_fetch("SELECT no FROM ".DB_FUNDRAISERS." WHERE it_id = '$data[it_id]' ");
	$isEvent = ($check[no] > 0) ? $isEvent : '-';
	$zip = $data[mb_zip1] ? '('.$data[mb_zip1].'-'.$data[mb_zip2].') ' : false;
?>
<tr>
    <td style='text-align: center'><input type='checkbox' name='od_ids[]' value='<?=$data[od_id]?>'/></td>
	<td style='font-size:10px; text-align: center;'><?=$data[od_id]?></td>
	<td style='font-size:10px; text-align: center;'><?=$data[od_time]?></td>
	<td style='font-size:11px; text-align: center;'><?=$data[mb_name]?></td>
	<td style='font-size:10px; text-align: center;'><?=$data[it_name]?></td>
	<td style='font-size:11px; text-align: center;'><?=$data[connect]?></td>
	<td style='font-size: 10px; text-align: right; padding-right: 10px'><?=number_format($data[od_amount]-$data[pay_remain])?>원</td>
	<td style='font-size:10px; text-align: center;'><?=$data[od_method]?></td>
	<td style='font-size:10px; text-align: center'><?=$isEvent?></td>
	<td style='font-size: 11px;'><?=$data['reward']?></td>
	<td style='font-size: 10px; text-align: center;'><?=$data[mb_contact]?></td>
	<? if(isset($_GET['search']) && $_GET['search'] == 'campaign' && isset($_GET['s']) && ($_GET['s'] == '1372771180' || $_GET['s'] == '1372609576' || $_GET['s'] == '1372573236')  ) { 
			$cn = explode('@',$data['mb_id']);
	?>
		<td style='width:100px;text-align:center;font-size:10px;'><? if($data[mb_type] == 44) { echo "$cn[0]";} ?></td>
	<? } ?>
	<td style='font-size:10px; text-align: center;'><?=$data[mb_email]?></td>
	<td style='font-size: 10px; padding-right:10px;'><?=$zip?><?=$data[mb_addr1]?> <?=$data[mb_addr2]?></td>
</tr>
<? endfor; ?>
</table>
</form>
<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>»</a> ";
?>
</span>

</div>
