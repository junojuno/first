<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no] ? $_REQUEST[no] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no) {
	$data = sql_fetch("SELECT * FROM ".DB_POSTSCRIPTS." WHERE wr_id = '$no' ");
}

?>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>캠페인 후기 <?=$title?></h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='postscript'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<link rel='stylesheet' href='/css/redactor.css' />
<script src='/js/redactor.min.js'></script>
<script>
$(document).ready(function() {
	$('textarea[name=wr_content]').redactor({
		imageUpload: "ajax.upload.php",
		imageUploadCallback: function(image, json) {
			console.log(image);
		}
	});
	$('#btn-delete').click(function() {
		if (confirm('정말 삭제하시겠습니까?')) {
			$('input[name=mode]').val('delete');
			document.form.submit();
		}
	});
});
</script>
<table cellpadding='0' cellspacing='0' style=' border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>캠페인</th>
	<td colspan='5' style='padding-left: 10px'>
	<select name='wr_campaign'>
	<option value=''></option>
	<?
	$sql = "SELECT * FROM ".DB_CAMPAIGNS." ORDER BY it_id DESC";
	$result = sql_query($sql);

	for ($i = 0; $row = sql_fetch_array($result); $i++) :
		$checked = $data[wr_campaign] == $row[it_id] ? 'selected' : false;
	?>
	<option value='<?=$row[it_id]?>' <?=$checked?>><?=$row[it_name]?></option>
	<? endfor; ?>
	</select>

	</td>
</tr>
<tr>
	<th style='width: 120px'>후기 구분</th>
	<td colspan='5' style='padding-left: 10px'>
	<? $checked = ($data[wr_category] == '1' || !$data[wr_category]) ? 'checked' : false; ?>
	<input type='radio' name='wr_category' value='1' <?=$checked?> />전달 후기
	<? $checked = ($data[wr_category] == '2') ? 'checked' : false; ?>
	<input type='radio' name='wr_category' value='2' <?=$checked?> />이벤트 후기
	</td>
</tr>
<tr>
<th style='width: 120px'>(NEW) 메인타이틀1<span class='help' title='리뉴얼 이벤트후기 top제목'></span></th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='wr_subject' style='width: 95%' value='<?=$data[wr_subject]?>' />
	</td>
</tr>
<tr>
<th style='width: 120px'>(NEW) 메인타이틀2<span class='help' title='리뉴얼 이벤트후기 bottom제목'></span></th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='wr_subject2' style='width: 95%' value='<?=$data[wr_subject2]?>' />
	</td>
</tr>
<? $noimg = "<span style='color: red; margin-right: 10px'>이미지 없음</span>"; ?>
<tr>
	<th style='width: 120px'>커버 이미지</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print ($data[wr_cover] && file_exists('../'.$data[wr_cover])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='$data[wr_cover]' target='_blank' title=\"<img src='$data[wr_cover]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='wr_cover' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>내용</th>
	<td colspan='5' style='padding-left: 10px;width:600px;'>
	<textarea name='wr_content' class='text' style='width: 600px; line-height: 20px'><?=$data[wr_content]?></textarea>
	</td>
</tr>
<tr>
	<th style='width: 120px'>작성일 지정<span class='help' title="미지정시 현재 시간이 작성일로 등록됩니다." /></th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='wr_datetime' class='datetime' value='<?=array_pop(array_reverse(explode(' ', $data[wr_datetime])));?>' readonly />
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='확인' class='submit' />
<input id='btn-delete' type='button' class='submit' value='이 글 삭제' style='margin-left: 30px' />
</div>
</form>

</div>

