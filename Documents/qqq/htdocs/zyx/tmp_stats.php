<?php
   $_required = true;
   include '../config.php';
   $info = sql_fetch("SELECT 
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE stats = 'a') AS num_a, 
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE stats = 'b') AS num_b, 
         COUNT(*) AS num_all,
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE stats = 'a' AND connect = 'PC') AS num_a_pc,
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE stats = 'a' AND connect = 'MOBILE') AS num_a_mob,
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE stats = 'b' AND connect = 'PC') AS num_b_pc,
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE stats = 'b' AND connect = 'MOBILE') AS num_b_mob,
         (SELECT COUNT(*) FROM ".DB_ABTEST." WHERE connect = '') AS num_null
         FROM ".DB_ABTEST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title></title>
</head>
<body>
<p>
   총 접속자수 : <?=$info[num_all]?>명
</p>
<p>
   A : <?=$info[num_a]?>명 (PC : <?=$info[num_a_pc]?>명, MOBILE : <?=$info[num_a_mob]?>명, ??? : <?=($info[num_a]-$info[num_a_mob]-$info[num_a_pc])?>명)
</p>
<p>
   B : <?=$info[num_b]?>명 (PC : <?=$info[num_b_pc]?>명, MOBILE : <?=$info[num_b_mob]?>명, ??? : <?=($info[num_b]-$info[num_b_mob]-$info[num_b_pc])?>명)
</p>
<p>
   ???  => (PC, MOBILE 접속여부 체크하는거 추가하기전이라 뭐로접속했는지모름)
</p>
   
</body>
</html>
