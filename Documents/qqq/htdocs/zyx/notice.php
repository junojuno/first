<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'upload_time';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = $_REQUEST[count] ? $_REQUEST[count] : 30;
$limit = ($page - 1) * $count;

$result = sql_fetch("SELECT COUNT(id) AS cnt FROM ".DB_NOTICE.$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>
<style>
   table td {text-align:center;}
</style>
<div style='padding: 0px 20px 100px 175px'>

<h2>공지사항 관리</h2>

<div style='text-align: right'>
<a href='./notice.form.php'><button style='float: right' class='button'>글 추가</button></a>
&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
   <th style='width: 100px'>사이트</th>
	<th style='width: 100px'>구분</th>
	<th>제목</th>
	<th style='width: 250px'>업로드시간</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_NOTICE." $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
   $site = $data[site_code] == 'A001' ? 'DSPZONE' : '위젠';
   $type = $data[type] == 1 ? 'NOTICE' : '나눔';
?>
<tr data-href='./notice.form.php?no=<?=$data[id]?>'> 
<td><?=$site?></td>
   <td><?=$type?></td>
<td><?=$data[title]?></td>
<td><?=$data[upload_time]?><?=$data[is_modified] == 1 ? ' 수정됨' : ''?></td>

</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal' class='nav'>»</a> ";
?>
</span>

</div>
