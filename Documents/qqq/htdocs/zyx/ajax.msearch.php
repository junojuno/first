<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	header("HTTP/1.0 404 Not Found");
//	exit;
}

$_required = true;
include '../config.php';

sec_session_start();

if (!$_SESSION[is_admin]) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

if($_POST['s'] == '') exit;

$except = "";
if($_POST['except'] != '') {
   
   $ex = explode("|", $_POST['except']);
   for($i =0; $i < count($ex); $i++)  {
      $except .= " AND mb_no != $ex[$i] ";
   }
}

$res = sql_query("SELECT * FROM ".DB_MEMBERS." 
      WHERE (mb_id LIKE '%".$_POST['s']."%' OR mb_email LIKE '%".$_POST['s']."%' OR mb_name LIKE '%".$_POST['s']."%' OR mb_contact LIKE '%".$_POST['s']."%') 
      $except AND mb_type != 44 AND isAdmin = 0 
      ORDER BY mb_date DESC LIMIT 0, 10");

if($res) {
   $ret = "";
   while($data = sql_fetch_array($res)) {
      $sp = $ret == "" ? "" : "|";
      //변수에 출력하기
      ob_start();
      drawPortrait($data[mb_no]);
      $image = ob_get_contents();
      ob_end_clean();
      $ret .= $sp.$data[mb_no]."#!#".$image."#!#".$data[mb_name]."#!#".$data[mb_id]."#!#".$data[mb_email]."#!#".$data[mb_contact];
   }
   echo $ret;
}

?>
