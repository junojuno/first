<?
$_required = true;
include '../config.php';

sec_session_start();

require "./pp_cli_hub_lib.php";

if(!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
//	exit;
}
$_SESSION[token] = '';

if (array_search($_SESSION[user_no], $settings[admin]) === false ) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$no = $_REQUEST[no];
if (!$no || !preg_match("/^[0-9]+$/", $no)) {
	alert('비정상 접근');
	exit;
}
$data = sql_fetch("SELECT * FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$no' AND isFirst = '1' ");
if (!$data[mb_no]) {
	alert('비정상 접근');
	exit;
}

$pay_method = 'CARD';  // 결제 방법
$ordr_idxx  = date('ymdHis').rand(100, 999);  // 주문 번호
$good_name  = '정기결제-관리자';  // 상품 정보
$good_mny   = $data[od_amount];  // 결제 금액
$buyr_name  = $data[od_name];  // 주문자 이름
$buyr_mail  = $_POST[ "buyr_mail"  ];  // 주문자 E-Mail
$buyr_tel1  = $_POST[ "buyr_tel1"  ];  // 주문자 전화번호
$buyr_tel2  = $_POST[ "buyr_tel2"  ];  // 주문자 휴대폰번호
$req_tx     = 'pay';  // 요청 종류
$currency   = '410';  // 화폐단위 (WON/USD)

$group_id   = $data[od_groupid];
$batch_key  = $data[od_escrow2]; 

$soc_no     = $_POST[ "soc_no"  ];     // 주민등록번호
$tran_cd    = ""; // 트랜잭션 코드
$bSucc      = ""; // DB 작업 성공 여부

$res_cd     = ""; // 결과코드
$res_msg    = ""; // 결과메시지
$tno        = ""; // 거래번호

$card_pay_method = 'Batch';  // 카드 결제 방법
$card_cd         = "";                                                       // 카드 코드
$card_no         = "";                                                       // 카드 번호
$card_name       = "";                                                       // 카드명
$app_time        = "";                                                       // 승인시간
$app_no          = "";                                                       // 승인번호
$noinf           = "";                                                       // 무이자여부
$quota			 = "";														 // 할부개월
$cust_ip = getenv( "REMOTE_ADDR" );

/* =   02. 인스턴스 생성 및 초기화                                              = */

$c_PayPlus  = new C_PAYPLUS_CLI;
$c_PayPlus->mf_clear();


if ( $req_tx == "pay" ) {
	$tran_cd = "00100000";
    $common_data_set = "";
    $common_data_set .= $c_PayPlus->mf_set_data_us("amount", $good_mny);
    $common_data_set .= $c_PayPlus->mf_set_data_us("currency", $currency);

	if (!$soc_no == "") {
		$common_data_set .= $c_PayPlus->mf_set_data_us("soc_no",  $soc_no );
	}
	$common_data_set .= $c_PayPlus->mf_set_data_us("cust_ip",  $cust_ip );
	$common_data_set .= $c_PayPlus->mf_set_data_us("escw_mod", "N"      );

	$c_PayPlus->mf_add_payx_data( "common", $common_data_set );

			// 주문 정보
			$c_PayPlus->mf_set_ordr_data( "ordr_idxx", $ordr_idxx );
//			$c_PayPlus->mf_set_ordr_data( "good_name", good_name );
			$c_PayPlus->mf_set_ordr_data( "good_mny",  $good_mny  );
//			$c_PayPlus->mf_set_ordr_data( "buyr_name", buyr_name );
//			$c_PayPlus->mf_set_ordr_data( "buyr_tel1", buyr_tel1 );
//			$c_PayPlus->mf_set_ordr_data( "buyr_tel2", buyr_tel2 );
//			$c_PayPlus->mf_set_ordr_data( "buyr_mail", buyr_mail );

	if ( $pay_method == "CARD" ) {
		$card_data_set;

		$card_data_set .= $c_PayPlus->mf_set_data_us( "card_mny", $good_mny );        // 결제 금액

		if ( $card_pay_method == "Batch" ) {
			$card_data_set .= $c_PayPlus->mf_set_data_us( "card_tx_type",   "11511000" );
			$card_data_set .= $c_PayPlus->mf_set_data_us( "quota", "00"); // 히든폼값을 없애고 강제지정
			$card_data_set .= $c_PayPlus->mf_set_data_us( "bt_group_id",    $group_id );
			$card_data_set .= $c_PayPlus->mf_set_data_us( "bt_batch_key",   $batch_key );
		}
		$c_PayPlus->mf_add_payx_data( "card", $card_data_set );
	}
}
// 취소/매입 요청
else if ( $req_tx == "mod" ) {
	$mod_type = $_POST[ "mod_type" ];

	$tran_cd = "00200000";

	$c_PayPlus->mf_set_modx_data( "tno",      $_POST[ "tno" ]      );      // KCP 원거래 거래번호
	$c_PayPlus->mf_set_modx_data( "mod_type", $mod_type            );      // 원거래 변경 요청 종류
	$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip             );      // 변경 요청자 IP
	$c_PayPlus->mf_set_modx_data( "mod_desc", $_POST[ "mod_desc" ] );      // 변경 사유
}

if ( $tran_cd != "" ) {
	$c_PayPlus->mf_do_tx( $trace_no, $g_conf_home_dir, $g_conf_batch_cd, $g_conf_batch_key, $tran_cd, "",
		$g_conf_gw_url, $g_conf_gw_port, "payplus_cli_slib", $ordr_idxx,
		$cust_ip, "3" , 0, 0, $g_conf_key_dir, $g_conf_log_dir); // 응답 전문 처리

	$res_cd  = $c_PayPlus->m_res_cd;  // 결과 코드
	$res_msg = $c_PayPlus->m_res_msg; // 결과 메시지
}
else {
	$c_PayPlus->m_res_cd  = "9562";
	$c_PayPlus->m_res_msg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
}

    /* =   04. 승인 결과 처리                                                       = */

if ( $req_tx == "pay" ) {
	if ( $res_cd == "0000" ) {
		$tno       = $c_PayPlus->mf_get_res_data( "tno"       ); // KCP 거래 고유 번호

		if ( $pay_method == "CARD" ) {
			$card_cd   = $c_PayPlus->mf_get_res_data( "card_cd"   ); // 카드사 코드
			$card_no   = $c_PayPlus->mf_get_res_data( "card_no"   ); // 카드 번호
			$card_name = $c_PayPlus->mf_get_res_data( "card_name" ); // 카드 종류
			$app_time  = $c_PayPlus->mf_get_res_data( "app_time"  ); // 승인 시간
			$app_no    = $c_PayPlus->mf_get_res_data( "app_no"    ); // 승인 번호
			$noinf     = $c_PayPlus->mf_get_res_data( "noinf"     ); // 무이자 여부 ( 'Y' : 무이자 )
			$quota     = $c_PayPlus->mf_get_res_data( "quota"     ); // 할부 개월 수

			$check = sql_fetch("SELECT preferdate, coin_method FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$data[mb_no]' AND isFirst = '1' ");

			$result = mysql_query("INSERT INTO ".DB_REGULARPAYMENT."
						SET
						od_id		= '$ordr_idxx',
						mb_no		= '$data[mb_no]',
						od_amount	= '$good_mny',
						od_ip		= '$cust_ip',
						od_method	= '신용카드',
						od_escrow1	= '$card_cd',
						od_escrow2	= '$batch_key',
						od_groupid  = '$group_id',
                  od_tno = '$tno',
						preferdate	= '$check[preferdate]',
						coin_method = '$check[coin_method]',
						isFirst		= '0'
			");

/*
						od_name		= '$buyr_name',
						od_hp		= '$buyr_tel1',
						od_zip1		= '$data[od_zip1]',
						od_zip2		= '$data[od_zip2]',
						od_addr1	= '$data[od_addr1]',
						od_addr2	= '$data[od_addr2]',
*/

			if ($result) {
				$result = mysql_query("INSERT INTO ".DB_COINS."
						SET
						mb_no		= '$data[mb_no]',
						coin_category = '2',
						coin_desc	= 'REG_ADMIN',
						od_id		= '$ordr_idxx',
						amount	= '$good_mny'
				");
			}
		}

		$bSucc = "";             // DB 작업 실패일 경우 "false" 로 세팅

		if ( $req_tx == "pay" ) {
			if( $res_cd == "0000" ) {	
				if ( $bSucc == "false" ) {
					$c_PayPlus->mf_clear();

					$tran_cd = "00200000";

					$c_PayPlus->mf_set_modx_data( "tno",      $tno                         );  // KCP 원거래 거래번호
					$c_PayPlus->mf_set_modx_data( "mod_type", "STSC"                       );  // 원거래 변경 요청 종류
					$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip                     );  // 변경 요청자 IP
					$c_PayPlus->mf_set_modx_data( "mod_desc", "결과 처리 오류 - 자동 취소" );  // 변경 사유

					$c_PayPlus->mf_do_tx( $tno,  $g_conf_home_dir, $g_conf_site_cd,
						  $g_conf_site_key,  $tran_cd,    "",
						  $g_conf_gw_url,  $g_conf_gw_port,  "payplus_cli_slib",
						  $ordr_idxx, $cust_ip, "3" ,
						  0, 0, $g_conf_key_dir, $g_conf_log_dir);

					$res_cd  = $c_PayPlus->m_res_cd;
					$res_msg = $c_PayPlus->m_res_msg;
				}
			}
		}
	}
}
//echo $mod_type;
$res_msg = iconv("euc-kr", "utf-8", $res_msg);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>정기후원 처리</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<script>
alert('<?=$res_msg?>');
document.location.href = './givex.php';
</script>
</head>

<body>
</body>
</html>
