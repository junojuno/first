<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'timestamp';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = (!$_REQUEST[count]) ? (!$_REQUEST[s]) ? 30 : 999 : $_REQUEST[count];
$limit = ($page - 1) * $count;

if ($_REQUEST[s]) {
	switch ($_REQUEST[search]) {
		case 'campaign' :
			$where = " WHERE o.it_id = '$_REQUEST[s]' ";
			$code = "$('#search').val('campaign'); $('#campaign').show().val('$_REQUEST[s]'); $('#searchbox').hide();";
			break;
		case 'date' :
			$where = " WHERE o.od_time LIKE '%".$_REQUEST[s]."%' ";
			$code = "$('#search').val('date'); $('#searchbox').show(); $('#searchstring').val('$_REQUEST[s]');";
			break;
		case 'member' :
			$where = " WHERE o.mb_no = '$_REQUEST[s]' OR m.mb_name LIKE '%".$_REQUEST[s]."%' ";
			$code = "$('#search').val('member'); $('#searchbox').show(); $('#searchstring').val('$_REQUEST[s]');";
			break;
		default :
			$where = false;
			break;
	}
}

$sql = "
	SELECT * FROM (
	(
		SELECT
			'2' AS action,
			o.mb_no AS mb_no,
			'donate-done' AS param1,
			i.it_name AS param2,
			o.od_amount AS param3,
			o.od_time AS timestamp
		FROM ".DB_ORDERS." o
		LEFT JOIN ".DB_CAMPAIGNS." i ON (o.it_id = i.it_id) 
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = o.mb_no)
		WHERE o.od_amount > 0
	)
	UNION (
		SELECT
			'4' AS action,
			s.mb_no AS mb_no,
			'share' AS param1,
			i.it_name AS param2,
			'' AS param3,
			acquired AS timestamp
		FROM ".DB_BADGES." s
		LEFT JOIN ".DB_CAMPAIGNS." i ON (s.it_id = i.it_id)
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = s.mb_no)
		WHERE s.category = '3'
	)
	UNION (
		SELECT
			'2' AS action,
			r.mb_no AS mb_no,
			'givex' AS param1,
			'' AS param2, 
			od_amount AS param3,
			r.od_time AS timestamp
		FROM ".DB_REGULARPAYMENT." r
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = r.mb_no)
		WHERE r.isFirst = '1'
	)
	UNION (
		SELECT
			action,
			o.mb_no,
			param1,
			IF(it_name != '', it_name, param2) AS param2,
			'' AS param3,
			timestamp
		FROM ".DB_ACTIVITY." o
		LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no
		LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.param2
	)
	UNION (
		SELECT
			'4' AS action,
			c.mb_no,
			'comment' AS param1,
			IF(it_name != '', it_name, '') AS param2,
			'' AS param3,
			cmt_time AS timestamp
		FROM ".DB_CAMPAIGN_CMTS." c
		LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = c.mb_no
		LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = c.it_id
	)
	UNION (
		SELECT
			'0' AS action,
			m.mb_no AS mb_no,
			'register' AS param1,
			m.mb_id AS param2,
			'' AS param3,
			m.mb_date AS timestamp
		FROM ".DB_MEMBERS." m
		WHERE m.mb_level = '1'
	)
	)a
	WHERE mb_no NOT IN(101, 339)
	ORDER BY timestamp DESC
";


$result = sql_query($sql);
$total = mysql_num_rows($result);

$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>ACTIVITY LOG</h2>

<div style='height: 30px'>
<a href='excel.php?search=<?=$_REQUEST[search]?>&s=<?=$_REQUEST[s]?>'><button class='button' style='float: right; width: auto'>현재 목록을 엑셀로 저장</button></a>
<select id='search' name='search'>
<option value=''>조건별 정렬</option>
<option value='date'>후원일자별</option>
<option value='member'>회원별</option>
</select>
<script>
$(document).ready(function() {
	<?=$code?>
	$('#search').change(function() {
		if (!$('#search option:selected').val()) {
			$('#campaign').hide();
			$('#searchbox').hide();
		}
		else {
			$('#campaign').hide();
			$('#searchbox').show();
		}
	});

	$('#campaign').change(function() {
		document.location.href = './donation.php?search=campaign&s=' + $('#campaign option:selected').val();
	});

	$('#searchbtn').click(function() {
		document.location.href = './donation.php?search=' + $('#search option:selected').val() + '&s=' + $('#searchstring').val();
	});
});
</script>

<span id='searchbox' style='display: none'>
<input id='searchstring' type='text' name='s' />
<button id='searchbtn'>검색</button>
</span>

</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>일시</th>
	<th style='width: 120px'>회원명</th>
	<th>활동내용</th>
</tr>
<?

$sql = $sql . "	LIMIT $limit, $count ";


$result = sql_query($sql);
$total = mysql_num_rows($result);

$actions = array('campaign', 'detail', 'givex', 'about', 'request', 'postscript', 'partners', 'my');
$actionDescs = array(
	'[캠페인 리스트] 페이지',
	' 상세 페이지',
	'[정기후원] 페이지',
	'[위젠 소개] 페이지',
	'[협력요청] 페이지',
	'[후기] 페이지',
	'[파트너] 페이지',
	'마이페이지'
);

$donations = array('donate-done', 'donate-coin', 'donate', 'givex');
$donationDescs = array(
	'에 COST원 후원',
	'에 코인 COST개 후원',
	'후원하기 버튼 누름',
	' 매달 COST원 정기후원을 신청'
);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$name = sql_fetch("SELECT mb_name FROM ".DB_MEMBERS." WHERE mb_no = '$data[mb_no]' ");
	switch ($data[action]) {
		case 0:
			$memtype = (strpos($data[param2], 'fb@') !== false) ? '페이스북' : '일반';
			$msg = $data[param1] == 'register' ? '위젠에 가입 ('.$memtype.')' : '로그인';
			break;
		case 1:
			$n = array_search($data[param1], $actions);
			$it_name = ($data[param2]) ? '['.$data[param2].']' : false;
			$msg = $it_name.$actionDescs[$n].'를 조회';
			break;

		case 2:
			$n = array_search($data[param1], $donations);
			$it_name = ($data[param2]) ? '['.$data[param2].']' : false;
			$msg = $it_name.$donationDescs[$n];
			$msg = ($data[param3]) ? str_replace('COST', number_format($data[param3]), $msg) : $msg;
			break;
	}

?>
<tr>
	<td style='font-size: 10px; text-align: center'><?=$data[timestamp]?></td>
	<td><?=$name[mb_name]?></td>
	<td><?=$msg?> <? print (!$msg) ? $data[action].'/'.$data[param1].'/'.$data[param2] : ''; ?></td>
</tr>
<? unset($msg); endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>»</a> ";
?>
</span>

</div>