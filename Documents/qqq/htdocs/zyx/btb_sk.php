<?php
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$cpy_admin = $arr_btb['sk'];
?>
<style>
.charge {
   color:red;
   cursor:pointer;
}
</style>
<script>
$(document).ready(function() {
   $('.charge').click(function(){
      if(confirm('충전하시겠습니까?')) {
         $('input[name=id]').val($(this).attr('id'));
         $('input[name=amount]').val($(this).attr('data-coin'));
         $('#charge_form').submit();
      }
   });

});
</script>
<div style='padding: 0px 20px 100px 175px'>

<h2>SK 관리</h2>

<div style='margin:40px 0'>
   <p>SK 총 코인 : <strong><?php print number_format(getCoin($cpy_admin)).'개';?></strong></p>
</div>

<div>
   <h3>요청 내역</h3>
   <table style='width:100%;font:10pt NanumGothic;text-align:center;'>
      <tr>
         <th style='width:150px;'>일자</th>
         <th>요청코인갯수</th>
         <th style='width:177px;'>상태(대기중/충전완료)</th>
      </tr>
      <?php 
      $sql = "SELECT * FROM ".DB_COINS." WHERE coin_category = 4 AND mb_no = '{$cpy_admin}'";
      $result = sql_query($sql);
      if(mysql_num_rows($result) == 0) {
         print "<tr><td colspan='3'>요청내역이 없습니다.</td></tr>";
      } else {
         for($i =0; $row = sql_fetch_array($result); $i++) { ?>
            <tr>
               <td><?=$row['od_time']?></td>
               <td><?=number_format($row['amount'])?>개</td>
               <td>
                  <?
                  if($row['coin_desc'] == 'REQUEST') {
                     print "<span class='charge' id='{$row['id']}' data-coin='{$row['amount']}'><strong>대기중</strong></span>";
                  } else { 
                     print "충전완료";
                  }        
                  ?>
               </td>
            </tr>
      <? }
      } ?>
   </table>
   <form id='charge_form' action="do.php" method="POST">
      <input type='hidden' name='section' value='btb'/>
      <input type='hidden' name='cpy_admin' value='<?=$cpy_admin?>'/>
      <input type='hidden' name='mode' value='charge'/>
      <input type='hidden' name='id' />
      <input type='hidden' name='amount' />
      <? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
      <input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>
   </form>
</div>

</div>