<?php
// 페이지 추가
// 2013. 08. 14.
// 진입명
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$query = "SELECT it_id, it_name, it_fundraiser
		  FROM wegen_campaign
		  WHERE it_isEnd != 1
		  ORDER BY it_id DESC";
		  
$res = mysql_query($query) or die(mysql_error());
while ($c[] = mysql_fetch_array($res, MYSQL_ASSOC));
array_pop($c);

$query = "SELECT od_method
		  FROM wegen_donation
		  WHERE od_method NOT LIKE '%신용카드%' AND od_method != '' AND od_method NOT LIKE '%자동%' 
		  GROUP BY od_method
		  ORDER BY od_method ASC";

$res = mysql_query($query) or die(mysql_error());
while ($m[] = mysql_fetch_array($res, MYSQL_ASSOC));
array_pop($m);
// print_r($row);
?>
<script type='text/javascript' src='/js/zipsearch.js'></script>
<script>
jQuery(document).ready(function($){
	$('#od_receiptPno').hide();
	$('#od_receiptName').hide();

  	$('#od_info').autocomplete({
  		source:'list.users.json.php',
  		minLength:2,
  		select: function (evt, ui) {  		
  			this.form.mb_no.value = ui.item.mb_no;
  			this.form.od_name.value = ui.item.mb_name;
  			this.form.od_hp.value = ui.item.mb_contact;
  			this.form.od_zip1.value = ui.item.mb_zip1;
  			this.form.od_zip2.value = ui.item.mb_zip2;
  			this.form.od_addr1.value = ui.item.mb_addr1;
  			this.form.od_addr2.value = ui.item.mb_addr2;
  			this.form.no.value = ui.item.mb_no;
  		}
  	});

	$('#register').click(function () {
		if ($('#od_info').val() === '') {
			alert('이름을 입력해 주세요.');
			return;
		}

		if ($('#od_zip1').val() === '' || $('#od_zip2').val()) {
			alert('주소를 입력해 주세요.');
			return;
		}

		if ($('#od_hp').val() === '') {
			alert('전화번호를 입력해 주세요.');
			return;
		}

		if ($('#od_amount').val() === '' || Number($('#od_amount').val()) <= 0) {
			alert('금액을 입력해 주세요.');
			return;
		}

		$('#thisform').submit();
	});

	$('input[name=od_isReceipt]').bind('click', function () {
		$('#od_receiptName').val('');
		$('#od_receiptName').hide();
		$('#od_receiptName').attr('placeholder', '');
		$('#od_receiptPno').val('');
		$('#od_receiptPno').hide();
		$('#od_receiptPno').attr('placeholder', '');

		if ($(this).val() == '1') {
			$('#od_receiptName').show();
			$('#od_receiptName').attr('placeholder', '이름');
			$('#od_receiptPno').show();
			$('#od_receiptPno').attr('placeholder', '주민등록번호');
		} else if ($(this).val() == '2') {
			$('#od_receiptName').show();
			$('#od_receiptName').attr('placeholder', '회사명');
			$('#od_receiptPno').show();
			$('#od_receiptPno').attr('placeholder', '사업자등록번호');
		}
	});
  });
</script>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

	<h2>기부금 수동 등록</h2>

	<form id="thisform" name='thisform' enctype='multipart/form-data' method='post' action='do.php' autocomplete="off">
		<input type='hidden' name='section' value='donation'/>
		<input type='hidden' name='mode' value='insert'/>
		<input type='hidden' name='no' value=''/>
		<input type='hidden' name='ordr_idxx' value='<?=date('ymdHis').rand(100, 999)?>' />
		<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
		<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

		<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
			<tr>
				<th style='width: 120px'>후원방법</th>
				<td colspan='5' style='padding-left: 10px'>
					<select type="text" name="od_method" id="od_method">
					<?php foreach ($m as $value) :?>
						<option value="<?php echo $value['od_method']; ?>"><?php echo $value['od_method']; ?></option>
					<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<th style='width: 120px'>캠페인명</th>
				<td colspan='5' style='padding-left: 10px'>
					<select type="text" name="it_info" id="it_info">
					<?php foreach ($c as $value) :?>
						<option value="<?php echo $value['it_id'].','.$value['it_fundraiser']; ?>"><?php echo $value['it_name']; ?></option>
					<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<th style='width: 120px'>이름</th>
				<td colspan='5' style='padding-left: 10px'>
					<input type='text' id='od_info' name='od_info' value='' style='width: 95%' />
					<input type='hidden' id='od_name' name='od_name' value='' style='width: 95%' />
					<input type='hidden' id='mb_no' name='mb_no' value=''/>
				</td>
			</tr>
			<tr>
				<th style='width: 120px'>회원 정보</th>
				<td colspan='5' style='padding-left: 10px'>
					<lable>핸드폰:</label>
					<input type='text' name='od_hp' value=''/>
					<br>
					<lable>우편번호:</label>
					<input type='text' style="width:30px;" name='od_zip1' value=''/>
					<lable>-</label>
					<input type='text' style="width:30px;" name='od_zip2' value=''/>
					<lable>주소:</label>
					<input type='text' style="width:50%;" name='od_addr1' value=''/>
					<input type='text' style="text-align:left" name='od_addr2' value=''/>
					<br>
					<div class='zipcode-finder'>
						<label>주소 검색: </label><input type='text' id="dongName" />
						<input type='button' class='zipcode-search' value='검색' />
						<div class="zipcode-search-result"></div>
					</div>
				</td>
			</tr>	
			<tr>
				<th style='width: 120px'>기부금 영수증</th>
				<td colspan='5' style='padding-left: 10px'>
					<input type='radio' id='od_isReceipt' name='od_isReceipt' value='0' checked /><label>미신청</label>
					<input type='radio' id='od_isReceipt' name='od_isReceipt' value='1' /><label>개인</label>
					<input type='radio' id='od_isReceipt' name='od_isReceipt' value='2' /><label>사업자</label>
					<input type='text' id='od_receiptName' name='od_receiptName' value='' />
					<input type='text' id='od_receiptPno' name='od_receiptPno' value='' />
				<td>
			</tr>
			<tr>
				<th style='width: 120px'>이벤트 참여</th>
				<td colspan='5' style='padding-left: 10px'>
					<input type='radio' name='od_isEvent' value='1' checked /><label>선택</label>
					<input type='radio' name='od_isEvent' value='0' /><label>미선택</label>
				<td>
			</tr>		
			<tr>
				<th style='width: 120px'>후원금액</th>
				<td colspan='5' style='padding-left: 10px'>
					<input type='text' id='od_amount'  name='od_amount' value='' style='width: 95%' />
				</td>
			</tr>
		</table>

		<div style='text-align: center; margin-top: 30px'>
			<input type='button' value='등록하기' id="register" class='submit'/>
			<input type='button' value='취소' class='submit-red' onclick="javascript: window.location.href = 'donation.php';"/>			
		</div>
	</form>

</div>
