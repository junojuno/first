<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$data = sql_fetch("SELECT * FROM ".DB_INFO." LIMIT 1");
?>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>환경설정</h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='settings'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<h4 style='margin-top: 30px'>기본 정보</h4>
<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>대표</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_ceo' value='<?=$data[opt_ceo]?>' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>회사 주소</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_address' value='<?=$data[opt_address]?>' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>회사 전화번호</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_tel' value='<?=$data[opt_tel]?>' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>회사 팩스번호</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_fax' value='<?=$data[opt_fax]?>' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>이메일</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_email' value='<?=$data[opt_email]?>' style='width: 300px' />
	</td>
</tr>
<tr>
   <th style='width: 120px'>사업자등록번호</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_corp_num' value='<?=$data[opt_corp_num]?>' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>통신판매업신고번호</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_comm_num' value='<?=$data[opt_comm_num]?>' style='width: 300px' />
	</td>
</tr>
</table>

<h4 style='margin-top: 30px'>후원 문자 관리</h4>
<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>대표 전화번호</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='opt_sms_num' value='<?=$data[opt_sms_num]?>' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>감사 문자 (기본)</th>
	<td colspan='5' style='padding-left: 10px'>
   <input type='text' name='opt_sms' value='<?=$data[opt_sms] == '' ? '[위제너레이션] 기부에 참여해주셔서 정말 감사합니다♥ 행복한 하루되세요!' : $data[opt_sms]?>' style='width: 50%' />
	<span class='bytes'></span>/80 bytes
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='적용' class='submit' />
</div>
</form>

</div>

<script>
$.fn.getBytes = function() {
	return this.each(function() {
		var str = $(this).val();
		var b = str.match(/[^\x00-\xff]/g);
		$(this).parent('td').children('.bytes').html( str.length + (!b ? 0: b.length) );
	});
}

$(document).ready(function() {
	$('input[name=opt_sms]').getBytes().keyup(function() {
		$(this).getBytes();
	});
});
</script>
