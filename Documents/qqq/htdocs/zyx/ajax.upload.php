<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	header("HTTP/1.0 404 Not Found");
//	exit;
}

$_required = true;
include '../config.php';
include '../module/class.upload.php';

sec_session_start();

if (array_search($_SESSION[user_no], $settings[admin]) === false) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$dir = $_REQUEST[from] == 'notice' ? '../data/notices/' : '../data/postscripts/';

if ($_FILES['file']) {
	$handle = new upload($_FILES['file']);
	if ($handle->uploaded) {
		$salted = md5(date('YmdHis'));
		$handle->forbidden				= array('application/*');
		$handle->allowed				= array('image/*');
		$handle->image_convert			= 'jpg';
		$handle->file_new_name_body		= $salted;
		$handle->file_overwrite			= true;
		$handle->process($dir);

		if ($handle->processed) {
			$handle->clean();

         $array = array('filelink' => "http://$_SERVER[HTTP_HOST]".substr($dir, 2).$salted.".jpg");
			echo stripslashes(json_encode($array));   

		} else {
			echo 'error : ' . $handle->error;
		}
	}
}


/*
$_FILES['file']['type'] = strtolower($_FILES['file']['type']);

if ($_FILES['file']['type'] == 'image/png' || $_FILES['file']['type'] == 'image/jpg' || $_FILES['file']['type'] == 'image/gif' || $_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/pjpeg')
{
    // setting file's mysterious name
    $filename = md5(date('YmdHis')).'.jpg';
    $file = $dir.$filename;

    // copying
    copy($_FILES['file']['tmp_name'], $file);

    // displaying file    
    $array = array(
        'filelink' => '/data/postscripts/'.$filename
    );

    echo stripslashes(json_encode($array));   

}

?>
