<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'pt_no';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>파트너 관리</h2>

<div>
<a href='./partners.form.php'><button style='float: right' class='button'>파트너 추가</button></a>
&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 100px'>파트너ID</th>
	<th style='width: 200px'>로고</th>
	<th>파트너명</th>
	<th style='width: 300px'>홈페이지</th>
   <th style='width: 70px;'>영문버전</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_PARTNERS." $where ORDER BY $order $sort";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
   $isEn = ($data['is_en'] == 1) ? 
      "<button class='btn btn-warning btn-small' onclick=\"location.href='./partners.form.en.php?no={$data['pt_no']}&mode=edit'\">수정</button>" : 
      "<button class='btn btn-primary btn-small' onclick=\"location.href='./partners.form.en.php?no={$data['pt_no']}&mode=insert'\">추가</button>";
?>
<tr>
	<td data-href='./partners.form.php?no=<?=$data[pt_no]?>' style='font-size: 10px; text-align: center'><?=$data[pt_no]?></td>
	<td data-href='./partners.form.php?no=<?=$data[pt_no]?>' style='text-align: center'><img src='/data/partners/<?=$data[pt_logo]?>' style='width: 120px; height: 60px; outline: solid 1px silver; margin: 10px 0px' /></td>
	<td data-href='./partners.form.php?no=<?=$data[pt_no]?>' style='padding-left: 10px'><?=$data[pt_name]?></td>
	<td data-href='./partners.form.php?no=<?=$data[pt_no]?>' style='font-size: 11px'><?=$data[pt_homepage]?></td>
   <td style='text-align:center;'><?=$isEn?></td>
</tr>
<? endfor; ?>
</table>

</div>