<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'wr_id';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = $_REQUEST[count] ? $_REQUEST[count] : 30;
$limit = ($page - 1) * $count;

$result = sql_fetch("SELECT COUNT(wr_id) AS cnt FROM ".DB_MEDIA.$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>언론소개 관리</h2>

<div style='text-align: right'>
<a href='./media.form.php'><button style='float: right' class='button'>글 추가</button></a>
&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 70px'>글번호</th>
	<th>제목</th>
	<th style='width: 100px'>작성일</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_MEDIA." $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
?>
<tr data-href='./media.form.php?no=<?=$data[wr_id]?>'>
	<td style='font-size: 10px; text-align: center'><?=$data[wr_id]?></td>
	<td><?=$data[wr_subject]?></td>
	<td style='font-size: 10px; text-align: center'><?=$data[wr_datetime]?></td>
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal' class='nav'>»</a> ";
?>
</span>

</div>