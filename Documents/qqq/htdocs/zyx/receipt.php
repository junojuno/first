<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'o.it_id';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = (!$_REQUEST[count]) ? (!$_REQUEST[s]) ? 30 : 999 : $_REQUEST[count];
$limit = ($page - 1) * $count;

if ($_REQUEST[s]) {
	switch ($_REQUEST[search]) {
		case 'campaign' :
			$where = " AND o.it_id = '$_REQUEST[s]' ";
			$code = "$('#search').val('campaign'); $('#campaign').show().val('$_REQUEST[s]'); $('#searchbox').hide();";
			break;
		case 'member' :
			$where = " AND o.mb_no = '$_REQUEST[s]' OR m.mb_name LIKE '%".$_REQUEST[s]."%' ";
			$code = "$('#search').val('member'); $('#searchbox').show(); $('#searchstring').val('$_REQUEST[s]');";
			break;
		default :
			$where = false;
			break;
	}
}

$result = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_ORDERS." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no WHERE isReceipt != 0 ".$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>기부금영수증 발급 관리</h2>

<div style='height: 30px'>
<a href='excel.php?section=receipt&search=<?=$_REQUEST[search]?>&s=<?=$_REQUEST[s]?>'><button class='button' style='float: right; width: auto'>현재 목록을 엑셀로 저장</button></a>
<!--<a href='excel.php?section=receipt&special=1'><button class='button' style='float:right;width:auto'>연말정산 영수증 미신청 목록 엑셀저장</button></a> -->
<select id='search' name='search'>
<option value=''>조건별 정렬</option>
<option value='campaign'>캠페인별</option>
<option value='member'>회원별</option>
</select>
<script>
$(document).ready(function() {
	<?=$code?>
	$('#search').change(function() {
		if ($('#search option:selected').val() == 'campaign') {
			$('#campaign').show();
			$('#searchbox').hide();
		}
		else if (!$('#search option:selected').val()) {
			$('#campaign').hide();
			$('#searchbox').hide();
		}
		else {
			$('#campaign').hide();
			$('#searchbox').show();
		}
	});

	$('#campaign').change(function() {
		document.location.href = './receipt.php?search=campaign&s=' + $('#campaign option:selected').val();
	});

	$('#searchbtn').click(function() {
		document.location.href = './receipt.php?search=' + $('#search option:selected').val() + '&s=' + $('#searchstring').val();
	});
});
</script>

<select id='campaign' name='campaign' style='display: none'>
<option value=''></option>
<?
$sql = "SELECT * FROM ".DB_CAMPAIGNS." ORDER BY it_id DESC";
$result = sql_query($sql);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
?>
<option value='<?=$data[it_id]?>'><?=$data[it_name]?></option>
<? endfor; ?>
</select>

<span id='searchbox' style='display: none'>
<input id='searchstring' type='text' name='s' />
<button id='searchbtn'>검색</button>
</span>

</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>후원코드<br/>(후원일자)</th>
	<th style='width: 100px'>회원명</th>
	<th>캠페인명</th>
	<th style='width: 100px'>후원액수</th>
   <th style='width: 70px'>종류</th>
	<th style='width: 150px'>주민등록번호/사업자</th>
	<th style='width: 200px'>이메일</th>
	<th style='width: 150px'>연락처</th>
	<th style='width: 70px'>발급여부</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_ORDERS." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id WHERE isReceipt != 0 and o.od_amount - o.pay_remain > 0 $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$zip = $data[mb_zip1] ? '('.$data[mb_zip1].'-'.$data[mb_zip2].') ' : false;
   $apply = $data[subReceipt] == 1 ? '신청' : '미신청';
   $kind = $data[isReceipt] == 1 ? '개인' : '사업자';
?>
<tr>
	<td style='font-size: 10px; text-align: center'><?=$data[od_id]?><br/>(<?=$data[od_time]?>)</td>
	<td style='text-align: center'><?=$data[receiptName]?></td>
	<td><?=$data[it_name]?></td>
	<td style='font-size: 11px; text-align: right; padding-right: 10px'><?=number_format($data[od_amount])?>원<br/>(<?=$data[od_method]?>)</td>
   <td style='font-size: 11px; text-align: center;'><?=$kind?></td>
	<td style='font-size: 11px; text-align: center'><?=$data[receiptPno]?></td>
	<td><?=$data[mb_email]?></td>
	<td style='font-size: 10px; text-align: center'><?=$data[mb_contact]?></td>
	<td style='text-align: center; <?=$data[subReceipt] == 1 ? "color: red;" : false;?>'><?=$apply?></td>
<!--	<td style='font-size: 11px'><?=$zip?><?=$data[mb_addr1]?> <?=$data[mb_addr2]?></td> -->
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>»</a> ";
?>
</span>

</div>
