<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'no';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>펀드레이저 관리</h2>

<div>
<a href='./fundraisers.form.php'><button style='float: right; width: auto' class='button'>펀드레이저 추가</button></a>
&nbsp;
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 50px'>ID</th>
	<th style='width: 80px'>사진</th>
	<th style='width: 150px'>펀드레이저명</th>
	<th>참여 캠페인</th>
	<th>이벤트 내용</th>
   <th style='width: 70px;'>영문버전</th>
</tr>
<?
$sql = "SELECT f.no, f.fr_logo, f.fr_name, c.it_name, f.fr_desc, f.is_en FROM ".DB_FUNDRAISERS." f LEFT JOIN ".DB_CAMPAIGNS." c ON f.it_id = c.it_id ORDER BY $order $sort";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
   $isEn = ($data['is_en'] == 1) ? 
      "<button class='btn btn-warning btn-small' onclick=\"location.href='./fundraisers.form.en.php?no={$data['no']}&mode=edit'\">수정</button>" : 
      "<button class='btn btn-primary btn-small' onclick=\"location.href='./fundraisers.form.en.php?no={$data['no']}&mode=insert'\">추가</button>";
?>
<tr>
	<td data-href='./fundraisers.form.php?no=<?=$data[no]?>' style='font-size: 10px; text-align: center'><?=$data[no]?></td>
	<td data-href='./fundraisers.form.php?no=<?=$data[no]?>' style='text-align: center'><img src='/data/partners/<?=$data[fr_logo]?>' style='width: 50px; height: 55px; outline: solid 1px silver; margin: 10px 0px' /></td>
	<td data-href='./fundraisers.form.php?no=<?=$data[no]?>' style='padding-left: 10px'><?=$data[fr_name]?></td>
	<td data-href='./fundraisers.form.php?no=<?=$data[no]?>' style='padding-left: 10px'><?=$data[it_name]?></td>
	<td data-href='./fundraisers.form.php?no=<?=$data[no]?>' style='padding-left: 10px; padding-right: 10px'><?=nl2br($data[fr_desc])?></td>
   <td style='text-align: center;'><?=$isEn?></td>
</tr>
<? endfor; ?>
</table>

</div>
