<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'od_id';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = (!$_REQUEST[count]) ? (!$_REQUEST[s]) ? 30 : 999 : $_REQUEST[count];
$limit = ($page - 1) * $count;

$where = ($_REQUEST[s]) ? " AND preferdate = '$_REQUEST[s]' " : false;

$result = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_REGULARPAYMENT." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no ".$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>정기후원내역 관리</h2>

<div style='height: 30px'>
<a href='excel.php?search=<?=$_REQUEST[search]?>&s=<?=$_REQUEST[s]?>'><button class='button' style='float: right; width: auto'>현재 목록을 엑셀로 저장</button></a>
</div>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>후원코드<br/>(후원일자)</th>
	<th style='width: 150px'>회원명</th>
	<th>내역</th>
	<th style='width: 100px'>후원액수</th>
<?/*	<th style='width: 100px'>코인사용내역</th> */?>
	<th style='width: 100px'>분배방식</th>
	<th style='width: 100px'>유효기간내<br/>미사용 잔여코인</th>
	<th style='width: 200px'>잔여코인분배</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_REGULARPAYMENT." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id WHERE o.mb_no NOT IN(339, 372) ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$zip = $data[mb_zip1] ? '('.$data[mb_zip1].'-'.$data[mb_zip2].') ' : false;

	$date = explode(' ', $data[od_time]);
	$started = substr(array_pop(array_reverse($date)), 0, -3);
	$cur = explode('-', $started);
	$now = date('Y-m');

	$type = ($data[isFirst] == '1') ? '정기후원 첫 신청' : $cur[0].'년 '.$cur[1].'월 정기결제';

	list($year, $month, $day) = explode('-', $date[0]);
	$standard = date('ymd', mktime(0, 0, 0, $month, $day, $year)) . '000000000';
	$expires = date('ymd', mktime(0, 0, 0, $month + 1, $data[preferdate], $year)) . '000000999';
	$today = date('ymd') . '235959999';

	$ssql = "SELECT SUM(amount) as amt FROM ".DB_COINS." WHERE mb_no = '$data[mb_no]' AND od_id > '$standard' AND od_id < '$expires' ";
	$check = sql_fetch($ssql);

	$coin_left = ($check[amt] < 0) ? 0 : $check[amt];
	$coin_left = ($today <= $expires) ? '-' : $coin_left;

	// 문영돈, 홍기대 첫 정기결제 관련 잔액 강제 조정
	if ($data[isFirst] == 1 && ($data[mb_no] == 361 || $data[mb_no] == 530)) $coin_left = 0;

	
	$totalcoin = sql_fetch("SELECT SUM(amount) as amt FROM ".DB_COINS." WHERE mb_no = '$data[mb_no]' ");

	$coinproc = ($coin_left > 0) ? 'asd' : "<div style='text-align: center; color: red'>분배가능 코인 없음</div>";

?>
<tr>
	<td style='font-size: 10px; text-align: center'><?=$data[od_id]?><br/>(<?=$data[od_time]?>)</td>
	<td><span title="<span class='caption'><strong>회원번호</strong> <?=$data[mb_no]?><br/><strong>연락처</strong> <?=$data[mb_contact]?><br/><strong>주소</strong> <?=$zip?><?=$data[mb_addr1]?> <?=$data[mb_addr2]?></span>"><?=$data[mb_name]?></span></td>
	<td><?=$type?></td>
	<td style='font-size: 11px; text-align: right; padding-right: 10px'><?=number_format($data[od_amount])?>원</td>
<?/*	<td style='text-align: center'><span class='btn-coin' data-href='<?=$data[mb_no]?>'>내역보기</span></td> */?>
	<td style='text-align: center'><? print ($data[coin_method] == 1) ? '추천분배' : '균등분배'; ?></td>
	<td style='text-align: center'><?=$coin_left?></td>
	<td><?=$coinproc?></td>
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal&search=".$_REQUEST[search]."&s=".$_REQUEST[s]."' class='nav'>»</a> ";
?>
</span>

</div>

<div id='coinLayer'></div>

<script>
$(document).ready(function() {
	$('.btn-coin').click(function() {

		$.ajax({
			type: "POST",
			url: "./ajax.coin.php",
			data: {
				'mb_no' : $(this).attr('data-href')
			},
			cache: false,
			success: function(result) {
				$('#coinLayer').empty().append(result);
				createLayer($('#coinLayer'));				
			}
		});
	});
});
</script>