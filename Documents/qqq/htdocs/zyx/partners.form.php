<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no] ? $_REQUEST[no] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no) {
	$data = sql_fetch("SELECT * FROM ".DB_PARTNERS." WHERE pt_no = '$no' ");
}
?>

<script>
$(document).ready(function() {
	$('#btn-delete').click(function() {
		if (confirm('정말 삭제하시겠습니까?')) {
			$('input[name=mode]').val('delete');
			document.form.submit();
		}
	});
});
</script>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>파트너 <?=$title?></h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='partner'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>파트너명</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='pt_name' value='<?=$data[pt_name]?>' style='width: 95%' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>홈페이지</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='pt_homepage' value='<?=$data[pt_homepage]?>' style='width: 95%' />
	</td>
</tr>
<? $noimg = "<span style='color: red; margin-right: 10px'>이미지 없음</span>"; ?>
<tr>
	<th style='width: 120px'>로고 이미지</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/partners/'.$data[pt_logo])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/partners/$data[pt_logo]' target='_blank' title=\"<img src='/data/partners/$data[pt_logo]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='pt_logo' style='width: 300px' />
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' />
<input id='btn-delete' type='button' class='submit' value='파트너 삭제' style='margin-left: 30px' />
</div>
</form>

</div>