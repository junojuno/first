<?php

$_required = true;
include '../config.php';

sec_session_start();

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
//	exit;
}
$_SESSION[token] = '';

$section = ($_REQUEST[section]) ? $_REQUEST[section] : 'donate';
$heart = false;
// 2013.08.07 added
// 101 is wegen@admin.kr
if (array_search($_SESSION[user_no], $settings[admin]) === false) {
// 2013.08.07 annotated
// 339 is 심정우
//if ($_SESSION[user_no] != '339') {
//	alert('관리자 로그인이 필요합니다.');
	exit;
}

if (PHP_SAPI == 'cli')
	die('This code should run from a Web Browser');

require_once '../module/excel/PHPExcel.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Aquatype")
							 ->setLastModifiedBy("Aquatype")
							 ->setTitle("WEGEN DONATIONS LIST")
							 ->setSubject("WEGEN DONATIONS LIST")
							 ->setDescription("Transaction list")
//							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Result");

if ($section == 'donate') {
	if ($_REQUEST[s]) {
		switch ($_REQUEST[search]) {
			case 'campaign' :
				$where = " WHERE o.it_id = '$_REQUEST[s]' ";
				if($_REQUEST['s'] == '1372771180' || $_REQUEST['s'] == '1372609576' || $_REQUEST['s'] == '1372573236') {
					$heart = true;
				}
				break;
			case 'date' :
				$where = " WHERE o.od_time LIKE '%".$_REQUEST[s]."%' ";
				break;
			case 'member' :
				$where = " WHERE o.mb_no = '$_REQUEST[s]' ";
				break;
			default :
				$where = false;
				break;
		}
	}
   $order = "od_time DESC";
   if($_REQUEST[kara_fan] == '1') {$where = " WHERE o.it_id = '$SPECIAL[kara_fan]' AND pay_remain = 0 "; $order = "od_time ASC";}
	$sql = "SELECT * FROM ".DB_ORDERS." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id $where ORDER BY $order";
	$result = sql_query($sql);
	if($heart) {
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', '후원코드')
				->setCellValue('B1', '후원일자')
				->setCellValue('C1', '회원명')
				->setCellValue('D1', '캠페인명')
				->setCellValue('E1', '접속수단')
				->setCellValue('F1', '후원금액')
				->setCellValue('G1', '결제수단')
				->setCellValue('H1', '이벤트')
				->setCellValue('I1', '리워드')
				->setCellValue('J1', '연락처')
				->setCellValue('K1', '사번')
				->setCellValue('L1', '이메일')
				->setCellValue('M1', '주소');
   } else if($_REQUEST[kara_fan] == '1') {
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', '후원코드')
				->setCellValue('B1', '후원일자')
				->setCellValue('C1', '회원명')
				->setCellValue('D1', '캠페인명')
				->setCellValue('E1', '접속수단')
				->setCellValue('F1', '후원금액')
				->setCellValue('G1', '결제수단')
				->setCellValue('H1', '연락처')
				->setCellValue('I1', '이메일')
				->setCellValue('J1', '주소')
				->setCellValue('K1', '사이트')
				->setCellValue('L1', '좌석');
            

   } else {
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', '후원코드')
				->setCellValue('B1', '후원일자')
				->setCellValue('C1', '회원명')
				->setCellValue('D1', '캠페인명')
				->setCellValue('E1', '접속수단')
				->setCellValue('F1', '후원금액')
				->setCellValue('G1', '결제수단')
				->setCellValue('H1', '이벤트')
				->setCellValue('I1', '리워드')
				->setCellValue('J1', '연락처')
				->setCellValue('K1', '이메일')
				->setCellValue('L1', '주소');
	}
	
	$sIndex = 3;
	for ($i = 0; $data = sql_fetch_array($result); $i++) {

		$isEvent = ($data[od_isEvent] != '없음') ? ($data[od_isEvent] != '미참여') ? "참여" : "미참여" : '-';
		if (!$data[od_isEvent]) $isEvent = "미참여";
		$check = sql_fetch("SELECT no FROM ".DB_FUNDRAISERS." WHERE it_id = '$data[it_id]' ");
		$isEvent = ($check[no] > 0) ? $isEvent : '-';
		$zip = $data[mb_zip1] ? '('.$data[mb_zip1].'-'.$data[mb_zip2].') ' : false;
		if($heart) {
			$cn = "";
			if($data[mb_type] == 44) {
				$cn = explode('@',$data['mb_id']);
				$cn = $cn[0];
			}
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$sIndex, $data[od_id])
					->setCellValue('B'.$sIndex, $data[od_time])
					->setCellValue('C'.$sIndex, $data[od_name])
					->setCellValue('D'.$sIndex, $data[it_name])
					->setCellValue('E'.$sIndex, $data[connect])
					->setCellValue('F'.$sIndex, number_format($data[od_amount]-$data[pay_remain]))
					->setCellValue('G'.$sIndex, $data[od_method])
					->setCellValue('H'.$sIndex, $isEvent)
					->setCellValue('I'.$sIndex, $data['reward'])
					->setCellValue('J'.$sIndex, $data[mb_contact])
					->setCellValue('K'.$sIndex, $cn)
					->setCellValue('L'.$sIndex, $data[mb_email])
					->setCellValue('M'.$sIndex, $zip.$data[mb_addr1].$data[mb_addr2]);
      } else if($_REQUEST[kara_fan] == '1') {
         $site = $data[site_code] == 'A001' ? 'DSPZONE' : '위젠';
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$sIndex, $data[od_id])
					->setCellValue('B'.$sIndex, $data[od_time])
					->setCellValue('C'.$sIndex, $data[mb_name])
					->setCellValue('D'.$sIndex, $data[it_name])
					->setCellValue('E'.$sIndex, $data[connect])
					->setCellValue('F'.$sIndex, number_format($data[od_amount]-$data[pay_remain]))
					->setCellValue('G'.$sIndex, $data[od_method])
					->setCellValue('H'.$sIndex, $data[mb_contact] )
					->setCellValue('I'.$sIndex, $data[mb_email])
					->setCellValue('J'.$sIndex, $zip.$data[mb_addr1].$data[mb_addr2])
					->setCellValue('K'.$sIndex, $site)
					->setCellValue('K'.$sIndex, $data[var_1]);
      } else {
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$sIndex, $data[od_id])
					->setCellValue('B'.$sIndex, $data[od_time])
					->setCellValue('C'.$sIndex, $data[mb_name])
					->setCellValue('D'.$sIndex, $data[it_name])
					->setCellValue('E'.$sIndex, $data[connect])
					->setCellValue('F'.$sIndex, number_format($data[od_amount]-$data[pay_remain]))
					->setCellValue('G'.$sIndex, $data[od_method])
					->setCellValue('H'.$sIndex, $isEvent)
					->setCellValue('I'.$sIndex, $data['reward'])
					->setCellValue('J'.$sIndex, $data[mb_contact])
					->setCellValue('K'.$sIndex, $data[mb_email])
					->setCellValue('L'.$sIndex, $zip.$data[mb_addr1].$data[mb_addr2]);
		}
		
		$sIndex++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Transactions');

	$objPHPExcel->setActiveSheetIndex(0);

} else if($section === 'member') {
   $where = $_REQUEST[s] ? " WHERE mb_type != 44 AND mb_no = '$_REQUEST[s]' OR mb_name LIKE '%".$_REQUEST[s]."%' OR mb_id LIKE '%".$_REQUEST[s]."%'" : " WHERE mb_type != 44";
   $sql = "SELECT * FROM ".DB_MEMBERS." $where ORDER BY mb_no DESC";
   $result = sql_query($sql);
   $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A1', '회원번호')
         ->setCellValue('B1', '분류')
         ->setCellValue('C1', '이름')
         ->setCellValue('D1', '이메일')
         ->setCellValue('E1', '연락처')
         ->setCellValue('F1', '총 기부횟수')
         ->setCellValue('G1', '총 기부액수')
         ->setCellValue('H1', '가입일');
   $sIndex = 2;
   for($i =0; $data = sql_fetch_array($result); $i++) {
      $memtype = array_pop(array_reverse(explode('@', $data[mb_id]))) != 'fb' ? array_pop(array_reverse(explode('@', $data[mb_id]))) != 'tw' ? '일반회원' : '소셜(TW)' : '소셜(FB)';
      $check = sql_fetch("SELECT COUNT(od_id) AS cnt, SUM(od_amount)-SUM(pay_remain) AS amt FROM ".DB_ORDERS." WHERE mb_no = '$data[mb_no]' and od_amount-pay_remain > 0 ");
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$sIndex, $data[mb_no])
            ->setCellValue('B'.$sIndex, $memtype)
            ->setCellValue('C'.$sIndex, $data[mb_name])
            ->setCellValue('D'.$sIndex, $data[mb_email])
            ->setCellValue('E'.$sIndex, $data[mb_contact])
            ->setCellValue('F'.$sIndex, $check[cnt])
            ->setCellValue('G'.$sIndex, $check[amt]."원")
            ->setCellValue('H'.$sIndex, array_pop(array_reverse(explode(' ', $data[mb_date]))));
      $sIndex++;
   }
} else if($section === 'givex') {
   $sql = "SELECT * FROM ".DB_REGULARPAYMENT." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id WHERE isFirst = 1 and isCancel = 0 ORDER BY preferdate ASC, od_id DESC";
   $result = sql_query($sql);
   $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A1', '신청일자')
         ->setCellValue('B1', '회원명')
         ->setCellValue('C1', '후원액수')
         ->setCellValue('D1', '결제희망일')
         ->setCellValue('E1', '연락처')
         ->setCellValue('F1', '주소');
   $sIndex = 2;
   for($i =0; $data = sql_fetch_array($result); $i++) {
      $zip = $data[mb_zip1] ? '('.$data[mb_zip1].'-'.$data[mb_zip2].') ' : false;
      $started = substr(array_pop(array_reverse(explode(' ', $data[od_time]))), 0, -3);
      $now = date('Y-m');
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$sIndex, array_pop(array_reverse(explode(' ', $data[od_time]))))
            ->setCellValue('B'.$sIndex, $data[mb_name])
            ->setCellValue('C'.$sIndex, number_format($data[od_amount])."원 (".$data[od_method].")")
            ->setCellValue('D'.$sIndex, $data[preferdate]."일")
            ->setCellValue('E'.$sIndex, $data[mb_contact])
            ->setCellValue('F'.$sIndex, $zip." ".$data[mb_addr1].$data[mb_addr2]);
      $sIndex++;
   }
   
} else {
   if ($_REQUEST[s]) {
      $where = " WHERE o.isReceipt != 0 ";
      switch ($_REQUEST[search]) {
         case 'campaign' :
            $where .= " AND o.it_id = '$_REQUEST[s]' ";
            break;
         case 'date' :
            $where .= " AND o.od_time LIKE '%".$_REQUEST[s]."%' ";
            break;
         case 'member' :
            $where .= " AND o.mb_no = '$_REQUEST[s]' ";
            break;
         default :
            $where .= false;
            break;
      }
   }
   $order = " od_time ";
   if($_REQUEST[special]) {
      $where = " WHERE o.isReceipt != 0 and o.subReceipt = 0 and o.it_id IN (SELECT it_id FROM ". DB_CAMPAIGNS." b WHERE b.it_isReceipt = 1 and b.it_startdate >=20130104 and b.it_startdate <=20131108) ";
      $order = " o.it_id ";
   }
   $sql = "SELECT * FROM ".DB_ORDERS." o LEFT JOIN ".DB_MEMBERS." m ON m.mb_no = o.mb_no LEFT JOIN ".DB_CAMPAIGNS." i ON i.it_id = o.it_id AND o.od_amount - o.pay_remain > 0 $where ORDER BY $order desc";
   $result = sql_query($sql);
   $objPHPExcel->setActiveSheetIndex(0)
         ->setCellValue('A1', '후원코드')
         ->setCellValue('B1', '후원일자')
         ->setCellValue('C1', '회원명')
         ->setCellValue('D1', '캠페인명')
         ->setCellValue('E1', '후원금액')
         ->setCellValue('F1', '종류')
         ->setCellValue('G1', '주민번호/사업자번호')
         ->setCellValue('H1', '이메일')
         ->setCellValue('I1', '연락처')
         ->setCellValue('J1', '발급여부');
   $sIndex = 3;
   for ($i = 0; $data = sql_fetch_array($result); $i++) {
      $apply = $data[subReceipt] == 1 ? '신청' : '미신청';
      $kind = $data[isReceipt] == 1 ? '개인' : '사업자';
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$sIndex, $data[od_id])
            ->setCellValue('B'.$sIndex, $data[od_time])
            ->setCellValue('C'.$sIndex, $data[mb_name])
            ->setCellValue('D'.$sIndex, $data[it_name])
            ->setCellValue('E'.$sIndex, number_format($data[od_amount]))
            ->setCellValue('F'.$sIndex, $kind)
            ->setCellValue('G'.$sIndex, $data[receiptPno])
            ->setCellValue('H'.$sIndex, $data[mb_email])
            ->setCellValue('I'.$sIndex, $data[mb_contact])
            ->setCellValue('J'.$sIndex, $apply);
      $sIndex++;
   }
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="wegen_'.date("ymdHis").'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
