<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no] ? $_REQUEST[no] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no) {
	$data = sql_fetch("SELECT * FROM wegen_guide WHERE no = '$no' ");
}

$categories = array('위제너레이션', '계정설정', '후원하기', '이벤트/리워드', '정기후원', '뱃지/레벨', '캠페인신청', '기타');

?>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>위젠 가이드 <?=$title?></h2>

<form name='form' method='post' action='do.php'>
<input type='hidden' name='section' value='guide'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'/>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>카테고리</th>
	<td colspan='5' style='padding-left: 10px'>
	<select name='category'>
	<?
	for ($i = 0; $i < count($categories); $i++) {
		print "<option value='$i'>$categories[$i]</option>";
	}
	?>
	</select>
	<script>
	$(document).ready(function() {
		$('select[name=category]').val('<?=$data[category]?>');
	});
	</script>
	</td>
</tr>
<tr>
	<th style='width: 120px'>질문</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='question' style='width: 95%' value='<?=$data[question]?>' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>답변</th>
	<td colspan='5' style='padding-left: 10px'>
	<textarea name='answer' rows='10' class='text' style='width: 95%; line-height: 20px'><?=$data[answer]?></textarea>
	</td>
</tr>
<tr>
	<th style='width: 120px'>출력순서</th>
	<td colspan='5' style='padding-left: 10px'>
	<select name='listorder'>
		<option value='0'>미지정</option>
		<option value='1'>1</option>
		<option value='2'>2</option>
		<option value='3'>3</option>
		<option value='4'>4</option>
		<option value='5'>5</option>
	</select>
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' />
</div>
</form>

</div>