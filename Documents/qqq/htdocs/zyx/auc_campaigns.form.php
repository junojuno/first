<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$it_id = $_REQUEST[it_id] ? $_REQUEST[it_id] : false;
$mode = $it_id ? 'edit' : 'insert';
$title = $it_id ? '수정' : '등록';

if ($it_id) {
	$data = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id' ");
   $reward_loop = $data[reward_first_num];
}
else {
	$check = sql_fetch("SELECT MAX(it_id) AS last FROM ".DB_CAMPAIGNS);
	$it_id = $check[last] + mt_rand(10000, 200000);
}
?>
<script>
prevNum = 0;


function addAucItemBlank() {

   i = prevNum+1;

   $("input:hidden[name='auc_num']").val(i);

   var strs = "<tr class='elastic_auc_items'><th colspan='2'>Item "+i+"</th></tr>";
   strs += "<tr class='elastic_auc_items'><td class='am'>애장품명</td><td><input type='text' name='auc_name"+i+"' class='wd500' value='<?=$auc_data[auc_name]?>'></td></tr>";
   strs += "<tr class='elastic_auc_items'><td class='am'>경매기간</td><td>";
   
   strs += "<input type='text' name='auc_startdate"+i+"' class='date wd61 elastic_auc_items'> 00:00:00 ~ ";
   strs += "<input type='text' name='auc_enddate"+i+"' class='date wd61 elastic_auc_items'> 23:59:59 ";
   
   strs += "</td></tr>";
   strs += "<tr class='elastic_auc_items'><td class='am'>애장품 사진</td>";

   strs += "<td>";
   
   strs += "<input class='elastic_auc_items' type='file' name='image_auc_main"+i+"'></td></tr>";
   strs += "<tr class='elastic_auc_items'><td class='am'>입찰최소금액</td><td><input type='text' class='numonly' name='auc_min"+i+"' value='<?=($auc_data[auc_min] ? $auc_data[auc_min] : 0)?>'></td></tr>";
   strs += "<tr class='elastic_auc_items'><td class='am'>설명</td><td><textarea name='auc_desc"+i+"' class='wd500 hei70'><?=$auc_data[auc_desc]?></textarea></td></tr>";
   strs += "<tr class='elastic_auc_items'><td class='am'>SMS 축소url<span class='help' title='url 관리에서 등록한 url입력해주세요 빈칸으로 남겨두시면 http://wegen.kr/m/campaign/~ 풀url로 날라갑니다'></span></td>";
   strs += "<td class='elastic_auc_items'><input type='text' name='auc_url"+i+"' value='<?=$auc_data[auc_url]?>'</td></tr> ";
   $('#tr_item_num').after(strs);
   prevNum++;
}

function addAucItemBlanks(sel) {
   $("input:hidden[name='auc_num']").val(sel.value);
   var cnt = 0;
   $('.elastic_auc_items').each(function(){
         $(this).remove();
   });
   alert(cnt);

   for(var i=sel.value; i>0; i--) {
      
      var strs = "<tr class='elastic_auc_items'><th colspan='2'>Item "+i+"</th></tr>";
      strs += "<tr class='elastic_auc_items'><td class='am'>애장품명</td><td><input type='text' name='auc_name"+i+"' class='wd500' value='<?=$auc_data[auc_name]?>'></td></tr>";
      strs += "<tr class='elastic_auc_items'><td class='am'>경매기간</td><td>";
      
      strs += "<input type='text' name='auc_startdate"+i+"' class='date wd61 elastic_auc_items'> 00:00:00 ~ ";
      strs += "<input type='text' name='auc_enddate"+i+"' class='date wd61 elastic_auc_items'> 23:59:59 ";
      
      strs += "</td></tr>";
      strs += "<tr class='elastic_auc_items'><td class='am'>애장품 사진</td>";

      strs += "<td>";
      
      strs += "<input class='elastic_auc_items' type='file' name='image_auc_main"+i+"'></td></tr>";
      strs += "<tr class='elastic_auc_items'><td class='am'>입찰최소금액</td><td><input type='text' class='numonly' name='auc_min"+i+"' value='<?=($auc_data[auc_min] ? $auc_data[auc_min] : 0)?>'></td></tr>";
      strs += "<tr class='elastic_auc_items'><td class='am'>설명</td><td><textarea name='auc_desc"+i+"' class='wd500 hei70'><?=$auc_data[auc_desc]?></textarea></td></tr>";
      strs += "<tr class='elastic_auc_items'><td class='am'>SMS 축소url<span class='help' title='url 관리에서 등록한 url입력해주세요 빈칸으로 남겨두시면 http://wegen.kr/m/campaign/~ 풀url로 날라갑니다'></span></td>";
      strs += "<td class='elastic_auc_items'><input type='text' name='auc_url"+i+"' value='<?=$auc_data[auc_url]?>'</td></tr> ";
      $('#tr_item_num').after(strs);
   }

}



</script>


<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>

<h2>캠페인 <?=$title?></h2>
<h3>캠페인 추가 가능, 수정은 구현중.</h3>
<h3>경매 아이템 날짜는 20140713 형태로 하드타이핑.</h3>
<h3>테스트 페이지. 계속 페이지 수정,구현 진행중</h3>
<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='auc_campaign'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='it_id' value='<?=$it_id?>'/>
<input type='hidden' name='auc_num' value='0'/>
<input type='hidden' name='auc_num_before' value='0'/>

<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<h4 style='margin-top: 30px'>기본 정보</h4>
 
<!-- <input type="radio" name="type" value="3" <? print ($data[type] == '3') ? 'checked' : false; ?> />경매캠페인 -->
<? if(isset($data[type]) && $data[type] !=1 && $data[type] !=2 ) { ?>
	<input type="radio" name="type" value="<?=$data[type]?>" checked/>커스텀캠페인
<? } ?>
<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF; margin-top: 10px'>
<tr>
	<th style='width: 120px'>캠페인명</th>
	<td colspan='8' style='padding-left: 10px'>
	<input type='text' name='it_name' value='<?=$data[it_name]?>' style='width: 95%' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>짧은 캠페인명<span class='help' title="최근활동 섹션에 표시되는 내용으로, '캠페인' 문구를 제외하고 15자 내외를 권장합니다." /></th>
	<td colspan='8' style='padding-left: 10px'>
	<input type='text' name='it_shortname' value='<?=$data[it_shortname]?>' style='width: 95%' />
	</td>
</tr>
<tr style='height: 100px'>
	<th style='width: 120px'>캠페인 요약<span class='help' title='하이라이트 캠페인 하단 설명 및 페이스북 공유시 표시되는 내용으로, 150자 내외를 권장합니다. 개행금지!!' /></th>
	<td colspan='8' style='padding-left: 10px'>
	<textarea name='it_shortdesc' style='width: 95%' rows='5'><?=$data[it_shortdesc]?></textarea>
	</td>
</tr>
<tr style='height: 100px' id="test">
	<th style='width: 120px'>리워드</th>
	<td colspan='8' style='padding-left: 10px'>
   <textarea name='it_reward' style='width: 95%' rows='5'><?=$data[it_reward]?></textarea><br/>
      선착순리워드 <select name="reward_first_num">
         <option value="0">0</option>
         <option value="1">1</option>
         <option value="2">2</option>
         <option value="3">3</option>
         <option value="4">4</option>
         <option value="5">5</option>
         <option value="6">6</option>
         <option value="7">7</option>
         </select><br/>
      <table id='rf_table' style='font-size:10pt;text-align:center;'>
         <tr>
            <th style='width:103px;'>금액(~이상)</th>
            <th style='width:88px;'>인원(선착순)</th>
            <th style='width:430px;'>리워드</th>
         </tr>
      <? 
         $reward_res = sql_query("SELECT * FROM ".DB_REWARDS." WHERE it_id = '$it_id'");
         for($i =0; $reward_data = sql_fetch_array($reward_res); $i++) { ?>
         <tr class='rf_row' data-row='<?=$i?>'>
            <input type='hidden' name='rf_id[]' value="<?=$reward_data[id]?>">
            <td><input type='text' name='rf_mny[]' class='numonly' value='<?=$reward_data[reward_amount]?>' style='width:82px;'></td>
            <td><input type='text' name='rf_num[]' class='numonly' value='<?=$reward_data[reward_max]?>' style='width:41px;'></td>
            <td><input type='text' name='rf_reward[]' value='<?=$reward_data[reward_name]?>' style='width:400px;'></td>
         </tr>
      <? } ?>
      </table>
	</td>
</tr>

<tr style='height: 100px'>
	<th style='width: 120px'>경매</th>
	<td colspan='8' class='pl10 ptb10'>
      <? 
      $auc_data = sql_fetch("SELECT * FROM ".DB_AUCTION." WHERE auc_id = '$data[it_auction]'");
      ?>
      <input type="hidden" name="auc_id" value="<?=$auc_data[auc_id]?>">
   
      <table class='auction bdr0 wdp100'>
         <tr id="tr_item_num">
            <td class='am wd100'>경매 물품 개수<span class='help' title='경매 물품 개수 선택.'></span></td>
            <td>
               <select name='sel_auc_item_num' onchange="addAucItemBlanks(this)">
                  <?
                     for($i=0;$i<=15;$i++){
                        print "<option vallue=$i>$i</option>";
                     }
                  ?>
               </select>
            </td>
            <td>
               <text onclick='addAucItemBlank()'>아이템 추가</text>
               
            </td>
         </tr>

         <?
            $res = sql_query("SELECT * FROM wegen_auction WHERE it_id='$it_id' ORDER BY auc_id DESC");
            $idx = mysql_num_rows($res);
            $tmp = $idx;
            while($row = sql_fetch_array($res)){
               $strs = "<tr class='elastic_auc_items'><th colspan='2'>Item ".$idx."</th></tr>";
               $strs .= "<tr class='elastic_auc_items'><td class='am'>애장품명</td><td><input type='text' name='auc_name".$idx."' class='wd500' value='$row[auc_name]'></td></tr>";
               $strs .= "<tr class='elastic_auc_items'><td class='am'>경매기간</td><td>";
               
               $strs .= "<input type='text' name='auc_startdate".$idx."' class='date wd61 elastic_auc_items' value='$row[auc_startdate]'> 00:00:00 ~ ";
               $strs .= "<input type='text' name='auc_enddate".$idx."' class='date wd61 elastic_auc_items' value ='$row[auc_enddate]'> 23:59:59";
               
               $strs .= "</td></tr>";
               $strs .= "<tr class='elastic_auc_items'><td class='am'>애장품 사진</td>";
               $strs .= "<td>";
               
               $strs .= "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/auc_main".$idx.".jpg' target='_blank' title=\"<div style='overflow: hidden; max-height: 500px'><img src='/data/campaign/$data[it_id]/auc_main".$idx.".jpg' style='width: 100%' /></div>\">현재 이미지</a><input type='hidden' name='auc_file_pass' value='1'>";

               $strs .= "<input class='elastic_auc_items' type='file' name='image_auc_main".$idx."' value=''></td></tr>";
               $strs .= "<tr class='elastic_auc_items'><td class='am'>입찰최소금액</td><td><input type='text' class='numonly' name='auc_min".$idx."' value='$row[auc_min]'></td></tr>";
               $strs .= "<tr class='elastic_auc_items'><td class='am'>설명</td><td><textarea name='auc_desc".$idx."' class='wd500 hei70'>$row[auc_desc]</textarea></td></tr>";
               $strs .= "<tr class='elastic_auc_items'><td class='am'>SMS 축소url<span class='help' title='url 관리에서 등록한 url입력해주세요 빈칸으로 남겨두시면 http://wegen.kr/m/campaign/~ 풀url로 날라갑니다'></span></td>";
               $strs .= "<td class='elastic_auc_items'><input type='text' name='auc_url".$idx."' value='$row[auc_url]'</td></tr> ";
               echo $strs;
               $idx--;
            }
            $idx = $tmp;
         ?>

      </table>
   </td>
</tr>
<tr>
	<th style='width: 120px'>카테고리</th>
	<td colspan='8' style='padding-left: 10px'>
	<?
	$categories = array('역사/문화', '환경/동물보호', '아동/청소년', '노인', '장애인', '저소득가정', '다문화가정', '자활형 캠페인');
	$category = explode(', ', $data[category]);
   
	for ($i = 0; $i < count($categories); $i++) {
		$checked = (array_search($categories[$i], $category) !== false) ? ' checked' : false;
		print "<input type='checkbox' name='cat_".($i+1)."' $checked />$categories[$i]\n";
	}
	?>
	</td>
</tr>
<tr>
   <th>수혜자TALK<span class='help' title='검색주기가 깁니다. 검색이 안될때는 검색어친상태에서 좌우를 움직여주면됨. (cafe24 php호스팅서버에서 너무많은요청을 날리면 자동으로 로그아웃시키기때문에 일부러 길게 해놨음..)'></span></th>
   <td colspan='8' style='padding-left:10px;position:relative;'>
      <div style=''>
         <div style='float:left;margin-right:5px;display:table-cell;vertical-align:top;position:relative;'>
            <input type='text' name='member_search' style='width:350px;height:23px;margin:0;' autocomplete="off">
            <label for='ms' style='color:#b9b9b9;font-size:9pt;position:absolute;left:5px;top:6px;cursor:text;'>이메일, 이름, 연락처로 검색</label>
            <div class='_msearch' style='display:none;width:356px;background-color:white;border:1px solid #ddd;position:absolute;left:0;top:24px;'>
               <ul id='_msearch'>
               </ul>
            </div>
         </div>
         <div class='_mselect' style='display:table-cell;overflow:hidden;vertical-align:top;'>
            <ul id='_mselect' style=''>
            <? if($data[it_connectors]) {
               $connectors = explode("|", $data[it_connectors]);
               for($i =0; $i < count($connectors);$i++) {
                  $name = sql_fetch("SELECT mb_name FROM ".DB_MEMBERS." WHERe mb_no = $connectors[$i]");
                  echo "<li class='_item2' data-num='$i'>$name[mb_name]<button type='button' data-num='$i' class='del'>X</button><input type='hidden' name='it_connectors[]' value='$connectors[$i]'></li>";
               }
            } ?>
            </ul>
         </div>
      </div>
   </td>
</tr>
<tr>
	<th>시작일</th>
	<td style='padding-left: 10px'><input type='text' name='it_startdate' class='date' value='<?=$data[it_startdate]?>' readonly /></td>
	<th style='width: 120px'>마감일</th>
	<td style='padding-left: 10px'><input type='text' name='it_enddate' class='date' value='<?=$data[it_enddate]?>' readonly /></td>
</tr>
<tr>
	<th>공개여부</th>
	<td style='padding-left: 10px'><input type='checkbox' name='it_isPublic' <? print ($data[it_isPublic]) ? 'checked' : ''; ?> /></td>
	<th style='width: 120px'>진행여부</th>
	<td style='padding-left: 10px'><input type='checkbox' name='it_isEnd' <? print ($data[it_isEnd] == 0) ? 'checked' : ''; ?> /></td>
	<th style='width: 120px'>메인에 표시</th>
	<td style='padding-left: 10px'><input type='checkbox' name='it_isMain' <? print ($data[it_isMain]) ? 'checked' : ''; ?> /></td>
</tr>
<tr>
	<th style='width: 120px'>오픈캠페인</th>
	<td style='padding-left: 10px'><input type='checkbox' name='it_isOpenCampaign' <? print ($data[it_isOpenCampaign]) ? 'checked' : ''; ?> /></td>
   <th style='width:120px;'>오픈캠페인<br> 무료응모 타이틀 <span class='help' title='상세페이지 무료응모버튼 눌러서 나오는 레이아웃의 상단에 들어갈 타이틀'></th>
   <td style='padding-left:10px;' colspan='5'><textarea type='text' name='it_openCampaignTitle' style='width:550px;'><?=$data[it_openCampaignTitle]?></textarea></td>
</tr>
<tr>
   <th style='width: 120px'>오픈캠페인공유<br>이벤트명 <span class='help' title='~에 응모하셨습니다.'><span></th>
	<td style='padding-left: 10px' colspan="2"><input type='text' name='it_openCampaignEvent' value="<?=$data[it_openCampaignEvent]?>"  style='width:100%;'/></td>
</tr>
<tr>
   <th style='width: 120px'>오픈캠페인 페북<br>타이틀 <span class='help' title='페북포스트 타이틀'><span></th>
	<td style='padding-left: 10px' colspan="2"><input type='text' name='it_ocShareTitle' value="<?=$data[it_ocShareTitle]?>"  style='width:100%;'/></td>
</tr>
<tr>
   <th style='width: 120px'>오픈캠페인 페북<br>상세설명 <span class='help' title='페북포스트 상세설명'><span></th>
	<td style='padding-left: 10px' colspan="4"><textarea type='text' name='it_ocShareDesc' style='width:100%;height:50px;'><?=$data[it_ocShareDesc]?></textarea></td>
</tr>
<tr>
   <th style='width: 120px'>오픈캠페인공유<br>이미지<span></th>
	<td style='padding-left: 10px' colspan="2">
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/open_campaign_share.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/open_campaign_share.jpg' target='_blank' title=\"<img src='/data/campaign/$data[it_id]/open_campaign_share.jpg' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_open_campaign_share' style='width: 300px' />
   </td>
</tr>

<tr class='tr' style='display: <?=(($data[type]) == 2)? '' : 'none' ; ?>'>
	<th>파트너(비영리단체)<br/>발급</th>
	<td style='padding-left: 10px'><input type='checkbox' name='it_issueCheck' id='tr1' <? print ($data[it_issueCheck]) ? 'checked' : ''; ?> /></td>
	<th style='width: 120px'>사이트 발급<span class='help' title='특정 사이트에 발급받는 경우 그 사이트 정보를 적어주세요.'></th>
	<td style='padding-left: 10px'><input type='text' id='tr2' name='it_issueLoc' value='<?=$data[it_issueLoc]?>'/></td>
</tr>
</table>

<h4 style='margin-top: 30px'>이미지</h4>
<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<? $noimg = "<span style='color: red; margin-right: 10px'>이미지 없음</span>"; ?>
<tr>
   <th>(NEW) 메인 배너<span class='help' title='width:1400px, height:450px'></span></th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/main_banner.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/main_banner.jpg' target='_blank' title=\"<img src='/data/campaign/$data[it_id]/main_banner.jpg' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_main_banner' style='width: 300px' />
	</td>
</tr>
<tr>
   <th>(NEW) 메인 배너 배경색<span class='help' title='#abcdef 형식으로 적어줄것 (필수!)'></span></th>
	<td colspan='5' style='padding-left: 10px'>
   <input type='text' name='it_bg_color' style='width:300px' value="<?=$data[it_bg_color]?>"/>
	</td>
</tr>
<tr>
	<th>배너</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/banner.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/banner.jpg' target='_blank' title=\"<img src='/data/campaign/$data[it_id]/banner.jpg' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_banner' style='width: 300px' />
	</td>
</tr>
<tr>
   <th style='width: 141px'>(NEW) 리스트<span class='help' title='width:927px; height:246'></span></th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/main_list.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/main_list.jpg' target='_blank' title=\"<img src='/data/campaign/$data[it_id]/main_list.jpg' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_main_list' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 141px'>리스트</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/list.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/list.jpg' target='_blank' title=\"<img src='/data/campaign/$data[it_id]/list.jpg' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_list' style='width: 300px' />
	</td>
</tr>
<tr>
	<th>캠페인 상세</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/detail.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/detail.jpg' target='_blank' title=\"<div style='overflow: hidden; max-height: 500px'><img src='/data/campaign/$data[it_id]/detail.jpg' style='width: 100%' /></div>\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_detail' style='width: 300px' />
	</td>
</tr>
<tr>
	<th onclick="alert('asdf')">동영상 대체</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/campaign/'.$data[it_id].'/inner.jpg')) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/campaign/$data[it_id]/inner.jpg' target='_blank' title=\"<img src='/data/campaign/$data[it_id]/inner.jpg' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='image_inner' style='width: 300px' />
	</td>
</tr>
<tr>
	<th>유튜브<span class='help' title='동영상 고유 11자리 식별코드만 입력합니다. 예: 8lJXehTicS8' /></th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='it_youtube' maxlength='11' value='<?=$data[it_youtube]?>' style='width: 100px' />
	</td>
</tr>
<tr>
   <th>유튜브(감사)<select name='ya_num' style='width:40px;'>
   <?
   $it_ya = explode("|", $data[it_youtube_after]);
   $it_ya_num = count($it_ya);
   for($i =1; $i <= 10 ; $i++) {
      $selected = $i == $it_ya_num ? 'selected' : '';
      echo "<option value='$i' $selected>$i</option>";
   } ?>
   </select><span class='help' title='동영상 고유 11자리 식별코드만 입력합니다. 예: 8lJXehTicS8' />
   </th>
	<td class='ya' colspan='5' style='padding-left: 10px'>
   <? if($it_ya_num == 0) { ?>
	   <input type='text' name='it_youtube_after[]' maxlength='11' value='' data-row="0" style='width: 100px' />
   <? } else {
      for($i =0; $i < count($it_ya);$i++) {
         echo " <input type='text' name='it_youtube_after[]' maxlength='11' value='".$it_ya[$i]."' data-row='$i' style='width: 100px'/>";
      }
   } ?>
	</td>
</tr>
</table>

<h4 style='margin-top: 30px'>제휴 정보</h4>
<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>파트너</th>
	<td colspan='5' style='padding-left: 10px'>
	<select name='it_partner' style='width: 300px' />
	<option value='' style='color: red'>파트너 없음</option>
	<?
	$sql = "SELECT * FROM ".DB_PARTNERS." ORDER BY pt_no ASC";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	for ($i = 0; $row = sql_fetch_array($result); $i++) :
		$selected = ($data[it_partner] == $row[pt_no]) ? ' selected' : false;
   ?>
      <option value='<?=$row[pt_no]?>'<?=$selected?>><?=$row[pt_name]?></option>
   <? endfor; ?>
	</select>
	</td>
</tr>
<tr>
	<th style='width: 120px'>펀드레이저</th>
	<td colspan='5' style='padding-left: 10px'>
	<select name='it_fundraiser' style='width: 300px' />
	<option value='0' style='color: red'>펀드레이저 없음</option>
	<?
	$sql = "SELECT * FROM ".DB_FUNDRAISERS." ORDER BY no ASC";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	for ($i = 0; $row = sql_fetch_array($result); $i++) :
		$selected = ($data[it_fundraiser] == $row[no]) ? ' selected' : false;
      if($row['it_id'] == '') { ?>
         <option value='<?=$row[no]?>'><?=$row[fr_name]?></option>    
   <? } else if($selected) { ?>
         <option value='<?=$row[no]?>'<?=$selected?>><?=$row[fr_name]?></option>
   <? }
	endfor; ?>
	</select>
	</td>
</tr>
<tr>
	<th style='width: 120px'>스폰서</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='it_temp' style='width: 200px' />
	</td>
</tr>
</table>

<h4 style='margin-top: 30px'>후원 관리</h4>
<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>후원 감사 문자</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='it_sms' value='<?=$data[it_sms]?>' style='width: 50%' />
	<span class='bytes'></span>/80 bytes
	</td>
</tr>
<tr>
	<th style='width: 120px'>캠페인 성공 문자</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='it_sms_succ' value='<?=$data[it_sms_succ]?>' style='width: 50%' />
	<span class='bytes'></span>/80 bytes
	</td>
</tr>
<!--
<tr>
	<th style='width: 120px'>캠페인 완료 문자</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='it_sms_end' value='<?=$data[it_sms_end]?>' style='width: 50%' />
	<span class='bytes'></span>/80 bytes
	</td>
</tr>
-->
</table>


<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' onsubmit="alert('Q')"/>
</div>
</form>

</div>

<script>


$.fn.getBytes = function() {
	return this.each(function() {
		var str = $(this).val();
		var b = str.match(/[^\x00-\xff]/g);
		$(this).parent('td').children('.bytes').html( str.length + (!b ? 0: b.length) );
	});
}


$(document).on('focusin','li._item', function() {
   if(!$(this).hasClass('ev_focus')) {
      $(this).addClass('ev_focus');
   }
});

$(document).on('focusout','li._item', function() {
   if($(this).hasClass('ev_focus')) {
      $(this).removeClass('ev_focus');
   }
});

$(document).on('hover','li._item', function(){
   $('li._item.ev_focus').removeClass('ev_focus');
   $(this).addClass('ev_focus');

}, function() {
   $(this).removeClass('ev_focus');
});

$(document).on('click', '#_msearch li', function() {
   var len = $("#_mselect li").length;
   $("#_mselect").append("<li class='_item2' data-num='"+len+"'>"+$(this).data('name')+"<button type='button' data-num='"+len+"' class='del'>X</button> <input type='hidden' name='it_connectors[]' value='"+$(this).data('mbno')+"'></li>");
   $("#_mselect .del").click(function() {
      $("li[data-num="+$(this).data('num')+"]").remove();
   });
   $(this).remove();
});

$(document).on('click', '#_mselect .del', function() {
   $("li[data-num="+$(this).data('num')+"]").remove();
});

$(document).on('click', 'label[for=ms]', function() {
   $("input[name=member_search]").focus();
});

$(document).on('keydown','.numonly', function(e) {
   if (e.keyCode == 46 || e.keyCode == 8
         || e.keyCode == 9
         || e.keyCode == 27
         || e.keyCode == 13
         || e.keyCode == 37
         || e.keyCode == 39
         || e.keyCode == 65
         && e.ctrlKey === true) {
      return;
   } else if (e.shiftKey
         || (e.keyCode < 48 || e.keyCode > 57)
         && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
   }
});

$(document).ready(function() {
   prevNum = <?=$idx?>;
   $('select[name=sel_auc_item_num]').val(<?=$idx?>);
   $("input:hidden[name='auc_num_before']").val(<?=$idx?>);

   $("form").submit(function(){
      var tmp = $("textarea[name=it_shortdesc]").val();
      $("textarea[name=it_shortdesc]").val(tmp.replace(/\n/g,' '));
      if($("input[name=auc_switch]:checked").val() == "1") {
         if(!$("input[name=auc_name]").val()) {
            alert('애장품명을 입력해주세요');
            $("input[name=auc_name]").focus();
            return false;
         }
         if(!$("input[name=auc_startdate]").val()) {
            alert('경매시작날짜를 입력해주세요');
            $("input[name=auc_startdate]").focus();
            return false;
         }
         if(!$("input[name=auc_enddate]").val()) {
            alert('경매종료날짜를 입력해주세요');
            $("input[name=auc_enddate]").focus();
            return false;
         }
         if($("input[name=auc_startdate]").val()*1 > $("input[name=auc_enddate]").val()*1) {
            alert("경매시작날짜가 종료날짜보다 큽니다");
            $("input[name=auc_startdate]").focus();
            return false;
         }
         if(!$("input[name=image_auc_main]").val()) {
            if(!$("input[name=auc_file_pass]").val()) {
               alert('애장품 사진을 업로드 해주세요');
               $("input[name=image_auc_main]").focus();
               return false;
            }
         }
         if(!$("textarea[name=auc_desc]").val()) {
            alert('애장품 설명을 입력해주세요');
            $("textarea[name=auc_desc]").focus();
            return false;
         }
      } 
   });

	$('input[name=it_sms]').getBytes().keyup(function() {
		$(this).getBytes();
	});

   // $('input[name=sel_auc_item_num]').change(function(){
   //    alert("QQS");
   // });

	$('input:radio[name="type"]').change(function(){
      if($(this).val() == '1'){
         $('.targetinfo').each(function(){
            var text = $(this).text().replace('목표인원','목표금액');
            $(this).text(text);
         });
         $('.targetinfounit').each(function(){
            var text = $(this).text().replace('명','원');
            $(this).text(text);
         });
         $('.tr').hide();


      }
      else if($(this).val() == '2'){
         $('.targetinfo').each(function(){
            var text = $(this).text().replace('목표금액','목표인원');
            $(this).text(text);
         });
         $('.targetinfounit').each(function(){
            var text = $(this).text().replace('원','명');
            $(this).text(text);
         });
         $('.tr').show();
      }
   });

	if($('#tr1').is(":checked")){
		$('#tr2').hide();
	}
	$('#tr1').click(function(){
		if($('#tr1').is(":checked")){
			$('#tr2').hide();
		}else{
			$('#tr2').show();
		}
	});
	
   $('select[name=ya_num]').change(function(){
      var target = $(this).val()*1;
      var now = $('input[name="it_youtube_after[]"]').length*1;
      if(target > now ) {
         for(var i = now; i < target;i++) {
            $('td.ya').append(" <input type='text' class='text' name='it_youtube_after[]' maxlength='11' data-row='"+i+"' style='width:100px;'>");
         }
      } else {
         $('input[name="it_youtube_after[]"]').each(function() {
            if($(this).data('row')*1 >= target) {
               $(this).remove();
            }
         });
      }
   });

   if( 0 != <?=$reward_loop?>) {
      $('select[name=reward_first_num]').val('<?=$reward_loop?>').attr("selected", "selected");
   }
   $('select[name=reward_first_num]').change(function() {
      var target = $(this).val()*1;
      var now = $('tr.rf_row').length*1;
      if(target > now) {
         for(var i = now; i < target; i++) {
            $('#rf_table').append("<tr class='rf_row' data-row='"+i+"'>"+
               "<td><input type='text' class='text numonly' name='rf_mny[]' style='width:82px;'></td>"+
               "<td><input type='text' class='text numonly' name='rf_num[]' style='width:41px;'></td>"+
               "<td><input type='text' class='text' name='rf_reward[]' style='width:400px;'></td></tr>");
         }
      } else {
         $('tr.rf_row').each(function() {
            if($(this).data('row')*1 >= target) {
               $(this).remove();
            }
         });
      }
   });

   //input text 맨앞으로 커서옮겨지는거 방지
   $('input[name=member_search]').keydown(function(e){
      var keyCode = e.keyCode ? e.keyCode : e.which;
      if(keyCode == 38 || keyCode == 13) {
         e.preventDefault();
      }
   });
   var search_flag = false;
   $('input[name=member_search]').keyup(function(e) {
      var keyCode = e.keyCode ? e.keyCode : e.which;
      var _len = $(this).val().length;
      //down key
      if(keyCode == 40 ) {
         if($("div._msearch").css('display') == 'none') {
            $("div._msearch").show();
         } else {
            if($("li._item.ev_focus").length == 0) {
               $("li._item[data-order=0]").focusin(); 
            } else {
               var be = $("li._item.ev_focus");
               if($("li._item").length-1 != be.data('order')*1 ) {
                  be.focusout();
                  $("li._item[data-order="+(be.data('order')*1+1)+"]").focusin();
               }
            }
         }
      } 
      //up key
      else if(keyCode == 38) {
         if($("li._item.ev_focus").length != 0) {
            var be = $("li._item.ev_focus");
            if(be.data('order')*1 == 0) {
               $("div._msearch").hide();
            } else {
               be.focusout();
               $("li._item[data-order="+(be.data('order')*1-1)+"]").focusin();
            }
         }
      }
      //enter key
      else if(keyCode == 13 ) {
         if($("li._item.ev_focus").length >0 ) {
            $("li._item.ev_focus").click();
            $("div._msearch").hide();
         }
      } else {
         if( _len >= 2) {
            if(!search_flag) {
               search_flag = true;
               $("label[for=ms]").hide();
               var _str = $(this).val();
               var _except = "";
               $("input[name='it_connectors[]']").each(function() {
                  var sp = _except == "" ? "" : "|";
                  _except += sp+ $(this).val();
               });
               $.ajax({
                  type:"POST",
                  url:"/zyx/ajax.msearch.php",
                  data:{
                     s: _str,
                     except : _except
                  },
                  cache:false,
                  success: function(res) {
                     var _mem = res.split("|");
                     if(_mem[0] != '') {
                        $("#_msearch").html('');
                        for(var i =0; i < _mem.length;i++) {
                           var _data = _mem[i].split("#!#");
                           $('#_msearch').append("<li class='_item' data-order='"+i+"' data-mbno='"+_data[0]+"' data-name='"+_data[2]+"'><div style='width:35px;display:inline-block;float:left;margin-right:10px;'>"+_data[1]+"</div> "+_data[2]+" ("+_data[3]+" , "+_data[4]+", "+_data[5]+" )</li>");
                        }
                        $("div._msearch").show();
                     } else {
                        $("div._msearch").show();
                        if($("li[data-nosearch=1]").length == 0) {
                           $("#_msearch").html('');
                           $("#_msearch").append("<li class='_item' data-nosearch='1'>검색 결과가 없습니다<li>");
                        }
                     }
                     //domUpdate();
                     search_flag = false;

                  }
               });
            }
         } else {
            $("label[for=ms]").show();
            $('div._msearch').hide();
            $('#_msearch').html('');
         }

      }
   });
   $('input[name=member_search]').click(function() {
      if($("div._msearch").css('display') == 'none') {
         $("div._msearch").show();
      } else {
         $("div._msearch").hide();
      }
   });
   //domUpdate();


   $("input[name=auc_switch]").change(function() {
      if($(this).val() == '1') {
         $("table.auction input,table.auction textarea").each(function() {
            $(this).removeAttr("disabled");
         });
      } 
   <? if($auc_data[auc_id] == '') { ?>
      if($(this).val() == '0') {
         $("table.auction input, table.auction textarea").each(function() {
            if($(this).attr("type") == "radio") return;
            $(this).attr("disabled","");
         });
      }
   <? } ?>
   });

   <? if($auc_data[auc_id] == '') { ?>
   if($("input[name=auc_switch]:checked").val() == '0') {
      $("table.auction input,table.auction textarea").each(function() {
         if($(this).attr("type") == "radio") return;
         $(this).attr("disabled","");
      });
   }
   <? } ?>
});
</script>
