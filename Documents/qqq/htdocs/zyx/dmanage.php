<?php
// 페이지 추가
// 2013. 08. 13
// 진입명
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$query = "SELECT wd.od_method AS 'method', CONCAT(FORMAT(wd.od_amount, 0), '원') AS 'amount', wc.it_name AS 'camp', wd.od_name AS 'name', 
				 wd.od_hp AS 'hp', CONCAT(wd.od_addr1, wd.od_addr2) AS 'addr', DATE_FORMAT(wd.od_time,'%Y년 %m월 %d일 %h:%i %p') AS 'time',
				 wm.mb_email AS 'email', wd.isReceipt AS 'rec', wd.it_id AS 'cid' 
		  FROM wegen_campaign wc, wegen_donation wd, wegen_member wm
		  WHERE wc.it_id = wd.it_id AND wd.mb_no = wm.mb_no
		  ORDER BY wd.od_time DESC
		  LIMIT 0, 50";
		  
$res = mysql_query($query) or die(mysql_error());
while ($row[] = mysql_fetch_array($res, MYSQL_ASSOC));
array_pop($row);

// print_r($row);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>기부금 수동 관리 (최근 50개)</h2>

<div>
<a href='./addDonation.php'><button style='float: right' class='button'>기부금 등록</button></a>
</div>
<br />
<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 70px'>결제수단</th>
	<th style='width: 200px'>후원시간</th>
	<th style='width: 80px'>후원금액</th>
	<th style='width: 160px'>이름</th>
	<th style='width: 200px'>이메일</th>
	<th>켐페인명</th>
</tr>

<?php
foreach ($row as $value) :
?>
<tr>
	<td style='font-size: 10px; text-align: center;'><?=$value["method"]?></td>
	<td style='font-size: 10px; text-align: center;'><?=$value["time"]?></td>
	<td style='font-size: 10px; text-align: right; padding-right: 10px;'><?=$value["amount"]?></td>
	<td style='text-align: center'><span title="휴대폰: <?=$value['hp']?><br /> 주소: <?=$value['addr']?>"><?=$value["name"]?></td>
	<td style='text-align: left;'><?=$value["email"]?></td>
	<td style='text-align: left;'><a href="http://wegen.kr/campaign/<?=$value['cid']?>"><?=$value["camp"]?></a></td>
</tr>
<?php endforeach; ?>
</table>