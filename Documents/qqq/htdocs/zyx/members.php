<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$order = $_REQUEST[order] ? $_REQUEST[order] : 'mb_no';
$sort = $_REQUEST[sort] ? $_REQUEST[sort] : 'desc';
$page = $_REQUEST[page] ? $_REQUEST[page] : 1;
$count = $_REQUEST[count] ? $_REQUEST[count] : 30;
$limit = ($page - 1) * $count;
$where = $_REQUEST[s] ? " WHERE mb_type != 44 AND mb_no = '$_REQUEST[s]' OR mb_name LIKE '%".$_REQUEST[s]."%' OR mb_id LIKE '%".$_REQUEST[s]."%'" : " WHERE mb_type != 44";

$result = sql_fetch("SELECT COUNT(mb_no) AS cnt FROM ".DB_MEMBERS.$where);
$total = $result[cnt];
$pageTotal = ceil($total / $count);

?>

<div style='padding: 0px 20px 100px 175px'>

<h2>회원 관리 <span style='color:red;font:11pt NanumGothicBold;'>(전체회원 수 : <?=$total?>명)</span></h2>

<div>
	<form name='form' method='get'>
	<input type='hidden' name='page' value='<?=$page?>' />
	<input type='text' name='s' style='width: 100px' /> <input type='submit' value='회원 검색' />
	</form>
   <a href='excel.php?section=member'><button class='button' style='float: right; width: auto'>전체회원목록 엑셀로 저장</button></a>
</div>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal' class='nav'>»</a> ";
?>
</span>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 70px'>회원번호</th>
	<th style='width: 70px'>분류</th>
	<th>이름</th>
	<th style='width: 180px'>이메일</th>
	<th style='width: 120px'>연락처</th>
	<th style='width: 100px'>총 기부횟수</th>
	<th style='width: 100px'>총 기부액수</th>
	<th style='width: 100px'>가입일</th>
	<th style='width: 100px'>최종방문일</th>
</tr>
<?
$sql = "SELECT * FROM ".DB_MEMBERS." $where ORDER BY $order $sort LIMIT $limit, $count";
$result = sql_query($sql);
$total = mysql_num_rows($result);

for ($i = 0; $data = sql_fetch_array($result); $i++) :
	$memtype = array_pop(array_reverse(explode('@', $data[mb_id]))) != 'fb' ? array_pop(array_reverse(explode('@', $data[mb_id]))) != 'tw' ? '일반회원' : '소셜(TW)' : '소셜(FB)';
   $check = sql_fetch("SELECT COUNT(od_id) AS cnt, SUM(od_amount)-SUM(pay_remain) AS amt FROM ".DB_ORDERS." WHERE mb_no = '$data[mb_no]' and od_amount - pay_remain > 0");
?>
<tr>
	<td style='text-align: center'><?=$data[mb_no]?></td>
	<td style='font-size: 11px; text-align: center'><?=$memtype?></td>
	<td><?=$data[mb_name]?></td>
	<td style='font-size: 10px'><?=$data[mb_email]?></td>
	<td style='font-size: 10px; text-align: center'><?=$data[mb_contact]?></td>
	<td style='font-size: 11px; text-align: center'><? print ($check[cnt]) ? $check[cnt] : ''; ?></td>
	<td style='font-size: 11px; text-align: right; padding-right: 10px'><? print ($check[amt]) ? number_format($check[amt]).'원' : ''; ?></td>
	<td style='font-size: 10px; text-align: center'><?=array_pop(array_reverse(explode(' ', $data[mb_date])))?></td>
	<td style='font-size: 10px; text-align: center'></td>
</tr>
<? endfor; ?>
</table>

<span style='display: block; text-align: center; word-spacing: 10px'>
<?
$loopStarts = ($page <= 5) ? 1 : $page-5;
$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
$prev = $page - 1;
$next = $page + 1;

if ($page > 6) print "<a href='".$_SERVER[PHP_SELF]."?page=1' class='nav'>«</a> ";
if ($page != 1) print "<a href='".$_SERVER[PHP_SELF]."?page=$prev' class='nav'>‹</a>";

for ($i = $loopStarts; $i <= $loopEnds; $i++) {
	print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='".$_SERVER[PHP_SELF]."?page=$i'>$i</a> ";
}

if ($page < $pageTotal - 4) print "<a href='".$_SERVER[PHP_SELF]."?page=$next' class='nav'>›</a> ";
if ($page < 11) print "<a href='".$_SERVER[PHP_SELF]."?page=$pageTotal' class='nav'>»</a> ";
?>
</span>

</div>
