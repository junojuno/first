<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$_required = true;
include '../config.php';

sec_session_start();

if (!preg_match("/^[0-9]+$/", $_POST[mb_no])) {
	print "0009";
	exit;
}
?>

<table cellpadding='0' cellspacing='0' style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>사용일자</th>
	<th>사용분류</th>
	<th style='width: 100px'>후원액수</th>
	<th style='width: 200px'>잔여코인분배</th>
</tr>

<?
$sql = "SELECT * FROM ".DB_COINS." o
		LEFT JOIN ".DB_CAMPAIGNS." c ON (o.it_id = c.it_id)
		WHERE mb_no = '$_POST[mb_no]' AND coin_category !=4
		ORDER BY id DESC
		";
$result = sql_query($sql);
$total = mysql_num_rows($result);

$totalAmount = 0;
$coin_sum = getCoin($_SESSION[user_no]);

for ($i = 0; $row = sql_fetch_array($result); $i++) :
?>
<tr>
	<td><?=$row[od_time]?></td>
	<td><?=$row[coin_category]?></td>
	<td><?=$row[amount]?></td>
	<td><?=$row[od_id]?></td>
</tr>
<? endfor; ?>

</table>