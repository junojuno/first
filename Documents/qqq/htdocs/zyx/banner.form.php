<?
$_required = true;
include '../config.php';
include '../module/_head.admin.php';

$no = $_REQUEST[no] ? $_REQUEST[no] : false;
$mode = $no ? 'edit' : 'insert';
$title = $no ? '수정' : '등록';

if ($no) {
	$data = sql_fetch("SELECT * FROM ".DB_BANNER." WHERE bn_no = '$no' ");
}

$bn_type = array('공지 배너');

?>

<div style='width: 100%; max-width: 1024px; padding: 0px 20px 100px 175px'>
<h2>메인 배너 <?=$title?></h2>

<form name='form' enctype='multipart/form-data' method='post' action='do.php'>
<input type='hidden' name='section' value='banner'/>
<input type='hidden' name='mode' value='<?=$mode?>'/>
<input type='hidden' name='no' value='<?=$no?>'>
<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>

<table cellpadding='0' cellspacing='0' style='width: 100%; border: solid 1px #DFDFDF'>
<tr>
	<th style='width: 120px'>종류</th>
	<td colspan='5' style='padding-left: 10px'>
	<select name='bn_type'>
	<?
	for ($i = 0; $i < count($bn_type); $i++) {
		print "<option value='$i'>$bn_type[$i]</option>";
	}
	?>
	</select>
	<script>
	$(document).ready(function() {
		$('select[name=bn_type]').val('<?=$data[bn_type]?>');
	});
	</script>
	</td>
</tr>
<tr>
	<th style='width: 120px'>배너 명</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='bn_name' style='width: 95%' value='<?=$data[bn_name]?>' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>배너 이미지</th>
	<td colspan='5' style='padding-left: 10px'>
	<? if ($mode == 'edit') print (file_exists('../data/banner/'.$data[bn_image])) ? "<a style='color: blue; text-decoration: underline; margin-right: 10px' href='/data/banner/$data[bn_image]' target='_blank' title=\"<img src='/data/banner/$data[bn_image]' style='width: 100%' />\">현재 이미지</a>" : $noimg; ?>
	<input type='file' name='bn_image' style='width: 300px' />
	</td>
</tr>
<tr>
	<th style='width: 120px'>링크</hr>(있는경우)</th>
	<td colspan='5' style='padding-left: 10px'>
	<input type='text' name='bn_link' style='width: 95%' value='<?=$data[bn_link]?>' />
	</td>
</tr>
</table>

<div style='text-align: center; margin-top: 30px'>
<input type='submit' value='<?=$title?>하기' class='submit' />
</div>
</form>
</div>
