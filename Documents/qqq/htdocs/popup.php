<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>
<body>
<style>
   .popup_bottom {text-align:right;}
   .popup_exit {cursor:pointer;background-color:black;color:white;font-size:9pt;padding:4px;}
</style>
<?php
?>
<div class="popup_layout">
   <img src="/data/popup/cam_marri_popup.jpg" alt="failed to loading" data-width='600' data-height='950'>
   <div class="popup_bottom" data-height='25'>
   <span style="">
      <input type="checkbox" id="notToday"><label for="notToday" style="cursor:pointer;">오늘 하루 열지 않음</label>&nbsp;&nbsp;
      <span class="popup_exit">닫기 X</span>
   </span>
   </div>
</div>

<script>

function setCookie( name, value, expiredays ) {
   var todayDate = new Date();
   todayDate.setDate( todayDate.getDate() + expiredays );
   document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}

$(document).ready(function(){
   $(".popup_layout").css("width",$(".popup_layout img").width());
   $(".popup_bottom").css("width",$(".popup_layout img").width());
   $(".popup_exit").click(function(){
      if($("#notToday").is(":checked")) {
         setCookie("wegen_close_popup","1",1);         
         window.close();
      } else {
         window.close();
      }
   });
});
</script>
</body>
</html>
