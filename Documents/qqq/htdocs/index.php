<?
$_required = true;
include 'config.php';
include 'module/_head.php';
?>

<? include 'module/coin.php'; ?>
<div class='main_top'>
<div class='wrap'>
   <? include 'module/list.banner.new.php';?>
</div>
</div>

<div class='main_content'>
   <? if($fb_url[main_fb_url]) { ?>
      <div id='right_fb' class='ui-widget-content' style='position:absolute;top:697px;right:50%;margin-right:-845px;z-index:1;'>
         <div class="fb-post" data-href="<?=$fb_url[main_fb_url]?>" data-width="350" data-height="500"></div>
      </div>
   <? } ?>
   <div class='wrap'>
	
      <h3 class='campaign'>SPECIAL CAMPAIGN</h3>
      <div class='campaign_list'>
         <? include 'module/layout/list.main_campaign.php'; ?>
      </div>

      <h3 class='event'>SPECIAL EVENT</h3>
      <div class='event_list'>
         <? include 'module/layout/list.event.php';?>
      </div>
      <div class='event_more'>
         <img src='/images/more_btn.png' data-href='/postscript'>
      </div>

      <h3 class='fundraiser'>FUNDRAISER</h3>
      <div class='main_scrolling fundraiser'>
         <div class='prev'><</div>
         <div class='next'>></div>
         <? include 'module/layout/list.fundraisers.php'; ?>
      </div>

      <h3 class='sponsor'>SPONSOR</h3>
      <div class='main_scrolling sponsor'>
         <div class='prev'><</div>
         <div class='next'>></div>
         <? include 'module/layout/list.sponsors.php'; ?>
      </div>

      <h3 class='partner'>PARTNER</h3>
      <div class='main_scrolling partner'>
         <div class='prev'><</div>
         <div class='next'>></div>
         <? include 'module/layout/list.partners.php'; ?>
      </div>
      
   </div>
</div>
<?
include 'module/_tail.php';
?>


