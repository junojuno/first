<?
$_required = true;
include 'config.php';
include 'module/class.upload.php';

sec_session_start();

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
	exit;
}
$_SESSION[token] = '';

if ($_FILES['attfile']) {
	$handle = new upload($_FILES['attfile']);
	if ($handle->uploaded) {
//		$handle->allowed				= array('image/*');
		$handle->image_convert			= 'jpg';
		$handle->file_new_name_body		= 'req_'.$_POST['token'];
		$handle->process('data/userupload/');

		if ($handle->processed) {
			$fileuploaded = $handle->file_dst_pathname;
			$handle->clean();
		} else {
			echo 'error : ' . $handle->error;
		}
	}
}

if ($_POST[mailtype] == 'request') {

	$_t = 'seoyoung@wegen.kr';

	$_m = "
	단체명 : $_POST[req_name] \n\n
	단체 소개 : $_POST[req_about] \n\n
	담당자명 : $_POST[req_charge] \n\n
	담당자 연락처 : $_POST[req_hp1] - $_POST[req_hp2] - $_POST[req_hp3] \n\n
	담당자 이메일 : $_POST[req_mail] \n\n
	캠페인 내용 : $_POST[req_desc] \n\n
	목표 모금액 : ". number_format($_POST[req_target]). " \n\n
	제공 가능한 리워드 : $_POST[req_reward]
	";

	$_s = '=?UTF-8?B?' . base64_encode('[위젠] 신규 캠페인 요청') . '?=';

}
else if ($_POST[mailtype] == 'talent') {

	$_t = 'say0602@gmail.com';

	$_m = "
	신청분야 : $_POST[tal_category] \n\n
	신청자명 : $_POST[tal_name] \n\n
	자기소개 : $_POST[tal_about] \n\n
	신청자 연락처 : $_POST[tal_hp1] - $_POST[tal_hp2] - $_POST[tal_hp3] \n\n
	신청자 이메일 : $_POST[tal_mail] \n\n
	재능기부 내용 : $_POST[tal_desc]
	";

	$_s = '=?UTF-8?B?' . base64_encode('[위젠] 재능기부 신청') . '?=';

}
else if ($_POST[mailtype] == 'inquiry') {

	$_t = 'say0602@gmail.com';
	$_POST[inq_desc] = str_replace("\\r\\n","\n",$_POST[inq_desc]);
	$_m = "* 문의자명 : $_POST[inq_name] \n\n* 이메일 : $_POST[inq_mail] \n\n * 문의내용 :\n$_POST[inq_desc]";

	$_s = '=?UTF-8?B?' . base64_encode("[1:1문의] $_POST[inq_subject]") . '?=';

}
else if ($_POST[mailtype] == 'code') {
	$_t = $_POST[email_confirm];
	$_m = $_POST[content];
	$_s = '=?UTF-8?B?' . base64_encode("[위젠 GIVEx] $_POST[subject]") . '?=';
}

if ($fileuploaded) {
	$_m .= "\n\n첨부파일 다운로드 : http://wegen.kr/".$fileuploaded;
}

//$_m = wordwrap($_m, 70);

// To send HTML mail, the Content-type header must be set
//$_h  = 'MIME-Version: 1.0' . "\r\n";
//$_h .= 'Content-type: text/html; charset=utf-8' . "\r\n";
$_h .= 'Content-type: text/plain; charset=utf-8' . "\r\n";

// Additional headers
//$_h .= 'To: '.$name.' <'.$to.'>' . "\r\n";
$_h .= ($_POST[mailtype] == 'code') ? "From: ".$_POST[username]." <".$_POST[email].">" . "\r\n" : 'From: WEGEN <noreply@wegen.kr>' . "\r\n";
//$_h .= 'Cc: birthdayarchive@example.com' . "\r\n";
//$_h .= 'Bcc: birthdaycheck@example.com' . "\r\n";

mail($_t, $_s, $_m, $_h);
$returnto = ($_POST[mailtype] != 'inquiry') ? ($_POST[mailtype] == 'code') ? '/' : '/request/' : '/about/inquiry';
$msg = ($_POST[mailtype] == 'code') ? '초대장을 성공적으로 발송했습니다.' : '해당 내용이 성공적으로 접수되었습니다.';
alert($msg, $returnto);
?>