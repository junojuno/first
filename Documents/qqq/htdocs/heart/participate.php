<?php
$_required = false;
include '../config.php';

if(!isset($_GET['type']) || ($_GET['type'] != 100 && $_GET['type'] != 101 && $_GET['type'] != 102)) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title>제일모직 HEART 캠페인 - 위제너레이션</title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.0' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='/js/scripts.js'></script>
<script type='text/javascript'>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-34058829-1']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<style>
.h_content {
	width:760px;
	margin:20px auto 50px auto;
}

.h_unit {
	margin-bottom:35px;
	text-align:center;
	width:100%;
}

.format {
	width:332px;
	margin:0 auto;
	padding:40px 30px;
	background-color:#f3f4e4;
}
.h_unit table td,
.h_unit table th {
	height:40px;
	border:0;
}

.h_unit table th {
	width:100px;
}

input.text {
	width:180px;
	height:22px;
}

</style>
<script>
$(document).ready(function(){
	$('#submit_btn').click(function(){
		if($('input[name=person_name]').val() == '') {
			qAlert('사원명을 입력해주세요');
			$('input[name=person_name]').focus();
			return false;
		}
		if($('input[name=person_number]').val() == '') {
			qAlert('사번을 입력해주세요');
			$('input[name=person_number]').focus();
			return false;
		}
		if($('input[name=person_mobile_1]').val() == '' || $('input[name=person_mobile_2]').val() == '' || $('input[name=person_mobile_3]').val() == '' ) {
			qAlert('핸드폰 번호를 입력해주세요');
			return false;
		}
		if($('input[name=person_comment]').val() == '') {
			qAlert('아동들에게 응원 한마디를 남겨주세요 ^^!');
			$('input[name=person_comment]').focus();
			return false;
		}
		
		$('#participate_form').submit();
	});
});
</script>
</head>

<body>
<div class='h_content'>
	<div class='h_unit'>
		<div style='width:750px;height:101px;cursor:pointer' onclick="location.href='/heart/detail/<?=$_GET['type']?>'">
			<img src='/images/heart/<?=$_GET['type']?>_title.png'/>
		</div>
	</div>
	<form id='participate_form' action='/heart/result' method='post'>
		<input type='hidden' name='type' value='<?=$_GET['type']?>'/>
		<div class='h_unit' style='margin-top:65px;'>
			<div class='format'>
				<table>
					<tr>
						<th>사원명</th>
						<td><input type='text' class='text' name='person_name'/></td>
					</tr>
					<tr>
						<th>사번</th>
						<td><input type='text' class='text numonly' name='person_number'/></td>
					</tr>
					<tr>
						<th>핸드폰번호</th>
						<td><input type='text' class='text numonly' name='person_mobile_1' maxlength='3' style='width:44px;'/> - 
						<input type='text' class='text numonly' name='person_mobile_2' maxlength='4' style='width:47px;'/> - 
						<input type='text' class='text numonly' name='person_mobile_3' maxlength='4' style='width:47px;'/></td>
					</tr>
					<tr>
						<td colspan='2' style='text-align:left;padding-left:22px;height:13px;'>
							<span style='font:8pt NanumGothic;color:#9b9b9b;font-weight:bold;'>*이벤트 당첨시 연락드리기 위해 연락처를 받습니다</span>
						</td>
					</tr>
					<tr>
						<th>이메일</th>
						<td><input type='text' class='text' name='person_email'/></td>
					</tr>
				</table>
				
				<div style="height:40px;margin-top:20px;width:254px;background-image:url('/images/heart/participate_comment.png');"></div>
				<textarea name='person_comment' class='text' style='height:70px;width:280px;margin-top:10px;'></textarea>
			</div>
		</div>
		<div class='h_unit'>
			<div id='submit_btn' style="width:212px;height:57px;margin:0 auto;display:inline-block;background-image:url('/images/heart/participate_button.png');cursor:pointer;">
			</div>
		</div>
	</form>
</div>


</body>

</html>
