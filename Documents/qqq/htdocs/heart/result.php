<?php
$_required = false;
include '../config.php';

if(!isset($_POST['person_name']) || $_POST['person_name'] =='' || 
	!isset($_POST['person_number']) || $_POST['person_number'] =='' ||
	!isset($_POST['person_mobile_1']) || $_POST['person_mobile_1'] =='' ||
	!isset($_POST['person_mobile_2']) || $_POST['person_mobile_2'] =='' ||
	!isset($_POST['person_mobile_3']) || $_POST['person_mobile_3'] =='' ||
	!isset($_POST['person_comment']) || $_POST['person_comment'] == '' ||
	!isset($_POST['type']) || ($_POST['type'] !== '100' && $_POST['type'] !== '101' && $_POST['type'] !== '102' )) {
	alert('비정상적인 접근입니다');
	exit;
}
$sql = "SELECT * FROM ".DB_CAMPAIGNS." WHERE type={$_POST['type']}";
$it = sql_fetch($sql);
$sql = "SELECT mb_no, mb_id, mb_name FROM ".DB_MEMBERS." WHERE mb_id='{$_POST['person_number']}@cii.com'";
$member = sql_fetch($sql);
if(!$member) { 
	$password = hash('sha512', $_POST['person_number']);
	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
	$password = hash('sha512', $password.$random_salt);
	$contact = "{$_POST['person_mobile_1']}-{$_POST['person_mobile_2']}-{$_POST['person_mobile_3']}";
	$sql = "INSERT INTO ".DB_MEMBERS." SET
		mb_id='{$_POST['person_number']}@cii.com',
		mb_type=44,
		mb_name='{$_POST['person_name']}',
		mb_email='{$_POST['person_email']}',
		mb_contact='{$contact}',
		mb_icon='logo_cheil.png',
		mb_password='{$password}',
		salt='{$random_salt}'
		";
	sql_query($sql);
	$member = sql_fetch("SELECT mb_no, mb_id, mb_name, mb_contact FROM ".DB_MEMBERS." WHERE mb_id='{$_POST['person_number']}@cii.com'");
} else {
	$res = sql_fetch("SELECT od_id FROM ".DB_ORDERS." WHERE mb_no={$member['mb_no']} AND it_id ='{$it['it_id']}'");
	if($res) {
		alert("{$it[it_name]} 은 이미 참여하신 캠페인입니다.");
		exit;
	}
}
$sql = "SELECT no FROM ".DB_FUNDRAISERS."
	WHERE it_id = '{$it['it_id']}'
	";
$fr = sql_fetch($sql);
$ordr_idx = date('ymdHis').rand(100, 999);
$time = date("Y-m-d H:i:s",time());
$connect = $isMobile ? '모바일' : 'PC';
$sql = "INSERT INTO ".DB_ORDERS." SET
	od_id='{$ordr_idx}',
	it_id='{$it['it_id']}',
	mb_no={$member['mb_no']},
	od_name='{$member['mb_name']}',
	od_hp='{$member['mb_contact']}',
	od_isEvent='{$fr['no']}',
	od_time='$time',
	od_method='제일모직',
	connect='{$connect}'";
sql_query($sql);

$sql = "INSERT INTO ".DB_CAMPAIGN_CMTS." SET
	cmt_category=1,
	it_id='{$it['it_id']}',
	mb_no={$member['mb_no']},
	cmt='{$_POST['person_comment']}',
	cmt_depth=0,
	parent_id=0,
	via='제일모직',
	cmt_ip = '{$_SERVER['REMOTE_ADDR']}'
	";
sql_query($sql);

$settings[type] = ($settings[type]) ? $settings[type] : 'website';
//$settings[url] = ($settings[url]) ? $settings[url] : "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
$settings[url] = ($settings[url]) ? $settings[url] : "http://wegen.kr" . $_SERVER['REQUEST_URI'];
$settings[title_page] = ($settings[title] == '') ? '위제너레이션' : $settings[title].' - 위제너레이션';
$settings[image] = ($settings[image]) ? $settings[image] : 'http://wegen.kr/images/common/logo_facebook.jpg';
$settings[desc] = ($settings[desc]) ? $settings[desc] : '스타와 함께 하는 즐거운 기부, 위제너레이션';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta property="fb:app_id" content="211534425639026" />
<meta property='og:title' content='<?=$settings[title]?>' />
<meta property='og:type' content='<?=$settings[type]?>' />
<meta property='og:url' content='<?=$settings[url]?>' />
<meta property='og:image' content='<?=$settings[image]?>' />
<meta property='og:description' content='<?=$settings[desc]?>' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title>제일모직 HEART 캠페인 - 위제너레이션</title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.0' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='/js/scripts.js'></script>
<script type='text/javascript'>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-34058829-1']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

	function postToFeed() {
		var obj = {
			method: 'feed',
			link: 'http://wegen.kr/campaign/<?=$it['it_id']?>',
			picture: 'http://wegen.kr/data/campaign/<?=$it['it_id']?>/list.jpg',
			name: '<?=$_POST['person_name']?>님이 위젠 캠페인에 후원하셨습니다.',
			caption: '스타와 함께 하는 즐거운 기부, 위젠',
			description: '[<?=$it['it_name']?>] <?=$it['it_shortdesc']?>'
		};

		function callback(response) {
			if (response) {
				procShare('<?=$it['it_id']?>', 'facebook', response['post_id']);
			}
		}
		FB.ui(obj, callback);
	}
</script>
<style>
.h_content {
	width:800px;
	margin:20px auto 50px auto;
}

.h_unit {
	margin-bottom:35px;
	text-align:center;
	width:100%;
}

.h_left_btn {
	float:left;
	width:232px;
	height:60px;
	margin-left:160px;
	background-color:orange;
}

.h_right_btn {
	float:right;
	width:232px;
	height:60px;
	margin-right:160px;
	background-color:blue;
}
</style>

</head>

<body>
<div id='fb-root'></div>
<div class='h_content'>
	<div class='h_unit' style='margin-bottom:80px;'>
		<div style='width:791px;height:110px;cursor:pointer;' onclick="location.href='/heart'">
			<img src='/images/heart/main_title.png'/>
		</div>
	</div>
	
	<div class='h_unit' style='margin-bottom:52px;'>
		<div style='margin:0 auto;'>
			<img src='/images/heart/thanks_medal.png'/>
		</div>
	</div>
	
	<div class='h_unit' style='font-size:16pt;line-height:33px;'>
		<span style='font-size:23pt;line-height:45px;font-weight:bold;'>감사합니다!</span><br/>
		<?=$_POST['person_name']?>님은 저소득 청소년 아이들에게<br/>
		꿈을 선물하셨습니다.
	</div>
	
	
	<div class='h_unit' style='height:60px;margin-top:40px;'>
		<div style='display:inline-block;cursor:pointer' onclick='postToFeed();'>
			<img src='/images/heart/thanks_share_button.png'/>
		</div>
	</div>
	
</div>
</body>

</html>

