<?php
$_required = false;
include '../config.php';

if(!isset($_GET['type']) || ($_GET['type'] != 100 && $_GET['type'] != 101 && $_GET['type'] != 102)) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$sql = "SELECT * FROM ".DB_CAMPAIGNS." WHERE type={$_GET['type']}";
$row = sql_fetch($sql);

$settings[type] = 'wegenapp:campaign';
$settings[title] = $row['it_name'];
$settings[desc] = $row['it_shortdesc'];
$settings[image] = 'http://wegen.kr/data/campaign/'.$row['it_id'].'/list.jpg';

$settings[url] = ($settings[url]) ? $settings[url] : "http://wegen.kr" . $_SERVER['REQUEST_URI'];
$settings[title_page] = ($settings[title] == '') ? '위제너레이션' : $settings[title].' - 위제너레이션';
$settings[image] = ($settings[image]) ? $settings[image] : 'http://wegen.kr/images/common/logo_facebook.jpg';
$settings[desc] = ($settings[desc]) ? $settings[desc] : '스타와 함께 하는 즐거운 기부, 위제너레이션';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta property="fb:app_id" content="211534425639026" />
<meta property='og:title' content='<?=$settings[title]?>' />
<meta property='og:type' content='<?=$settings[type]?>' />
<meta property='og:url' content='<?=$settings[url]?>' />
<meta property='og:image' content='<?=$settings[image]?>' />
<meta property='og:description' content='<?=$settings[desc]?>' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title>제일모직 HEART 캠페인 - 위제너레이션</title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.0' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='/js/scripts.js'></script>
<script type='text/javascript'>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-34058829-1']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<style>
.h_content {
	width:1000px;
	margin:20px auto 100px auto;
}

.h_unit {
	margin-bottom:16px;
	text-align:center;
	width:100%;
}

.h_left {
	float:left;
	width:700px;
	text-align:right;
	margin-left:114px;
}
.h_right {
	width:148px;
	display:inline-block;
	margin-top:38px;
}
.h_right .c_img {
	height:86px;
	background-repeat:no-repeat;
	margin-bottom:11px;
	cursor:pointer;
}
.h_right .c_txt {
	height:100px;
	width:110px;
	background-repeat:no-repeat;
}

.h_unit .m_left {
	width:177px;
}
.h_unit .m_right {
	text-align:left;
}

.h_unit .datetime {
	font:7pt NanumGothic;
	color:#c4c4c4;
	display:inline-block;
}
.h_unit .c_btn {
	width:700px;
	height:91px;
	margin-right:17px;
	display:inline-block;
	cursor:pointer;
}

.c_btn.give {
	background-image:url('/images/heart/button_give_off.png');
}

.c_btn.give:hover {
	background-image:url('/images/heart/button_give_on.png');
}

.c_img.home {
	background-image:url('/images/heart/button_home_on.png');
	background-repeat:no-repeat;
}
.c_img.home:hover {
	background-image:url('/images/heart/button_home_off.png');
	background-repeat:no-repeat;
}
.c_img.home:active {
	background-image:url('/images/heart/button_home_on.png');
	background-repeat:no-repeat;
}
.c_img.first.dream {
	background-image:url('/images/heart/button_dream_on.png');
	background-repeat:no-repeat;
}
.c_img.first.dream:hover {
	background-image:url('/images/heart/button_dream_off.png');
	background-repeat:no-repeat;
}
.c_img.first.dream:active {
	background-image:url('/images/heart/button_dream_on.png');
	background-repeat:no-repeat;
}

.c_img.first.art, .c_img.second.art {
	background-image:url('/images/heart/button_art_on.png');
	background-repeat:no-repeat;
}
.c_img.first.art:hover, .c_img.second.art:hover {
	background-image:url('/images/heart/button_art_off.png');
	background-repeat:no-repeat;
}
.c_img.first.art:active, c_img.second.art:active {
	background-image:url('/images/heart/button_art_on.png');
	background-repeat:no-repeat;
}

.c_img.second.miracle {
	background-image:url('/images/heart/button_miracle_on.png');
	background-repeat:no-repeat;
}
.c_img.second.miracle:hover {
	background-image:url('/images/heart/button_miracle_off.png');
	background-repeat:no-repeat;
}
.c_img.second.miracle:active {
	background-image:url('/images/heart/button_miracle_on.png');
	background-repeat:no-repeat;
}

</style>
<script>
function postToFeed() {
	var obj = {
		method: 'feed',
		link: 'http://wegen.kr/campaign/<?=$row['it_id']?>',
		picture: 'http://wegen.kr/data/campaign/<?=$row['it_id']?>/list.jpg',
		name: '<?=$row['it_name']?>',
		caption: '스타와 함께 하는 즐거운 기부, 위젠',
		description: '<?=addslashes($row['it_shortdesc'])?>'
	};

	function callback(response) {
		if (response) {
			procShare('<?=$row['it_id']?>', 'facebook', response['post_id']);
		}
	}
	FB.ui(obj, callback);
}

$(document).ready(function(){
	if(100 == <?=$_GET['type']?>) {
		$('.c_img.first').addClass('art');
		$('.c_img.first').click(function(){location.href='/heart/detail/101';});

		$('.c_img.second').addClass('miracle');
		$('.c_img.second').click(function(){location.href='/heart/detail/102';});
		
	} else if (101 == <?=$_GET['type']?>) {
		$('.c_img.first').addClass('dream');
		$('.c_img.first').click(function(){location.href='/heart/detail/100';});

		$('.c_img.second').addClass('miracle');
		$('.c_img.second').click(function(){location.href='/heart/detail/102';});
	} else {
		$('.c_img.first').addClass('dream');
		$('.c_img.first').click(function(){location.href='/heart/detail/100';});
	
		$('.c_img.second').addClass('art');
		$('.c_img.second').click(function(){location.href='/heart/detail/101';});
	}
});
</script>
</head>

<body>
<div id='fb-root'></div>
<div class='h_content'>
	<div class='h_unit' style='padding:10px 0;'>
		<div style='width:750px;height:101px;cursor:pointer;' onclick="location.href='/heart/detail/<?=$_GET['type']?>'">
			<img src='/images/heart/<?=$_GET['type']?>_title.png'/>
		</div>
	</div>
	
	<div class='h_unit'>
		<div class='h_left'>
			<div style='position: relative'>
				<div class='fb-like' style='position: relative; top: 5px' data-href='http://wegen.kr/campaign/?<?=$row['it_id']?>' data-send='false' data-layout='button_count' data-width='100' data-show-faces='false'></div>
				<span style='position: relative; top: 5px;margin-right:17px;'><a href="https://twitter.com/share" class="twitter-share-button" count='horizontal' data-url='http://wegen.kr/campaign/<?=$row['it_id']?>' data-lang="ko">Tweet</a></span>
				<img src='/images/campaign/btn_share.png' class='btn_facebook clickable' style='z-index: 10; position: absolute; top: 3px; right: 0px' />
			</div>
			<div style='margin-top:15px;'>
				<img src='/images/heart/<?=$row['type']?>_detail_top.jpg'/>
			</div>
			
			<div style='margin-top:15px;'>
				<div class='c_btn give' onclick="location.href='/heart/participate/<?=$_GET['type']?>'"></div>
			</div>
			
			<div style='margin-top:15px;'>
				<img style='width:100%;' src='/images/heart/<?=$row['type']?>_detail_bottom.jpg'/>
			</div>
			
			<? if($row['it_youtube'] != '') { ?>
			<div style='margin-top:15px;height:466px;background-color:black;color:white;'>
				
					<div id='movieFrame'></div>
					<script type='text/javascript'>
					
					$.getScript('https://www.youtube.com/iframe_api');
					function onYouTubeIframeAPIReady() {
						var p = new YT.Player('movieFrame',{
							videoId:'<?=$row[it_youtube]?>',
							width:'700',
							height:'466',
			//				origin:'http://wegen.kr',
							playerVars:{showinfo: 0}
						});
					}
					
					</script>
				
			</div>
			<? } ?>
			
			<div style='margin-top:15px;background-color: #F4F4E3; border: solid 2px #AFAFAF;'>
				<div style='padding: 10px'>
				<h3 style='margin: 0px; border: none;text-align:left;'>참여인원</h3>
					<?php 
					$c = sql_fetch("SELECT (count(*)) as count FROM ".DB_ORDERS." WHERE it_id='{$row['it_id']}'");
					$percent = $c['count'];
					$width = ($percent >= 100) ? 100 : $percent;
					$unit = '명';
					
					print "
					<div class='gauge'>
					<div class='progress' style='width: ${width}%'></div>
					<div class='percent'>${percent}%</div>
					</div>";
					print "
					<span class='funded' style='float:left;'>${c['count']}${unit}</span>
					<span class='targeted'>100${unit}</span>
					<div style='clear: left'></div>";
					?>
				</div>
			</div>
			
			<div style='margin-top:15px;'>
				<div class='c_btn give' onclick="location.href='/heart/participate/<?=$_GET['type']?>'"></div>
			</div>
			
			<?php 
			
			$sql = "SELECT * FROM ".DB_CAMPAIGN_CMTS." a
			LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no
			WHERE cmt_depth =0 AND it_id ='{$row['it_id']}' AND b.mb_type=44 ORDER BY cmt_time DESC";
			$res = sql_query($sql);
			
			?>
			<div style='margin-top:25px;'>
				<div style='height:55px;'>
					<img src='/images/heart/comments_campaign.png'/>
				</div>
				<table style='border:0;width:655px;margin-top:10px;margin-left:46px;'>
					<?php 
					
					for($i =0;$row = sql_fetch_array($res);$i++) {
						echo "
						<tr>
							<th class='m_left'>{$row['mb_name']} <span class='datetime'>{$row['cmt_time']}</span></th>
							<td class='m_right'>{$row['cmt']}</td>
						</tr>
						";
					}
					
					?>
					<!-- 
					<tr>
						<th class='m_left'>박재성 <span class='datetime'>2013-11-05 11:43:21</span></th>
						<td class='m_right'>힘내, 애들아. 사랑한다!</td>
					</tr>
					<tr>
						<th class='m_left'>김서영 <span class='datetime'>2013-11-05 11:43:21</span></th>
						<td class='m_right'>힘내, 애들아. 아직도 세상에는 희망이...</td>
					</tr>
					<tr>
						<th class='m_left'>홍고은 <span class='datetime'>2013-11-05 11:43:21</span></th>
						<td class='m_right'>조금이나마 도움이 되길</td>
					</tr>
					 -->
				</table>
			</div>
		</div>
		<div class='h_right'>
			<div class='c_img home' style='height:36px;' onclick="location.href='/heart';">
			</div>
			<div class='c_img first'>
			</div>
			<div class='c_img second'>
			</div>
		</div>
		<div style='clear:both;'></div>
	</div>
</div>
</body>

</html>
