<?php
$_required = false;
include '../config.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title>제일모직 HEART 캠페인 - 위제너레이션</title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.0' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='/js/scripts.js'></script>
<script type='text/javascript'>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-34058829-1']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<style>
.h_content {
	width:1000px;
	margin:40px auto 100px auto;
}

.h_unit {
	text-align:center;
	width:100%;
}

.c_unit {
	display:inline-block;
	margin-right:34px;
	width:290px;
	cursor:pointer;
	border:2px solid #cacaca;
}

.c_img {
	width:100%;
	height:230px;
}

.c_mid {
	width:100%;
	height:50px;
	padding:30px 0;
	line-height:31px;
}

.c_mid .count_f {
	font-size:24pt;
	font-weight:bold;
}
.c_mid .count_b {
	font-size:19pt;
	font-weight:bold;
}
.c_mid .title {
	color:#7c7c7c;
	font-size:11pt;
}
.c_bottom {
	font:9pt NanumGothic;
	height:80px;
	padding:10px 0;
}

.c_bottom .highlight {
	font-weight:bold;
}

.c_btn {
	display:inline-block;
	margin:11px auto 0;
}
.h_unit table td {
	height:40px;
}
.h_unit .m_left {
	width:177px;
	font-weight:bold;
}

.h_unit .m_right {
	text-align:left;
}

.h_unit .datetime {
	font:7pt NanumGothic;
	color:#c4c4c4;
	display:inline-block;
}

</style>
<script>
$(document).ready(function() {
	$('div.c_unit').hover(
		function() {
			$(this).stop().animate({
				borderColor : "#3ba423"
			}, 250);
		}, function() {
			$(this).stop().animate({
				borderColor : "#cacaca"
			}, 250);
		}
	);
});
</script>

</head>

<body>
<?php 
	$sql = "SELECT it_id, (SELECT count(*) FROM ".DB_ORDERS." WHERE it_id=a.it_id) as count FROM ".DB_CAMPAIGNS." a WHERE type=100 LIMIT 1";
	$dream_row = sql_fetch($sql);
	$sql = "SELECT it_id, (SELECT count(*) FROM ".DB_ORDERS." WHERE it_id=a.it_id) as count FROM ".DB_CAMPAIGNS." a WHERE type=101 LIMIT 1";
	$art_row = sql_fetch($sql);
	$sql = "SELECT it_id, (SELECT count(*) FROM ".DB_ORDERS." WHERE it_id=a.it_id) as count FROM ".DB_CAMPAIGNS." a WHERE type=102 LIMIT 1";
	$miracle_row = sql_fetch($sql);
?>
<div class='h_content'>
	<div style='width:791px;height:110px;cursor:pointer;' onclick="location.href='/heart';">
		<img src='/images/heart/main_title.png'/>
	</div>
	
	<div class='h_unit' style='margin-top:15px;'>
		<img src='/images/heart/main_contents_1.png'/>
	</div>
	
	<div class='h_unit' style='position:relative;padding:0 17px;margin-top:30px;'>
		<div class='c_unit' onclick="location.href='/heart/detail/100';">
			<div class='c_img'>
				<?="<img src='/data/campaign/{$dream_row['it_id']}/list.jpg'/>"?>
			</div>
			<div class='c_mid'>
				<span class='count_f'><?=$dream_row['count']?>명 </span><span class='count_b'>참여</span><br/>
				<span class='title'>저소득 청소년 학습비 지원</span>
			</div>
			<div class='c_bottom' style='background-color:#e7f4cf;'>
				추첨 20분을 삼청동에서 있을<br/>
				<span class='highlight'><u>배우 변정수의 Dream Talk</u></span>에 초대합니다<br/>
				<div class='c_btn'>
					<img src='/images/heart/button_1.png'/>
				</div>
			</div>
		</div>
		<div class='c_unit' onclick="location.href='/heart/detail/101';">
			<div class='c_img'>
				<?="<img src='/data/campaign/{$art_row['it_id']}/list.jpg'/>"?>
			</div>
			<div class='c_mid'>
				<span class='count_f'><?=$art_row['count']?>명 </span><span class='count_b'>참여</span><br/>
				<span class='title'>폭력 피해아동 미술 심리치료 지원</span>
			</div>
			<div class='c_bottom' style='background-color:#dbf0f9;'>
				추첨 25분을 삼청동에서 있을<br/>
				<span class='highlight'><u>밴드 가을방학의 미니콘서트</u></span>에 초대합니다<br/>
				<div class='c_btn'>
					<img src='/images/heart/button_2.png'/>
				</div>
			</div>
		</div>
		<div class='c_unit' onclick="location.href='/heart/detail/102';">
			<div class='c_img'>
				<?="<img src='/data/campaign/{$miracle_row['it_id']}/list.jpg'/>"?>
			</div>
			<div class='c_mid'>
				<span class='count_f'><?=$miracle_row['count']?>명 </span><span class='count_b'>참여</span><br/>
				<span class='title'>장애아동 재활병원 건립 지원</span>
			</div>
			<div class='c_bottom' style='background-color:#fbe8e8;'>
				추첨 30분을 제일모직 사옥 1층에서 있을<br/>
				<span class='highlight'><u>가수 션의 Miracle Talk</u></span>에 초대합니다<br/>
				<div class='c_btn'>
					<img src='/images/heart/button_3.png'/>
				</div>
			</div>
		</div>
	</div>
	
	<?php 
	$sql = "SELECT * FROM ".DB_CAMPAIGN_CMTS." a
		LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no
		WHERE cmt_depth =0 AND it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." WHERE type = 100 OR type = 101 OR type = 102) AND b.mb_type=44 ORDER BY cmt_time DESC";
	$res = sql_query($sql);
	?>
	<div class='h_unit' style='margin-top:50px;'>
		<div style='width:930px;height:55px;'>
			<img src='/images/heart/comment_title.png'/>
		</div>
		<table style='border:0;margin-top:10px;margin-left:44px;width:794px;'>
			<?php 
			
			for($i =0;$row = sql_fetch_array($res);$i++) {
				if($row['it_id'] == $art_row['it_id']) {
					$color = "#0096d5";
					$kind = "[HEART FOR ART]";
				} else if($row['it_id'] == $dream_row['it_id']) {
					$color = "#3ba423";
					$kind = "[HEART FOR DREAM]";
				} else {
					$color = "#da2322";
					$kind = "[HEART FOR MIRACLE]";
				}
				echo "
				<tr>
					<th class='m_left'>{$row['mb_name']} <span class='datetime'>{$row['cmt_time']}</span></th>
					<td class='m_right'><strong style='color:{$color}'>{$kind}</strong> {$row['cmt']}</td>
				</tr>
				";
			}
			
			?>
		</table>
	</div>
</div>

</body>

</html>
