<?php
   define(WEB_ROOT,$_SERVER[DOCUMENT_ROOT]);
   
   require_once WEB_ROOT.'/lib/Component/ClassLoader/UniversalClassLoader.php';

   use Symfony\Component\ClassLoader\UniversalClassLoader;
   $loader = new UniversalClassLoader();
   $loader->registerNamespaces(array(
         'Symfony\Component' => WEB_ROOT.'/lib/Component',
         'Assetic' => WEB_ROOT.'/lib/Assetic',
         'Assetic\Asset' => WEB_ROOT.'/lib/Assetic/Asset',
         'Assetic\Filter' => WEB_ROOT.'/lib/Assetic/Filter'
   ));
   $loader->useIncludePath(true);
   $loader->register();


   include WEB_ROOT.'/lib/Assetic/Asset/AssetCollection.php';
   include WEB_ROOT.'/lib/Assetic/Asset/FileAsset.php';
   include WEB_ROOT.'/lib/Assetic/Filter/Sass/ScssFilter.php';
   /*
   use Assetic\Asset\AssetCollection;
   use Assetic\Asset\FileAsset;
   use Assetic\Filter\Sass\ScssFilter;
   */

   $styles = new AssetCollection(
      array(new FileAsset(WEB_ROOT.'/scss/style.scss')),
      array(new ScssFilter())
   );
   
   header('Content-Type: text/css');
   echo $styles->dump();
?>
