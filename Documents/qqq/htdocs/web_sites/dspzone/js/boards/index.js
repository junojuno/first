$(document).ready(function() {
   var notice_scrollable = $('.notice .slides').scrollable({
      next:".notice .paging .arrow.next",
      prev:".notice .paging .arrow.prev"
   });
   var nanum_scrollable = $('.nanum .slides').scrollable({
      next:".nanum .paging .arrow.next",
      prev:".nanum .paging .arrow.prev"
   });

   window.api_notice = notice_scrollable.data("scrollable");
   window.api_nanum = nanum_scrollable.data("scrollable");

   $('.notice .paging span[data-page]').click(function() {
      api_notice.seekTo($(this).data('page')*1-1);
      $('.notice .now').removeClass('now');
      $(this).addClass('now');
   });
   $('.notice .paging .arrow.left').click(function() {
      api_notice.seekTo(0);
      $('.notice .now').removeClass('now');
      $('.notice span[data-page=1]').addClass('now');
   });
   $('.notice .paging .arrow.prev').click(function() {
      var before = $('.notice .now').data('page');
      if(before != 1) {
         $('.notice .now').removeClass('now');
         $('.notice span[data-page='+(before-1)+']').addClass('now');
      }
   });
   $('.notice .paging .arrow.next').click(function() {
      var before = $('.notice .now').data('page');
      if(before != api_notice.getSize()) {
         $('.now').removeClass('now');
         $('.notice span[data-page='+(before+1)+']').addClass('now');
      }
   });
   $('.notice .paging .arrow.right').click(function() {
      var last = api_notice.getSize();
      api_notice.seekTo(last-1);
      $('.notice .now').removeClass('now');
      $('.notice span[data-page='+last+']').addClass('now');
   });
   $('.notice .row').click(function() {
      var tmp_id = $(this).data('id');
      console.log(tmp_id);
      $.ajax({
         url:'/ajax/get_boards.php',
         type:'POST',
         data:{
            type:'notice',
            id:tmp_id
         },
         cache:false,
         success:function(e) {
            e = JSON.parse(e);
            if(e.res_status == 'true') {
               $('.notice .view .board_title').html(e.res_title);
               $('.notice .view .board_time').html(e.res_time);
               $('.notice .view .board_content').html(e.res_content);
               $('.notice .list').hide();
               $('.notice .view').fadeIn();
               $('.notice .view .board_content p img').css({'width':'100%','height':'auto'});
            } else {
               alert('공지사항을 읽어오는데 오류가 발생했습니다.');
            }
         },
         error:function() {
            alert('오류가 발생했습니다. 잠시후에 다시 시도해주세요.');
         }
      });
   });
   if(show_notice) {
      var saved_page = $('.row[data-id='+show_notice+']').data('page');
      $('.notice .row[data-id='+show_notice+']').click();
      $('.notice span[data-page='+saved_page+']').click();
   } else {
      $('.notice .paging span[data-page=1]').addClass('now');
   }

   $('.notice .list_btn').click(function() {
      $('.notice .view').hide();
      $('.notice .list').fadeIn();
   });


   $('.nanum .paging span[data-page]').click(function() {
      api_nanum.seekTo($(this).data('page')*1-1);
      $('.nanum .now').removeClass('now');
      $(this).addClass('now');
   });
   $('.nanum .paging .arrow.left').click(function() {
      api_nanum.seekTo(0);
      $('.nanum .now').removeClass('now');
      $('.nanum span[data-page=1]').addClass('now');
   });
   $('.nanum .paging .arrow.prev').click(function() {
      var before = $('.nanum .now').data('page');
      if(before != 1) {
         $('.nanum .now').removeClass('now');
         $('.nanum span[data-page='+(before-1)+']').addClass('now');
      }
   });
   $('.nanum .paging .arrow.next').click(function() {
      var before = $('.nanum .now').data('page');
      if(before != api_nanum.getSize()) {
         $('.nanum .now').removeClass('now');
         $('.nanum span[data-page='+(before+1)+']').addClass('now');
      }
   });
   $('.nanum .paging .arrow.right').click(function() {
      var last = api_nanum.getSize();
      api_nanum.seekTo(last-1);
      $('.nanum .now').removeClass('now');
      $('.nanum span[data-page='+last+']').addClass('now');
   });
   $('.nanum .row').click(function() {
      var tmp_id = $(this).data('id');
      console.log(tmp_id);
      $.ajax({
         url:'/ajax/get_boards.php',
         type:'POST',
         data:{
            type:'nanum',
            id:tmp_id
         },
         cache:false,
         success:function(e) {
            e = JSON.parse(e);
            if(e.res_status == 'true') {
               $('.nanum .view .board_title').html(e.res_title);
               $('.nanum .view .board_time').html(e.res_time);
               $('.nanum .view .board_content').html(e.res_content);
               $('.nanum .list').hide();
               $('.nanum .view').fadeIn();
               $('.nanum .view .board_content p img').css({'width':'100%','height':'auto'});
            } else {
               alert('공지사항을 읽어오는데 오류가 발생했습니다.');
            }
         },
         error:function() {
            alert('오류가 발생했습니다. 잠시후에 다시 시도해주세요.');
         }
      });
   });
   if(show_nanum) {
      var saved_page = $('.row[data-id='+show_nanum+']').data('page');
      $('.nanum .row[data-id='+show_nanum+']').click();
      $('.nanum span[data-page='+saved_page+']').click();
   } else {
      $('.nanum .paging span[data-page=1]').addClass('now');
   }

   $('.nanum .list_btn').click(function() {
      $('.nanum .view').hide();
      $('.nanum .list').fadeIn();
   });

});
