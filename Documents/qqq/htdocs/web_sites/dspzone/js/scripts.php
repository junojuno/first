<?php
   header('Content-Type: text/javascript');
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
?>
var scriptLoader = {
   require_callback : function(url, callback) {
      var script = document.createElement("script");
      script.type = "text/javascript";

      if (script.readyState){  //IE
         script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
               script.readyState == "complete"){
               script.onreadystatechange = null;
               callback();
            }
         };
      } else {  //Others
         script.onload = function(){
            callback();
         };
      }
      script.src = url;
      document.getElementsByTagName("head")[0].appendChild(script);
   },
   require: function(url) {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = url;
      document.getElementsByTagName("head")[0].appendChild(script);
      
   },
	load: function() {
      var path = '/js/';
      this.require_callback('http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js',function() {
         scriptLoader.require(path + 'global.js');
         scriptLoader.require('<?=str_replace('.php','.js',SCRIPT_NAME)?>');
         scriptLoader.require('http://connect.facebook.net/en_US/all.js');
         scriptLoader.require('https://platform.twitter.com/widgets.js');
      });
	}
}
scriptLoader.load();
