$(document).ready(function() {
   $('.print').css('cursor','pointer');
   $('.print.not').css('cursor','default');
   $('.print').click(function() {
      if(!$(this).hasClass('not')) {
         window.print();
      }
   });
   $('.not_pay').each(function() {
      if($(this).attr('past') == 'true') {
         showTooltip('my_not_pay', $(this), "<div class='account_ctt'><div class='past'>입금 기한이 지났습니다</div></div>");
      } else {
         var data = JSON.parse($(this).attr('info'));
         var msg = "<div class='account_ctt'>"+
            "<table>"+
               "<tr>"+
                  "<th>은행</th>"+
                  "<td>"+data.bank+"</td>"+
               "</tr>"+
               "<tr>"+
                  "<th>예금주</th>"+
                  "<td>"+data.depositor+"</td>"+
               "</tr>"+
               "<tr>"+
                  "<th>계좌번호</th>"+
                  "<td>"+data.account+"</td>"+
               "</tr>"+
               "<tr>"+
                  "<th>금액</th>"+
                  "<td>"+data.amount+"</td>"+
               "</tr>"+
               "<tr>"+
                  "<th>입금기한</th>"+
                  "<td>"+data.va_date+"</td>"+
               "</tr>"+
            "</table></div>";
         showTooltip('my_not_pay', $(this), msg);
      }
   });
})
