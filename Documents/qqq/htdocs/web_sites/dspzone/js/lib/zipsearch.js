
var address = {
	bindZipcodeFind: function(){
		$('.zipcode-search').click(doSearch);
		$('#dongName').keypress(function(e) { if(e.which == 13) { doSearch(); return false; }});
	},
	bindPutAddress: function(){
		$('.zipcode-search-result a').click(function(){
         var first = $('.zipcode-search-result').data('name') == undefined ? '' : $('.zipcode-search-result').data('name') ;
         first = first != '' ? first : 'od';
			$('[name='+first+'_zip1]').val($(this).parent().parent().find('.postcd1').text());
			$('[name='+first+'_zip2]').val($(this).parent().parent().find('.postcd2').text());
			$('[name='+first+'_addr1]').val(address.remove_useless_addr($(this).parent().parent().find('.address').text()));
			address.hideZipcodeFinder();
			$('#addr').show();
			return false;
		});
	},
	remove_useless_addr: function(address){
		if(address.indexOf('~') != -1){
			address = address.split(' ').slice(0,-1).join(' ');
		}
		return address;
	},
	hideZipcodeFinder: function(){
		$('.zipcode-search-result').slideUp();
		$('#dongName').val('동/리/건물명').addClass('label');
	}
}
function doSearch() {
	$('.zipcode-search-result').show().text("검색중...");
	$.get('/ajax/ajax.zipsearch.php',{
		query: $('#dongName').val()
	},function(data){
		$('.zipcode-search-result').html(data);
		address.bindPutAddress();
	})
}


$(document).ready(function() {

//	$('#addr').hide();

	$('#dongName').val('동/리/건물명 입력').addClass('label').focusin(function() {
		if (!$(this).val() || $(this).val() == '동/리/건물명 입력') {
			$(this).val('').removeClass('label');
		}
	}).focusout(function() {
		if (!$(this).val()) {
			$(this).val('동/리/건물명 입력').addClass('label');
		}
	});

	if (!$('input[name=od_addr2]').val()) {
		$('input[name=od_addr2]').val('상세주소 입력').addClass('label');
	}
	$('input[name=od_addr2]').focusin(function() {
		if (!$(this).val() || $(this).val() == '상세주소 입력') {
			$(this).val('').removeClass('label');
		}
	}).focusout(function() {
		if (!$(this).val()) {
			$(this).val('상세주소 입력').addClass('label');
		}
	});

	address.bindZipcodeFind();
});

