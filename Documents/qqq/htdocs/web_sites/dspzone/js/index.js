$(document).ready(function() {
   var notice_scrollable = $('.notice .slides').scrollable({
      next:".notice .paging .next",
      prev:".notice .paging .prev"
   });
   
   var nanum_scrollable = $('.nanum .slides').scrollable({
      next:".nanum .paging .next",
      prev:".nanum .paging .prev"
   });
   
   
   window.api_notice = notice_scrollable.data("scrollable");
   window.api_nanum = nanum_scrollable.data("scrollable");

   $('.notice .paging span[data-page]').click(function() {
      api_notice.seekTo($(this).data('page')*1-1);
      $('.notice .now').removeClass('now');
      $(this).addClass('now');
   });
   $('.notice .paging .arrow.left').click(function() {
      api_notice.seekTo(0);
      $('.notice .now').removeClass('now');
      $('.notice span[data-page=1]').addClass('now');
   });
   $('.notice .paging .arrow.prev').click(function() {
      var before = $('.notice .now').data('page');
      if(before != 1) {
         $('.notice .now').removeClass('now');
         $('.notice span[data-page='+(before-1)+']').addClass('now');
      }
   });
   $('.notice .paging .arrow.next').click(function() {
      var before = $('.notice .now').data('page');
      if(before != api_notice.getSize()) {
         $('.notice .now').removeClass('now');
         $('.notice span[data-page='+(before+1)+']').addClass('now');
      }
   });
   $('.notice .paging .arrow.right').click(function() {
      var last = api_notice.getSize();
      api_notice.seekTo(last-1);
      $('.notice .now').removeClass('now');
      $('.notice span[data-page='+last+']').addClass('now');
   });



   $('.notice .list_btn').click(function() {
      $('.notice .view').hide();
      $('.notice .list').fadeIn();
   });


   $('.nanum .paging span[data-page]').click(function() {
      api_nanum.seekTo($(this).data('page')*1-1);
      $('.nanum .now').removeClass('now');
      $(this).addClass('now');
   });
   $('.nanum .paging .arrow.left').click(function() {
      api_nanum.seekTo(0);
      $('.nanum .now').removeClass('now');
      $('.nanum span[data-page=1]').addClass('now');
   });
   $('.nanum .paging .arrow.prev').click(function() {
      var before = $('.nanum .now').data('page');
      if(before != 1) {
         $('.nanum .now').removeClass('now');
         $('.nanum span[data-page='+(before-1)+']').addClass('now');
      }
   });
   $('.nanum .paging .arrow.next').click(function() {
      var before = $('.nanum .now').data('page');
      if(before != api_nanum.getSize()) {
         $('.nanum .now').removeClass('now');
         $('.nanum span[data-page='+(before+1)+']').addClass('now');
      }
   });
   $('.nanum .paging .arrow.right').click(function() {
      var last = api_nanum.getSize();
      api_nanum.seekTo(last-1);
      $('.nanum .now').removeClass('now');
      $('.nanum span[data-page='+last+']').addClass('now');
   });

   $('.paging span[data-page=1]').addClass('now');
});
