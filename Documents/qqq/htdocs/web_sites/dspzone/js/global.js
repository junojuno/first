$(document).ready(function() {
   FB.init({
      //appId : "408565205831331",
      appId : "636087649795432",
      status : true,
      cookie : true,
      xfbml : true
   });
   $('[href], [open-layer]').css('cursor','pointer');
   $('[href]').click(function(){
      if($(this).attr('nw') == 'true') {
         window.open($(this).attr('href'));
      } else {
         location.href = $(this).attr('href');
      }
   });
   $('[open-layer]').click(function(){
      var open_layer = $(this).attr('open-layer');
      var obj = getLayerId(open_layer);
      if($('.layer').length > 0) {
         destroyLayer(getLayerId($('.layer').data('info')));
      }
      createLayer($(obj),open_layer);
      if(open_layer == 'join') {
         $('input[name=join_name]').focus();
         showTooltip('duple_check', $('.duple_check'),'중복체크');
      } else if(open_layer == 'login') {
         $('input[name=login_email]').focus();
         showTooltip('login_find_pw', $('.find_pw'), '비밀번호를 잊어버리셨나요?');
      } else if(open_layer == 'find_pw') {
         $('input[name=find_email]').focus();
      }
   });

   if(getParameterByName('layer')) {
      $('[open-layer='+getParameterByName('layer')+']').click();
   }

   $('.family_site select').change(function() {
      window.open($(this).val(),'_newtab');
   });
   $('#loginLayer label, #findLayer label, #joinLayer label').click(function(){
      $(this).parent().children('input').focus();
   });
   $('#loginLayer input, #findLayer input, #joinLayer input').keyup(function(e) {
      var keyCode = e.which ? e.which : e.keyCode;
      if($(this).val().length > 0) {
         $(this).parent().children('label').hide();
      } else {
         $(this).parent().children('label').show();
      }
      if($(this).attr('name') == 'join_email') {
         if(keyCode != 13)
            $('input[name=dupleChecked]').val('0');
      }

      if(keyCode == 13) {
         if(/join/.test($(this).attr('name'))) {
            $('.join_btn').click();
         } else if(/login/.test($(this).attr('name'))) {
            $('.login_btn').click();
         }
      }
   });
   $("[event-ajax]").click(function() {
      var ajax_type = $(this).attr('event-ajax');
      switch(ajax_type) {
         case 'email_duple_check':
            if($(this).data('qtip')) $(this).qtip("destroy",true);
            if(!$('input[name=join_email]').val()) {
               showTooltip('duple_check_no', $(this), '이메일을 입력해주세요.');
               return false;
            }
            if(!email_check($('input[name=join_email]').val())) {
               showTooltip('duple_check_no', $(this), '이메일 주소가 올바르지 않습니다');
               return false;
            }
            ajaxRequest(ajax_type,{mb_email:$('input[name=join_email]').val()});
            break;
         case 'login_check':
            if(checkFormElement('login')) {
               $('input[name=login_p]').remove();
               createHashElement($('form[name=login]'),'login_p',$('input[name=login_pw]').val());
               ajaxRequest(ajax_type,{login_email:$('input[name=login_email]').val(), p:$('input[name=login_p]').val()});
            }  
            break;
      }
   });
   $("[form-name]").click(function(e) {
      var f_name = $(this).attr('form-name');
      var url = '';
      switch(f_name) {
         case 'join':
            url = '/register';
            break;
         case 'login':
            url = '/login';
            break;
         case 'donate':
            url = '/campaign/pp_ax_hub';
            break;
         case 'my_modify':
            url = '/my/modify';
            break;
      }
      if(checkFormElement(f_name)) {
         $('input[name=login_pw]').val('');
         if(f_name == 'donate' && is_mobile == 'true') {
            e.preventDefault();
            return false;
         }
         $('form[name='+f_name+']').attr('action',url);  
         $('form[name='+f_name+']').submit();
      } else {
         e.preventDefault();
         return false;
      }
   });
   $(".numonly").keydown(function(e) {
      if (e.keyCode == 46 || e.keyCode == 8
            || e.keyCode == 9
            || e.keyCode == 27
            || e.keyCode == 13
            || e.keyCode == 37
            || e.keyCode == 39
            || e.keyCode == 65
            && e.ctrlKey === true) {
         return
      } else if (e.shiftKey
            || (e.keyCode < 48 || e.keyCode > 57)
            && (e.keyCode < 96 || e.keyCode > 105)) {
         e.preventDefault();
      }
   });
});

function ajaxRequest(type, post_data) {
   var req_url = '';
   switch(type) {
      case 'email_duple_check':
         req_url = '/ajax/check_email.php';
         break;
      case 'login_check':
         req_url = '/ajax/check_login.php';
         break;
   } 
   $.ajax({
      url: req_url,
      type:'POST',
      data: post_data,
      cache:false,
      success:function(res) {
         console.log('ajax response : '+res);
         res = JSON.parse(res);
         switch(type) {
            case 'email_duple_check':
               if(res.response == 'yes') {
                  $('input[name=dupleChecked]').val('1');
                  showTooltip('duple_check_yes', $('.duple_check'),'사용 가능합니다.');
                  $('input[name=join_email]').focus();
               } else if(res.response == 'no') {
                  $('input[name=dupleChecked]').val('0');
                  showTooltip('duple_check_no', $('.duple_check'), '중복된 이메일입니다.');
               } else {
                  alert('오류가 발생했습니다. 다시 시도해주세요.');
               }
               break;
            case 'login_check':
               if(res.response == 'yes') {
                  $('input[name=login_pw]').val('');
                  $('form[name=login]').attr("action",'/login').submit();
               } else {
                  alert('존재하지 않는 이메일이거나 비밀번호가 정확하지 않습니다.');
                  $('input[name=login_email]').focus();
               }
               break;
         }
      },
      error:function() {
         alert('오류가 발생했습니다. 다시 시도해주세요');
      }
   });
}

function checkFormElement(f_name) {
   $('.qtip').remove();
   $('.input_error').removeClass('input_error');
   switch(f_name) {
      case 'join':
         if(!$('input[name=join_name]').val()) {
            showTooltip('chk_form_1',$('input[name=join_name]'), '이름을 입력해주세요.');
            return false;
         }
         if(!$('input[name=join_email]').val()) {
            showTooltip('chk_form_1',$('input[name=join_email]'), '이메일을 입력해주세요.');
            return false;
         }
         if($('input[name=dupleChecked]').val() == '0') {
            showTooltip('duple_check_no',$('.duple_check'), '중복확인을 해주세요.');
            return false;
         }
         if(!$('input[name=join_pw]').val()) {
            showTooltip('chk_form_1',$('input[name=join_pw]'), '비밀번호를 입력해주세요.');
            return false;
         }
         if(!$('input[name=join_pw_confirm]').val()) {
            showTooltip('chk_form_1',$('input[name=join_pw_confirm]'), '비밀번호 확인을 입력해주세요.');
            return false;
         }
         if($('input[name=join_pw]').val() != $('input[name=join_pw_confirm]').val()) {
            showTooltip('chk_form_1',$('input[name=join_pw_confirm]'), '비밀번호 확인이 일치하지 않습니다.');
            return false;
         }
         createHashElement($('form[name='+f_name+']'),'p',$('input[name=join_pw]').val());
         $('input[name=join_pw], input[name=join_pw_confirm]').val('');
         return true;
      case 'login':
         if(!$('input[name=login_email]').val()) {
            showTooltip('chk_form_1',$('input[name=login_email]'), '이메일을 입력해주세요.');
            return false;
         }
         if(!$('input[name=login_pw]').val()) {
            showTooltip('chk_form_1',$('input[name=login_pw]'), '비밀번호를 입력해주세요.');
            return false;
         }
         return true;
      case 'donate':
         if(!$('input[name=od_method]:checked').val()) {
            showTooltip('chk_form_1',$('.payment table'), '결제 방법을 선택해주세요.');
            return false;
         }
         $('input[name=pay_method]').val($('input[name=od_method]:checked').val());
         if(!$('input[name=buyr_name]').val()) {
            showTooltip('chk_form_1', $('input[name=buyr_name]'), '이름을 입력해주세요.');
            return false;
         }
         if(!$('input[name=od_hp1]').val() || !$('input[name=od_hp2]').val() || !$('input[name=od_hp3]').val()) {
            showTooltip('chk_form_1', $('.contact'), '핸드폰 번호를 입력해주세요.');
            return false;
         }
         var my_contact = $.trim($('input[name=od_hp1]').val())+'-'+$.trim($('input[name=od_hp2]').val())+'-'+$.trim($('input[name=od_hp3]').val());
         if(!phone_check(my_contact)) {
            showTooltip('chk_form_1', $('.contact'), '핸드폰 번호를 확인해주세요.');
            return false;
            
         }
         $('input[name=buyr_tel1]').val(my_contact);
         if(!$('input[name=buyr_mail]').val()) {
            showTooltip('chk_form_1', $('input[name=buyr_mail]'), '이메일을 입력해주세요.');
            return false;
         }
         if(!email_check($('input[name=buyr_mail]').val())) {
            showTooltip('chk_form_1', $('input[name=buyr_mail]'), '이메일형식을 확인해주세요.');
            return false;

         }
         if(!$('input[name=od_zip1]').val() || !$('input[name=od_zip2]') || !$('input[name=od_addr1]')) {
            showTooltip('chk_form_1', $('#dongName'), '주소를 검색해주세요.');
            return false;
         }
         if(!$('input[name=od_addr2]').val() || $('input[name=od_addr2]').val() == '상세주소 입력') {
            showTooltip('chk_form_1', $('input[name=od_addr2]'), '상세주소를 입력해주세요.');
            return false;
         }
         $('input[name=good_mny]').val('5000');
         var RetVal = true;
         if(is_mobile == 'true') {
            switch($('input[name=od_method]:checked').val()) {
               case "100000000000":
                  $('input[name=pay_method]').val('CARD');
                  $('input[name=ActionResult]').val('card');
                  break;
               case "001000000000":
                  $('input[name=pay_method]').val('VCNT');
                  $('input[name=ActionResult]').val('vcnt');
                  break;
               case "000010000000":
                  $('input[name=pay_method]').val('MOBX');
                  $('input[name=ActionResult]').val('mobx');
                  break;
               case "010000000000":
                  showTooltip('chk_form_1', $('.payment table'),'모바일은 계좌이체를 지원하지 않습니다.');
                  RetVal = false;
                  break;
            }

            if(RetVal) { // 멤버정보 저장하는 리퀘스트 날리기.
               $.ajax({
                  url:'/ajax/update_mb_info.php',
                  type:'POST',
                  data:{
                     name:$('input[name=buyr_name]').val(),
                     contact:my_contact,
                     email:$('input[name=buyr_mail]').val(),
                     zips: {zip1:$('input[name=od_zip1]').val(), zip2:$('input[name=od_zip2]').val()},
                     address: {addr1:$('input[name=od_addr1]').val(), addr2:$('input[name=od_addr2]').val()} 
                  },
                  success:function(e) {
                     console.log(e);
                     e = JSON.parse(e);
                     if(e.res_status == 'true') {
                        kcp_AJAX();
                     } else {
                        RetVal = false;
                        alert('오류가 발생했습니다. 다시 시도해주세요.');
                     }
                  },
                  error:function() {
                     alert('오류가 발생했습니다. 다시 시도해주세요.');
                  }
               });
            }            
         } else {
            if (MakePayMessage(document.donate) == true) {
               RetVal = true;
            }
            else {
               // res_cd= 오류코드, res_msg=오류메시지
               res_cd  = document.donate.res_cd.value;
               res_msg = document.donate.res_msg.value;
               console.log('error : ['+res_cd+'] '+res_msg);
            }
         }
         return RetVal;
      case 'my_modify':
         var pw_flag = false;
         if(!$('input[name=mb_name]').val()) {
            showTooltip('chk_form_1', $('input[name=mb_name]'), '이름을 입력해주세요');
            return false;
         }
         if(!$('input[name=mb_email]').val()){
            showToolip('chk_form_1', $('input[name=mb_email]'), '이메일을 입력해주세요');
            return false;
         }
         if(!email_check($('input[name=mb_email]').val())) {
            showTooltip('chk_form_1', $('input[name=mb_email]'), '이메일형식을 확인해주세요.');
            return false;
         }
         if($('input[name=mb_pw]').val()) {
            if($('input[name=mb_pw]').val().length < 8) {
               showTooltip('chk_form_1', $('input[name=mb_pw]'), '비밀번호는 최소한 8자리 이상이여야합니다.');
               return false;
            }
            if(!$('input[name=mb_pw_confirm]').val()) {
               showTooltip('chk_form_1', $('input[name=mb_pw_confirm]'), '비밀번호 확인을 확인해주세요.');
               return false;
            }
            if($('input[name=mb_pw]').val() != $('input[name=mb_pw_confirm]').val()) {
               showTooltip('chk_form_1', $('input[name=mb_pw_confirm]'), '비밀번호와 비밀번호 확인이 일치하지 않습니다.');
               return false;
            }
            
            pw_flag = true;
         }
         if($('input[name=od_hp1]').val() || $('input[name=od_hp2]').val() || $('input[name=od_hp3]').val()) {
            if(!$('input[name=od_hp1]').val() || !$('input[name=od_hp2]').val() || !$('input[name=od_hp3]').val()) {
               showTooltip('chk_form_1', $('.contact'), '핸드폰 번호를 입력해주세요.');
               return false;
            }
            var contact = $('input[name=od_hp1]').val()+'-'+$('input[name=od_hp2]').val()+'-'+$('input[name=od_hp3]').val();
            if(!phone_check(contact)) {
               showTooltip('chk_form_1', $('.contact'), '핸드폰 번호를 확인해주세요.');
               return false;
            }
            $('input[name=mb_contact]').val(contact);
         }
         if ($('input[name=mb_profile]').val()) {
            var ext = $('input[name=mb_profile]').val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
               showTooltip('chk_form_1', $('input[name=mb_profile]'), '올바른 이미지 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
               return false;
            }
         }

         if(pw_flag) {
            createHashElement($('form[name='+f_name+']'),'p',$('input[name=mb_pw]').val());
            $('input[name=mb_pw], input[name=mb_pw_confirm]').val('');
         }
         
         return true;
         
   }
}

function email_check(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function phone_check(phone) {
   return true;
}

function createLayer(str, info) {
   if(getParameterByName('url') != '' && info == 'login') return;
	$(str).show().addClass('centering').css('margin-top',
			'-' + $(str).height() / 2 + 'px').css('margin-left',
			'-' + $(str).width() / 2 + 'px');
	$('body').append($(document.createElement('div')).addClass('layer').data('info',info).append(str));
   if(info != 'pluginLayer') {
      $(".layer").hide().fadeIn("fast");
   }
   $(".layer, .layer .btn_close").click(function() {
      destroyLayer($(str));
   });
   $(str).click(function(e) {
      e.stopPropagation();
   });
}

function destroyLayer(str) {
	$(str).hide().prependTo($('body'));
   $('.qtip').remove();
   $('.input_error').removeClass('input_error');
   $('.layer').fadeOut("fast",function() {
      $(this).remove();
   });
}
function getLayerId(name) {
   switch(name) {
      case 'join':
         return '#joinLayer';
      case 'login':
         return '#loginLayer';
      case 'find_pw':
         return '#findLayer';
   }
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function showTooltip(type, obj, msg) {
   if($(obj).data("qtip")) $(obj).qtip('destroy');
   var custom_class ='';
   switch(type) {
      case 'duple_check':
      case 'duple_check_yes':
      case 'duple_check_no':
      case 'login_find_pw':
         if(type == 'duple_check' || type == 'login_find_pw') {
            var keeping = true;
            custom_class = 'qtip-my-dark';
         }
         else if(type == 'duple_check_yes') custom_class = 'qtip-my-green';
         else custom_class = 'qtip-my-red';
         var show_evt = typeof keeping === 'undefined' ? false : 'mouseenter';
         $(obj).qtip({
            content : msg,
            position: {
               my: 'bottom center',
               at: 'top center'
            },
            show : {
               event : show_evt,
               ready : true,
               delay : 0,
               persistent : false
            },
            hide : {
               delay : 2500,
               effect : function() {
                  $(this).fadeOut(250);
               }
            },
            style: { classes: custom_class }
         });
         break;
      case 'chk_form_1':
         custom_class = 'qtip-red qtip-red-chkform1';
         $(obj).qtip({
            content : msg,
            position: {
               my: 'right center',
               at: 'left center'
            },
            show : {
               event : false,
               ready : true,
               delay : 0,
               persistent : false
            },
            hide : {
               event : false
            },
            style: { classes: custom_class },
            events : {
               render : function(evt, api) {
                  $(obj).addClass('input_error');
               }
            }
         });
         break;
      case 'my_not_pay':
         custom_class = 'qtip-my-white';
         $(obj).qtip({
            content : msg,
            position: {
               my: 'bottom center',
               at: 'top center'
            },
            show : {
               event : 'click',
               persistent : false
            },
            hide : {
               event : 'click'
            },
            style: { classes: custom_class }
         });
         break;
   }
}

function js_send_email(post_data) {
   $.ajax({
      url:'/module/sendmail.php',
      type:'POST',
      data:post_data,
      cache:false,
      success: function(t) {
         console.log(t);
      }
   });
}

function js_send_sms(post_data) {
   $.ajax({
      url:'/module/sendsms.php',
      type:'POST',
      data:post_data,
      cache:false,
      success: function(t) {
         console.log(t);
      }
   });
}
