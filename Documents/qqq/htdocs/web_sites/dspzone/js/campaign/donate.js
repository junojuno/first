scriptLoader.require('/js/lib/zipsearch.js');
$(document).ready(function() {
   if(is_mobile == 'true') {
      chk_pay();
   } else {
      createLayer($("#pluginLayer"), 'pluginLayer');
      kcp_checkPlugin();
   }
   $('input[type=text], input[type=button]').addClass('input1');
   $('input[name=od_method]').click(function(){
      if($(this).val() == '001000000000') {
         var custom_class = 'qtip-red';
         var msg = "<div class='ac_caution'>"+
            "<div>가상계좌(무통장 입금) 신청 시 다른 결제 수단으로 추가 결제는 불가능하니 무통장 입금 신청시에는 반드시 해당 계좌로 입금해야 합니다(1인 1매 구매 원칙)</div>"+
            "<div class='ac_bot'>실제 입금 완료 후 티켓이 발급되며, 입금된 순서로 좌석이 자동 배정됩니다. 좌석은 입금 후 메일과 문자로 받아보실 수 있습니다.</div></div>"
         $('label[for=vc]').qtip({
            content : msg,
            position: {
               my: 'top center',
               at: 'bottom center'
            },
            show : {
               event : 'click',
               ready : true,
               delay : 0,
               persistent : false
            },
            hide : {
               event : 'click'
            },
            style: { classes: custom_class }
         });
      }
   })
});

function kcp_checkPlugin() {
	if (navigator.userAgent.indexOf('MSIE') > 0 || (navigator.userAgent.indexOf('Trident/7.0') > 0)) {
		if ( document.Payplus.object != null ) {
         destroyLayer($('#pluginLayer'));
		}
	}
	else {
		var inst = 0;
		for (var i = 0; i < navigator.plugins.length; i++) {
			if (navigator.plugins[i].name == 'KCP') {
				inst = 1;
				break;
			}		
		}

		if (inst == 1) {
         destroyLayer($('#pluginLayer'));
		}
		else {
			document.location.href=GetInstallFile();
		}
	}
}

function call_pay_form() {
   var v_frm = document.donate;

   layer_cont_obj   = document.getElementById("content");
   layer_pay_obj = document.getElementById("layer_pay");

   document.getElementById("header").style.display="none";
   document.getElementById("tail").style.display="none";
   layer_cont_obj.style.display = "none";
   layer_pay_obj.style.display = "block";
   v_frm.target = "frm_pay";

   if(v_frm.encoding_trans.value == "UTF-8") {
      v_frm.action = PayUrl.substring(0,PayUrl.lastIndexOf("/")) + "/jsp/encodingFilter/encodingFilter.jsp";
      v_frm.PayUrl.value = PayUrl;
   }

   if(v_frm.Ret_URL.value == "") {
      /* Ret_URL값은 현 페이지의 URL 입니다. */
      alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다.");
      return false;
   } else {
      v_frm.submit();
   }
}


 /* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청*/
function chk_pay() {
   /*kcp 결제서버에서 가맹점 주문페이지로 폼값을 보내기위한 설정(변경불가)*/
   self.name = "tar_opener";

   var pay_form = document.donate;

   if (pay_form.res_cd.value == "3001" ) {
      alert("사용자가 취소하였습니다.");
      pay_form.res_cd.value = "";
      return false;
   } else if (pay_form.res_cd.value == "3000" ) {
      alert("30만원 이상 결제 할수 없습니다.");
      pay_form.res_cd.value = "";
      return false;
   }

   if (pay_form.enc_data.value != "" && pay_form.enc_info.value != "" && pay_form.tran_cd.value !="" ) {
      pay_form.target = "";
      pay_form.action = "/campaign/pp_ax_hub";
      pay_form.submit();
   } else {
      return false;
   }
}
