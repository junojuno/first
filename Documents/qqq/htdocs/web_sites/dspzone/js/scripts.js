var scriptLoader = {
   require_callback : function(url, callback) {
      var script = document.createElement("script");
      script.type = "text/javascript";

      if (script.readyState){  //IE
         script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
               script.readyState == "complete"){
               script.onreadystatechange = null;
               callback();
            }
         };
      } else {  //Others
         script.onload = function(){
            callback();
         };
      }
      script.src = url;
      document.getElementsByTagName("head")[0].appendChild(script);
   },
   require: function(url) {
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src = url;
      script.async = true;
      document.getElementsByTagName("head")[0].appendChild(script);
      
   },
	load: function() {
      var path = '/js/';
      this.require_callback('http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js',function() {
         scriptLoader.require_callback('http://cdn.jsdelivr.net/qtip2/2.2.0/jquery.qtip.min.js', function() {
            scriptLoader.require_callback('http://connect.facebook.net/en_US/all.js', function() {
               scriptLoader.require_callback(path + 'global.js', function() {
                  if(js_file) scriptLoader.require(js_file); // var js_file in head.php
               });
            });
         });
         scriptLoader.require(path + 'lib/encrypt.js');
         scriptLoader.require('https://platform.twitter.com/widgets.js');
      });
	}
}
scriptLoader.load();
