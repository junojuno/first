<?php
   sec_session_start();

   require INCLUDE_ROOT.'/lib/facebook/facebook.php';

   $facebook = new Facebook(array(
      'appId'  => FACEBOOK_APP_ID,
      'secret' => FACEBOOK_APP_SECRET
   ));

   $user = $facebook->getUser();
   if ($user) {
      try {
         $user_profile = $facebook->api('/me');
      }
      catch (FacebookApiException $e) {
         error_log($e);
         $user = null;
      }
      // process login
      if(!$_SESSION[user_no]) {
         $pwdstr = hash('sha512', $user_profile[id].'tryFaceBOOKsessionLOGin');
         $membercheck = login('fb@'.$user_profile[id], $pwdstr);

         // if not member, build member profile
         if ($membercheck === 'NOTUSER') {
            $password = hash('sha512', $user_profile[id].'tryFaceBOOKsessionLOGin');
            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $password = hash('sha512', $password.$random_salt);
            $fbid = 'fb@'.$user_profile[id];

            if (!$user_profile[email]) $user_profile[email] = 'asd@asd.com';

            $res = sql_query("INSERT INTO ".DB_MEMBERS." SET 
               mb_id = ?, mb_name = ?, mb_password = ?, mb_email = ?, salt = ?", array(
               $fbid, $user_profile[name], $password, $user_profile[email], $random_salt
            ));

            // and login again
            if($res) login('fb@'.$user_profile[id], $pwdstr);
         } 
      }  
   } else {
      $loginUrl = $facebook->getLoginUrl(array('scope' => 'email,publish_actions'));
   }
?>
