<?php
   sec_session_start();
   $_status = false;
   $script_var = "<script> var success_flag = null; </script>";
   if(!$_SESSION[user_no]) {
      alert('로그인을 해주세요','/login?url='.$_SERVER[REQUEST_URI]);
   }
   if($_SERVER[REQUEST_METHOD] == 'POST') {
      $mb_name = $_POST[mb_name];
      $mb_email = $_POST[mb_email];
      $mb_pw = $_POST[p] ? $_POST[p] : "";
      $mb_contact = $_POST[mb_contact] ? $_POST[mb_contact] : "";
      $mb_zip1 = $_POST[mb_zip1] ? $_POST[mb_zip1] : "";
      $mb_zip2 = $_POST[mb_zip2] ? $_POST[mb_zip2] : "";
      $mb_addr1 = $_POST[mb_addr1] ? $_POST[mb_addr1] : "";
      $mb_addr2 = $_POST[mb_addr2] ? $_POST[mb_addr2] : "";

      if($mb_name == '' || $mb_email == '') {
         alert('오류가 발생했습니다.','./');
      }

      if($mb_pw != '') {
         if($_SESSION[is_facebook] || $_SESSION[is_twitter]) {
            alert('오류가 발생했습니다.','./');
         }
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$mb_pw = hash('sha512', $mb_pw.$random_salt);
         $query = "UPDATE ".DB_MEMBERS." SET mb_name = ?, mb_email = ?, mb_password = ?, salt = ?, mb_zip1 = ?, mb_zip2 = ?, mb_addr1 = ?, mb_addr2 = ?, mb_contact = ? WHERE mb_no = ?";
         $info_arr = array($mb_name, $mb_email, $mb_pw, $random_salt, $mb_zip1, $mb_zip2, $mb_addr1, $mb_addr2, $mb_contact, $_SESSION[user_no]);
      } else {
         $query = "UPDATE ".DB_MEMBERS." SET mb_name = ?, mb_email = ?, mb_zip1 = ?, mb_zip2 = ?, mb_addr1 = ?, mb_addr2 = ?, mb_contact = ? WHERE mb_no = ?";
         $info_arr = array($mb_name, $mb_email, $mb_zip1, $mb_zip2, $mb_addr1, $mb_addr2, $mb_contact, $_SESSION[user_no]);
      }
      $res = sql_query($query, $info_arr);

		if ($_FILES['mb_profile']) {
         include INCLUDE_ROOT.'/lib/class.upload.php';
			$handle = new upload($_FILES['mb_profile']);
			if ($handle->uploaded) {
				$handle->forbidden				= array('application/*');
				$handle->allowed				= array('image/*');
				$handle->image_convert			= 'jpg';
				$handle->file_new_name_body		= 'profile_'.$_SESSION[user_no];
				$handle->image_resize			= true;
				$handle->image_ratio_crop		= true;
				$handle->image_x				= 100;
				$handle->image_y				= 100;
				$handle->file_overwrite 		= true;
				$handle->process(DATA_PATH.'/member/');
		
				if ($handle->processed) {
					$handle->clean();
					sql_query("UPDATE ".DB_MEMBERS." SET mb_icon = 'profile_{$_SESSION[user_no]}.jpg' WHERE mb_no = ? ", array($_SESSION[user_no]));
				} else {
					$script_var .= "<script>alert('프로필 사진 업로드 실패  : $handle->error')</script>";
				}
			}
		}

      if($res) {
         $script_var .= "<script>alert('수정이 완료되었습니다.');</script>";
      } else {
         $script_var .= "<script>alert('오류가 발생했습니다. 다시 시도해주세요.');</script>";
      }
   } 

   $my = sql_fetch_array("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = ?", array($_SESSION[user_no]), true);
   $my_ctt = explode('-', $my[mb_contact]);
   $isSNS = $_SESSION[is_facebook] || $_SESSION[is_twitter] ? true : false;
   $profile_img = get_profile_url($_SESSION[user_no]);
   $join_day = date('Y년 m월 d일 가입',strtotime($my[mb_date]));

   $_status = true;
?>
