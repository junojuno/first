<?php
   $_status = false;
   sec_session_start();
   if(!$_SESSION[user_no]) {
      alert('로그인을 해주세요', '/login?url='.$_SERVER[REQUEST_URI]);
   }

   $my = sql_fetch_array("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = ?", array($_SESSION[user_no]), true);

   $donate = sql_fetch_array("SELECT a.*, b.it_name, IF(a.od_method = '가상계좌' AND a.pay_remain != 0, a.od_escrow1, '0' ) AS account_info 
         FROM ".DB_ORDERS." a LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id WHERE a.it_id = '1376371774' and a.mb_no = ? ", array($_SESSION[user_no]));
   $donate_num = count($donate);
   $donate_amount = sql_fetch_array("SELECT SUM(od_amount - pay_remain) AS amount_sum FROM ".DB_ORDERS." WHERE mb_no = ? AND site_code = ?", array($_SESSION[user_no], SITE_CODE), true);

   $profile_img = get_profile_url($_SESSION[user_no]);
   $join_day = date('Y년 m월 d일 가입',strtotime($my[mb_date]));

   for($i =0; $i < count($donate); $i++) {
      if($donate[$i][account_info] != '0') {
         $info = explode('/', $donate[$i][account_info]);
         if(strtotime($info[3]) <= time()) {
            $donate[$i][is_past] = 'true';
            $donate[$i][account_content] = "<div class='account_ctt'><div class='past'>입금 기한이 지났습니다</div></div>";
         } else {
            $donate[$i][is_past] = 'false';
            $donate[$i][info_json] = "
               {\"bank\":\"$info[0]\",
               \"depositor\":\"$info[1]\",
               \"account\":\"$info[2]\",
               \"amount\":\"".number_format($donate[$i][od_amount])."원\",
               \"va_date\":\"".date('Y-m-d H:i:s',strtotime($info[3]))."\"
               }";
         }
         continue;
      }
   }


   $_status = true;
?>
