<?php
   $_status = true;
   if($_POST[join_email] == '' || $_POST[p] == '' || $_POST[join_name] == '') {
      $_status = false;
   }

   if($_status) {
      
      $chk = sql_fetch_array("SELECT mb_no FROM ".DB_MEMBERS." WHERE mb_id = ?", array($_POST[join_email]));
      if($chk[0][mb_no]) {
         $_status = false;
      }

      if($_status) {
         $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
         $password = hash('sha512', $_POST[p].$random_salt);
         $res = sql_query("INSERT INTO ".DB_MEMBERS." SET 
               mb_id = ?, mb_password = ?, mb_name = ?, mb_email = ?, salt = ?", array(
               $_POST[join_email], $password, $_POST[join_name], $_POST[join_email], $random_salt
               ));
         if(!$res) {
            $_status = false;
         }
      }
   }

   if(!$_status)  {
      $move_url = '/?layer=join';
   } else {
      login($_POST[join_email], $_POST[p]);
      $move_url = '/';
   }
?>
