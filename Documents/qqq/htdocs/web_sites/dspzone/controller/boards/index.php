<?php
   $script_var = "<script>var notice_now = 1, nanum_now = 1; var show_notice = false, show_nanum = false;</script>";

   if($_GET[notice]) {
      $notice_id = preg_match('/[0-9]+/', (int)$_GET[notice]) ? $_GET[notice] : false;
      $notice_id = $notice_id == '0' ? false : $notice_id;
   }
   if($_GET[nanum]) {
      $nanum_id = preg_match('/[0-9]+/', (int)$_GET[nanum]) ? $_GET[nanum] : false;
      $nanum_id = $nanum_id == '0' ? false : $nanum_id;
   }
   
   $row_num = 10;

   $notices = sql_fetch_array("SELECT * FROM ".DB_NOTICE." WHERE site_code = ? AND type = 1 ORDER BY upload_time DESC", array(SITE_CODE));
   $notice_num = count($notices);
   $notice_page_num = ceil($notice_num/2);
   $notice_count = 0;
   if($notice_id) {
      $script_var .= "<script>show_notice = $notice_id</script>";
   }

   $nanums = sql_fetch_array("SELECT * FROM ".DB_NOTICE." WHERE site_code = ? AND type = 2 ORDER BY upload_time DESC", array(SITE_CODE));
   $nanum_num = count($nanums);
   $nanum_page_num = ceil($nanum_num/10);
   $nanum_count = 0;
   if($nanum_id) {
      $script_var .= "<script>show_nanum = $nanum_id</script>";
   }
   
?>
