<?
require INCLUDE_ROOT.'/lib/pp_ax_hub_lib.php';

sec_session_start();
$_status = false;

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
   alert('오류가 발생했습니다','/campaign/'.$_POST[it_id].'/donate');
	exit;
}
$_SESSION[token] = '';

if (!$_SESSION[user_no]) {
	alert('로그인이 필요한 페이지입니다.', '/?layer=login');
}


$connect_type = "PC";

$it_id		= $_POST["it_id"];
if (!preg_match("/^[0-9]{10}$/", $it_id)){
   alert('잘못된 캠페인 코드입니다.');  
}

//DSPZONE 캠페인 체크
$cam_chk = sql_fetch_array("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id = ?", array($it_id), true);
if(!$cam_chk[it_id] || !preg_match('/'.SITE_CODE.'/', $cam_chk[share_codes])) {
   alert('잘못된 캠페인 코드입니다.');
}
$diff = round((strtotime($cam_chk[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
if($diff < 0) {
   alert('참여 기간이 지났습니다.');
}
$ticket_chk = sql_fetch_array("SELECT COUNT(od_id) AS donate_num 
            FROM ".DB_ORDERS." a 
            WHERE a.it_id = ?", array($it_id), true);
if($ticket_chk[donate_num] >= ($cam_chk[it_target]/5000)-32) {
   alert('남아있는 티켓이 없습니다 T^T', '/campaign/'.$it_id);
}

$donate_chk = sql_fetch_array("SELECT od_id FROM ".DB_ORDERS." WHERE it_id = ? AND mb_no = ?", array($it_id, (int)$_SESSION[user_no]), true);
if($donate_chk[od_id]) {
   alert('이미 티켓을 구매하셨습니다. (1인 1매)');
}

$req_tx		= $_POST["req_tx"]; // 요청 종류
$tran_cd	= $_POST["tran_cd"]; // 처리 종류

$cust_ip	= getenv("REMOTE_ADDR"); // 요청 IP
$ordr_idxx	= $_POST["ordr_idxx"]; // 쇼핑몰 주문번호
if (!preg_match("/^[0-9]{15}$/", $ordr_idxx)) exit;

$good_name	= $_POST["good_name"]; // 상품명
$good_mny	= $_POST["good_mny"]; // 결제 총금액
if (!preg_match("/^[0-9]+$/", $good_mny) || $good_mny != '5000') {
   alert('정상적인 금액이 아닙니다.');
}
$connect_type = $_POST["connect_type"] == "MOBILE" ? "모바일" : "PC";

$res_cd		= "";                         // 응답코드
$res_msg	= "";                         // 응답메시지
$res_en_msg	= "";                         // 응답 영문 메세지
$tno		= $_POST["tno"]; // KCP 거래 고유 번호

$buyr_name	= $_POST["buyr_name"]; // 주문자명
$buyr_tel1	= trim($_POST["buyr_tel1"]); // 주문자 전화번호
$buyr_tel2	= $_POST["buyr_tel2"]; // 주문자 핸드폰 번호
$buyr_mail	= $_POST["buyr_mail"]; // 주문자 E-mail 주소

$isReceipt  = $_POST["isReceipt"] ? $_POST["isReceipt"] : 0;
$realname   = $_POST["realname"];
$receiptPno = ($isReceipt != 0) ? ($isReceipt == 1) ? $_POST["serial1"].'-'.$_POST["serial2"] : $_POST["permit1"].'-'.$_POST["permit2"].'-'.$_POST["permit3"] : false;
$serialno = "".$_POST["serial1"].$_POST["serial2"];
$permitno = "".$_POST["permit1"].$_POST["permit2"].$_POST["permit3"];

$od_isEvent	= "".$_POST["od_isEvent"];
$reward_req = "".$_POST["reward_req"];

$mod_type	= $_POST["mod_type"]; // 변경TYPE VALUE 승인취소시 필요
$mod_desc	= $_POST["mod_desc"]; // 변경사유

$use_pay_method	= $_POST["use_pay_method"]; // 결제 방법
$bSucc		= "";                         // 업체 DB 처리 성공 여부

$app_time	= "";                         // 승인시간 (모든 결제 수단 공통)
$amount		= "";                         // KCP 실제 거래 금액
$total_amount	= 0;                          // 복합결제시 총 거래금액

$card_cd	= "";                         // 신용카드 코드
$card_name	= "";                         // 신용카드 명
$app_no		= "";                         // 신용카드 승인번호
$noinf		= "";                         // 신용카드 무이자 여부
$quota		= "";                         // 신용카드 할부개월
$partcanc_yn	= "";						  // 부분취소 가능유무
$card_bin_type_01 = "";                       // 카드구분1
$card_bin_type_02 = "";                       // 카드구분2

$bank_name      = "";                         // 은행명
$bank_code      = "";						  // 은행코드
$bk_mny         = "";

$pnt_issue      = "";                         // 결제 포인트사 코드
$pt_idno        = "";                         // 결제 및 인증 아이디
$pnt_amount     = "";                         // 적립금액 or 사용금액
$pnt_app_time   = "";                         // 승인시간
$pnt_app_no     = "";                         // 승인번호
$add_pnt        = "";                         // 발생 포인트
$use_pnt        = "";                         // 사용가능 포인트
$rsv_pnt        = "";                         // 총 누적 포인트

$commid         = "";                         // 통신사 코드
$mobile_no      = "";                         // 휴대폰 번호

$cash_yn        = $_POST["cash_yn"]; // 현금영수증 등록 여부
$cash_authno    = "";                         // 현금 영수증 승인 번호
$cash_tr_code   = $_POST["cash_tr_code"]; // 현금 영수증 발행 구분
$cash_id_info   = $_POST["cash_id_info"]; // 현금 영수증 등록 번호

// 인스턴스 생성 및 초기화
$c_PayPlus = new C_PP_CLI;
$c_PayPlus->mf_clear();

// 승인 요청
if ( $req_tx == "pay" ) {
// 결제 유효성 검사. 원금
//	$c_PayPlus->mf_set_ordr_data( "ordr_mony",  "1004" );
	$c_PayPlus->mf_set_encx_data( $_POST["enc_data" ], $_POST["enc_info" ] );
}

// 취소/매입 요청
else if ( $req_tx == "mod" ) {
	$tran_cd = "00200000";
	$c_PayPlus->mf_set_modx_data( "tno",      $tno      ); // KCP 원거래 거래번호
	$c_PayPlus->mf_set_modx_data( "mod_type", $mod_type ); // 원거래 변경 요청 종류
	$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip  ); // 변경 요청자 IP
	$c_PayPlus->mf_set_modx_data( "mod_desc", $mod_desc ); // 변경 사유
}

/* =   04. 실행                                                                 = */
if ( $tran_cd != "" ) {
	$c_PayPlus->mf_do_tx( $trace_no, $g_conf_home_dir, $g_conf_site_cd, $g_conf_site_key, $tran_cd, "",
	$g_conf_gw_url, $g_conf_gw_port, "payplus_cli_slib", $ordr_idxx,
	$cust_ip, "3" , 0, 0, $g_conf_key_dir, $g_conf_log_dir); // 응답 전문 처리

	$res_cd  = $c_PayPlus->m_res_cd;  // 결과 코드
	$res_msg = $c_PayPlus->m_res_msg; // 결과 메시지
	/* $res_en_msg = $c_PayPlus->mf_get_res_data( "res_en_msg" );  // 결과 영문 메세지 */ 
}
else {
	$c_PayPlus->m_res_cd  = "9562";
	$c_PayPlus->m_res_msg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
}

/* =   05. 승인 결과 값 추출                                                    = */
if ( $req_tx == "pay" ) {
	if( $res_cd == "0000" ) {
		$tno       = $c_PayPlus->mf_get_res_data( "tno"       ); // KCP 거래 고유 번호
		$amount    = $c_PayPlus->mf_get_res_data( "amount"    ); // KCP 실제 거래 금액
		$pnt_issue = $c_PayPlus->mf_get_res_data( "pnt_issue" ); // 결제 포인트사 코드

		/* =   05-1. 신용카드 승인 결과 처리                                            = */
		if ( $use_pay_method == "100000000000" ) {
			$card_cd   = $c_PayPlus->mf_get_res_data( "card_cd"   ); // 카드사 코드
			$card_name = $c_PayPlus->mf_get_res_data( "card_name" ); // 카드 종류
			$app_time  = $c_PayPlus->mf_get_res_data( "app_time"  ); // 승인 시간
			$app_no    = $c_PayPlus->mf_get_res_data( "app_no"    ); // 승인 번호
			$noinf     = $c_PayPlus->mf_get_res_data( "noinf"     ); // 무이자 여부 ( 'Y' : 무이자 )
			$quota     = $c_PayPlus->mf_get_res_data( "quota"     ); // 할부 개월 수
			$partcanc_yn = $c_PayPlus->mf_get_res_data( "partcanc_yn" ); // 부분취소 가능유무
			$card_bin_type_01 = $c_PayPlus->mf_get_res_data( "card_bin_type_01" ); // 카드구분1
			$card_bin_type_02 = $c_PayPlus->mf_get_res_data( "card_bin_type_02" ); // 카드구분2

         $card_name = iconv("euc-kr", "utf-8", $card_name);

			if ( $pnt_issue == "SCSK" || $pnt_issue == "SCWB" ) {
				$pt_idno      = $c_PayPlus->mf_get_res_data ( "pt_idno"      ); // 결제 및 인증 아이디    
				$pnt_amount   = $c_PayPlus->mf_get_res_data ( "pnt_amount"   ); // 적립금액 or 사용금액
				$pnt_app_time = $c_PayPlus->mf_get_res_data ( "pnt_app_time" ); // 승인시간
				$pnt_app_no   = $c_PayPlus->mf_get_res_data ( "pnt_app_no"   ); // 승인번호
				$add_pnt      = $c_PayPlus->mf_get_res_data ( "add_pnt"      ); // 발생 포인트
				$use_pnt      = $c_PayPlus->mf_get_res_data ( "use_pnt"      ); // 사용가능 포인트
				$rsv_pnt      = $c_PayPlus->mf_get_res_data ( "rsv_pnt"      ); // 총 누적 포인트
				$total_amount = $amount + $pnt_amount;                          // 복합결제시 총 거래금액
			}
		}

		/* =   05-2. 계좌이체 승인 결과 처리                                            = */
		if ( $use_pay_method == "010000000000" ) {
			$app_time  = $c_PayPlus->mf_get_res_data( "app_time"   );  // 승인 시간
			$bank_name = $c_PayPlus->mf_get_res_data( "bank_name"  );  // 은행명
			$bank_code = $c_PayPlus->mf_get_res_data( "bank_code"  );  // 은행코드
			$bk_mny = $c_PayPlus->mf_get_res_data( "bk_mny" ); // 계좌이체결제금액
         $bank_name = iconv("euc-kr", "utf-8", $bank_name);
		}
       /* =   05-3. 가상계좌 승인 결과 처리                                            = */
      if ( $use_pay_method == "001000000000" )
      {
          $app_time  = $c_PayPlus->mf_get_res_data( "app_time"  ); // 승인 시간
          $bankname  = $c_PayPlus->mf_get_res_data( "bankname"  ); // 입금할 은행 이름
          $depositor = $c_PayPlus->mf_get_res_data( "depositor" ); // 입금할 계좌 예금주
          $account   = $c_PayPlus->mf_get_res_data( "account"   ); // 입금할 계좌 번호
          $va_date   = $c_PayPlus->mf_get_res_data( "va_date"   ); // 가상계좌 입금마감시간
          $bankname = iconv("euc-kr", "utf-8", $bankname);
          $depositor = iconv("euc-kr", "utf-8", $depositor);
      }

		/* =   05-5. 휴대폰 승인 결과 처리                                              = */
		if ( $use_pay_method == "000010000000" ) {
			$app_time  = $c_PayPlus->mf_get_res_data( "hp_app_time"  ); // 승인 시간
			$commid    = $c_PayPlus->mf_get_res_data( "commid"	     ); // 통신사 코드
			$mobile_no = $c_PayPlus->mf_get_res_data( "mobile_no"	 ); // 휴대폰 번호
		}

		/* =   05-7. 현금영수증 결과 처리                                               = */
		$cash_authno  = $c_PayPlus->mf_get_res_data( "cash_authno"  ); // 현금 영수증 승인 번호

	}
}

/* =   06. 승인 및 실패 결과 DB처리                                             = */
/* =       결과를 업체 자체적으로 DB처리 작업하시는 부분입니다.                 = */

if ( $req_tx == "pay" ) {
	if( $res_cd == "0000" ) {
		if ( $use_pay_method == "100000000000" ) {
			// 06-1-1. 신용카드
         $result = sql_query("INSERT INTO ".DB_ORDERS." SET 
               od_id = ?, it_id = ?, mb_no = ?, od_name = ?, od_hp = ?, od_zip1 = ?, od_zip2 = ?, 
               od_addr1 = ?, od_addr2 = ?, od_isEvent = ?, od_amount = ?, od_time = ?, od_ip = ?, od_method = ?, 
               od_escrow1 = ?, od_escrow2 = ?, od_tno = ?, isReceipt = ?, receiptName = ?, receiptPno = ?, reward = ?, connect = ?, site_code = ?
               ", array(
                  "$ordr_idxx", "$it_id", $_SESSION[user_no], "$buyr_name", "$buyr_tel1", "$_POST[od_zip1]", "$_POST[od_zip2]", 
                  "$_POST[od_addr1]", "$_POST[od_addr2]", "$od_isEvent", "$good_mny", "$app_time", "$cust_ip", '신용카드', 
                  "$card_cd / $card_name", "$app_no", "$tno", "$isReceipt", "$realname", "$receiptPno", "$reward_req", "$connect_type", SITE_CODE
                  ));

			if ( $pnt_issue == "SCSK" || $pnt_issue == "SCWB" ) {
				// 06-1-1-1. 복합결제(신용카드 + 포인트)
			}
		}
		if ( $use_pay_method == "010000000000" ) {
			// 06-1-2. 계좌이체
         $result = sql_query("INSERT INTO ".DB_ORDERS." SET 
               od_id = ?, it_id = ?, mb_no = ?, od_name = ?, od_hp = ?, od_zip1 = ?, od_zip2 = ?, 
               od_addr1 = ?, od_addr2 = ?, od_isEvent = ?, od_amount = ?, od_time = ?, od_ip = ?, od_method = ?, 
               od_escrow1 = ?, od_tno = ?, isReceipt = ?, receiptName = ?, receiptPno = ?, reward = ?, connect = ?, site_code = ?
               ", array(
                  "$ordr_idxx", "$it_id", $_SESSION[user_no], "$buyr_name", "$buyr_tel1", "$_POST[od_zip1]", "$_POST[od_zip2]", 
                  "$_POST[od_addr1]", "$_POST[od_addr2]", "$od_isEvent", "$good_mny", "$app_time", "$cust_ip", '계좌이체', 
                  "$bank_code / $bank_name", "$tno", "$isReceipt", "$realname", "$receiptPno", "$reward_req", "$connect_type", SITE_CODE
                  ));
		}
      if ( $use_pay_method == "001000000000" ) {
         // 가상계좌
         $result = sql_query("INSERT INTO ".DB_ORDERS." SET 
               od_id = ?, it_id = ?, mb_no = ?, od_name = ?, od_hp = ?, od_zip1 = ?, od_zip2 = ?, 
               od_addr1 = ?, od_addr2 = ?, od_isEvent = ?, od_amount = ?, od_time = ?, od_ip = ?, od_method = ?, 
               od_escrow1 = ?, od_tno = ?, isReceipt = ?, receiptName = ?, receiptPno = ?, reward = ?, pay_remain = ?, connect = ?, site_code = ?
               ", array(
                  "$ordr_idxx", "$it_id", $_SESSION[user_no], "$buyr_name", "$buyr_tel1", "$_POST[od_zip1]", "$_POST[od_zip2]", 
                  "$_POST[od_addr1]", "$_POST[od_addr2]", "$od_isEvent", "$good_mny", "$app_time", "$cust_ip", '가상계좌', 
                  "$bankname/$depositor/$account/$va_date", "$tno", "$isReceipt", "$realname", "$receiptPno", "$reward_req", "$good_mny", "$connect_type", SITE_CODE
                  ));
      }
		if ( $use_pay_method == "000010000000" ) {
			// 06-1-5. 휴대폰
         $result = sql_query("INSERT INTO ".DB_ORDERS." SET 
               od_id = ?, it_id = ?, mb_no = ?, od_name = ?, od_hp = ?, od_zip1 = ?, od_zip2 = ?, 
               od_addr1 = ?, od_addr2 = ?, od_isEvent = ?, od_amount = ?, od_time = ?, od_ip = ?, od_method = ?, 
               od_escrow1 = ?, od_escrow2 = ?, od_tno = ?, isReceipt = ?, receiptName = ?, receiptPno = ?, reward = ?, connect = ?, site_code = ?
               ", array(
                  "$ordr_idxx", "$it_id", $_SESSION[user_no], "$buyr_name", "$buyr_tel1", "$_POST[od_zip1]", "$_POST[od_zip2]", 
                  "$_POST[od_addr1]", "$_POST[od_addr2]", "$od_isEvent", "$good_mny", "$app_time", "$cust_ip", '핸드폰', 
                  "$commid", "$mobile_no", "$tno", "$isReceipt", "$realname", "$receiptPno", "$reward_req", "$connect_type", SITE_CODE
                  ));
		}
	}
	else if ( $res_cd != "0000" ) {
	/* =   06. 승인 실패 결과 DB처리                                             = */
	}
}

//좌석배치
if($result && $use_pay_method != "001000000000") {

   $info = sql_fetch_array("SELECT COUNT(od_id) AS rank FROM ".DB_ORDERS." WHERE it_id = ? AND pay_remain = 0 AND od_time <= ?", array($it_id, $app_time), true);
   $target_rank = $info[rank];
   include INCLUDE_ROOT.'/set_seat_var.php';
   $result = sql_query("UPDATE ".DB_ORDERS." SET var_1 = ? WHERE od_id = ?", array($seat_info, $ordr_idxx));
}

// 회원정보 업데이트
$serialno = $serialno ? ", mb_serial   = '$serialno' " : false;
$permitno = $permitno ? ", mb_permit   = '$permitno' " : false;

sql_query("UPDATE ".DB_MEMBERS." SET
mb_email = ?, mb_contact = ?, mb_zip1 = ?, mb_zip2 = ?, mb_addr1 = ?, mb_addr2 = ? WHERE mb_no = ?", array(
   $buyr_mail, $buyr_tel1, $_POST[od_zip1], $_POST[od_zip2], $_POST[od_addr1], $_POST[od_addr2], (int)$_SESSION[user_no]
   ));

// 뱃지 발급 루틴
$procBadges = array();

if ($result) {
	// 최초 기부(위젠 레벨) 체크
	$c = sql_fetch_array("
			SELECT COUNT(od_id) AS total
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			AND mb_no = ?
			", array($_SESSION[user_no]), true);
	if ($c[total] == 1) {
//		array_push($procBadges, 'level');
	}

	// 해당캠페인 최초 기부 체크
	$c = sql_fetch_array("
			SELECT COUNT(od_id) AS total
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			AND it_id = ?
			", array($it_id), true);
	if ($c[total] == 1) {
		array_push($procBadges, '1');
	}

	// 최후 기부 & 오버킬 기부 체크
	$c = sql_fetch_array("
			SELECT SUM(od_amount) AS re_sum, i.it_target
			FROM ".DB_ORDERS." o
			LEFT JOIN ".DB_CAMPAIGNS." i ON (o.it_id = i.it_id)
			WHERE o.it_id = ?
			GROUP BY o.it_id
			", array($it_id), true);

	$diff = $c[re_sum] - $c[it_target];

	// 모금액이 목표액 초과했을 경우
	if ($diff >= 0) {
		// 최후 기부 체크 (초과액수$diff가 기부액수보다 작을 경우)
		// 작거나 '같은' 경우를 체크할 경우 모금액과 목표액이 딱 맞춰진 상황에서 다음 1인에게도 뱃지가 중복적용됨
		if ($good_mny > $diff) {
			array_push($procBadges, '2');
		}
		// 오버킬 기부
		else {
			array_push($procBadges, '6');
		}
	}

	// 카테고리 뱃지 체크
	$c = sql_fetch_array("
			SELECT category
			FROM ".DB_CAMPAIGNS."
			WHERE it_id = ?
			", array($it_id), true);
	if (strpos($c[category], ',') === false) $category = $c[category];
	else $category = explode(', ', $c[category]) and $category = $category[0];

	if ($category == '역사/문화') { array_push($procBadges, '101'); }
	elseif ($category == '환경/동물보호') { array_push($procBadges, '102'); }
	elseif ($category == '아동/청소년') { array_push($procBadges, '103'); }
	elseif ($category == '노인') { array_push($procBadges, '104'); }
	elseif ($category == '장애인') { array_push($procBadges, '105'); }
	elseif ($category == '저소득가정') { array_push($procBadges, '106'); }
	elseif ($category == '다문화가정') { array_push($procBadges, '107'); }
	elseif ($category == '자활형 캠페인') { array_push($procBadges, '108'); }

	for ($i = 0; $i < count($procBadges); $i++) {
		$check = sql_fetch_array("
				SELECT mb_no
				FROM ".DB_BADGES."
				WHERE mb_no = ?
				AND category = ?
				AND it_id = ?
				", array($_SESSION[user_no], $procBadges[$i], $it_id), true);
		if (!$check[mb_no]) {
			$badge = "INSERT ".DB_BADGES."
                  SET mb_no		= ?,
							category		= ?,
							od_id			= ?,
							it_id			= ?,
							param			= ?
					";
               sql_query($badge, array(
                  $_SESSION[user_no], $procBadges[$i], $ordr_idxx, $it_id, $good_mny
                  ));
		}
		unset($check);
	}
}
// 뱃지루틴 끗

$result = $result ? "true" : "false";
$bSucc = $result; // DB 작업 실패 또는 금액 불일치의 경우 "false" 로 세팅
/* =   07-1. DB 작업 실패일 경우 자동 승인 취소                                 = */
if ( $req_tx == "pay" ) {
	if( $res_cd == "0000" ) {	
		if ( $bSucc === "false") {
			$c_PayPlus->mf_clear();

			$tran_cd = "00200000";

			$c_PayPlus->mf_set_modx_data( "tno",      $tno                         );  // KCP 원거래 거래번호
			$c_PayPlus->mf_set_modx_data( "mod_type", "STSC"                       );  // 원거래 변경 요청 종류
			$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip                     );  // 변경 요청자 IP
			$c_PayPlus->mf_set_modx_data( "mod_desc", "결과 처리 오류 - 자동 취소" );  // 변경 사유

			$c_PayPlus->mf_do_tx( $tno,  $g_conf_home_dir, $g_conf_site_cd, $g_conf_site_key, $tran_cd, "", $g_conf_gw_url,  $g_conf_gw_port, "payplus_cli_slib", $ordr_idxx, $cust_ip, "3", 0, 0, $g_conf_key_dir, $g_conf_log_dir);

			$res_cd  = $c_PayPlus->m_res_cd;
			$res_msg = $c_PayPlus->m_res_msg;
		}
	}
}

$res_msg = iconv("euc-kr", "utf-8", $res_msg);
$card_name = iconv("euc-kr", "utf-8", $card_name);
$_status = true;
?>
