<?php
   $_status = true;
   $it_id = $_GET[it_id];
   $cam_chk = sql_fetch_array("SELECT * FROM ".DB_CAMPAIGNS." i WHERE i.it_id = ?", array($it_id), true);
                     
   if(!$cam_chk[it_id] || !preg_match('/'.SITE_CODE.'/', $cam_chk[share_codes])) {
      $_status = false;
      alert('잘못된 캠페인 코드입니다.');
   }
   
   $inner = "<img src='http://$_SERVER[HTTP_HOST]/images/campaign/kara/poster.jpg' href='/campaign/$it_id'>";


   $donate_info = sql_fetch_array("SELECT *, COUNT(od_id) AS donate_num
                     FROM ".DB_ORDERS." a 
                     WHERE a.it_id = ? AND pay_remain = 0", array($it_id), true);
   $target = $cam_chk[it_target]/5000;
   $now = $target < $donate_info[donate_num] ? $target : $donate_info[donate_num];
   $gauge_pct = ($now/$target)*100;
   $it_name = $cam_chk[it_name];
   $it_shortdesc = $cam_chk[it_shortdesc];

   $script_var = "
      <script>
         var it_id = '$it_id';
         var it_name = '$it_name';
         var it_shortdesc = '$it_shortdesc';
         var img_server = '".IMG_HOST."';
      </script>
   ";
?>
