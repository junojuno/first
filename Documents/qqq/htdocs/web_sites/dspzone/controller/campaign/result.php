<?php
$_status = false;
$it_id			  = $_POST[ "it_id"          ];
$site_cd          = $_POST[ "site_cd"        ];      // 사이트코드
$req_tx           = $_POST[ "req_tx"         ];      // 요청 구분(승인/취소)
$use_pay_method   = $_POST[ "use_pay_method" ];      // 사용 결제 수단
$bSucc            = $_POST[ "bSucc"          ];      // 업체 DB 정상처리 완료 여부
/* = -------------------------------------------------------------------------- = */
$res_cd           = $_POST[ "res_cd"         ];      // 결과코드
$res_msg          = $_POST[ "res_msg"        ];      // 결과메시지
$res_msg_bsucc    = "";
/* = -------------------------------------------------------------------------- = */
$ordr_idxx        = $_POST[ "ordr_idxx"      ];      // 주문번호
$tno              = $_POST[ "tno"            ];      // KCP 거래번호
$good_mny         = $_POST[ "good_mny"       ];      // 결제금액
$good_name        = $_POST[ "good_name"      ];      // 상품명
$buyr_name        = $_POST[ "buyr_name"      ];      // 구매자명
$buyr_tel1        = $_POST[ "buyr_tel1"      ];      // 구매자 전화번호
$buyr_tel2        = $_POST[ "buyr_tel2"      ];      // 구매자 휴대폰번호
$buyr_mail        = $_POST[ "buyr_mail"      ];      // 구매자 E-Mail
/* = -------------------------------------------------------------------------- = */
// 공통
$pnt_issue        = $_POST[ "pnt_issue"      ];      // 포인트 서비스사
$app_time         = $_POST[ "app_time"       ];      // 승인시간 (공통)
/* = -------------------------------------------------------------------------- = */
// 신용카드
$card_cd          = $_POST[ "card_cd"        ];      // 카드코드
$card_name        = $_POST[ "card_name"      ];      // 카드명
$noinf			  = $_POST[ "noinf"          ];      // 무이자 여부
$quota            = $_POST[ "quota"          ];      // 할부개월
$app_no           = $_POST[ "app_no"         ];      // 승인번호
/* = -------------------------------------------------------------------------- = */
// 계좌이체
$bank_name        = $_POST[ "bank_name"      ];      // 은행명
$bank_code        = $_POST[ "bank_code"      ];      // 은행코드
/* = -------------------------------------------------------------------------- = */
// 가상계좌
$bankname         = $_POST[ "bankname"       ];      // 입금할 은행
$depositor        = $_POST[ "depositor"      ];      // 입금할 계좌 예금주
$account          = $_POST[ "account"        ];      // 입금할 계좌 번호
$va_date		      = $_POST[ "va_date"        ];      // 가상계좌 입금마감시간
/* = -------------------------------------------------------------------------- = */
// 포인트
$pt_idno          = $_POST[ "pt_idno"        ];      // 결제 및 인증 아이디
$add_pnt          = $_POST[ "add_pnt"        ];      // 발생 포인트
$use_pnt          = $_POST[ "use_pnt"        ];      // 사용가능 포인트
$rsv_pnt          = $_POST[ "rsv_pnt"        ];      // 총 누적 포인트
$pnt_app_time     = $_POST[ "pnt_app_time"   ];      // 승인시간
$pnt_app_no       = $_POST[ "pnt_app_no"     ];      // 승인번호
$pnt_amount       = $_POST[ "pnt_amount"     ];      // 적립금액 or 사용금액
/* = -------------------------------------------------------------------------- = */
//상품권
$tk_van_code	  = $_POST[ "tk_van_code"    ];      // 발급사 코드
$tk_app_no		  = $_POST[ "tk_app_no"      ];      // 승인 번호
/* = -------------------------------------------------------------------------- = */
//휴대폰
$commid			  = $_POST[ "commid"		 ];      // 통신사 코드
$mobile_no		  = $_POST[ "mobile_no"      ];      // 휴대폰 번호
/* = -------------------------------------------------------------------------- = */
// 현금영수증
$cash_yn          = $_POST[ "cash_yn"        ];      //현금영수증 등록 여부
$cash_authno      = $_POST[ "cash_authno"    ];      //현금영수증 승인 번호
$cash_tr_code     = $_POST[ "cash_tr_code"   ];      //현금영수증 발행 구분
$cash_id_info     = $_POST[ "cash_id_info"   ];      //현금영수증 등록 번호
/* = -------------------------------------------------------------------------- = */

$seat_info        = $_POST[ "seat_info" ];
$req_tx_name = "";

if( $req_tx == "pay" ) {
	$req_tx_name = "후원";
}
else if( $req_tx == "mod" ) {
	$req_tx_name = "매입/취소";
}
else {
	$req_tx_name = '봉사활동 신청';
}

if ($req_tx == "pay") {
	if($bSucc == "false") {
      if($res_cd == '0000') {
         alert('결제 결과를 처리하는 중 오류가 발생하여 결제 취소 요청 되었습니다.\n다시 시도해주시고 만약 계속 오류가 발생한다면 dspzone@wegen.kr로 문의해주시기 바랍니다.','/campaign/'.$it_id);
      } else {
         alert('결제 결과를 처리하는 중 오류가 발생하여 결제 취소 요청을 했지만, 취소 요청에 실패했습니다. 반드시 dspzone@wegen.kr로 문의해주시기 바랍니다.');
      } 
   } 
}

$script_var = "<script>var ordr_idxx = '$ordr_idxx', send_flag = false, account_flag = false;</script>";
if($bSucc != "false" && $res_cd == "0000" && $use_pay_method == "001000000000" ) { 
   $od = sql_fetch_array("SELECT od_escrow1 FROM ".DB_ORDERS." WHERE od_id = ?", array($ordr_idxx),true);
   $escrow1 = explode("/",$od[od_escrow1]);
   $chk = $escrow1[4] == "1" ? true : false;
   if(!$chk) $script_var .= "<script>account_flag = true;</script>";
}

if($bSucc != 'false' && $res_cd == "0000" && $use_pay_method != "001000000000") $script_var .= "<script>send_flag = true;</script>"; 


$_status = true;

?>
