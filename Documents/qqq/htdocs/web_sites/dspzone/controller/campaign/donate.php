<?php
   sec_session_start();
   $_status = true;

   if(!$_SESSION[user_no]) {
      $_status = false;
      alert('로그인을 해주세요.','/login?url='.urlencode($_SERVER[REQUEST_URI]));
   }
   $it_id = $_GET[it_id];
   $cam_chk = sql_fetch_array("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id = ?", array($it_id), true);

   if(!$cam_chk[it_id] || !preg_match('/'.SITE_CODE.'/', $cam_chk[share_codes])) {
      $_status = false;
      alert('잘못된 캠페인 코드입니다.');
   }

   $diff = round((strtotime($cam_chk[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
   if($diff < 0) {
      $_status = false;
      alert('참여 기간이 지났습니다.');
   }

   $ticket_chk = sql_fetch_array("SELECT COUNT(od_id) AS donate_num 
               FROM ".DB_ORDERS." a 
               WHERE a.it_id = ?", array($it_id), true);
   if($ticket_chk[donate_num] >= ($cam_chk[it_target]/5000)-32) {
      $_status = false;
      alert('남아있는 티켓이 없습니다 T^T');
   }

   $donate_chk = sql_fetch_array("SELECT od_id FROM ".DB_ORDERS." WHERE it_id = ? AND mb_no = ?", array($it_id, (int)$_SESSION[user_no]), true);
   if($donate_chk[od_id]) {
      $_status = false;
      alert('이미 티켓을 구매하셨습니다. (1인 1매)');
   }

   $my = sql_fetch_array("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = ?", array((int)$_SESSION[user_no]), true);
   $my_ctt = explode('-', $my[mb_contact]);


   if($isMobile) {
       $req_tx          = $_POST[ "req_tx"         ]; // 요청 종류         
       $res_cd          = $_POST[ "res_cd"         ]; // 응답 코드         
       $tran_cd         = $_POST[ "tran_cd"        ]; // 트랜잭션 코드     
       $ordr_idxx       = $_POST[ "ordr_idxx"      ]; // 쇼핑몰 주문번호   
       $good_name       = $_POST[ "good_name"      ]; // 상품명            
       $good_mny        = $_POST[ "good_mny"       ]; // 결제 총금액       
       $buyr_name       = $_POST[ "buyr_name"      ]; // 주문자명          
       $buyr_tel1       = $_POST[ "buyr_tel1"      ]; // 주문자 전화번호   
       $buyr_tel2       = $_POST[ "buyr_tel2"      ]; // 주문자 핸드폰 번호
       $buyr_mail       = $_POST[ "buyr_mail"      ]; // 주문자 E-mail 주소
       $use_pay_method  = $_POST[ "use_pay_method" ]; // 결제 방법         
       $ipgm_date       = $_POST[ "ipgm_date"      ]; // 가상계좌 마감시간 
       $enc_info        = $_POST[ "enc_info"       ]; // 암호화 정보       
       $enc_data        = $_POST[ "enc_data"       ]; // 암호화 데이터     
       $van_code        = $_POST[ "van_code"       ];
       $cash_yn         = $_POST[ "cash_yn"        ];
       $cash_tr_code    = $_POST[ "cash_tr_code"   ];
   }


   $a = $isMobile? 'true' : 'false';
   $script_var = "
      <script>
         var g_conf_js_url = '$g_conf_js_url';
         var is_mobile = '$a';
      </script>
   ";


?>
