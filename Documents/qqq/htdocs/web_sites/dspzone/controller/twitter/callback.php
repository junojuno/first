<?php
   sec_session_start();
   require_once(INCLUDE_ROOT.'/lib/twitter/twitteroauth.php');

   /* If the oauth_token is old redirect to the connect page. */
   if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
      $_SESSION['oauth_status'] = 'oldtoken';
      session_destroy();
      alert('세션이 올바르지 않습니다. 다시 로그인 해주세요.','/twitter/login');
      header('Location: /twitter/login');
   }

   /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
   $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

   /* Request access tokens from twitter */
   $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

   /* Save the access tokens. Normally these would be saved in a database for future use. */
   $_SESSION['access_token'] = $access_token;

   /* Remove no longer needed request tokens */
   unset($_SESSION['oauth_token']);
   unset($_SESSION['oauth_token_secret']);

   /* If HTTP response is 200 continue otherwise send to connect page to retry */
   if (200 == $connection->http_code) {
      /* The user has been verified and the access tokens can be saved for future use */
      $_SESSION['status'] = 'verified';
      $user_profile = $connection->get('account/verify_credentials');
      $user_profile = (array)$user_profile;

      if(!$_SESSION[user_no]) {
         $pwdstr = hash('sha512', $user_profile[id].'TryTwiTTeRsessionLOGin');
         $membercheck = login('tw@'.$user_profile[id], $pwdstr);

         // if not member, build member profile
         if ($membercheck === 'NOTUSER') {
            $password = hash('sha512', $user_profile[id].'TryTwiTTeRsessionLOGin');
            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $password = hash('sha512', $password.$random_salt);
            $twid = 'tw@'.$user_profile[id];

            $res = sql_query("INSERT INTO ".DB_MEMBERS." SET 
               mb_id = ?, mb_name = ?, mb_password = ?, tw_icon = ?, salt = ?", array(
               $twid, $user_profile[name], $password, $user_profile[profile_image_url], $random_salt   
            ));
            
            if($res)
               login('tw@'.$user_profile[id], hash('sha512', $user_profile[id].'TryTwiTTeRsessionLOGin'));
         }

         $closed = true;
      }
   } else {
      /* Save HTTP status for error dialog on connnect page.*/
      session_destroy();
      alert('오류가 발생했습니다. 다시 로그인 해주세요','/twitter/login');
   }
?>
