<?php
   $mysql[host] = ($_debug) ? 'localhost' : 'localhost';
   $mysql[user] = ($_debug) ? 'root' : 'wegenkr';
   $mysql[password] = ($_debug) ? 'wotjdWkd' : '!wegenkr08642';
   $mysql[database] = ($_debug) ? 'wegenkr' : 'wegenkr';

   try {
      $dbh = new PDO('mysql:host='.$mysql[host].';dbname='.$mysql[database], $mysql[user], $mysql[password], array(
         PDO::ATTR_PERSISTENT => true,
         PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
      ));
      $dbh->exec("SET NAMES utf8");
   } catch(PDOException $e) {
      die("<p>".$e->getMessage()."</p>");
   }

   function sql_query($sql, $params = null) {
      global $dbh;
      try {
         $stmt = $dbh->prepare($sql);
         $res = $stmt->execute($params);
      } catch(PDOException $e) {
         die("<p>$sql</p><p>".$e->getMessage()."</p><p>file : $_SERVER[PHP_SELF]</p>");
      }
      return $res;
   }

   function sql_fetch_array($sql, $params = null, $one_ret = false) {
      global $dbh;
      try {
         $stmt = $dbh->prepare($sql);
         $stmt->execute($params);
      } catch(PDOException $e) {
         die("<p>$sql</p><p>".$e->getMessage()."</p><p>file : $_SERVER[PHP_SELF]</p>");
      }
      $ret = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $ret = $one_ret ? $ret[0] : $ret;
      return $ret;
   }
?>
