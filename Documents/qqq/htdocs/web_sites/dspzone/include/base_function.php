<?php
function sec_session_start($regen = true) {
   $session_name = 'sec_session_id'; // Set a custom session name
   $secure = false; // true if using https

   ini_set('session.use_only_cookies', 1);
   $cookieParams = session_get_cookie_params();
   session_set_cookie_params(time()+60*60*24*30, $cookieParams['path'], $cookieParams['domain'], $secure, true); 
   session_name($session_name);
   session_start();
   if($regen) {
      session_regenerate_id(true);
   }
}

function login($email, $password) {
	$account = explode('@',$email);
	$password_original = $password;
   
   $data = sql_fetch_array("SELECT * FROM ".DB_MEMBERS." WHERE mb_id = ? LIMIT 1", array($email));
   $mb_info = $data[0];
   if($mb_info[mb_no]) {
      $password = hash('sha512', $password.$mb_info[salt]);
      if($mb_info[quit] == 1) {
         return false;
      } else {
         if($mb_info[mb_password] == $password) {
            $user_browser = $_SERVER['HTTP_USER_AGENT'];
            $_SESSION['user_no'] = preg_replace('/[^0-9]+/', '', $mb_info[mb_no]); // XSS protection
            $_SESSION['username'] = htmlspecialchars(strip_tags($mb_info[mb_name])); // XSS protection
            $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
            $_SESSION['is_facebook'] = $account[0] == 'fb' ? true : false;
            $_SESSION['is_twitter'] = $account[0] == 'tw' ? true : false;
            $_SESSION['is_admin'] = $mb_info[isAdmin] == 1 ? true : false;

            // 자동로그인을 위한 쿠키저장
            if(!isset($_COOKIE[wegen_id]) || !isset($_COOKIE[wegen_pw])) {
               setcookie('wegen_id',$email,time()+60*60*24*30,'/');
               setcookie('wegen_pw',$password_original,time()+60*60*24*30,'/');
            }
            return true;
         } else {
            //not correct
            $chk_again = sql_fetch_array("SELECT * FROM ".DB_MEMBERS." WHERE mb_id = '$email' AND mb_password_old = password('$password_original')");
            $mb_info = $chk_again[0];
            if($mb_info[mb_no]){
               sql_query("UPDATE ".DB_MEMBERS." SET mb_password = '$password' WHERE mb_id = '$email'");
               $user_browser = $_SERVER['HTTP_USER_AGENT'];
               $_SESSION['user_no'] = preg_replace('/[^0-9]+/', '', $mb_info[mb_no]); // XSS protection
               $_SESSION['username'] = htmlspecialchars(strip_tags($mb_info[mb_name])); // XSS protection
               $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
               $_SESSION['is_facebook'] = $account[0] == 'fb' ? true : false;
               $_SESSION['is_twitter'] = $account[0] == 'tw' ? true : false;
               $_SESSION['is_admin'] = $mb_info[isAdmin] == 1 ? true : false;

               // 자동로그인을 위한 쿠키저장
               if(!isset($_COOKIE[wegen_new_cookie]) || !isset($_COOKIE[wegen_id]) || !isset($_COOKIE[wegen_pw])) {
                  setcookie('wegen_new_cookie','0.0',time()+60*60*24*30,'/');
                  setcookie('wegen_id',$email,time()+60*60*24*30,'/');
                  setcookie('wegen_pw',$password_original,time()+60*60*24*30,'/');
               }
               return true;
            }
            return false;
         }
      }
   } else {
      return "NOTUSER";
   }
   return false;
}
function alert($msg, $url = null) {
	$str = $url ? "document.location.href = '$url';" : "history.back();";
	print "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert('$msg'); $str </script>";
	exit;
}

function json_fail() {
   global $json_ret;
   $json_ret[response] = 'failed';
   echo json_encode($json_ret);
   exit;
}

function get_profile_url($uno) {
	$data = sql_fetch_array("
			SELECT mb_id, mb_name, mb_icon, tw_icon, fb_profile, tw_profile
			FROM ".DB_MEMBERS."
			WHERE mb_no = ?
			", array($uno), true);
   $head = "http://".IMG_HOST;
	$id = explode('@', $data[mb_id]);
	if($id[0] == 'fb') {
		$url = $data[mb_icon] != '' ? $head."/data/member/".$data[mb_icon] : ($data[fb_profile] == 1 ? "https://graph.facebook.com/$id[1]/picture" : $head.'/images/common/no_photo.png');
	} else if($id[0] == 'tw') {
		$url = $data[mb_icon] != '' ? $head."/data/member/".$data[mb_icon] : ($data[tw_profile] == 1 ? ($data[tw_icon] ? $data[tw_icon] : $head.'/images/common/no_photo.png') : $head.'/images/common/no_photo.png');
	} else {
		$url = $data[mb_icon] != '' ? $head.'/data/member/'.$data[mb_icon] : $head.'/images/common/no_photo.png';
	}
   return $url.'?'.rand(0,999);
}


?>
