<?php
   $_debug = ($_SERVER[HTTP_HOST] == '14.47.134.182' || $_SERVER[HTTP_HOST] == '14.47.134.182:81') ? true : false;
   /* GLOBAL VARIABLES */
   define(DB_MEMBERS, 'wegen_member');
   define(DB_CAMPAIGNS, 'wegen_campaign');
   define(DB_BANNER, 'wegen_banner');
   define(DB_CAMPAIGN_CMTS, 'wegen_campaign_comment');
   define(DB_ORDERS, 'wegen_donation');
   define(DB_COINS, 'wegen_coin');
   define(DB_BADGES, 'wegen_badge');
   define(DB_PARTNERS, 'wegen_partner');
   define(DB_FUNDRAISERS, 'wegen_fundraiser');
   define(DB_POSTSCRIPTS, 'wegen_event');
   define(DB_REGULARPAYMENT, 'wegen_regularpayment');
   define(DB_MEDIA, 'wegen_media');
   define(DB_ACTIVITY, 'wegen_activity');
   define(DB_INFO, 'wegen_info');
   define(DB_SPONSORS, 'wegen_sponsor');
   define(DB_LINKS, 'wegen_link');
   define(DB_REWARDS, 'wegen_reward');
   define(DB_REWARDS_COUNT, 'wegen_reward_count');
   define(DB_CMT_LIKES,'wegen_comment_like');
   define(DB_SMS_LOG, 'wegen_sms_log');
   define(DB_MAIL_LOG, 'wegen_mail_log');
   define(DB_AUCTION, 'wegen_auction');
   define(DB_BIDDING, 'wegen_auction_bid');
   define(DB_NOTICE, 'wegen_notice');

   define(DB_CAMPAIGNS_EN, 'wegen_campaign_en');
   define(DB_PARTNERS_EN, 'wegen_partner_en');
   define(DB_FUNDRAISERS_EN, 'wegen_fundraiser_en');

   define(WEB_ROOT, $_SERVER[DOCUMENT_ROOT]);
   define(REQ_URI, $_SERVER[REQUEST_URI]);

   define(VIEW_ROOT, WEB_ROOT.'/view');
   define(INCLUDE_ROOT, WEB_ROOT.'/include');
   define(CSS_ROOT, WEB_ROOT.'/css');
   define(JS_ROOT, WEB_ROOT.'/js');
   define(CTR_ROOT, WEB_ROOT.'/controller');
   define(MOD_ROOT, WEB_ROOT.'/module');
   define(LAYOUT_ROOT, MOD_ROOT.'/layout');

   define(SCRIPT_NAME, substr($_SERVER[SCRIPT_NAME], 5));

   define(MUTEX_KEY, 920319);

   $img_host = $_debug ? '14.47.134.182:81' : 'wegen.kr';
   $data_path = $_debug ? '/var/www/wegen/data' : '/home/hosting_users/wegenkr/www/data';
   define(IMG_HOST, $img_host);
   define(DATA_PATH, $data_path);

   $settings[title] = '';
   /*
    * 101 : wegenkr@admin.kr 
    * 3850 : hellojeju@hellojeju.com
    */
   /* FACEBOOK */
   $fb_appid = $_debug ? '408565205831331' : '636087649795432';
   $fb_secret = $_debug ? '3c5847c2d4bb7bb294d874e7b4c0b75a' : '3cbadbad9fa45b0cfb9ac178b175ef01';
   define('FACEBOOK_APP_ID', $fb_appid);
   define('FACEBOOK_APP_SECRET', $fb_secret);

   /* TWITTER API SETTINGS */
   $tw_key = $_debug ? 'mHjT9yupoSagvjA73XEQzne8r' : 'yM12D4ymbR2AFfUlkvG14iZHv';
   $tw_secret = $_debug ? 'x8cM9DQole4v8BBdn292FtgvwwLaGvuhBrlrvSs14NSBb8HeaV' : 'Sj2qljRJK9zroqCiFhqxKSN0AnCWbpPcsrvnaXUxkUrxyGyMYG';
   $tw_cb = $_debug ? "http://$_SERVER[HTTP_HOST]/twitter/callback" : 'http://dsp.dspzone.co.kr/twitter/callback';
   define('CONSUMER_KEY', $tw_key);
   define('CONSUMER_SECRET', $tw_secret);
   define('OAUTH_CALLBACK', $tw_cb);

   /* KCP SETTINGS */
   $g_conf_site_name	= 'DSPZONE_WEGEN';
   $g_conf_log_level	= '3';
   $g_conf_gw_port	= '8090';
   $g_conf_mode		= 0;		// 배치결제
   $g_conf_home_dir	= ($_debug) ? '/var/www/kcp/dspzone' : '/home/hosting_users/wegenkr/kcp/dspzone';
   $g_conf_gw_url		= ($_debug) ? 'testpaygw.kcp.co.kr' : 'paygw.kcp.co.kr'; // testpaygw.kcp | paygw.kcp
   $g_conf_js_url		= ($_debug) ? 'http://pay.kcp.co.kr/plugin/payplus_test_un.js' : 'http://pay.kcp.co.kr/plugin/payplus_un.js'; // payplus_test_un.js | payplus_un.js
   $g_wsdl           = ($_debug) ? "KCPPaymentService.wsdl" : "real_KCPPaymentService.wsdl";
   $module_type		= '01';

   $g_conf_site_cd		= ($_debug) ? 'T0000' : 'B1460';
   $g_conf_site_key	= ($_debug) ? '3grptw1.zW0GSo4PQdaGvsF__' : '4ebhAZlW7nSuwqfe-T3HB1Y__';

   /* Mobile Check */
   include INCLUDE_ROOT."/lib/Mobile_Detect.php";
   $detect = new Mobile_Detect;
   $isMobile = $detect->isMobile();
   $isTablet = $detect->isTablet();

   /* etc */
   define(SITE_CODE, "A001");
?>
