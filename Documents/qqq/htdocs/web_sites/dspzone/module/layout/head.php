<?php
   $setting[title] = $setting[title] == '' ? 'DSP ZONE' : $setting[title].' - DSP ZONE';
   sec_session_start();

   if(!$_SESSION[user_no]) {
      if(isset($_COOKIE[wegen_id]) && isset($_COOKIE[wegen_pw])) {
         login($_COOKIE[wegen_id],$_COOKIE[wegen_pw],$mysqli);
      }
   }
   if(preg_match('/^\/login/', $_SERVER[REQUEST_URI])) {
      if($_SESSION[user_no]) {
         $move = true;
         $move_url = $move_url ? $move_url : $_GET[url];
      }  
   }
   if(preg_match('/^\/dspzone/',$_SERVER[REQUEST_URI])) {
      $li_2 = 'selected';
   } else if(preg_match('/^\/campaign/',$_SERVER[REQUEST_URI])) {
      $li_1 = 'selected';
   } else if(preg_match('/^\/boards/',$_SERVER[REQUEST_URI])) {
      $li_3 = 'selected';
   }

   $cam = sql_fetch_array("SELECT it_id, it_youtube FROM ".DB_CAMPAIGNS." WHERE share_codes = ?", array(SITE_CODE), true);
   $it_id = $cam[it_id];
?>
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8">
   <title><?=$setting[title]?></title>
   <?=$isMobile? '<meta name="viewport" content="width=device-width, user-scalable=1.0, initial-scale=1.0">' : false ?>
   <link rel='stylesheet' href='/css<?=str_replace('.php','.css',SCRIPT_NAME)?>' /> 
   <link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
   <?=$script_var?>
   <script> 
      var js_file = '<?=file_exists(JS_ROOT.str_replace('.php','.js', SCRIPT_NAME)) ? '/js'.str_replace('.php','.js',SCRIPT_NAME) : false ?>';
      var http_host = '<?=$_SERVER[HTTP_HOST]?>';
   </script>
<? if(preg_match('/donate/', $_SERVER[REQUEST_URI])) { ?>
   <? if(!$isMobile) { ?>
      <script src='<?=$g_conf_js_url?>'></script>
      <script>StartSmartUpdate();</script>
   <? } else { ?>
      <script src="/module/kcp_mobile/approval_key.js"></script>
   <? } ?>
<? } ?>
   <script src='/js/scripts.js'></script>
   <!-- <link rel='shortcut icon' type='image/png' href='/images/favicon.png' /> -->
</head>
<body>
<div id='fb-root'></div>
<div id='container'>
   
   <? if(!$_SESSION[user_no]) include LAYOUT_ROOT.'/head_layer.php' ?>
   <div id='header'>
      <div class="content">
         <div class='logo'><a href="/"><img src="/images/common/top_dsp_logo.png" alt="DSP ZONE"></a></div>
         <div class='menu'>
            <ul>
               <li href='/campaign/<?=$it_id?>' class='<?=$li_1?>'>CAMPAIGN #1</li>
               <li href='/dspzone' class='<?=$li_2?>'>DSP ZONE ?</li>
               <li href='/boards' class='<?=$li_3?>'>NOTICE</li>
            </ul>
         </div>
         <div class="stat">
         <? if(!$_SESSION[user_no]) { ?>
            <span rel='#loginLayer' open-layer='login'>로그인</span> | 
            <span rel='#joinLayer' open-layer='join'>회원가입</span>
         <? } else { ?>
            <span><strong><?=$_SESSION[username]?></strong>님</span> | 
            <span href='/my'>마이페이지</span> | 
            <span href='/logout'>로그아웃</span>
         <? } ?>
         </div>
      </div>
   </div>

