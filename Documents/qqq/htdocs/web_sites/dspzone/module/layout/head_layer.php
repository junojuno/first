<div id='joinLayer'>
   <span class='btn_close'>x</span>
   <div class='content'>
      <div class='top'>
         <h1>회원가입</h1>
         <div class='btn_cont' href='/facebook/login' nw='true'><img src='/images/common/fb_join_btn.png'></div>
         <div class='btn_cont' href='/twitter/login' nw='true'><img src='/images/common/tw_join_btn.png'></div>
      </div>
      <div class='or-with-lines'>
         <div>또는</div>
      </div>
      <div class='bottom'>
         <form name='join' method='post'>
            <div class='input_cont'>
               <label>이름</label><input type='text' name='join_name' class='input1'>
            </div>
            <div class='input_cont'>
               <label>이메일</label><input type='text' name='join_email' class='input1'>
               <div class='duple_check' event-ajax='email_duple_check'><img src='/images/common/question_mark.png'></div>
            </div>
            <div class='input_cont'>
               <label>비밀번호</label><input type='password' name='join_pw' class='input1'>
            </div>
            <div class='input_cont'>
               <label>비밀번호 확인</label><input type='password' name='join_pw_confirm'class='input1'>
            </div>

            <div class='use_provision'>
               <span href='/provision/' class='hl' nw='true'>이용약관</span>에 동의하시면 아래 버튼을 눌러주세요
            </div>

            <div class='btn_cont'>
               <div class='join_btn' form-name='join'>
                  회원가입
               </div>
            </div>
            <input type='hidden' name='dupleChecked' value="0">
         </form>
      </div>
   </div>
</div>
<div id='loginLayer'>
   <span class='btn_close'>x</span>
   <div class='content'>
      <div class='top'>
         <h1>LOGIN</h1>
         <div class='btn_cont' href='/facebook/login' nw='true'><img src='/images/common/fb_login_btn.png'></div>
         <div class='btn_cont' href='/twitter/login' nw='true'><img src='/images/common/tw_login_btn.png'></div>
      </div>
      <div class='or-with-lines'>
         <div>또는</div>
      </div>
      <div class='bottom'>
         <form method='post' name='login'>
            <div class='input_cont'>
               <label>이메일</label><input type='text' name='login_email' class='input1'>
            </div>
            <div class='input_cont'>
               <label>비밀번호</label><input type='password' name='login_pw' class='input1'>
               <div class='find_pw' open-layer='find_pw'><img src='/images/common/question_mark.png'></div>
            </div>
            <div class='btn_cont'>
               <div class='login_btn' event-ajax='login_check'>
                  로그인
               </div>
            </div>
            <input type='hidden' name='moveurl' value='<?=$_GET[url] ? $_GET[url] : $_SERVER[REQUEST_URI]?>'>
         </form>
      </div>
   </div>
</div>
<div id='findLayer'>
   <span class='btn_close'>x</span>
   <div class='content'>
      <div class='top'>
         <h1>비밀번호찾기</h1>
         <div class='tit'>
            가입하신 이메일을 입력하시면 임시 비밀번호를 보내드립니다.
         </div>
      </div>
      <div class='bottom'>
         <form method='post'>
            <div class='input_cont'>
               <label>이메일</label><input type='text' name='mb_email' class='input1'>
            </div>
            <div class='btn_cont'>
               <div class='find_btn'>
                  비밀번호 받기
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
