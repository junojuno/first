<?php
   $seat_info = $_POST[seat_info];
   $name = $_POST[od_name];
   $it_id = $_POST[it_id];
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>DSP ZONE</title>
</head>
<body>
<div style='display:block;'>
   <div style='display:block;padding:40px 20px 20px;background-color:#f5f5f5;font-family:NanumGothic;margin:0 auto; width:900px;'>
      <div style='display:block;'>
         <div style='text-align:center;'>
            <a href='http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>'><img src='http://<?=$_SERVER[HTTP_HOST]?>/images/campaign/kara/detail_title.png' style='width:466px;'></a>
         </div>
         <div style='text-align:center;margin-top:15px;'>
            <img src='http://<?=$_SERVER[HTTP_HOST]?>/images/campaign/kara/detail_title_band.png'>
         </div>
      </div>
      <div style='width:670px;margin:45px auto 0;'>
         <div style='border:1px solid #777;background-color:#fafafa;padding:48px 37px 60px;'>
            <h1 style='text-align:center;margin:0;padding:0;font-weight:bold;font-size:24px;font-family:NanumGothicBold;border-bottom:2px solid #c9c9c9;padding-bottom:25px;letter-spacing:-3px;'>
               감사합니다! <?=$name?> 님 후원이 완료되었습니다
            </h1>
            <table style='width:510px;font:14px NanumGothicBold;border-collapse:collapse;margin:12px auto;'>
               <tr style='border-bottom:1px solid #ccc;'>
                  <td style='width:140px;height:45px;'>이름</td>
                  <td><?=$name?></td>
               </tr>
               <tr style='border-bottom:1px solid #ccc;'>
                  <td style='height:45px;'>행사명</td>
                  <td>2014 카밀리아 데이 (5월 24일 블루스퀘어 삼성카드홀)</td>
               </tr>
               <tr style='border:none;'>
                  <td style='height:45px;'>좌석번호</td>
                  <td>
                     <?=$seat_info?>
                     <div style='margin-left:10px;display:inline-block;*display:inline;*zoom:1;position:relative;top:6px;'>
                  <a href='http://ticketimage.interpark.com/TicketImage/bluesquare/card_musical.gif'><img src='http://<?=$_SERVER[HTTP_HOST]?>/images/campaign/kara/seat_map.png'></a>
                     </div>
                  </td>
               </tr>
            </table>
            <div style='font:13px NanumGothicBold;border-top:1px solid black;padding-top:19px;padding-left:12px;line-height:27px;'>
               * 행사당일 <span style='font-weight:bold;text-decoration:underline;'>신분증</span>과 <span style='font-weight:bold;text-decoration:underline;'>확인문자</span> 또는 <span style='font-weight:bold;text-decoration:underline;'>확인 이메일을 출력</span>하여 지참하셔야 합니다. <br/>
               * 팬미팅 관련 문의- nykim@dspmedia.co.kr / 시스템 관련 문의 - dspzone@wegen.kr
            </div>
         </div>
      </div>
      <div style='margin:30px 0;text-align:right;'>
         <a style='text-decoration:none;color:red;text-decoration:underline;font-size:15px;font-weight:bold;' href='http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>'>팬미팅 자세한 정보 보기 ></a>
      </div>
   </div>
   <div style='padding:30px 0;text-align:center;background-color:white;border:2px solid #D3D3D3;width:940px;margin:0 auto;'>
      <div style='display:inline-block;*display:inline;*zoom:1;line-height:20px;font-size:13px;margin-right:30px;width:277px;'>
         <a href='http://dspmedia.co.kr/'><img src='http://<?=$_SERVER[HTTP_HOST]?>/images/common/bottom_dsp_logo.png' style='margin-bottom:5px'></a>
         <div>(주)DSPmedia 서울시 강남구 논현동 36-12번지</div>
         <div>문의메일: dspmedia@daum.net</div>
      </div>
      <div style='display:inline-block;*display:inline;*zoom:1;line-height:20px;font-size:13px;margin-left:30px;width:277px;' >
         <a href='http://wegen.kr/'><img src='http://<?=$_SERVER[HTTP_HOST]?>/images/common/bottom_wegen_logo.png' style='margin-bottom:5px;'></a><br/>
         <div>DSP Zone은 스타와 함께하는 즐거운 기부,</div>
         <div>위제너레이션(http://wegen.kr)과 함께합니다.</div>
      </div>
   </div>
</div>
</body>
</html>
