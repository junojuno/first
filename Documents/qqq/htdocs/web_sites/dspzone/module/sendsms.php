<?php
   /* SMS API SETTIONG (coolsms.co.kr) */
   define('SMS_API_KEY', 'NCS5337C5B6D6EB5');
   define('SMS_API_SECRET', '18D3DC5EA6ABBE4F4CBF3B56AABDE8AA');

   include $_SERVER[DOCUMENT_ROOT].'/include/lib/coolsms.php';

   $rest = new coolsms(SMS_API_KEY, SMS_API_SECRET);

   function phoneCheck($phone) {
      $regExp = '/^(01[016789]{1})-?[0-9]{3,4}-?[0-9]{4}$/';
      if(preg_match($regExp,$phone)) return true;
      else return false;
   }
   function sendSMS($contact_arr, $content, $send_type ='SMS', $send_phone = null, $datetime=null) {
      global $rest;
      $options = new stdClass();
      $options->to = join(', ',$contact_arr);
      $from_num = $send_phone ? $send_phone : "070-4164-1151";
      $options->from = $from_num;
      $options->text = $content;
      $options->type = $send_type;
      if($datatime) {
         $options->datatime = $datetime;
      }

      $res = $rest->send($options);
      return $res;
   }

   $_sms_contact = array();
   if(isset($_POST[sms_type])) {
      $_sms_type = $_POST[sms_type];
      include $_SERVER[DOCUMENT_ROOT].'/include/config.php';
      include $_SERVER[DOCUMENT_ROOT].'/include/database.php';
      include INCLUDE_ROOT.'/base_function.php';
   }
   sec_session_start(false);

   if($_sms_type == 'donate_account_info') {
      $_sms_type = 'dsp_'.$_sms_type;
      if($_POST[od_id] == '') {
         echo 'failed1';
         exit;
      }
      $info = sql_fetch_array("SELECT * FROM ".DB_ORDERS." WHERE od_id = ?", array($_POST[od_id]), true);
      $escrow = explode("/",$info[od_escrow1]);
      $chk = $escrow[4] == "1" ? true : false;
      if($chk) {
         echo 'failed2';
         exit;
      }
      $bankname = $escrow[0];
      $depositor = $escrow[1];
      $account = $escrow[2];
      $mny = $info[od_amount];
      
      if($info[od_hp] != '') {
         if (phoneCheck($info[od_hp])) {
            $_sms_content = "[DSPZONE] $info[od_name]님 ".number_format($mny)."원\n$bankname $account\n(예금주 : $depositor)";
            $_sms_phone = explode("-", $info[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type=?, mb_no = ?, contact = ?, group_id= ?", 
                  array($_sms_type, $_SESSION[user_no], $info[od_hp], $result->group_id));
               sql_query("UPDATE ".DB_ORDERS." SET od_escrow1 = CONCAT(od_escrow1,'/1') WHERE od_id = ?", array($_POST[od_id]));
            }
         }
      }

      echo 'success';
   }

   if($_sms_type == 'donate') {
      $_sms_type = 'dsp_'.$_sms_type;
      if(isset($_POST[sms_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }

      if(isset($_POST[sms_type])) {
         $od = sql_fetch_array("SELECT *, a.sent_sms AS o_sent_sms FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
            LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
            WHERE a.od_id = ?", array($_POST[od_id]), true);
      } // 가상계좌일때는 이미 $od 에 fetch했음. (가상계좌는 POST가아님)
      
      if($od[od_method] == '가상계좌') $od[var_1] = $seat_info;
      if($od[o_sent_sms] == 0) {
         if (phoneCheck($od[mb_contact])) {
            $_sms_content = "[DSPZONE] $od[od_name] 님\n2014 카밀리아 데이!\n5월 24일 07:00PM\n블루스퀘어 삼성카드홀\n좌석번호: $od[var_1]\n\n*당일 신분증을 꼭 지참해주십시오\n*수익금 전액이 기부되므로 환불은 불가합니다\n* 관련문의: wegen@wegen.kr";
            $_sms_phone = explode("-", $od[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content, 'LMS');
            if($result->result_code == "00") {
               sql_query("UPDATE ".DB_ORDERS." SET sent_sms = 1 WHERE od_id = ?", array($od[od_id]));
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type= ? , mb_no = ? , contact = ?, group_id= ? , var_1 = ?", 
                        array($_sms_type, $od[mb_no], $od[od_hp], $result->group_id, $od[od_id]));
            }
         }
      }
      echo 'success';
   }

   if($_sms_type == 'kara_account_late') {
      $_sms_type = 'dsp_'.$_sms_type;
      if(isset($_POST[sms_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }
      
      if($od[od_method] == '가상계좌') $od[var_1] = $seat_info;
      if($od[sent_sms] == 0) {
         if (phoneCheck($od[mb_contact])) {
            $_sms_content = $od[it_sms] != '' ? $od[it_sms] : "[DSPZONE] $od[od_name] 님\n2014 카밀리아데이 티켓이 이미 매진되었습니다. 환불받으실 계좌를 답변으로 보내주시면 환불 처리해드리겠습니다. 참여해주셔서 감사합니다.";
            $_sms_phone = explode("-", $od[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content, 'LMS', '010-4542-1419');
            if($result->result_code == "00") {
               sql_query("UPDATE ".DB_ORDERS." SET sent_sms = 1 WHERE od_id = ?", array($od[od_id]));
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type= ?, mb_no = ?, contact = ?, group_id= ? , var_1 = ?", 
                        array($_sms_type, $od[mb_no], $od[od_hp], $result->group_id, $od[od_id]));
            }
         }
      }
      echo 'success';
      
   }

