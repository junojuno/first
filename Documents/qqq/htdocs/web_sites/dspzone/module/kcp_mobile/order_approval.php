<?php
   header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
   header("Cache-Control: no-store");
   header("Pragma: no-cache");
   
   /* config */
   $_debug = ($_SERVER[HTTP_HOST] == '14.47.134.182') ? true : false;
   $g_conf_site_name	= 'DSPZONE_WEGEN';
   $g_conf_log_level	= '3';
   $g_conf_gw_port	= '8090';
   $g_conf_mode		= 0;		// 배치결제
   $g_conf_home_dir	= ($_debug) ? '/var/www/kcp/dspzone' : '/home/hosting_users/wegenkr/kcp/dspzone';
   $g_conf_gw_url		= ($_debug) ? 'testpaygw.kcp.co.kr' : 'paygw.kcp.co.kr'; // testpaygw.kcp | paygw.kcp
   $g_conf_js_url		= ($_debug) ? 'http://pay.kcp.co.kr/plugin/payplus_test_un.js' : 'http://pay.kcp.co.kr/plugin/payplus_un.js'; // payplus_test_un.js | payplus_un.js
   $g_wsdl           = ($_debug) ? "KCPPaymentService.wsdl" : "real_KCPPaymentService.wsdl";
   $module_type		= '01';

   require "KCPComLibrary.php";              // library [수정불가]
?>
<?php
    // 쇼핑몰 페이지에 맞는 문자셋을 지정해 주세요.
    $charSetType      = "utf-8";             // UTF-8인 경우 "utf-8"로 설정
    
    $siteCode         = $_GET[ "site_cd"     ];
    $orderID          = $_GET[ "ordr_idxx"   ];
    $paymentMethod    = $_GET[ "pay_method"  ];
    $escrow           = ( $_GET[ "escw_used"   ] == "Y" ) ? true : false;
    $productName      = $_GET[ "good_name"   ];

    // 아래 두값은 POST된 값을 사용하지 않고 서버에 SESSION에 저장된 값을 사용하여야 함.
    $paymentAmount    = $_GET[ "good_mny"    ]; // 결제 금액
    $returnUrl        = $_GET[ "Ret_URL"     ];

    // Access Credential 설정
    $accessLicense    = "";
    $signature        = "";
    $timestamp        = "";

    // Base Request Type 설정
    $detailLevel      = "0";
    $requestApp       = "WEB";
    $requestID        = $orderID;
    $userAgent        = $_SERVER['HTTP_USER_AGENT'];
    $version          = "0.1";

    try
    {
        $payService = new PayService( $g_wsdl );

        $payService->setCharSet( $charSetType );
        
        $payService->setAccessCredentialType( $accessLicense, $signature, $timestamp );
        $payService->setBaseRequestType( $detailLevel, $requestApp, $requestID, $userAgent, $version );
        $payService->setApproveReq( $escrow, $orderID, $paymentAmount, $paymentMethod, $productName, $returnUrl, $siteCode );

        $approveRes = $payService->approve();
                
        printf( "%s,%s,%s,%s", $payService->resCD,  $approveRes->approvalKey,
                               $approveRes->payUrl, $payService->resMsg );

    }
    catch (SoapFault $ex )
    {
        printf( "%s,%s,%s,%s", "95XX", "", "", iconv("EUC-KR","UTF-8","연동 오류 (PHP SOAP 모듈 설치 필요)" ) );
    }
?>
