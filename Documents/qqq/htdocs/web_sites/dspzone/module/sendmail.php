<?php
   define(SMTP_EMAIL_ID, 'dspzone@wegen.kr');
   define(SMTP_EMAIL_PW, '1generation');
   define(SMTP_EMAIL_NAME, 'DSP ZONE');

   include $_SERVER[DOCUMENT_ROOT].'/include/lib/mailer/PHPMailerAutoload.php';

   
   $_mail = new PHPMailer;
   $_mail->isSMTP();
   $_mail->ContentType = "text/html";
   $_mail->CharSet = "utf-8";
   $_mail->Host = 'smtp.gmail.com';  // Specify main and backup server
   // 0 = off (for production use)
   // 1 = client messages
   // 2 = client and server messages
   //$_mail->SMTPDebug = 2;
   //$_mail->Debugoutput = 'html';
   $_mail->Port = 587;
   $_mail->SMTPAuth = true;                               // Enable SMTP authentication
   $_mail->Username = SMTP_EMAIL_ID;                            // SMTP username
   $_mail->Password = SMTP_EMAIL_PW;                           // SMTP password
   $_mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

   $_mail->From = SMTP_EMAIL_ID;
   $_mail->FromName = SMTP_EMAIL_NAME;

   $_mail->isHTML(true);                                  // Set email format to HTML

   function send_email($mail_layout, $email, $post_arr) {
      global $_mail;

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return false;

      $_mail->addAddress($email);
      $postdata = http_build_query($post_arr);
      $opts = array('http' => 
         array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
         )
      );
      $ctx = stream_context_create($opts);
      switch($mail_layout) {
         case 'donate':
            $_mail->Subject = "[DSPZONE] $post_arr[od_name]님 후원해주셔서 감사합니다!";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/layout/mail/donate.php', false, $ctx);
            break;

      }

      $_mail->msgHTML($mail_html);

      if(!$_mail->send()) {
         return false;
      } else {
         $_mail->clearAddresses();
         return true;
      }
   }
   
   if(isset($_POST[mail_type])) {
      $_mail_type = $_POST[mail_type];
      include $_SERVER[DOCUMENT_ROOT].'/include/config.php';
      include $_SERVER[DOCUMENT_ROOT].'/include/database.php';
      include INCLUDE_ROOT.'/base_function.php';
   }
   sec_session_start(false);

   if($_mail_type =='donate') {
      $_mail_type = 'dsp_'.$_mail_type;
      if(isset($_POST[mail_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }
      
      if(isset($_POST[mail_type])) {
         $od = sql_fetch_array("SELECT *, a.sent_mail AS o_sent_mail FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
            LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
            WHERE a.od_id = ?", array($_POST[od_id]), true);
      } // 가상계좌일때는 이미 $od 에 fetch했음. (가상계좌는 POST가아님)
      
      $use_pay_method = $od[od_method];
      if($use_pay_method == '가상계좌') $od[var_1] = $seat_info;
      $postarr = array(
         'od_name' => $od[od_name],
         'seat_info' => $od[var_1],
         'it_id' => $od[it_id]
         );

      if($od[o_sent_mail] == 0 ) {
         if(send_email('donate',$od[mb_email], $postarr)) {
            sql_query("UPDATE ".DB_ORDERS." SET sent_mail = 1 WHERE od_id = ?", array($od[od_id]));
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type=?, mb_no= ?, email= ?, var_1 = ?", array($_mail_type, $od[mb_no], $od[mb_email], $od[it_id]));
            echo "success";
         }
      }
   }
?>
