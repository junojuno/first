<?php
   include '../../include/config.php';
   include '../../include/base_function.php';
   
   include '../../include/database.php';
   $kcp_server = ($_debug) ? array('210.122.73.58') : array('203.238.36.173','203.238.36.178');
   // kcp 서버 ip만 받기
   if(array_search($_SERVER['REMOTE_ADDR'], $kcp_server) === false) {
      exit;
   }

   $site_cd      = $_POST [ "site_cd"  ];                 // 사이트 코드
   $tno          = $_POST [ "tno"      ];                 // KCP 거래번호
   $order_no     = $_POST [ "order_no" ];                 // 주문번호
   $tx_cd        = $_POST [ "tx_cd"    ];                 // 업무처리 구분 코드
   $tx_tm        = $_POST [ "tx_tm"    ];                 // 업무처리 완료 시간

   $ipgm_name    = "";                                    // 주문자명
   $remitter     = "";                                    // 입금자명
   $ipgm_mnyx    = "";                                    // 입금 금액
   $bank_code    = "";                                    // 은행코드
   $account      = "";                                    // 가상계좌 입금계좌번호
   $op_cd        = "";                                    // 처리구분 코드
   $noti_id      = "";                                    // 통보 아이디
   $cash_a_no    = "";                                    // 현금영수증 승인번호

   if ( $tx_cd == "TX00" )
   {
      $ipgm_name = $_POST[ "ipgm_name" ];                // 주문자명
      $remitter  = $_POST[ "remitter"  ];                // 입금자명
      $ipgm_mnyx = $_POST[ "ipgm_mnyx" ];                // 입금 금액
      $bank_code = $_POST[ "bank_code" ];                // 은행코드
      $account   = $_POST[ "account"   ];                // 가상계좌 입금계좌번호
      $op_cd     = $_POST[ "op_cd"     ];                // 처리구분 코드
      $noti_id   = $_POST[ "noti_id"   ];                // 통보 아이디
      $cash_a_no = $_POST[ "cash_a_no" ];                // 현금영수증 승인번호
   }
   /* = -------------------------------------------------------------------------- = */
   /* =   03-1. 가상계좌 입금 통보 데이터 DB 처리 작업 부분                        = */
   /* = -------------------------------------------------------------------------- = */
   if ( $tx_cd == "TX00" )
   {
      $ipgm_mnyx = $op_cd == "13" ? "-".$ipgm_mnyx : $ipgm_mnyx; // 13이외는 다 입금처리

      // 중복 노티 방지
      $od = sql_fetch_array("SELECT * FROM ".DB_ORDERS." a
         LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
         LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
         WHERE a.od_id = ? and a.od_tno = ?", array($order_no, $tno), true);
      $escrow = explode("/",$od[od_escrow2]);
      if($escrow[0] != $noti_id) {
         $info = sql_fetch_array("SELECT COUNT(od_id)+1 AS rank FROM ".DB_ORDERS." WHERE it_id = ? AND pay_remain = 0 AND od_time <= ?", array($od[it_id], $tx_tm), true);
         $target_rank = $info[rank];
         include INCLUDE_ROOT.'/set_seat_var.php';

         $sql = "UPDATE ".DB_ORDERS." SET 
                  od_time = ?,
                  pay_remain = pay_remain - ?,
                  od_escrow2 = concat(od_escrow2, ?),
                  var_1 = ?
                  WHERE od_id = ? AND od_tno = ?";
         sql_query($sql, array($tx_tm, (int)$ipgm_mnyx, "$noti_id/$bank_code/$account/$remitter/$op_cd/$cash_a_no|", $seat_info, $order_no, $tno));

         if($info[rank] > ($od[it_target]/5000)-32) {
            $_sms_type='kara_account_late';
            include $_SERVER[DOCUMENT_ROOT].'/module/sendsms.php';

         } else {
            $_mail_type='donate';
            include $_SERVER[DOCUMENT_ROOT].'/module/sendmail.php';

            $_sms_type='donate';
            include $_SERVER[DOCUMENT_ROOT].'/module/sendsms.php';
         }


      }
   }
   /* ============================================================================== */
?>
