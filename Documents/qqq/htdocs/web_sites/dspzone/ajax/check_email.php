<?php
   $_AJAX = true;
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';

   if($_POST[mb_email] == '') {
      json_fail();
      exit;
   }

   $chk = sql_fetch_array("SELECT mb_no FROM ".DB_MEMBERS." WHERE mb_id = ?", array($_POST[mb_email]));

   if($chk[0][mb_no]) $json_ret[response] = 'no';
   else $json_ret[response] = 'yes';

   echo json_encode($json_ret);
   exit;
?>
