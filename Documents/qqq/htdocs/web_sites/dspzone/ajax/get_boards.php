<?php
   $_AJAX = true;
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
   
   if($_POST[type] == '' || $_POST[id] == '') {
      json_fail();
   }
   $type = $_POST[type] == 'notice' ? 1 : 2;
   $chk = sql_fetch_array("SELECT * FROM ".DB_NOTICE." WHERE id = ? AND type = ? AND site_code = ?", array($_POST[id], $type, SITE_CODE), true);
   if($chk[id]) {
      $json_ret[res_status] = 'true';
      $json_ret[res_title] = $chk[title];
      $json_ret[res_time] = $chk[upload_time]; 
      $json_ret[res_time] = $chk[is_modified] == 1 ? $json_ret[res_time].' 수정됨' : $json_ret[res_time];
      $json_ret[res_content] = $chk[content];
      echo json_encode($json_ret);
      exit;
   } else {
      json_fail();
   }

   

?>
