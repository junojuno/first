<?php
   $_AJAX = true;
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
   sec_session_start();
   
   if(!$_SESSION[user_no]) {
      json_fail();
   }

   if($_POST[name] == '' || $_POST[email] == '' || $_POST[contact] == '' || $_POST[zips] == '' || $_POST[address] == '') {
      json_fail();
   }
   $mb_name = $_POST[name];
   $contact = $_POST[contact];
   $zip = $_POST[zips];
   $address = $_POST[address];
   
   $res = sql_query("UPDATE ".DB_MEMBERS." SET 
         mb_name = ?, mb_contact = ?, mb_email = ?, mb_zip1 = ?, mb_zip2 = ?, mb_addr1 = ?, mb_addr2 = ? WHERE mb_no = ? ", 
         array($mb_name, $contact, $_POST[email], $zip[zip1], $zip[zip2], $address[addr1], $address[addr2], $_SESSION[user_no]));
   
   if($res) {
      $json_ret[res_status] = 'true';
      echo json_encode($json_ret);
   } else {
      json_fail();
   }
?>
