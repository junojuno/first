<?php
   $_AJAX = true;
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
   
   if($_POST[login_email] == '' || $_POST[p] == '') {
      json_fail();
      exit;
   }
   
   
   $chk = sql_fetch_array("SELECT * FROM ".DB_MEMBERS." WHERE mb_id = ?",array($_POST[login_email]));
   if($chk[0][mb_no]) {
      $password = hash('sha512', $_POST[p].$chk[0][salt]);
      if($password == $chk[0][mb_password]) {
         $json_ret[response] = 'yes';
         echo json_encode($json_ret);
      } else {
         json_fail();
         exit;
      }
   } else {
      json_fail();
      exit;
   }

   exit;
?>
