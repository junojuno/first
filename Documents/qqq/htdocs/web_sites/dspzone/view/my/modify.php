<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
if($_status) {
   include LAYOUT_ROOT.'/head.php';
?>

<div id='content'>
   <div class='content'>
      <div class='profile_summary'>
         <div class='profile_img'>
            <img src='<?=$profile_img?>'>
         </div>
         <div class='profile_txt'>
            <div class='top'>
               <div class='name'><?=$my[mb_name]?></div>
            </div>
            <div class='join_day'><?=$join_day?></div>
         </div>
      </div>
      <form name='my_modify' enctype='multipart/form-data' method="POST">
      <div class='mb_info'>
         <div class='ctt'>
            <table>
               <tr>
                  <th class='left_col'>이름</th>
                  <td><input type='text' name='mb_name' value='<?=$my[mb_name]?>'></td>
               </tr>
               <tr>
                  <th>이메일</th>
                  <td><input type='text' name='mb_email' value='<?=$my[mb_email]?>'></td>
               </tr>
               <? if(!$isSNS) { ?>
               <tr>
                  <th>비밀번호</th>
                  <td><input type='password' name='mb_pw'></td>
               </tr>
               <tr>
                  <th>비밀번호 확인</th>
                  <td><input type='password' name='mb_pw_confirm'></td>
               </tr>
               <? } ?>
               <tr>
                  <th>전화번호</th>
                  <td class='contact'>
                     <input type='text' name='od_hp1' class='numonly' value='<?=$my_ctt[0]?>'> - 
                     <input type='text' name='od_hp2' class='numonly' value='<?=$my_ctt[1]?>'> - 
                     <input type='text' name='od_hp3' class='numonly' value='<?=$my_ctt[2]?>'> 
                  </td>
                  <input type='hidden' name='mb_contact' value='<?=$my[mb_contact]?>'>
               </tr>
               <tr>
                  <th>프로필 사진</th>
                  <td>
                     <input type='file' name='mb_profile'>
                     <div class='file_size'>사이즈는 100x100 으로 통일되며 jpg, gif, png파일을 지원합니다.</div>
                  </td>
               </tr>
               <tr>
                  <th>주소</th>
                  <td>
                     <div class='zipcode-finder'>
                        <input type='text' id="dongName" />
                        <div class='btn_cont'>
                           <input type='button' class='zipcode-search' value='검색'/>
                        </div>
                        <div class="zipcode-search-result" data-name='mb'></div>
                     </div>
                     <div id='addr'>
                        <input type='text' name="mb_zip1" value="<?=$my[mb_zip1]?>" size='3' maxlength='3' readonly /> -
                        <input type='text' name="mb_zip2" value="<?=$my[mb_zip2]?>" size='3' maxlength='3' readonly />
                        <input type='text' name="mb_addr1" value="<?=$my[mb_addr1]?>" readonly /><br/>
                        <input type='text' name="mb_addr2" value="<?=$my[mb_addr2]?>" />
                     </div>
                  </td>
               </tr>
            </table>
            <div class='cont'>
               <div class='btn_cont'>
                  <div class='modify_btn' form-name='my_modify'>
                     수정하기
                  </div>
               </div>
            </div>
         </div>
      </div>
      </form>
   </div>
</div>

<?php
   include LAYOUT_ROOT.'/tail.php';
}
?>
