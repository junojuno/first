<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';

if($_status) {
   include LAYOUT_ROOT.'/head.php';
?>
<div id='content'>
   <div class='content'>
      <div class='profile_summary'>
         <div class='profile_img'>
            <img src='<?=$profile_img?>'>
         </div>
         <div class='profile_txt'>
            <div class='top'>
               <div class='name'><?=$my[mb_name]?></div>
               <div class='btn_cont'>
                  <div class='modify_btn' href='/my/modify'>정보수정</div>
               </div>
            </div>
            <div class='join_day'><?=$join_day?></div>
         </div>
      </div>

      <div class='donate_list'>
         <table>
            <tr>
               <td>후원 코드</td>
               <td>후원 일시</td>
               <td>참여 캠페인</td>
               <td>후원 금액</td>
               <td>좌석</td>
               <td>확인증 출력</td>
            </tr>
         <? if($donate_num == 0) { ?>
            <tr>
               <td colspan="6">참여한 캠페인이 없습니다</td>
         <? } else { ?>
            <? for($i = 0; $i < $donate_num;$i++) { ?>
            <tr class='<?=$i == $donate_num-1 ? "last" : false?>'>
               <td><?=$donate[$i][od_id]?></td>
               <td><?=$donate[$i][od_time]?></td>
               <td href='/campaign/<?=$donate[$i][it_id]?>'><?=$donate[$i][it_name]?></td>
               <td>
                  <?=number_format($donate[$i][od_amount]-$donate[$i][pay_remain]).'원'?>
               <? if($donate[$i][account_info] != '0' && $donate[$i][pay_remain] != 0) { ?>
                  <div class='not_pay' past='<?=$donate[$i][is_past]?>' info='<?=$donate[$i][info_json]?>'>
                     (미입금)
                  </div>
               <? } ?>
               </td>
               <td><?=$donate[$i][var_1]?></td>
               <td>
                  <span class='print <?=$donate[$i][pay_remain] == 0? false : 'not'?>'>Print</span>
               </td>
            </tr>
            <? } ?>
         <? } ?>
         </table>
         <div class='donate_summary'>
            <div class='cont'>
               <div class='info'>
                  <div class='campaign_num'>참여 캠페인 수: <hl><?=$donate_num?></hl>개</div>
                  <div class='donate_amount'>총 후원 금액: <hl><?=number_format($donate_amount[amount_sum])?></hl> 원</div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php
   include LAYOUT_ROOT.'/tail.php';
}
?>
