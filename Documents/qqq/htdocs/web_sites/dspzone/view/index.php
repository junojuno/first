<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
   include LAYOUT_ROOT.'/head.php';
?>
<div id='content'>
   <div class='top_banner' href='/campaign/<?=$it_id?>'>
   </div>

   <div class='content'>
      <div class='campaign_part'>
         <div class='title'>
            <img src='/images/main/main_title_1.png'>
         </div>
         <div class='ctt'>
            <div class='video'>
               <? if($cam[it_youtube]) {?>
               <iframe width="425" height="290" src="http://www.youtube.com/embed/<?=$cam[it_youtube]?>?wmode=opaque">
               </iframe>
               <? } else echo $inner; ?>
            </div>
            <div class='info'>
               <img src='/images/main/main_meeting_info.png'>
               <div class='btn_group'>
                  <div class='btn_cont'>
                  <img src='/images/main/main_detail_btn.png' href='/campaign/<?=$it_id?>'>
                  </div>
                  <div class='btn_cont end'>
                  <img src='/images/main/main_join_btn.png' href='/campaign/<?=$it_id?>/donate'>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class='main_bottom'>
         
         <div class='dsp_zone'>
            <div class='title'><img src='/images/main/title_dspzone.png'></div>
            <div class='ctt' href='/dspzone/'>
               <div class='more'>더 보기></div>
               <img src='/images/main/main_dsp_intro.png'>
            </div>
         </div>

         <div class='notices'>
            <div class='notice'>
               <div class='title'><div class='more' href='/boards'>더 보기></div><img src='/images/common/title_notice.png'></div>
               <div class='slides'>
                  <div class='items'>
                  <? if($notice_num == 0) { ?>
                     <div>
                        <table class='page'>
                           <tr><td>등록된 게시글이 없습니다</td></tr>
                        </table>
                     </div>
                  <? } else { ?>
                     <? for($i =1; $i <= $notice_page_num;$i++) { ?>
                     <div>
                     <table class='page'>
                        <? for($j =$notice_count, $tmp_count =1; $j < $notice_num; $j++, $notice_count++,$tmp_count++) { 
                           if($tmp_count % ($row_num+1) == 0) break;
                        ?>
                        <tr class='row' href='/boards?notice=<?=$notices[$j][id]?>'data-page='<?=$i?>'>
                           <td class='title'>
                              <?=mb_strlen($notices[$j][title],'utf-8') > 19 ? mb_substr($notices[$j][title],0,19,'utf-8').'...' : $notices[$j][title]?>
                           </td>
                           <td class='datetime'><?=date("Y-m-d",strtotime($notices[$j][upload_time]))?></td>
                        </tr>
                        <? } ?>
                     </table>
                     </div>
                     <? } ?>
                  <? } ?>
                  </div>
               </div>
               <div class='paging'>
                  <span class='arrow left'><<</span> 
                  <span class='arrow prev'><</span> 
                  <? for($i =1; $i <= $notice_page_num; $i++) { ?> 
                     <span data-page='<?=$i?>'><?=$i?></span>
                  <? } ?>
                  <span class='arrow next'>></span> 
                  <span class='arrow right'>>></span>
               </div>
            </div>

            <div class='nanum'>
               <div class='title'><div class='more' href='/boards'>더 보기></div><img src='/images/main/title_nanum.png'></div>
               <div class='slides'>
                  <div class='items'>
                  <? if($nanum_num == 0) { ?>
                     <div>
                        <table class='page'>
                           <tr><td>등록된 게시글이 없습니다</td></tr>
                        </table>
                     </div>
                  <? } else { ?>
                     <? for($i =1; $i <= $nanum_page_num;$i++) { ?>
                     <div>
                     <table class='page'>
                        <? for($j =$nanum_count, $tmp_count =1; $j < $nanum_num; $j++, $nanum_count++,$tmp_count++) { 
                           if($tmp_count % ($row_num+1) == 0) break;
                        ?>
                        <tr class='row' href='/boards?nanum=<?=$nanums[$j][id]?>' data-page='<?=$i?>'>
                           <td class='title'>
                              <?=mb_strlen($nanums[$j][title],'utf-8') > 19 ? mb_substr($nanums[$j][title],0,19,'utf-8').'...' : $nanums[$j][title]?>
                           </td>
                           <td class='datetime'><?=date("Y-m-d",strtotime($nanums[$j][upload_time]))?></td>
                        </tr>
                        <? } ?>
                     </table>
                     </div>
                     <? } ?>
                  <? } ?>
                  </div>
               </div>
               <div class='paging'>
                  <span class='arrow left'><<</span> 
                  <span class='arrow prev'><</span> 
                  <? for($i =1; $i <= $nanum_page_num; $i++) { ?> 
                     <span data-page='<?=$i?>'><?=$i?></span>
                  <? } ?>
                  <span class='arrow next'>></span> 
                  <span class='arrow right'>>></span>
               </div>
            </div>
         </div>

      </div>

   </div>

</div>


<?php include LAYOUT_ROOT.'/tail.php'; ?>
