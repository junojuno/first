<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>Twitter Login - DSP ZONE</title>
</head>
<body>
   <script>
   <? if($closed) { ?>
      function getParameterByName(name) {
          name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
          var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
              results = regex.exec(opener.location.search);
          return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      }
      if(getParameterByName('url')) {
         opener.location.href= getParameterByName('url');
      } else {
         opener.location.reload();
      }
      self.close();
   <? } ?>
   </script>
   
</body>
</html>
