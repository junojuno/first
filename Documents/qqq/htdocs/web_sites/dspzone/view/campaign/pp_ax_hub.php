<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
?>
<!DOCTYPE html>
<html>

<head>
<title>결제중.. - DSP ZONE</title>
   <meta charset="UTF-8">
   <link rel='stylesheet' href='/css<?=str_replace('.php','.css',SCRIPT_NAME)?>' /> 
   <link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
</head>

<body onload="document.pay_info.submit()">
<? if($_status) { ?> 
<script type='text/javascript'>
function noRefresh() {
	if ((event.keyCode == 78) && (event.ctrlKey == true)) {
		event.keyCode = 0;
		return false;
	}
	if(event.keyCode == 116) {
		event.keyCode = 0;
		return false;
	}
}
document.onkeydown = noRefresh;
</script>

<div class='layer'>
	<div id='pluginLayer' class='centering' style='display: block; margin-left: -200px; margin-top: -150px'>
	<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
	<p>결제 진행 중입니다.</p>
	창을 닫거나 새로고침하지 마십시오.
	</div>
</div>

<form name='pay_info' method='post' action='/campaign/<?=$it_id?>/result'>
<input type='hidden' name="site_cd"           value="<?=$g_conf_site_cd ?>">
<input type='hidden' name="req_tx"            value="<?=$req_tx         ?>">
<input type='hidden' name="use_pay_method"    value="<?=$use_pay_method ?>">
<input type='hidden' name="bSucc"             value="<?=$bSucc          ?>">

<input type='hidden' name="res_cd"            value="<?=$res_cd         ?>">
<input type='hidden' name="res_msg"           value="<?=$res_msg        ?>">
<input type='hidden' name="res_en_msg"        value="<?=$res_en_msg     ?>">
<input type='hidden' name="ordr_idxx"         value="<?=$ordr_idxx      ?>">
<input type='hidden' name="tno"               value="<?=$tno            ?>">
<input type='hidden' name="good_mny"          value="<?=$good_mny       ?>">
<input type='hidden' name="good_name"         value="<?=$good_name      ?>">
<input type='hidden' name="it_id"		      value="<?=$it_id          ?>">
<input type='hidden' name="buyr_name"         value="<?=$buyr_name      ?>">
<input type='hidden' name="buyr_tel1"         value="<?=$buyr_tel1      ?>">
<input type='hidden' name="buyr_tel2"         value="<?=$buyr_tel2      ?>">
<input type='hidden' name="buyr_mail"         value="<?=$buyr_mail      ?>">

<input type='hidden' name="card_cd"           value="<?=$card_cd        ?>">
<input type='hidden' name="card_name"         value="<?=$card_name      ?>">
<input type='hidden' name="app_time"          value="<?=$app_time       ?>">
<input type='hidden' name="app_no"            value="<?=$app_no         ?>">
<input type='hidden' name="quota"             value="<?=$quota          ?>">
<input type='hidden' name="noinf"             value="<?=$noinf          ?>">
<input type='hidden' name="partcanc_yn"       value="<?=$partcanc_yn    ?>">
<input type='hidden' name="card_bin_type_01"  value="<?=$card_bin_type_01 ?>">
<input type='hidden' name="card_bin_type_02"  value="<?=$card_bin_type_02 ?>">

<input type='hidden' name="bank_name"         value="<?=$bank_name      ?>">
<input type='hidden' name="bank_code"         value="<?=$bank_code      ?>">

<input type='hidden' name="pnt_issue"         value="<?=$pnt_issue      ?>">
<input type='hidden' name="pnt_app_time"      value="<?=$pnt_app_time   ?>">
<input type='hidden' name="pnt_app_no"        value="<?=$pnt_app_no     ?>">
<input type='hidden' name="pnt_amount"        value="<?=$pnt_amount     ?>">
<input type='hidden' name="add_pnt"           value="<?=$add_pnt        ?>">
<input type='hidden' name="use_pnt"           value="<?=$use_pnt        ?>">
<input type='hidden' name="rsv_pnt"           value="<?=$rsv_pnt        ?>">

<input type='hidden' name='bankname'          value="<?=$bankname?>">
<input type='hidden' name='depositor'         value="<?=$depositor?>">
<input type='hidden' name='account'           value="<?=$account?>">
<input type='hidden' name='va_date'           value="<?=$va_date?>">

<input type='hidden' name="commid"            value="<?=$commid         ?>">
<input type='hidden' name="mobile_no"         value="<?=$mobile_no      ?>">

<input type='hidden' name="cash_yn"           value="<?=$cash_yn        ?>">
<input type='hidden' name="cash_authno"       value="<?=$cash_authno    ?>">
<input type='hidden' name="cash_tr_code"      value="<?=$cash_tr_code   ?>">
<input type='hidden' name="cash_id_info"      value="<?=$cash_id_info   ?>">

<input type='hidden' name='seat_info'         value="<?=$seat_info?>">
</form>
<? } ?>
</body>
</html>
