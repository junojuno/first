<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
   include LAYOUT_ROOT.'/head.php';
if($_status) {
?>

<div id='content'>
   <div class='content'>
      <div class='donate_top'>
         <div class='tit'>
            <img src='/images/campaign/kara/detail_title.png'>
         </div>
         <div class='tit_band'>
            <img src='/images/campaign/kara/detail_title_band.png'>
         </div>
         <div class='donated_info'>
            <div class='info'>
               <h1>감사합니다! <?=$buyr_name?> 님 후원이 완료되었습니다</h1>
               <table class='detail'>
                  <tr>
                     <td style='width:140px'>이름</td>
                     <td><?=$buyr_name?></td>
                  </tr>
               <? if ( $use_pay_method == "100000000000" ) { ?>
                  <tr>
                     <td>결제수단</td>
                     <td>신용카드 <a href="javascript:receiptView('<?=$tno?>');">[영수증 출력]</a></td>
                  </tr>
                  <tr>
                     <td>승인시간</td>
                     <td><?=date("Y-m-d H:m:s",strtotime($app_time))?></td>
                  </tr>
                  <tr class='last'>
                     <td>좌석번호</td>
                     <td>
                        <strong><?=$seat_info?></strong>
                        <div class='btn_cont' href='http://ticketimage.interpark.com/TicketImage/bluesquare/card_musical.gif' nw='true'>
                           <img src='/images/campaign/kara/seat_map.png'>
                        </div>
                     </td>
                  </tr>
			      <? } 	else if ($use_pay_method == "010000000000") { ?>
                  <tr>
                     <td>결제수단</td>
                     <td>계좌이체</td>
                  </tr>
                  <tr>
                     <td>승인시간</td>
                     <td><?=date("Y-m-d H:m:s",strtotime($app_time))?></td>
                  </tr>
                  <tr class='last'>
                     <td>좌석번호</td>
                     <td>
                        <strong><?=$seat_info?></strong>
                        <div class='btn_cont' href='http://ticketimage.interpark.com/TicketImage/bluesquare/card_musical.gif' nw='true'>
                           <img src='/images/campaign/kara/seat_map.png'>
                        </div>
                     </td>
                  </tr>
               <? } else if ($use_pay_method == "001000000000") { ?>
                  <tr>
                     <td>결제수단</td>
                     <td>가상계좌</td>
                  </tr>
                  <tr>
                     <td>은행/예금주</td>
                     <td><?="$bankname/$depositor"?></td>
                  </tr>
                  <tr>
                     <td>계좌번호</td>
                     <td><?=$account?></td>
                  </tr>
                  <tr>
                     <td>입금마감시간</td>
                     <td><?=date("Y-m-d H:m:s",strtotime($va_date))?></td>
                  </tr>
                  <tr class='last'>
                     <td>좌석번호</td>
                     <td>
                        입금후에 문자와 메일로 좌석이 배정됩니다.
                     </td>
                  </tr>
               <? } else if ( $use_pay_method == "000010000000" ) { ?>
                  <tr>
                     <td>결제수단</td>
                     <td>핸드폰 (<?=$commid?> / <?=$mobile_no?>)</td>
                  </tr>
                  <tr>
                     <td>승인시간</td>
                     <td><?=date("Y-m-d H:m:s",strtotime($app_time))?></td>
                  </tr>
                  <tr class='last'>
                     <td>좌석번호</td>
                     <td>
                        <strong><?=$seat_info?></strong>
                        <div class='btn_cont' href='http://ticketimage.interpark.com/TicketImage/bluesquare/card_musical.gif' nw='true'>
                           <img src='/images/campaign/kara/seat_map.png'>
                        </div>
                     </td>
                  </tr>
               <? } ?>
               </table>
            </div>
         </div>

         <div class='bottom'>
            <div class='caution'>
               * 좌석번호는 문자와 이메일로 자동 발송되며 행사일 <hl>신분증</hl>과 <hl>확인페이지 또는 확인 이메일을 출력하여 반드시</hl> 지참하셔야 합니다.<br/>
               * 티켓 양도, 거래는 적발 시 팬미팅 입장이 불가합니다.
            </div>

            <div class='btn_cont'>
               <img src='/images/campaign/kara/print_btn.png' class='print'>
            </div>
         </div>

      </div>
   </div>
</div>

<?php
}
   include LAYOUT_ROOT.'/tail.php';
?>
