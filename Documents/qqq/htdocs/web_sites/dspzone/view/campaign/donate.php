<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
if($_status) {
   include LAYOUT_ROOT.'/head.php';
?>

<div id="layer_pay" style="position:absolute;width:100%;height:100%;z-index:1;display:none;">
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
        <tr width="100%" height="100%">
            <td style="height:inherit;">
                <iframe name="frm_pay"  frameborder="0" marginheight="0" marginwidth="0" border="0" width="100%" height="100%" scrolling="auto"></iframe>
            </td>
        </tr>
    </table>
</div>
<div id='pluginLayer'>
<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
<p>결제 플러그인 설치 여부를 확인하는 중입니다.</p>
브라우저 상단에 노란 알림표시줄이 나타날 경우<br/>
<span style='font-weight: bold; color: white'>"ActiveX 컨트롤 설치"</span>를 선택해 주십시오.
</div>

<div id='content'>
   <div class='content'>
      <div class='donate_top'>
         <div class='tit'>
            <img src='/images/campaign/kara/detail_title.png'>
         </div>
         <div class='tit_band'>
            <img src='/images/campaign/kara/detail_title_band.png'>
         </div>
         <div class='meet_info'>
            <div class='info'>
               <h1>팬미팅 기본정보</h1>
               <div class='detail'>
                  ● 일시 : 2014년 5월 24일 PM 07:00<br/>
                  ● 장소 : 블루스퀘어 삼성카드홀<br/>
                  ● 입장시간 : 2014년 5월 24일 (토) PM 06:30 ~ PM 06:50<br/>
                  ● 티켓수령 : 팬미팅 당일 티켓부스에서 진행 (신분증 지참)<br/>
                  ● 티켓부스 운영시간 : PM 05:30 ~ <br/>
                  <hl>(당일 현장 상황에 따라 변경될 수 있습니다)</hl>
               </div>
               <div class='add'>
                  ※ 좌석 배정은 <hl>결제 선착순</hl>이며 선택 불가능합니다. 후원과 동시에 좌석을 확인할 수 있습니다.<br/>
                  ※ 본 행사의 모든 수익은 저소득층 청소년 후원에 사용되는만큼 후원금 <hl>취소/환불은 불가합니다</hl>.
               </div>
            </div>
         </div>
      </div>

      <div class='donate_info'>
      <form method='post' name='donate'>
         <div class='left'>
            <div class='ticket'>
               <h1>티켓 수량</h1>
               <table>
                  <tr>
                     <td>KARA 자선 팬미팅</td>
                     <td>5,000원</td>
                     <td>1매</td>
                  </tr>
                  <tr class='bot_row'>
                     <td colspan='2'>TOTAL</td>
                     <td>5,000원</td>
                  </tr>
               </table>
            </div>
            <div class='payment'>
               <h1>결제 방법</h1>
               <table>
                  <tr>
                     <td><input type="radio" name='od_method' value="100000000000" id='cd'><label for='cd'>신용카드</label></td>
                     <td><input type="radio" name='od_method' value="010000000000" id='ac'><label for='ac'>계좌이체</label></td>
                  </tr>
                  <tr>
                     <td><input type="radio" name='od_method' value="000010000000" id='mo'><label for='mo'>핸드폰</label></td>
                     <td><input type="radio" name='od_method' value="001000000000" id='vc'><label for='vc'>가상계좌(무통장입금)</label></td>
                  </tr>
               </table>
            </div>
         </div>
         <div class='right'>
            <div class='mb_info'>
               <h1>개인정보입력</h1>
               <table>
                  <tr>
                     <td class='left_col'>이름</td>
                     <td><input type='text' name='buyr_name' value='<?=$my[mb_name]?>'></td>
                  </tr>
                  <tr>
                     <td>전화번호</td>
                  <td class='contact'>
                     <input type='text' name='od_hp1' class='numonly' value='<?=$my_ctt[0]?>'> - 
                     <input type='text' name='od_hp2' class='numonly' value='<?=$my_ctt[1]?>'> - 
                     <input type='text' name='od_hp3' class='numonly' value='<?=$my_ctt[2]?>'> 
                  </td>
                  </tr>
                  <tr>
                     <td>이메일</td>
                     <td><input type='text' name='buyr_mail' value='<?=$my[mb_email]?>'></td>
                  </tr>
                  <tr>
                     <td>주소</td>
                     <td>
                        <div class='zipcode-finder'>
                           <input type='text' id="dongName" />
                           <div class='btn_cont'>
                              <input type='button' class='zipcode-search' value='검색'/>
                           </div>
                           <div class="zipcode-search-result"></div>
                        </div>
                        <div id='addr'>
                           <input type='text' name="od_zip1" value="<?=$my[mb_zip1]?>" size='3' maxlength='3' readonly /> -
                           <input type='text' name="od_zip2" value="<?=$my[mb_zip2]?>" size='3' maxlength='3' readonly />
                           <input type='text' name="od_addr1" value="<?=$my[mb_addr1]?>" readonly /><br/>
                           <input type='text' name="od_addr2" value="<?=$my[mb_addr2]?>" />
                        </div>
                     </td>
                  </tr>
               </table>
            </div>
            <div class='donate_btn'>
               <div class='btn_cont' form-name='donate'>
                  후원 결제하기
               </div>
            </div>
         </div>
         <? if(!$isMobile) { ?>
         <input type='hidden' name='ordr_idxx'		value='<?=date('ymdHis').rand(100, 999)?>' />
         <input type='hidden' name='it_id'			value='<?=$it_id?>' />
         <input type='hidden' name='pay_method'		value='' />
         <input type='hidden' name='buyr_tel1'		value='' />
         <input type='hidden' name='buyr_tel2'		value='' />
         <input type='hidden' name='good_name'		value='<?=$cam_chk[it_name]?>' />
         <input type='hidden' name='good_mny'		value='' />
         <input type='hidden' name='req_tx'			value='pay' />
         <input type='hidden' name='site_cd'			value="<?=$g_conf_site_cd?>" />
         <input type='hidden' name='site_key'		value="<?=$g_conf_site_key?>" />
         <input type='hidden' name='site_name'		value="<?=$g_conf_site_name?>" />
         <input type='hidden' name='quotaopt'		value='12' />
         <input type='hidden' name='currency'		value='WON'/>
         <input type="hidden" name="vcnt_expire_term" value="3"/>
         <!-- PLUGIN 설정 (변경 불가) -->
         <input type='hidden' name='module_type'		value='01' />
         <input type='hidden' name='epnt_issu'		value='' />
         <input type='hidden' name='res_cd'			value='' />
         <input type='hidden' name='res_msg'			value='' />
         <input type='hidden' name='tno'				value='' />
         <input type='hidden' name='trace_no'		value='' />
         <input type='hidden' name='enc_info'		value='' />
         <input type='hidden' name='enc_data'		value='' />
         <input type='hidden' name='ret_pay_method'	value='' />
         <input type='hidden' name='tran_cd'			value='' />
         <input type='hidden' name='bank_name'		value='' />
         <input type='hidden' name='bank_issu'		value='' />
         <input type='hidden' name='use_pay_method'	value='' /> 
         <input type='hidden' name='cash_tsdtime'	value='' />
         <input type='hidden' name='cash_yn'			value='' />
         <input type='hidden' name='cash_authno'		value='' />
         <input type='hidden' name='cash_tr_code'	value='' />
         <input type='hidden' name='cash_id_info'	value='' />
         <!-- 제공 기간 설정 0:일회성 1:기간설정(ex 1:2012010120120131) -->
         <input type='hidden' name='good_expr'		value='0' />
         <input type='hidden' name='site_logo'		value='' />
         <input type='hidden' name='skin_indx'		value='1' />
         <input type='hidden' name='connect_type'	value='PC' />
         <input type='hidden' name='disp_tax_yn'		value='N' />
         <? }  else { ?> 
         <input type='hidden' name='ordr_idxx'     value='<?=$ordr_idxx? $ordr_idxx : date('ymdHis').rand(100, 999)?>' /> 
         <input type='hidden' name='it_id'         value='<?=$it_id?>' />
         <input type='hidden' name='pay_method'    value='' />
         <input type='hidden' name='req_tx'        value='pay' />
         <input type='hidden' name='site_cd'       value="<?=$g_conf_site_cd?>" />
         <input type='hidden' name='site_key'      value="<?=$g_conf_site_key?>" />
         <input type='hidden' name='shop_name'     value="<?=$g_conf_site_name?>" />
         <input type='hidden' name='quotaopt'      value='12' />
         <input type='hidden' name='currency'      value='410'/>
         <input type="hidden" name='Ret_URL'       value="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>/donate">
         <input type="hidden" name='param_opt_1'    value=""/>
         <input type="hidden" name='param_opt_2'    value=""/>
         <input type="hidden" name='param_opt_3'    value=""/>
         <input type="hidden" name='escw_used'    value="N">
         <input type='hidden' name='connect_type'  value='MOBILE' />
         <input type="hidden" name="encoding_trans" value="UTF-8">
         <input type="hidden" name="PayUrl">
         
         <input type='hidden' name='buyr_tel1'     value='<?=$buyr_tel1?> '/>
         <input type='hidden' name='buyr_tel2'     value='<?=$buyr_tel2?>' />
         <input type="hidden" name="res_cd"         value="<?=$res_cd?>">               <!-- 결과 코드          -->
         <input type="hidden" name="tran_cd"        value="<?=$tran_cd?>">              <!-- 트랜잭션 코드      -->
         <input type="hidden" name="good_mny"       value="<?=$good_mny?>">             <!-- 휴대폰 결제금액    -->
         <input type="hidden" name="good_name"      value="<?=$cam_chk[it_name]?>">            <!-- 상품명             -->
         <input type="hidden" name="enc_info"       value="<?=$enc_info?>">
         <input type="hidden" name="enc_data"       value="<?=$enc_data?>">
         <input type="hidden" name="use_pay_method" value="<?=$use_pay_method?>">
         <input type="hidden" name="cash_tr_code"   value="<?=$cash_tr_code?>">
         <!-- PLUGIN 설정 (변경 불가) -->
         <input type='hidden' name='ActionResult'  value=''>
         <input type="hidden" name='approval_key'  id="approval">
         <input type='hidden' name='module_type'      value='01' />
         <input type='hidden' name='res_msg'       value='' />
         <input type='hidden' name='tno'           value='' />
         <input type='hidden' name='trace_no'      value='' />
         <input type='hidden' name='ret_pay_method'   value='' />
         <input type='hidden' name='cash_tsdtime'  value='' />
         <input type='hidden' name='cash_authno'      value='' />
         <input type="hidden" name="vcnt_expire_term" value="3"/>
         
         <!-- 제공 기간 설정 0:일회성 1:기간설정(ex 1:2012010120120131) -->
         <input type='hidden' name='good_expr'     value='0' />
         <input type='hidden' name='site_logo'     value='' />
         <input type='hidden' name='skin_indx'     value='1' />

         <? } ?>

         <? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
         <input type='hidden' name='token'			value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
      </form>
      </div>
   </div>

</div>

<?php
   include LAYOUT_ROOT.'/tail.php';
}
?>
