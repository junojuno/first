<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';

if($_status) {
   include LAYOUT_ROOT.'/head.php';

?>

<div id='content'>
   <div class='content'>
      <div class='campaign_top'>
         <div class='tit'>
            <img src='/images/campaign/kara/detail_title.png'>
         </div>
         <div class='tit_band'>
            <img src='/images/campaign/kara/detail_title_band.png'>
         </div>
         <div class='video'>
            <? if($cam[it_youtube]) {?>
            <iframe width="425" height="290" src="http://www.youtube.com/embed/<?=$cam[it_youtube]?>?wmode=opaque">
            </iframe>
            <? } else echo $inner; ?>
         </div>

         <div class='sns_share'>
            <div class='fb-wrapper'>
               <div class='fb-like' 
                  data-href='http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>' 
                  data-send='false' 
                  data-layout='button_count' 
                  data-width='100' 
                  data-show-faces='false'>
               </div>
            </div>
            <div class='twit'>
            <a href="https://twitter.com/share" class="twitter-share-button" 
               count='horizontal' 
               data-url='http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>' 
               data-lang="ko">Tweet</a>
            </div>
            <div class='fb_share'>
               <img src='/images/campaign/btn_share.png' />
            </div>
         </div>
      </div>

      <div class='campaign_info'>
         <div class='tit'>
            <img src='/images/campaign/kara/detail_info_title.png'>
         </div>
         <div class='info'>
            <div class='left'>
               <div class='img_cont'>
                  <img src='/images/campaign/kara/detail_left_info.png'>
               </div>
            </div>
            <div class='right'>
               <div class='right_top'>
                  <div class='summary_title'>
                     <img src='/images/campaign/kara/detail_right_title.png'>
                  </div>
                  <!--
                  <div class='gauge_cont'>
                     <div class='gauge'>
                        <div class='gauge_bg'>
                           <div class='gauge_bar' style='width:<?=$gauge_pct?>%'></div>
                        </div>
                        <div class='gauge_info'>
                           <div class='gauge_count'><?=$now?>명</div>
                           <div class='gauge_target'><?=$target?>명</div>
                           <div class='clear'></div>
                        </div>
                     </div>
                  </div>
                  -->

                  <div class='donate_btn_cont'>
                     <div class='btn_cont' href='/campaign/<?=$it_id?>/donate'>
                        <img class='donate_btn' src='/images/campaign/donate_btn.png'>
                     </div>
                  </div>
               </div>
               <div class='right_bottom'>
                  <img src='/images/campaign/kara/detail_right_info.png'>
               </div>

            </div>
         </div>
      </div>
   </div>
</div>

<?php
   include LAYOUT_ROOT.'/tail.php';
}
?>
