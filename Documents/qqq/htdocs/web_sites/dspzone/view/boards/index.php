<?php
   include $_SERVER[DOCUMENT_ROOT].'/auto_load.php';
   include LAYOUT_ROOT.'/head.php';
?>
<div id='content'>
   <div class='content'>
      <div class='boards'>
         <div class='notice'>
            <div class='title'>
               <img src='/images/common/notice_title.png'>               
            </div>
            <div class='list'>
               <table class='head'>
                  <tr>
                     <td class='no'>NO</td>
                     <td class='title'>제목</td>
                     <td class='datetime'>작성일</td>
                  </tr>
               </table>
               <div class='slides'>
                  <div class='items'>
                  <? if($notice_num == 0) { ?>
                     <div>
                        <table class='page'>
                           <tr><td>등록된 게시글이 없습니다</td></tr>
                        </table>
                     </div>
                  <? } else { ?>
                     <? for($i =1; $i <= $notice_page_num;$i++) { ?>
                     <div>
                     <table class='page'>
                        <? for($j =$notice_count, $tmp_count =1; $j < $notice_num; $j++, $notice_count++,$tmp_count++) { 
                           if($tmp_count % ($row_num+1) == 0) break;
                        ?>
                        <tr class='row' data-page='<?=$i?>' data-id='<?=$notices[$j][id]?>'>
                           <td class='no'><?=($notice_num-$j)?></td>
                           <td class='title'>
                              <?=mb_strlen($notices[$j][title],'utf-8') > 19 ? mb_substr($notices[$j][title],0,19,'utf-8').'...' : $notices[$j][title]?>
                           </td>
                           <td class='datetime'><?=date("Y-m-d",strtotime($notices[$j][upload_time]))?></td>
                        </tr>
                        <? } ?>
                     </table>
                     </div>
                     <? } ?>
                  <? } ?>
                  </div>
               </div>
               <div class='paging'>
                  <span class='arrow left'><<</span> 
                  <span class='arrow prev'><</span> 
                  <? for($i =1; $i <= $notice_page_num; $i++) { ?> 
                     <span data-page='<?=$i?>'><?=$i?></span>
                  <? } ?>
                  <span class='arrow next'>></span> 
                  <span class='arrow right'>>></span>
               </div>
            </div>
            <div class='view'>
               <table class='ctt'>
                  <tr>
                     <td class='board_title'></td>
                  </tr>
                  <tr>
                     <td class='board_time'></td>
                  </tr>
                  <tr>
                     <td class='board_content'></td>
                  </tr>
               </table>
               <div class='cont'>
                  <div class='btn_cont'>
                     <div class='list_btn'>목록</div>
                  </div>
               </div>
            </div>

         </div>

         <div class='nanum'>
            <div class='title'>
               <img src='/images/common/nanum_title.png'>               
            </div>
            <div class='list'>
               <table class='head'>
                  <tr>
                     <td class='no'>NO</td>
                     <td class='title'>제목</td>
                     <td class='datetime'>작성일</td>
                  </tr>
               </table>
               <div class='slides'>
                  <div class='items'>
                  <? if($nanum_num == 0) { ?>
                     <div>
                        <table class='page'>
                           <tr><td>등록된 게시글이 없습니다</td></tr>
                        </table>
                     </div>
                  <? } else { ?>
                     <? for($i =1; $i <= $nanum_page_num;$i++) { ?>
                     <div>
                     <table class='page'>
                        <? for($j =$nanum_count, $tmp_count =1; $j < $nanum_num; $j++, $nanum_count++,$tmp_count++) { 
                           if($tmp_count % ($row_num+1) == 0) break;
                        ?>
                        <tr class='row' data-page='<?=$i?>' data-id='<?=$nanums[$j][id]?>'>
                           <td class='no'><?=($nanum_num-$j)?></td>
                           <td class='title'>
                              <?=mb_strlen($nanums[$j][title],'utf-8') > 19 ? mb_substr($nanums[$j][title],0,19,'utf-8').'...' : $nanums[$j][title]?>
                           </td>
                           <td class='datetime'><?=date("Y-m-d",strtotime($nanums[$j][upload_time]))?></td>
                        </tr>
                        <? } ?>
                     </table>
                     </div>
                     <? } ?>
                  <? } ?>
                  </div>
               </div>
               <div class='paging'>
                  <span class='arrow left'><<</span> 
                  <span class='arrow prev'><</span> 
                  <? for($i =1; $i <= $nanum_page_num; $i++) { ?> 
                     <span data-page='<?=$i?>'><?=$i?></span>
                  <? } ?>
                  <span class='arrow next'>></span> 
                  <span class='arrow right'>>></span>
               </div>
            </div>
            <div class='view'>
               <table class='ctt'>
                  <tr>
                     <td class='board_title'></td>
                  </tr>
                  <tr>
                     <td class='board_time'></td>
                  </tr>
                  <tr>
                     <td class='board_content'></td>
                  </tr>
               </table>
               <div class='cont'>
                  <div class='btn_cont'>
                     <div class='list_btn'>목록</div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
</div>


<?php
   include LAYOUT_ROOT.'/tail.php';
?>
