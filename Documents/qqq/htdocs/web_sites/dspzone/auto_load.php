<?php
   /* REDIRECT */
   if ($_SERVER[HTTP_HOST] == 'www.dspzone.co.kr') {
      header("Location: http://dspzone.co.kr".$_SERVER[REQUEST_URI]);
   }

   if (strpos($_SERVER[HTTP_HOST], 'www.') !== false) {
      $url = str_replace('www.', '', $_SERVER[HTTP_HOST]);

      header("Location: http://".$url.$_SERVER[REQUEST_URI]);
   }

   $_debug = ($_SERVER[HTTP_HOST] == '14.47.134.182') ? true : false;

   include $_SERVER[DOCUMENT_ROOT].'/include/config.php';
   include $_SERVER[DOCUMENT_ROOT].'/include/base_function.php';
   
   include $_SERVER[DOCUMENT_ROOT].'/include/database.php';

   if($_AJAX) {
      if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
         header("HTTP/1.0 404 Not Found");
         exit;
      }
      $json_ret = array();
   } else {
      if(file_exists(CTR_ROOT.SCRIPT_NAME)) {
         include CTR_ROOT.SCRIPT_NAME;
      }
   }


?>
