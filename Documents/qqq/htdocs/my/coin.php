<?
$_loginrequired = true;
$_required = true;
include '../config.php';
include '../module/_head.php';
$isbtb = btbCheck($_SESSION[user_no], $settings[btb]);
if($isbtb) {
   switch(getMbrType($_SESSION[user_no])) {
      case 5:
         $cpy_name = "헬로제주";
         $cpy_url = "hellojeju";
         break;
      case 10:
         $cpy_name = "SK";
         $cpy_url = "sk";
         break;
      default:
         $cpy_name = "";
         $cpy_url = "";
         break;
   }
}
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>

	<div id='mypage' style='position: relative'>
	<menu class='submenu'>
		<a href='./'><li>내 뱃지</li></a>
		<li>내 코인</li>
		<a href='./donated'><li>후원목록</li></a>
	</menu>
	<div style='clear: both'></div>

		<ul id='myInfo' style='margin-top: 30px'>
			<li><?=drawPortrait($_SESSION[user_no]); ?></li>
			<li><strong><?=$_SESSION[username]?></strong> 님</li>
			<li>
				<? $level = getWegenLevel($_SESSION[user_no], true); ?>
				<div><div class='wegenLevel'>WEGEN LEVEL <span class='level'><?=$level?></span></div></div>
			</li>
			<li><button class='modify'>회원정보 수정</button></li>
		</ul>
		<div style='clear: both'></div>
<? if(!$isbtb) { ?>
   <div style='position: absolute; top: 50px; right: 0px; width: 250px; background-color: #F4F4F4'>
      <div style='padding: 15px; font-size: 9pt; line-height: 20px'>
      <?
      $data = sql_fetch("SELECT * FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' AND isFirst = '1' ");
      if($data[od_id]) {
      ?>
      정기후원 시작일 : <strong><?=array_pop(array_reverse(explode(' ', $data[od_time])))?></strong><br/>
      정기후원 금액 : <strong><?=number_format($data[od_amount]);?>원</strong> <span style='color: #E30000'>(<?=$data[vow_percentage]?>%)</span><br/>
      정기후원 결제일 : <strong>매월 <?=$data[preferdate]?>일</strong>
      <? } else { ?>
      아직 정기후원을 신청하지 않으셨습니다.<br/><br/>
      <a href='/givex/' class='red'><strong>정기후원 신청하기 &gt;</strong></a>
      <? } ?>
      </div>
   </div>
<? } ?>

		<table cellpadding='0' cellpadding='0' style='width: 100%; margin: 30px auto; border-top: solid 2px gray; border-bottom: solid 1px gray'>
			<colgroup>
				<col width='100' />
				<col width='/' />
				<col width='100' />
				<col width='100' />
				<col width='100' />
				<col width='100' />
			</colgroup>
			<tr style='text-align: center; font-family: NanumGothicBold; color: #E30000; text-shadow: 0px 1px #F8C0C0'>
			<td>일시</td>
			<td>내역</td>
			<td>충전코인</td>
			<td>사용코인</td>
			<td>잔여코인</td>
			<td>사용기간</td>
			</tr>
		<?
		$sql = "SELECT * FROM ".DB_COINS." o
				LEFT JOIN ".DB_CAMPAIGNS." c ON (o.it_id = c.it_id)
				WHERE mb_no = '$_SESSION[user_no]' AND coin_category !=4
				ORDER BY id DESC
				";
		$result = sql_query($sql);
		$total = mysql_num_rows($result);

		$totalAmount = 0;
		$coin_sum = getCoin($_SESSION[user_no]);

		for ($i = 0; $row = sql_fetch_array($result); $i++) {
			$datetime = explode('-', $row[od_time]);

			switch ($row[coin_category]) {
				case 1:
					$category = '충전';
					$desc = "정기결제 첫 신청";
					$coin_get = number_format($row[amount]);
					unset($coin_used);
					break;
				case 2:
					$category = '충전';
					$desc = $datetime[0]."년 ".$datetime[1]."월 정기결제";
					$coin_get = number_format($row[amount]);
					unset($coin_used);
					break;
				case 3:
					$category = '충전';
					$desc = "GIVE 코드 입력";
					$coin_get = number_format($row[amount]);
					unset($coin_used);
					break;
				// case 4 : 기업이 코인요청한거
				case 8:
					$category = '사용';
					$desc = "코인 유효기간 만료 / <a href='/campaign/".$row[it_id]."'>[".$row[it_name]."]</a>에 자동 후원";
					$coin_used = number_format(abs($row[amount]));
					unset($coin_get);
					break;
				case 9:
					$category = '사용';
					$desc = "<a href='/campaign/".$row[it_id]."'>[".$row[it_name]."]</a>에 후원";
					$coin_used = number_format(abs($row[amount]));
					unset($coin_get);
					break;
				case 10: // 제공되는건 정기후원처리안함 , 10~11 : btb관리자에서 생성한 계쩡 로그 | 12~16 : btb관리자 계정 로그  
					$category = '제공';
					$desc = "{$cpy_name}에서 코인 제공";
					$coin_get = number_format(abs($row[amount]));
					unset($coin_used);
					break;
				case 11:
					$category = '회수';
					$desc = "{$cpy_name}에서 코인 회수";
					$coin_used = number_format(abs($row[amount]));
					unset($coin_get);
					break;
				case 12:
					$category = '제공';
					$desc = "위젠에서 코인 충전";
					$coin_get = number_format(abs($row[amount]));
					unset($coin_used);
					break;
				case 13:
					$category = '회수';
					$desc = "위젠에서 코인 회수";
					$coin_used = number_format(abs($row[amount]));
					unset($coin_get);
					break;
				case 14:
					$category = '사용';
					$desc = "특정 계정 생성 <strong><a href='/btb/{$cpy_url}/coin_log'>자세히</a></strong>";
					$coin_used = number_format(abs($row[amount]));
					unset($coin_get);
					break;
				case 15:
					$category = '회수';
					$desc = "특정 계정 코인 회수 <strong><a href='/btb/{$cpy_url}/coin_log'>자세히</a></strong>";
					$coin_get = number_format(abs($row[amount]));
					unset($coin_used);
					break;
				case 16:
					$category = '사용';
					$desc = "특정 계정 코인 충전 <strong><a href='/btb/{$cpy_url}/coin_log'>자세히</a></strong>";
					$coin_used = number_format(abs($row[amount]));
					unset($coin_get);
					break;
			}

			$date = mktime(0, 0, 0, $datetime[1], substr($datetime[2], 0, 2), $datetime[0]);
			$deadline = ($category == '충전') ? strftime( '%Y-%m-%d', strtotime( '+1 month', $date)).' 까지' : '-';
		?>
			<tr style='text-align: center'>
			<td style='font: 8pt Arial'><?=$row[od_time]?></td>
			<td style='text-align: left'><?=$desc?></td>
			<td style='text-align: right; padding-right: 10px'><?=$coin_get?></td>
			<td style='text-align: right; padding-right: 10px'><?=$coin_used?></td>
			<td style='text-align: right; padding-right: 10px'><?=number_format($coin_sum)?></td>
			<td style='font: 8pt Arial, NanumGothic'><?=$deadline?></td>
			</tr>
			<?
				$coin_sum -= intval($row[amount]);
				$totalAmount += (intval($row[amount]) > 0) ? intval($row[amount]) : 0;
			}
			?>
		</table>

		<? if(!$isbtb) { ?>
         <div style='text-align: right; font-weight: bold'>총 정기후원 금액 : <span style='color: #E30000'><?=number_format($totalAmount)?></span> 원</div>
      <? } ?>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li:eq(1)').addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>