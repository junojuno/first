<?
$_loginrequired = true;
$_required = true;
include '../config.php';
include '../module/_head.php';
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>

	<div id='mypage'>
	<menu class='submenu'>
		<a href='./'><li>내 뱃지</li></a>
		<a href='./coin'><li>내 코인</li></a>
		<li>후원목록</li>
	</menu>
	<div style='clear: both'></div>

		<ul id='myInfo' style='margin-top: 30px'>
			<li><?=drawPortrait($_SESSION[user_no]); ?></li>
			<li><strong><?=$_SESSION[username]?></strong> 님</li>
			<li>
				<? $level = getWegenLevel($_SESSION[user_no], true); ?>
				<div><div class='wegenLevel'>WEGEN LEVEL <span class='level'><?=$level?></span></div></div>
			</li>
			<li><button class='modify'>회원정보 수정</button></li>
		</ul>
		<div style='clear: both'></div>

		<table cellpadding='0' cellpadding='0' style='width: 100%; margin: 30px auto; border-top: solid 2px gray; border-bottom: solid 1px gray'>
			<colgroup>
				<col width='100' />
				<col width='100' />
				<col width='/' />
				<col width='100' />
				<col width='100' />
				<col width='100' />
			</colgroup>
			<tr style='text-align: center; font-family: NanumGothicBold; color: #E30000; text-shadow: 0px 1px #F8C0C0'>
         <td style='width:120px;'>후원코드</td>
			<td>후원일시</td>
			<td style='width:100px;'>카테고리</td>
			<td style='width:255px;'>캠페인명</td>
			<td>후원금액</td>
			<td>영수증 신청</td>
         <td>기타</td>
			</tr>
		<?
		$sql = "SELECT * FROM ".DB_ORDERS." o
				LEFT JOIN ".DB_CAMPAIGNS." c ON (o.it_id = c.it_id)
				WHERE mb_no = '$_SESSION[user_no]'
				AND o.od_amount > 0
				ORDER BY od_time DESC
				";
		$result = sql_query($sql);
		$total = mysql_num_rows($result);

		for ($i = 0; $row = sql_fetch_array($result); $i++) {
			$totalAmount += intval($row[od_amount])-intval($row[pay_remain]);
         
			$category = explode(',', $row[category]);
         $od_escrow = explode('/', $row[od_escrow1]);
		?>
			<tr style='text-align: center'>
         <td style='font-size:12px;'><?=$row[od_id]?></td>
			<td style='font: 8pt Arial'><?=$row[od_time]?></td>
			<td>[<?=$category[0]?>]</td>
			<td style='text-align: left'><a href='/campaign/<?=$row[it_id]?>'><?=$row[it_name]?></a></td>
         <td style='text-align: right; padding-right: 10px'>
            <span id='<?=$row[od_id]?>'><?=number_format(intval($row[od_amount])-intval($row[pay_remain]))?> 원</span>
         </td>
         <? if($row[od_method] != '무료응모' && $row[pay_remain] > 0) {
            $content = "<table><tr><th style=\"width:100px;\">은행/예금주</th><td>$od_escrow[0] / $od_escrow[1]</td></tr><tr><th>계좌번호</th><td>$od_escrow[2]</td></tr><tr><th>입금날짜기한</th><td>".date("Y-m-d H:m:s",strtotime($od_escrow[3]))."</td></tr><tr><th>기부액</th><td>".number_format($row[od_amount])."원 중 ".number_format($row[od_amount]-$row[pay_remain])."</td></tr></table>";
            ?>
            <script>
            $(document).ready(function(){
               $('span#<?=$row[od_id]?>').css({
                  "cursor":"pointer",
                  "text-decoration":"underline",
                  "font-weight":"bold"
               });
               $('span#<?=$row[od_id]?>').click(function() {
                  if($(this).data("qtip")) $(this).qtip("destroy");
                  else {
                     $(this).qtip({
                     content : '<?=$content?>',
                        position: {
                           corner: {
                              tooltip:'topMiddle',
                              target:'bottomMiddle'
                           }
                        },
                        show: {
                           when: false,
                           ready:true
                        },
                        hide:false,
                        style: {
                           padding: 15,
                           textAlign: 'left',
                           tip: true
                        }
                     }); 
                  }
                });
            });
            </script>
            <?
         }
         ?>
			<td><? print ($row[isReceipt] != 0) ? "신청" : "미신청" ?></td>
         <td><?=$row[var_1]?></td>
			</tr>
		<? } ?>
		</table>

		<div style='text-align: right; font-weight: bold'>총 후원 금액 : <span style='color: #E30000'><?=number_format($totalAmount)?></span> 원</div>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li:eq(2)').addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>
