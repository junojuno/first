<?
$_loginrequired = true;
$_required = true;
include '../config.php';
include '../module/_head.php';
include '../module/class.upload.php';

//UPDATE 루틴
if(isset($_POST['is_modify'])) {
	if($_POST['is_modify'] == 'Y') {
      $agree_sms = $_POST[agree_sms] == '1' ? 1 : 0;
      $agree_mail = $_POST[agree_mail] == '1' ? 1 : 0;

		if($stmt = $mysqli->prepare("UPDATE ".DB_MEMBERS." SET mb_name= ?, mb_contact = ?, mb_zip1 = ?, mb_zip2 = ?, mb_addr1 = ?, mb_addr2 = ?, mb_agreesms = ?, mb_agreemail =? WHERE mb_no = ?")) {
			$stmt->bind_param('ssssssiii',$_POST[username],$_POST[mb_contact], $_POST[od_zip1], $_POST[od_zip2], $_POST[od_addr1], $_POST[od_addr2],$agree_sms, $agree_mail,$_SESSION['user_no']);
			$stmt->execute();
			$SESSION['user_name'] = $_POST[username];
		}
		
		if(!$_SESSION['is_facebook'] && !$_SESSION['is_twitter'] && $_POST['p'] != '') {
			$password = $_POST['p'];
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$password = hash('sha512', $password.$random_salt);
			
			if ($stmt = $mysqli->prepare("UPDATE ".DB_MEMBERS." SET mb_password = ?, salt = ? WHERE mb_no = ?")) {
				$stmt->bind_param('ssi', $password, $random_salt, $_SESSION[user_no]);
				$stmt->execute();
			}
			
			$_SESSION['login_string'] = hash('sha512', $password.$_SERVER['HTTP_USER_AGENT']);
		}
		
		if(isset($_POST['profile_delete'])) {
			mysql_query("UPDATE ".DB_MEMBERS." SET mb_icon = '' WHERE mb_no = '{$_SESSION['user_no']}' ");
		}
		
		if(isset($_POST['facebook_delete'])) {
			mysql_query("UPDATE ".DB_MEMBERS." SET fb_profile = 0 WHERE mb_no = '{$_SESSION['user_no']}' ");
		} else {
			mysql_query("UPDATE ".DB_MEMBERS." SET fb_profile = 1 WHERE mb_no = '{$_SESSION['user_no']}' ");
		}
		
		if(isset($_POST['twitter_delete'])) {
			mysql_query("UPDATE ".DB_MEMBERS." SET tw_profile = 0 WHERE mb_no = '{$_SESSION['user_no']}' ");
		} else {
			mysql_query("UPDATE ".DB_MEMBERS." SET fb_profile = 1 WHERE mb_no = '{$_SESSION['user_no']}' ");
		}
		
		if ($_FILES['profilepicture']) {
			$handle = new upload($_FILES['profilepicture']);
			if ($handle->uploaded) {
				$handle->forbidden				= array('application/*');
				$handle->allowed				= array('image/*');
				$handle->image_convert			= 'jpg';
				$handle->file_new_name_body		= 'profile_'.$_SESSION[user_no];
				$handle->image_resize			= true;
				$handle->image_ratio_crop		= true;
				$handle->image_x				= 100;
				$handle->image_y				= 100;
				$handle->file_overwrite 		= true;
				$handle->process('../data/member/');
		
				if ($handle->processed) {
					$handle->clean();
					mysql_query("UPDATE ".DB_MEMBERS." SET mb_icon = 'profile_{$_SESSION['user_no']}.jpg' WHERE mb_no = '{$_SESSION['user_no']}' ");
				} else {
					echo "<script>alert('프로필 사진 업로드 실패  : $handle->error')</script>";
				}
			}
		}
		
		echo "<script>$('input[name=is_modify]').val('N'); alert('회원정보가 수정되었습니다.');</script>";
	}
}

$info = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
$info_contact = explode("-", $info[mb_contact]);

?>

<script type='text/javascript' src='/js/encrypt.js'></script>
<script type='text/javascript' src='/js/zipsearch.js'></script>
<script>
$(document).ready(function() {
	$('input[type=text], input[type=password]').addClass('text');

	$('#modifyForm').submit(function() {
		if( !$.trim($('input[name=username]').val()) ) {
			qAlert('이름을 입력해주세요.');
			return false;
		}

      var pw = $.trim($('input[name=password]').val());
      var pw_confirm = $.trim($('input[name=password_confirm]').val());

		if('0' == <?=$_SESSION['is_facebook'] ? '1' : '0'?> && '0' == <?=$_SESSION['is_twitter'] ? '1' : '0'?>) {
         if(pw || pw_confirm) {
            if(pw.length <8) {
               qAlert('비밀번호는 8자리 이상이어야 합니다.');
               $('input[name=password]').focus();
               return false;
            }
            if(pw != pw_confirm) {
					qAlert('비밀번호와 비밀번호 확인이 서로 일치하지 않습니다.');
					$('input[name=password]').focus();
					return false;

            }
         }
		}
		
		if ($('input[name=profilepicture]').val()) {
			var ext = $('input[name=profilepicture]').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				qAlert('올바른 이미지 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
				return false;
			}
		}
      
      if($('input[name=mb_hp1]').val() && $('input[name=mb_hp2]').val() && $('input[name=mb_hp3]').val()) {
         $('input[name=mb_contact]').val($('input[name=mb_hp1]').val()+'-'+$('input[name=mb_hp2]').val()+'-'+$('input[name=mb_hp3]').val());
         if(!phone_check($('input[name=mb_contact]').val())) {
            qAlert("잘못된 연락처 입니다. 한번 더 확인해주세요");
            $('input[name=mb_hp1]').focus();
            return false;
         }
      }
      if(pw) {
         $('#registerForm').append(
            $(document.createElement("input"))
            .attr('name','p')
            .attr('type','hidden')
            .val(hex_sha512(pw))
         );
         $('input[name=password]').val('');
         $('input[name=password_confirm]').val('');
      }
		$('input[name=is_modify]').val('Y');
	});

	$('.quit_btn').click(function() {
		if(confirm('정말로 탈퇴하시겠습니까?')) {
			$.ajax({
				type : "POST",
				url : "/quit.php",
				cache : false,
				success : function(m) {
					if(m == 'success') {
						alert('탈퇴되었습니다.');
						location.href='/logout.php';
					} else {
						alert('다시 시도해주세요');
					}
				}
			});
		}
	});
});


</script>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>

	<h2 style='margin-bottom: 14px; border: none'>회원정보 수정</h2>

		<form id='modifyForm' enctype='multipart/form-data' method='post'>
		<? $_SESSION[token] = dechex(crc32(session_id().'thisisRegSALT')); ?>
		<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisRegSALT'))?>' />
		<input type='hidden' name='is_modify' value='N' />

<!--		<h3 style='border-bottom: solid 2px black; margin: 0px 0px 10px'>필수정보 입력</h3> -->
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr>
				<th>이름</th>
				<td>
					<input type='text' name='username' value='<?=$info[mb_name]?>' />
				</td>
			</tr>
			<tr>
				<th>이메일(아이디)</th>
				<td><?=$info[mb_email]?></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td>
					<input type='password' name='password' <? if($_SESSION['is_facebook'] || $_SESSION['is_twitter']) print 'readonly';?>/> 
					<? if($_SESSION['is_facebook']) { ?><span style='color:red;'> Facebook 유저는 비밀번호를 바꿀 수 없습니다.</span> 
					<? } else if($_SESSION['is_twitter']) { ?><span style='color:red;'> Twitter 유저는 비밀번호를 바꿀 수 없습니다.</span> <? } ?>
				</td>
			</tr>
			<tr>
				<th>비밀번호 확인</th>
				<td>
					<input type='password' name='password_confirm' <? if($_SESSION['is_facebook']) print 'readonly';?> />
				</td>
			</tr>
		
			<tr style='height: 120px'>
				<th>주소</th>
				<td>

					<div class='zipcode-finder'>
						<input type='text' id="dongName" />
						<input type='button' class='zipcode-search' value='검색' />
						<div class="zipcode-search-result"></div>
					</div>
					<div id='addr'>
						<input type='text' name="od_zip1" value="<?=$info[mb_zip1]?>" size='3' maxlength='3' readonly /> -
						<input type='text' name="od_zip2" value="<?=$info[mb_zip2]?>" size='3' maxlength='3' readonly />
						<input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 250px' readonly /><br/>
						<input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
					</div>
	
				</td>
			</tr>
         <tr>
            <th>핸드폰</th>
            <td>
               <input type='text' name='mb_hp1' maxlength='4' class='numonly' style='width:30px' value='<?=$info_contact[0]?>'/> - 
               <input type='text' name='mb_hp2' maxlength='4' class='numonly' style='width:30px' value='<?=$info_contact[1]?>'/> - 
               <input type='text' name='mb_hp3' maxlength='4' class='numonly' style='width:30px' value='<?=$info_contact[2]?>'/>
               <input type='hidden' name='mb_contact' class='numonly'/>
            </td>
         </tr>
			<tr>
				<th>프로필 사진</th>
				<td>
					<input type='file' name='profilepicture' />
					100*100 크기로 자동 조정됩니다. <br/>
					<input type='checkbox' name='profile_delete'/> 기존 이미지 삭제하기 <br/>
					<? if($_SESSION['is_facebook']) { ?>
					<input type='checkbox' name='facebook_delete' <? if( isShowFbProfile($_SESSION[user_no]) == 0) print 'checked'; ?>/> 페이스북 이미지 안보이기 <br/>
					<? } else if($_SESSION['is_twitter']) {  ?>
					<input type='checkbox' name='twitter_delete' <? if( isShowFbProfile($_SESSION[user_no]) == 0) print 'checked'; ?>/> 트위터 이미지 안보이기 <br/>
					<? } ?>
				</td>
			</tr>
         <tr>
            <th>SMS 수신동의</th>
            <td><input type="checkbox" name="agree_sms" value="1" <? if($info[mb_agreesms] == 1) echo 'checked'; ?>></td>
         </tr>

         <tr>
            <th>메일 수신동의</th>
            <td><input type="checkbox" name="agree_mail" value="1" <? if($info[mb_agreemail] == 1) echo 'checked'; ?>></td>
         </tr>
			<tr>
				<th>회원탈퇴</th>
				<td>
					<input type='button' class='quit_btn' value='회원탈퇴'/> 회원탈퇴를 하셔도 기부하신 내역은 보존됩니다.
				</td>
			</tr>
		
		</table>
		<div style='text-align: left; margin-top: 30px;margin-left:20px;'><input type="submit" value='정보수정' /></div>
		</form>
		


</div>
</div>
&nbsp;
</div>

<?
include '../module/_tail.php';
?>
