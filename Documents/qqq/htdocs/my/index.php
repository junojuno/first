<?
$_loginrequired = true;
$_required = true;
include '../config.php';
include '../module/_head.php';

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'my', referer = '$_SERVER[HTTP_REFERER]' ");
}

$level = getWegenLevel($_SESSION[user_no], true);
$badgeCount = $level ? 1 : 0;
$givex = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' ");
$givex = ($givex[cnt] > 0) ? '_on' : false;
$badgeCount = $givex ? $badgeCount + 1 : $badgeCount;

$first300 = sql_fetch("SELECT COUNT(category) AS cnt FROM ".DB_BADGES." WHERE mb_no = '$_SESSION[user_no]' AND category = '91' ");
$first300 = ($first300[cnt] > 0) ? '_on' : false;
$badgeCount = $first300 ? $badgeCount + 1 : $badgeCount;

?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>

	<div id='mypage'>
	<menu class='submenu'>
		<li>내 뱃지</li>
		<a href='./coin'><li>내 코인</li></a>
		<a href='./donated'><li>후원목록</li></a>
	</menu>
	<div style='clear: both'></div>

		<ul id='myInfo' style='margin-top: 30px'>
			<li><?=drawPortrait($_SESSION[user_no]); ?></li>
			<li><strong><?=$_SESSION[username]?></strong> 님</li>
			<li>
				<div><div class='wegenLevel'>WEGEN LEVEL <span class='level'><?=$level?></span></div></div>
			</li>
			<li><button class='modify'>회원정보 수정</button></li>
		</ul>
		<div style='clear: both'></div>

	<h3 style='margin: 30px 0px; border-bottom: solid 2px gray'>스페셜 뱃지</h3>
	<div style='position: relative; top: -54px; text-align: right; color: black; font: 12px NanumGothicBold'>총 뱃지 수 : <span class='badgeCount' style='color: #E30000'></span></div>

	<ul class='myBadge'>
		<li style='width: 125px; height: 170px' title='회원님의 위젠 레벨을 나타내는 뱃지입니다.'><img src='/images/common/badges/level<?=$level?>.png' style='width: 110px' /><p style='margin-top: 10px; font-family: NanumGothicBold'>위젠 레벨</p></li>
		<li style='width: 125px; height: 170px' title='정기후원을 신청하신 회원님께 드리는 뱃지입니다.'><img src='/images/common/badges/90<?=$givex?>.png' style='width: 110px' /><p style='margin-top: 10px; font-family: NanumGothicBold'>정기후원자</p></li>
		<li style='width: 125px; height: 170px' title='최초로 정기후원을 신청하신 300분께 드리는 뱃지입니다.'><img src='/images/common/badges/91<?=$first300?>.png' style='width: 110px' /><p style='margin-top: 10px; font-family: NanumGothicBold'>최초 300명</p></li>
	<?
	$badges = array(
				9 => 'PERFECT|후원 내역을 공유하고 응원코멘트를 모두 작성하신 회원님께 드리는 뱃지입니다.',
				1 => '개척자|각 캠페인에 처음으로 기부하신 회원님께 드리는 뱃지입니다.',
				2 => '종결자|각 캠페인에 마지막으로 기부하신 회원님께 드리는 뱃지입니다.',
				3 => '메신저|각 캠페인을 SNS로 공유해주신 회원님께 드리는 뱃지입니다.',
				4 => '히어로|캠페인에 최고액을 후원해주신 회원님께 드리는 뱃지입니다.',
//				5 => '럭키가이/럭키걸|유명인사 이벤트에 당첨되신 회원님께 드리는 뱃지입니다.',
				6 => '홈런|목표액이 달성된 캠페인에 추가로 기부하신 회원님께 드리는 뱃지입니다.',
				7 => '100%달성|후원하신 캠페인이 목표 모금액을 달성 시 드리는 뱃지입니다.',
				8 => 'VIP|총 100만원 이상 후원하신 회원님께 드리는 뱃지입니다.'
	);

	$badgess = getBadges($_SESSION[user_no]);

	foreach($badges as $k => $v) {
		$d = explode('|', $v);
		$cb = $k == 9 ? " style='clear: both'" : false;
		if ($badgess[$k]) {
			print "<li title='$d[1]'$cb><img src='/images/common/badges/${k}_on.png' class='procBadge' onclick=\"procBadge('$k', '$d[0]')\" /><p style='margin-top: 10px; font-family: NanumGothicBold'>$d[0]</p><div class='counter'>$badgess[$k]</div></li>";
		}
		else {
			print "<li title='$d[1]'$cb><img src='/images/common/badges/$k.png'/><p style='margin-top: 10px; font-family: NanumGothicBold'>$d[0]</p></li>";
		}
		$badgeCount += $badgess[$k];
	}
	?>
	</ul>
	<div style='clear: both'></div>

	<h3 style='margin: 30px 0px; border-bottom: solid 2px gray'>카테고리 뱃지</h3>

	<ul class='myBadge'>
	<?
	$badges = array(
				101 => '역사/문화|역사/문화 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				102 => '환경/동물보호|환경/동물보호 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				103 => '아동/청소년|아동/청소년 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				104 => '노인|노인 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				105 => '장애인|장애인 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				106 => '저소득가정|저소득가정 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				107 => '다문화가정|다문화가정 캠페인에 후원하신 회원님께 드리는 뱃지입니다.',
				108 => '자활|자활 캠페인에 후원하신 회원님께 드리는 뱃지입니다.'
	);
	foreach($badges as $k => $v) {
		$d = explode('|', $v);
		if ($badgess[$k]) {
			print "<li title='$d[1]'><img src='/images/common/badges/${k}_on.png'/><p style='margin-top: 10px; font-family: NanumGothicBold'>$d[0]</p><div class='counter'>$badgess[$k]</div></li>";
		}
		else {
			print "<li title='$d[1]'><img src='/images/common/badges/$k.png'/><p style='margin-top: 10px; font-family: NanumGothicBold'>$d[0]</p></li>";
		}
		$badgeCount += $badgess[$k];
	}
	?>
	</ul>
	<div style='clear: both'></div>

	</div>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li:eq(0)').addClass('on');
	$('.badgeCount').html('<?=$badgeCount?>');
});
</script>

<?
include '../module/_tail.php';
?>