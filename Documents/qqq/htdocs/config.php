<?
// error_reporting(E_ALL&~E_NOTICE&~E_WARNING); 

date_default_timezone_set('Asia/Seoul'); 
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

/* REDIRECT */
if ($_SERVER[HTTP_HOST] == 'www.wegeneration.co.kr' || $_SERVER[HTTP_HOST] == 'wegeneration.co.kr') {
	header("Location: http://wegen.kr".$_SERVER[REQUEST_URI]);
}

if (strpos($_SERVER[HTTP_HOST], 'www.') !== false) {
	$url = str_replace('www.', '', $_SERVER[HTTP_HOST]);

	header("Location: http://".$url.$_SERVER[REQUEST_URI]);
}

/* GLOBAL VARIABLES */
$_debug = ($_SERVER[HTTP_HOST] == '127.0.0.1' || $_SERVER[HTTP_HOST] == '14.47.134.182:81') ? true : false;
define(DB_MEMBERS, 'wegen_member');
define(DB_CAMPAIGNS, 'wegen_campaign');
define(DB_BANNER, 'wegen_banner');
define(DB_CAMPAIGN_CMTS, 'wegen_campaign_comment');
define(DB_ORDERS, 'wegen_donation');
define(DB_COINS, 'wegen_coin');
define(DB_BADGES, 'wegen_badge');
define(DB_PARTNERS, 'wegen_partner');
define(DB_FUNDRAISERS, 'wegen_fundraiser');
define(DB_POSTSCRIPTS, 'wegen_event');
define(DB_REGULARPAYMENT, 'wegen_regularpayment');
define(DB_MEDIA, 'wegen_media');
define(DB_ACTIVITY, 'wegen_activity');
define(DB_INFO, 'wegen_info');
define(DB_SPONSORS, 'wegen_sponsor');
define(DB_LINKS, 'wegen_link');
define(DB_REWARDS, 'wegen_reward');
define(DB_REWARDS_COUNT, 'wegen_reward_count');
define(DB_CMT_LIKES,'wegen_comment_like');
define(DB_SMS_LOG, 'wegen_sms_log');
define(DB_MAIL_LOG, 'wegen_mail_log');
define(DB_AUCTION, 'wegen_auction');
define(DB_BIDDING, 'wegen_auction_bid');
define(DB_NOTICE, 'wegen_notice');
define(DB_ABTEST, 'wegen_abtest');

define(DB_CAMPAIGNS_EN, 'wegen_campaign_en');
define(DB_PARTNERS_EN, 'wegen_partner_en');
define(DB_FUNDRAISERS_EN, 'wegen_fundraiser_en');

define(WEB_ROOT, $_SERVER[DOCUMENT_ROOT]);
define(JS_ROOT, WEB_ROOT.'/js');
define(SCRIPT_NAME, $_SERVER[SCRIPT_NAME]);

$settings[title] = '';
/*
 * 101 : wegenkr@admin.kr 
 * 3850 : hellojeju@hellojeju.com
 */
$settings[admin] = array('101', '530','25', '3248', '3504');
/* about b to b */
$settings[btb] = array('3850','8044');
$settings[btb_url][3850] = "/btb/hellojeju";
$settings[btb_url][8044] = "/btb/sk";
$arr_btb[hellojeju] = '3850';
$arr_btb[sk] = '8044';

$settings[hellojeju] = array('3850', '101', '339', '3344','25','3248');
$settings[sk] = array('8044', '101', '339', '3344','25','3248');

$DRAW_GAUGE[normal] = 0;
$DRAW_GAUGE[volunteer] = 1;
$DRAW_GAUGE[lol] = 2;
$DRAW_GAUGE[en] = 3;
$DRAW_GAUGE[cam_school] = 4;
$DRAW_GAUGE[cw] = 5;
$DRAW_GAUGE[kara_fan] = 6;

$SPECIAL[lol] = '1371778995'; 
$SPECIAL[solar] = '1372288181';
$SPECIAL[sayouri] = '1373085377';
$SPECIAL[cam_school] = '1373810233';
$SPECIAL[snoop] = '1373875737';
$SPECIAL[cw1] = '1374802696';
$SPECIAL[cw2] = '1374667358';
$SPECIAL[cam_hanta_bada] = '1376345692';
$SPECIAL[kara_fan] = '1376371774';

/* DB CONNECTION */
$mysql[host] = ($_debug) ? '127.0.0.1' : '127.0.0.1';
$mysql[user] = ($_debug) ? 'root' : 'root';
$mysql[password] = ($_debug) ? '1230' : '1230';
$mysql[database] = ($_debug) ? 'wegenkr' : 'wegenkr';
echo $_debug;

$connect = @mysql_connect("$mysql[host]","$mysql[user]","$mysql[password]") or die('CONNECTION FAILED');
@mysql_select_db("$mysql[database]") or die('DB SET FAILED');
mysql_query("SET NAMES 'utf8'");
mysql_query("SET CHARACTER SET 'utf8'");
$mysqli = new mysqli($mysql[host], $mysql[user], $mysql[password], $mysql[database]);
$mysqli->set_charset('utf8');


/* TWITTER API SETTINGS */
define('CONSUMER_KEY', 'WhSlwAMgzWvZrxvOd5kIw');
define('CONSUMER_SECRET', '3GA6Oglzu709E9CYZgmubDnmssxBJcPtN3XYanakqY');
define('OAUTH_CALLBACK', 'http://wegen.kr/module/twitter/callback.php');

/* KCP SETTINGS */
$g_conf_site_name	= 'WEGENERATION';
$g_conf_log_level	= '3';
$g_conf_gw_port	= '8090';
$g_conf_mode		= 0;		// 배치결제
$g_conf_home_dir	= ($_debug) ? '/var/www/kcp' : '/home/hosting_users/wegenkr/kcp/';
$g_conf_gw_url		= ($_debug) ? 'testpaygw.kcp.co.kr' : 'paygw.kcp.co.kr'; // testpaygw.kcp | paygw.kcp
$g_conf_js_url		= ($_debug) ? 'http://pay.kcp.co.kr/plugin/payplus_test_un.js' : 'http://pay.kcp.co.kr/plugin/payplus_un.js'; // payplus_test_un.js | payplus_un.js
$g_wsdl           = ($_debug) ? "KCPPaymentService.wsdl" : "real_KCPPaymentService.wsdl";
$module_type		= '01';

$g_conf_site_cd		= ($_debug) ? 'T0000' : 'N4434';
$g_conf_site_key	= ($_debug) ? '3grptw1.zW0GSo4PQdaGvsF__' : '4cCKT.BIpikzV9J0qLdYJFK__';

$g_conf_batch_cd	= ($_debug) ? 'BA001' : 'E0326';
$g_conf_batch_key	= ($_debug) ? '2T5.LgLrH--wbufUOvCqSNT__' : '1YYjS3R4hTlagkqHNU4kdvN__';

/* ANTI SQL-INJECTION */
if(get_magic_quotes_gpc()) {
	$_GET  = array_map("stripslashes", $_GET);
	$_POST = array_map("stripslashes", $_POST);
}
$_GET  = array_map("cleanup", $_GET);
$_GET  = array_map("mysql_real_escape_string", $_GET);
$_POST = array_map("cleanup", $_POST);
$_POST = array_map("mysql_real_escape_string", $_POST);

/* Mobile Check */
include "module/Mobile_Detect.php";
$detect = new Mobile_Detect;
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();

/* FUNCTIONS LIBRARY */

function cleanup($str) {
	return strip_tags(htmlspecialchars($str, ENT_QUOTES));
}

function alert($msg, $url=null) {
	// header('Content-Type:text/html;charset=utf-8');
	$str = $url ? "document.location.href = '$url';" : "history.back();";
	print "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert('$msg'); $str </script>";
	exit;
}

function sql_query($sql, $error=TRUE) {
    if ($error)
        $result = @mysql_query($sql) or die("<p>$sql</p><p>" . mysql_errno() . " : " .  mysql_error() . "</p><p>error file : $_SERVER[PHP_SELF]</p>");
    else
        $result = @mysql_query($sql);
    return $result;
}

function sql_fetch($sql, $error=TRUE) {
    $result = sql_query($sql, $error);
    $row = sql_fetch_array($result);
    return $row;
}

function sql_fetch_array($result) {
    $row = @mysql_fetch_assoc($result);
    return $row;
}

function sec_session_start($regen = true) {
	$session_name = 'sec_session_id'; // Set a custom session name
	$secure = false; // true if using https

	ini_set('session.use_only_cookies', 1);
	$cookieParams = session_get_cookie_params();
	session_set_cookie_params(time()+60*60*24*30, $cookieParams['path'], $cookieParams['domain'], $secure, true); 
	session_name($session_name);
	session_start();
   if($regen) {
      session_regenerate_id(true);
   }
}

function login($email, $password, $mysqli) {
	$account = explode('@',$email);
	$password_original = $password;
	//$password = hash('sha512', $password);
	if ($stmt = $mysqli->prepare("SELECT mb_no, mb_name, mb_password, salt, quit, isAdmin FROM wegen_member WHERE mb_id = ? LIMIT 1")) {
		$stmt->bind_param('s', $email);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($user_no, $username, $db_password, $salt, $quit, $isAdmin);
		$stmt->fetch();
		$password = hash('sha512', $password.$salt);

		if ($stmt->num_rows == 1) {
	//		if(checkbrute($user_no, $mysqli) == true) { 
			if ($quit == 1) {
					// Account is locked
				return false;
			} else {
				if($db_password == $password) {
					$user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
					
//					$_SESSION['userid'] = filter_var($user_id, FILTER_VALIDATE_EMAIL); // XSS protection
					$_SESSION['user_no'] = preg_replace('/[^0-9]+/', '', $user_no); // XSS protection
					$_SESSION['username'] = htmlspecialchars(strip_tags($username)); // XSS protection
					$_SESSION['login_string'] = hash('sha512', $password.$user_browser);
					$_SESSION['is_facebook'] = $account[0] == 'fb' ? true : false;
					$_SESSION['is_twitter'] = $account[0] == 'tw' ? true : false;
               $_SESSION['is_admin'] = $isAdmin == 1 ? true : false;
					// Login successful.
					mysql_query("INSERT INTO ".DB_ACTIVITY." SET mb_no = '$user_no', action = '0', param1 = 'login' ");
               // 자동로그인을 위한 쿠키저장
               if(!isset($_COOKIE[wegen_new_cookie]) || !isset($_COOKIE[wegen_id]) || !isset($_COOKIE[wegen_pw])) {
                  setcookie('wegen_new_cookie','0.0',time()+60*60*24*30,'/');
                  setcookie('wegen_id',$email,time()+60*60*24*30,'/');
                  setcookie('wegen_pw',$password_original,time()+60*60*24*30,'/');
               }

					return true;
				} else {
					// Password is not correct
//					$mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_no', '$now')");
					$check = sql_fetch("SELECT mb_no, mb_name FROM wegen_member WHERE mb_id = '$email' AND mb_password_old = password('$password_original') ");
					if ($check[mb_no]) {
						// using old mysql password algorithm
						$mysqli->query("UPDATE wegen_member SET mb_password = '$password' WHERE mb_id = '$email' ");

						$user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
	//					$_SESSION['userid'] = filter_var($user_id, FILTER_VALIDATE_EMAIL); // XSS protection
						$_SESSION['user_no'] = preg_replace('/[^0-9]+/', '', $user_no); // XSS protection
						$_SESSION['username'] = htmlspecialchars(strip_tags($username)); // XSS protection
						$_SESSION['login_string'] = hash('sha512', $password.$user_browser);
						$_SESSION['is_facebook'] = $account[0] == 'fb' ? true : false;
						$_SESSION['is_twitter'] = $account[0] == 'tw' ? true : false;
                  $_SESSION['is_admin'] = $isAdmin == 1 ? true : false;

                  // 자동로그인을 위한 쿠키저장
                  if(!isset($_COOKIE[wegen_id]) && !isset($_COOKIE[wegen_pw]) ) {
                     setcookie('wegen_id',$email,time()+60*60*24*30,'/');
                     setcookie('wegen_pw',$password_original,time()+60*60*24*30,'/');
                  } 
						return true;
					}
					else {
						return false;
					}
				}
			}
		} else {
		// No user exists. 
		return '0009';
		}
	}
}

function login_check($mysqli) {
   // Check if all session variables are set
   if(isset($_SESSION['user_no'], $_SESSION['username'], $_SESSION['login_string'])) {
     $user_no = $_SESSION['user_no'];
     $login_string = $_SESSION['login_string'];
     $username = $_SESSION['username'];
 
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
 
     if ($stmt = $mysqli->prepare("SELECT mb_password FROM wegen_member WHERE mb_no = ? LIMIT 1")) { 
        $stmt->bind_param('i', $user_no); // Bind "$user_no" to parameter.
        $stmt->execute(); // Execute the prepared query.
        $stmt->store_result();
 
        if($stmt->num_rows == 1) { // If the user exists
           $stmt->bind_result($password); // get variables from result.
           $stmt->fetch();
           $login_check = hash('sha512', $password.$user_browser);
           if($login_check == $login_string) {
              return true;
           } else {
              return false;
           }
        } else {
            return false;
        }
     } else {
        return false;
     }
   } else {
     return false;
   }
}

function checkbrute($user_no, $mysqli) {
   // Get timestamp of current time
   $now = time();
   // All login attempts are counted from the past 2 hours. 
   $valid_attempts = $now - (2 * 60 * 60); 
 
   if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) { 
      $stmt->bind_param('i', $user_no); 
      // Execute the prepared query.
      $stmt->execute();
      $stmt->store_result();
      // If there has been more than 5 failed logins
      if($stmt->num_rows > 5) {
         return true;
      } else {
         return false;
      }
   }
}


function drawPortrait($uno, $isTooltip=null) {
	$data = sql_fetch("
			SELECT mb_id, mb_name, mb_icon, tw_icon, fb_profile, tw_profile
			FROM ".DB_MEMBERS."
			WHERE mb_no = '$uno'
			");
	$id = explode('@', $data[mb_id]);
	if($id[0] == 'fb') {
		$url = $data[mb_icon] != '' ? '/data/member/'.$data[mb_icon] : ($data[fb_profile] == 1 ? "https://graph.facebook.com/$id[1]/picture" : '/images/common/no_photo.png');
	} else if($id[0] == 'tw') {
		$url = $data[mb_icon] != '' ? '/data/member/'.$data[mb_icon] : ($data[tw_profile] == 1 ? ($data[tw_icon] ? $data[tw_icon] : '/images/common/no_photo.png') : '/images/common/no_photo.png');
	} else {
		$url = $data[mb_icon] != '' ? '/data/member/'.$data[mb_icon] : '/images/common/no_photo.png';
	}
	
	// $url = ($data[mb_icon] || $data[tw_icon]) ? ($data[tw_icon]) ? $data[tw_icon] : $path.'/data/member/'.$data[mb_icon] : $path.'/images/common/no_photo.png';
	// $url = ($id[0] =='fb') ? "https://graph.facebook.com/$id[1]/picture" : $url;
	$title = ($isTooltip == true) ?  " title='$data[mb_name]'" : false;
//	if ($data[mb_level] < 3) {
		print "<img class='portrait' src='{$url}?".time()."' memberNo='$uno' $title />"; // time() => 이미지 캐싱 방지. (개인정보 수정후 바로 보일 수 잇도록)
//	}
}

function getProfilePhotoUrl($uno) {
	$data = sql_fetch("
			SELECT mb_id, mb_name, mb_icon, tw_icon, fb_profile, tw_profile
			FROM ".DB_MEMBERS."
			WHERE mb_no = '$uno'
			");
   $head = "http://$_SERVER[HTTP_HOST]";
	$id = explode('@', $data[mb_id]);
	if($id[0] == 'fb') {
		$url = $data[mb_icon] != '' ? $head."/data/member/".$data[mb_icon] : ($data[fb_profile] == 1 ? "https://graph.facebook.com/$id[1]/picture" : $head.'/images/common/no_photo.png');
	} else if($id[0] == 'tw') {
		$url = $data[mb_icon] != '' ? $head."/data/member/".$data[mb_icon] : ($data[tw_profile] == 1 ? ($data[tw_icon] ? $data[tw_icon] : $head.'/images/common/no_photo.png') : $head.'/images/common/no_photo.png');
	} else {
		$url = $data[mb_icon] != '' ? $head.'/data/member/'.$data[mb_icon] : $head.'/images/common/no_photo.png';
	}

   return $url;
}

function getCoin($uno) {
	//$check = sql_fetch("SELECT isFirst FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$uno' AND isFirst = '1' ");
	//if ($check[isFirst]) {
		$data = sql_fetch("SELECT SUM(amount) AS coin_sum FROM ".DB_COINS." WHERE mb_no = '$uno' AND coin_category != 4");
		return $data[coin_sum] != null ? $data[coin_sum] : '0';
	//}
	//else return false;
}

function getWegenLevel($uno, $isReturnNumber=null, $count=null, $amount=null) {
	if ($count && $amount) {
		$data[count] = $count;
		$data[amount] = $amount;
	} else {
		$data = sql_fetch("
				SELECT COUNT(DISTINCT it_id) AS count, SUM(od_amount) AS amount
				FROM ".DB_ORDERS."
				WHERE mb_no = '$uno'
				");
	}
	
	$level = 0;
	$levels = array(1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50);
	for ($i = 0; $i < count($levels); $i ++) {
		$amount = ($i == 0) ? 1 : 10000;
		if ($data[count] >= $levels[$i] && $data[amount] >= $levels[$i] * $amount) {
			$level = $levels[$i];
		}
	}
	return ($level == 0) ? $isReturnNumber ? 0 : '없음' : $level;
}

function getBadges($uno, $isMain=null) {
	$order = $isMain ? 'RAND()' : 'category ASC';
	$limit = $isMain ? 'LIMIT 5' : false;

	$badges = array(
				1 => '개척자', 2 => '종결자', 3 => '메신저', 4 => '히어로',
				5 => '럭키가이/럭키걸', 6 => '홈런', 7 => '100% 달성', 8 => 'VIP',
				9 => 'PERFECT',
				101 => '캠페인 후원: 역사/문화', 102 => '캠페인 후원: 환경/동물보호',
				103 => '캠페인 후원: 아동/청소년', 104 => '캠페인 후원: 노인',
				105 => '캠페인 후원: 장애인', 106 => '캠페인 후원: 저소득가정',
				107 => '캠페인 후원: 다문화가정', 108 => '캠페인 후원: 자활'
			);

	$sql = "
			SELECT category, COUNT(category) AS num
			FROM ".DB_BADGES."
			WHERE mb_no = '$uno'
			AND category != '90'
			GROUP BY category
			ORDER BY $order
			$limit
			";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	for ($i = 0; $data = sql_fetch_array($result); $i++) {
		$j = $data[category];
//		$data[category] = (strlen($data[category]) == 1) ? '0'.$data[category] : $data[category];

		if ($isMain) {
			print "<li title='$badges[$j]'><img src='/images/common/badges/".$data[category]."_on.png'/></li>";
		}
		else {
			$arr[$j] = $data[num];
		}
	}
	if ($isMain) {
		for ($i = 0; $i < (5 - $total); $i++) {
			print "<li>&nbsp;</li>";
		}
	}
	else return $arr;
}

function drawGauge($funded=0, $targeted=0, $drawAmountInfo=true, $today=null, $type = 0) {
   global $DRAW_GAUGE;

	$percent = @ceil($funded * 100 / $targeted); 
	//$percent = ($funded > 0 && $percent == 0) ? 1 : $percent;
	$percent = ($percent == 100 && $funded < $targeted ) ? 99 : $percent;
	$width = ($percent >= 100) ? 100 : $percent;
	$today = ($today) ? "<div class='progressToday' style='width: ".$today."%'></div>" : false;

   switch($type) {
      case $DRAW_GAUGE[volunteer]:
         $unit = '명';
         break;
      case $DRAW_GAUGE[lol]:
         $unit = '팀';
         break;
      case $DRAW_GAUGE[en]:
         $unit = false;
         $front = '￦';
         $added = " <span style='font-size:7pt'> ($1 = about ￦1,000)</span>";
         break;
      case $DRAW_GAUGE[cam_school]:
         if($width == 0) {
            $c_img_src = "/images/campaign/cam_school_gauge_0.gif";
            $startc = "start";

         } else if($width < 50) $c_img_src = "/images/campaign/cam_school_gauge_1.gif";
         else if($width <100) $c_img_src = "/images/campaign/cam_school_gauge_2.gif";
         else {
            $c_img_src = "/images/campaign/cam_school_gauge_3.png";
            $endc = "end";
         }
         $c_html_1 = "<img class='cam_school $startc $endc' src='$c_img_src'>";
         break;
      case $DRAW_GAUGE[cw]:
         $unit = '개';
         $funded = (int)($funded / 15000);
         $targeted = $targeted / 15000;
         $percent = $funded*100 / $targeted;
         break;
      case $DRAW_GAUGE[kara_fan]:
         $unit = '명';
         $funded = $funded >= $targeted ? $targeted : ($funded + 32);
         break;
      default:
         $unit = '원';
         break;
   }
	$funded = number_format($funded);
	$targeted = number_format($targeted);

	print "
			<div class='gauge'>
				<div class='progress' style='width: ${width}%;position:relative;'>$today$c_html_1</div>
				<div class='percent' style='z-index:1;'>${percent}%</div>
			</div>";
	if ($drawAmountInfo) print "
			<span class='funded'>{$front}{$funded}${unit}{$added}</span>
			<span class='targeted'>{$front}${targeted}${unit}</span>
			<div style='clear: left'></div>";
}

function encoding($s1, $s2, $arr) {
	while(list($key, $val) = each($arr)) {
		$arr[$key] = iconv($s1, $s2, $val);
	}
}

function isShowFbProfile($uno) {
	$sql = "SELECT fb_profile FROM ".DB_MEMBERS." WHERE mb_no = '$uno'";
	$result = sql_fetch($sql);
	return $result[fb_profile];
}

function getMbrType($uno) {
	$sql = "SELECT mb_type FROM ".DB_MEMBERS." WHERE mb_no = '$uno'";
	$result = sql_fetch($sql);
	return $result[mb_type];
}

function btbCheck($uno, $arr_btb) {
	if(array_search($uno, $arr_btb) !== false ){
		return true;
	}
	switch(getMbrType($uno)) {
		case 5:
      case 10:
			return true;
		default:
			return false;
	}
}


?>
