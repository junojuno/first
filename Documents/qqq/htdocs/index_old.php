<?
$_required = true;
//$_hidestatus = true;
include 'config.php';
include 'module/_head.php';

//$fb_url = sql_fetch("SELECT main_fb_url FROM ".DB_INFO);
?>

<? include 'module/coin.php'; ?>
<div id='highlight'>&nbsp;
<div class='wrap'>

	<div style='float: left; position: relative; width: 200px; height: 400px'>
		<? if (!$_SESSION[user_no]) { ?>
		<div id='loginBox'><div style='padding: 50px 10px 10px'>
			<h3 class='blind'>로그인</h3>
			<ul id='loginCombo'>
				<li class='btn_login_facebook' title='페이스북 아이디로 로그인'>페이스북 아이디로 로그인</li>
				<li class='btn_login_twitter' title='트위터 아이디로 로그인'>트위터 아이디로 로그인</li>
				<li class='btn_login' title='위젠 아이디로 로그인'>위젠 아이디로 로그인</li>
			</ul>
			<div style='clear: both'></div>
		</div></div>

		<a href='/register'><img src='/images/main/btn_signup.png' style='width: 100%; margin-top: 10px' /></a>

		<? } else { ?>
		
		<div id='myInfoBox'>
			<div style='padding: 40px 10px 10px'>
				
				<h3 class='blind'><a href='my/'>내 정보</a></h3>
				<ul id='myInfo'>
					<li><a href='my/'><?=drawPortrait($_SESSION[user_no]); ?></a></li>
					<?
					$level = getWegenLevel($_SESSION[user_no], true);
					$givex = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' ");
					$givex = ($givex[cnt] > 0) ? '_on' : false;
					$givex_desc = ($givex) ? '자' : ' 미신청';
					?>
					<li><a href='my/'><strong><?=$_SESSION[username]?></strong> 님</a></li>
					<li style='padding-top: 5px'>
					<a href='my/'><img src='/images/common/badges/level<?=$level?>.png' title='WEGEN LEVEL <?=$level?>' style='width: 45px; height: 45px; vertical-align: middle' /></a>
					<? if (!$givex) { ?><a href='/givex/'><? } ?>
					<img src='/images/common/badges/90<?=$givex?>.png' title='정기후원<?=$givex_desc?>' style='width: 45px; height: 45px; vertical-align: middle' />
					<? if (!$givex) { ?></a><? } ?>
					</li>
				</ul>
				<div style='clear: both'></div>
	
				<h3>내 뱃지</h3>
				<ul id='myBadge'>
				<?=getBadges($_SESSION[user_no], true)?>
				</ul>
				<div style='clear: both'></div>
			</div>
		</div>
		
		<? } ?>

		<div id='recentBox'><div style='padding: 40px 10px 10px'>
			<ul id='recentFeeds'>
				<? include 'module/list.recent.php'; ?>
			</ul>
		</div></div>

	</div>

	<? include 'module/list.banner.php'; ?>
	<div style='clear: both'></div>

</div>&nbsp;
</div>

<div id='content'>
	<!-- section 1 -->
	<?php 
		$sql = "SELECT it_id FROM ".DB_CAMPAIGNS." i WHERE i.type = 301 AND i.it_isPublic = '1' AND i.it_isEnd =0 AND i.it_isMain > 0";
		$res = sql_query($sql);
		if(mysql_num_rows($res) == 3 ) { 
   ?>
		<h3>Beauty Inside</h3>
   	<?php
      $listType = 'special';
      $rows = 3;
      $where = 'type = 301 AND it_isEnd = 0 AND it_isMain > 0';
      $orderBy = 'it_id ASC';
      include 'module/list.campaign.php';?>
      
      <?
      }
      
      ?>
      
   <!-- 온도계 -->
   <!--
   <div style='position:absolute;top:639px;left:50%;margin-left:-594px;width:110px;background-color:white;'>
      <div style='padding-left:1px;'>
         <img src='/images/therm_top.png'/>
      </div>
      <div>
         <div style='display:inline-block;'>
            <img src='/images/therm_left.png'/>
         </div>
         <div style='display:inline-block;height:311px;background-color:white;position:relative;width:19px;'>
            <span id='hug_percent' style='color:red;position:absolute;top:6px;left:-3px;font-size:12px;'><strong><?=ceil($hug_percent)?>%</strong></span>
            <div style='width:19px;border:1px solid black;background-color:#e2f4fe;height:283px;position:absolute;bottom:0;'>
               <div id='hug_gauge' style='position:absolute;bottom:0;background-color:red;width:100%;height:13px;'></div>
            </div>
         </div>
         <div style='display:inline-block;'>
            <img src='/images/therm_right.png'/>
         </div>
      </div>
      <div style='padding-left:1px;'>
         <img src='/images/therm_bottom.png'/>
      </div>
   </div>
   -->
   

   <? if($fb_url[main_fb_url]) { ?>
      <div id='right_fb' class='ui-widget-content' style='position:absolute;top:697px;right:50%;margin-right:-845px;z-index:1;'>
         <div class="fb-post" data-href="<?=$fb_url[main_fb_url]?>" data-width="350" data-height="500"></div>
      </div>
   <? } ?>
	
	<h3>진행 캠페인</h3>
	<?
	$listType	= 'ongoing';
	$rows		= 4;
	$where		= 'it_isMain > 0 AND it_isEnd = 0 AND type < 100';
	$orderBy	= 'it_isHighlighted DESC, (it_funded / it_target) DESC';

	include 'module/list.campaign.php';
	?>
	<!-- /section 1 -->

	<!-- section 2 -->
	<h3>성공 캠페인</h3>
	<?
	$listType	= 'done';
	$rows		= 5;
	$where		= 'it_isMain > 0 AND it_isEnd = 1';
	$orderBy	= 'it_isMain DESC, it_id DESC';
	include 'module/list.campaign.php';
	?>
   <div style='text-align:right;padding-right:12px;padding-bottom:10px;font:11pt NanumGothicBold;'><a href='/campaign/?success'>더보기 >></a></div>
	<!-- /section 2 -->

	<!-- section 3 -->
	<h3>캠페인/이벤트 후기</h3>
	<?
	$rows = 3;
	$itemsPerRow = 1;
	include 'module/list.postscript.php';
	?>
	<!-- /section 3 -->

	<!-- section 4 -->
	<h3>WEGEN Partners</h3>
	<?
	$pListType = 'main';
	include 'module/list.partners.php'; ?>
	<!-- /section 4 -->

	<!-- section 5 -->
	<h3>WEGEN Sponsors</h3>
	<? include 'module/list.sponsors.php'; ?>
	<!-- /section 5 -->

	<div class="fb-like-box" style='margin-top: 50px' data-href="http://www.facebook.com/WeGeneration" data-width="930" data-height="405" data-show-faces="true" data-stream="false" data-header="false"></div>
</div>
<?
include 'module/_tail.php';
?>


