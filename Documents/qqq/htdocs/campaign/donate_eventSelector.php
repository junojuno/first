<?
$sql = "SELECT * FROM ".DB_FUNDRAISERS."
		WHERE it_id = '$row[it_id]'
		";
$result = sql_query($sql);
$total = mysql_num_rows($result);

if ($total > 0) {
	if($row[it_id] != '1371778995') {
?>
		<h3 style='margin-top: 20px'>이벤트 참여 여부 선택</h3>
		<ul>
			<?
			for ($i = 0; $fr = sql_fetch_array($result); $i++) {
				$fr[fr_desc] = nl2br($fr[fr_desc]);
			?>
			<li><input type='radio' name='od_isEvent' value="<?=$fr[no]?>" id='e<?=$i?>' checked/>
			<label for='e<?=$i?>' title='<?=$fr[fr_desc]?>'><strong><?=$fr[fr_name]?></strong>님의 펀드레이저 이벤트에 참여합니다.</label></li>
			<? } ?>
			<li><input type='radio' name='od_isEvent' value="미참여" id='ef' />
			<label for='ef'>이벤트에 참여하지 않겠습니다.</label></li>
		</ul>
		<div style='clear: both'></div>
	
		<p>위젠은 기부만을 희망하시는 회원님의 모금액에는 일체 수수료를 부과하지 않습니다.<br/>
		이를 통해 100% 기부를 지속적으로 가능케 하려고 합니다.</p>
		<p>기부와 더불어 유명인사와 함께하는 펀드레이저 이벤트에 참여를 희망하시는 회원님의<br/>
		모금액에는 저녁식사 등 이벤트 활동비를 고려하여 총 20%의 운영수수료를 부과합니다.</p>
		<p>이벤트는 추첨으로 진행되며 기부액과 참여도를 고려하여 확률이 가산됩니다.</p>
	<? } ?>
<? } else { ?>
		<input type='hidden' name='od_isEvent' value='없음' />
<? } ?>

<? if ($it_id == 1361249881) { ?>
<h3 class="mt30">후원할 대학</h3>
<?
	$univs = array(
		'고려대학교',
		'명지대학교',
		'상명대학교',
		'성균관대학교',
		'아주대학교',
		'연세대학교',
		'이화여자대학교',
		'한세대학교',
		'한양대학교',
		'홍익대학교'
		);
	$records = 9;
	$univ = explode('univ=', $_SERVER[REQUEST_URI]);
	$univ = $univ[1];
	for ($i=0; $i < count($univs); $i++) {
		if ($univ == ($i+1)) $checked = "checked ";
?>
		<input type="radio" name="is_event" value="<?=$univs[$i]?>" <?=$checked?> />
		<img src='/images/campaign/univ/logo_<?=$i+1?>.png' style='width: 30px; height: 30px; vertical-align: middle'/>
		<span style='font-weight: bold'><?=$univs[$i]?></span>에 학자금을 후원합니다.
<? }} //end of univ ?>