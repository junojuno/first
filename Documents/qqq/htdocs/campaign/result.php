<?
$_required = true;
include '../config.php';
//if (!$_POST) header("Location: /");
include '../module/_head.php';

$it_id			  = $_POST[ "it_id"          ];
$site_cd          = $_POST[ "site_cd"        ];      // 사이트코드
$req_tx           = $_POST[ "req_tx"         ];      // 요청 구분(승인/취소)
$use_pay_method   = $_POST[ "use_pay_method" ];      // 사용 결제 수단
$bSucc            = $_POST[ "bSucc"          ];      // 업체 DB 정상처리 완료 여부
/* = -------------------------------------------------------------------------- = */
$res_cd           = $_POST[ "res_cd"         ];      // 결과코드
$res_msg          = $_POST[ "res_msg"        ];      // 결과메시지
$res_msg_bsucc    = "";
/* = -------------------------------------------------------------------------- = */
$ordr_idxx        = $_POST[ "ordr_idxx"      ];      // 주문번호
$tno              = $_POST[ "tno"            ];      // KCP 거래번호
$good_mny         = $_POST[ "good_mny"       ];      // 결제금액
$good_name        = $_POST[ "good_name"      ];      // 상품명
$buyr_name        = $_POST[ "buyr_name"      ];      // 구매자명
$buyr_tel1        = $_POST[ "buyr_tel1"      ];      // 구매자 전화번호
$buyr_tel2        = $_POST[ "buyr_tel2"      ];      // 구매자 휴대폰번호
$buyr_mail        = $_POST[ "buyr_mail"      ];      // 구매자 E-Mail
/* = -------------------------------------------------------------------------- = */
// 공통
$pnt_issue        = $_POST[ "pnt_issue"      ];      // 포인트 서비스사
$app_time         = $_POST[ "app_time"       ];      // 승인시간 (공통)
/* = -------------------------------------------------------------------------- = */
// 신용카드
$card_cd          = $_POST[ "card_cd"        ];      // 카드코드
$card_name        = $_POST[ "card_name"      ];      // 카드명
$noinf			  = $_POST[ "noinf"          ];      // 무이자 여부
$quota            = $_POST[ "quota"          ];      // 할부개월
$app_no           = $_POST[ "app_no"         ];      // 승인번호
/* = -------------------------------------------------------------------------- = */
// 계좌이체
$bank_name        = $_POST[ "bank_name"      ];      // 은행명
$bank_code        = $_POST[ "bank_code"      ];      // 은행코드
/* = -------------------------------------------------------------------------- = */
// 가상계좌
$bankname         = $_POST[ "bankname"       ];      // 입금할 은행
$depositor        = $_POST[ "depositor"      ];      // 입금할 계좌 예금주
$account          = $_POST[ "account"        ];      // 입금할 계좌 번호
$va_date		      = $_POST[ "va_date"        ];      // 가상계좌 입금마감시간
/* = -------------------------------------------------------------------------- = */
// 포인트
$pt_idno          = $_POST[ "pt_idno"        ];      // 결제 및 인증 아이디
$add_pnt          = $_POST[ "add_pnt"        ];      // 발생 포인트
$use_pnt          = $_POST[ "use_pnt"        ];      // 사용가능 포인트
$rsv_pnt          = $_POST[ "rsv_pnt"        ];      // 총 누적 포인트
$pnt_app_time     = $_POST[ "pnt_app_time"   ];      // 승인시간
$pnt_app_no       = $_POST[ "pnt_app_no"     ];      // 승인번호
$pnt_amount       = $_POST[ "pnt_amount"     ];      // 적립금액 or 사용금액
/* = -------------------------------------------------------------------------- = */
//상품권
$tk_van_code	  = $_POST[ "tk_van_code"    ];      // 발급사 코드
$tk_app_no		  = $_POST[ "tk_app_no"      ];      // 승인 번호
/* = -------------------------------------------------------------------------- = */
//휴대폰
$commid			  = $_POST[ "commid"		 ];      // 통신사 코드
$mobile_no		  = $_POST[ "mobile_no"      ];      // 휴대폰 번호
/* = -------------------------------------------------------------------------- = */
// 현금영수증
$cash_yn          = $_POST[ "cash_yn"        ];      //현금영수증 등록 여부
$cash_authno      = $_POST[ "cash_authno"    ];      //현금영수증 승인 번호
$cash_tr_code     = $_POST[ "cash_tr_code"   ];      //현금영수증 발행 구분
$cash_id_info     = $_POST[ "cash_id_info"   ];      //현금영수증 등록 번호
/* = -------------------------------------------------------------------------- = */

$top_amount       = $_POST[ "top_amount"];
$my_donated       = $_POST[ "my_donated"];
$seat_info        = $_POST[ "seat_info" ];

$req_tx_name = "";

if( $req_tx == "pay" ) {
	$req_tx_name = "후원";
}
else if( $req_tx == "mod" ) {
	$req_tx_name = "매입/취소";
}
else {
	$req_tx_name = '봉사활동 신청';
}

$campaign = sql_fetch("SELECT it_shortdesc FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id' ");
$donate_info = sql_fetch("SELECT COUNT(od_id) AS num FROM ".DB_ORDERS." WHERE mb_no = $_SESSION[user_no] AND pay_remain = 0");

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	$param = ($use_pay_method == 'COIN') ? 'donate_coin' : 'donate_done';
//	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = '$param', param2 = '$ordr_idxx', referer = '$_SERVER[HTTP_REFERER]' ");
}
$it = sql_fetch("SELECT it_youtube_after FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");

if($it_id == $SPECIAL[kara_fan]) {
   if($use_pay_method == "001000000000") {
      $seat_html = "<tr><th>좌석번호</th><td>입금후에 문자와 메일로 좌석이 배정됩니다.</td></tr>";
   } else {
      $seat_html = "<tr><th>좌석번호</th><td>$seat_info <strong><a href='http://ticketimage.interpark.com/TicketImage/bluesquare/card_musical.gif'>좌석배치도</a></strong></td></tr>";
   }
}
?>
    
<div id='highlight'>&nbsp;
<div id='content' class='solid'>
	<div class='inner donation'>
   <? if($bSucc != "false" && $res_cd == "0000" && $it[it_youtube_after] != '') { 
         $ya_arr = explode("|",$it[it_youtube_after]);
         $thank_youtube_id = $ya_arr[time()%count($ya_arr)];
   ?>
		<div style='background: black;margin:20px 0;'><div id='movieFrame'></div></div>
		<script type='text/javascript'>
		$.getScript('https://www.youtube.com/iframe_api');
		function onYouTubeIframeAPIReady() {
			var p = new YT.Player('movieFrame',{
				videoId:'<?=$thank_youtube_id?>',
				width:'100%',
				height:'500',
				playerVars:{showinfo: 0,wmode:'transparent'}
			});
		}
		</script>
   <? } ?>

	<h2><?=$req_tx_name?> 결과</h2>

		<table cellpadding='0' cellpadding='0' style='width: 500px; margin: 30px auto'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>결과</th><td><?=$res_msg?> (<?=$res_cd?>)</td></tr>
<?
if ($req_tx == "pay") {
	if($bSucc == "false") {
		$res_msg_bsucc = ($res_cd == "0000") ? '결제 결과를 처리하는 중 오류가 발생하여 결제 취소 요청 되었습니다.<br/>다시 시도해주시고 만약  계속 오류가 발생한다면 1:1 문의로 메일 부탁드립니다.' : '결제 결과를 처리하는 중 오류가 발생하여 결제 취소 요청을 했지만, <strong>취소 요청에 실패했습니다.</strong><br/>반드시 1:1 문의로 알려주시기 바랍니다.';
		print "<tr><td colspan='2'>$res_msg_bsucc</td></tr>";
      print $res_cd == "0000" ? "<tr><td colspan='2'><a href='/campaign/$it_id/donate'><strong>다시 후원하기</strong></a></td></tr></table>" : "</table>";
	}
	else {
		if ($res_cd == "0000") {

         require '../module/facebook/facebook.php';
         $facebook = new Facebook(array(
           'appId'  => '211534425639026',
           'secret' => '9431d43672f271a5e9ed3c4569d9c04a',
         ));
         
         $user = $facebook->getUser();
         if ($user) {
            $response = $facebook->api('me/wegenapp:donate', 'POST', array('campaign' => "http://wegen.kr/campaign/".$it_id));
         }
         

			if (strpos($app_time, '-') === false) {
				$app_time = str_split($app_time, 2);
				$app_time = $app_time[0].$app_time[1].'-'.$app_time[2].'-'.$app_time[3].' '.$app_time[4].':'.$app_time[5].':'.$app_time[6];
			}
?>

<!--			<tr><th>후원 번호</th><td><?=$ordr_idxx?></td></tr> -->
			<tr><th>후원 캠페인명</th><td><?=$good_name?></td></tr>
			<tr><th>후원 금액</th><td><?=number_format($good_mny);?>원</td></tr>

<?
			// 카드결제
			if ( $use_pay_method == "100000000000" ) {
?>
			<tr><th>후원 수단</th><td>신용카드 <a href="javascript:receiptView('<?=$tno?>')">[영수증 출력]</a></td></tr>
         <?=$seat_html?>
<!--			<tr><th>승인시간</th><td><?=$app_time?></td></tr>
			<tr><th>결제카드</th><td><?=$card_cd?> / <?=$card_name?></td></tr>
			<tr><th>승인번호</th><td><?=$app_no?></td></tr> -->
<?
			}
			// 계좌이체
			else if ($use_pay_method == "010000000000") {
?>
			<tr><th>후원 수단</th><td>계좌이체</td></tr>
         <?=$seat_html?>
<!--			<tr><th>은행</th><td><?=$bank_name?></td></tr>
			<tr><th>승인시간</th><td><?=$app_time?></td></tr>-->
<?
			}
         // 가상계좌
         else if ($use_pay_method == "001000000000") {
?>
			<tr><th>후원 수단</th><td>가상계좌</td></tr>
         <?=$seat_html?>
         <tr>
            <th>은행/예금주</th>
            <td><?="$bankname/$depositor"?></td>
         </tr>
         <tr>
            <th>계좌번호</th>
            <td><?=$account?></td>
         </tr>
         <tr>
            <th>입금마감시간</th>
            <td><?=date("Y-m-d H:m:s",strtotime($va_date))?></td>
         </tr>
         <tr><td colspan="2" style='padding-left:14px;'>계좌 정보는 마이페이지 후원내역에서 다시 조회할 수 있습니다.</td></tr>
<?
         }
			//휴대폰
			else if ( $use_pay_method == "000010000000" ) {
?>
			<tr><th>후원 수단</th><td>핸드폰 (<?=$commid?> / <?=$mobile_no?>)</td></tr>
         <?=$seat_html?>
<!--			<tr><th>승인시간</th><td><?=$app_time?></td></tr>
			<tr><th>통신사코드</th><td><?=$commid?></td></tr>
			<tr><th>핸드폰 번호</th><td><?=$mobile_no?></td></tr>-->
<?
			}
			//코인
			else if ( $use_pay_method == 'COIN') {
?>
			<tr><th>후원 수단</th><td>코인</td></tr>
         <?=$seat_html?>
<!--			<tr><th>승인시간</th><td><?=$app_time?></td></tr>-->
<?
			}
		}
?>

		</table>

	<h2>공유하기</h2>
		<div style='text-align: center; margin: 30px 0px'>
		<img src='/images/campaign/result.png' style='margin-bottom: 30px' /><br/>
		<img class='btn_facebook clickable' src='/images/campaign/btn_share_fb.png' />
		<img class='btn_twitter clickable' src='/images/campaign/btn_share_tw.png' />
		</div>

	<h2>응원 코멘트</h2>

		<img src='/images/campaign/wtf.png'/>

		<div id='commentbox1' style='margin-top: 20px'>
			<form name='commentForm' style='margin:0px'>
			<input type='hidden' name='it_id' value='<?=$it_id?>' />
			<input type='hidden' name='category' />
			<input type='hidden' name='viewport' value='result' />
			<div style='float: left; width: 175px; height: 80px; font: 10pt/30px NanumGothicBold'>
			1) 수혜자에게 전하실 말씀<br/>
			<span id='smiley1' style='float: left; display: block; width: 30px; height: 30px; margin-right: 5px; background: url(/images/campaign/icon_smiley.png) top'></span>
			<span id='subtext1' style='font: 9pt/30px NanumGothic'>응원 한마디를 남겨보세요</span>
			</div>
			<div style='float: right; width: 70px'><img class='btn_submitcmt clickable' src='/images/campaign/btn_submitcmt.png' /></div>
			<textarea class='text' index='1' style='margin: 0px; width: 600px; height: 64px'></textarea>
			<div style='clear: both'></div>
		</div>

		<div id='commentbox2' style='margin-top: 20px'>
			<div style='float: left; width: 175px; height: 80px; font: 10pt/30px NanumGothicBold'>
			2) 스타에게 전하실 말씀<br/>
			<span id='smiley2' style='float: left; display: block; width: 30px; height: 30px; margin-right: 5px; background: url(/images/campaign/icon_smiley.png) top'></span>
			<span id='subtext2' style='font: 9pt/30px NanumGothic'>응원 한마디를 남겨보세요</span>
			</div>
			<div style='float: right; width: 70px'><img class='btn_submitcmt clickable' src='/images/campaign/btn_submitcmt.png' /></div>
			<textarea class='text' index='2' style='margin: 0px; width: 600px; height: 64px'></textarea>
			<div style='clear: both'></div>
			</form>
		</div>
<?
	}
}
?>

	</div>
</div>&nbsp;
</div>

<?
?>
<script type='text/javascript'>
$(document).ready(function() {
	$('textarea').focusout(function() {
		$('textarea').attr('name', '');
		$(this).attr('name', 'cmt');
		$('input[name=category]').val($(this).attr('index'));
	});

<? if($bSucc != "false" && $res_cd == "0000" && $use_pay_method == "001000000000" ) { 
      $od = sql_fetch("SELECT od_escrow1 FROM ".DB_ORDERS." WHERE od_id = '$ordr_idxx'");
      $escrow1 = explode("/",$od[od_escrow1]);
      $chk = $escrow1[4] == "1" ? true : false;
      if(!$chk) {
?>
      js_send_sms({
         sms_type:'donate_account_info',
         od_id:'<?=$ordr_idxx?>'
      });
<?    }
   } ?>

<? if($bSucc != 'false' && $res_cd == "0000" && $use_pay_method != "001000000000") { 
      if($it_id == $SPECIAL[kara_fan]) { ?>
         js_send_email({mail_type:'kara_donate',od_id:'<?=$ordr_idxx?>'});
         js_send_sms({sms_type:'kara_donate',od_id:'<?=$ordr_idxx?>'});
      <? } else { ?>
         js_send_email({mail_type:'donate',od_id:'<?=$ordr_idxx?>'});
         js_send_sms({sms_type:'donate',od_id:'<?=$ordr_idxx?>'});

      <? }?>

<? } ?>

<? if($it_id != $SPECIAL[cw1] && $it_id != $SPECIAL[cw2] && $it_id != $SPECIAL[kara_fan]) { ?>
   if('<?=$my_donated?>'*1 >= '<?=$top_amount?>'*1) {
      var isPublic ="0";
      if(confirm("최고 기부자이십니다! 최고기부자 명단에 이름을 공개하시겠습니까?")) {
         isPublic = "1";
      } else {
         isPublic = "0";
      }

      $.ajax({
         type: "POST",
         url : "/campaign/ajax.topPublic.php",
         data : {
            topPublic : isPublic,
            mbno : "<?=$_SESSION[user_no]?>",
         },
         cache : false,
         success : function(e) {
         }
      });
   }
<? } ?>
});

function postToFeed() {
	var obj = {
		method: 'feed',
		link: 'http://wegen.kr/campaign/<?=$it_id?>',
		picture: 'http://wegen.kr/data/campaign/<?=$it_id?>/list.jpg',
		name: '<?=$_SESSION[username]?>님이 위젠 캠페인에 후원하셨습니다.',
		caption: '스타와 함께 하는 즐거운 기부, 위젠',
		description: '[<?=$good_name?>] <?=$campaign[it_shortdesc]?>'
	};

	function callback(response) {
		if (response) {
			procShare('<?=$it_id?>', 'facebook', response['post_id']);
		}
	}
	FB.ui(obj, callback);
}

function postToTweet() {
	window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent('http://wegen.kr/campaign/<?=$it_id?>') + '&text=' + encodeURIComponent("위제너레이션에서 진행하는 '<?=$good_name?>'에 후원했습니다."),"twitterPop", 'width=600 height=350');
}

function receiptView(tno) {
	receiptWin = "https://admin8.kcp.co.kr/assist/bill.BillAction.do?cmd=card_bill&tno=" + tno;
	window.open(receiptWin , "" , "width=470, height=815");
}

function receiptView3() 
{ 
   receiptWin3 = "http://devadmin.kcp.co.kr/Modules/Noti/TEST_Vcnt_Noti.jsp"; 
   window.open(receiptWin3, "", "width=520, height=300"); 
} 
</script>

<?
include '../module/_tail.php';
?>
