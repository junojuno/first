<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;

$_loginrequired = true;
$_required = true;
include '../config.php';

sec_session_start();

$row = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id='$_GET[it_id]' ");
if (!$row[it_id]) { header("Location: /"); }
if ($row[it_isEnd] == '1') { alert("이미 종료된 캠페인입니다."); }
$it_opt1 = explode('|', $row[it_currency]);
$totalopt = count($it_opt1)-1;

$check = sql_fetch("SELECT od_id FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' LIMIT 1");
if (!$check[od_id] && $_SESSION[user_no]) {
	alert('정기결제를 신청하지 않으셔서 코인 후원이 불가능합니다.\\n정기결제 안내 페이지로 이동합니다.', '/givex/');
}

$check = sql_fetch("SELECT SUM(amount) AS coin FROM ".DB_COINS." WHERE mb_no = '$_SESSION[user_no]' ");
if ($check[coin] <= 0  && $_SESSION[user_no]) {
	alert('잔여 코인이 부족하여 코인 후원이 불가능합니다.\\n일반 후원을 이용해주세요.');
}

include '../module/_head.php';
?>

<script type='text/javascript' src='<?=$g_conf_js_url?>'></script>
<script type='text/javascript' src='/js/zipsearch.js'></script>
<script type='text/javascript'>
StartSmartUpdate();

$(document).ready(function() {
	$('input[type=text]').addClass('text');
});

function checkForm(form) {

	var coinLeft = <?=$check[coin]?>;

	var p = checkRadioValue(document.donationForm.od_price);
	if (!p) {
		qAlert('후원하실 금액을 선택해 주세요.');
		return false;
	}
	if (p == 'user_input') {
		p = $.trim($('input[name=user_input]').val());
		if (!p || parseInt(p) == 0) {
			qAlert('후원을 희망하는 금액을 입력해 주세요.');
			return false;
		}
		else if (p < 1000) {
			qAlert('후원은 최소 1,000원 이상부터 가능합니다.');
			return false;
		}
	}
	if (p > coinLeft) {
		qAlert('후원금액이 잔여 코인 수보다 많습니다.');
		return false;
	}
	$('input[name=good_mny]').val(p);

	if (!$.trim($('input[name=buyr_name]').val())) {
		qAlert('후원자 이름을 입력해 주세요.');
		$('input[name=buyr_name]').focus();
		return false;
	}

	var hps = ['input[name=od_hp1]', 'input[name=od_hp2]', 'input[name=od_hp3]'];
	for (var i = 0; i < 3; i++)	{
		if (!$.trim($(hps[i]).val())) {
			qAlert('후원자 연락처를 입력해 주세요.');
			$(hps[i]).focus();
			return false;
		}
	}
	$('input[name=buyr_tel1]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());

	if (!$.trim($('input[name=buyr_mail]').val())) {
		qAlert('후원자 이메일 주소를 입력해 주세요.');
		$('input[name=buyr_mail]').focus();
		return false;
	}

	if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=buyr_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('input[name=od_zip1]').val())) {
		qAlert('우편번호 검색창을 통해 주소를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=od_addr2]').val()) || $('input[name=od_addr2]').val() == '상세주소 입력') {
		qAlert('상세주소를 입력해 주세요.');
		return false;
	}

	if (!checkRadioValue(document.donationForm.od_isEvent)) {
		qAlert('이벤트 참여 여부를 선택해 주세요.');
		return false;
	}

	return true;
}
</script>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner donation'>

	<h2 style='margin-bottom: 14px; border: none'>코인으로 후원하기</h2>

	<div style='font: 13px/30px NanumGothicBold; color: black; text-align: center; background-color: #F4F4E3; margin-bottom: 20px'>
	<img src='/images/common/coin.png' style='vertical-align: middle' />
	현재 잔여 코인 수 :  <span style='color: #E30000; font: bold 10pt Arial'><?=number_format($check[coin])?></span>
	</div>

	<form name='donationForm' method='post' action='/campaign/proc_coin.php' onsubmit="return checkForm(this.form);">

	<!-- left -->
	<div id='donationLeft' style='float: left; width: 48%; margin-right: 2%'>

		<h3>후원금액</h3>
		<ul id='donationPrice'>
			<?
			for ($i = 0; $i < count($it_opt1); $i++) {
				$it_opt = explode(';', $it_opt1[$i]);
				$it_opt = $it_opt[0];
				if ($it_opt != 'user_input') {
			?>
			<li><input type='radio' name='od_price' value='<?=$it_opt?>' id='p<?=$i?>' />
			<label for='p<?=$i?>'><?=number_format($it_opt)?>원</label></li>
			<? }} ?>
			<li><input type='radio' name='od_price' value='user_input' />
			직접입력
			<input type='text' name='user_input' onfocus="$('input[name=od_price]:last').prop('checked',true)" style='text-align: right' class='numonly' /> 원</li>
		</ul>
		<p>1코인은 1원이며, 현재 보유하고 있는 코인 수 내에서 자유롭게 선택하여 후원 가능합니다.</p>

	<?
	$rewards = explode('||', $row[it_reward]);
//	if (count($rewards) > 1) :
	if ($row[it_reward]) :
	?>
	<h3 style='margin-top: 20px'>리워드 선택</h3>

		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>요청사항</th><td>
			<input type='text' name='reward_req' style='width: 250px' /></td></tr>
		</table>

		<p>원하는 리워드(티셔츠의 경우 사이즈 포함)를 기재해주세요.</p>
	<div style='clear: both'></div>
	<? endif; ?>

		<?
		include 'donate_eventSelector.php';
		?>
	</div>
	<!-- /left -->

	<!-- right -->
	<div style='float: left; width: 48%; margin-left: 2%'>
		<h3>개인정보 입력</h3>
		<?
		$info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
		$contact = explode('-', $info[mb_contact]);
		?>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>이름</th><td>
			<input type='text' name='buyr_name' value="<?=$_SESSION[username]?>" /></td></tr>
			<tr><th>전화번호</th><td>
			<input type='text' name="od_hp1" value="<?=$contact[0]?>" size='3' maxlength='3' class='numonly' /> -
			<input type='text' name="od_hp2" value="<?=$contact[1]?>" size='4' maxlength='4' class='numonly' /> -
			<input type='text' name="od_hp3" value="<?=$contact[2]?>" size='4' maxlength='4' class='numonly' /></td></tr>
			<tr><th>이메일</th><td>
			<input type='text' name='buyr_mail' value='<?=$info[mb_email]?>' maxlength='50' style='width: 250px' /></td></tr>
			<tr style='height: 120px'><th>주소</th><td>

			<div class='zipcode-finder'>
				<input type='text' id="dongName" />
				<input type='button' class='zipcode-search' value='검색' />
				<div class="zipcode-search-result"></div>
			</div>
			<div id='addr'>
				<input type='text' name="od_zip1" value="<?=$info[mb_zip1]?>" size='3' maxlength='3' readonly /> -
				<input type='text' name="od_zip2" value="<?=$info[mb_zip2]?>" size='3' maxlength='3' readonly />
				<input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 250px' readonly /><br/>
				<input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
			</div>

			</td></tr>
		</table>

		<div style='text-align: center;  margin: 0px auto'>
		<p>입력하신 개인정보는 캠페인 진행 상황 및 이벤트 당첨 알림, 리워드 발송 등의 목적으로만<br/>
		사용되며, <a href='/privacy-policy'><strong>개인정보 취급방침</strong></a>에 의거해 보호됩니다.</p>
<!--		<input type='checkbox' name='saveInfo' checked />
		위 정보를 앞으로 후원시에도 자동으로 사용하겠습니다.
-->

		<input type='submit' value='후원하기' class='submit' />

		</div>
	</div>
	<!-- /right -->
	<div style='clear: both'></div>

	<input type='hidden' name='ordr_idxx'		value='<?=date('ymdHis').rand(100, 999)?>' />
	<input type='hidden' name='it_id'			value='<?=$_GET[it_id]?>' />
	<input type='hidden' name='good_name'		value='<?=$row[it_name]?>' />
	<input type='hidden' name='buyr_tel1'		value='' />
	<input type='hidden' name='buyr_tel2'		value='' />
	<input type='hidden' name='good_mny'		value='' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token'			value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
	</form>

	</div>
</div>&nbsp;
</div>

<?
include '../module/_tail.php';
?>