<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;

$_loginrequired = true;
$_required = true;
include '../config.php';
include '../module/_head.php';

$row = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id='$_GET[it_id]' AND type = '2' ");


if(!$row[it_id]) { alert("캠페인 정보가 없습니다.","$g4[path]"); }
if ($row[it_isEnd] == '1') { alert("이미 종료된 캠페인입니다."); }

$it_opt1 = explode('|', $row[it_currency]);
$totalopt = count($it_opt1)-1;

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'donate', param2 = '$row[it_id]', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<script type='text/javascript' src='/js/zipsearch.js'></script>
<script type='text/javascript'>

$(document).ready(function() {
	$('input[type=text]').addClass('text');

	$('input[name=isReceipt]').change(function() {
		if ($(this).val() == 1) {
			$('.ppForm').show();
			$('.pcForm').hide();
		}
		else {
			$('.ppForm, .pcForm').hide();
		}
	});
});

function kcp_runPlugin(form) {

	if (!$.trim($('input[name=buyr_name]').val())) {
		qAlert('봉사활동 신청자 이름을 입력해 주세요.');
		$('input[name=buyr_name]').focus();
		return false;
	}

	var hps = ['input[name=od_hp1]', 'input[name=od_hp2]', 'input[name=od_hp3]'];
	for (var i = 0; i < 3; i++)	{
		if (!$.trim($(hps[i]).val())) {
			qAlert('봉사활동 신청자 연락처를 입력해 주세요.');
			$(hps[i]).focus();
			return false;
		}
	}
	$('input[name=buyr_tel1]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());

	if (!$.trim($('input[name=buyr_mail]').val())) {
		qAlert('봉사활동 신청자 이메일 주소를 입력해 주세요.');
		$('input[name=buyr_mail]').focus();
		return false;
	}

	if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=buyr_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('input[name=od_zip1]').val())) {
		qAlert('우편번호 검색창을 통해 주소를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=od_addr2]').val()) || $('input[name=od_addr2]').val() == '상세주소 입력') {
		qAlert('상세주소를 입력해 주세요.');
		return false;
	}

	var isReceipt = checkRadioValue(document.donationForm.isReceipt);
	if (isReceipt == 1) {
		if (!$.trim($('input[name=realname]').val())) {
			qAlert('확인증 발행을 위해 실명을 입력해 주세요.');
			return false;
		}
		if (!$.trim($('input[name=serial1]').val()) || !$.trim($('input[name=serial2]').val())) {
			qAlert('확인증 발행을 위해 주민등록번호를 입력해 주세요.');
			return false;
		}
	}

}
</script>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner donation'>

	<h2 style='margin-bottom: 14px; border: none'>봉사활동 신청하기</h2>

	<form name='donationForm' method='post' action='/campaign/proc_volunteer.php'>

	<!-- left -->
	<div id='donationLeft' style='float: left; width: 48%; margin-right: 2%'>

		<h3>상세내용</h3>
<? /*
		<ul id='donationPrice'>
			<?
			for ($i = 0; $i < count($it_opt1); $i++) {
				$it_opt = explode(';', $it_opt1[$i]);
				$it_opt = $it_opt[0];
				if ($it_opt != 'user_input') {
			?>
			<li><input type='radio' name='od_price' value='<?=$it_opt?>' id='p<?=$i?>' />
			<label for='p<?=$i?>'><?=number_format($it_opt)?>원</label></li>
			<? }} ?>
			<li><input type='radio' name='od_price' value='user_input' />
			직접입력
			<input type='text' name='user_input' onfocus="$('input[name=od_price]:last').prop('checked',true)" style='text-align: right' class='numonly' /> 원</li>
		</ul>
*/ ?>
<img src='/data/campaign/<?=$row[it_id]?>/detail.jpg' style='margin-top: 20px; max-width: 100%' />
		<div style='clear: both'></div>

	</div>
	<!-- /left -->

	<!-- right -->
	<div style='float: left; width: 48%; margin-left: 2%'>
		<h3>개인정보 입력</h3>
		<?
		$info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
		$contact = explode('-', $info[mb_contact]);
		?>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>이름</th><td>
			<input type='text' name='buyr_name' value="<?=$_SESSION[username]?>" /></td></tr>
			<tr><th>전화번호</th><td>
			<input type='text' name="od_hp1" value="<?=$contact[0]?>" size='3' maxlength='3' class='numonly' /> -
			<input type='text' name="od_hp2" value="<?=$contact[1]?>" size='4' maxlength='4' class='numonly' /> -
			<input type='text' name="od_hp3" value="<?=$contact[2]?>" size='4' maxlength='4' class='numonly' /></td></tr>
			<tr><th>이메일</th><td>
			<input type='text' name='buyr_mail' value='<?=$info[mb_email]?>' maxlength='50' style='width: 250px' /></td></tr>
			<tr style='height: 120px'><th>주소</th><td>

			<div class='zipcode-finder'>
				<input type='text' id="dongName" />
				<input type='button' class='zipcode-search' value='검색' />
				<div class="zipcode-search-result"></div>
			</div>
			<div id='addr'>
				<input type='text' name="od_zip1" value="<?=$info[mb_zip1]?>" size='3' maxlength='3' readonly /> -
				<input type='text' name="od_zip2" value="<?=$info[mb_zip2]?>" size='3' maxlength='3' readonly />
				<input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 250px' readonly /><br/>
				<input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
			</div>

			</td></tr>
		</table>

		<h3 style='margin-top: 20px'>자원봉사 확인증 신청</h3>
		
		
		<? if($row[it_issueCheck]) { ?>

		
		<ul id='donationReceipt' <?= $row[it_id] == '1372442997' ? print "style='display:none;'" : false ?> >
			<li style='width: 50%'><input type='radio' name='isReceipt' value='0' id='pn' checked /><label for='pn'>미신청</label></li>
			<li style='width: 50%'><input type='radio' name='isReceipt' value='1' id='pp' /><label for='pp'>신청</label></li>
		</ul>
		<div style='clear: both'></div>
		<table cellpadding='0' cellpadding='0' style='width: 100%' <?= $row[it_id] == '1372442997' ? print "style='display:none;'" : false ?> >
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr class='ppForm' style='display: none'><th>실명</th><td>
			<input type='text' name='realname' maxlength='10' /></td></tr>
			<tr class='ppForm' style='display: none'><th>주민등록번호</th><td>
			<input type='text' name='serial1' maxlength='6' class='numonly' style='width: 50px; text-align: center' /> - <input type='text' name='serial2' maxlength='7' class='numonly' style='width: 50px; text-align: center' /></td></tr>
		</table>
		<? } else { ?>

		<? if($row[it_issueLoc]) { ?>
			<li>
				<? print '봉사시간은 웹사이트' ;?>
				<? print $row[it_issueLoc] ;?>
				<? print '을 통해서 발급 가능하십니다.' ;?>
			</li>
		<? } else { ?>
			<li>
				<? print '본 캠페인은 확인증을 발급받을 수 없습니다.' ;?>
			</li>
		<? } ?>
		<? } ?>
		<div style='text-align: center;  margin: 0px auto'>
		<p>입력하신 개인정보는 캠페인 진행 상황 및 이벤트 당첨 알림, 리워드 발송 등의 목적으로만<br/>
		사용되며, <a href='/privacy-policy'><strong>개인정보 취급방침</strong></a>에 의거해 보호됩니다.</p>

		<input type='submit' value='봉사활동 신청하기' class='submit' onclick="return kcp_runPlugin(this.form);" />

		</div>
	</div>
	<!-- /right -->
	<div style='clear: both'></div>

	<input type='hidden' name='ordr_idxx'		value='<?=date('ymdHis').rand(100, 999)?>' />
	<input type='hidden' name='it_id'			value='<?=$_GET[it_id]?>' />
	<input type='hidden' name='buyr_tel1'		value='' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token'			value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
	</form>

	</div>
</div>&nbsp;
</div>

<?
include '../module/_tail.php';
?>