<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;
$it_id = $_GET[it_id];
$_required = true;
include '../config.php';

/*
if($isMobile) {
	header("Location: /m/campaign/{$_GET[it_id]}");
	exit;
}
*/

$ipadURL = $isiPad ? '/m' : false;

$sql = "SELECT *,
			(SELECT SUM(od_amount)-SUM(pay_remain) 
			FROM ".DB_ORDERS." o 
			WHERE o.it_id = i.it_id)
		AS it_funded
		FROM ".DB_CAMPAIGNS." i
		LEFT JOIN ".DB_PARTNERS." p ON (i.it_partner = p.pt_no)
		WHERE i.it_id = '$it_id' ";
$it = sql_fetch($sql);

if($it[it_isPublic] == 0) {
   alert('잘못된 캠페인 코드입니다.');
   exit;
}
$isOpenCampaign = $it[it_isOpenCampaign] == 1 ? true : false;

$isVol = ($it[type] == '2') ? true : false;

if ($isVol) {
	$req = sql_fetch("SELECT COUNT(od_id) AS cnt FROM wegen_volunteer WHERE it_id = '$it_id' ");
	$req = $req[cnt];
}

switch($it_id) {
   case $SPECIAL[cam_school] :
      $draw_type = $DRAW_GAUGE[cam_school];
      break;
   case $SPECIAL[cw1] :
   case $SPECIAL[cw2] :
      $draw_type = $DRAW_GAUGE[cw];
      $type_cw = true;
      break;
   case $SPECIAL[kara_fan]:
      $draw_type = $DRAW_GAUGE[kara_fan];
      break;
   default:
      $draw_type = ($isVol) ? $DRAW_GAUGE[volunteer] : $DRAW_GAUGE[normal];
}


$sql = "SELECT SUM(od_amount)-SUM(pay_remain) as amount, a.mb_no, b.mb_name,b.mb_topPublic FROM wegen_donation a, wegen_member b WHERE it_id = '$it_id' and b.mb_no = a.mb_no and a.od_method !='직접입력' and a.od_method !='스타후원' and a.mb_no != 610 and a.apply_topDonor = 1 group by a.mb_no order by amount desc";
$res = sql_query($sql);
$topDonor = array();
$flag = false;
for($i =0; $row = sql_fetch_array($res);$i++) {
   if($flag) {
      if($topDonor[$i-1][0] > $row[amount]) break;
   }
   $topDonor[$i][0] = $row[amount];
   $topDonor[$i][1] = $row[mb_no];
   $topDonor[$i][2] = $row[mb_topPublic];
   $topDonor[$i][3] = $row[mb_name];
   $flag = true;
}

$sql = "SELECT *
		FROM ".DB_POSTSCRIPTS."
		WHERE wr_campaign = '$it_id'
		AND wr_category = '1'
		";
$ps = sql_query($sql);
$pstotal = mysql_num_rows($ps);
$pstotal = ($pstotal > 0) ? " ($pstotal)" : false;

$sql = "SELECT *
		FROM ".DB_POSTSCRIPTS."
		WHERE wr_campaign = '$it_id'
		AND wr_category = '2'
		";
$ev = sql_query($sql);
$evtotal = mysql_num_rows($ev);
$evtotal = ($evtotal > 0) ? " ($evtotal)" : false;
$settings[type] = 'wegenapp:campaign';
$settings[title] = $it[it_name];
$settings[desc] = $it[it_shortdesc];
$settings[image] = 'http://wegen.kr/data/campaign/'.$it[it_id].'/list.jpg';

if($isOpenCampaign && isset($_GET[oc])) {
   $js_var = "var from_fb_link_oc = true;";
}

if($isOpenCampaign) {
   $open_share_link = "http://$_SERVER[HTTP_HOST]/campaign/".$it_id."?oc=1&stats=$choice";
   $open_share_img = "http://$_SERVER[HTTP_HOST]/data/campaign/$it_id/open_campaign_share.jpg";
   $open_share_name = $it[it_ocShareTitle];
   $open_share_desc = $it[it_ocShareDesc];
}
if($isOpenCampaign && isset($_GET[stats])) {
   $connect = $isMobile ? "MOBILE" : "PC";
   if($_GET[stats] == 'a') {
      sql_query("INSERT INTO ".DB_ABTEST." SET 
         it_id = '$it_id',
         stats = 'a',
         connect = '$connect',
         cust_ip = '$_SERVER[REMOTE_ADDR]'");
   } else if($_GET[stats] == 'b') {
      sql_query("INSERT INTO ".DB_ABTEST." SET 
         it_id = '$it_id',
         stats = 'b',
         connect = '$connect',
         cust_ip = '$_SERVER[REMOTE_ADDR]'");
   }
}

include '../module/_head.php';


// ACTIVITY LOG
if ($_SESSION[user_no]) {
	$param = $_GET[view] ? $_GET[view] : 'detail';
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = '$param', param2 = '$it_id', referer = '$_SERVER[HTTP_REFERER]' ");
   $mb_info = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERe mb_no = $_SESSION[user_no]");
   $mb_contact = explode('-', $mb_info[mb_contact]);
}

/* 
$status = ($it[it_funded] >= $it[it_target]) ? '성공' : '완료';
if ($it[it_isEnd] == 0) {
	$diff = round((strtotime($it[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
	$status = ($diff < 0) ? '완료' : "D-".$diff;
	if ($diff < 0) {
		$ismain = ($status == '완료') ? ", it_isMain = '0' " : false;
		mysql_query("UPDATE ".DB_CAMPAIGNS." SET it_isEnd = '1' $ismain WHERE it_id = '$it_id' ");
	}
}
*/

if (!$it[it_id]) alert("잘못된 접근입니다.");

$partner = $it[pt_name] ? $it[pt_name] : false;
$partner = $it[pt_homepage] ? "<a href='$it[pt_homepage]' target='_blank' style='border:none;color:none;'>$partner</a>" : $partner;
$sql = "SELECT * FROM ".DB_FUNDRAISERS."
      WHERE no = '$it[it_fundraiser]'
      ";
$event_info = sql_fetch($sql);

include '../module/coin.php';

include '../module/layout/detail_open_campaign.php';
?>


<div id='highlight' class='campaign'>&nbsp;
	<div style='width: 930px; margin: 30px auto 0px'>
	<h2>
		<?=$it[it_name]?> 
	</h2>
	
	</div>

<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<a href='/campaign/<?=$it_id?>'><li>캠페인 내용</li></a>
		<? if ($isVol) : ?>
		<a href='/postscript/<?=$it_id?>'><li>봉사 후기<?=$pstotal?></li></a>
		<? else : ?>
      <a href='/campaign/<?=$it_id?>/connect'><li>캠페인 TALK</li></a>
		<a href='/postscript/<?=$it_id?>'><li>전달 후기<?=$pstotal?></li></a>
		<a href='/postscript/event/<?=$it_id?>'><li>이벤트 후기<?=$evtotal?></li></a>
		<? endif; ?>
	</menu>
	<div style='clear: both'></div>

	<!-- left -->
	<div style='float: left; margin-top: 30px; width: 600px'>
	<?
	if (!$_GET[view]) {
		$eq = '0';
		if (file_exists('../../module/custom.'.$it[it_id].'.php')) {
			include '../../module/custom.'.$it[it_id].'.php';
		}
		else if ($it[it_youtube]) {
		?>

		<div style='background: black; min-height: 400px'><div id='movieFrame'></div></div>
		<script type='text/javascript'>
		$.getScript('https://www.youtube.com/iframe_api');
		function onYouTubeIframeAPIReady() {
			var p = new YT.Player('movieFrame',{
				videoId:'<?=$it[it_youtube]?>',
				width:'600',
				height:'400',
//				origin:'http://wegen.kr',
				playerVars:{showinfo: 0,wmode:'transparent'}
			});
		}
		</script>
		<? } else { ?>
		<img src='/data/campaign/<?=$it[it_id]?>/inner.jpg' style='width: 600px; height: 400px' />
		<? } ?>
      <? if($it[it_id] != $SPECIAL[snoop] && $it[it_auction] != -1) { ?>
		<div style='margin-top: 20px; background-color: #F4F4E3; border: solid 2px #AFAFAF;'>
		<div style='padding: 10px'>
		<h3 style='margin: 0px; border: none'>
			<? 
			if($isVol || $it['type'] == 100 || $it['type'] == 101 || $it['type'] == 102 ) {
				print '참여인원';
			} else if($it[it_id] == '1371778995') {
				print '참가팀수';	
			} else if($type_cw) {
            print '판매수량';
         } else if($it[it_id] == $SPECIAL[kara_fan]) {
            echo '참여인원';
         }else {
				print '모금액';	
			}
			?>
		</h3>
		<? 
		if($it[it_id] == '1371778995') {
         $sql = "SELECT mb_no FROM ".DB_ORDERS." WHERE it_id='1371778995' AND od_amount='20000'";
         $res = sql_query($sql);
         $total = @mysql_num_rows($res); 
         drawGauge($total, 64, true, false, 2);
		} else if($it[it_id] == $SPECIAL[solar]) {
			drawGauge($it[it_funded],50000000);
			print "<br/><h3 style='margin: 0px; border: none'>한국동서발전, 유니슨 4배매칭 기부</h3>";
			$funded = $it[it_funded]*4;
			$targeted = 200000000;
			$percent = @ceil($funded * 100 / $targeted);
			//$percent = ($funded > 0 && $percent == 0) ? 1 : $percent;
			$percent = ($percent == 100 && $funded < $targeted ) ? 99 : $percent;
			$width = ($percent >= 100) ? 100 : $percent;
			$funded = number_format($funded);
			$targeted = number_format($targeted);
			$unit = '원';
			
			print "
			<div class='gauge'>
			<div class='progress' style='width: ${width}%;background-color:orange;'></div>
			<div class='percent'>${percent}%</div>
			</div>";
			print "
			<span class='funded'>${funded}${unit}</span>
			<span class='targeted'>${targeted}${unit}</span>
			<div style='clear: left'></div>";
			
		} else if($it[it_id] == $SPECIAL[sayouri] ) {
         drawGauge($it[it_funded],5000000);
         print "<br/><h3 style='margin: 0px; border: none'>사유리, 2배매칭 기부</h3>";
         $funded = $it[it_funded]*2;
         $targeted = 10000000;
         $funded = $funded >= $targeted ? $targeted : $funded;
         $percent = @ceil($funded * 100 / $targeted);
         //$percent = ($funded > 0 && $percent == 0) ? 1 : $percent;
         $percent = ($percent == 100 && $funded < $targeted ) ? 99 : $percent;
         $width = ($percent >= 100) ? 100 : $percent;
         $funded = number_format($funded);
         $targeted = number_format($targeted);
         $unit = '원';
         
         print "
         <div class='gauge'>
         <div class='progress' style='width: ${width}%;background-color:orange;'></div>
         <div class='percent'>${percent}%</div>
         </div>";
         print "
         <span class='funded'>${funded}${unit}</span>
         <span class='targeted'>${targeted}${unit}</span>
         <div style='clear: left'></div>";
      } else if ($it[it_id] == $SPECIAL[cam_hanta_bada])  {
         drawGauge($it[it_funded], $it[it_target]);
         print "<br/><h3 style='margin: 0px; border: none'>한국타이어, 5배 매칭 기부</h3>";
         $funded = $it[it_funded]*5;
         $targeted = 10000000;
         $funded = $funded >= $targeted ? $targeted : $funded;
         $percent = @ceil($funded *100 / $targeted);
         $percent = ($percent == 100 && $funded < $targeted ) ? 99 : $percent;
         $width = ($percent >= 100) ? 100 : $percent;
         $funded = number_format($funded);
         $targeted = number_format($targeted);
         $unit = '원';
         
         print "
         <div class='gauge'>
         <div class='progress' style='width: ${width}%;background-color:orange;'></div>
         <div class='percent'>${percent}%</div>
         </div>";
         print "
         <span class='funded'>${funded}${unit}</span>
         <span class='targeted'>${targeted}${unit}</span>
         <div style='clear: left'></div>";
      }
      else if($it['type'] == 100 || $it['type'] == 101 || $it['type'] == 102 ) {
			$c = sql_fetch("SELECT (count(*)) as count FROM ".DB_ORDERS." WHERE it_id='{$it['it_id']}'");
			$percent = $c['count'];
			$width = ($percent >= 100) ? 100 : $percent;
			$unit = '명';
			$mny = $it['it_funded']*1+10000000;
			$mny = number_format($mny);
			print "
			<div class='gauge'>
			<div class='progress' style='width: ${width}%'></div>
			<div class='percent'>${percent}%</div>
			</div>";
			print "
			<div style='text-align:center;'>
			<div class='funded' style='display:inline-block;float:left;'>${c['count']}${unit}</div>
			<div style='font: bold 11pt Arial, NanumGothic;display:inline-block;margin-top:5px;'>{$mny}원</div>
			<div class='funded' style='display:inline-block;float:right;'>100${unit}</div>
			</div>
			<div style='clear: left'></div>";
      } else {
         switch($draw_type) {
            case $DRAW_GAUGE[cw]:
               drawGauge($it[it_funded], $it[it_target], true, false, $DRAW_GAUGE[cw]);
               break;
            case $DRAW_GAUGE[kara_fan]:
               drawGauge($it[it_funded]/5000, $it[it_target]/5000, true, false, $DRAW_GAUGE[kara_fan]);
               break;
            default:
               ($isVol) ? drawGauge($req, $it[it_target], true, false, 1) : drawGauge($it[it_funded],$it[it_target], true, false, $draw_type); 
               break;
         }
		} 
		
		?>
		</div></div>
      <? } ?>
      <? if($it[it_auction] != -1) include WEB_ROOT."/module/detail_auction.php"; ?>

      <? if($it[it_connectors]) {
         $c_mbno = explode("|", $it[it_connectors]);
         $recent = sql_fetch("SELECT * FROM ".DB_CAMPAIGN_CMTS." a LEFT JOIN ".DB_MEMBERS." b ON b.mb_no = a.mb_no
            WHERE it_id = '$it_id' AND cmt_category = 100 ORDER BY cmt_time DESC LIMIT 1");
         if($recent[cmt]) { 
      ?>
      <div style='margin-top:15px;border:3px solid rgb(246,220,0);padding:9px;position:relative;'>
         <div style='font:13pt NaNumGothicBold;margin-bottom:15px;'>수혜자의 최근 TALK:</div>
         <a href='/campaign/<?=$it_id?>/connect' style='position:absolute;right:11px;top:11px;'>
            <img src='/images/campaign/to_connect_btn.png'>
         </a>
         <div style='display:block;margin-left:10px;'>
            <div style='display:block;margin-bottom:5px;'>
               <div style='width:40px;display:block;float:left;margin-right:8px;'><? drawPortrait($recent[mb_no]);?></div>
               <div style='display:table-cell;'>
                  <div style='line-height:26px;margin-top:2px;'>
                     <span style='font:14px NaNumGothicBold;'><?=$recent[mb_name]?></span><br/><span style='font-size:9pt;color:#a8a8a8;'><?=$recent[cmt_time]?></span><br/>
                  </div>
               </div>
            </div>
            <div class='redactor_editor'>
               <?=$recent[cmt]?>
            </div>
         </div>
      </div>
      <? }
      }?>


	<div id='campaignDetail' style='width: 600px; margin-top: 30px'>
		<div id='it_desc'>
		<?
		$customizedDetail = '../module/detail.'.$it_id.'.php';
		if (file_exists($customizedDetail)) {
			include $customizedDetail;
		}
		?>
		<? //$it[it_desc]?>
		<?
		// 2013.08.07 added 
		if(!defined("SKIPPING_DETAIL_IMAGE")) { 
		?>
		<img src='/data/campaign/<?=$it[it_id]?>/detail.jpg' style='width: 100%' />
		<? } ?>
		</div>

		<div id='it_postscript'>
		<?=$it[it_postscript]?>
		</div>
	</div>
	<?php if ($it[it_id] == $SPECIAL[solar]) { ?>
	<div style='width: 600px; margin-top: 30px'>
		<div style='background: black; min-height: 400px'>
			<iframe id="player" type="text/html" width="100%" height="400"
			  src="http://www.youtube.com/embed/mK-5SfcYuoE?enablejsapi=1"
			  frameborder="0"></iframe>
				
		</div>
	</div>
	<?php } ?>

	<? if($it[it_auction] == -1) include WEB_ROOT."/module/detail_auction.php"; ?>


	<div id='comment' style='width: 600px; margin-top: 30px'>
	<h3>응원코멘트</h3>

	<div style='width: 600px; margin-bottom: 20px; background-color: #F4F4E3'>
	<img src='/images/campaign/comment.png' />
	</div>

<? $action = ($_SESSION[user_no]) ? 'btn_submitcmt' : 'btn_login'; ?>
	<div id='commentbox' style='margin-bottom: 20px'>
		<form name='commentForm' style='margin:0px'>
		<input type='hidden' name='it_id' value='<?=$it_id?>' />
		<div style='float: left; width: 70px; height: 80px'><?=drawPortrait($_SESSION[user_no])?></div>
		<div style='float: right; width: 70px'><img class='<?=$action?> clickable' src='/images/campaign/btn_submitcmt.png' /></div>
		<textarea name='cmt' class='text' style='margin: 0px; width: 445px; height: 64px'<? print ($_SESSION[user_no]) ? false : ' readonly'; ?>></textarea>
		<input type='radio' id='c1' name='category' value='1' checked /><label for='c1'>수혜자 코멘트</label>
		<? if (!$isVol) : ?>
		<input type='radio' id='c2' name='category' value='2' class='starcmt' /><label for='c2' class='starcmt'>스타/멘토 코멘트</label>
		<input type='radio' id='c3' name='category' value='3' /><label for='c3'>스폰서 코멘트</label>
		<? endif; ?>
		<input type='radio' id='c4' name='category' value='0' /><label for='c4'>위젠 코멘트</label>
		<div style='clear: both'></div>
		</form>
	</div>
<? $action = ($_SESSION[user_no]) ? 'btn_submitreply' : 'btn_login'; ?>
	<div id='replybox'>
		<form name='replyForm' style='margin:0px'>
		<input type='hidden' name='cmt_id' />
		<div style='float: left; width: 70px; height: 80px'><?=drawPortrait($_SESSION[user_no])?></div>
		<div style='float: right; width: 70px'><img class='<?=$action?> clickable' src='/images/campaign/btn_submitcmt.png' /></div>
		<textarea name='r_cmt' class='text' style='margin: 0px; width: 445px; height: 64px'></textarea>
		<div style='clear: both'></div>
		</form>
	</div>

	<div id='commentList'>
	<?
	$sql = "SELECT *, IF(cmt_depth = '0', cmt_id, parent_id) AS orderNum
			FROM ".DB_CAMPAIGN_CMTS." p
			LEFT JOIN ".DB_MEMBERS." m ON (p.mb_no = m.mb_no) 
			WHERE it_id = '$it_id' AND cmt_category <= 3 
			ORDER BY orderNum DESC, cmt_depth ASC, cmt_time DESC";
	$result = sql_query($sql);
	$commentTotal = mysql_num_rows($result);

	for ($i=0; $row=sql_fetch_array($result); $i++) {
		$replyClass = ($row[cmt_depth] == 0) ? 'commentCell' : 'replyCell';
		$replyWidth = ($row[cmt_depth] == 0) ? '530' : '480';
	?>
	<div class='<?=$replyClass?>'>
		<ul>
		<li style='float: left; width: 70px'><?=drawPortrait($row[mb_no])?></li>
		<li style='float: left; width: <?=$replyWidth?>px'>
			<? if ($row[mb_no] == $_SESSION[user_no]) { ?><span class='btn_delete' item='<?=$row[it_id]?>' cmt='<?=$row[cmt_id]?>'>삭제</span><? } ?>
			<? if (array_search($_SESSION[user_no], $settings[admin]) !== false && $row[cmt_depth] == '0') { ?><span class='btn_reply' item='<?=$row[it_id]?>' cmt='<?=$row[cmt_id]?>'>답글</span><? } ?>
			<p><strong><?=$row[mb_name]?></strong> <span style='color: gray; font: 9px Arial'><?=$row[cmt_time]?></span></p>
			<p style='margin-top: 10px'>
			<?
			switch ($row[cmt_category]) {
				case 1 :
					print "[수혜자에게]"; break;
				case 2 :
					print "[스타/멘토에게]"; break;
				case 3 :
					print "[스폰서에게]"; break;
				case 0 :
					print ""; break;
			}
			?>
			<?=nl2br($row[cmt])?></p>
		</li>
		</ul>
		<div style='clear: left'></div>
	</div>
	<? } ?>
	<div style='border-top: solid 1px gray; clear: left; height: 50px'></div>
	<!-- 코멘트 리스트 -->
	</div>

	</div>

<!-- detail -->


	<?
	} // end of campaign detail
   else if($_GET[view] == 'connect') {
      $eq = '1';
      $connector = explode("|",$it[it_connectors]);
      /*
      $chk_donate = sql_fetch("SELECT od_id FROM ".DB_ORDERS." WHERE it_id='$it_id' AND mb_no='$_SESSION[user_no]' AND od_amount-pay_remain > 0 ");
      if(!$_SESSION[is_admin] && !$chk_donate[od_id] && array_search($_SESSION[user_no],$connector) === false) {
         alert('캠페인 톡은 캠페인에 후원하신 분들만의 비밀 네트워크 공간입니다 :)');
         exit;
      }
      */
   ?>
      <div class='connect_top'>
         <img src='/images/campaign/connect_top.png'>
      </div>

      <div>
         <h3 class='connect_h3' style='border-bottom:2px solid rgb(246,220,0);'>수혜자 TALK</h3>
         <? if($_SESSION[is_admin] || array_search($_SESSION[user_no],$connector) !== false ) {
            $_SESSION[connect_it_id] = $it_id; ?>
            <script src='/js/redactor.min.js'></script>
            <script>
               $(document).ready(function() {
                  $('textarea[name=connect_post_content]').redactor({
                     imageUpload: "/campaign/ajax.upload.php",
                     imageUploadCallback: function(image, json) {
                     }
                  });
                  $('#connect_post_btn').click(function(){
                     if('<?=$connector[0]?>' == '') {
                        qAlert('커넥터가 설정되어있지 않습니다. 캠페인관리에서 커넥터를 등록해주세요.');
                        return false;
                     }
                     if($('.redactor_.redactor_editor').html() == '<p><br></p>') {
                        qAlert('내용을 입력해주세요');
                        $('textarea[name=connect_post_content]').focus();
                        return false;
                     }
                     var cmt = $(".redactor_.redactor_editor").html();
                     $.ajax({
                        type:'POST',
                        url:'/campaign/ajax.comment.php',
                        data:{ 
                           it_id: '<?=$it_id?>',
                           target:'connect_post',
                           content: cmt
                        },
                        cache:false,
                        success:function(n) {
                           if(n== 'failed') {
                              qAlert('오류가 발생했습니다. 다시 시도해주세요');
                           } else {
                              $('.connect_posts').prepend(n);
                              $('.connect_posts .connect_post:first').hide().fadeIn("slow");
                              $('.redactor_.redactor_editor').html('<p><br><p>');
                              js_send_sms({sms_type:'connect_post_upload', it_id:'<?=$it_id?>'});
                              js_send_email({mail_type:'connect_post_upload', it_id:'<?=$it_id?>'});
                           }
                        }
                     });
                  });
               });
   
               $(document).on("click",".del_post_btn",function(){
                  if(!confirm('정말 삭제하시겠습니까?')) {
                     return false;
                  }
                  var pid = $(this).data('postid');
                  $.ajax({
                     type:'POST',
                     url:'/campaign/ajax.comment.php',
                     data:{
                        target:'connect_post',
                        mode:'delete',
                        post_id:pid
                     },
                     cache:false,
                     success:function(n) {
                        if(n=='failed') {
                           qAlert('오류가 발생했습니다. 다시 시도해주세요');
                        } else {
                           $(".connect_post[data-postid="+pid+"]").remove();
                        }
                     }
                  });
               });

            </script>
            <div class='connect_input'>
               <textarea name='connect_post_content'>
               </textarea>
               <div style='text-align:right;'>
                  <button type='button' id='connect_post_btn' class='btn' style='margin-top:5px;'>게시</button>
               </div>
            </div>
         <? } ?>
         <script>
            $(document).on("click",".reply_post_btn",function(){
               var input_obj = $(".connect_post[data-postid="+$(this).data("postid")+"] .connect_cmt.cmt_input");
               if(input_obj.css("display") == 'none') {
                  input_obj.fadeIn();
                  $(".connect_post[data-postid="+$(this).data("postid")+"] .connect_cmt.cmt_input textarea").focus();
               } else {
                  input_obj.fadeOut('fast');
               }
            });
            var twice_flag = false; // ie 한글 불완전입력중일때 shift+enter누르면 enter 두번누르는현상 방지
            var enter_flag = false;
            var shift_enter_map = {16:false,13:false};
            $(document).on("keydown",".connect_cmt.cmt_input textarea",function(e){
               var keyCode = e.which ? e.which : e.keyCode;
               if(keyCode == 16) {
                  shift_enter_map[16] = true;
               }
               if(keyCode == 13) {
                  if(!shift_enter_map[16]) {
                     e.preventDefault();
                  }
               }
            });
            $(document).on("keyup",".connect_cmt.cmt_input textarea",function(e) {
               var keyCode = e.which ? e.which : e.keyCode;
               if(keyCode != 13 && keyCode != 16) {
                  shift_enter_map[16] = false;
                  enter_flag = false;
               }
               if(keyCode == 13) {
                  if(!shift_enter_map[16]) {
                     if(!enter_flag) {
                        console.log("click :: "+shift_enter_map[16]);
                        enter_flag = true;
                        if(!twice_flag) {
                           twice_flag = true;
                           $(".connect_cmt_input_btn[data-postid="+$(this).data("postid")+"]").click();
                        }
                     } else enter_flag = false;
                  } else {
                     console.log("new line :: "+shift_enter_map[16]);
                     enter_flag = true;
                  }
                  shift_enter_map[16] = false;
               }
            });
            $(document).on("click",".connect_cmt_input_btn",function() {
               var ta_obj = $(".connect_post[data-postid="+$(this).data("postid")+"] .cmt_input textarea");
               if(ta_obj.val() == '') {
                  qAlert("댓글 내용을 입력해주세요.");
                  ta_obj.focus();
                  twice_flag = false;
                  return ;
               }
               var pid = $(this).data("postid");
               var cmt_content = ta_obj.val();
               $.ajax({
                  type:'POST',
                  url:'/campaign/ajax.comment.php',
                  data: {
                     target : 'connect_cmt',
                     it_id : '<?=$it_id?>',
                     post_id : pid,
                     cmt : cmt_content
                  },
                  cache:false,
                  success:function(n) {
                     if(n == 'failed' || n== '0009') {
                        qAlert('오류가 발생했습니다. 다시 시도해주세요.');
                     } else {
                        $(".connect_post[data-postid="+pid+"] .connect_cmt_list .added").prepend(n);
                        $(".connect_post[data-postid="+pid+"].connect_cmt_list .added:first").hide().fadeIn("slow");
                        ta_obj.val('');
                     }
                     twice_flag = false;
                  }
               });
            });
            $(document).on("click",".connect_cmt_del",function() {
               if(!confirm("정말 삭제하시겠습니까?")) {
                  return ;
               }
               
               var cid = $(this).data("cmtid");
               $.ajax({
                  type:'POST',
                  url:'/campaign/ajax.comment.php',
                  data: {
                     target : 'connect_cmt',
                     mode :'delete',
                     cmt_id : cid
                  },
                  cache:false,
                  success:function(n) {
                     console.log(n);
                     if(n == 'failed' || n== '0009') {
                        qAlert('오류가 발생했습니다. 다시 시도해주세요.');
                     } else {
                        var obj_deleted = $(".connect_cmt[data-cmtid="+cid+"]");
                        obj_deleted.fadeOut('fast');
                        obj_deleted.remove();
                     }
                  }
               });
            });
            $(document).on("click",".like_post_btn",function(){
               var pid = $(this).data('postid');
               $.ajax({
                  type:'POST',
                  url:'/campaign/ajax.like.php',
                  data:{
                     target:'connect_post',
                     post_id: pid,
                     mode: 'like'
                  },
                  success:function(n){
                     if(n == 'failed') {
                        qAlert('오류가 발생했습니다. 다시 시도해주세요.');
                     } else if(n=='exist') {
                        qAlert('이미 좋아요를 하셨습니다.');
                     } else {
                        var like_box = $(".connect_post[data-postid="+pid+"] .like_box .num");
                        var like_btn = $(".connect_post[data-postid="+pid+"] .like_post_btn");
                        like_box.animate({opacity:0},200,function(){
                           like_box.html(n);
                           like_btn.removeClass("like_post_btn");
                           like_btn.addClass("like_cancel_post_btn");
                           like_btn.html('좋아요 취소');
                        });
                        like_box.animate({opacity:1},200);
                     }
                  }
               });
            });
            $(document).on("click",".like_cancel_post_btn",function(){
               var pid = $(this).data('postid');
               $.ajax({
                  type:'POST',
                  url:'/campaign/ajax.like.php',
                  data:{
                     target:'connect_post',
                     post_id: pid,
                     mode: 'like_cancel'
                  },
                  success:function(n){
                     if(n == 'failed') {
                        qAlert('오류가 발생했습니다. 다시 시도해주세요.');
                     } else if(n=='not exist') {
                        qAlert('이미 좋아요 취소를 하셨습니다.');
                     } else {
                        var like_box = $(".connect_post[data-postid="+pid+"] .like_box .num");
                        var like_btn = $(".connect_post[data-postid="+pid+"] .like_cancel_post_btn");
                        like_box.animate({opacity:0},200,function(){
                           like_box.html(n);
                           like_btn.removeClass("like_cancel_post_btn");
                           like_btn.addClass("like_post_btn");
                           like_btn.html('좋아요');
                        });
                        like_box.animate({opacity:1},200);
                     }
                  }
               });

            });

         </script>

         <div class='connect_posts'>
         <?
            $isLike = $_SESSION[user_no] ? ", IF((SELECT COUNT(id) FROM ".DB_CMT_LIKES." d WHERE d.cmt_id = a.cmt_id AND mb_no = $_SESSION[user_no]), 1, 0) AS isLike " : false;
            $cp_res = sql_query("SELECT *,(SELECT IFNULL(COUNT(id),0) FROM ".DB_CMT_LIKES." c WHERE c.cmt_id = a.cmt_id ) AS like_num
                  $isLike FROM ".DB_CAMPAIGN_CMTS." a 
                  LEFT JOIN ".DB_MEMBERS." b ON b.mb_no = a.mb_no 
                  WHERE cmt_category = 100 AND it_id = '$it_id' 
                  ORDER BY cmt_time DESC");
            while($connect_post = sql_fetch_array($cp_res)) {
         ?>
            
            <div class='connect_post' data-postid='<?=$connect_post[cmt_id]?>'>
               <div class='connect_post_info'>
                  <div class='profile_img'><? drawPortrait($connect_post[mb_no]);?></div>
                  <div class='profile_info'>
                     <div style='line-height:26px;margin-top:2px;'>
                     <span style='font:14px NaNumGothicBold;'><?=$connect_post[mb_name]?></span><br/><span style='font-size:9pt;color:#a8a8a8;'><?=$connect_post[cmt_time]?></span><br/>
                     </div>
                  </div>
                  <div class='btn_group'>
                     <span class='<?=$_SESSION[user_no] ? ($connect_post[isLike] == 0 ? "like_post_btn" : "like_cancel_post_btn") : "btn_login"?>' data-postid='<?=$connect_post[cmt_id]?>' style='cursor:pointer;'>
                        좋아요 <?=$connect_post[isLike] == 1? "취소" : false?></span>
                     <div class='like_box'><span class='num'><?=$connect_post[like_num]?></span></div>
                     <!--<span class='<?= $_SESSION[user_no]? "reply_post_btn" : "btn_login"?>' data-postid='<?=$connect_post[cmt_id]?>' style='cursor:pointer;'>댓글달기</span> -->
                     <? if($_SESSION[connect_it_id]) { ?>&nbsp;<span class='del_post_btn' data-postid='<?=$connect_post[cmt_id]?>' style='cursor:pointer;' >삭제</span><? } ?>
                  </div>
               </div>
               <div class='redactor_editor'>
                  <?=$connect_post[cmt]?>
               </div>
               <div class='connect_cmt_list'>
                  <div class="connect_cmt cmt_input">
                     <ul>
                        <li style="float:left;width:41px"><? drawPortrait($_SESSION[user_no]); ?></li>
                        <li style="float:left;margin-left:10px;">
                           <textarea name='connect_cmt_content' data-postid='<?=$connect_post[cmt_id]?>' class='text <?=$_SESSION[user_no]? false : "btn_login";?>' 
                              style='width:438px;padding-left:7px;padding-top:7px;overflow:hidden;vertical-align:top;height:28px;'></textarea>
                           <img class='<?= $_SESSION[user_no]? "connect_cmt_input_btn" : "btn_login"?>' data-postid='<?=$connect_post[cmt_id]?>' src="/images/campaign/btn_submitcmt.png" style='cursor:pointer;width:40px;'>
                        </li>
                     </ul>
                     <div style="clear: left"></div>
                  </div>
                  <div class="added">
                     <?
                     $cmts_res = sql_query("SELECT * FROM ".DB_CAMPAIGN_CMTS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
                           WHERE it_id = '$it_id' AND cmt_category = 101 AND parent_id = '$connect_post[cmt_id]'
                           ORDER BY cmt_time DESC");
                     while($cmt_data = sql_fetch_array($cmts_res)) { 
                     ?>
                        <div class="connect_cmt" data-cmtid="<?=$cmt_data[cmt_id]?>">
                           <ul>
                              <li style="float:left;width:41px"><? drawPortrait($cmt_data[mb_no]); ?></li>
                              <li style="float:left;width:484px;margin-left:10px;">
                              <? if($_SESSION[is_admin] || $_SESSION[user_no] == $cmt_data[mb_no]) { ?>
                                 <span class="connect_cmt_del" data-cmtid="<?=$cmt_data[cmt_id]?>" style='float:right;margin-left:5px;cursor:pointer;'>삭제</span>
                              <? } ?>
                                 <p><strong><?=$cmt_data[mb_name]?></strong> 
                                 <span style="color:gray;font:9px Arial"><?=$cmt_data[cmt_time]?></span></p>
                                 <p style="margin-top: 10px"><?=$cmt_data[cmt]?></p>
                              </li>
                           </ul>
                           <div style="clear: left"></div>
                        </div>
                     <? } ?>
                  </div>
               </div>
            </div>
         <? } ?>
         
         </div>
         
      </div>

   <?
   }
   
   else if ($_GET[view] == 'postscript') {
		$eq = '2';
	?>
	<table style='width: 100%'>
	<?
		if (!$pstotal) print "<tr><td style='text-align: center'>등록된 후기가 없습니다.</td></tr>";
		for ($i = 0; $data = sql_fetch_array($ps); $i++) {
	?>
	<tr><td style='font: 13pt NanumGothicBold'><?=$data[wr_subject]?></td><td style='text-align: right; font: 9pt Arial'><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])));?></td></tr>
	<tr><td colspan='2' class='postscript redactor_editor' style='max-width: 100%; padding-bottom: 50px'><?=$data[wr_content]?></td></tr>
	<?
		}
	?>
	</table>
	<?
	} // end of postscript
	else if ($_GET[view] == 'event') {
		$eq = '3';
	?>
	<table style='width: 100%'>
	<?
		if (!$evtotal) print "<tr><td style='text-align: center'>등록된 이벤트 후기가 없습니다.</td></tr>";
		for ($i = 0; $data = sql_fetch_array($ev); $i++) {
	?>
	<tr><td style='font: 13pt NanumGothicBold'><?=$data[wr_subject]?></td><td style='text-align: right; font: 9pt Arial'><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])));?></td></tr>
<tr><td colspan='2' class='postscript redactor_editor' style='max-width: 100%; padding-bottom: 50px'><?=$data[wr_content]?></td></tr>
	<?
		}
	?>
	</table>
	<?
	} // end of event
	?>
	</div>
	<!-- /left -->

	<!-- right -->
	<div id='campaignInfo'>
		<div class='campaignDetail'>
			<div class='sns_share'>
            <div class='fb-wrapper'>
               <div class='fb-like' 
                  data-href='http://wegen.kr/campaign/?<?=$it_id?>' 
                  data-send='false' 
                  data-layout='button_count' 
                  data-width='100' 
                  data-show-faces='false'>
               </div>
            </div>
            <div class='twit'>
               <a href="https://twitter.com/share" class="twitter-share-button" 
                  count='horizontal' 
                  data-url='http://wegen.kr/campaign/<?=$it_id?>' 
                  data-lang="ko">Tweet</a>
            </div>
            <div class='fb_share'>
               <img src='/images/campaign/btn_share.png' class='btn_facebook clickable'/>
            </div>
			</div>

         <div class='detail_right_top'>
            <div class='campaign_summary'>
               <div class='cont'>
                  <div class='title'>
                     캠페인 기본 정보
                  </div>               
                  <div class='info'>
                     <li>기간: <?=date('Y.m.d', strtotime($it[it_startdate]))?> ~ <?=date('Y.m.d', strtotime($it[it_enddate]))?></li>
                     <? if($it[it_auction]!=-1){ ?>
                     <li>목표 모금액: <?=number_format($it[it_target])?> 원</li>
                     <? } ?>
                     <? if($partner) { ?>
                     <li>수혜처: <?=$partner?></li>
                     <? } ?>
                     <? if($event_info[no] && $it[it_auction]!=-1) { ?>
                     <li>이벤트: <?=nl2br($event_info[fr_desc])?></li>
                     <? } ?>
                     <? if($it[it_reward]) { ?>
                     <li>리워드: <?=nl2br($it[it_reward])?></li>
                     <? } ?>
                  </div>
               </div>
            </div>
            <div class='recent_donor'>
               <div class='title'>
                  <?= $it[it_action] != -1? '최근 기부자':'최근 입찰자'; ?>
               </div>
               <div class='list'>
                  <?
                  if($it[it_auction] == -1) {
                  	$res = sql_query("SELECT DISTINCT bid_name FROM wegen_auction as a LEFT JOIN wegen_auction_bid as b ON a.auc_id = b.auc_id WHERE a.it_id = '$it[it_id]'");
                    while($row = sql_fetch_array($res)) {
	                  	print("<li>$row[bid_name]</li>");
	              	}
                  }
                  else {
	                 $res = sql_query("SELECT mb_no, od_name FROM ".DB_ORDERS." WHERE it_id = '$it_id' AND od_amount-pay_remain > 0 GROUP BY mb_no ORDER BY od_time ASC");
                     while($row = sql_fetch_array($res)) { 
	                 	print("<li><?=$row[od_name]?></li>");
                   	 } 
              	  }?>
               </div>
            </div>
         </div>

			<?
			if ($it[it_isEnd] == 0 && $it[it_auction] != -1) {

				$checker = sql_fetch("SELECT no FROM ".DB_FUNDRAISERS." WHERE it_id = '$it[it_id]' ORDER BY no ASC LIMIT 1");
				$checker = ($checker[no] > 0) ? $checker[no] : '0';

				if ($isVol) {
					if ($_SESSION[user_no]) {

			?>
			<a href='/campaign/<?=$it[it_id]?>/volunteer'><img src='/images/campaign/btn_volunteer.png' style='width: 100%; margin: 20px 0px 10px' /></a>
			<?
					} else {
			?>
			<img src='/images/campaign/btn_volunteer.png' class='btn_login' style='width: 100%; margin: 20px 0px 10px' url='/campaign/<?=$it_id?>/volunteer' />
			<?
					}
				} else {
               switch($it_id) {
                  case $SPECIAL[lol]:
                     $btn_img_url = '/images/campaign/lol.png';
                     $added = !$_SESSION[user_no] ? "<span style='color:red;font:10pt NanumGothic;'>참가신청을 위해서는 페이스북/트위터 로그인이나 위젠계정으로 로그인이 필요합니다.</span>" : false;
                     break;
                  case $SPECIAL[cw1]:
                  case $SPECIAL[cw2]:
                     $btn_img_url = '/images/campaign/btn_buy.png';
                     break;
                  default:
                     $btn_img_url = '/images/campaign/btn_donate.png';
                     $coin_html = "<img src='/images/campaign/btn_coin.png' style='width:100%;margin-bottom:10px;cursor:pointer;' class='donate_coin' itemid='$it_id' itemname='$it[it_name]' isEvent='$checker'>"; 
                     if($isOpenCampaign) {
                        $btn_img_url = '/images/campaign/open_donate_btn.png';
                        $coin_html = "<img src='/images/campaign/open_ticket_btn.png' style='width:100%;margin-bottom:10px;cursor:pointer;' class='open_share_btn'>";
                     }
               }
					if ($_SESSION[user_no]) {
			?>
			<a href='<?=$ipadURL?>/campaign/<?=$it[it_id]?>/donate'><img src='<?=$btn_img_url?>' style='width: 100%; margin: 20px 0px 10px' /></a>
			<?
					} else {
			?>
         <img src='<?=$btn_img_url?>' class='btn_login' style='width: 100%; margin: 20px 0px 10px' url='<?=$ipadURL?>/campaign/<?=$it_id?>/donate' />
			<?
					}
               echo $added;
               echo $coin_html;
				}
			}
			?>
		</div>

		<?
		if ($event_info[no]) {
			$starcmt = true;
		?>
		<div class='campaignDetail' style='border: none'><div style='padding: 10px'>
			<h3>스타/멘토 이벤트</h3>
			<ul>
				<?
					$event_info[fr_desc] = nl2br($event_info[fr_desc]);
					print "<li>$event_info[fr_desc]</li>";
				?>
			</ul>
		</div></div>
		<? } ?>

		<? if ($it_id != '1370759233' && !$type_cw) : ?>
		<div class='campaignDetail'><div style='padding: 10px'>
			<h3><? print $isVol ? '봉사시간 확인증 발급가능여부' : '리워드'; ?></h3>
			<ul>
			<? if($isVol) { ?>
				<? if($it[it_issueLoc]) { ?>
				<li>
					<? print '자원봉사시간은 ' ;?>
					<? print $it[it_issueLoc];?>
					<? print '을 통해 인증서를 발급받으실 수 있습니다.' ;?>
				</li>
				<? } else { ?>
				<li>
					<? print '칸이 비어있으면 본 캠페인은 확인증을 발급받을 수 없습니다.' ;?>
				</li>
				<? } ?>
			<? } else if ($it[it_reward]) { ?>
				<li><? print nl2br($it[it_reward]); ?></li>
			<? } else if ($it[reward_first_num] == 0) { ?>
				<li><? print '제공하는 리워드가 없습니다.'; ?></li>
			<? } ?>

         <? if($it[reward_first_num] > 0) {
            $res = sql_query("SELECT *,(SELECT COUNT(id) FROM ".DB_REWARDS_COUNT." b WHERE b.reward_id = a.id) AS now FROM ".DB_REWARDS." a WHERE a.it_id = '$it[it_id]'");
            while($reward_row = sql_fetch_array($res)) { ?>
            <li style='line-height:20px;margin-top:16px;padding:12px 16px;background-color:#f3f4e4;'>
               <span style='font:10pt NanumGothicBold;'><?=number_format($reward_row[reward_amount])?>원 이상 선착순 <?=$reward_row[reward_max]?>명</span><br>
               <span style='font:10pt NanumGothicBold;margin-left:14px;'><?=$reward_row[reward_name]?> 
               <span style='color:red;'>(현재 <? print $reward_row[reward_max]-$reward_row[now]?>명 남음)</span></span><br>
            </li>
            <? } 
            } ?>
			</ul>
		</div></div>
		<? endif; ?>

      <? if(!$type_cw && $it[it_id] != $SPECIAL[kara_fan]) { ?>
      <div class='campaignDetail' style='border: 1px solid #ccc;'>
         <div style='padding: 10px;line-height:19px;'>
            <h3>이 캠페인의  <?=  $it[it_auction] != -1 ?'최고 기부자':'최고액 입찰자' ?></h3>
            <?
	            if($it[it_auction] != -1){
	            	print("*** ".number_format($topDonor[0][0])."원 ***<br/>");
	        	} else {
	        		$query = "SELECT b.bid_amount, b.bid_name FROM wegen_auction as a LEFT JOIN wegen_auction_bid as b ON a.auc_id=b.auc_id WHERE a.it_id=$it[it_id] ORDER BY b.bid_amount DESC LIMIT 1";
	        		$res = sql_fetch($query);
	        		print("*** ".number_format($res[bid_amount])."원 ***<br/>");
	        	}
            ?>
            <ul id='donators' style='margin-top:5px;'>
            <?
            	if($it[it_auction] != -1){
				 	for($i =0; $i < count($topDonor);$i++) {
	               		$name = $topDonor[$i][2] == 0 ? "익명" : $topDonor[$i][3];
	               		$p = $_SESSION[user_no] == $topDonor[$i][1] ? "  <span id='topPublic' data-mbno='".$_SESSION[user_no]."' data-public='".$topDonor[$i][2]."' style='cursor:pointer;font-weight:bold;'>$name</span> 님" : $name." 님";
	               		$n = $i > 0 ? '<br/>' : '';
	               		echo $n.$p;
	            	} 
            	}else {
            		echo $res[bid_name]."님";
            	}
            ?>
            </ul>
         </div>
      </div>
      <? } ?>

		<div class='campaignDetail' style='border: none'><div style='padding: 10px'>
			<h3><? print $isVol ? '봉사활동 참여를 신청한 회원' : ($it[it_id] == '1371778995' ? '이 게임대회에 참가신청한 팀' : ($it[it_auction] != -1 ? '이 캠페인에 후원한 회원':'이 캠페인에 입찰한 회원')); ?></h3>
			<ul id='donators'>
			<?
			if($it[it_auction] != -1) {

				// $lol_condition = $it[it_id] == '1371778995' ? "AND od_amount='20000' " : false;
				// $table = $isVol ? 'wegen_volunteer' : DB_ORDERS;
				// $sql = "SELECT mb_no FROM ".$table." WHERE it_id = '$it[it_id]' $lol_condition AND od_amount - pay_remain > 0 GROUP BY it_id, mb_no ORDER BY rand()";
				
				$lol_condition = $it[it_id] == '1371778995' ? "AND od_amount='20000' " : false;
				$table = $isVol ? 'wegen_volunteer' : DB_ORDERS;
				$sql = "SELECT mb_no
						FROM ".$table."
	               WHERE it_id = '$it[it_id]' $lol_condition AND od_amount - pay_remain > 0
	               GROUP BY it_id, mb_no
						ORDER BY rand()";


				$result = sql_query($sql);
				$total = mysql_num_rows($result);
				$limit = 0;
				for($i= 0; $row = sql_fetch_array($result); $i++) {
					if ($limit == 15) { 
	               if($it[type] != 300) break; 
	            }
					print '<li>' and drawPortrait($row[mb_no], true) and print '</li>';
					$limit++;
				}
			}
			else {
				$query = "SELECT DISTINCT b.mb_no FROM wegen_auction_bid as b LEFT JOIN wegen_auction as a ON b.auc_id=a.auc_id WHERE a.it_id='$it[it_id]'";
				$res = sql_query($query);
				$total = mysql_num_rows($res);

				while($row = sql_fetch_array($res)){
					print '<li>' and drawPortrait($row[mb_no], true) and print '</li>';
				}
			}
			?>			
			</ul>
			<div style='clear: both'></div>
			<div style='text-align: right'><span style='color: #E30000; font-weight: bold'><?=$total?><?=$it[it_id] == '1371778995' ? '팀' : '명'?></span>이 참여하셨습니다</div>
		</div></div>

		<div class='campaignDetail'><div style='padding: 10px'>
			<h3>이 캠페인을 공유한 회원</h3>
			<ul id='sharers'>
			<?
			$sql = "SELECT mb_no
					FROM ".DB_BADGES."
					WHERE it_id = '$it[it_id]' AND category = 3
					GROUP BY mb_no
					ORDER BY rand()";
			$result = sql_query($sql);
			$total = mysql_num_rows($result);

			$limit = 0;
			for($i= 0; $row = sql_fetch_array($result); $i++) {
				if ($limit == 15) { break; }
				print '<li>' and drawPortrait($row[mb_no], true) and print '</li>';
				$limit++;
			}
         if($isOpenCampaign) {
            $sql = "SELECT mb_no FROM ".DB_ORDERS." WHERE it_id = '$it[it_id]' AND od_method='무료응모'";
            $result = sql_query($sql);
            $total += mysql_num_rows($result);
            for($i= 0; $row = sql_fetch_array($result); $i++) {
               if ($limit == 15) { break; }
               print '<li>' and drawPortrait($row[mb_no], true) and print '</li>';
               $limit++;
            }
         }
			?>			
			</ul>
			<div style='clear: both'></div>
			<div style='text-align: right'><span style='color: #E30000; font-weight: bold'><?=$total?>명</span>이 참여하셨습니다</div>
		</div></div>

	</div>
	<!-- /right -->
	<div style='clear: both'></div>



   </div>
   </div>
   &nbsp;
   </div>

<script type='text/javascript'>
   $(document).ready(function() {	
      if($('#topPublic').length != 0) {
         $('#topPublic').click(function() {
            if($(this).data("qtip")) $(this).qtip("destroy");
            else {
               var checked = $(this).data('public') == '1' ? 'checked' : '';
               $(this).qtip({
                  content : {
                     text : "<div style='padding:5px;'>"+
                     "<input type='checkbox' id='tpChk' name='isTopPublic' "+checked+">"+
                     "<label for='tpChk'>이름공개<label> <button type='button' id='tpBtn' style='margin:0;margin-left:6px;width:40px;'>반영</button></div>",
                     title : {
                        text : "본인공개여부",
                        button : false
                     }
                  },
                  position: {
                     corner: {
                        tooltip:'topMiddle',
                        target:'bottomMiddle'
                     }
                  },
                  show: {
                     when: false,
                     ready:true
                  },
                  hide:false,
                  style: {
                     tip: true
                  },
                  events: {
                     render: function() {
                        $('#tpBtn').click(function(){
                           console.log('clickbtn');
                           var chkVal = $('#tpChk').is(":checked") ? "1" : "0";
                           $.ajax({
                              type: "POST",
                              url : "/campaign/ajax.topPublic.php",
                              data : {
                                 topPublic : chkVal,
                                 mbno : "<?=$_SESSION[user_no]?>",
                              },
                              cache : false,
                              success : function(e) {
                                 console.log(e);
                                 if(e == 'success') {
                                    if(chkVal == "0") {
                                       $("#topPublic").html("익명");
                                       $("#topPublic").data("public","0");
                                    } else {
                                       $("#topPublic").html("<?=$_SESSION[username]?>");
                                       $("#topPublic").data("public","1");
                                    }
                                 } else {
                                    qAlert('다시 시도해주세요');
                                 }
                              }
                           });
                        });
                     }
                  }
               }); 
            }
         }); 
      }
      $('#content menu li').eq(<?=$eq?>).addClass('on');
      $('#share_comment').html('<?=$commentTotal?>');
      $('.donate_coin').click(function() {
<? if ($_SESSION[user_no]) { 
      $givex = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' ");
      if ($givex[cnt] > 0 || getCoin($_SESSION[user_no]) > 0) {
?>
         $('#movieFrame').hide();
         createLayer($('#coinDonateLayer'));

         $('input[name=itemid]').val($(this).attr('itemid'));
         $('input[name=good_name]').val($(this).attr('itemname'));
         var eventnum = $(this).attr('isEvent');
         if (eventnum > 0) {
            $('input[name=od_isEvent]').val(eventnum);
            $('#eventbox').show();
         }
<?
      } else { ?>
         alert('정기후원을 신청하지 않으셨습니다.\n확인 버튼을 누르면 신청 페이지로 이동합니다.');
         document.location.href = '/givex/';
<?
      }
   } else { ?>
		createLayer($('#loginLayer'));
<? } ?>
	});
<? if (!$starcmt) { ?>
	$('.starcmt').hide();
<? } ?>
});

twttr.ready(function (twttr) {
	twttr.events.bind('tweet', function(event) {
		procShare('<?=$it_id?>', 'twitter', '');
	});
});

function postToFeed() {
	var list = <?=$it_id?> == '1372929550' ? 'list2.jpg' : 'list.jpg';
	var obj = {
		method: 'feed',
		link: 'http://wegen.kr/campaign/<?=$it_id?>',
		picture: 'http://wegen.kr/data/campaign/<?=$it_id?>/'+list,
		name: '<?=$it[it_name]?>',
		caption: '스타와 함께 하는 즐거운 기부, 위젠',
		description: '<?=addslashes($it[it_shortdesc])?>'
	};

	function callback(response) {
		if (response) {
			procShare('<?=$it_id?>', 'facebook', response['post_id']);
		}
	}
	FB.ui(obj, callback);
}

function openCampaignShare(pd) {
   var list = 'open_campaign_share.jpg';
   var obj = {
      method: 'feed',
      link: '<?=$open_share_link?>',
      picture: '<?=$open_share_img?>',
      name: '<?=$open_share_name?>',
      //name: $('input[name=open_name]').val()+'님이 <?=$it[it_openCampaignEvent]?>에 응모했습니다.',
      caption: ' ',
      description: '<?=$open_share_desc?>'
   };

   pd.share = {};
   pd.share.obj = obj;
   FB.getLoginStatus(function(response) {
      if(response.status === 'connected') {
         if(response.authResponse) {
            pd.fb_account = '1';
            chkOpenCampaign(pd);
         } else {
            FB.login(function(response){
               if(response.authResponse) {
                  pd.fb_account = '1';
                  chkOpenCampaign(pd);
               } else {
                  alert('페이스북 로그인을 취소하셨습니다.');
                  share_btn_click = false;
               }
            });
         }
      } else {
         FB.login(function(response){
            if(response.authResponse) {
               pd.fb_account = '1';
               chkOpenCampaign(pd);
            } else {
               alert('페이스북 로그인을 취소하셨습니다.');
               share_btn_click = false;
            }
         });
      }
   });
}
function postToTweet() {
	window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent('http://wegen.kr/campaign/<?=$it_id?>') + '&text=' + encodeURIComponent('<?=$it[it_name]?>'),"twitterPop", 'width=600 height=350');
}
</script>

<?
include '../module/_tail.php';
?>
