<?
$_required = true;
include '../config.php';
include '../module/class.upload.php';
require 'pp_ax_hub_lib.php';

sec_session_start();

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
	exit;
}
$_SESSION[token] = '';

if (!$_SESSION[user_no]) {
	alert('로그인이 필요한 페이지입니다.');
	exit;
}
if( $_POST['it_id'] == $SPECIAL[cw1] || $_POST['it_id'] == $SPECIAL[cw2]) {
   $sql = "SELECT IFNULL(SUM(od_amount), 0) as funded,
      (SELECT IFNULL(SUM(round(od_amount/15000, 0)),0) FROM ".DB_ORDERS." WHERE it_id='$row[it_id]' 
         AND substr(reward,1,1) = '1'
         AND IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1 ) as goods_num_1,
      (SELECT IFNULL(SUM(round(od_amount/15000,0)),0) FROM ".DB_ORDERS." WHERE it_id='$row[it_id]'
         AND substr(reward,1,1) = '2'
         AND IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1 ) as goods_num_2
      FROM ".DB_ORDERS." WHERE it_id='$row[it_id]' and IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1";
   $data = sql_fetch($sql);
   $mny = substr($_POST['reward_req'],0,1) == "1" ? 50 - (int)$data[goods_num_1] : 50 - (int)$data[goods_num_2];
   if((((int)$_POST['good_mny'])-2500)%15000 != 0 ) {
      alert('잘못된 금액입니다.');
      exit;
   } else if((int)$_POST['good_mny'] / 15000 > $mny) {
      alert('해당 상품의 재고가 부족합니다.');
      exit;
   }
}
$connect_type = $_POST["connect_type"] == "MOBILE" ? "모바일" : "PC";

$it_id		= $_POST["it_id"];
if (!preg_match("/^[0-9]{10}$/", $it_id)) exit;
if($it_id == $SPECIAL[kara_fan]) {
   $cam_chk = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
   $ticket_chk = sql_fetch("SELECT COUNT(od_id) AS donate_num 
               FROM ".DB_ORDERS." a 
               WHERE a.it_id = '$it_id'");
   $diff = round((strtotime($cam_chk[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
   if($diff < 0) {
      alert('참여 기간이 지났습니다.');
      exit;
   }
   if($ticket_chk[donate_num] >= ($cam_chk[it_target]/5000)-32) {
      alert('남아있는 티켓이 없습니다 T^T');
      exit;
   }
   $donate_chk = sql_fetch("SELECT od_id FROM ".DB_ORDERS." WHERE it_id = '$it_id' AND mb_no = $_SESSION[user_no]");
   if($donate_chk[od_id]) {
      alert('이미 티켓을 구매하셨습니다. (1인 1매)');
      exit;
   }
   if($_POST[good_mny] != '5000') {
      alert('오류가 발생했습니다.');
      exit;
   }
}

$od_isEvent	= $_POST["od_isEvent"];

$req_tx		= $_POST["req_tx"]; // 요청 종류
$tran_cd	= $_POST["tran_cd"]; // 처리 종류

$cust_ip	= getenv("REMOTE_ADDR"); // 요청 IP
$ordr_idxx	= $_POST["ordr_idxx"]; // 쇼핑몰 주문번호
if (!preg_match("/^[0-9]{15}$/", $ordr_idxx)) exit;

$good_name	= $_POST["good_name"]; // 상품명
$good_mny	= $_POST["good_mny"]; // 결제 총금액
if (!preg_match("/^[0-9]+$/", $good_mny)) exit;

$res_cd		= "";                         // 응답코드
$res_msg	= "";                         // 응답메시지
$res_en_msg	= "";                         // 응답 영문 메세지
$tno		= $_POST["tno"]; // KCP 거래 고유 번호

$buyr_name	= $_POST["buyr_name"]; // 주문자명
$buyr_tel1	= trim($_POST["buyr_tel1"]); // 주문자 전화번호
$buyr_tel2	= $_POST["buyr_tel2"]; // 주문자 핸드폰 번호
$buyr_mail	= $_POST["buyr_mail"]; // 주문자 E-mail 주소

$isReceipt  = $_POST["isReceipt"];
$realname   = $_POST["realname"];
$receiptPno = ($isReceipt != 0) ? ($isReceipt == 1) ? $_POST["serial1"].'-'.$_POST["serial2"] : $_POST["permit1"].'-'.$_POST["permit2"].'-'.$_POST["permit3"] : false;
$serialno = $_POST["serial1"].$_POST["serial2"];
$permitno = $_POST["permit1"].$_POST["permit2"].$_POST["permit3"];

$od_isEvent	= $_POST["od_isEvent"];
$reward_req = $_POST["reward_req"];

$mod_type	= $_POST["mod_type"]; // 변경TYPE VALUE 승인취소시 필요
$mod_desc	= $_POST["mod_desc"]; // 변경사유

$use_pay_method	= $_POST["use_pay_method"]; // 결제 방법
$bSucc		= "";                         // 업체 DB 처리 성공 여부

$app_time	= "";                         // 승인시간 (모든 결제 수단 공통)
$amount		= "";                         // KCP 실제 거래 금액
$total_amount	= 0;                          // 복합결제시 총 거래금액

$card_cd	= "";                         // 신용카드 코드
$card_name	= "";                         // 신용카드 명
$app_no		= "";                         // 신용카드 승인번호
$noinf		= "";                         // 신용카드 무이자 여부
$quota		= "";                         // 신용카드 할부개월
$partcanc_yn	= "";						  // 부분취소 가능유무
$card_bin_type_01 = "";                       // 카드구분1
$card_bin_type_02 = "";                       // 카드구분2

$bank_name      = "";                         // 은행명
$bank_code      = "";						  // 은행코드
$bk_mny         = "";

$pnt_issue      = "";                         // 결제 포인트사 코드
$pt_idno        = "";                         // 결제 및 인증 아이디
$pnt_amount     = "";                         // 적립금액 or 사용금액
$pnt_app_time   = "";                         // 승인시간
$pnt_app_no     = "";                         // 승인번호
$add_pnt        = "";                         // 발생 포인트
$use_pnt        = "";                         // 사용가능 포인트
$rsv_pnt        = "";                         // 총 누적 포인트

$commid         = "";                         // 통신사 코드
$mobile_no      = "";                         // 휴대폰 번호

$cash_yn        = $_POST["cash_yn"]; // 현금영수증 등록 여부
$cash_authno    = "";                         // 현금 영수증 승인 번호
$cash_tr_code   = $_POST["cash_tr_code"]; // 현금 영수증 발행 구분
$cash_id_info   = $_POST["cash_id_info"]; // 현금 영수증 등록 번호

// 인스턴스 생성 및 초기화
$c_PayPlus = new C_PP_CLI;
$c_PayPlus->mf_clear();

// 승인 요청
if ( $req_tx == "pay" ) {
// 결제 유효성 검사. 원금
//	$c_PayPlus->mf_set_ordr_data( "ordr_mony",  "1004" );
	$c_PayPlus->mf_set_encx_data( $_POST["enc_data" ], $_POST["enc_info" ] );
}

// 취소/매입 요청
else if ( $req_tx == "mod" ) {
	$tran_cd = "00200000";
	$c_PayPlus->mf_set_modx_data( "tno",      $tno      ); // KCP 원거래 거래번호
	$c_PayPlus->mf_set_modx_data( "mod_type", $mod_type ); // 원거래 변경 요청 종류
	$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip  ); // 변경 요청자 IP
	$c_PayPlus->mf_set_modx_data( "mod_desc", $mod_desc ); // 변경 사유
}

/* =   04. 실행                                                                 = */
if ( $tran_cd != "" ) {
	$c_PayPlus->mf_do_tx( $trace_no, $g_conf_home_dir, $g_conf_site_cd, $g_conf_site_key, $tran_cd, "",
	$g_conf_gw_url, $g_conf_gw_port, "payplus_cli_slib", $ordr_idxx,
	$cust_ip, "3" , 0, 0, $g_conf_key_dir, $g_conf_log_dir); // 응답 전문 처리

	$res_cd  = $c_PayPlus->m_res_cd;  // 결과 코드
	$res_msg = $c_PayPlus->m_res_msg; // 결과 메시지
	/* $res_en_msg = $c_PayPlus->mf_get_res_data( "res_en_msg" );  // 결과 영문 메세지 */ 
}
else {
	$c_PayPlus->m_res_cd  = "9562";
	$c_PayPlus->m_res_msg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
}

/* =   05. 승인 결과 값 추출                                                    = */
if ( $req_tx == "pay" ) {
	if( $res_cd == "0000" ) {
		$tno       = $c_PayPlus->mf_get_res_data( "tno"       ); // KCP 거래 고유 번호
		$amount    = $c_PayPlus->mf_get_res_data( "amount"    ); // KCP 실제 거래 금액
		$pnt_issue = $c_PayPlus->mf_get_res_data( "pnt_issue" ); // 결제 포인트사 코드

		/* =   05-1. 신용카드 승인 결과 처리                                            = */
		if ( $use_pay_method == "100000000000" ) {
			$card_cd   = $c_PayPlus->mf_get_res_data( "card_cd"   ); // 카드사 코드
			$card_name = $c_PayPlus->mf_get_res_data( "card_name" ); // 카드 종류
			$app_time  = $c_PayPlus->mf_get_res_data( "app_time"  ); // 승인 시간
			$app_no    = $c_PayPlus->mf_get_res_data( "app_no"    ); // 승인 번호
			$noinf     = $c_PayPlus->mf_get_res_data( "noinf"     ); // 무이자 여부 ( 'Y' : 무이자 )
			$quota     = $c_PayPlus->mf_get_res_data( "quota"     ); // 할부 개월 수
			$partcanc_yn = $c_PayPlus->mf_get_res_data( "partcanc_yn" ); // 부분취소 가능유무
			$card_bin_type_01 = $c_PayPlus->mf_get_res_data( "card_bin_type_01" ); // 카드구분1
			$card_bin_type_02 = $c_PayPlus->mf_get_res_data( "card_bin_type_02" ); // 카드구분2

         $card_name = iconv("euc-kr", "utf-8", $card_name);

			if ( $pnt_issue == "SCSK" || $pnt_issue == "SCWB" ) {
				$pt_idno      = $c_PayPlus->mf_get_res_data ( "pt_idno"      ); // 결제 및 인증 아이디    
				$pnt_amount   = $c_PayPlus->mf_get_res_data ( "pnt_amount"   ); // 적립금액 or 사용금액
				$pnt_app_time = $c_PayPlus->mf_get_res_data ( "pnt_app_time" ); // 승인시간
				$pnt_app_no   = $c_PayPlus->mf_get_res_data ( "pnt_app_no"   ); // 승인번호
				$add_pnt      = $c_PayPlus->mf_get_res_data ( "add_pnt"      ); // 발생 포인트
				$use_pnt      = $c_PayPlus->mf_get_res_data ( "use_pnt"      ); // 사용가능 포인트
				$rsv_pnt      = $c_PayPlus->mf_get_res_data ( "rsv_pnt"      ); // 총 누적 포인트
				$total_amount = $amount + $pnt_amount;                          // 복합결제시 총 거래금액
			}
		}

		/* =   05-2. 계좌이체 승인 결과 처리                                            = */
		if ( $use_pay_method == "010000000000" ) {
			$app_time  = $c_PayPlus->mf_get_res_data( "app_time"   );  // 승인 시간
			$bank_name = $c_PayPlus->mf_get_res_data( "bank_name"  );  // 은행명
			$bank_code = $c_PayPlus->mf_get_res_data( "bank_code"  );  // 은행코드
			$bk_mny = $c_PayPlus->mf_get_res_data( "bk_mny" ); // 계좌이체결제금액
         $bank_name = iconv("euc-kr", "utf-8", $bank_name);
		}
       /* =   05-3. 가상계좌 승인 결과 처리                                            = */
      if ( $use_pay_method == "001000000000" )
      {
          $app_time  = $c_PayPlus->mf_get_res_data( "app_time"  ); // 승인 시간
          $bankname  = $c_PayPlus->mf_get_res_data( "bankname"  ); // 입금할 은행 이름
          $depositor = $c_PayPlus->mf_get_res_data( "depositor" ); // 입금할 계좌 예금주
          $account   = $c_PayPlus->mf_get_res_data( "account"   ); // 입금할 계좌 번호
          $va_date   = $c_PayPlus->mf_get_res_data( "va_date"   ); // 가상계좌 입금마감시간
          $bankname = iconv("euc-kr", "utf-8", $bankname);
          $depositor = iconv("euc-kr", "utf-8", $depositor);
      }

		/* =   05-5. 휴대폰 승인 결과 처리                                              = */
		if ( $use_pay_method == "000010000000" ) {
			$app_time  = $c_PayPlus->mf_get_res_data( "hp_app_time"  ); // 승인 시간
			$commid    = $c_PayPlus->mf_get_res_data( "commid"	     ); // 통신사 코드
			$mobile_no = $c_PayPlus->mf_get_res_data( "mobile_no"	 ); // 휴대폰 번호
		}

		/* =   05-7. 현금영수증 결과 처리                                               = */
		$cash_authno  = $c_PayPlus->mf_get_res_data( "cash_authno"  ); // 현금 영수증 승인 번호

	}
}

/* =   06. 승인 및 실패 결과 DB처리                                             = */
/* =       결과를 업체 자체적으로 DB처리 작업하시는 부분입니다.                 = */

if ( $req_tx == "pay" ) {
	if( $res_cd == "0000" ) {
		if ( $use_pay_method == "100000000000" ) {
			// 06-1-1. 신용카드
			$result = mysql_query("INSERT INTO ".DB_ORDERS."
						SET
						od_id		= '$ordr_idxx',
						it_id		= '$it_id',
						mb_no		= '$_SESSION[user_no]',
						od_name		= '$buyr_name',
						od_hp		= '$buyr_tel1',
						od_zip1		= '$_POST[od_zip1]',
						od_zip2		= '$_POST[od_zip2]',
						od_addr1	= '$_POST[od_addr1]',
						od_addr2	= '$_POST[od_addr2]',
						od_isEvent	= '$od_isEvent',
						od_amount	= '$good_mny',
						od_time		= '$app_time',
						od_ip		= '$cust_ip',
						od_method	= '신용카드',
						od_escrow1	= '$card_cd / $card_name',
						od_escrow2	= '$app_no',
						od_tno		= '$tno',
						isReceipt   = '$isReceipt',
						receiptName = '$realname',
						receiptPno  = '$receiptPno',
						reward		= '$reward_req',
						connect		= '$connect_type'
			");

			if ( $pnt_issue == "SCSK" || $pnt_issue == "SCWB" ) {
				// 06-1-1-1. 복합결제(신용카드 + 포인트)
			}
		}
		if ( $use_pay_method == "010000000000" ) {
			// 06-1-2. 계좌이체
			$result = mysql_query("INSERT INTO ".DB_ORDERS." SET
						od_id		= '$ordr_idxx',
						it_id		= '$it_id',
						mb_no		= '$_SESSION[user_no]',
						od_name		= '$buyr_name',
						od_hp		= '$buyr_tel1',
						od_zip1		= '$_POST[od_zip1]',
						od_zip2		= '$_POST[od_zip2]',
						od_addr1	= '$_POST[od_addr1]',
						od_addr2	= '$_POST[od_addr2]',
						od_isEvent	= '$od_isEvent',
						od_amount	= '$good_mny',
						od_time		= '$app_time',
						od_ip		= '$cust_ip',
						od_method	= '계좌이체',
						od_escrow1	= '$bank_code / $bank_name',
						od_tno		= '$tno',
						isReceipt   = '$isReceipt',
						receiptName = '$realname',
						receiptPno  = '$receiptPno',
						reward		= '$reward_req',
						connect		= '$connect_type'
			");
		}
      if ( $use_pay_method == "001000000000" ) {
         // 가상계좌
			$result = mysql_query("INSERT INTO ".DB_ORDERS." SET
						od_id		= '$ordr_idxx',
						it_id		= '$it_id',
						mb_no		= '$_SESSION[user_no]',
						od_name		= '$buyr_name',
						od_hp		= '$buyr_tel1',
						od_zip1		= '$_POST[od_zip1]',
						od_zip2		= '$_POST[od_zip2]',
						od_addr1	= '$_POST[od_addr1]',
						od_addr2	= '$_POST[od_addr2]',
						od_isEvent	= '$od_isEvent',
						od_amount	= '$good_mny',
						od_time		= '$app_time',
						od_ip		= '$cust_ip',
						od_method	= '가상계좌',
						od_escrow1	= '$bankname/$depositor/$account/$va_date',
						od_tno		= '$tno',
						isReceipt   = '$isReceipt',
						receiptName = '$realname',
						receiptPno  = '$receiptPno',
						reward		= '$reward_req',
                  pay_remain  = '$good_mny',
						connect		= '$connect_type'
			");
      }
		if ( $use_pay_method == "000010000000" ) {
			// 06-1-5. 휴대폰
			$result = mysql_query("INSERT INTO ".DB_ORDERS." SET
						od_id		= '$ordr_idxx',
						it_id		= '$it_id',
						mb_no		= '$_SESSION[user_no]',
						od_name		= '$buyr_name',
						od_hp		= '$buyr_tel1',
						od_zip1		= '$_POST[od_zip1]',
						od_zip2		= '$_POST[od_zip2]',
						od_addr1	= '$_POST[od_addr1]',
						od_addr2	= '$_POST[od_addr2]',
						od_isEvent	= '$od_isEvent',
						od_amount	= '$good_mny',
						od_time		= '$app_time',
						od_ip		= '$cust_ip',
						od_method	= '핸드폰',
						od_escrow1	= '$commid',
						od_escrow2	= '$mobile_no',
						od_tno		= '$tno',
						isReceipt   = '$isReceipt',
						receiptName = '$realname',
						receiptPno  = '$receiptPno',
						reward		= '$reward_req',
						connect		= '$connect_type'
			");
		}
	}
	else if ( $res_cd != "0000" ) {
	/* =   06. 승인 실패 결과 DB처리                                             = */
	}
}

if($it_id == $SPECIAL[kara_fan]) {
   //좌석배치
   if($result && $use_pay_method != "001000000000") {
      $info = sql_fetch("SELECT COUNT(od_id) AS rank FROM ".DB_ORDERS." WHERE it_id = '$it_id' AND pay_remain = 0 AND od_time <= '$app_time'");
      $target_rank = $info[rank];
      include '../module/set_seat_var.php';
      $result = sql_query("UPDATE ".DB_ORDERS." SET var_1 = '$seat_info' WHERE od_id = '$ordr_idxx'");
   }
}
if ($_FILES['permitimage']) {
	$handle = new upload($_FILES['permitimage']);
	if ($handle->uploaded) {
		$handle->allowed				= array('image/*');
		$handle->image_convert			= 'jpg';
		$handle->file_new_name_body		= 'permit_'.$_SESSION[user_no];
//		$handle->image_resize			= true;
//		$handle->image_ratio_crop		= true;
//		$handle->image_x				= 100;
//		$handle->image_y				= 100;
		$handle->process('../data/userupload/');

		if ($handle->processed) {
			$handle->clean();
			mysql_query("UPDATE ".DB_MEMBERS." SET mb_permit_img = 'profile_$_SESSION[user_no].jpg' WHERE mb_no = '$_SESSION[user_no]' ");
		} else {
			echo 'error : ' . $handle->error;
		}
	}
}


// 회원정보 업데이트
$serialno = $serialno ? ", mb_serial   = '$serialno' " : false;
$permitno = $permitno ? ", mb_permit   = '$permitno' " : false;


mysql_query("UPDATE ".DB_MEMBERS."
			SET mb_email	= '$buyr_mail',
				mb_contact	= '$buyr_tel1',
				mb_zip1		= '$_POST[od_zip1]',
				mb_zip2		= '$_POST[od_zip2]',
				mb_addr1	= '$_POST[od_addr1]',
				mb_addr2	= '$_POST[od_addr2]'
				$serialno
				$permitno
			WHERE mb_no = '$_SESSION[user_no]'
			");

// 뱃지 발급 루틴
$procBadges = array();

if ($result) {
	// 최초 기부(위젠 레벨) 체크
	$c = sql_fetch("
			SELECT COUNT(od_id) AS total
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			AND mb_no = '$_SESSION[user_no]'
			");
	if ($c[total] == 1) {
//		array_push($procBadges, 'level');
	}

	// 해당캠페인 최초 기부 체크
	$c = sql_fetch("
			SELECT COUNT(od_id) AS total
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			AND it_id = '$it_id'
			");
	if ($c[total] == 1) {
		array_push($procBadges, '1');
	}

	// 최후 기부 & 오버킬 기부 체크
	$c = sql_fetch("
			SELECT SUM(od_amount) AS re_sum, i.it_target
			FROM ".DB_ORDERS." o
			LEFT JOIN ".DB_CAMPAIGNS." i ON (o.it_id = i.it_id)
			WHERE o.it_id = '$it_id'
			GROUP BY o.it_id
			");

	$diff = $c[re_sum] - $c[it_target];

	// 모금액이 목표액 초과했을 경우
	if ($diff >= 0) {
		// 최후 기부 체크 (초과액수$diff가 기부액수보다 작을 경우)
		// 작거나 '같은' 경우를 체크할 경우 모금액과 목표액이 딱 맞춰진 상황에서 다음 1인에게도 뱃지가 중복적용됨
		if ($good_mny > $diff) {
			array_push($procBadges, '2');
		}
		// 오버킬 기부
		else {
			array_push($procBadges, '6');
		}
	}

	// 카테고리 뱃지 체크
	$c = sql_fetch("
			SELECT category
			FROM ".DB_CAMPAIGNS."
			WHERE it_id = '$it_id'
			");
	if (strpos($c[category], ',') === false) $category = $c[category];
	else $category = explode(', ', $c[category]) and $category = $category[0];

	if ($category == '역사/문화') { array_push($procBadges, '101'); }
	elseif ($category == '환경/동물보호') { array_push($procBadges, '102'); }
	elseif ($category == '아동/청소년') { array_push($procBadges, '103'); }
	elseif ($category == '노인') { array_push($procBadges, '104'); }
	elseif ($category == '장애인') { array_push($procBadges, '105'); }
	elseif ($category == '저소득가정') { array_push($procBadges, '106'); }
	elseif ($category == '다문화가정') { array_push($procBadges, '107'); }
	elseif ($category == '자활형 캠페인') { array_push($procBadges, '108'); }

	for ($i = 0; $i < count($procBadges); $i++) {
		$check = sql_fetch("
				SELECT mb_no
				FROM ".DB_BADGES."
				WHERE mb_no = '$_SESSION[user_no]'
				AND category = '$procBadges[$i]'
				AND it_id = '$it_id'
				");
		if (!$check[mb_no]) {
			$badge = "INSERT ".DB_BADGES."
						SET mb_no			= '$_SESSION[user_no]',
							category		= '$procBadges[$i]',
							od_id			= '$ordr_idxx',
							it_id			= '$it_id',
							param			= '$good_mny'
					";
			sql_query($badge);
		}
		unset($check);
	}
}
// 뱃지루틴 끗

// 리워드업데이트
if($result) {
   include '../module/reward.count.update.php'; 
}

$result = $result ? "true" : "false";
$bSucc = $result; // DB 작업 실패 또는 금액 불일치의 경우 "false" 로 세팅
/* =   07-1. DB 작업 실패일 경우 자동 승인 취소                                 = */
if ( $req_tx == "pay" ) {
	if( $res_cd == "0000" ) {	
		if ( $bSucc === "false") {
			$c_PayPlus->mf_clear();

			$tran_cd = "00200000";

			$c_PayPlus->mf_set_modx_data( "tno",      $tno                         );  // KCP 원거래 거래번호
			$c_PayPlus->mf_set_modx_data( "mod_type", "STSC"                       );  // 원거래 변경 요청 종류
			$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip                     );  // 변경 요청자 IP
			$c_PayPlus->mf_set_modx_data( "mod_desc", "결과 처리 오류 - 자동 취소" );  // 변경 사유

			$c_PayPlus->mf_do_tx( $tno,  $g_conf_home_dir, $g_conf_site_cd, $g_conf_site_key, $tran_cd, "", $g_conf_gw_url,  $g_conf_gw_port, "payplus_cli_slib", $ordr_idxx, $cust_ip, "3", 0, 0, $g_conf_key_dir, $g_conf_log_dir);

			$res_cd  = $c_PayPlus->m_res_cd;
			$res_msg = $c_PayPlus->m_res_msg;
		}
	}
}

$res_msg = iconv("euc-kr", "utf-8", $res_msg);
$card_name = iconv("euc-kr", "utf-8", $card_name);

$sql = "SELECT SUM(od_amount)-SUM(pay_remain) as amount FROM ".DB_ORDERS." WHERE it_id = '$it_id' and od_method !='직접입력' group by mb_no order by amount desc";
$top = sql_fetch($sql);

$sql = "SELECT SUM(od_amount)-SUM(pay_remain) as amount FROM ".DB_ORDERS." WHERE it_id = '$it_id' and mb_no = $_SESSION[user_no] group by mb_no";
$my_donated = sql_fetch($sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>위제너레이션</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='/css/style.css' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
</head>

<body onload="document.pay_info.submit()">

<script type='text/javascript'>
function noRefresh() {
	if ((event.keyCode == 78) && (event.ctrlKey == true)) {
		event.keyCode = 0;
		return false;
	}
	if(event.keyCode == 116) {
		event.keyCode = 0;
		return false;
	}
}
document.onkeydown = noRefresh;
</script>

<div class='layer'>
	<div id='pluginLayer' class='centering' style='display: block; margin-left: -200px; margin-top: -150px'>
	<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
	<p>결제 진행 중입니다.</p>
	창을 닫거나 새로고침하지 마십시오.
	</div>
</div>

<form name='pay_info' method='post' action='./result.php'>
<input type='hidden' name="site_cd"           value="<?=$g_conf_site_cd ?>">
<input type='hidden' name="req_tx"            value="<?=$req_tx         ?>">
<input type='hidden' name="use_pay_method"    value="<?=$use_pay_method ?>">
<input type='hidden' name="bSucc"             value="<?=$bSucc          ?>">

<input type='hidden' name="res_cd"            value="<?=$res_cd         ?>">
<input type='hidden' name="res_msg"           value="<?=$res_msg        ?>">
<input type='hidden' name="res_en_msg"        value="<?=$res_en_msg     ?>">
<input type='hidden' name="ordr_idxx"         value="<?=$ordr_idxx      ?>">
<input type='hidden' name="tno"               value="<?=$tno            ?>">
<input type='hidden' name="good_mny"          value="<?=$good_mny       ?>">
<input type='hidden' name="good_name"         value="<?=$good_name      ?>">
<input type='hidden' name="it_id"		      value="<?=$it_id          ?>">
<input type='hidden' name="buyr_name"         value="<?=$buyr_name      ?>">
<input type='hidden' name="buyr_tel1"         value="<?=$buyr_tel1      ?>">
<input type='hidden' name="buyr_tel2"         value="<?=$buyr_tel2      ?>">
<input type='hidden' name="buyr_mail"         value="<?=$buyr_mail      ?>">

<input type='hidden' name="card_cd"           value="<?=$card_cd        ?>">
<input type='hidden' name="card_name"         value="<?=$card_name      ?>">
<input type='hidden' name="app_time"          value="<?=$app_time       ?>">
<input type='hidden' name="app_no"            value="<?=$app_no         ?>">
<input type='hidden' name="quota"             value="<?=$quota          ?>">
<input type='hidden' name="noinf"             value="<?=$noinf          ?>">
<input type='hidden' name="partcanc_yn"       value="<?=$partcanc_yn    ?>">
<input type='hidden' name="card_bin_type_01"  value="<?=$card_bin_type_01 ?>">
<input type='hidden' name="card_bin_type_02"  value="<?=$card_bin_type_02 ?>">

<input type='hidden' name="bank_name"         value="<?=$bank_name      ?>">
<input type='hidden' name="bank_code"         value="<?=$bank_code      ?>">

<input type='hidden' name="pnt_issue"         value="<?=$pnt_issue      ?>">
<input type='hidden' name="pnt_app_time"      value="<?=$pnt_app_time   ?>">
<input type='hidden' name="pnt_app_no"        value="<?=$pnt_app_no     ?>">
<input type='hidden' name="pnt_amount"        value="<?=$pnt_amount     ?>">
<input type='hidden' name="add_pnt"           value="<?=$add_pnt        ?>">
<input type='hidden' name="use_pnt"           value="<?=$use_pnt        ?>">
<input type='hidden' name="rsv_pnt"           value="<?=$rsv_pnt        ?>">

<input type='hidden' name='bankname'          value="<?=$bankname?>">
<input type='hidden' name='depositor'         value="<?=$depositor?>">
<input type='hidden' name='account'           value="<?=$account?>">
<input type='hidden' name='va_date'           value="<?=$va_date?>">

<input type='hidden' name="commid"            value="<?=$commid         ?>">
<input type='hidden' name="mobile_no"         value="<?=$mobile_no      ?>">

<input type='hidden' name="cash_yn"           value="<?=$cash_yn        ?>">
<input type='hidden' name="cash_authno"       value="<?=$cash_authno    ?>">
<input type='hidden' name="cash_tr_code"      value="<?=$cash_tr_code   ?>">
<input type='hidden' name="cash_id_info"      value="<?=$cash_id_info   ?>">

<input type='hidden' name="top_amount"        value="<?=$top[amount]?>">
<input type='hidden' name="my_donated"        value="<?=$my_donated[amount]?>">
<input type='hidden' name="seat_info"         value="<?=$seat_info?>">
</form>

</body>
</html>
