<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	//header("HTTP/1.0 404 Not Found");
	//exit;
}

$_required = true;
$_path = '../';
include '../config.php';

sec_session_start();

if (!$_SESSION[user_no]) {
   print '0009';
   exit;
}
if($_POST[target] === 'connect_post') {
   if(!$_SESSION[connect_it_id] ) {
      print 'failed';
      exit;
   }
   if($_POST[mode] != 'delete') {
      if(!isset($_POST[it_id]) || !preg_match("/^[0-9]{10}$/", $_POST[it_id])) {
         print 'failed';
         exit;
      }
      $it_id = $_POST[it_id];

      $chk_cn = sql_fetch("SELECT it_connectors FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
      $chk_cn = explode("|",$chk_cn[it_connectors]);

      if($chk_cn[0] == '') {
         echo 'failed';
         exit;
      }

      $connector = $chk_cn[0];
      $connector_info = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = $connector");
      $category = 100;
      $cmt = htmlspecialchars_decode($_POST[content]);
      //$cmt = preg_replace('/(<p><br><\/p>)*$/','',$cmt);

      $res = sql_query("INSERT INTO ".DB_CAMPAIGN_CMTS." SET 
            cmt_category = '$category',
            it_id = '$it_id',
            mb_no = '$connector',
            cmt = '$cmt',
            cmt_ip = '$_SERVER[REMOTE_ADDR]',
            cmt_depth = '0',
            parent_id = '0',
            via = '$_POST[viewport]'
      ");
      if($res) {
            
         $cmt_id = mysql_insert_id();
      ?>
         <div class='connect_post' data-postid='<?=$cmt_id?>'>
            <div class='connect_post_info'>
               <div class='profile_img'><? drawPortrait($connector);?></div>
               <div class='profile_info'>
                  <div style='line-height:26px;margin-top:2px;'>
                  <span style='font:14px NaNumGothicBold;'><?=$connector_info[mb_name]?></span><br/><span style='font-size:9pt;color:#a8a8a8;'><?=date('Y-m-d H:i:s')?></span><br/>
                  </div>
               </div>
               <div class='btn_group'>
                  <span class='like_post_btn' data-postid='<?=$cmt_id?>' style='cursor:pointer;'>좋아요</span>
                  <div class='like_box'><span class='num'>0</span></div>
                  <span class='reply_post_btn' data-postid='<?=$cmt_id?>' style='cursor:pointer;'>댓글달기</span>
                  <? if($_SESSION[connect_it_id]) { ?>&nbsp;&nbsp;<span class='del_post_btn' data-postid='<?=$cmt_id?>' style='cursor:pointer;'>삭제</span><? } ?>
               </div>
            </div>
            <div class='redactor_editor'>
               <?=$cmt?>
            </div>
            <div class='connect_cmt_list'>
               <div class="connect_cmt cmt_input">
                  <ul>
                     <li style="float:left;width:41px"><? drawPortrait($_SESSION[user_no]); ?></li>
                     <li style="float:left;width:484px;margin-left:10px;">
                        <textarea name='connect_cmt_content' data-postid='<?=$cmt_id?>' class='text' 
                           style='width:423px;padding-left:7px;padding-top:7px;overflow:hidden;vertical-align:top;height:28px;'></textarea>
                        <img class='connect_cmt_input_btn' data-postid='<?=$cmt_id?>' src="/images/campaign/btn_submitcmt.png" style='cursor:pointer;width:40px;'>
                     </li>
                  </ul>
                  <div style="clear: left"></div>
               </div>
               <div class="added">
               </div>
            </div>
         </div>
      
   <? } else {
         print 'failed';
         exit;
      }
   } else {
      if(!isset($_POST[post_id]) || $_POST[post_id] == '') {
         print 'failed';
         exit;
      }
      $chk_mbno = $_SESSION[is_admin] ? false : " AND a.mb_no = '$_SESSION[user_no]' ";
      $res = sql_query("DELETE a.* FROM ".DB_CAMPAIGN_CMTS." a WHERE a.cmt_id = $_POST[post_id] $chk_mbno ");
      if($res) {
         if(mysql_affected_rows() == 1) {
            sql_query("DELETE FROM ".DB_CAMPAIGN_CMTS." WHERE cmt_id = $_POST[post_id]");
            sql_query("DELETE FROM ".DB_CMT_LIKES." WHERE cmt_id = $_POST[post_id]");
            echo 'success';
         } else {
            echo 'failed';
         }
      }
      exit;
   }

} else if($_POST[target] === 'connect_cmt') {
   if($_POST[mode] != 'delete') {
      if(!isset($_POST[post_id]) || $_POST[post_id] =='' || !isset($_POST[post_id]) || $_POST[post_id] == '') {
         print 'failed';
         exit;
      }
      $category = 101;
      $it_id = $_POST[it_id];
      $cmt = str_replace("\\n","<br>",$_POST[cmt]);
      $pid = $_POST[post_id];
      $res = sql_query("INSERT INTO ".DB_CAMPAIGN_CMTS." SET 
            cmt_category = '$category',
            it_id = '$it_id',
            mb_no = '$_SESSION[user_no]',
            cmt = '$cmt',
            cmt_ip = '$_SERVER[REMOTE_ADDR]',
            cmt_depth = '0',
            parent_id = '$pid',
            via = '$_POST[viewport]'
      ");
      if($res) {
         $cmt_id = mysql_insert_id();
      ?>
         <div class="connect_cmt" data-cmtid="<?=$cmt_id?>">
            <ul>
               <li style="float:left;width:41px"><? drawPortrait($_SESSION[user_no]); ?></li>
               <li style="float:left;width:484px;margin-left:10px;">
                  <span class="connect_cmt_del" data-cmtid="<?=$cmt_id?>" style='float:right;margin-left:5px;cursor:pointer;'>삭제</span>
                  <p><strong><?=$_SESSION[username]?></strong> 
                  <span style="color:gray;font:9px Arial"><?=date('Y-m-d H:i:s')?></span></p>
                  <p style="margin-top: 10px"><?=$cmt?></p>
               </li>
            </ul>
            <div style="clear: left"></div>
         </div>
         
      <?
      } else {
         print 'failed';
         exit;
      }
   } else {
      if(!isset($_POST[cmt_id]) || $_POST[cmt_id] == '') {
         print 'failed';
         exit;
      }
      $chk_mbno = $_SESSION[is_admin] ? false : " AND a.mb_no = $_SESSION[user_no] ";
      $res = sql_query("DELETE a.* FROM ".DB_CAMPAIGN_CMTS." a WHERE a.cmt_id = $_POST[cmt_id] $chk_mbno ");
      if($res) {
         if(mysql_affected_rows() == 1) {
            print 'success';
            exit;
         } else {
            print 'failed';
            exit;
         }
      } else {
         print 'failed';
         exit;
      }
   }

} else {
   $category = $_POST[category];
   $it_id = $_POST[it_id];
   if (!preg_match("/^[0-9]{10}$/", $it_id)) exit;
   $parent_id = $_POST[cmt_id];
   $cmt = $_POST[cmt];
   $depth = ($parent_id) ? 1 : 0;
   $is_en = ($_POST[is_en] == 1) ? true : false;

   if (!$_POST[mode] || $_POST[mode] != 'delete') {
      if (!$cmt) {
         print '0008';
         exit;
      }

      sql_query("INSERT INTO ".DB_CAMPAIGN_CMTS."
            SET cmt_category = '$category',
               it_id = '$it_id',
               mb_no = '$_SESSION[user_no]',
               cmt = '$cmt',
               cmt_ip = '$_SERVER[REMOTE_ADDR]',
               cmt_depth = '$depth',
               parent_id = '$parent_id',
               via = '$_POST[viewport]'
      ");

      // ACTIVITY LOG
      if ($_SESSION[user_no]) {
   //		mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '3', mb_no = '$_SESSION[user_no]', param1 = 'comment', param2 = '$it_id', referer = '$_SERVER[HTTP_REFERER]' ");
      }

      $cmt_id = mysql_insert_id();

      $cmt = str_replace('\\n', '<br/>', $cmt);
      if ($depth == 0) {
   ?>
   <div class='commentCell'>
      <ul>
      <li style='float: left; width: 70px'><?=drawPortrait($_SESSION[user_no])?></li>
      <li style='float: left; width: 530px'>
            <span class='btn_delete' item='<?=$it_id?>' cmt='<?=$cmt_id?>'>삭제</span>
            <? if ($depth == '0') { ?><span class='btn_reply' item='<?=$it_id?>' cmt='<?=$cmt_id?>'>답글</span><? } ?>
         <p><strong><?=$_SESSION[username]?></strong> <span style='color: gray; font: 9px Arial'><?=date('Y-m-d H:i:s')?></span></p>
         <p style='margin-top: 10px'>
         <?
         switch ($category) {
            case 1 :
               print $is_en ? "[To Victim]" : "[수혜자에게]"; break;
            case 2 :
               print $is_en ? "[To Star/Mentor]" : "[스타/멘토에게]"; break;
            case 3 :
               print $is_en ? "[To Sponsor]" : "[스폰서에게]"; break;
            case 4 :
               print $is_en ? "[To Wegen]" : "[위젠에게]"; break;
         }
         ?>
         <?=$cmt?></p>
      </li>
      </ul>
      <div style='clear: left'></div>
   </div>
   <?
      }
      else {
   ?>
   <div class='replyCell fresh'>
      <ul>
      <li style='float: left; width: 70px'><?=drawPortrait($_SESSION[user_no])?></li>
      <li style='float: left; width: 480px'>
         <span class='btn_delete' item='<?=$it_id?>' cmt='<?=$cmt_id?>'>삭제</span>
         <p><strong><?=$_SESSION[username]?></strong> <span style='color: gray; font: 9px Arial'><?=date('Y-m-d H:i:s')?></span></p>
         <p style='margin-top: 10px'><?=$cmt?></p>
      </li>
      </ul>
      <div style='clear: left'></div>
   </div>
   <?
      }
   }
   else { // mode: delete
      $result = mysql_query("DELETE FROM ".DB_CAMPAIGN_CMTS."
            WHERE it_id = '$it_id'
            AND mb_no = '$_SESSION[user_no]'
            AND cmt_id = '$parent_id'
      ");

      print $result ? "0000" : "0001";
   }
}
?>
