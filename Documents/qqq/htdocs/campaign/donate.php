<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;

$_loginrequired = true;
$_required = true;
include '../config.php';

if($isMobile) { 
	header("Location: /m/campaign/{$_GET[it_id]}/donate");
	exit;
	//echo "<script>location.href='http://wegen.kr/campaign/$_GET[it_id]/donate_m';</script>";
}

include '../module/_head.php';
$row = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id='$_GET[it_id]' AND type!=2");

if(!$row['it_id']) { alert("캠페인 정보가 없습니다.","$g4[path]"); }
if ($row['it_isEnd'] == '1') { alert("이미 종료된 캠페인입니다."); }
if($row[it_isPublic] == 0) {
   alert('잘못된 캠페인 코드입니다.');
   exit;
}
$isOpenCampaign = $row[it_isOpenCampaign] == 1 ? true : false;
$it_id = $row[it_id];

$it_opt1 = explode('|', $row['it_currency']);
$totalopt = count($it_opt1)-1;

$hzh = $row['it_id'] == '1371778995' ? 1 : 0;
$dikpung = $row['it_id'] == '1372190258' ? 1 : 0;
$blockb = $row['it_id'] == '1371992245' ? 1 : 0;
$sandara = $row['it_id'] == '1372929550' ? 1 : 0;
$kara_fan = $row[it_id] == $SPECIAL[kara_fan] ? true : false;

if($hzh == 1) {
	$sql = "SELECT mb_no FROM ".DB_ORDERS." WHERE it_id='1371778995' AND od_amount='20000'";
	$res = sql_query($sql);
	$num = @mysql_num_rows($res);
	if($num == 64) {
		alert('참가팀수가 다 찼습니다');
		exit;
	}
}
if($kara_fan) {
   $ticket_chk = sql_fetch("SELECT COUNT(od_id) AS donate_num 
               FROM ".DB_ORDERS." a 
               WHERE a.it_id = '$row[it_id]'");
   if($ticket_chk[donate_num] >= ($row[it_target]/5000)-32) {
      alert('남아있는 티켓이 없습니다 T^T');
      exit;
   }
   $donate_chk = sql_fetch("SELECT od_id FROM ".DB_ORDERS." WHERE it_id = '$row[it_id]' AND mb_no = $_SESSION[user_no]");
   if($donate_chk[od_id]) {
      alert('이미 티켓을 구매하셨습니다. (1인 1매)');
      exit;
   }
}
if($row['it_id'] == $SPECIAL[cw1] || $row['it_id'] == $SPECIAL[cw2]) {
   $sql = "SELECT IFNULL(SUM(od_amount), 0) as funded,
      (SELECT IFNULL(SUM(round(od_amount/15000, 0)),0) FROM ".DB_ORDERS." WHERE it_id='$row[it_id]' 
         AND substr(reward,1,1) = '1'
         AND IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1 ) as goods_num_1,
      (SELECT IFNULL(SUM(round(od_amount/15000,0)),0) FROM ".DB_ORDERS." WHERE it_id='$row[it_id]'
         AND substr(reward,1,1) = '2'
         AND IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1 ) as goods_num_2
      FROM ".DB_ORDERS." WHERE it_id='$row[it_id]' and IF(od_method = '가상계좌', IF(substr(od_escrow1,-14) > ".date("YmdHms").", 1, 0), 1)  = 1";
   $data = sql_fetch($sql);
   if($data[funded] >= $row[it_target]) {
      alert("재고가 없습니다 T^T");
      exit;
   }
   $type_cw = true;
   $goods_num = ($row[it_target] - $data[funded]) / 15000;
   if($row['it_id'] == $SPECIAL[cw1]) {
      $goods_1 = '1.봄의은하(압화)';
      $goods_2 = '2.브라운(기본)';
   } else {
      $goods_1 = "1.꽃밭에서(WHITE)";
      $goods_2 = "2.바램(BLACK)";
   }
   $goods_1_num = 50-(int)$data[goods_num_1];
   $cw_selected_1 = $goods_1_num == 0 ? false : 'selected';
   $goods_2_num = 50-(int)$data[goods_num_2];
   $cw_selected_2 = $cw_selected_1 ? false : 'selected';
}

// ACTIVITY LOG
if ($_SESSION['user_no']) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'donate', param2 = '$row[it_id]', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<script type='text/javascript' src='<?=$g_conf_js_url?>'></script>
<script type='text/javascript' src='/js/zipsearch.js'></script>
<script type='text/javascript'>
StartSmartUpdate();
$(document).ready(function() {
   createLayer($('#pluginLayer'));
   setTimeout('kcp_checkPlugin()', 1000);
	$('input[type=text]').addClass('text');

	$('input[name=isReceipt]').change(function() {
		if ($(this).val() == 1) {
			$('.ppForm').show();
			$('.pcForm').hide();
		}
		else if ($(this).val() == 2) {
			$('.ppForm').hide();
			$('.pcForm').show();
		}
		else {
			$('.ppForm, .pcForm').hide();
		}
	});
<? if($type_cw) { ?>
   $('#cw_mod_btn').click(function() {
      var gnum = $('input[name=cw_goods_num]').val()*1;
      var lnum = $('select[name=reward_req]').val() == '<?=$goods_1?>' ? <?=$goods_1_num?> : <?=$goods_2_num?>;
      if(gnum > lnum) {
         qAlert('재고 수량보다 많습니다. (현재 재고량 : '+lnum+' 개)');
         $('input[name=cw_goods_num]').val('1');
         $('#cw_price').html('17,500');
         $('input[name=od_price]').val('17500');
         return false;
      }
      $('input[name=od_price]').val(''+(gnum*15000+2500));
      var reg = /\B(?=(\d{3})+(?!\d))/g
      $('#cw_price').html((gnum*15000+2500).toString().replace(reg, ","));
   });
<? } ?> 
});

function kcp_checkPlugin() {

	if (navigator.userAgent.indexOf('MSIE') > 0 || (navigator.userAgent.indexOf('Trident/7.0') > 0)) {
		if ( document.Payplus.object != null ) {
			$('.layer').remove();
		}
	}
	else {
		var inst = 0;
		for (var i = 0; i < navigator.plugins.length; i++) {
			if (navigator.plugins[i].name == 'KCP') {
				inst = 1;
				break;
			}		
		}

		if (inst == 1) {
			$('.layer').remove();
		}
		else {
			document.location.href=GetInstallFile();
		}
	}
}

function kcp_runPlugin(form) {
	
	var RetVal = false;
   if( 0 == <?=$hzh?>) {
   <? if($type_cw)  { ?>
      var gnum = $('input[name=cw_goods_num]').val()*1;
      var lnum = $('select[name=reward_req]').val() == '<?=$goods_1?>' ? <?=$goods_1_num?> : <?=$goods_2_num?>;
      if(gnum > lnum) {
         qAlert('재고 수량보다 많습니다. (현재 재고량 : '+lnum+' 개)');
         $('input[name=cw_goods_num]').val('1');
         $('#cw_price').val('17,500');
         $('input[name=od_price]').html('17500');
         return false;
      }
      $('input[name=good_mny]').val($('input[name=od_price]').val());
      <? } else if($kara_fan) { ?>
      $('input[name=good_mny]').val('5000');
      <? } else { ?>
		var p = checkRadioValue(document.donationForm.od_price);
		if (!p) {
			qAlert('후원하실 금액을 선택해 주세요.');
			return false;
		}
		if (p == 'user_input') {
			p = $.trim($('input[name=user_input]').val());
			if (!p || parseInt(p) == 0) {
				qAlert('후원을 희망하는 금액을 입력해 주세요.');
				return false;
			}
         <? if($it_id == '1376819019') {  // 클라라캠페인만 임시적으로. ?>
			if (p*1 < 1000 ) {
				qAlert('후원은 최소 1,000원 이상부터 가능합니다.');
				return false;
			}
         <? } else { ?>
			if (p*1 < $('#donationPrice #p0').val()*1 ) {
				qAlert('후원은 최소 '+$('label[for=p0]').html()+' 이상부터 가능합니다.');
				return false;
			}

         <? } ?>
         
		}
		$('input[name=good_mny]').val(p);
   <? } ?>
   } else {
		if(!$('input[name=lol_team_name]').val()) {
			qAlert('팀명을 입력해주세요');
			$('input[name=lol_team_name]').focus();
			return false;
		}
		if(!$('input[name=lol_name_1]').val()) {
			qAlert('대표자 이름을 적어주세요');
			$('input[name=lol_name_1]').focus();
			return false;
		}
		
		if(!$('input[name=lol_num_1]').val()) {
			qAlert('대표자 번호를 적어주세요');
			$('input[name=lol_name_1]').focus();
			return false;
		}
		$('#lol_table').append("<input type='hidden' name='reward_req'/>");
		var lol = $('input[name=lol_team_name]').val() + '|' +
				$('input[name=lol_name_1]').val() +','+ $('input[name=lol_num_1]').val();

		$('input[name=reward_req]').val(lol);
		$('input[name=good_mny]').val('20000');
	}

	var m = checkRadioValue(document.donationForm.od_method);
	if (!m) {
		qAlert('결제방법을 선택해 주세요.');
		return false;
	}
	$('input[name=pay_method]').val(m);

	if (!$.trim($('input[name=buyr_name]').val())) {
		qAlert('후원자 이름을 입력해 주세요.');
		$('input[name=buyr_name]').focus();
		return false;
	}

	var hps = ['input[name=od_hp1]', 'input[name=od_hp2]', 'input[name=od_hp3]'];
	for (var i = 0; i < 3; i++)	{
		if (!$.trim($(hps[i]).val())) {
			qAlert('후원자 연락처를 입력해 주세요.');
			$(hps[i]).focus();
			return false;
		}
	}
	$('input[name=buyr_tel1]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());

	if (!$.trim($('input[name=buyr_mail]').val())) {
		qAlert('후원자 이메일 주소를 입력해 주세요.');
		$('input[name=buyr_mail]').focus();
		return false;
	}

	if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=buyr_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('input[name=od_zip1]').val())) {
		qAlert('우편번호 검색창을 통해 주소를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=od_addr2]').val()) || $('input[name=od_addr2]').val() == '상세주소 입력') {
		qAlert('상세주소를 입력해 주세요.');
		return false;
	}
<?
$sql = "SELECT * FROM ".DB_FUNDRAISERS."
		WHERE it_id = '$row[it_id]'
		";
$result = sql_query($sql);
$total = mysql_num_rows($result);
if ($total > 0 && !$kara_fan) {
	if( $hzh == 0) {
?>
	if (!checkRadioValue(document.donationForm.od_isEvent)) {
		qAlert('이벤트 참여 여부를 선택해 주세요.');
		return false;
	}
<? } else { ?>
	$('#lol_table').append("<input type='hidden' name='od_isEvent'/>");
	$('input[name=od_isEvent]').val('24');
<? } 
}?>

<? if ($row[it_isReceipt] == 1) : ?>
	var isReceipt = checkRadioValue(document.donationForm.isReceipt);
	if (isReceipt == 1) {
		if (!$.trim($('input[name=realname]').val())) {
			qAlert('영수증 발행을 위해 실명을 입력해 주세요.');
			return false;
		}
		if (!$.trim($('input[name=serial1]').val()) || !$.trim($('input[name=serial2]').val())) {
			qAlert('영수증 발행을 위해 주민등록번호를 입력해 주세요.');
			return false;
		}
	}
	if (isReceipt == 2) {

		if (!$.trim($('input[name=permit1]').val()) || !$.trim($('input[name=permit2]').val()) || !$.trim($('input[name=permit3]').val())) {
			qAlert('영수증 발행을 위해 사업자등록번호를 입력해 주세요.');
			return false;
		}

		var ext = $('input[name=permitimage]').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
			qAlert('사업자등록증 이미지가 올바른 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
			return false;
		}
	}
<? endif; ?>

if(m == 'coin') {
   document.donationForm.action = '/campaign/proc_coin.php';
   document.donationForm.method = 'post';
   return true;
} else {
	if (MakePayMessage(form) == true) {
		RetVal = true;
	}
	else {
		// res_cd= 오류코드, res_msg=오류메시지
		res_cd  = document.donationForm.res_cd.value;
		res_msg = document.donationForm.res_msg.value;
	}
	return RetVal ;
}

}
</script>

<div id='pluginLayer' style='display:none;'>
<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
<p>결제 플러그인 설치 여부를 확인하는 중입니다.</p>
브라우저 상단에 노란 알림표시줄이 나타날 경우<br/>
<span style='font-weight: bold; color: white'>"ActiveX 컨트롤 설치"</span>를 선택해 주십시오.
</div>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner donation'>

	<h2 style='margin-bottom: 14px; border: none'><? if($hzh == 1 ) print '신청하기'; else print '후원하기'; ?> </h2>

	<form name='donationForm' enctype='multipart/form-data' method='post' action='../campaign/pp_ax_hub.php'>

	<!-- left -->
	<div id='donationLeft' style='float: left; width: 48%; margin-right: 2%'>

		<h3 style='padding-bottom:5px;'>
         <? if($hzh == 1 ) { 
         print "팀 참가비 : 20,000원<br/><span style='color:#CCC;font:9pt NanumGothic;'><span style='color:red'>5명 팀</span>으로만 참가가 가능하고 팀 대표의 연락처는 꼭 작성해주시기바랍니다.</span>";
         } else if($type_cw) {
            print '결제금액';
         } else { print "후원금액";}?>
      </h3>
         
      <? if($kara_fan){ ?>
         <table class='kara_ticket'>
            <tr>
               <td>KARA 자선 팬미팅</td>
               <td>5,000원</td>
               <td>1매</td>
            </tr>
            <tr>
               <td colspan='2'>TOTAL</td>
               <td>5,000원</td>
            </tr>
         </table>
         <input type='hidden' name='od_price' value='5000'>
      <? }else if($hzh == 1) { ?>
		<div style='width:100%;padding-left:3px;padding-top:14px;'>
			<strong>팀명 : </strong> <input type='text' class='text' name='lol_team_name'/> <br/>
		</div>
		<table id='lol_table' style='width:100%;'>
			<tr>
				<th style='width:20px;'></th>
				<th style='padding:0'>이름</th>
				<th style='padding:0'>전화번호</th>
			</tr>
			<tr>
				<td style='text-align:center;'>1<br/><span style='color:red;'>(대표)</span></td>
				<td><input type='text' class='text' name='lol_name_1'/></td>
				<td><input type='text' class='text numonly' name='lol_num_1'/></td>
			</tr>
		</table>
		<? } else if($type_cw) { ?>
      <table id='cw_table' style='width:100%;'>
         <tr>
            <th style='width:100px;'>상품종류</th>
            <td><select name='reward_req'>
               <option value='<?=$goods_1?>' <?=$cw_selected_1?> ><?="$goods_1 (남은갯수 : $goods_1_num 개)"?></option>
               <option value='<?=$goods_2?>' <?=$cw_selected_2?> ><?="$goods_2 (남은갯수 : $goods_2_num 개)"?></option>
            </select></td>
         </tr>
         <tr>
            <th>판매가</th>
            <td><span id='cw_price'>17,500</span>원 (배송비 2,500원 포함 가격)<input type='hidden' name='od_price' value='17500'></td>
         </tr>
         <tr>
            <th>수량</th>
            <td>
               <input type="text" class='numonly' name='cw_goods_num' style='width:40px;' value='1'>
               <button type='button' id='cw_mod_btn' style='width:44px;'>수정</button>
            </td>
         </tr>
      </table>
      <? } else {  ?>
		<ul id='donationPrice'>
			<?
			for ($i = 0; $i < count($it_opt1); $i++) {
				$it_opt = explode(';', $it_opt1[$i]);
				$it_opt = $it_opt[0];
				if ($it_opt != 'user_input') {
			?>
			<li><input type='radio' name='od_price' value='<?=$it_opt?>' id='p<?=$i?>' />
      <label for='p<?=$i?>'><?=number_format($it_opt)?>원 <? print ($isOpenCampaign) ? "(ticket ".($it_opt/100)."장)" : "" ?></label></li>
			<? }} ?>
			<li><input type='radio' name='od_price' value='user_input' />
			직접입력
			<input type='text' name='user_input' onfocus="$('input[name=od_price]:last').prop('checked',true)" style='text-align: right' class='numonly' /> 원</li>
		</ul>
      <? } ?>
		<h3 style='margin-top: 20px'>결제방법</h3>
		
		<ul id='donationMethod'>
			<li><input type='radio' name='od_method' value='100000000000' id='cd' /><label for='cd'>신용카드</label></li>
			<li><input type='radio' name='
				od_method' value='010000000000' id='ac' /><label for='ac'>계좌이체</label></li>
         <li><input type='radio' name='od_method' value='001000000000' id='vc' /><label for='vc'>가상계좌(무통장입금)</label></li>
			<li><input type='radio' name='od_method' value='000010000000' id='mo' /><label for='mo'>핸드폰</label></li>
         <? if($isOpenCampaign) { ?>
         <li><input type='radio' name='od_method' value='coin' id='co'/><label for='co'>코인</label></li>

         <? } ?>
		</ul>
		<div style='clear: both'></div>
   
	<?
	$rewards = explode('||', $row[it_reward]);
//	if (count($rewards) > 1) :
   if ($row[it_id] == $SPECIAL[cam_school]) : ?>
   <h3 style='margin-top:20px'>수혜자 선택</h3>
      <ul>
         <li><input type="radio" name='reward_req' value="1" id='cs1' checked>
            <label for='cs1'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_1.jpg' target='_blank' title="<div style='overflow: hidden; max-height:800px'><img src='/images/campaign/cam_school_detail_1.jpg' style='width: 100%' /></div>">
               신하영 (9세) 지적장애 3급
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="2" id='cs2'>
            <label for='cs2'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_2.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_2.jpg' style='width: 100%' /></div>">
               김규리 (13세) 뇌성마비 3급
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="3" id='cs3'>
            <label for='cs3'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_3.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_3.jpg' style='width: 100%' /></div>">
               전혜민 (16세) 연골무형성증
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="4" id='cs4'>
            <label for='cs4'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_4.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_4.jpg' style='width: 100%' /></div>">
               박선영 (10세) 뇌병변 1급
            </span></label>
         </li>
         <li><input type="radio" name='reward_req' value="5" id='cs5'>
            <label for='cs5'><span style='text-decoration: none; margin-right: 10px' href='/images/campaign/cam_school_detail_5.jpg' target='_blank' title="<div style='overflow: hidden; max-height: 500px'><img src='/images/campaign/cam_school_detail_5.jpg' style='width: 100%' /></div>">
               김민규 (14세) 연골무형성증
            </span></label>
         </li>
      </ul>
   <?
   endif;
   if (!$kara_fan && $row[it_reward_fromer]) :
	?>
	<h3 style='margin-top: 20px'>리워드 선택</h3>

		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>요청사항</th><td>
			<input type='text' name='reward_req' style='width: 250px' /></td></tr>
		</table>

		<p><?=nl2br($row[it_reward_fromer])?></p>
		
		<?
		// Added by 2013.08.07
		$additional_reward_image = "/data/campaign/{$_GET['it_id']}/reward.jpg";
		if(file_exists("..{$additional_reward_image}")) {
			echo "<img src='{$additional_reward_image}' style='width:auto; height:auto;'>";
		}
		?>
	<div style='clear: both'></div>
	<? endif; ?>

		<?
      if(!$kara_fan)
		include 'donate_eventSelector.php';
		?>
	</div>
	<!-- /left -->


	<!-- right -->
	<div style='float: left; width: 48%; margin-left: 2%'>
		<h3><? if($type_cw) print '배송정보 입력'; else print '개인정보 입력';?></h3>
		<?
		$info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
		$contact = explode('-', $info[mb_contact]);
		?>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>이름</th><td>
			<input type='text' name='buyr_name' value="<?=$_SESSION[username]?>" /></td></tr>
			<tr><th>전화번호</th><td>
			<input type='text' name="od_hp1" value="<?=$contact[0]?>" size='3' maxlength='3' class='numonly' /> -
			<input type='text' name="od_hp2" value="<?=$contact[1]?>" size='4' maxlength='4' class='numonly' /> -
			<input type='text' name="od_hp3" value="<?=$contact[2]?>" size='4' maxlength='4' class='numonly' /></td></tr>
			<tr><th>이메일</th><td>
			<input type='text' name='buyr_mail' value='<?=$info[mb_email]?>' maxlength='50' style='width: 250px' /></td></tr>
			<tr style='height: 120px'><th>주소<? if($hzh == 1) print "<br/><br/><span style='color:red;font:8pt NanumGothic;'>대회관련 상품이 배송될 주소이니 정확히 기재해주세요</span>";?></th><td>

			<div class='zipcode-finder'>
				<input type='text' id="dongName" />
				<input type='button' class='zipcode-search' value='검색' />
				<div class="zipcode-search-result"></div>
			</div>
			<div id='addr'>
				<input type='text' name="od_zip1" value="<?=$info[mb_zip1]?>" size='3' maxlength='3' readonly /> -
				<input type='text' name="od_zip2" value="<?=$info[mb_zip2]?>" size='3' maxlength='3' readonly />
				<input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 250px' readonly /><br/>
				<input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
			</div>

			</td></tr>
		</table>

		<h3 style='margin-top: 20px;<? if($hzh == 1) print "display:none;";?>'>기부금 영수증 신청</h3>
		<? if ($row[it_isReceipt] == 1) : ?>
			<ul id='donationReceipt' style='<?= $hzh == 1 ? "display:none;" : false;?>'>
				<li><input type='radio' name='isReceipt' value='0' id='pn' /><label for='pn'>미신청</label></li>
				<li><input type='radio' name='isReceipt' value='1' id='pp' checked /><label for='pp'>개인</label></li>
				<li><input type='radio' name='isReceipt' value='2' id='pc' /><label for='pc'>사업자</label></li>
			</ul>
			<div style='clear: both'></div>
	
			<table cellpadding='0' cellpadding='0' style='width: 100%;'>
				<colgroup>
					<col width='120' />
					<col width='/' />
				</colgroup>
				<tr class='ppForm'><th>실명</th><td>
				<input type='text' name='realname' maxlength='10' /></td></tr>
				<tr class='ppForm'><th>주민등록번호</th><td>
				<input type='text' name='serial1' maxlength='6' class='numonly' style='width: 50px; text-align: center' /> - <input type='text' name='serial2' maxlength='7' class='numonly' style='width: 50px; text-align: center' /></td></tr>
				<tr class='pcForm' style='display: none'><th>사업자등록번호</th><td>
				<input type='text' name='permit1' maxlength='3' class='numonly' style='width: 30px; text-align: center' /> - <input type='text' name='permit2' maxlength='2' class='numonly' style='width: 20px; text-align: center' /> - <input type='text' name='permit3' maxlength='5' class='numonly' style='width: 50px; text-align: center' /></td></tr>
				<tr class='pcForm' style='display: none'><th>등록증 사본</th><td>
				<input type='file' name='permitimage' style='width: 100%; text-align: center' /></td></tr>
			</table>
	
			<p style='<?= $hzh == 1 ? "display:none;" : false;?>'>영수증 발행은 해당 캠페인의 지원 단체에 요청되며, 기부금 영수증은 캠페인이 완료된 후 해당 단체를 통해 일괄 발송됩니다.<br/>
			입력하신 회원님의 실명 정보와 주민등록번호는 영수증 발행 이의외 목적으로는 사용되지 않으며, 목적 달성 즉시 파기됩니다. 입력하신 정보가 정확하지 않을 경우 영수증 발급이 어려울 수 있습니다.</p>
		<? else : ?>
		<ul id='donationReceipt'>
			<li style='width: 100%; color: silver'>본 캠페인은 기부금 영수증 발급이 불가능합니다.</li>
		</ul>
		<div style='clear: both'></div>
		<? endif; ?>

		<div style='text-align: center;  margin: 0px auto'>
		<p>입력하신 개인정보는 캠페인 진행 상황 및 이벤트 당첨 알림, 리워드 발송 등의 목적으로만<br/>
		사용되며, <a href='/privacy-policy'><strong>개인정보 취급방침</strong></a>에 의거해 보호됩니다.</p>
<!--		<input type='checkbox' name='saveInfo' checked />
		위 정보를 앞으로 후원시에도 자동으로 사용하겠습니다.
-->

		<input type='submit' value='<?=$hzh == 1 ? '참가신청' : '후원하기'?>' class='submit' onclick="return kcp_runPlugin(this.form);" />

		</div>
	</div>
	<!-- /right -->
	<div style='clear: both'></div>

	<input type='hidden' name='ordr_idxx'		value='<?=date('ymdHis').rand(100, 999)?>' />
	<input type='hidden' name='it_id'			value='<?=$_GET[it_id]?>' />
	<input type='hidden' name='itemid'			value='<?=$_GET[it_id]?>' />
	<input type='hidden' name='pay_method'		value='' />
	<input type='hidden' name='buyr_tel1'		value='' />
	<input type='hidden' name='buyr_tel2'		value='' />
	<input type='hidden' name='good_name'		value='<?=$row[it_name]?>' />
	<input type='hidden' name='good_mny'		value='' />
	<input type='hidden' name='req_tx'			value='pay' />
	<input type='hidden' name='site_cd'			value="<?=$g_conf_site_cd?>" />
	<input type='hidden' name='site_key'		value="<?=$g_conf_site_key?>" />
	<input type='hidden' name='site_name'		value="<?=$g_conf_site_name?>" />
	<input type='hidden' name='quotaopt'		value='12' />
	<input type='hidden' name='currency'		value='WON'/>
   <input type="hidden" name="vcnt_expire_term" value="3"/>
	<!-- PLUGIN 설정 (변경 불가) -->
	<input type='hidden' name='module_type'		value='01' />
	<input type='hidden' name='epnt_issu'		value='' />
	<input type='hidden' name='res_cd'			value='' />
	<input type='hidden' name='res_msg'			value='' />
	<input type='hidden' name='tno'				value='' />
	<input type='hidden' name='trace_no'		value='' />
	<input type='hidden' name='enc_info'		value='' />
	<input type='hidden' name='enc_data'		value='' />
	<input type='hidden' name='ret_pay_method'	value='' />
	<input type='hidden' name='tran_cd'			value='' />
	<input type='hidden' name='bank_name'		value='' />
	<input type='hidden' name='bank_issu'		value='' />
	<input type='hidden' name='use_pay_method'	value='' />
	<input type='hidden' name='cash_tsdtime'	value='' />
	<input type='hidden' name='cash_yn'			value='' />
	<input type='hidden' name='cash_authno'		value='' />
	<input type='hidden' name='cash_tr_code'	value='' />
	<input type='hidden' name='cash_id_info'	value='' />
	<!-- 제공 기간 설정 0:일회성 1:기간설정(ex 1:2012010120120131) -->
	<input type='hidden' name='good_expr'		value='0' />
	<input type='hidden' name='site_logo'		value='' />
	<input type='hidden' name='skin_indx'		value='1' />
	<input type='hidden' name='connect_type'	value='PC' />
<?
/*
신용카드사 삭제
<input type='hidden' name="not_used_card" value="CCPH:CCSS:CCKE:CCHM:CCSH:CCLO:CCLG:CCJB:CCHN:CCCH"/>
사용카드 설정 여부 (통합결제창 노출 유무)
<input type='hidden' name="used_card_YN"value="Y"/>
사용카드 설정 (해당 카드만 결제창에 보이게 설정. used_card_YN 값이 Y일때 적용)
<input type='hidden' name="used_card"value="CCBC:CCKM:CCSS"/>					
해외카드 구분 (해외비자, 해외마스터, 해외JCB로 구분하여 표시)
<input type='hidden' name="used_card_CCXX"value="Y"/>  
결제창 영문 표시
<input type='hidden' name='eng_flag'  value='Y'>
현금영수증 등록창 출력 여부
현금영수증 사용시 KCP 상점관리자 페이지에서 현금영수증 사용 동의 필요
*/
?>
	<input type='hidden' name='disp_tax_yn'		value='N' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token'			value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
	</form>

	</div>
</div>&nbsp;
</div>

<?
include '../module/_tail.php';
?>
