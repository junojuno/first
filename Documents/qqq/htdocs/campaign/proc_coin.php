<?
$_required = true;
include '../config.php';

sec_session_start();

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
	exit;
}
$_SESSION[token] = '';

if (!$_SESSION[user_no]) {
	alert('로그인이 필요한 페이지입니다.');
	exit;
}

$it_id		= $_POST["itemid"];
if (!preg_match("/^[0-9]{10}$/", $it_id)) exit;

$od_isEvent	= $_POST["od_isEvent"];
$cust_ip	= getenv("REMOTE_ADDR"); // 요청 IP
$ordr_idxx	= $_POST["ordr_idxx"]; // 쇼핑몰 주문번호
if (!preg_match("/^[0-9]{15}$/", $ordr_idxx)) exit;

$good_mny	= $_POST["good_mny"]; // 결제 총금액
if (!preg_match("/^[0-9]+$/", $good_mny)) exit;

$buyr_name	= $_POST["buyr_name"]; // 주문자명
$buyr_tel1	= $_POST["buyr_tel1"]; // 주문자 전화번호
$buyr_tel2	= $_POST["buyr_tel2"]; // 주문자 핸드폰 번호
$buyr_mail	= $_POST["buyr_mail"]; // 주문자 E-mail 주소

$good_name  = $_POST["good_name"];
$od_isEvent	= $_POST["od_isEvent"];
$reward		= $_POST["reward_req"];

$use_pay_method	= '코인'; // 결제 방법
$app_time = date('Y-m-d H:i:s');

// 코인 잔여량 체크
$check = sql_fetch("SELECT SUM(amount) AS coin FROM ".DB_COINS."
					WHERE mb_no = '$_SESSION[user_no]' AND coin_category != 4");

if ($check[coin] < $good_mny) {
	alert('남은 코인 수가 후원금액보다 적어 후원이 불가능합니다.');
	exit;
}

// 후원 기본설정 체크
$check = sql_fetch("SELECT isReceipt, receiptName, receiptPno FROM ".DB_REGULARPAYMENT."
					WHERE mb_no = '$_SESSION[user_no]'
					AND isFirst = '1' ");

$mem = sql_fetch("SELECT mb_name, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = $_SESSION[user_no]");
// 처리
if($it_id == $SPECIAL[kara_fan]) {
   $cam_chk = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
   $ticket_chk = sql_fetch("SELECT COUNT(od_id) AS donate_num 
               FROM ".DB_ORDERS." a 
               WHERE a.it_id = '$it_id'");
   $diff = round((strtotime($cam_chk[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
   if($diff < 0) {
      alert('참여 기간이 지났습니다.');
      exit;
   }
   if($ticket_chk[donate_num] >= $cam_chk[it_target]/5000) {
      alert('남아있는 티켓이 없습니다 T^T');
      exit;
   }
   $donate_chk = sql_fetch("SELECT od_id FROM ".DB_ORDERS." WHERE it_id = '$it_id' AND mb_no = $_SESSION[user_no]");
   if($donate_chk[od_id]) {
      alert('이미 티켓을 구매하셨습니다. (1인 1매)');
      exit;
   }
   if($good_mny != '5000') {
      alert('5천원만 가능합니다.');
      exit;
   }
}
$result = sql_query("INSERT INTO ".DB_ORDERS."
			SET
			od_id		= '$ordr_idxx',
			it_id		= '$it_id',
			mb_no		= '$_SESSION[user_no]',
			od_name		= '$mem[mb_name]',
			od_hp		= '$mem[mb_contact]',
			od_zip1		= '$mem[mb_zip1]',
			od_zip2		= '$mem[mb_zip2]',
			od_addr1	= '$mem[mb_addr1]',
			od_addr2	= '$mem[mb_addr2]',
			od_isEvent	= '$od_isEvent',
			od_amount	= '$good_mny',
			od_time		= '$app_time',
			od_ip		= '$cust_ip',
			od_method	= '코인',
			isReceipt   = '$check[isReceipt]',
         receiptName = '$check[receiptName]',
         receiptPno  = '$check[receiptPno]',
			reward		= '$reward_req'
");

if($it_id == $SPECIAL[kara_fan]) {
   //좌석배치
   if($result) {
      $info = sql_fetch("SELECT COUNT(od_id) AS rank FROM ".DB_ORDERS." WHERE it_id = '$it_id' AND pay_remain = 0 AND od_time <= '$app_time'");
      $target_rank = $info[rank];
      include '../module/set_seat_var.php';
      $result = sql_query("UPDATE ".DB_ORDERS." SET var_1 = '$seat_info' WHERE od_id = '$ordr_idxx'");
   }
}
if ($result) {
	$good_mny2 = 0 - $good_mny;
	$result = mysql_query("INSERT INTO ".DB_COINS."
				SET
				mb_no		= '$_SESSION[user_no]',
				coin_category = '9',
				coin_desc	= '캠페인 직접후원',
				od_id		= '$ordr_idxx',
				it_id		= '$it_id',
				amount		= '$good_mny2',
				od_time		= '$app_time'
	");
}

// 회원정보 업데이트
//mysql_query("UPDATE ".DB_MEMBERS."
//			SET mb_email	= '$buyr_mail',
//				mb_contact	= '$buyr_tel1',
//				mb_zip1		= '$_POST[od_zip1]',
//				mb_zip2		= '$_POST[od_zip2]',
//				mb_addr1	= '$_POST[od_addr1]',
//				mb_addr2	= '$_POST[od_addr2]'
//			WHERE mb_no = '$_SESSION[user_no]'
//			");

// 뱃지 발급 루틴
$procBadges = array();

if ($result) {
	// 최초 기부(위젠 레벨) 체크
	$c = sql_fetch("
			SELECT COUNT(od_id) AS total
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			AND mb_no = '$_SESSION[user_no]'
			");
	if ($c[total] == 1) {
//		array_push($procBadges, 'level');
	}

	// 해당캠페인 최초 기부 체크
	$c = sql_fetch("
			SELECT COUNT(od_id) AS total
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			AND it_id = '$it_id'
			");
	if ($c[total] == 1) {
		array_push($procBadges, '1');
	}

	// 최후 기부 & 오버킬 기부 체크
	$c = sql_fetch("
			SELECT SUM(od_amount) AS re_sum, i.it_target
			FROM ".DB_ORDERS." o
			LEFT JOIN ".DB_CAMPAIGNS." i ON (o.it_id = i.it_id)
			WHERE o.it_id = '$it_id'
			GROUP BY o.it_id
			");

	$diff = $c[re_sum] - $c[it_target];

	// 모금액이 목표액 초과했을 경우
	if ($diff >= 0) {
		// 최후 기부 체크 (초과액수$diff가 기부액수보다 작을 경우)
		// 작거나 '같은' 경우를 체크할 경우 모금액과 목표액이 딱 맞춰진 상황에서 다음 1인에게도 뱃지가 중복적용됨
		if ($good_mny > $diff) {
			array_push($procBadges, '2');
		}
		// 오버킬 기부
		else {
			array_push($procBadges, '6');
		}
	}

	// 카테고리 뱃지 체크
	$c = sql_fetch("
			SELECT category
			FROM ".DB_CAMPAIGNS."
			WHERE it_id = '$it_id'
			");
	if (strpos($c[category], ',') === false) $category = $c[category];
	else $category = explode(', ', $c[category]) and $category = $category[0];

	if ($category == '역사/문화') { array_push($procBadges, '101'); }
	elseif ($category == '환경/동물보호') { array_push($procBadges, '102'); }
	elseif ($category == '아동/청소년') { array_push($procBadges, '103'); }
	elseif ($category == '노인') { array_push($procBadges, '104'); }
	elseif ($category == '장애인') { array_push($procBadges, '105'); }
	elseif ($category == '저소득가정') { array_push($procBadges, '106'); }
	elseif ($category == '다문화가정') { array_push($procBadges, '107'); }
	elseif ($category == '자활형 캠페인') { array_push($procBadges, '108'); }

	for ($i = 0; $i < count($procBadges); $i++) {
		$check = sql_fetch("
				SELECT mb_no
				FROM ".DB_BADGES."
				WHERE mb_no = '$_SESSION[user_no]'
				AND category = '$procBadges[$i]'
				AND it_id = '$it_id'
				");
		if (!$check[mb_no]) {
			$badge = "INSERT ".DB_BADGES."
						SET mb_no			= '$_SESSION[user_no]',
							category		= '$procBadges[$i]',
							od_id			= '$ordr_idxx',
							it_id			= '$it_id',
							param			= '$good_mny'
					";
			sql_query($badge);
		}
		unset($check);
	}
}
// 뱃지루틴 끗

// 리워드업데이트
if($result) {
   include '../module/reward.count.update.php'; 
}

$result = $result ? "true" : "false";
$bSucc = $result; // DB 작업 실패 또는 금액 불일치의 경우 "false" 로 세팅

$sql = "SELECT SUM(od_amount)-SUM(pay_remain) as amount FROM ".DB_ORDERS." WHERE it_id = '$it_id' and od_method !='직접입력' group by mb_no order by amount desc";
$top = sql_fetch($sql);

$sql = "SELECT SUM(od_amount)-SUM(pay_remain) as amount FROM ".DB_ORDERS." WHERE it_id = '$it_id' and mb_no = $_SESSION[user_no] group by mb_no";
$my_donated = sql_fetch($sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>위제너레이션</title>
<link rel='stylesheet' type='text/css' href='/css/style.css' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
</head>

<body onload="document.pay_info.submit()">

<script type='text/javascript'>
function noRefresh() {
	if ((event.keyCode == 78) && (event.ctrlKey == true)) {
		event.keyCode = 0;
		return false;
	}
	if(event.keyCode == 116) {
		event.keyCode = 0;
		return false;
	}
}
document.onkeydown = noRefresh;
</script>

<div class='layer'>
	<div id='pluginLayer' class='centering' style='display: block; margin-left: -200px; margin-top: -150px'>
	<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
	<p>결제 진행 중입니다.</p>
	창을 닫거나 새로고침하지 마십시오.
	</div>
</div>

<form name='pay_info' method='post' action='./result.php'>
<input type='hidden' name="use_pay_method"    value="COIN" />
<input type='hidden' name="req_tx"            value="pay">
<input type='hidden' name="bSucc"             value="<?=$bSucc          ?>">
<input type='hidden' name="res_cd"            value="0000">
<input type='hidden' name="res_msg"           value="정상처리">
<input type='hidden' name="ordr_idxx"         value="<?=$ordr_idxx      ?>">
<input type='hidden' name="good_mny"          value="<?=$good_mny       ?>">
<input type='hidden' name="good_name"         value="<?=$good_name      ?>">
<input type='hidden' name="it_id"		      value="<?=$it_id          ?>">
<input type='hidden' name="buyr_name"         value="<?=$buyr_name      ?>">
<input type='hidden' name="buyr_tel1"         value="<?=$buyr_tel1      ?>">
<input type='hidden' name="buyr_tel2"         value="<?=$buyr_tel2      ?>">
<input type='hidden' name="buyr_mail"         value="<?=$buyr_mail      ?>">
<input type='hidden' name="app_time"          value="<?=$app_time       ?>">

<input type='hidden' name="top_amount"        value="<?=$top[amount]?>">
<input type='hidden' name="my_donated"        value="<?=$my_donated[amount]?>">
<input type='hidden' name='seat_info'         value="<?=$seat_info?>">
</form>

</body>
</html>
