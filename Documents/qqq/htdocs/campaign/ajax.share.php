<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$_required = true;
$_path = '../';
include '../config.php';

sec_session_start();

if (!preg_match("/^[0-9]{10}$/", $_POST[it_id])) exit;
if ($_POST[post_id]) {
	if (!preg_match("/[0-9]+_[0-9]+$/", $_POST[post_id])) exit;
}
if ($_POST[via] != 'facebook' && $_POST[via] != 'twitter') exit;

$sql = "UPDATE ".DB_CAMPAIGNS."
		SET it_share_".$_POST[via]." = it_share_".$_POST[via]." + 1
		WHERE it_id = '$_POST[it_id]'
		";
$result = mysql_query($sql);
if (!$result) {
	print '0007'; // no such campaign
	exit;
}

if ($_SESSION['user_no']) {
	$check = "SELECT id FROM ".DB_BADGES."
			WHERE mb_no = '$_SESSION[user_no]'
			AND category = '3'
			AND od_id = '$_POST[via]'
			AND it_id = '$_POST[it_id]' ";
	$row = sql_fetch($check);
	if (!$row[id]) {

		$badge = "INSERT INTO ".DB_BADGES."
				SET mb_no			= '$_SESSION[user_no]',
					category		= '3',
					od_id			= '$_POST[via]',
					it_id			= '$_POST[it_id]',
					param			= '$_POST[post_id]' ";
		$result = mysql_query($badge);

		if ($result) {
			print "0000:$_POST[via]:$_POST[it_id]"; // badge issue successful
			exit;
		}
	}
	else {
		print '0001'; // already earned badge
		exit;
	}
}
else {
	print '0008'; // not logged in user
	exit;
}
print '0009'; // unknown error
?>