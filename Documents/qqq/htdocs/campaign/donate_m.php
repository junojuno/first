<?
	if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;
	
	$_loginrequired = true;
	$_required = true;
	include '../config.php';
	
	if(!$isMobile) {
		header("Location: /campaign/{$_GET[it_id]}/donate");
		exit;
	}
	include '../module/_head.php';
	
	/* kcp와 통신후 kcp 서버에서 전송되는 결제 요청 정보 */
	encoding("euc-kr", "UTF-8", &$_POST); // kcp 에서 euc-kr로 받은걸 다시 utf-8로 변환
	
	$req_tx          = $_POST[ "req_tx"         ]; // 요청 종류
	$res_cd          = $_POST[ "res_cd"         ]; // 응답 코드
	
	if(isset($_POST["res_cd"]) && $_POST["res_cd"] != "3000" && $_POST["res_cd"] != "3001") {
		$isKCP = true;
	} else {
		$isKCP = false;
	}
	
	$tran_cd         = $_POST[ "tran_cd"        ]; // 트랜잭션 코드
	$ordr_idxx       = $_POST[ "ordr_idxx"      ]; // 쇼핑몰 주문번호
	$good_name       = $_POST[ "good_name"      ]; // 상품명
	$good_mny        = $_POST[ "good_mny"       ]; // 결제 총금액
	$buyr_name       = $_POST[ "buyr_name"      ]; // 주문자명
	$buyr_tel1       = $_POST[ "buyr_tel1"      ]; // 주문자 전화번호
	$buyr_tel2       = $_POST[ "buyr_tel2"      ]; // 주문자 핸드폰 번호
	$buyr_mail       = $_POST[ "buyr_mail"      ]; // 주문자 E-mail 주소
	$use_pay_method  = $_POST[ "use_pay_method" ]; // 결제 방법
	$enc_info        = iconv("UTF-8","euc-kr",$_POST[ "enc_info"       ]); // 암호화 정보
	$enc_data        = iconv("UTF-8","euc-kr",$_POST[ "enc_data"       ]); // 암호화 데이터
	
	/*
	 * 기타 파라메터 추가 부분 - Start -
	*/
	$param_opt_1     = $_POST[ "param_opt_1"    ]; // 주소 data 
	$param_opt_2     = $_POST[ "param_opt_2"    ]; // 이벤트 data
	$param_opt_3     = $_POST[ "param_opt_3"    ]; // 영수증 data
	/*
	 * 기타 파라메터 추가 부분 - End -
	*/
	
	if($isKCP) {
		$param_1_arr = explode("|", $param_opt_1);
		$param_2_arr = explode("|", $param_opt_2);
		$param_3_arr = explode("|", $param_opt_3);
	}
	
	$g_conf_site_name	= 'WEGENERATION';
	//$g_conf_key_dir		= 'C:/usr/htdocs/module/kcp/bin/pub.key';
	//$g_conf_log_dir		= 'C:/usr/htdocs/module/kcp/log';
	$g_conf_log_level	= '3';
	$g_conf_gw_port		= '8090';
	$g_conf_mode		= 0;		// 배치결제
	$g_conf_home_dir	= (FALSE) ? 'C:/usr/htdocs/module/kcp' : '/module/kcp_mobile';
	$g_conf_gw_url		= ($_debug) ? 'testpaygw.kcp.co.kr' : 'paygw.kcp.co.kr'; // testpaygw.kcp | paygw.kcp
	$g_conf_js_url		= 'http://pay.kcp.co.kr/plugin/payplus_un.js'; // payplus_test_un.js | payplus_un.js
	
	$g_conf_site_cd		= ($_debug) ? 'T0000' : 'N4434';
	$g_conf_site_key	= ($_debug) ? '3grptw1.zW0GSo4PQdaGvsF__' : '4cCKT.BIpikzV9J0qLdYJFK__';
	
	$g_conf_batch_cd	= ($_debug) ? 'P0010' : 'E0326';
	$g_conf_batch_key	= ($_debug) ? '3JX4soqiYo928vH6tJHm4Dl__' : '1YYjS3R4hTlagkqHNU4kdvN__';
	
	$g_wsdl           = ($_debug) ? "KCPPaymentService.wsdl" : "real_KCPPaymentService.wsdl";
	
	$tablet_size      = "1.0";
	
	$row = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS." WHERE it_id='$_GET[it_id]' AND type = '1' ");
	
	if(!$row[it_id]) { alert("캠페인 정보가 없습니다.","$g4[path]"); }
	if ($row[it_isEnd] == '1') { alert("이미 종료된 캠페인입니다."); }
	
	$it_opt1 = explode('|', $row[it_currency]);
	$totalopt = count($it_opt1)-1;
	
	// ACTIVITY LOG
	if ($_SESSION[user_no]) {
		mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'donate', param2 = '$row[it_id]', referer = '$_SERVER[HTTP_REFERER]' ");
	}
?>
<script type="text/javascript" src="/module/kcp_mobile/approval_key.js"></script>
<script>
	
    /* kcp web 결제창 호출 (변경불가)*/
    function call_pay_form()
    {
       var v_frm = document.donationForm;
       var charset = document.charset;
       document.charset="euc-kr";

        layer_cont_obj   = document.getElementById("content");
        layer_pay_obj = document.getElementById("layer_pay");

        document.getElementById("fb-root").style.display="none";
		document.getElementById("header").style.display = "none";
		document.getElementById("menu").style.display="none";
		document.getElementById("highlight").style.display="none";
		document.getElementById("footerwrap").style.display="none";
		document.getElementById("copyrightwrap").style.display="none";
        layer_cont_obj.style.display = "none";
    	$('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>');
        layer_pay_obj.style.display = "block";

        v_frm.target = "frm_pay";
        v_frm.action = PayUrl;
        
		if(v_frm.Ret_URL.value == "")
		{
			/* Ret_URL값은 현 페이지의 URL 입니다. */
			alert("연동시 Ret_URL을 반드시 설정하셔야 됩니다.");
			return false;
		}
		else
        {
			v_frm.submit();
			document.charset = charset;
		}

    }


    /* kcp 통신을 통해 받은 암호화 정보 체크 후 결제 요청*/
    function chk_pay()
    {
    	/*kcp 결제서버에서 가맹점 주문페이지로 폼값을 보내기위한 설정(변경불가)*/
    	self.name = "tar_opener";

        var pay_form = document.donationForm;

        if (pay_form.res_cd.value == "3001" )
        {
            alert("사용자가 취소하였습니다.");
            pay_form.res_cd.value = "";
            return false;
        }
        else if (pay_form.res_cd.value == "3000" )
        {
            alert("30만원 이상 결제 할수 없습니다.");
            pay_form.res_cd.value = "";
            return false;
        }
        
        if (pay_form.enc_data.value != "" && pay_form.enc_info.value != "" && pay_form.tran_cd.value !="" )
        {
        	$('input[name=it_id]').val("<?=$param_1_arr[0]?>");
            
            $('input[name=od_zip1]').val("<?=$param_1_arr[1]?>");
            $('input[name=od_zip2]').val("<?=$param_1_arr[2]?>");
            $('input[name=od_addr1]').val("<?=$param_1_arr[3]?>");
            $('input[name=od_addr2]').val("<?=$param_1_arr[4]?>");

			var param_2_branch = "<?=$param_2_arr[0]?>";
			if( "1" == param_2_branch ) {
				$('input[name=od_isEvent]').filter("input[value="+"<?=$param_2_arr[1]?>"+"]").attr("checked", "checked");
			} else if( "2" == param_2_branch ) {
				$('input[name=reward_req]').val("<?=$param_2_arr[1]?>");
			} else if( "3" == param_2_branch ) {
				$('input[name=od_isEvent]').filter("input[value="+"<?=$param_2_arr[1]?>"+"]").attr("checked", "checked");
				$('input[name=reward_req]').val("<?=$param_2_arr[2]?>");
			}

            var receipt_val = "<?=$param_3_arr[0]?>";
			$('input[name=isReceipt]').filter("input[value="+receipt_val+"]").attr("checked", "checked");
			
			if( "1" == receipt_val ) {
				$('input[name=realname]').val("<?=$param_3_arr[1]?>");
				$('input[name=serial1]').val("<?=$param_3_arr[2]?>");
				$('input[name=serial2]').val("<?=$param_3_arr[3]?>");
			} else if( "2" == receipt_val) {
				$('input[name=permit1]').val("<?=$param_3_arr[1]?>");
				$('input[name=permit2]').val("<?=$param_3_arr[2]?>");
				$('input[name=permit3]').val("<?=$param_3_arr[3]?>");
				$('input[name=permitimage]').val("<?=$param_3_arr[4]?>");
			}
            
            pay_form.target = "";
            pay_form.action = "/campaign/pp_ax_hub.php";
            $("form#donationForm").attr("enctype", "multipart/form-data");
            pay_form.submit();
        }
        else
        {
             return false;
        }
    }

</script>

<script type='text/javascript' src='/js/zipsearch.js'></script>
<script type='text/javascript'>
$(document).ready(function() {
	//createLayer($('#pluginLayer'));
	//setTimeout('kcp_checkPlugin()', 1000);
	//init_orderid();
	chk_pay();
	$('input[type=text]').addClass('text');

	$('input[name=isReceipt]').change(function() {
		if ($(this).val() == 1) {
			$('.ppForm').show();
			$('.pcForm').hide();
		}
		else if ($(this).val() == 2) {
			$('.ppForm').hide();
			$('.pcForm').show();
		}
		else {
			$('.ppForm, .pcForm').hide();
		}
	});
});

function kcp_checkPlugin() {

	if (navigator.userAgent.indexOf('MSIE') > 0) {
		if ( document.Payplus.object != null ) {
			$('.layer').remove();
		}
	}
	else {
		var inst = 0;
		for (var i = 0; i < navigator.plugins.length; i++) {
			if (navigator.plugins[i].name == 'KCP') {
				inst = 1;
				break;
			}		
		}

		if (inst == 1) {
			$('.layer').remove();
		}
		else {
			//no redirect install file
			$('.layer').remove();
			//document.location.href=GetInstallFile();
		}
	}
}

function kcp_runPlugin(form) {
	var RetVal = false;

	var p = checkRadioValue(document.donationForm.od_price);
	if (!p) {
		qAlert('후원하실 금액을 선택해 주세요.');
		return false;
	}
	if (p == 'user_input') {
		p = $.trim($('input[name=user_input]').val());
		if (!p || parseInt(p) == 0) {
			qAlert('후원을 희망하는 금액을 입력해 주세요.');
			return false;
		}
		else if (p < 1000) {
			qAlert('후원은 최소 1,000원 이상부터 가능합니다.');
			return false;
		}
		
	}
	$('input[name=good_mny]').val(p);

	var m = checkRadioValue(document.donationForm.od_method);
	if (!m) {
		qAlert('결제방법을 선택해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=buyr_name]').val())) {
		qAlert('후원자 이름을 입력해 주세요.');
		$('input[name=buyr_name]').focus();
		return false;
	}

	var hps = ['input[name=od_hp1]', 'input[name=od_hp2]', 'input[name=od_hp3]'];
	for (var i = 0; i < 3; i++)	{
		if (!$.trim($(hps[i]).val())) {
			qAlert('후원자 연락처를 입력해 주세요.');
			$(hps[i]).focus();
			return false;
		}
	}
	$('input[name=buyr_tel1]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());

	if (!$.trim($('input[name=buyr_mail]').val())) {
		qAlert('후원자 이메일 주소를 입력해 주세요.');
		$('input[name=buyr_mail]').focus();
		return false;
	}

	if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=buyr_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('input[name=od_zip1]').val())) {
		qAlert('우편번호 검색창을 통해 주소를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=od_addr2]').val()) || $('input[name=od_addr2]').val() == '상세주소 입력') {
		qAlert('상세주소를 입력해 주세요.');
		return false;
	}
	var param_1_data = "<?=$row[it_id]?>"+ "|" + 
				$('input[name=od_zip1]').val() + "|" + $('input[name=od_zip2]').val() + "|" +
				$('input[name=od_addr1]').val() + "|" + $('input[name=od_addr2]').val();
	$("input[name=param_opt_1]").val(param_1_data);

	var param_2_data = 0;
<?
$sql = "SELECT * FROM ".DB_FUNDRAISERS."
		WHERE it_id = '$row[it_id]'
		";
$result = sql_query($sql);
$total = mysql_num_rows($result);
if ($total > 0) {
?>
	if (!checkRadioValue(document.donationForm.od_isEvent)) {
		qAlert('이벤트 참여 여부를 선택해 주세요.');
		return false;
	}
	param_2_data = 1;
<? } ?>
	if($('input[name=reward_req]').val()) {
		param_2_data = param_2_data == 1 ? 3 : 2;
	}

	if(param_2_data == 1) {
		param_2_data += "|" + $("input[name=od_isEvent]").val();
	} else if( param_2_data == 2 ) { 
		param_2_data += "|" + $('input[name=reward_req]').val();
	} else if( param_2_data == 3 ) {
		param_2_data += "|" + $("input[name=od_isEvent]").val() + "|" + $('input[name=reward_req]').val();
	}
	$("input[name=param_opt_2]").val(param_2_data);
	
<? if ($row[it_isReceipt] == 1) : ?>
	var isReceipt = checkRadioValue(document.donationForm.isReceipt);
	var receipt_data = isReceipt + "|";
	if (isReceipt == 1) {
		if (!$.trim($('input[name=realname]').val())) {
			qAlert('영수증 발행을 위해 실명을 입력해 주세요.');
			return false;
		}
		if (!$.trim($('input[name=serial1]').val()) || !$.trim($('input[name=serial2]').val())) {
			qAlert('영수증 발행을 위해 주민등록번호를 입력해 주세요.');
			return false;
		}
		receipt_data += $.trim($('input[name=realname]').val()) + "|" + $.trim($('input[name=serial1]').val()) + "|"
					+ $.trim($('input[name=serial2]').val());
	}
	if (isReceipt == 2) {

		if (!$.trim($('input[name=permit1]').val()) || !$.trim($('input[name=permit2]').val()) || !$.trim($('input[name=permit3]').val())) {
			qAlert('영수증 발행을 위해 사업자등록번호를 입력해 주세요.');
			return false;
		}

		var ext = $('input[name=permitimage]').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
			qAlert('사업자등록증 이미지가 올바른 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
			return false;
		}
		receipt_data += $.trim($('input[name=permit1]').val()) + "|" + $.trim($('input[name=permit2]').val()) + "|" 
					+ $.trim($('input[name=permit3]').val()) + "|" + $('input[name=permitimage]').val();
	}
	
	$("input[name=param_opt_3]").val(receipt_data);
<? endif; ?>
	if(m == "100000000000") { //card
		$('input[name=pay_method]').val('CARD');
		$('input[name=ActionResult]').val('card');
		RetVal = true;
	} else if(m == "010000000000") { //acc
		$('input[name=pay_method]').val('BANK');
		$('input[name=ActionResult]').val('acnt');
		RetVal = true;
	} else if(m == "000010000000") { //mob
		$('input[name=pay_method]').val('MOBX');
		$('input[name=ActionResult]').val('mobx');
		RetVal = true;
	}
	$('input[name=use_pay_method]').val(m);

	kcp_AJAX();
	RetVal = true;
	/*if (MakePayMessage(form) == true) {
		RetVal = true;
	}
	else {
		// res_cd= 오류코드, res_msg=오류메시지
		res_cd  = document.donationForm.res_cd.value;
		res_msg = document.donationForm.res_msg.value;
	}*/
	
	return RetVal ;
}
</script>

<!-- 
<div id='pluginLayer'>
<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
<p>결제 플러그인 설치 여부를 확인하는 중입니다.</p>
브라우저 상단에 노란 알림표시줄이 나타날 경우<br/>
<span style='font-weight: bold; color: white'>"ActiveX 컨트롤 설치"</span>를 선택해 주십시오.
</div>
-->

<div id="layer_pay" style="z-index:1; max-width=100%; display:none;">
    <table border="0" cellspacing="0" cellpadding="0" style="position:releative; margin:30px 0 auto; text-align:center; width:100%; height:100%;">
        <tr>
            <td>
                <iframe name="frm_pay"  frameborder="0" border="0" width="310" height="480" scrolling="auto"></iframe>
            </td>
        </tr>
    </table>
</div>

<div id='highlight'>&nbsp;
	
	<div id='content' class='solid'>

		<div class='inner donation'>
	
			<h2 style='margin-bottom: 14px; border: none'>후원하기 (모바일)</h2>
		
			<form name='donationForm' method='post'> <!--  -->
		
				<!-- left -->
				<div id='donationLeft' style='float: left; width: 48%; margin-right: 2%'>
			
					<h3>후원금액</h3>
					<ul id='donationPrice'>
						<?
						for ($i = 0; $i < count($it_opt1); $i++) {
							$it_opt = explode(';', $it_opt1[$i]);
							$it_opt = $it_opt[0];
							if ($it_opt != 'user_input') {
						?>
						<li><input type='radio' name='od_price' value='<?=$it_opt?>' id='p<?=$i?>' />
						<label for='p<?=$i?>'><?=number_format($it_opt)?>원</label></li>
						<? }} ?>
						<li><input type='radio' name='od_price' value='user_input' />
						직접입력
						<input type='text' name='user_input' onfocus="$('input[name=od_price]:last').prop('checked',true)" style='text-align: right' class='numonly' /> 원</li>
					</ul>
			
					<h3 style='margin-top: 20px'>결제방법</h3>
					
					<ul id='donationMethod'>
						<li><input type='radio' name='od_method' value='000010000000' id='mo' checked /><label for='mo' style="margin-left:3px">휴대폰</label> (추천)</li>
						<li><input type='radio' name='od_method' value='100000000000' id='cd' /><label for='cd' style="margin-left:3px">신용카드</label> (모바일ISP 혹은 PayPin 앱 설치과정이 있을 수 있습니다.)</li>
						<!-- <li><input type='radio' name='od_method' value='010000000000' id='ac' /><label for='ac'>계좌이체</label></li> -->
						
					</ul>
					<div style='clear: both'></div>
			
				<?
				$rewards = explode('||', $row[it_reward]);
			//	if (count($rewards) > 1) :
				if ($row[it_reward_fromer]) :
				?>
				<h3 style='margin-top: 20px'>리워드 선택</h3>
			
					<table cellpadding='0' cellpadding='0' style='width: 100%'>
						<colgroup>
							<col width='120' />
							<col width='/' />
						</colgroup>
						<tr><th>요청사항</th><td>
						<input type='text' name='reward_req' style='width: 250px' /></td></tr>
					</table>
			
					<p><?=nl2br($row[it_reward_fromer])?></p>
					
					<?
					// Added by 2013.08.07
					$additional_reward_image = "/data/campaign/{$_GET['it_id']}/reward.jpg";
					if(file_exists("..{$additional_reward_image}")) {
						echo "<img src='{$additional_reward_image}' style='width:auto; height:auto;'>";
					}
					?>
				<div style='clear: both'></div>
				<? endif; ?>
			
					<?
					include 'donate_eventSelector.php';
					?>
				</div>
				<!-- /left -->
			
				<!-- right -->
				<div style='float: left; width: 48%; margin-left: 2%'>
					<h3>개인정보 입력</h3>
					<?
					$info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
					$contact = explode('-', $info[mb_contact]);
					?>
					<table cellpadding='0' cellpadding='0' style='width: 100%'>
						<colgroup>
							<col width='120' />
							<col width='/' />
						</colgroup>
						<tr><th>이름</th><td>
						<input type='text' name='buyr_name' value="<?= $isKCP ? $buyr_name : $_SESSION[username]?>" /></td></tr>
						<tr><th>전화번호</th><td>
						<input type='text' name="od_hp1" value="<?=$contact[0]?>" size='3' maxlength='3' class='numonly' onKeyUp='moveFocus(3,this.form, od_hp2);' /> -
						<input type='text' name="od_hp2" value="<?=$contact[1]?>" size='4' maxlength='4' class='numonly' onKeyUp='moveFocus(3,this.form, od_hp3);'/> -
						<input type='text' name="od_hp3" value="<?=$contact[2]?>" size='4' maxlength='4' class='numonly' /></td></tr>
						<tr><th>이메일</th><td>
						<input type='text' name='buyr_mail' value='<?= $isKCP ? $buyr_mail : $info[mb_email]?>' maxlength='50' style='width: 250px' /></td></tr>
						<tr style='height: 120px'><th>주소</th><td>
			
						<div class='zipcode-finder'>
							<input type='text' id="dongName" />
							<input type='button' class='zipcode-search' value='검색' />
							<div class="zipcode-search-result"></div>
						</div>
						<div id='addr'>
							<input type='text' name="od_zip1" value="<?=$info[mb_zip1]?>" size='3' maxlength='3' readonly /> -
							<input type='text' name="od_zip2" value="<?=$info[mb_zip2]?>" size='3' maxlength='3' readonly />
							<input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 250px' readonly /><br/>
							<input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
						</div>
			
						</td></tr>
					</table>
			
					<h3 style='margin-top: 20px'>기부금 영수증 신청</h3>
					<? if ($row[it_isReceipt] == 1) : ?>
					<ul id='donationReceipt'>
						<li><input type='radio' name='isReceipt' value='0' id='pn' checked /><label for='pn'>미신청</label></li>
						<li><input type='radio' name='isReceipt' value='1' id='pp' /><label for='pp'>개인</label></li>
						<li><input type='radio' name='isReceipt' value='2' id='pc' /><label for='pc'>사업자</label></li>
					</ul>
					
					<div style='clear: both'></div>
			
					<table cellpadding='0' cellpadding='0' style='width: 100%'>
						<colgroup>
							<col width='120' />
							<col width='/' />
						</colgroup>
						<tr class='ppForm' style='display: none'><th>실명</th><td>
						<input type='text' name='realname' maxlength='10' /></td></tr>
						<tr class='ppForm' style='display: none'><th>주민등록번호</th><td>
						<input type='text' name='serial1' maxlength='6' style='width: 50px;' class='numonly' /> - <input type='text' name='serial2' maxlength='7' class='numonly' style='width: 50px;' /></td></tr>
						<tr class='pcForm' style='display: none'><th>사업자등록번호</th><td>
						<input type='text' name='permit1' maxlength='3' style='width: 30px;' class='numonly'/> - <input type='text' name='permit2' maxlength='2' class='numonly' style='width: 20px;' class='numonly'/> - <input type='text' name='permit3' maxlength='5' class='numonly' style='width: 50px;' /></td></tr>
						<tr class='pcForm' style='display: none'><th>등록증 사본</th><td>
						<input type='file' name='permitimage' style='width: 100%; text-align: center' /></td></tr>
					</table>
			
					<p>영수증 발행은 해당 캠페인의 지원 단체에 요청되며, 기부금 영수증은 캠페인이 완료된 후 해당 단체를 통해 일괄 발송됩니다.<br/>
					입력하신 회원님의 실명 정보와 주민등록번호는 영수증 발행 이의외 목적으로는 사용되지 않으며, 목적 달성 즉시 파기됩니다. 입력하신 정보가 정확하지 않을 경우 영수증 발급이 어려울 수 있습니다.</p>
			
					<? else : ?>
					<ul id='donationReceipt'>
						<li style='width: 100%; color: silver'>본 캠페인은 기부금 영수증 발급이 불가능합니다.</li>
					</ul>
					<div style='clear: both'></div>
					<? endif; ?>
			
					<div style='text-align: center;  margin: 0px auto'>
						<p>입력하신 개인정보는 캠페인 진행 상황 및 이벤트 당첨 알림, 리워드 발송 등의 목적으로만<br/>
						사용되며, <a href='/privacy-policy'><strong>개인정보 취급방침</strong></a>에 의거해 보호됩니다.</p>
						<!--	<input type='checkbox' name='saveInfo' checked />
								위 정보를 앞으로 후원시에도 자동으로 사용하겠습니다.
						-->
						<input type='button' value='후원하기' class='submit' onclick="kcp_runPlugin(this.form);" />
					</div>
				</div>
				<!-- /right -->
				<div style='clear: both'></div>
			
				<input type='hidden' name='ordr_idxx'		value='<?= $isKCP ? $ordr_idxx : date('ymdHis').rand(100, 999)?>' />
				
				<input type='hidden' name='it_id'			value='<?=$row[it_id]?>' />
				
				<input type='hidden' name='pay_method'		value='' />
				
				<input type='hidden' name='buyr_tel1'		value='<?= $isKCP ? $buyr_tel1 : ""; ?> '/>
				<input type='hidden' name='buyr_tel2'		value='<?= $isKCP ? $buyr_tel2 : ""; ?>' />
				
				<input type='hidden' name='good_name'		value='<?= $isKCP ? $good_name : $row[it_name];?>' />
				<input type='hidden' name='good_mny'		value='<?= $isKCP ? $good_mny : "" ;?>'/>
				
				<input type='hidden' name='req_tx'			value='pay' />
				
				<input type='hidden' name='site_cd'			value="<?=$g_conf_site_cd?>" />
				
				<input type='hidden' name='site_key'		value="<?=$g_conf_site_key?>" />
				<input type='hidden' name='shop_name'		value="<?=$g_conf_site_name?>" />
				<input type='hidden' name='quotaopt'		value='12' />
				<input type='hidden' name='currency'		value='410'/>
				<input type="hidden" name='Ret_URL'      	value="http://wegen.kr/campaign/<?=$row[it_id]?>/donate_m">
				<input type="hidden" name='param_opt_1'	 value="<?=$param_opt_1?>"/>
				<input type="hidden" name='param_opt_2'	 value="<?=$param_opt_2?>"/>
				<input type="hidden" name='param_opt_3'	 value="<?=$param_opt_3?>"/>
				<input type="hidden" name='escw_used'    value="N">
				<input type='hidden' name='connect_type'	value='MOBILE' />
				
				
				<!-- PLUGIN 설정 (변경 불가) -->
				<input type='hidden' name='ActionResult' 	value=''>
				<input type="hidden" name='approval_key' 	id="approval">
				<input type='hidden' name='module_type'		value='01' />
				<input type='hidden' name='epnt_issu'		value='' />
				<input type='hidden' name='res_cd'			value='<?=$res_cd?>'/>
				<input type='hidden' name='res_msg'			value='' />
				<input type='hidden' name='tno'				value='' />
				<input type='hidden' name='trace_no'		value='' />
				<input type='hidden' name='enc_info'		value='<?=$enc_info?>' />
				<input type='hidden' name='enc_data'		value='<?=$enc_data?>' />
				<input type='hidden' name='ret_pay_method'	value='' />
				<input type='hidden' name='tran_cd'			value='<?=$tran_cd?>' />
				<input type='hidden' name='bank_name'		value='' />
				<input type='hidden' name='bank_issu'		value='' />
				<input type='hidden' name='use_pay_method'	value='<?=$use_pay_method?>' />
				<input type='hidden' name='cash_tsdtime'	value='' />
				<input type='hidden' name='cash_yn'			value='' />
				<input type='hidden' name='cash_authno'		value='' />
				<input type='hidden' name='cash_tr_code'	value='' />
				<input type='hidden' name='cash_id_info'	value='' />
				
				<!-- 제공 기간 설정 0:일회성 1:기간설정(ex 1:2012010120120131) -->
				<input type='hidden' name='good_expr'		value='0' />
				<input type='hidden' name='site_logo'		value='' />
				<input type='hidden' name='skin_indx'		value='1' />
				<?
				/*
				신용카드사 삭제
				<input type='hidden' name="not_used_card" value="CCPH:CCSS:CCKE:CCHM:CCSH:CCLO:CCLG:CCJB:CCHN:CCCH"/>
				사용카드 설정 여부 (통합결제창 노출 유무)
				<input type='hidden' name="used_card_YN"value="Y"/>
				사용카드 설정 (해당 카드만 결제창에 보이게 설정. used_card_YN 값이 Y일때 적용)
				<input type='hidden' name="used_card"value="CCBC:CCKM:CCSS"/>					
				해외카드 구분 (해외비자, 해외마스터, 해외JCB로 구분하여 표시)
				<input type='hidden' name="used_card_CCXX"value="Y"/>  
				결제창 영문 표시
				<input type='hidden' name='eng_flag'  value='Y'>
				현금영수증 등록창 출력 여부
				현금영수증 사용시 KCP 상점관리자 페이지에서 현금영수증 사용 동의 필요
				*/
				?>
				<input type='hidden' name='disp_tax_yn'		value='N' />
				<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
				<input type='hidden' name='token'			value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
			</form>
		</div>
	</div>&nbsp;
</div>

<?
include '../module/_tail.php';
?>