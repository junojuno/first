<?
$_required = true;
include '../config.php';
include '../module/_head.php';

$sort = explode(',', $_SERVER[QUERY_STRING]);
$sort[1] = $sort[1] ? $sort[1] : 1;

if ($sort[0] == 'ongoing') {
	$where = 'it_isEnd != 1';
	$eq_target = 'first';
	$eq = 1;
}
else if ($sort[0] == 'success') {
	$where = 'it_isEnd = 1 AND it_isMain = 1';
	$eq_target = 'first';
	$eq = 2;
}
else {
	$categories = array('', '역사/문화','환경/동물보호','아동/청소년','노인','장애인','저소득가정','다문화가정','자활형 캠페인');
	$where = $sort[0] ? "category LIKE '%".$categories[$sort[0]]."%'" : false;
	$eq_target = $sort[0] ? 'last' : 'first';
	$eq = $sort[0] ? $sort[0] - 1 : 0;
}

$sorts = array('it_id DESC', 'it_id DESC', 'it_funded DESC', 'it_funded ASC', 'it_funded DESC', 'it_funded ASC');
if (preg_match("/^[0-9]$/", $sort[1])) {
	$orderBy = $sorts[$sort[1]];
	$orderEq = $sort[1];
}

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'campaign', referer = '$_SERVER[HTTP_REFERER]' ");
}

?>

<? include '../module/coin.php'; ?>

<div id='highlight'>&nbsp;
<div class='wrap'>

	<div style='float: left; width: 125px; margin-left: 50px'>

		<h2>캠페인 보기</h2>
		<menu class='leftmenu' style='margin-left: 10px'>
			<a href='/campaign'><li>* 전체 캠페인</li></a>
			<a href='/campaign/?ongoing'><li>* 진행중 캠페인</li></a>
			<a href='/campaign/?success'><li>* 성공 캠페인</li></a>
		</menu>

		<h2 style='margin-top: 20px'>카테고리</h2>
		<menu class='leftmenu' style='margin-left: 10px'>
			<a href='/campaign/?1'><li>* 역사/문화</li></a>
			<a href='/campaign/?2'><li>* 환경/동물보호</li></a>
			<a href='/campaign/?3'><li>* 아동/청소년</li></a>
			<a href='/campaign/?4'><li>* 노인</li></a>
			<a href='/campaign/?5'><li>* 장애인</li></a>
			<a href='/campaign/?6'><li>* 저소득가정</li></a>
			<a href='/campaign/?7'><li>* 다문화가정</li></a>
			<a href='/campaign/?8'><li>* 자활형 캠페인</li></a>
		</menu>
	</div>

	<? include '../module/list.banner.php'; ?>
	<div style='clear: both'></div>

</div>&nbsp;
</div>

<div id='content'>

	<div style='margin: 20px 0px; text-align: right'>
	<select id='orderBy' class='text' style='float: left'>
		<option value=''>출력 순서</option>
		<option value='1'>최신일자</option>
		<option value='2'>많은 후원자수</option>
		<option value='3'>적은 후원자수</option>
		<option value='4'>많은 후원금액</option>
		<option value='5'>적은 후원금액</option>
	</select>
	<input type='text' name='search' class='text' style='width: 150px; height: 18px' />
	<button class='btn_search' style='cursor: pointer; height: 20px; background-color: black; color: white; font: 9pt NanumGothic; outline: none; border: none'>검색</button>
	</div>

	<?
	$listType	= 'done';
	$rows		= 999;
	//$where		= 'type < 100';
//	$orderBy	= 'it_id desc';

	include '../module/list.campaign.php';
	?>
</div>

<script>
$(document).ready(function() {
	$('#highlight menu:<?=$eq_target?> li').eq(<?=$eq?>).addClass('on');
	<? if ($orderEq) { ?>$('#orderBy option').eq(<?=$orderEq?>).prop('selected', 'selected');<? } ?>

	addLabel('input[name=search]', '캠페인명으로 검색');

	$('#orderBy').change(function() {
		if ($(this).val()) {
			document.location.href = './?<?=$sort[0]?>,' + $(this).val();
		}
	});
	$('.btn_search').click(function() {
		var str = $('input[name=search]').val();
		if (!str || str == '캠페인명으로 검색') {
			qAlert('검색어를 입력해주세요.');
			return false;
		}

		$('.campaignTitle').each(function() {
			if ($(this).text().indexOf(str) == -1) {
				$(this).parent().closest('li').hide();
			}
			else $(this).parent().closest('li').show();
		});
	});
});
</script>

<?
include '../module/_tail.php';
?>
