<?
$_required = true;
include '../config.php';
include '../module/_head.php';

$it_id			  = 1366196183;
$site_cd          = $_POST[ "site_cd"        ];      // 사이트코드
$req_tx           = 'pay';      // 요청 구분(승인/취소)
$use_pay_method   = "100000000000";      // 사용 결제 수단
$bSucc            = 'true';      // 업체 DB 정상처리 완료 여부
/* = -------------------------------------------------------------------------- = */
$res_cd           = '0000';      // 결과코드
$res_msg          = '정상처리';      // 결과메시지
$res_msg_bsucc    = "";
/* = -------------------------------------------------------------------------- = */
$ordr_idxx        = $_POST[ "ordr_idxx"      ];      // 주문번호
$tno              = $_POST[ "tno"            ];      // KCP 거래번호
$good_mny         = $_POST[ "good_mny"       ];      // 결제금액
$good_name        = $_POST[ "good_name"      ];      // 상품명
$buyr_name        = $_POST[ "buyr_name"      ];      // 구매자명
$buyr_tel1        = $_POST[ "buyr_tel1"      ];      // 구매자 전화번호
$buyr_tel2        = $_POST[ "buyr_tel2"      ];      // 구매자 휴대폰번호
$buyr_mail        = $_POST[ "buyr_mail"      ];      // 구매자 E-Mail
/* = -------------------------------------------------------------------------- = */
// 공통
$pnt_issue        = $_POST[ "pnt_issue"      ];      // 포인트 서비스사
$app_time         = $_POST[ "app_time"       ];      // 승인시간 (공통)
/* = -------------------------------------------------------------------------- = */
// 신용카드
$card_cd          = $_POST[ "card_cd"        ];      // 카드코드
$card_name        = $_POST[ "card_name"      ];      // 카드명
$noinf			  = $_POST[ "noinf"          ];      // 무이자 여부
$quota            = $_POST[ "quota"          ];      // 할부개월
$app_no           = $_POST[ "app_no"         ];      // 승인번호
/* = -------------------------------------------------------------------------- = */
// 계좌이체
$bank_name        = $_POST[ "bank_name"      ];      // 은행명
$bank_code        = $_POST[ "bank_code"      ];      // 은행코드
/* = -------------------------------------------------------------------------- = */
// 가상계좌
$bankname         = $_POST[ "bankname"       ];      // 입금할 은행
$depositor        = $_POST[ "depositor"      ];      // 입금할 계좌 예금주
$account          = $_POST[ "account"        ];      // 입금할 계좌 번호
$va_date		  = $_POST[ "va_date"        ];      // 가상계좌 입금마감시간
/* = -------------------------------------------------------------------------- = */
// 포인트
$pt_idno          = $_POST[ "pt_idno"        ];      // 결제 및 인증 아이디
$add_pnt          = $_POST[ "add_pnt"        ];      // 발생 포인트
$use_pnt          = $_POST[ "use_pnt"        ];      // 사용가능 포인트
$rsv_pnt          = $_POST[ "rsv_pnt"        ];      // 총 누적 포인트
$pnt_app_time     = $_POST[ "pnt_app_time"   ];      // 승인시간
$pnt_app_no       = $_POST[ "pnt_app_no"     ];      // 승인번호
$pnt_amount       = $_POST[ "pnt_amount"     ];      // 적립금액 or 사용금액
/* = -------------------------------------------------------------------------- = */
//상품권
$tk_van_code	  = $_POST[ "tk_van_code"    ];      // 발급사 코드
$tk_app_no		  = $_POST[ "tk_app_no"      ];      // 승인 번호
/* = -------------------------------------------------------------------------- = */
//휴대폰
$commid			  = $_POST[ "commid"		 ];      // 통신사 코드
$mobile_no		  = $_POST[ "mobile_no"      ];      // 휴대폰 번호
/* = -------------------------------------------------------------------------- = */
// 현금영수증
$cash_yn          = $_POST[ "cash_yn"        ];      //현금영수증 등록 여부
$cash_authno      = $_POST[ "cash_authno"    ];      //현금영수증 승인 번호
$cash_tr_code     = $_POST[ "cash_tr_code"   ];      //현금영수증 발행 구분
$cash_id_info     = $_POST[ "cash_id_info"   ];      //현금영수증 등록 번호
/* = -------------------------------------------------------------------------- = */

$req_tx_name = "";

if( $req_tx == "pay" ) {
	$req_tx_name = "후원";
}
else if( $req_tx == "mod" ) {
	$req_tx_name = "매입/취소";
}
?>
    
<div id='highlight'>&nbsp;
<div id='content' class='solid'>
	<div class='inner donation'>

	<h2><?=$req_tx_name?> 결과</h2>

		<table cellpadding='0' cellpadding='0' style='width: 500px; margin: 30px auto'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>결과</th><td><?=$res_msg?> (<?=$res_cd?>)</td></tr>

<?
if ($req_tx == "pay") {
	if($bSucc == "false") {
		$res_msg_bsucc = ($res_cd == "0000") ? '결제는 정상적으로 이루어졌으나 결제 결과를 처리하는 중 오류가 발생하여 자동으로 취소 요청 되었습니다.<br/>고객센터로 문의하여 확인하시기 바랍니다.' : '결제는 정상적으로 이루어졌으나 결제 결과를 처리하는 중 오류가 발생하여 자동으로 취소 요청을 했지만, <strong>취소 요청에 실패했습니다.</strong><br/>반드시 고객센터로 문의해주시기 바랍니다.';
		print "<tr><td colspan='2'>$res_msg_bsucc</td></tr></table>";
	}
	else {
		if ($res_cd == "0000") {


			if (strpos($app_time, '-') === false) {
				$app_time = str_split($app_time, 2);
				$app_time = $app_time[0].$app_time[1].'-'.$app_time[2].'-'.$app_time[3].' '.$app_time[4].':'.$app_time[5].':'.$app_time[6];
			}
?>

<!--			<tr><th>후원 번호</th><td><?=$ordr_idxx?></td></tr> -->
			<tr><th>후원 캠페인명</th><td>농구스타 한기범과 함께하는 심장병 환아 치료비 지원 캠페인</td></tr>
			<tr><th>후원 금액</th><td><?=number_format(1000);?>원</td></tr>

<?
			// 카드결제
			if ( $use_pay_method == "100000000000" ) {
?>
			<tr><th>후원 수단</th><td>신용카드 <a href="javascript:receiptView('<?=$tno?>')">[영수증 출력]</a></td></tr>
<!--			<tr><th>승인시간</th><td><?=$app_time?></td></tr>
			<tr><th>결제카드</th><td><?=$card_cd?> / <?=$card_name?></td></tr>
			<tr><th>승인번호</th><td><?=$app_no?></td></tr> -->
<?
			}
			// 계좌이체
			else if ($use_pay_method == "010000000000") {
?>
			<tr><th>후원 수단</th><td>계좌이체</td></tr>
<!--			<tr><th>은행</th><td><?=$bank_name?></td></tr>
			<tr><th>승인시간</th><td><?=$app_time?></td></tr>-->
<?
			}
			//휴대폰
			else if ( $use_pay_method == "000010000000" ) {
?>
			<tr><th>후원 수단</th><td>핸드폰 (<?=$commid?> / <?=$mobile_no?>)</td></tr>
<!--			<tr><th>승인시간</th><td><?=$app_time?></td></tr>
			<tr><th>통신사코드</th><td><?=$commid?></td></tr>
			<tr><th>핸드폰 번호</th><td><?=$mobile_no?></td></tr>-->
<?
			}
			//코인
			else if ( $use_pay_method == 'COIN') {
?>
			<tr><th>후원 수단</th><td>코인</td></tr>
<!--			<tr><th>승인시간</th><td><?=$app_time?></td></tr>-->
<?
			}
		}
?>

		</table>

	<h2>공유하기</h2>
		<div style='text-align: center; margin: 30px 0px'>
		<img src='/images/campaign/result.png' style='margin-bottom: 30px' /><br/>
		<img class='btn_facebook clickable' src='/images/campaign/btn_share_fb.png' />
		<img class='btn_twitter clickable' src='/images/campaign/btn_share_tw.png' />
		</div>

	<h2>응원 코멘트</h2>

		<img src='/images/campaign/wtf.png'/>

		<div id='commentbox1' style='margin-top: 20px'>
			<form name='commentForm' style='margin:0px'>
			<input type='hidden' name='it_id' value='<?=$it_id?>' />
			<input type='hidden' name='category' />
			<input type='hidden' name='viewport' value='result' />
			<div style='float: left; width: 175px; height: 80px; font: 10pt/30px NanumGothicBold'>
			1) 수혜자에게 전하실 말씀<br/>
			<span id='smiley1' style='float: left; display: block; width: 30px; height: 30px; margin-right: 5px; background: url(/images/campaign/icon_smiley.png) top'></span>
			<span id='subtext1' style='font: 9pt/30px NanumGothic'>응원 한마디를 남겨보세요</span>
			</div>
			<div style='float: right; width: 70px'><img class='btn_submitcmt clickable' src='/images/campaign/btn_submitcmt.png' /></div>
			<textarea class='text' index='1' style='margin: 0px; width: 600px; height: 64px'></textarea>
			<div style='clear: both'></div>
		</div>

		<div id='commentbox2' style='margin-top: 20px'>
			<div style='float: left; width: 175px; height: 80px; font: 10pt/30px NanumGothicBold'>
			2) 스타에게 전하실 말씀<br/>
			<span id='smiley2' style='float: left; display: block; width: 30px; height: 30px; margin-right: 5px; background: url(/images/campaign/icon_smiley.png) top'></span>
			<span id='subtext2' style='font: 9pt/30px NanumGothic'>응원 한마디를 남겨보세요</span>
			</div>
			<div style='float: right; width: 70px'><img class='btn_submitcmt clickable' src='/images/campaign/btn_submitcmt.png' /></div>
			<textarea class='text' index='2' style='margin: 0px; width: 600px; height: 64px'></textarea>
			<div style='clear: both'></div>
			</form>
		</div>
<?
	}
}
?>

	</div>
</div>&nbsp;
</div>

<?
$row = sql_fetch("SELECT it_shortdesc FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id' ");
?>
<script type='text/javascript'>
$(document).ready(function() {
	$('textarea').focusout(function() {
		$('textarea').attr('name', '');
		$(this).attr('name', 'cmt');
		$('input[name=category]').val($(this).attr('index'));
	});
});

function postToFeed() {
	var obj = {
		method: 'feed',
		link: 'http://wegen.kr/campaign/<?=$it_id?>',
		picture: 'http://wegen.kr/data/campaign/<?=$it_id?>/list.jpg',
		name: '<?=$_SESSION[username]?>님이 위젠 캠페인에 후원하셨습니다.',
		caption: '스타와 함께 하는 즐거운 기부, 위젠',
		description: '[<?=$good_name?>] <?=$row[it_shortdesc]?>'
	};

	function callback(response) {
		if (response) {
			procShare('<?=$it_id?>', 'facebook', response['post_id']);
		}
	}
	FB.ui(obj, callback);
}

function postToTweet() {
	window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent('http://wegen.kr/campaign/<?=$it_id?>') + '&text=' + encodeURIComponent("위제너레이션에서 진행하는 '<?=$good_name?>'에 후원했습니다."),"twitterPop", 'width=600 height=350');
}

function receiptView(tno) {
	receiptWin = "https://admin8.kcp.co.kr/assist/bill.BillAction.do?cmd=card_bill&c_trade_no=" + tno;
	window.open(receiptWin , "" , "width=470, height=815");
}
</script>

<?
include '../module/_tail.php';
?>