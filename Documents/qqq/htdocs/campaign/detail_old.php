<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;
$it_id = $_GET[it_id];

$_required = true;
$_path = '../';
include '../module/_head.php';

if ($_COOKIE[ck_it_id] != $it_id) {
// 조회수
//	sql_query(" update $settings[db_campaign] set it_hit = it_hit + 1 where it_id = '$it_id' "); // 1증가
//	setcookie("ck_it_id", $it_id, time() + 3600, $config[cf_cookie_dir], $config[cf_cookie_domain]);
}

$sql = "SELECT i.*,
			(select sum(od_amount) 
			from $settings[db_orders] o 
			where o.it_id = i.it_id)
		as it_funded
		from $settings[db_campaign] i
		where i.it_id = '$it_id' ";
$it = sql_fetch($sql);

$status = ($it[it_funded] >= $it[it_amount3]) ? '성공' : '완료';
if ($it[it_use] == 0) {
	$diff = round((strtotime($it[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
	$status = ($diff < 0) ? '완료' : "D-".$diff; 
}

if (!$it[it_id])
    alert("잘못된 접근입니다.");

$ca_id = $it[ca_id];

$it[it_name] = '청각장애아동 인공와우수술비 지원 캠페인';
$it[category] = '카테고리';
?>
<script type="text/javascript">
	var SNS2 = {
	  twitter:function(url,title){
		url=encodeURIComponent(url);
		var d = title;
		window.open('http://twitter.com/share?url=' + url + '&text=' + encodeURIComponent(d),"twitterPop", 'width=600 height=350');
	  },
	  facebook:function(url,title,summary,image){
		var d = encodeURIComponent(title);
		//var image;
		if(image!=""){
		  image = "&p[images][0]="+image;
		}else{
		  image = "";
		}
		window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + encodeURIComponent(url)+"&p[title]="+d+image+"&p[summary]="+encodeURIComponent(summary),"facebookPop",'resizable=no,width=1000,height=600');
	  },
	}

function postToFeed() {
var obj = {
	method: 'feed',
	link: 'http://wegen.kr/campaign/<?=$it_id?>',
	picture: '<?=$g4[thumbnail]?>',
	name: '<?=$it[it_name]?>',
	caption: '위제너레이션 - 우리 세대의 기부방식',
	description: '<?=$g4[desc]?>'
};

function callback(response) {
	if (response) {
	$.ajax({
		type : 'POST',
		asyn : true,
		url : '/campaign/share.php',
		data : {'it_id' : '<?=$it_id?>', 'via' : 'facebook', 'post_id' : response['post_id'] },
		dataType : "text",
		success : function(result)
		{
			switch(result) {
				case "0001" :
					$('#badgeLayer').css('background-image', 'url(/images/common/badges/popup_03.png)');
					$('#layer').show();
			}

			if(g4_is_ie){$('#movieframe').hide()};
			var sharenum = $('#shareFB').html();
			$('#shareFB').html(parseInt(sharenum)+1);
		}
	});
	}
}

FB.ui(obj, callback);
}
</script>

<div id='highlight' class='campaign'>&nbsp;
<h2 style='margin-top: 30px; text-align: center'><?=$it[it_name]?></h2>

	<div id='campaignTab'>
		<menu>
			<li>캠페인 내용</li>
			<li>전달후기</li>
			<li>이벤트후기</li>
		</menu>
		<div style='clear: left'></div>
	</div>
<div id='content' class='solid'>
<div class='inner'>
	<!-- left -->
	<div style='float: left; width: 600px; background-color: black'>
		<h2 style='font: 12pt NanumGothic; color: white; margin: 0px; padding: 10px'></h2>
		<?
		if (file_exists('../../module/custom.'.$it[it_id].'.php')) {
			include '../../module/custom.'.$it[it_id].'.php';
		}
		else if ($it[it_youtube]) {
		?>
		<div id='movieFrame'></div>
		<script type='text/javascript'>
		load('https://www.youtube.com/iframe_api');
		function onYouTubeIframeAPIReady(){var p;p=new YT.Player('movieFrame',{videoId:'<?=$it[it_youtube]?>',width:'600',height:'400',origin:'http://localhost',playerVars:{showinfo: 0}});}
		</script>
		<? } else { ?>
		<img src='/data/campaign/<?=$it[it_id]?>/inner.jpg' style='width: 100%; height: 400px' />
		<? } ?>
	</div>
	<!-- /left -->

	<!-- right -->
	<div style='float: right; width: 300px; background-color: white; border: solid 1px gray; z-index: 2'>
	<div style='padding: 20px'>

		<div style='border-bottom: solid 1px gray; margin-bottom: 20px; padding-bottom: 20px'>
			<h3 style='font: 10pt NanumGothic; margin: 0px; float: left'><?=$it[category]?></h3>
			<div class="fb-like" data-href="http://wegen.kr/<?=$g4[shop]?>/?<?=$it_id?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false" style='float: right; margin-bottom: 10px'></div>
			<div style='clear: both'></div>
			<? drawGauge($it[it_funded], $it[it_target]); ?>
		</div>

		<div style='border-bottom: solid 1px gray; margin-bottom: 20px; padding-bottom: 20px'>
			<h3 class='blind'>협력단체</h3>
			<div style='height: 100px; border: solid 1px #dadada; text-align: center'>
				<img src="/data/campaign/<?=$it[it_id]?>/logo.jpg" />
			</div>
		</div>

		<div style='border-bottom: solid 1px gray; margin-bottom: 20px; padding-bottom: 20px; text-align: center'>
			<h3 class='blind'>공유하기</h3>
			<ul>
				<li id='btn_facebook' style='float: left; width: 33%; height: 40px; text-indent: -5000px; background: url(<?=$g4[abspath]?>/images/campaign/btn_facebook.png) no-repeat center top; cursor: pointer' onclick="postToFeed()">페이스북에 공유하기</li>
				<li id='btn_twitter' style='float: left; width: 33%; height: 40px; text-indent: -5000px; background: url(<?=$g4[abspath]?>/images/campaign/btn_twitter.png) no-repeat center top; cursor: pointer' onclick="SNS2.twitter('http://wegen.kr/<?=$g4[shop]?>/<?=$it_id?>','위젠 - <?=$it[it_name]?>')">트위터에 공유하기</li>
				<a href='#comment'><li id='btn_comment' style='float: left; width: 33%; height: 40px; text-indent: -5000px; background: url(<?=$g4[abspath]?>/images/campaign/btn_comment.png) no-repeat center top' >코멘트</li></a>
			</ul>
			<iframe id='procFrame' frameborder='0' marginwidth='0' marginheight='0' width='0' height='0'></iframe>

			<?
			$fb = "select * from yc4_share where it_id = '$it_id' and via = 'facebook' ";
//			$sharefb = sql_query($fb);
//			$fbNum = mysql_num_rows($sharefb);
			$tw = "select * from yc4_share where it_id = '$it_id' and via = 'twitter' ";
//			$sharetw = sql_query($tw);
//			$twNum = mysql_num_rows($sharetw);
			?>
			<div id='shareFB' style='width: 33%; text-align: center; float: left'><?=$fbNum?></div>
			<div id='shareTW' style='width: 33%; text-align: center; float: left'><?=$twNum?></div>
			<div id='commentNum' style='width: 33%; text-align: center; float: left'></div>
			<div class='cl'></div>
		</div>

	</div>
	</div>
	<!-- /right -->
	<div style='clear: left'></div>
</div>
</div>

<div id='content' style='background-color: transparent'>

	<div id='campaignDetail' style='width: 600px; margin-top: 30px'>
		<div id='it_desc'>
		<?
		$customizedDetail = '../module/detail.'.$it_id.'.php';
		if (file_exists($customizedDetail)) {
			include $customizedDetail;
		}
		?>
		<?=$it[it_desc]?>
		</div>

		<div id='it_postscript'>
		<?=$it[it_postscript]?>
		</div>
	</div>

	<!-- comment -->
	<div id='campaignComment'>
		<menu>
			<li>응원코멘트</li>
			<li>수호천사메세지</li>
		</menu>
		<div style='clear: left'></div>
	</div>

	<!-- comment_form -->
	<div id='commentbox' style='margin-bottom: 20px'>
		<form name="fviewcomment" method="post" action="#" style="margin:0px;">
		<input type='hidden' name='is_depth' value='0' />
		<input type='hidden' name='it_id' value='<?=$it_id?>' />
		<input type='hidden' name='comment_id' />
		<input type='hidden' name='cmt_type' value='0' />
		<input type='hidden' name='cmt_viewport' value='detail' />

		<ul style='list-style: none'>
			<li style='float: left; width: 70px'><img src='<?=$myPortrait?>' style='width: 64px; height: 64px; border: solid 1px gray' /></li>
			<li style='float: left; width: 450px'><textarea name="is_content" id="is_content" style='font-size: 9pt; border: solid 1px #E7E7E7; background-color: #EDEDED; height: 64px; width: 440px' onclick="textarea_check(this)">댓글을 입력하십시오.</textarea></li>
			<li style='float: left; width: 70px'><img class='btn_submit' src="/images/btn_comment.gif" alt="입력" style='margin: 0px; padding: 0px; height: 64px; cursor: pointer' /></li>
		</ul>
		<div style='clear: left'></div>
		</form>
	</div>
	<!-- /comment_form -->

	<?
	$sql = "select * from $settings[db_campaign_cmt] p
			left join $settings[db_member] m on (p.mb_id=m.mb_id) 
			where it_id = '$it_id' 
			order by is_time desc";
	$result = sql_query($sql);
	$commentTotal = mysql_num_rows($result);

	for ($i=0; $row=sql_fetch_array($result); $i++) {
//		$portrait = getPortrait($row[mb_id], $row[mb_icon], $row[tw_icon], $g4[abspath]);
	?>
	<div style='border-top: solid 1px gray; padding: 20px 0px'>
		<ul style='list-style: none'>
		<li style='float: left; width: 70px'><img src='<?=$portrait?>' style='width: 64px; height: 64px; cursor: pointer' onclick="profile_view_c('<?=$row[mb_no]?>')" /></li>
		<li style='float: left; width: 520px; float: left; font: 11px/150% 굴림; color: black'>
			<p>
			<span onclick="profile_view_c('<?=$row[mb_no]?>')" style='cursor: pointer; font-weight: bold'><?=$row[is_name]?></span>
			<span style='color: gray; font: 9px Verdana'><?=$row[is_time]?></span>
			</p>
			<p><? print ($row[is_subject]) ? '['.$row[is_subject].' 응원 메세지] ' : ''; ?><?=$row[is_content]?></p>
			<img src="<?=$g4[abspath]?>/images/btn_cmt.gif" alt="답글달기" style='margin-top: 8px; cursor: pointer' onclick="$('#comment_id').val('<?=$row[is_id]?>'); $('#comment_write').appendTo('#reply_<?=$row[is_id]?>');" />
		</li>
		</ul>
		<div style='clear: left'></div>
	</div>
	<? } ?>
	<div style='border-top: solid 1px gray; clear: left; height: 50px'></div>
	<!-- 코멘트 리스트 -->

</div>


<script type="text/javascript">
$(function() {
	$(".btn_submit").click(function() {
		var is_depth = $('#is_depth').val();
		var it_id = $('#it_id').val();
		var comment_id = $('#comment_id').val();
		var cmt_type = $('#cmt_type').val();
		var cmt_viewport = $('#cmt_viewport').val();
		var is_content = $('#is_content').val();

		var dataString = 'is_depth=' + is_depth + '&it_id=' + it_id + '&comment_id=' + comment_id + '&cmt_type=' + cmt_type + '&cmt_viewport=' + cmt_viewport + '&is_content=' + is_content;
		if(!is_content || is_content == '댓글을 입력하십시오.') {
			alert('댓글 내용을 입력해 주십시오.');
		}
		else {
			$.ajax({
				type: "POST",
				url: "ajaxcomment.php",
				data: dataString,
				cache: false,
				success: function(result){

					if (comment_id) {
						$('#reply_'+comment_id).append(result);
						$('#comment_write').appendTo('#commentbox');
					}
					else {
						var targetDiv = (cmt_type == 1) ? '1004' : 'cmt';
						$('#list_' + targetDiv).prepend(result);
//						$("#list_cmt .reply:first").fadeIn("slow");
					}

					$('#is_content').val('댓글을 입력하십시오.');
				}
			});
		}
		return false;
	});
});
</script>

<?
include '../module/_tail.php';
?>