<?
$_required = true;
include '../config.php';

sec_session_start();

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
	exit;
}
$_SESSION[token] = '';

if (!$_SESSION[user_no]) {
	alert('로그인이 필요한 페이지입니다.');
	exit;
}

$it_id		= $_POST["it_id"];
if (!preg_match("/^[0-9]{10}$/", $it_id)) exit;

$cust_ip	= getenv("REMOTE_ADDR"); // 요청 IP
$ordr_idxx	= $_POST["ordr_idxx"]; // 쇼핑몰 주문번호
if (!preg_match("/^[0-9]{15}$/", $ordr_idxx)) exit;

$buyr_name	= $_POST["buyr_name"]; // 주문자명
$buyr_tel1	= $_POST["buyr_tel1"]; // 주문자 전화번호
$buyr_tel2	= $_POST["buyr_tel2"]; // 주문자 핸드폰 번호
$buyr_mail	= $_POST["buyr_mail"]; // 주문자 E-mail 주소

$good_name  = $_POST["good_name"];
$od_isEvent	= $_POST["od_isEvent"];

$app_time = date('Y-m-d H:i:s');
$serial = $_POST[isReceipt] ? $_POST["serial1"].'-'.$_POST["serial2"] : false;

// 신청여부 체크
$check = sql_fetch("SELECT * FROM wegen_volunteer
					WHERE mb_no = '$_SESSION[user_no]' ");

if ($check[od_id]) {
	alert('이미 자원봉사 참여를 신청하셨습니다.');
	exit;
}

// 처리
$result = mysql_query("INSERT INTO wegen_volunteer
			SET
			od_id		= '$ordr_idxx',
			it_id		= '$it_id',
			mb_no		= '$_SESSION[user_no]',
			od_time		= '$app_time',
			od_ip		= '$cust_ip',
			isReceipt   = '$_POST[isReceipt]',
			receiptName = '$_POST[realname]',
			receiptPno	= '$serial'

");

// 회원정보 업데이트
mysql_query("UPDATE ".DB_MEMBERS."
			SET mb_email	= '$buyr_mail',
				mb_contact	= '$buyr_tel1',
				mb_zip1		= '$_POST[od_zip1]',
				mb_zip2		= '$_POST[od_zip2]',
				mb_addr1	= '$_POST[od_addr1]',
				mb_addr2	= '$_POST[od_addr2]'
			WHERE mb_no = '$_SESSION[user_no]'
			");

// 뱃지 발급 루틴


$result = $result ? "true" : "false";
$bSucc = $result; // DB 작업 실패 또는 금액 불일치의 경우 "false" 로 세팅

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>위제너레이션</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='/css/style.css' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
</head>

<body onload="document.pay_info.submit()">

<script type='text/javascript'>
function noRefresh() {
	if ((event.keyCode == 78) && (event.ctrlKey == true)) {
		event.keyCode = 0;
		return false;
	}
	if(event.keyCode == 116) {
		event.keyCode = 0;
		return false;
	}
}
document.onkeydown = noRefresh;
</script>

<div class='layer'>
	<div id='pluginLayer' class='centering' style='display: block; margin-left: -200px; margin-top: -150px'>
	<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
	<p>요청 처리 중입니다.</p>
	창을 닫거나 새로고침하지 마십시오.
	</div>
</div>

<script>
alert('봉사활동 신청이 성공적으로 완료되었습니다.\n참여해 주셔서 감사합니다!');
document.location.href = '/campaign/<?=$it_id?>';
</script>

</body>
</html>