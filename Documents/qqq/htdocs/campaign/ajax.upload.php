<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//	header("HTTP/1.0 404 Not Found");
//	exit;
}

$_required = true;
include '../config.php';
include '../module/class.upload.php';

sec_session_start();

if(!$_SESSION[connect_it_id]) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$dir = "../data/campaign/$_SESSION[connect_it_id]/connect/";

if ($_FILES['file']) {
	$handle = new upload($_FILES['file']);
	if ($handle->uploaded) {
		$salted = md5(date('YmdHis'));
		$handle->forbidden				= array('application/*');
		$handle->allowed				= array('image/*');
		$handle->image_convert			= 'jpg';
		$handle->file_new_name_body		= $salted;
		$handle->file_overwrite			= true;
		$handle->process($dir);

		if ($handle->processed) {
			$handle->clean();

			$array = array('filelink' => "http://$_SERVER[HTTP_HOST]/data/campaign/$_SESSION[connect_it_id]/connect/".$salted.".jpg");
			echo stripslashes(json_encode($array));   

		} else {
			echo 'error : ' . $handle->error;
		}
	}
}
?>
