<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$_required = true;
include '../config.php';

sec_session_start();

if(!isset($_POST[mbno]) || $_POST[mbno] == '' || !isset($_POST[topPublic]) || $_POST[topPublic] == '') exit;

$res = mysql_query("UPDATE ".DB_MEMBERS." SET mb_topPublic = $_POST[topPublic] WHERE mb_no = $_POST[mbno]");
if($res) {
   print 'success';
   exit;
} else {
   print 'failed';
   exit;
}

?>
