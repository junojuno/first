<?
$_required = true;
include 'config.php';

sec_session_start();

if(isset($_COOKIE[wegen_id]) || isset($_COOKIE[wegen_pw])) {
   unset($_COOKIE[wegen_id]);
   unset($_COOKIE[wegen_pw]);
   setcookie("wegen_id",null,-1,'/');
   setcookie("wegen_pw",null,-1,'/');
}

$_SESSION = array();
$params = session_get_cookie_params();
setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
session_destroy();
header('Location: /');
?>
