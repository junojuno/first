<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}


sec_session_start();

if(!login_check($mysqli)) {
   if(isset($_COOKIE[wegen_id]) && isset($_COOKIE[wegen_pw])) {
      login($_COOKIE[wegen_id],$_COOKIE[wegen_pw],$mysqli);
   }
}

if ($_loginrequired && login_check($mysqli) != true) {
	header("Location: /login.php?url=".urlencode($_SERVER[REQUEST_URI]));
	exit;
}

$settings[type] = ($settings[type]) ? $settings[type] : 'website';
//$settings[url] = ($settings[url]) ? $settings[url] : "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
$settings[url] = ($settings[url]) ? $settings[url] : "http://wegen.kr" . $_SERVER['REQUEST_URI'];
$settings[title_page] = ($settings[title] == '') ? '위제너레이션' : $settings[title].' - 위제너레이션';
$settings[image] = ($settings[image]) ? $settings[image] : 'http://wegen.kr/images/common/logo_facebook.jpg';
$settings[desc] = ($settings[desc]) ? $settings[desc] : '스타와 함께 하는 즐거운 기부, 위제너레이션';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta property="fb:app_id" content="211534425639026" />
<meta property='og:title' content='<?=$settings[title]?>' />
<meta property='og:type' content='<?=$settings[type]?>' />
<meta property='og:url' content='<?=$settings[url]?>' />
<meta property='og:image' content='<?=$settings[image]?>' />
<meta property='og:description' content='<?=$settings[desc]?>' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title><?=$settings[title_page]?></title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.0' /> 
<script type='text/javascript' src='/js/scripts.js'></script>

<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />

<script type='text/javascript'>
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-34058829-1']);
_gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

$(document).ready(function() {

<?
if ($_SESSION[user_no]) {
	$sql = "SELECT * FROM ".DB_BADGES."
			WHERE mb_no = '$_SESSION[user_no]'
			AND isChecked = '0'
			ORDER BY id ASC ";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	if ($total > 0) {
		$badges = array(
					1 => array('개척자', '캠페인에 첫 번째로 후원해주셔서'),
					2 => array('종결자', '캠페인에 마지막으로 후원해주셔서'),
					3 => array('메신저', '캠페인을 SNS로 공유해주셔서'),
					4 => array('히어로', '캠페인에 최고액을 후원해주셔서'),
					5 => array('럭키가이/럭키걸', '펀드레이저 이벤트에 당첨되셔서'),
					6 => array('홈런', '캠페인 목표 모금액 달성 후 초과 후원해주셔서'),
					7 => array('100% 달성', '후원해주신 캠페인이 목표 모금액을 달성하여'),
					8 => array('VIP', '총 후원액이 100만원을 넘어'),
					9 => array('PERFECT', '후원 내역을 공유하고 응원코멘트를 모두 작성하여'),
					90 => array('GIVEx', '정기후원을 서약하여'),
					91 => array('First 300', '정기후원을 서약하신 최초 300분 내에 들어'),
					101 => array('캠페인 후원: 역사/문화', ''),
					102 => array('캠페인 후원: 환경/동물보호', ''),
					103 => array('캠페인 후원: 아동/청소년', ''),
					104 => array('캠페인 후원: 노인', ''),
					105 => array('캠페인 후원: 장애인', ''),
					106 => array('캠페인 후원: 저소득가정', ''),
					107 => array('캠페인 후원: 다문화가정', ''),
					108 => array('캠페인 후원: 자활', '')
				);
		for ($i = 0; $row = sql_fetch_array($result); $i++) {
			$cat = $row[category];
			$sharebtn = ($row[category] < 100) ? "<button class='btn_sharebadge' badgecat='$cat'>공유하기</button>" : false;

			$badges[$cat][1] .= $badges[$cat][1] ? '<br/>' : false;

			if ($i == 0) {
?>
	qGrowl("<div class='grantBadge' category='<?=$cat?>' param1='<?=$row[od_id]?>' param2='<?=$row[it_id]?>'><img src='/images/common/badges/<?=$cat?>_on.png'/></div><p>축하합니다!</p><p><?=$badges[$cat][1]?>&quot;<strong><?=$badges[$cat][0]?></strong>&quot; 뱃지를 획득하셨습니다</p><p style='text-align: center'><?=$sharebtn?><button class='ok'>확인</button></p>", '뱃지 획득 알림', true);

<?			} else { ?>
	setTimeout(function() {
		qGrowl("<div class='grantBadge' category='<?=$cat?>' param1='<?=$row[od_id]?>' param2='<?=$row[it_id]?>'><img src='/images/common/badges/<?=$cat?>_on.png'/></div><p>축하합니다!</p><p><?=$badges[$cat][1]?>&quot;<strong><?=$badges[$cat][0]?></strong>&quot; 뱃지를 획득하셨습니다</p><p style='text-align: center'><?=$sharebtn?><button class='ok'>확인</button></p>", '뱃지 획득 알림', true);
	}, <?=$i*250?>);
<?
			}
		}
	}
}
else {
?>
	addLabel('input[name=uid]', '이메일 주소');
	addLabel('input[name=password_pseudo]', '비밀번호','password');

<? } ?>
});
</script>
</head>

<body>
<div id='fb-root'></div>

<!-- header -->
<div id='header'>
	<input type="hidden" name="ismobile" value="<?=$isMobile ? '1' : '0'?>"/>
	<h1><a href='/'><img src='/images/common/logo.png' alt='WEGENERATION' style='height: 30px' /></a>
	<img src='/images/common/logo_sub.png' alt='스타와 함께 하는 즐거운 기부, 위젠' style='height: 30px' /></h1>

	<div style='text-align: right; line-height: 60px<? print ($_hidestatus)?'; display: none' :''; ?>'>
	<? if (login_check($mysqli) == true) { ?>
	<strong id='userName'><?=$_SESSION['username']?></strong>님
	<?
		$coin = getCoin($_SESSION[user_no]);
		if ($coin !== '0') {
	?>
	<a href='/my/coin'><img src='/images/common/coin.png' style='width: 16px; height: 16px; margin-bottom: 2px; vertical-align: middle' />
	<span style='font: bold 10pt Arial'><?=number_format($coin);?></span></a> 
	<?
		}
		if (array_search($_SESSION[user_no], $settings[btb]) !== false) {
	?>
	| <a href='<?=$settings[btb_url][$_SESSION[user_no]]?>'>관리자페이지</a>
	<? }?>
	|
	<a class='red' href='/my/'>마이페이지</a> |
	<a href='/logout.php'>로그아웃</a>
	<? } else { ?>
	
	<script type="text/javascript" src='/js/encrypt.js'></script>
	<div id='loginLayer'><div style='padding: 20px'>
		<span class='btn_close close'>×</span>

		<h3 style='font: 10pt NanumGothicBold; margin: 0px 0px 10px 0px'>위젠 로그인</h3>
		<!-- <form id='loginForm' name='loginForm' action='/login.php' method='post'> -->
		<input type='hidden' name='url' value='<?=urlencode($_GET[url] ? $_GET[url] : $_SERVER[REQUEST_URI])?>' />
		<img src='/images/common/btn_login.png' style='float: right; cursor: pointer' onclick="if(checkLoginForm()) { processLogin('<?=urlencode($_GET[url] ? $_GET[url] : '')?>')}" />
		<input type='text' name='uid' class='text' /><br/>
		<input type='text' name='password_pseudo' class='text' />
		<input type='password' name='p' class='text' style='display:none;'/>
		<!-- </form>  -->

		<p style='font: 11px/20px NanumGothic; text-align: right; margin: 10px 0px'>
		아직 회원이 아니세요? <a id='regLink' href='/register'><strong>회원가입 &gt;</strong></a><br/>
		비밀번호가 생각나지 않으세요? <a href='/findpw'><strong>비밀번호찾기 &gt;</strong></a>
		</p>

		<h3 style='font: 10pt NanumGothicBold; margin: 0px 0px 10px 0px'>소셜 로그인</h3>
		<div style='text-align: center; margin-top: 4px'>
		<img src='/images/common/btn_facebook.png' class='btn_login_facebook clickable' />
		<img src='/images/common/btn_twitter.png' class='btn_login_twitter clickable' />
		</div>
	</div></div>

	<span class='btn_login clickable'>로그인</span> |
	<a href='/register'>회원가입</a>
	<? } ?>
	</div>
</div>
<!-- /header -->

<!-- menu -->
<h2 class='blind'>메인 메뉴</h2>
<div id='menu'>
	<menu>
	<li><a href='/campaign/'>캠페인 보기</a></li>
	<li><a href='/givex'>정기후원</a></li>
	<li><a href='/about/'>위젠이란?</a></li>
	<li><a href='/request/'>협력 신청</a></li>
	<li><a href='/postscript/'>후기</a></li>
	<li><a href='/partners/fundraisers'>파트너존</a></li>
	</menu>
</div>
<!-- /menu -->

<script>
</script>
