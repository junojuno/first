<?php
   /* SMS API SETTIONG (coolsms.co.kr) */
   define('SMS_API_KEY', 'NCS5337C5B6D6EB5');
   define('SMS_API_SECRET', '18D3DC5EA6ABBE4F4CBF3B56AABDE8AA');

   include "coolsms.php";

   $rest = new coolsms(SMS_API_KEY, SMS_API_SECRET);

   function phoneCheck($phone) {
      $regExp = '/^(01[016789]{1})-?[0-9]{3,4}-?[0-9]{4}$/';
      if(preg_match($regExp,$phone)) return true;
      else return false;
   }
   function sendSMS($contact_arr, $content, $send_type ='SMS', $send_phone = null, $datetime=null) {
      global $rest;
      $options = new stdClass();
      $options->to = join(', ',$contact_arr);
      if($send_phone) {
         $from_num = $send_phone;
      } else {
         $info = sql_fetch("SELECT opt_sms_num FROM ".DB_INFO." LIMIT 1");
         $from_num = str_replace("-","", $info[opt_sms_num]);
      }
      $options->from = $from_num;
      $options->text = $content;
      $options->type = $send_type;
      if($datatime) {
         $options->datatime = $datetime;
      }

      $res = $rest->send($options);
      return $res;
   }

   $_sms_contact = array();
   if(isset($_POST[sms_type])) {
      $_sms_type = $_POST[sms_type];
      $_required = true;
      include $_SERVER[DOCUMENT_ROOT].'/config.php';
      sec_session_start(false);
   }

   if($_sms_type == 'join') {
      if($_POST[mb_no] == '') {
         echo 'failed';
         return;
      }
      $mb_no = $_POST[mb_no];
      $info = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = $mb_no");
      if($info[mb_contact] != '') {
         if (phoneCheck($info[mb_contact])) {
            $_sms_content = "[위젠] $info[mb_name]님 반갑습니다. 지금 바로 스타와의 특별한 경험을 시작하세요!";
            $_sms_phone = explode("-", $info[mb_contact]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $mb_no , contact = '$info[mb_contact]', group_id='$result->group_id'");
               sql_query("UPDATE ".DB_MEMBERS." SET sent_sms = 1 WHERE mb_no = $mb_no");
            }
         }
      }
   }

   if($_sms_type == 'donate_account_info') {
      if($_POST[od_id] == '') {
         echo 'failed';
         exit;
      }
      $info = sql_fetch("SELECT * FROM ".DB_ORDERS." WHERE od_id = '$_POST[od_id]'");
      $escrow = explode("/",$info[od_escrow1]);
      $chk = $escrow[4] == "1" ? true : false;
      if($chk) {
         echo 'failed';
         exit;
      }
      $bankname = $escrow[0];
      $depositor = $escrow[1];
      $account = $escrow[2];
      $mny = $info[od_amount];
      
      if($info[od_hp] != '') {
         if (phoneCheck($info[od_hp])) {
            $_sms_content = "[위젠] $info[od_name]님 ".number_format($mny)."원\n$bankname $account\n(예금주 : $depositor) 입니다.";
            $_sms_phone = explode("-", $info[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $_SESSION[user_no] , contact = '$info[od_hp]', group_id='$result->group_id'");
               sql_query("UPDATE ".DB_ORDERS." SET od_escrow1 = CONCAT(od_escrow1,'/1') WHERE od_id = '$_POST[od_id]'");
            }
         }
      }

      echo 'success';
   }

   if($_sms_type == 'donate') {
      if(isset($_POST[sms_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }

      if(isset($_POST[sms_type])) {
         $od = sql_fetch("SELECT *, a.sent_sms AS o_sent_sms FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
            LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
            WHERE a.od_id = '$_POST[od_id]'");
      } // 가상계좌일때는 이미 $od 에 fetch했음. (가상계좌는 POST가아님)
      

      if($od[o_sent_sms] == 0) {
         if (phoneCheck($od[mb_contact])) {
            $_sms_content = $od[it_sms] != '' ? $od[it_sms] : "[위젠] $od[od_name] 님 후원해주셔서 감사합니다^^ 행복한 하루되세요 :)";
            $_sms_phone = explode("-", $od[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("UPDATE ".DB_ORDERS." SET sent_sms = 1 WHERE od_id = '$od[od_id]'");
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $od[mb_no] , contact = '$od[od_hp]', group_id='$result->group_id', var_1 = '$od[od_id]'");
            }
         }
      }
   }

   if($_sms_type == 'kara_donate') {
      if(isset($_POST[sms_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }

      if(isset($_POST[sms_type])) {
         $od = sql_fetch("SELECT *, a.sent_sms AS o_sent_sms FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
            LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
            WHERE a.od_id = '$_POST[od_id]'");
      } // 가상계좌일때는 이미 $od 에 fetch했음. (가상계좌는 POST가아님)
      if($od[o_sent_sms] == 0) {
         if (phoneCheck($od[mb_contact])) {
            $_sms_content = "[DSPZONE] $od[od_name] 님\n2014 카밀리아 데이!\n5월 24일 07:00PM\n블루스퀘어 삼성카드홀\n좌석번호: $od[var_1]\n\n*당일 신분증을 꼭 지참해주십시오\n*수익금 전액이 기부되므로 환불은 불가합니다\n*관련문의: wegen@wegen.kr";
            $_sms_phone = explode("-", $od[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content, 'LMS', '070-4164-1151');
            if($result->result_code == "00") {
               sql_query("UPDATE ".DB_ORDERS." SET sent_sms = 1 WHERE od_id = '$od[od_id]'");
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $od[mb_no] , contact = '$od[od_hp]', group_id='$result->group_id', var_1 = '$od[od_id]'");
               echo 'success';
            }
         }
      }
   }
   if($_sms_type == 'kara_account_late') {
      if(isset($_POST[sms_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }

      if($od[od_method] == '가상계좌') $od[var_1] = $seat_info;
      if($od[o_sent_sms] == 0) {
         if (phoneCheck($od[mb_contact])) {
            $_sms_content = "[WEGEN] $od[od_name] 님\n2014 카밀리아데이 티켓이 이미 매진되었습니다. 환불받으실 계좌를 답변으로 보내주시면 환불 처리해드리겠습니다. 참여해주셔서 감사합니다.";
            $_sms_phone = explode("-", $od[od_hp]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content, 'LMS', '010-4542-1419');
            if($result->result_code == "00") {
               sql_query("UPDATE ".DB_ORDERS." SET sent_sms = 1 WHERE od_id = '$od[od_id]'");
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $od[mb_no] , contact = '$od[od_hp]', group_id='$result->group_id', var_1 = '$od[od_id]'");
               echo 'success';
            }
         }
      }
      
   }

   if($_sms_type == 'success_campaign') {
      if(!isset($_POST[it_id]) || $_POST[it_id] == '' || !$_SESSION[is_admin]) {
         echo 'failed';
         exit;
      }
      $it_id = $_POST[it_id];
      $chk = sql_fetch("SELECT it_name, sent_sms FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
      if($chk[sent_sms] > 0) {
         echo 'failed';
         exit;
      }

      sql_query("UPDATE ".DB_CAMPAIGNS." SET sent_sms = 1 WHERE it_id = '$it_id'");
      $donate_res = sql_query("SELECT * FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
            WHERE a.it_id = '$it_id' AND a.od_amount - a.pay_remain > 0 AND b.mb_agreesms = 1 GROUP BY a.mb_no");

      $itname_euckr = mb_convert_encoding($chk[it_name],"euc-kr", "utf-8");

      while($row = sql_fetch_array($donate_res)) {
         if (phoneCheck($row[mb_contact])) {
            $_sms_contact = array(); // 연락처 배열 초기화

            //이름 길이에 따라 캠페인 적당하게 줄여서 내용에 추가
            $_sms_content = "[위젠] $row[mb_name] 님이 후원해주신 \"\" 의 목표모금액을 달성했습니다! 감사합니다";
            $_sms_content_euckr = mb_convert_encoding($_sms_content,'euc-kr','utf-8');
            $len = strlen($_sms_content_euckr) < 90 ? 90 - strlen($_sms_content_euckr) : 0;
            $cam_name = $len >= 8 ? 
               ( strlen($itname_euckr) < $len ? $itname_euckr : mb_convert_encoding(substr($itname_euckr,0,$len-2),"utf-8", "euc-kr") ) : 
               "";
            $cam_name = $len >=8 && strlen($itname) < $len ? $cam_name.".." : $cam_name;

            $_sms_content = "[위젠] $row[mb_name] 님이 후원해주신 \"$cam_name\" 의 목표모금액을 달성했습니다! 감사합니다";
            if($len < 8) $_sms_content = str_replace("\"\" ", "", $_sms_content);

            $_sms_phone = explode("-", $row[mb_contact]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $row[mb_no] , contact = '$row[mb_contact]', group_id='$result->group_id', var_1 = '$it_id'");
            }
         }
      }
      sql_query("UPDATE ".DB_CAMPAIGNS." SET sent_sms = 2 WHERE it_id = '$it_id'");

      echo 'success';
   }

   if($_sms_type == 'upload_postscript') {
      if(!isset($_POST[wr_id]) || $_POST[wr_id] == '' || !$_SESSION[is_admin]) {
         echo 'failed';
         exit;
      }

      $wr_id = $_POST[wr_id];
      $chk = sql_fetch("SELECT *, p.sent_sms AS p_sent_sms FROM ".DB_POSTSCRIPTS." p LEFT JOIN ".DB_CAMPAIGNS." b ON p.wr_campaign = b.it_id WHERE wr_id = '$wr_id'");
      if($chk[p_sent_sms] > 0) {
         echo 'failed';
         exit;
      }

      sql_query("UPDATE ".DB_POSTSCRIPTS." SET sent_sms  = 1 WHERE wr_id = '$wr_id'");
      $donate_res = sql_query("SELECT * FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
            WHERE a.it_id = '$chk[wr_campaign]' AND a.od_amount - a.pay_remain > 0 AND b.mb_agreesms = 1 GROUP BY a.mb_no");

      $itname_euckr = mb_convert_encoding($chk[it_name],"euc-kr", "utf-8");

      while($row = sql_fetch_array($donate_res)) {
         if (phoneCheck($row[mb_contact])) {
            $_sms_contact = array(); // 연락처 배열 초기화

            //이름 길이에 따라 캠페인 적당하게 줄여서 내용에 추가
            $_sms_content = "[위젠] $row[mb_name] 님이 후원해주신 \"\" 에 후기가 등록되었습니다";
            $_sms_content_euckr = mb_convert_encoding($_sms_content,'euc-kr','utf-8');
            $len = strlen($_sms_content_euckr) < 90 ? 90 - strlen($_sms_content_euckr) : 0;
            $cam_name = $len >= 8 ? 
               ( strlen($itname_euckr) < $len ? $chk[it_name] : mb_convert_encoding(substr($itname_euckr,0,$len-2),"utf-8", "euc-kr") ) : 
               "";
            $cam_name = $len >=8 && strlen($itname_euckr) > $len ? $cam_name.".." : $cam_name;
            $_sms_content = "[위젠] $row[mb_name] 님이 후원해주신 \"$cam_name\" 에 후기가 등록되었습니다";
            if($len < 8) $_sms_content = str_replace("\"\" ", "", $_sms_content);

            $_sms_phone = explode("-", $row[mb_contact]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $row[mb_no] , contact = '$row[mb_contact]', group_id='$result->group_id', var_1 = '$wr_id'");
            }
         }
      }
      sql_query("UPDATE ".DB_POSTSCRIPTS." SET sent_sms  = 2 WHERE wr_id = '$wr_id'");

      echo 'success';
   }

   if($_sms_type == 'connect_post_upload') {
      if(!isset($_POST[it_id]) || $_POST[it_id] == '') {
         echo 'failed';
         exit;
      }
      $it_id = $_POST[it_id];
      $it = sql_fetch("SELECT it_connectors FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
      $connector = explode("|",$it[it_connectors]);
      if(!$_SESSION[is_admin] && array_search($_SESSION[user_no],$connector) === false ) exit;
      
      $chk_sent = sql_fetch("SELECT a.cmt_id, a.sent_sms, b.it_name FROM ".DB_CAMPAIGN_CMTS." a 
         LEFT JOIN ".DB_CAMPAIGNS." b ON b.it_id = a.it_id 
         WHERE a.cmt_category = 100 AND a.it_id = '$it_id'
         ORDER BY a.cmt_time DESC LIMIT 1");
      if($chk_sent[sent_sms] != 0) {
         echo 'failed';
         exit;
      }
      
      $donate_res = sql_query("SELECT * FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
            WHERE a.it_id = '$it_id' AND a.od_amount - a.pay_remain > 0 AND b.mb_agreesms = 1 GROUP BY a.mb_no");

      $itname_euckr = mb_convert_encoding($chk_sent[it_name],"euc-kr", "utf-8");
      while($row = sql_fetch_array($donate_res)) {
         if (phoneCheck($row[mb_contact])) {
            $_sms_contact = array(); // 연락처 배열 초기화

            //이름 길이에 따라 캠페인 적당하게 줄여서 내용에 추가
            $_sms_content = "[위젠] $row[mb_name] 님이 후원해주신 \"\" 에 새로운 수혜자TALK가 올라왔습니다";
            $_sms_content_euckr = mb_convert_encoding($_sms_content,'euc-kr','utf-8');
            $len = strlen($_sms_content_euckr) < 90 ? 90 - strlen($_sms_content_euckr) : 0;
            $cam_name = $len >= 8 ? 
               ( strlen($itname_euckr) < $len ? $chk_sent[it_name] : mb_convert_encoding(substr($itname_euckr,0,$len-2),"utf-8", "euc-kr") ) : 
               "";
               $cam_name = $len >=8 && strlen($itname_euckr) > $len ? $cam_name.".." : $cam_name;
            $_sms_content = "[위젠] $row[mb_name] 님이 후원해주신 \"$cam_name\" 에 새로운 수혜자TALK가 올라왔습니다";
            if($len < 8) $_sms_content = str_replace("\"\" ", "", $_sms_content);

            $_sms_phone = explode("-", $row[mb_contact]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $row[mb_no] , contact = '$row[mb_contact]', group_id='$result->group_id', var_1 = '$chk_sent[cmt_id]'");
            }
         }
      }
      sql_query("UPDATE ".DB_CAMPAIGN_CMTS." SET sent_sms = 1 WHERE cmt_id = '$chk_sent[cmt_id]' ");

      echo 'success';
      
   }

   if($_sms_type == 'bid_new_rank') {
      if($_SESSION[auc_id] == '') {
         echo 'failed';
         exit;
      }

      $chk = sql_fetch("SELECT  
            (SELECT COUNT(bid_id) + 1 FROM ".DB_BIDDING." WHERE auc_id = a.auc_id AND bid_amount > a.bid_amount OR IF(bid_amount = a.bid_amount, IF(bid_time < a.bid_time, 1, 0), 0) = 1 ) AS rank ,
            b.it_id, c.auc_url 
            FROM ".DB_BIDDING." a 
            LEFT JOIN ".DB_CAMPAIGNS." b ON b.it_auction = a.auc_id 
            LEFT JOIN ".DB_AUCTION." c ON c.auc_id = a.auc_id 
            WHERE a.auc_id = '$_SESSION[auc_id]' AND mb_no = '$_SESSION[user_no]' ");

      if($chk[rank] != 1) {
         echo 'failed';
         exit;
      }

      $bid_res = sql_query("SELECT * FROM ".DB_BIDDING." a 
         WHERE a.auc_id = '$_SESSION[auc_id]' AND a.mb_no != '$_SESSION[user_no]' 
         GROUP BY a.bid_contact ");

      $auc_url = $chk[auc_url] ? $chk[auc_url] : "http://wegen.kr/m/campaign/$chk[it_id]";

      while($row = sql_fetch_array($bid_res)) {
         if (phoneCheck($row[bid_contact])) {
            $_sms_contact = array(); // 연락처 배열 초기화
            $_sms_content = "[위젠] 입찰하신 자선경매에 최고 입찰자가 생겼습니다\n$auc_url";
            $_sms_phone = explode("-", $row[bid_contact]);
            $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
            $_sms_contact[0] = $tran_phone;
            $_sms_contact[1] = '01045421419';
            $_sms_contact[2] = '01062518138';
            $result = sendSMS($_sms_contact,$_sms_content);
            if($result->result_code == "00") {
               sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = $row[mb_no] , contact = '$row[bid_contact]', group_id='$result->group_id'");
            }
         }
      }
      //대표님이랑 이사님한테도 문자보내기
      $_sms_contact = array();
      $_sms_content = "[위젠] 입찰하신 자선경매에 최고 입찰자가 생겼습니다\n$auc_url";
      $_sms_contact[0] = '01045421419';
      $_sms_contact[1] = '01062518138';
      $result = sendSMS($_sms_contact,$_sms_content);
      if($result->result_code == "00") {
         sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = '' , contact = '', group_id='$result->group_id', var_1 = '관리자 알림문자'");
      }
   }

   if($_sms_type == 'kara_fan_definite') {
      $res = sql_query("SELECT a.* FROM ".DB_ORDERS." a 
            WHERE a.it_id = '$SPECIAL[kara_fan]' AND a.od_time >= 20140509075000 AND a.od_time <=20140517170000 AND pay_remain = 0 AND a.sent_sms = 0 ORDER BY a.od_time ASC");
      $all_num = mysql_num_rows($res);
      while($row = sql_fetch_array($res)) {
         $_sms_contact = array();
         $_sms_content = "[DSPZONE] 좌석 배정 최종 확정 문자 드립니다\n\n최종 확정된 좌석 정보를 보내드립니다.\n이 문자를 지참하셔서 팬미팅 참가해주시기 바랍니다.\n\n2014 카밀리아 데이!\n5월 24일 07:00 PM\n블루스퀘어 삼성카드홀\n좌석번호: {$row[var_1]}\n\n*당일 신분증을 꼭 지참해주십시오\n*수익이 기부되므로 환불은 불가합니다\n*관련 문의: wegen@wegen.kr\n";
         $_sms_phone = explode("-", $row[od_hp]);
         $tran_phone = $_sms_phone[0].$_sms_phone[1].$_sms_phone[2];
         $_sms_contact[0] = $tran_phone;

         $result = sendSMS($_sms_contact,$_sms_content,'LMS','010-4542-1419');
         if($result->result_code == "00") {
            $fail_count++;
            sql_query("UPDATE ".DB_ORDERS." SET sent_sms = 1 WHERE od_id = '$row[od_id]'");
            sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = 0 , contact = '$row[od_hp]', group_id='$result->group_id', var_1 = '$row[od_id]'");
         } else {
            sql_query("INSERT INTO ".DB_SMS_LOG." SET type='$_sms_type', mb_no = 0, contact = '$row[od_hp]', group_id='$result->group_id', var_1 = '$row[od_id]', isFailed= '{$result->result_code}'");
         }
      }
      echo "총 {$all_num}개 중 ".$fail_count."개 성공";
   }
?>
