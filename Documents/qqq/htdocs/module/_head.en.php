<?
if (!$_required) {
   header("HTTP/1.0 404 Not Found");
   exit;
}

sec_session_start();

if ($_loginrequired && login_check($mysqli) != true) {
   header("Location: /login.php?url=".urlencode($_SERVER[REQUEST_URI]));
   exit;
}

$settings[type] = ($settings[type]) ? $settings[type] : 'website';
//$settings[url] = ($settings[url]) ? $settings[url] : "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
$settings[url] = ($settings[url]) ? $settings[url] : "http://wegen.kr" . $_SERVER['REQUEST_URI'];
$settings[title_page] = ($settings[title] == '') ? 'WEGENERATION' : $settings[title].' - WEGENERATION';
$settings[image] = ($settings[image]) ? $settings[image] : 'http://wegen.kr/images/common/logo_facebook.jpg';
$settings[desc] = ($settings[desc]) ? $settings[desc] : 'Celebrity Charity Crowdfunding, WEGENERATION';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title><?=$settings[title_page]?></title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.0' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='/js/scripts.js'></script>
<script type='text/javascript'>
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-34058829-1']);
_gaq.push(['_trackPageview']);

(function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
</head>
<body>
<div id='fb-root'></div>

<!-- header -->
<div id='header'>
   <input type="hidden" name="ismobile" value="<?=$isMobile ? '1' : '0'?>"/>
   <h1><a href='/'><img src='/images/common/logo.png' alt='WEGENERATION' style='height: 30px' /></a>
   <span style='font-size:11pt;'>Celebrity Charity Crowdfunding, WEGENERATION</span></h1>
   
   <div style='text-align: right; line-height: 60px<? print ($_hidestatus)?'; display: none' :''; ?>'>
      <? if (login_check($mysqli) == true) { ?>
         <strong id='userName' ><?=$_SESSION['username']?></strong> | 
         <a href='/logout.php' >Logout</a>
      <? } else { ?>
         <div id='loginLayer' style='width:520px;'>
            <div style='padding: 20px'>
               <span class='btn_close' style='font-size:20pt;'>×</span>
               <h3 style='font: 24pt NanumGothicBold; margin: 0px 0px 10px 0px;text-align:center;'>
                  <span style='color:#b0b0b0;'>ㅡ</span> CONNECT TO WEGEN <span style='color:#b0b0b0;'>ㅡ</span></h3>
               <input type='hidden' name='url' value='<?=urlencode($_GET[url] ? $_GET[url] : $_SERVER[REQUEST_URI])?>' />
               <div style='text-align: center; margin-top: 25px'>
                  <img src='/images/common/btn_facebook.png' class='btn_login_facebook clickable' style='width:300px;margin-bottom:16px;'/>
                  <img src='/images/common/btn_twitter.png' class='btn_login_twitter clickable' style='width:300px;'/>
               </div>
            </div>
         </div>
         <span class='btn_login clickable'>Login</span>
      <? } ?>
   </div>
</div>

<!-- menu -->
<div id='menu'>
</div>
<!-- /menu -->
