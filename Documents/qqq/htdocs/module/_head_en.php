<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}


sec_session_start();

if(!login_check($mysqli)) {
   if(isset($_COOKIE[wegen_id]) && isset($_COOKIE[wegen_pw])) {
      login($_COOKIE[wegen_id],$_COOKIE[wegen_pw],$mysqli);
   }
}

if ($_loginrequired && login_check($mysqli) != true) {
	header("Location: /login.php?url=".urlencode($_SERVER[REQUEST_URI]));
	exit;
}

$settings[type] = ($settings[type]) ? $settings[type] : 'website';
//$settings[url] = ($settings[url]) ? $settings[url] : "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
$settings[url] = ($settings[url]) ? $settings[url] : "http://wegen.kr" . $_SERVER['REQUEST_URI'];
$settings[title_page] = ($settings[title] == '') ? 'WeGeneration' : $settings[title].' - WeGeneration';
$settings[image] = ($settings[image]) ? $settings[image] : 'http://wegen.kr/images/common/logo_facebook.jpg';
$settings[desc] = ($settings[desc]) ? $settings[desc] : '스타와 함께 하는 즐거운 기부, 위제너레이션';
?>
<!DOCTYPE html>
<html>

<head>
<meta property="fb:app_id" content="211534425639026" />
<meta property='og:title' content='<?=$settings[title]?>' />
<meta property='og:type' content='<?=$settings[type]?>' />
<meta property='og:url' content='<?=$settings[url]?>' />
<meta property='og:image' content='<?=$settings[image]?>' />
<meta property='og:description' content='<?=$settings[desc]?>' />
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=9; IE=8; IE=7; IE=EDGE' />
<title><?=$settings[title_page]?></title>
<link rel='stylesheet' type='text/css' href='/css/style.css?ver=3.03' /> 
<script>
   var js_file = '<?=file_exists(JS_ROOT.str_replace('.php','.js', SCRIPT_NAME)) ? '/js'.str_replace('.php','.js',SCRIPT_NAME) : false ?>';
   var http_host = '<?=$_SERVER[HTTP_HOST]?>';
   var mb_no = '<?=$_SESSION[user_no]?>';
   var it_id = '<?=$_REQUEST[it_id]?>';
   var _debug = '<?=$_debug? 1 : 0?>';
   var _isMobile = '<?=$isMobile? 1 : 0?>';
   <?=$js_var?>
</script>
<script type='text/javascript' src='/js/scripts.js'></script>

<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />

<script type='text/javascript'>
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-34058829-1']);
_gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

$(document).ready(function() {

<?
if ($_SESSION[user_no]) {
	$sql = "SELECT * FROM ".DB_BADGES."
			WHERE mb_no = '$_SESSION[user_no]'
			AND isChecked = '0'
			ORDER BY id ASC ";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	if ($total > 0) {
		$badges = array(
					1 => array('개척자', '캠페인에 첫 번째로 후원해주셔서'),
					2 => array('종결자', '캠페인에 마지막으로 후원해주셔서'),
					3 => array('메신저', '캠페인을 SNS로 공유해주셔서'),
					4 => array('히어로', '캠페인에 최고액을 후원해주셔서'),
					5 => array('럭키가이/럭키걸', '펀드레이저 이벤트에 당첨되셔서'),
					6 => array('홈런', '캠페인 목표 모금액 달성 후 초과 후원해주셔서'),
					7 => array('100% 달성', '후원해주신 캠페인이 목표 모금액을 달성하여'),
					8 => array('VIP', '총 후원액이 100만원을 넘어'),
					9 => array('PERFECT', '후원 내역을 공유하고 응원코멘트를 모두 작성하여'),
					90 => array('GIVEx', '정기후원을 서약하여'),
					91 => array('First 300', '정기후원을 서약하신 최초 300분 내에 들어'),
					101 => array('캠페인 후원: 역사/문화', ''),
					102 => array('캠페인 후원: 환경/동물보호', ''),
					103 => array('캠페인 후원: 아동/청소년', ''),
					104 => array('캠페인 후원: 노인', ''),
					105 => array('캠페인 후원: 장애인', ''),
					106 => array('캠페인 후원: 저소득가정', ''),
					107 => array('캠페인 후원: 다문화가정', ''),
					108 => array('캠페인 후원: 자활', '')
				);
		for ($i = 0; $row = sql_fetch_array($result); $i++) {
			$cat = $row[category];
			$sharebtn = ($row[category] < 100) ? "<button class='btn_sharebadge' badgecat='$cat'>공유하기</button>" : false;

			$badges[$cat][1] .= $badges[$cat][1] ? '<br/>' : false;

			if ($i == 0) {
?>
	qGrowl("<div class='grantBadge' category='<?=$cat?>' param1='<?=$row[od_id]?>' param2='<?=$row[it_id]?>'><img src='/images/common/badges/<?=$cat?>_on.png'/></div><p>축하합니다!</p><p><?=$badges[$cat][1]?>&quot;<strong><?=$badges[$cat][0]?></strong>&quot; 뱃지를 획득하셨습니다</p><p style='text-align: center'><?=$sharebtn?><button class='ok'>확인</button></p>", '뱃지 획득 알림', true);

<?			} else { ?>
	setTimeout(function() {
		qGrowl("<div class='grantBadge' category='<?=$cat?>' param1='<?=$row[od_id]?>' param2='<?=$row[it_id]?>'><img src='/images/common/badges/<?=$cat?>_on.png'/></div><p>축하합니다!</p><p><?=$badges[$cat][1]?>&quot;<strong><?=$badges[$cat][0]?></strong>&quot; 뱃지를 획득하셨습니다</p><p style='text-align: center'><?=$sharebtn?><button class='ok'>확인</button></p>", '뱃지 획득 알림', true);
	}, <?=$i*250?>);
<?
			}
		}
	}
}
else {
?>
	addLabel('input[name=uid]', 'Email Address');
	addLabel('input[name=password_pseudo]', 'Password','password');

<? } ?>
});
</script>
</head>

<body>
<div id='fb-root'></div>

<!-- header -->
<div id='header'>
	<input type="hidden" name="ismobile" value="<?=$isMobile ? '1' : '0'?>"/>
	<h1><a href='/'><img src='/images/common/logo.png' alt='WEGENERATION' style='height: 30px' /></a>
	<img src='/images/common/logo_sub_en.png' alt='스타와 함께 하는 즐거운 기부, 위젠' style='height: 15px; margin-bottom: -15px;' /></h1>

	<div class='status'>
	<? if (login_check($mysqli) == true) { ?>
	<strong id='userName'><?=$_SESSION['username']?></strong>님
	<?
		$coin = getCoin($_SESSION[user_no]);
	?>
	<a href='/my/coin'><img src='/images/common/coin.png' style='width: 16px; height: 16px; margin-bottom: 2px; vertical-align: middle' />
	<span style='font: bold 10pt Arial'><?=number_format($coin);?></span></a> 
	<?
		if (array_search($_SESSION[user_no], $settings[btb]) !== false) {
	?>
	| <a href='<?=$settings[btb_url][$_SESSION[user_no]]?>'>관리자페이지</a>
	<? }?>
	|
<a class='red' href='/my/'>My page</a> |
	<a href='/logout.php'>Logout</a>
	<? } else { ?>
	
	<script type="text/javascript" src='/js/encrypt.js'></script>
	<div id='loginLayer'><div style='padding: 20px'>
		<span class='btn_close close'>×</span>

		<h3 style='font: 10pt NanumGothicBold; margin: 0px 0px 10px 0px'>WeGen Login</h3>
		<!-- <form id='loginForm' name='loginForm' action='/login.php' method='post'> -->
		<input type='hidden' name='url' value='<?=urlencode($_GET[url] ? $_GET[url] : $_SERVER[REQUEST_URI])?>' />
		<img src='/images/common/btn_login.png' style='float: right; cursor: pointer' onclick="if(checkLoginForm()) { processLogin('<?=urlencode($_GET[url] ? $_GET[url] : '')?>')}" />
		<input type='text' name='uid' class='text' /><br/>
		<input type='text' name='password_pseudo' class='text' />
		<input type='password' name='p' class='text' style='display:none;'/>
		<!-- </form>  -->

		<p style='font: 11px/20px NanumGothic; text-align: right; margin: 10px 0px'>
		아직 회원이 아니세요? <a id='regLink' href='/register'><strong>Signup &gt;</strong></a><br/>
		비밀번호가 생각나지 않으세요? <a href='/findpw'><strong>Find password &gt;</strong></a>
		</p>

		<h3 style='font: 10pt NanumGothicBold; margin: 0px 0px 10px 0px'>Social Login</h3>
		<div style='text-align: center; margin-top: 4px'>
		<img src='/images/common/btn_facebook_en.png' class='btn_login_facebook clickable' />
		<img src='/images/common/btn_twitter_en.png' class='btn_login_twitter clickable' />
		</div>
	</div></div>

	<span class='btn_login clickable'>Login</span> |
	<a href='/register'>Signup</a>
	<? } ?>
	</div>
</div>
<!-- /header -->

<!-- menu -->
<h2 class='blind'>메인 메뉴</h2>
<div id='menu'>
	
   <? if($_SERVER[REQUEST_URI] == '/' && !$isMobile) { ?>
   <div class='recent'>
   <div class='char'><div class='recent_arrow'></div>최근활동</div>
      <ul class='list'>
         <? include 'module/list.recent.php'; ?>
      </ul>
   </div>
      
   <? } ?>
</div>
<!-- /menu -->

<? if($_SESSION[user_no]) { 
      $res = sql_query("SELECT *, SUM(od_amount) AS it_funded FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id 
            LEFT JOIN ".DB_FUNDRAISERS." c ON b.it_fundraiser = c.no
            WHERE a.mb_no = $_SESSION[user_no] AND b.it_isOpenCampaign = 1 AND IF(od_method = '무료응모', 1, IF(od_amount-pay_remain = 0, 0, 1)) = 1 GROUP BY a.it_id");
      $total = mysql_num_rows($res);
      $count = 1;
      if($total != 0) { ?>
         <div class='ticket_list'>
            <div class='top'>
               <img src='/images/ticket_left.png'>
            </div>
            <div class='list'>
            <? while($row = sql_fetch_array($res)) { 
               $diff = round((strtotime($row[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
               $d_day = ($diff < 0) ? '완료' : "D-".$diff;
               $ticket = floor($row[it_funded]/100);
               $chklast = $count == $total ? 'last' : false;
               $count++;
            ?>
               <div class='unit <?=$chklast?>'>
                  <div class='cont'>
                     <?=$row[fr_name]?> (<?=$d_day?>):<br>
                     <?=$ticket?> Ticket 
                  </div>
               </div>
            <? } ?>
            </div>
         </div>
      <? } ?>
<? } ?>

<script>
</script>
