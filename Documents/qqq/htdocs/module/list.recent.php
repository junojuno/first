<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$sql = "
	SELECT * FROM (
	(
		SELECT
			'donate' AS type,
			o.mb_no AS mb_no,
			m.mb_name AS name,
			i.it_id AS itid,
			i.it_shortname AS itname,
         '' AS event,
			o.od_time AS proctime
		FROM ".DB_ORDERS." o
		LEFT JOIN ".DB_CAMPAIGNS." i ON (o.it_id = i.it_id) 
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = o.mb_no)
		WHERE o.od_amount - o.pay_remain > 0 AND i.it_isEnd != 1 AND i.it_isPublic != 0 AND o.mb_no != 610
	)
	UNION (
		SELECT
			'oc_share' AS type,
			o.mb_no AS mb_no,
			m.mb_name AS name,
			i.it_id AS itid,
			i.it_shortname AS itname,
         i.it_openCampaignEvent AS event,
         o.od_time AS proctime
		FROM ".DB_ORDERS." o
		LEFT JOIN ".DB_CAMPAIGNS." i ON (o.it_id = i.it_id)
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = o.mb_no)
		WHERE o.od_method = '무료응모'
	)
	UNION (
		SELECT
			'share' AS type,
			s.mb_no AS mb_no,
			m.mb_name AS name,
			i.it_id AS itid,
			i.it_shortname AS itname,
         '' AS event,
			acquired AS proctime
		FROM ".DB_BADGES." s
		LEFT JOIN ".DB_CAMPAIGNS." i ON (s.it_id = i.it_id)
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = s.mb_no)
		WHERE s.category = '3'
	)
	UNION (
		SELECT
			'givex' AS type,
			r.mb_no AS mb_no,
			m.mb_name AS name,
			'' AS itid,
			'' AS itname, 
         '' AS event,
			r.od_time AS proctime
		FROM ".DB_REGULARPAYMENT." r
		LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = r.mb_no)
		WHERE r.isFirst = '1'
	)
	UNION (
		SELECT
			'join' AS type,
			m.mb_no AS mb_no,
			m.mb_name AS name,
			'' AS itid,
			'' AS itname, 
         '' AS event,
			m.mb_date AS proctime
		FROM ".DB_MEMBERS." m
		WHERE m.mb_level = '1' AND m.mb_type != 44
	)
	)a
	ORDER BY proctime DESC 
	LIMIT 0 , 7
";

/*
	UNION (
	SELECT 'comment' AS type, p.it_id AS it_id, is_name AS od_name, is_time AS od_time, i.it_shortname as itname
	FROM "DB_CAMPAIGN_CMTS." p
	LEFT JOIN yc4_item i ON ( p.it_id = i.it_id )
	WHERE mb_id != 'wegen@wegen.kr'
*/

$result = sql_query($sql);
for ($i = 0; $recent = sql_fetch_array($result); $i++) {
	switch ($recent[type]) {
		case 'donate':
			print "<li>";
			drawPortrait($recent[mb_no]);
			print "<span>$recent[name]님이 캠페인에 후원 :</span> <a href='/campaign/$recent[itid]'>$recent[itname] 캠페인</a></li>";
			break;
      case 'oc_share':
			print "<li>";
			drawPortrait($recent[mb_no]);
         print "<a href='/campaign/$recent[itid]?oc=1'><span>$recent[name]님이 $recent[event]에 무료응모</span></a></li>";
         break;
		case 'share':
			print "<li>";
			drawPortrait($recent[mb_no]);
			print "<span>$recent[name]님이 캠페인을 공유 :</span> <a href='/campaign/$recent[itid]'>$recent[itname] 캠페인</a></li>";
			break;
		case 'givex':
			print "<li>";
			drawPortrait($recent[mb_no]);
			print "<span>$recent[name]님이 GIVEx 나눔 선서에 동참하셨습니다.</span></li>";
			break;
		case 'join':
			print "<li>";
			drawPortrait($recent[mb_no]);
			print "<span>$recent[name]님이 위젠에 가입하셨습니다.</span></li>";
			break;
	}
}
?>
