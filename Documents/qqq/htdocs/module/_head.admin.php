<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

sec_session_start();

if (!$_SESSION[is_admin]) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$pages = array(
			array('members', 'activities'),
			array('campaigns', 'partners', 'fundraisers', 'sponsors', 'postscripts'),
			array('media', 'guide','banner', 'notice'),
			array('auction','donation', 'givex', 'regulars', 'receipt', 'cohort'),
			array('dmanage'),
			array('btb_hellojeju','btb_sk')
		);
$current = array_pop(array_reverse(explode('.', array_pop(explode('/', $_SERVER[PHP_SELF])))));
for ($i = 0; $i < count($pages); $i++) {
	$index = array_search($current, $pages[$i]);
	if ($index !== false) {
		$eq = $i;
		$meq = $index;
		break;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>위제너레이션 관리자 페이지</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='/css/style.css' />
<link rel='stylesheet' type='text/css' href='http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
<script type='text/javascript' src='http://code.jquery.com/ui/1.10.3/jquery-ui.js'></script>
<script type='text/javascript' src='/js/jquery.qtip.min.js'></script>
<style>
h2 {padding-bottom: 20px; }
th {text-align: center; padding: 0px; background-color: #F0F0F0; }
.ui-datepicker th, .ui-datepicker td {height: auto; }
li {border-top: solid 1px white; border-bottom: solid 1px #DFDFDF; }
a.nav {padding: 0px 5px; background-color: #EEEEEE; font: 11pt Tahoma; outline: solid 1px #E3E3E3; }
#coinLayer {display: none; width: 800px; padding: 30px; border: solid 2px #DFDFDF; border-radius: 5px; background-color: white; }
.tab-contents {cursor: pointer; }
.cell-contents {line-height: 25px; padding: 10px 20px; background-color: #F9F9F9; display: none; }
.cell-contents p {line-height: 25px; }
.submit {margin: 30px 0px; outline: none; border: none; width: 150px; height: 40px; text-align: center; background-color: black; color: white; font: 12pt NanumGothic; cursor: pointer; }
.submit-red {margin: 30px 0px; outline: none; border: none; width: 150px; height: 40px; text-align: center; background-color: red; color: white; font: 12pt NanumGothic; cursor: pointer; }
.button {margin: 0px 10px; outline: none; border: none; display: inline !important; padding:5px; text-align: center; background-color: black; color: white; font: 10pt NanumGothic; cursor: pointer; }
.button-red {margin: 0px 10px; outline: none; border: none; display: inline !important; width: 80px; height: 25px; text-align: center; background-color: red; color: white; font: 10pt NanumGothic; cursor: pointer; }
.button-small {margin: 0px 10px; outline: none; border: none; display: inline !important; padding: 0px 10px; height: 20px; text-align: center; background-color: black; color: white; font: 10pt NanumGothic; cursor: pointer; }
.procPayment {cursor: pointer; }
.help {font: 10px NanumGothic; padding-left: 3px; }
.caption {font-size: 12px; line-height: 18px; }
.btn-coin {color: blue; }
.ui-autocomplete-loading {
    background: white url('/images/ui-anim_basic_16x16.gif') right center no-repeat;
  }
li._item { border:0;padding:4px 10px;overflow:hidden;text-decoration:none;vertical-align:top;cursor:pointer;
}
li._item.ev_focus { background-color:#eee;
}
li._item2 {border:0;display:inline;padding:5px;background-color:#8D8D8D;border:2px solid #b4b4b4;margin:3px;color:white;
}
</style>
<script type='text/javascript'>
$(document).ready(function() {

	$.fn.qtip.defaults.position.my = 'bottom left';
	$.fn.qtip.defaults.position.at = 'top center';
	$.fn.qtip.defaults.style.classes = 'qtip-bootstrap';
	$('[title]').qtip();
	$('.help').html('[?]');

	$('.date').datepicker({dateFormat: 'yymmdd'});
	$('.datetime').datepicker({dateFormat: 'yy-mm-dd'});
	$('input[type=text], textarea').addClass('text');
	$('input[type=submit]').addClass('button-small');
	$('.cell-contents').eq(<?=$eq?>).show();
	$('.cell-contents').eq(<?=$eq?>).children('p').eq(<?=$meq?>).css('font-family', 'NanumGothicBold');

	$('.tab-contents').click(function() {
		var cur = $('.tab-contents').index(this);
		$('.cell-contents').slideUp(200);
		if (!$('.cell-contents').eq(cur).is(':visible')) {
			$('.cell-contents').eq(cur).slideToggle(200);
		}
	});

	$('tr[data-href]').css('cursor', 'pointer').hover(
		function() {
			$(this).css('background-color', '#F9F9F9');
		},
		function() {
			$(this).css('background-color', 'white');
		}
	).click(function() {
		document.location.href = $(this).attr('data-href');
	});

   $('td[data-href]').css('cursor', 'pointer').hover(
      function() {
         $(this).css('background-color', '#F9F9F9');
      },
      function() {
         $(this).css('background-color', 'white');
      }
   ).click(function() {
      document.location.href = $(this).attr('data-href');
   });
});

function createLayer(str) {
	$(str).show().addClass('centering').css('margin-top', '-' + $(str).height()/2 + 'px').css('margin-left', '-' + $(str).width()/2 + 'px');
	$('body').append($(document.createElement('div')).addClass('layer').css('background', 'none').append(str));
}

function destroyLayer(str) {
	$(str).hide().prependTo($('body'));
	$('.layer').remove();
}

function js_send_email(post_data) {
   $.ajax({
      url:'/module/sendmail.php',
      type:'POST',
      data:post_data,
      cache:false,
      success: function(t) {
         if(t.match(/success/)) {
            if(post_data.mail_type == 'success_campaign') {
               $("td[data-kind=email][data-itid="+post_data.it_id+"]").html("<span style='color:red;'>보냈음</span>");
            }
            if(post_data.mail_type == 'upload_postscript') {
               $("td[data-kind=email][data-wrid="+post_data.wr_id+"]").html("<span style='color:red;'>보냈음</span>");
            }
         }
      }
   });
}

function js_send_sms(post_data) {
   $.ajax({
      url:'/module/sendsms.php',
      type:'POST',
      data:post_data,
      cache:false,
      success: function(t) {
         if(t.match(/success/)) {
            if(post_data.sms_type == 'success_campaign') {
               $("td[data-kind=sms][data-itid="+post_data.it_id+"]").html("<span style='color:red;'>보냈음</span>");
            }
            if(post_data.sms_type == 'upload_postscript') {
               $("td[data-kind=sms][data-wrid="+post_data.wr_id+"]").html("<span style='color:red;'>보냈음</span>");
            }
         }
      }
   });
}

function getBytes(a) {
   var b = a.match(/[^\x00-\xff]/g);
   return a.length + (!b? 0 : b.length);
}

</script>
</head>

<body>

<div style='position: fixed; left: 0px; top: 0px; width: 150px; height: 100%; background-color: #ECECEC; font: 10pt/30px NanumGothicBold'>
<ul>
	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -300px -33px'></span>
	<span class='tab-contents'>회원</span>
	<div class='cell-contents'>
	<p><a href='./members.php'>회원 관리</a></p>
	<p><a href='./activities.php'>ACTIVITY LOG</a></p>
	</div>
	</li>

	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -29px -33px'></span>
	<span class='tab-contents'>캠페인</span>
	<div class='cell-contents'>
	<p><a href='./campaigns.php'>캠페인 관리</a></p>
	<p><a href='./partners.php'>파트너 관리</a></p>
	<p><a href='./fundraisers.php'>펀드레이저 관리</a></p>
	<p><a href='./sponsors.php'>스폰서 관리</a></p>
	<p><a href='./postscripts.php'>캠페인 후기 관리</a></p>
	</div>
	</li>

	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -149px -33px'></span>
	<span class='tab-contents'>컨텐츠</span>
	<div class='cell-contents'>
	<p><a href='./media.php'>언론소개 관리</a></p>
	<p><a href='./guide.php'>위젠가이드 관리</a></p>
	<p><a href='./banner.php'>메인 배너 관리</a></p>
	<p><a href='./notice.php'>공지 사항 관리</a></p>
	</div>
	</li>


	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -330px -33px'></span>
	<span class='tab-contents'>현황</span>
	<div class='cell-contents'>
	<p><a href='./auction.php'>경매 참여 관리</a></p>
	<p><a href='./donation.php'>후원내역 관리</a></p>
	<p><a href='./givex.php'>정기후원 관리</a></p>
	<p><a href='./regulars.php'>정기후원내역 관리</a></p>
	<p><a href='./receipt.php'>영수증 발급 관리</a></p>
	<p><a href='./cohort.php'>방문자 분석</a></p>
	</div>	
	</li>
	
	<!--
	기부금 메뉴 추가
	2013. 08. 13.
	진입명
	-->
	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -330px -33px'></span>
	<span class='tab-contents'>기부금</span>
	<div class='cell-contents'>
	<p><a href='./dmanage.php'>기부금 관리</a></p>
	</div>	
	</li>
	
	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -330px -33px'></span>
	<span class='tab-contents'>B to B</span>
	<div class='cell-contents'>
	<p><a href='./btb_hellojeju.php'>헬로제주</a></p>
   <p><a href='./btb_sk.php'>SK</a></p>
	</div>
	</li>
	
	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -239px -33px'></span>
	<a href='./wegeninfo.php'>위젠정보</a>
	</li>
	
	<li><span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -239px -33px'></span><a href='./settings.php'>환경설정</a></li>

</ul>
</div>
