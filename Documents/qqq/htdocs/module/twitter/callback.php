<?
$_required = true;
include '../../config.php';
sec_session_start();
require_once('twitteroauth/twitteroauth.php');

/* If the oauth_token is old redirect to the connect page. */

if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
  $_SESSION['oauth_status'] = 'oldtoken';
  header('Location: ./clearsessions.php');
}


/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

/* Request access tokens from twitter */
$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

/* Save the access tokens. Normally these would be saved in a database for future use. */
$_SESSION['access_token'] = $access_token;

/* Remove no longer needed request tokens */
unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);

/* If HTTP response is 200 continue otherwise send to connect page to retry */
if (200 == $connection->http_code) {
   /* The user has been verified and the access tokens can be saved for future use */

   $_SESSION['status'] = 'verified';

   $user_profile = $connection->get('account/verify_credentials');
   $user_profile = (array)$user_profile;
	// process wegen login
   if (login_check($mysqli) == false) {

		$pwdstr = hash('sha512', $user_profile[id].'TryTwiTTeRsessionLOGin');
//		$membercheck = login('tw@'.$user_profile[id], $pwdstr, $mysqli);
		$membercheck = login('tw@'.$user_profile[id], $pwdstr, $mysqli);

		// if not member, build member profile
		if ($membercheck === '0009') {
			$password = hash('sha512', $user_profile[id].'TryTwiTTeRsessionLOGin');
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$password = hash('sha512', $password.$random_salt);
			$twid = 'tw@'.$user_profile[id];

			if ($stmt = $mysqli->prepare("INSERT INTO wegen_member (mb_id, mb_name, mb_password, tw_icon, salt) VALUES (?, ?, ?, ?, ?)")) {    
			   $stmt->bind_param('sssss', $twid, $user_profile[name], $password, $user_profile[profile_image_url], $random_salt); 
			   $stmt->execute();

			   $mb_no = $stmt->insert_id;
            
            ?>
            <html>
            <head>
               <meta charset="UTF-8">
               <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
            </head>
            <body>
               <script>
               $(document).ready(function() {
                  $.ajax({
                     url:'/module/sendmail.php',
                     type:'POST',
                     data:{
                        mail_type:'join',
                        mb_no:'<?=$mb_no?>'
                     },
                     cache:false,
                     success: function(t) {
                     }
                  });
               });
               </script>
            </body>
            </html>
            <?
			}

			// and login again
			login('tw@'.$user_profile[id], hash('sha512', $user_profile[id].'TryTwiTTeRsessionLOGin'), $mysqli);
		}

		// self close and reload
		print "<script>opener.location.reload(); self.close();</script>";
	}



} else {
  /* Save HTTP status for error dialog on connnect page.*/
  header('Location: ./clearsessions.php');
}
