<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
if (!$page) $page = 1;

$itemsPerRow = ($itemsPerRow) ? $itemsPerRow : 3;
$items = $itemsPerRow * $rows;
$limitFrom = ($page - 1) * $itemsPerRow;
$limitTo = $items;
$whereClause = ($_category) ? "WHERE wr_category = '$_category'" : false;

$sql = "SELECT *
		FROM ".DB_POSTSCRIPTS."
		$whereClause
		ORDER BY wr_datetime DESC
		LIMIT $limitFrom , $limitTo";
//wr_campaign
$result = sql_query($sql);
$total = mysql_num_rows($result);
$pageTotal  = ceil($total / $items);
?>

<ul id='campaignList'>
<?
for ($i=0; $row=sql_fetch_array($result); $i++) {
	$float = ($i % 2 == 0) ? 'left' : 'right';

	$category = ($row[wr_category] == 1) ? "캠페인 후기" : "이벤트 후기";
	$contents = strip_tags(str_replace('&nbsp;', '', $row[wr_content]));
	$contents = mb_substr($contents, 0, 150, 'UTF-8') . '...';
?>

	<li class='postscript'>
	<div class='campaignCell postscript'>
	<a href='/postscript/<? print ($row[wr_category] == 2)? "event/" : '' ?><?=$row[wr_campaign]?>' style='display: block'>
		<div style='float: <?=$float?>; width: 40%; height: 228px;'>
			<img class='thumbnail' src='<?=$row[wr_cover]?>' />
		</div>
		<div style='float: left; width: 60%;position:relative;height:228px;'><div style='margin:23px;'>
			<h3><?=$category?></h3>
			<div class='campaignTitle'><?=$row[wr_subject]?></div>
			<p class='campaignDesc'><?=$contents?></p>
			<span>더 읽기 &gt;</span>
		</div></div>
	</a>
	</div>
	</li>
<?
}
?>
</ul>
<div style='clear: both'></div>
