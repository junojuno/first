<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
if (!$page) $page = 1;

$itemsPerRow = ($itemsPerRow) ? $itemsPerRow : 3;
$items = $itemsPerRow * $rows;
$limitFrom = ($page - 1) * $itemsPerRow;
$limitTo = $items;
$whereClause = ($where) ? " AND " . $where : "";
$orderClause = ($orderBy) ? $orderBy : "it_id desc";

$sql = "SELECT i.*,
			IF (type = '2', (SELECT COUNT(DISTINCT mb_no) FROM wegen_volunteer v WHERE v.it_id = i.it_id), (SELECT sum(od_amount)-sum(pay_remain) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) ) AS it_funded
		FROM ".DB_CAMPAIGNS." i
		WHERE it_isPublic = '1' $whereClause
		ORDER BY $orderClause
		LIMIT $limitFrom , $limitTo";
//			concat(substring(i.it_shortdesc,1,250),if(length(i.it_shortdesc)>250,'...','')) AS itdesc
//			(SELECT sum(od_amount) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) AS it_funded

$result = sql_query($sql);
$total = mysql_num_rows($result);
$pageTotal  = ceil($total / $items);
$hug_percent = $hug_percent ? $hug_percent : 0; // 온도계 퍼센트 비율계산
?>

<ul id='campaignList'>
<?
for ($i=0; $row=sql_fetch_array($result); $i++) {
	$isVol = ($row[type] == 2) ? true : false;

	$tooltip = '오늘의 후원자 : ';

	$status = '진행중';
	if ($row[it_isEnd] == 0) {
		$diff = round((strtotime($row[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
		$d_day = ($diff < 0) ? '완료' : "D-".$diff;
		if ($diff < 0) {
			$row[it_isEnd] = 1;
			$ismain = ($d_day == '완료') ? ", it_isMain = '$row[it_isMain]' " : false;
			mysql_query("UPDATE ".DB_CAMPAIGNS." SET it_isEnd = '1' $ismain WHERE it_id = '$row[it_id]' ");
			$status = ($row[it_funded] >= $row[it_target]) ? '성공' : '완료';
		}
	} else {
		$status = ($row[it_funded] >= $row[it_target]) ? '성공' : '완료';
	}

	$itdesc = (mb_strlen($row[it_shortdesc], 'UTF-8') > 250) ? mb_substr($row[it_shortdesc], 0, 250, 'UTF-8').'...' : $row[it_shortdesc];
	$heart = $row[type] >= 100 && $row[type] <=102  ? true : false;
	$t = ($listType == 'ongoing') ? true : false;
	$h = ($t && $i == 0) ? 'highlighted' : false;
	$cl = (($t && $i == 1) || ($i+1) % 3 == 0) ? ' closed' : false;
	$ind = ($status == '완료') ? 'closed' : 'success';
	$tag = (!$isVol) ? ($status == '완료' || $status == '성공') ? "<img class='tag' src='/images/campaign/tag_${ind}.png' />" : false : "<img class='tag' src='/images/campaign/tag_volunteer.png' />";
	
	$hzh = $row[it_id] == '1371778995' ? true : false;
	$solar = $row[it_id] == $SPECIAL[solar] ? true : false;
   $times2 = $row[it_id] == $SPECIAL[sayouri] ? true : false;
   $cam_school = $row[it_id] == $SPECIAL[cam_school] ? true : false;
   $snoop_dog = $row[it_id] == $SPECIAL[snoop] ? true : false;
   $type_cw = $row[it_id] == $SPECIAL[cw1] || $row[it_id] == $SPECIAL[cw2] ? true : false;

	if(!$heart) { ?>
		<li class='<?=$h?><?=$cl?>'>
		<div class='campaignCell<?=$h?' highlighted':false?>' for='<?=$row[it_id]?>'>
		<a href='/campaign/<?=$row[it_id]?>' style='display: block'>
			<?=$tag?>
			<img class='thumbnail' src='/data/campaign/<?=$row[it_id]?>/list.jpg' />
			<div class='margin'>
				<h3><? print $isVol ? '자원봉사, ' : false; ?><?=$row[category]?></h3>
				<div class='campaignTitle'>[<?= $row[it_isEnd] != 0 ? $status : $d_day ?>] <?=$row[it_name]?></div>
				<p class='campaignDesc'><?=$itdesc?></p>
				<?
				$today = "SELECT SUM(od_amount) AS sum
				FROM ".DB_ORDERS." 
				WHERE it_id = '$row[it_id]' AND substring(od_id,1,6) = '".date('ymd')."' ";
				$today = sql_fetch($today);

				$sql = "select *
				from ".DB_ORDERS."
				where it_id = '$row[it_id]'
				and substring(od_id,1,6) = '".date('ymd')."'
				and od_amount > 0";
				$t_result = sql_query($sql);
				$t_total = mysql_num_rows($t_result);
				if ($t_total > 0) {
					for ($j=0; $t_row=sql_fetch_array($t_result); $j++) {
						$comma = ($tooltip == '오늘의 후원자 : ') ? '' : ',';
						$tooltip .= "$comma $t_row[od_name]";
					}
				}

				if ($today[sum] > 0) {
					$t_per = @floor($today[sum] * 100 / $row[it_funded]); 
				}
				if($hzh) {
					$sql = "SELECT mb_no FROM ".DB_ORDERS." WHERE it_id='1371778995' AND od_amount='20000'";
					$res = sql_query($sql);
					$total = @mysql_num_rows($res);
				}

				$vol = $isVol ? sql_fetch("SELECT COUNT(DISTINCT mb_no) AS cnt FROM wegen_volunteer WHERE it_id = '$row[it_id]' ") : false;
				if($isVol) {
					drawGauge($vol[cnt], $row[it_target], true, false, 1);
				} else if($hzh) {
					drawGauge($total, 64, true, false, 2);
				} else if($solar) {
					drawGauge($row[it_funded]*5, 250000000, true, false);
            } else if($row[it_id] == $SPECIAL[cam_hanta_bada]) {
               $it_funded = $row[it_funded] >= 2000000 ? $row[it_funded]+10000000 : $row[it_funded]*6;
               drawGauge($it_funded, 12000000, true, false);
            }
            else if($times2) {
               $funded = $row[it_funded]*2 >= 10000000 ? 10000000+$row[it_funded] : $row[it_funded]*3;
               drawGauge($funded, $row[it_target], true, false);
               if($row[type] == 300) {
                  $hug_percent += (@ceil($funded * 100 / $row[it_target])) / 3;
               }
            } else if($cam_school){
               drawGauge($row[it_funded], $row[it_target], true, false, 4);
				} else if($snoop_dog){ 
               echo "<div style='background-color:rgb(228,0,0);color:white;text-align:center;padding:11px;font:14pt NanumGothic;margin-top:14px;'><span id='snoop'>모금 완료</span></div> ";
            } else if($type_cw) {
               drawGauge($row[it_funded], $row[it_target], true, false, $DRAW_GAUGE[cw]);
            } else if($row[it_id] == $SPECIAL[kara_fan]) {
               drawGauge($row[it_funded]/5000, $row[it_target]/5000, true, false, $DRAW_GAUGE[kara_fan]);
            } else {
					drawGauge($row[it_funded], $row[it_target], true, false);
               if($row[type] == 300) {
                  $hug_percent += (@ceil($row[it_funded] * 100 / $row[it_target])) / 3;
               }
            }
				?>

			</div>
		</a>
		<?
		$check = sql_fetch("SELECT no FROM ".DB_FUNDRAISERS." WHERE it_id = '$row[it_id]' ORDER BY no ASC LIMIT 1");
		$check = ($check[no] > 0) ? $check[no] : '0';
		if (!$isVol && !$type_cw && !$hzh && ($status != '성공' && $status != '완료')) {
		?>
		<div class='donateNow'><img src='/images/common/coin.png' /> <a style='cursor: pointer' itemid='<?=$row[it_id]?>' itemname="<?=$row[it_name]?>" isEvent='<?=$check?>'>코인 후원</a></div>
		<? } ?>
		</div>
		</li>
	<? } else { ?>
		<li class='<?=$cl?>'>
			<div class='campaignCell' style='background-color:<?=$bg?>;cursor:pointer;' for='<?=$row['it_id']?>'>
				<a href='/campaign/<?=$row['it_id']?>'>
					<?=$tag?>
					<img class='thumbnail' style='' src='/data/campaign/<?=$row[it_id]?>/list.jpg' />
					<div class='margin'>
						<h3><?=$row['category']?></h3>
						<div class='campaignTitle'>[<?= $row[it_isEnd] != 0 ? $status : $d_day ?>] <?=$row[it_name]?></div>
						<?
						$c = sql_fetch("SELECT (count(*)) as count FROM ".DB_ORDERS." WHERE it_id='{$row['it_id']}'");
						$percent = $c['count'];
						$width = ($percent >= 100) ? 100 : $percent;
						$unit = '명';
						$mny = $row['it_funded']*1+10000000;
						$mny = number_format($mny);
						print "
						<div class='gauge'>
						<div class='progress' style='width: ${width}%'></div>
						<div class='percent'>${percent}%</div>
						</div>";
						print "
						<div style='text-align:center;'>
						<div class='funded' style='display:inline-block;float:left;'>${c['count']}${unit}</div>
						<div style='font: bold 11pt Arial, NanumGothic;display:inline-block;margin-top:2px;'>{$mny}원</div>
						<div class='funded' style='display:inline-block;float:right;'>100${unit}</div>
						</div>
						<div style='clear: left'></div>";
						?>
			
					</div>
				</a>
				<? if (!$isVol && ($status != '성공' && $status != '완료')) { ?>
					<div class='donateNow' style='top:240px;'><img src='/images/common/coin.png' /> <a style='cursor: pointer' itemid='<?=$row['it_id']?>' itemname="<?=$row['it_name']?>" isEvent='<?=$check?>'>코인 후원</a></div>
				<? } ?>
				
			</div>
		</li>
	<? } ?>
<? } ?>

</ul>
<div style='clear: both'></div>
