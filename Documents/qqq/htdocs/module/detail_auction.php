   <?php
      // $auc = sql_fetch("SELECT * FROM ".DB_AUCTION." WHERE auc_id = '$it[it_auction]'");
      // echo "SELECT * FROM ".DB_AUCTION." WHERE it_id = '$it[it_id]'";
      $arr = sql_query("SELECT * FROM ".DB_AUCTION." WHERE it_id = '$it[it_id]'");
      $idx=0;
      while($auc = sql_fetch_array($arr)){

      if($auc[is_show] == 1) {
         $idx++;
         $_SESSION[auc_id] = $it[it_auction];
         $bid_res = sql_query("SELECT * FROM ".DB_BIDDING." a 
            LEFT JOIN ".DB_MEMBERS." b ON b.mb_no = a.mb_no
            WHERE a.auc_id = $auc[auc_id] AND a.type = 1
            ORDER BY bid_amount DESC, bid_time ASC");
         $bid_count = mysql_num_rows($bid_res);
         $bid_max = sql_fetch("SELECT IFNULL(MAX(bid_amount), 0) AS bid_amount FROM ".DB_BIDDING." 
            WHERE auc_id = $auc[auc_id] AND type = 1");
   ?>

   <div class='auc_det_outer<?=$idx?> auc_det_outer'>
      <div class='right_corner'>* 경매 수익은 본 캠페인에 함께 기부됩니다.</div>
      <div class='content'>
         <div class='content2'>
            <div class='content_title'>스타 애장품 자선 경매</div>
            <div class='content_cont'>
               <div class='content_left'>
                  <img src="/data/campaign/<?=$it_id?>/auc_main<?=$idx?>.jpg" alt="<?=$auc[auc_desc]?>">
               </div>
               <div class='content_right'>
                  <div class='_1'>
                     <div class='_1_left'>
                        <img src="/images/campaign/sandglass.png">
                     </div>
                     <div class='_1_right'>
                        <div class='_1_right_tit'>남은 시간</div>
                     <div class="_1_right_ctt auc_time<?=$idx?>"></div>
                  </div>
                  </div>
                  <div class='_2'>
                     <div class='_2_left'>
                        <img class='wd53'src="/images/campaign/auc_won.png">
                     </div>
                     <div class='_2_right'>
                        <div class='_2_right_tit'>자선경매 최고액</div>
                        <div class="_2_right_ctt _2_right_ctt<?=$idx?>"><?=number_format($bid_max[bid_amount])?></div>
                     </div>
                  </div>
                  <div class='auc_layout_open<?=$idx?> auc_layout_open' <?=$_SESSION[user_no] ? "rel='.auc_pop_outer'": ""?> style='cursor:pointer;'>
                     <img src="/images/campaign/auc_join_btn.png">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   
   <script src='/js/jquery.countdown.min.js'></script>



   <script>
         $(document).ready(function() {

      // $('#addr').hide();

         $('#dongName<?=$idx?>').val('동/리/건물명 입력').addClass('label').focusin(function() {
            if (!$(this).val() || $(this).val() == '동/리/건물명 입력') {
               $(this).val('').removeClass('label');
            }
         }).focusout(function() {
            if (!$(this).val()) {
               $(this).val('동/리/건물명 입력').addClass('label');
            }
         });

         if (!$('input[name=mb_addr2]').val()) {
            $('input[name=mb_addr2]').val('상세주소 입력').addClass('label');
         }
         $('input[name=mb_addr2]').focusin(function() {
            if (!$(this).val() || $(this).val() == '상세주소 입력') {
               $(this).val('').removeClass('label');
            }
         }).focusout(function() {
            if (!$(this).val()) {
               $(this).val('상세주소 입력').addClass('label');
            }
         });

         address.bindZipcodeFind();
      });

      function doSearch(idx) {
         
         $('.zipcode-search-result'+idx).show().text("검색중...");
         $.get('/module/ajax.zipsearch.php',{
            query: $('#dongName'+idx).val()
         },function(data){
            
            $('.zipcode-search-result'+idx).html(data);
            address.bindPutAddress(idx);
         })
      }

      var address = {
         bindZipcodeFind: function(){

            $('.zipcode-search<?=$idx?>').click(doSearch);
            $('#dongName<?=$idx?>').keypress(function(e) { if(e.which == 13) { doSearch(); return false; }});
         },
         bindPutAddress: function(idx){
            $('.zipcode-search-result'+idx+' a').click(function(){
               
               var first = $('.zipcode-search-result'+idx).data('name') == undefined ? '' : $('.zipcode-search-result'+idx).data('name') ;
               first = first != '' ? first : 'od';
               
               $('[name='+first+'_zip1'+idx+']').val($(this).parent().parent().find('.postcd1').text());
               $('[name='+first+'_zip2'+idx+']').val($(this).parent().parent().find('.postcd2').text());
               $('[name='+first+'_addr1'+idx+']').val(address.remove_useless_addr($(this).parent().parent().find('.address').text()));
               address.hideZipcodeFinder(idx);
               $('#addr').show();
               return false;
            });
         },
         remove_useless_addr: function(address){
            if(address.indexOf('~') != -1){
               address = address.split(' ').slice(0,-1).join(' ');
            }
            return address;
         },
         hideZipcodeFinder: function(idx){
            $('.zipcode-search-result'+idx).slideUp();
            $('#dongName'+idx).val('동/리/건물명').addClass('label');
         }
      }



      $(document).ready(function() { 
         $('input[type=text]').addClass('text');
            $('.auc_layout_open<?=$idx?>').click(function() {
               createLayer($('.auc_pop_outer<?=$idx?>'));
            });
            $('.auc_pop_outer<?=$idx?> .close').click(function() {
               destroyLayer($('.auc_pop_outer<?=$idx?>'));
            })
         $(".auc_time<?=$idx?>").countdown({
            until : new Date(<?=date("Y,m-1,d,H,i,s",strtotime($auc[auc_enddate]))?>),
            layout : '{dn}일 {hn}시간 {mn}분 {sn}초'
         });
         $('input[name=bid_amount<?=$idx?>]').keyup(function(e) {
            var keyCode = e.which ? e.which : e.keyCode;
            if(keyCode == 13) {
               $(".bid_btn<?=$idx?>").click();
            }
         });
         $(".bid_btn<?=$idx?>").click(function() {
            var post_data = {};
            if(!$("input[name=bid_amount<?=$idx?>]").val()) {
               qAlert('입찰금액을 입력해주세요');
               $("input[name=bid_amount<?=$idx?>]").focus();
               return false;
            }
            if($("input[name=bid_amount<?=$idx?>]").val()*1 < '<?=$auc[auc_min]?>'*1) {
               qAlert('이 캠페인의 입찰최소금액은 '+number_format(<?=$auc[auc_min]?>)+'원 입니다.');
               $('input[name=bid_amount<?=$idx?>]').focus();
               return false;
            }
            
            if($("input[name=bid_amount<?=$idx?>]").val()*1 % 1000 != 0) {
               qAlert('천원 단위의 금액만 입찰이 가능합니다.');
               $('input[name=bid_amount<?=$idx?>]').focus();
               return false;
            }

            if($(".list tr[data-chkme<?=$idx?>]").length == 1) {
               if($("input[name=bid_amount<?=$idx?>]").val() < $(".list tr[data-chkme<?=$idx?>]").data('amount')*1) {
                  qAlert('입찰했던 금액보다 낮게 입찰할 수 없습니다.');
                  $('input[name=bid_amount<?=$idx?>]').focus();
                  return false;
               }
            }

            post_data.bid_amount = $('input[name=bid_amount<?=$idx?>]').val();

            if(!$('input[name=mb_name<?=$idx?>]').val()) {
               qAlert('이름을 입력해주세요');
               $('input[name=mb_name<?=$idx?>]').focus();
               return false;
            }
            post_data.mb_name = $('input[name=mb_name<?=$idx?>]').val();

            if(!$('input[name=mb_hp1<?=$idx?>]').val()) {
               qAlert('전화번호를 입력해주세요');
               $('input[name=mb_hp1<?=$idx?>]').focus();
               return false;
            }
            if(!$('input[name=mb_hp2<?=$idx?>]').val()) {
               qAlert('전화번호를 입력해주세요');
               $('input[name=mb_hp2<?=$idx?>]').focus();
               return false;
            }
            if(!$('input[name=mb_hp3<?=$idx?>]').val()) {
               qAlert('전화번호를 입력해주세요');
               $('input[name=mb_hp3<?=$idx?>]').focus();
               return false;
            }
            var mb_contact = $('input[name=mb_hp1<?=$idx?>]').val()+'-'+$('input[name=mb_hp2<?=$idx?>]').val()+'-'+$('input[name=mb_hp3<?=$idx?>]').val();
            if(!phone_check(mb_contact)) {
               qAlert('전화번호 형식이 잘못되었습니다.');
               $('input[name=mb_hp1<?=$idx?>]').focus();
               return false;
            }
            post_data.mb_contact = mb_contact;

            if(!$('input[name=mb_email<?=$idx?>]').val()) {
               qAlert('이메일을 입력해주세요.');
               $('input[name=mb_email<?=$idx?>]').focus();
               return false;
            }
            if(!email_check($('input[name=mb_email<?=$idx?>]').val())) {
               qAlert('이메일 형식을 확인해주세요.');
               $('input[name=mb_email<?=$idx?>]').focus();
               return false;
            }
            post_data.mb_email = $('input[name=mb_email<?=$idx?>]').val();

            if(!$('input[name=mb_zip1<?=$idx?>]').val() || !$('input[name=mb_zip2<?=$idx?>]').val() || !$('input[name=mb_addr1<?=$idx?>]').val() || 
            !$("input[name=mb_addr2<?=$idx?>]").val()) {
               qAlert('주소를 입력해주세요.');
               $('#dongName<?=$idx?>').focus();
               return false;
            }
            post_data.a_id = <?=$auc[auc_id]?>;
            post_data.mb_zip1 = $('input[name=mb_zip1<?=$idx?>]').val();
            post_data.mb_zip2 = $('input[name=mb_zip2<?=$idx?>]').val();
            post_data.mb_addr1 = $('input[name=mb_addr1<?=$idx?>]').val();
            post_data.mb_addr2 = $('input[name=mb_addr2<?=$idx?>]').val();

            ajaxRequest('request_bid',post_data,<?=$idx?>);
         });

      });
    </script>
	
   <div class='auc_pop_outer<?=$idx?> auc_pop_outer'>
      <div class='close'>X</div>
      <div class='content'>
         <div class="content_tit">스타 애장품 자선 경매</div>
         <div class="content_info">
            <div class="info_left">
               <img src="/data/campaign/<?=$it_id?>/auc_main<?=$idx?>.jpg" alt="<?=$auc[auc_desc]?>">
            </div>
            <div class="info_right">
               <div class="_1">
                  <div class="_1_left">
                     <img src="/images/campaign/sandglass.png">
                  </div>
                  <div class="_1_right">
                     <div class='_1_right_tit'>남은 시간</div>
                     <div class="_1_right_ctt auc_time<?=$idx?>"></div>
                  </div>
               </div>
               
               <div class="_2">
                  *상품 설명:<br/>
                  <span class='desc'><?=$auc[auc_desc]?></span>
               </div>
               
               <div class="_2">
                  *기간: <?=date('Y.m.d',strtotime($auc[auc_startdate]))?> ~ 
                  <span class='red'><?=date('Y.m.d H:i:s',strtotime($auc[auc_enddate]))?></span>
               </div>
               
               <div class="_2">
                  *입금 기한: 경매 완료 다음 날<br/>
                  (24시간 이내 미입금시에는, 다음 분께 낙찰기회를 양도합니다)
               </div>
            </div>
         </div>
            
         <div class="content_bid">
            <table class="list">
               <tr class='top_row<?=$idx?>'>
                  <td class='rank'>순위</td>
                  <td>이름</td>
                  <td class='amount'>입찰가(원)</td>
                  <td>입찰시간</td>
               </tr>
            <? if($bid_count == 0)  {  ?>
               <tr class='empty' data-num='0'><td colspan="4">입찰한 사람이 없습니다</td></tr>
            <? } else { ?>

            <? for($i =1; $row = sql_fetch_array($bid_res);$i++) {
               $tmp = mb_convert_encoding($row[mb_name],'euc-kr','utf-8');
               $view_name = mb_convert_encoding(substr($tmp,0,3),'utf-8','euc-kr').'***';
               
            ?>
               <tr data-num='<?=$i?>' <?=$_SESSION[user_no] == $row[mb_no]? "data-chkme='1'" : false?> data-amount='<?=$row[bid_amount]?>'>
                  <td><?=$i?></td>
                  <td><?=$view_name?></td>
                  <td><?=number_format($row[bid_amount])?></td>
                  <td><?=$row[bid_time]?></td>
               </tr>

            <? } ?>


            <? } ?>

            </table>
         </div>

         <? $my = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]'"); 
            $contact = explode("-",$my[mb_contact]);
         ?>
         <div class="content_mb">
            <table class="mb_info">
               <tr>
                  <th>이름 (실명)</th>
                  <td colspan="3"><input type='text' name="mb_name<?=$idx?>" value="<?=$my[mb_name]?>"/></td>
               </tr>
               <tr>
                  <th>전화번호</th>
                  <td>
                     <input type='text' name='mb_hp1<?=$idx?>' maxlength='4' class='numonly mb_hp' value='<?=$contact[0]?>' style="width:30px"/> - 
                     <input type='text' name='mb_hp2<?=$idx?>' maxlength='4' class='numonly mb_hp' value='<?=$contact[1]?>' style="width:30px"/> - 
                     <input type='text' name='mb_hp3<?=$idx?>' maxlength='4' class='numonly mb_hp' value='<?=$contact[2]?>' style="width:30px"/>
                  </td>
                  <th>이메일</th>
                  <td><input type='text' name='mb_email<?=$idx?>' style='width:200px;' value='<?=$my[mb_email]?>'/></td>
               </tr>
               <tr>
                  <th>주소</th>
                  <td colspan="3">
                     <div class='zipcode-finder'>
                        <input type='text' id="dongName<?=$idx?>" />
                        <input type='button' class='zipcode-search<?=$idx?>' value='검색' onclick='doSearch(<?=$idx?>)'/>
                        <div class="zipcode-search-result<?=$idx?>" data-name='mb'></div>
                     </div>
                     <div id='addr'>
                        <input type='text' name="mb_zip1<?=$idx?>" size='3' maxlength='3' value='<?=$my[mb_zip1]?>' /> -
                        <input type='text' name="mb_zip2<?=$idx?>" size='3' maxlength='3' value='<?=$my[mb_zip2]?>' /><br/>
                        <input type='text' name="mb_addr1<?=$idx?>" style='width: 180px' value='<?=$my[mb_addr1]?>' />
                        <input type='text' name="mb_addr2<?=$idx?>" style='width: 230px' value='<?=$my[mb_addr2]?>'/>
                     </div>
                  </td>
               </tr>
            </table>
         </div>
            
         <div class="content_input">
            <div class=<?=$_SESSION[user_no]? "bid_btn$idx": "btn_login"?>>
               <img src="/images/campaign/auc_bid_btn.png">
            </div>
            <div class="won">원</div>
            <div class="input_mny">
               <span>입찰가 </span><input type='text' name='bid_amount<?=$idx?>' maxlength='9' class='numonly' /> 
            </div>
         </div>
      </div>
   </div>
   
<? }} ?>
