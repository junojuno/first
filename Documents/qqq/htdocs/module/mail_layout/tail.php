<?php
   $tail_info = sql_fetch("SELECT * FROM ".DB_INFO." LIMIT 1");
?>
<div>
   <? if(!$deny) { ?>
   <div style='text-align:center;padding:18px 0;background-color:rgb(230,230,230);font:12px NanumGothic;'>
      본 메일은 발신전용입니다. 위제너레이션 <a href='http://<?=$_SERVER[HTTP_HOST]?>/my' style='cursor:pointer;color:red;text-decoration:underline;'>마이페이지</a>에서 
      <a href='http://<?=$_SERVER[HTTP_HOST]?>/my/modify' style='cursor:pointer;color:red;text-decoration:underline;'>수신여부</a>를 설정할 수 있습니다.
   </div>
   <? } ?>

   <div style='width:100%; text-align:center; background-color:#373737;'>
      <div style='width:850px; text-align: left; margin: 0px auto;  padding: 40px 0px; font: 9pt/18px NanumGothic; color: #B9B9B9;'>
            <img src="http://<?=$_SERVER[HTTP_HOST]?>/images/common/copyright.png" style='float: left; margin-right: 15px' />
            위제너레이션 | <?=$tail_info[opt_address]?> | 대표 : <?=$tail_info[opt_ceo]?> <br>
            사업자등록번호 : <?=$tail_info[opt_corp_num]?> | 통신판매업신고번호 : <?=$tail_info[opt_comm_num]?> | 개인정보관리책임자 : <?=$tail_info[opt_ceo]?> <br>
            대표전화번호 :  <?=$tail_info[opt_tel]?> | <? if(!$tail_info[opt_fax]) echo "팩스 : $tail_info[opt_fax] | "; ?>이메일 : <?=$tail_info[opt_email]?>  <br><br>
            <strong>COPYRIGHTⓒ 2012 WEGENERATION. ALL RIGHTS RESERVED.</strong>
      </div>
   </div>
</div>
