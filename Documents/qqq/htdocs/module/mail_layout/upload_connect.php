<?php
   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   $cmt_id = $_POST[cmt_id];
   $it_id = $_POST[it_id];
   $connect = sql_fetch("SELECT * FROM ".DB_CAMPAIGN_CMTS." a 
         LEFT JOIN ".DB_MEMBERS." b ON b.mb_no = a.mb_no 
         WHERE cmt_id = '$cmt_id'");
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title></title>
</head>
<body>
   <div style='width:900px;margin:0 auto;font-family:NanumGothic;color:#555;'>
      <div style='margin:30px 0 40px;'>
         <div style='text-align:right;margin-bottom:5px;'>
            <a href='http://<?=$_SERVER[HTTP_HOST]?>/'><img style='width:200px;' src="http://<?=$_SERVER[HTTP_HOST]?>/images/common/logo.png" alt="WEGENERATION"></a>
         </div>
         <div style='text-align:center;line-height:33px;background-color:rgb(228,0,0);padding: 35px 0 17px;'>
            <span style='font-size:25px;color:white;'>참여하신 캠페인에 새로운 수혜자 소식이 등록되었습니다.</span><br/>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>/connect" style='text-decoration:none;text-decoration:underline;color:white;font-size:15px;'>
               캠페인 수혜자 TALK 바로가기
            </a>
         </div>
      </div>

      <div style='margin-bottom:30px;text-align:center;'>
         <div style='display:inline-block;margin:0 auto;text-align:left;min-width:500px;border:1px solid lightgray;padding:20px;max-width:860px;'>
            <div style='display:block;margin-bottom:5px;'>
               <div style='width:40px;display:block;float:left;margin-right:8px;'>
                  <img src="<?=getProfilePhotoUrl($connect[mb_no])?>" style='width:100%;cursor:pointer;outline:solid 1px #AFAFAF;border:0;'/>
               </div>
               <div style='display:table-cell;'>
                  <div style='line-height:20px;margin-top:2px;'>
                     <span style='font-size:14px; font-weight:bold;'><?=$connect[mb_name]?></span><br/><span style='font-size:9pt;color:#a8a8a8;'><?=$connect[cmt_time]?></span><br/>
                  </div>
               </div>
            </div>
            <div style='font-size:14px;padding:0 15px;margin:0;overflow:auto;white-space:normal;' >
               <?=$connect[cmt]?>
            </div>
         </div>
      </div>

      <div style='text-align:right;font-size:16px; margin-bottom:60px;'>
         <strong>
            캠페인의 주인공들을 직접 만나보세요! 
            <a style='color:red;text-decoration:underline;cursor:pointer;' href='http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>/connect'>더 보기</a>
         </strong>
      </div>

      <? include 'tail.php'; ?>
      

   </div>
</body>
</html>
