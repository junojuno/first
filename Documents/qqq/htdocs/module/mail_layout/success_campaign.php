<?php
   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   $it_id = $_POST[it_id];
   if($it_id == '') exit;

   $success_cam = sql_fetch("SELECT i.*, (SELECT sum(od_amount)-sum(pay_remain) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) AS it_funded FROM ".DB_CAMPAIGNS." i WHERE it_id = '$it_id'");
   $diff = round((strtotime($success_cam[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
   $diff = "D-".$diff; 
	$itdesc = (mb_strlen($success_cam[it_shortdesc], 'UTF-8') > 250) ? mb_substr($success_cam[it_shortdesc], 0, 250, 'UTF-8').'...' : $success_cam[it_shortdesc];

   $funded = $success_cam[it_funded];
   $targeted = $success_cam[it_target];
	$percent = @ceil($funded * 100 / $targeted); 
	//$percent = ($funded > 0 && $percent == 0) ? 1 : $percent;
	$percent = ($percent == 100 && $funded < $targeted ) ? 99 : $percent;
	$width = ($percent >= 100) ? 100 : $percent;
	$funded = number_format($funded);
	$targeted = number_format($targeted);
   $unit = '원';
	$gauge_html = "
			<div style='height:27px;margin-bottom:8px;position:relative;margin-top:12px;width:100%;border:solid 1px #c6c6c6;text-align:right;background-color:#e30000;'>
            <div style='display:inline-block;font:bold 14pt Arial;color:white;z-index:1;margin-right:4px;margin-top:2px;'>${percent}%</div>
			</div>";
	$gauge_html .= "
			<span style='font:bold 12pt Arial, NanumGothic;color:black;'>{$funded}${unit}</span>
			<span style='float:right;font:bold 12pt Arial, NanumGothic;color:black;'>${targeted}${unit}</span>
			<div style='clear: left'></div>";


   $campaigns_res = sql_query("SELECT i.*, (SELECT sum(od_amount)-sum(pay_remain) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) AS it_funded FROM ".DB_CAMPAIGNS." i
      WHERE it_isPublic = 1 AND it_isMain > 0 AND it_isEnd = 0 AND type < 100 AND it_id != '$it_id'
      ORDER BY it_isHighlighted DESC, (it_funded / it_target) DESC 
      LIMIT 0, 3");
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title></title>
</head>
<body>
   <div style='width:900px;margin:0 auto;font-family:NanumGothic;color:#555;'>
      <div style='margin:30px 0 60px;'>
         <div style='text-align:right;margin-bottom:5px;'>
            <a href='http://<?=$_SERVER[HTTP_HOST]?>/'><img style='width:200px;' src="http://<?=$_SERVER[HTTP_HOST]?>/images/common/logo.png" alt="WEGENERATION"></a>
         </div>
         <div style='text-align:center;line-height:33px;background-color:rgb(228,0,0);padding: 35px 0 17px;'>
            <span style='font-size:25px;color:white;'>감사합니다. 참여하신 캠페인이 목표 모금액을 돌파했습니다!</span><br/>
         </div>
      </div>

      <div style='margin-bottom:40px;text-align:center;'>
         <a href="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$it_id?>" style="cursor:pointer;text-decoration:none;">
         <div style='display:inline-block;text-align:left;border-color:rgb(226,226,223);width:600px;margin:0 auto 36px auto;position:relative;border:solid 2px #E2E2DF;background-color:#F4F4E3;'>
            <div style="background-image:url('http://<?=$_SERVER[HTTP_HOST]?>/data/campaign/<?=$it_id?>/list.jpg');background-repeat:no-repeat;width:100%;height:370px;">
               <img src='http://<?=$_SERVER[HTTP_HOST]?>/images/campaign/tag_success.png' />
            </div>
            <div style="padding:15px;">
               <h3 style="margin-top:20px;font:11pt/150% NanumGothic;font-weight:bold;color:#E30000;margin:0 0 4px;border:none;"><?=$success_cam[category]?></h3>
               <div style="font-size:18px;color:#222;text-shadow:0px 1px #BFBFBF;font-weight:bold;">[<?=$success_cam[it_isEnd] != 0 ? '성공' : $diff ?>] <?=$success_cam[it_name]?></div>
               <p style="margin:10px 0; font:10pt/180% NanumGothic;color:gray;text-align:justify;">
                  <?=$itdesc?>
               </p>
               <?=$gauge_html?>
            </div>
         </div>
         </a>
      </div>

      <h2 style='font-size:20px;color:black;border-bottom:2px solid red;padding-bottom:5px;text-shadow: 0px 1px #BFBFBF;position:relative;margin-bottom:15px;'>캠페인 둘러보기
         <a href="http://<?=$_SERVER[HTTP_HOST]?>" style="text-decoration:none;float:right;font-weight:bold;font-size:15px;color:red;cursor:pointer">더 보기 ></a>
      </h2>
      <div style='text-align:center;margin-bottom:60px;'>
         <div>
         <? for($i =1; $data = sql_fetch_array($campaigns_res); $i++) { 
            $diff = round((strtotime($data[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
            $diff = "D-".$diff; 
            $margin = $i%3 != 0 ? "margin-right:25px;" : false;
         ?>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$data[it_id]?>" style="text-decoration:none;color:black;cursor:pointer;">
               <div style="display:inline-block;width:270px;<?=$margin?>">
                  <div style="margin-bottom:10px;">
                     <img style="width:100%" src="<?="http://$_SERVER[HTTP_HOST]/data/campaign/$data[it_id]/list.jpg"?>" />
                  </div>
                  <div style="font-size:16px;font-weight:bold;text-align:left;padding-left:3px;">[<?=$diff?>] <?=$data[it_name]?></div>
               </div>
            </a>
         <? } ?>
         </div>
         
      </div>

      <? include 'tail.php'; ?>

</body>
</html>
