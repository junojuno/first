<?php
   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   $wr_id = $_POST[wr_id];
   $data = sql_fetch("SELECT * FROM ".DB_POSTSCRIPTS." WHERE wr_id = $wr_id");

   $campaigns_res = sql_query("SELECT i.*, (SELECT sum(od_amount)-sum(pay_remain) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) AS it_funded FROM ".DB_CAMPAIGNS." i
      WHERE it_isPublic = 1 AND it_isMain > 0 AND it_isEnd = 0 AND type < 100 AND it_id != '$data[wr_campaign]'
      ORDER BY it_isHighlighted DESC, (it_funded / it_target) DESC 
      LIMIT 0, 3");

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>[위제너레이션] 후기 메일</title>
</head>
<body>
   <div style='width:900px;margin:0 auto;font-family:NanumGothic;color:#555;'>
      <div style='margin:30px 0 40px;'>
         <div style='text-align:right;margin-bottom:5px;'>
            <a href='http://<?=$_SERVER[HTTP_HOST]?>/'><img style='width:200px;' src="http://<?=$_SERVER[HTTP_HOST]?>/images/common/logo.png" alt="WEGENERATION"></a>
         </div>
         <div style='text-align:center;line-height:33px;background-color:rgb(228,0,0);padding: 35px 0 17px;'>
            <span style='font-size:25px;color:white;'>참여하신 캠페인의 따끈따끈한 후기가 도착했습니다</span><br/>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$data[wr_campaign]?>" style='text-decoration:none;text-decoration:underline;color:white;font-size:15px;'>
               캠페인 내용 다시보기
            </a>
         </div>
      </div>

      <div style='margin-bottom:60px;'>
         <table style='width: 700px;margin:0 auto;border:0;'>
            <tr>
               <td style='font-size:18px;font-weight:bold;border-bottom:1px solid #e9e9e9;height:40px;'><?=$data[wr_subject]?></td>
               <td style='text-align: right; font: 12px Arial;border-bottom:1px solid #e9e9e9;'><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])));?></td>
            </tr>
            <tr>
               <td colspan='2' style='border-bottom:1px solid #e9e9e9;max-width: 700px;position:relative;padding:10px 15px 10px;overflow:auto;font-size:13px;'>
                  <?=$data[wr_content]?>
               </td>
            </tr>
         </table>
      </div>

      <h2 style='font-size:20px;color:black;border-bottom:2px solid red;padding-bottom:5px;text-shadow: 0px 1px #BFBFBF;position:relative;margin-bottom:15px;'>캠페인 둘러보기
         <a href="http://<?=$_SERVER[HTTP_HOST]?>" style="text-decoration:none;float:right;font-weight:bold;font-size:15px;color:red;cursor:pointer">더 보기 ></a>
      </h2>
      <div style='text-align:center;margin-bottom:60px;'>
         <div>
         <? for($i =1; $data = sql_fetch_array($campaigns_res); $i++) { 
            $diff = round((strtotime($data[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
            $diff = "D-".$diff; 
            $margin = $i%3 != 0 ? "margin-right:25px;" : false;
         ?>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$data[it_id]?>" style="text-decoration:none;color:black;cursor:pointer;">
               <div style="display:inline-block;width:270px;<?=$margin?>">
                  <div style="margin-bottom:10px;">
                     <img style="width:100%" src="<?="http://$_SERVER[HTTP_HOST]/data/campaign/$data[it_id]/list.jpg"?>" />
                  </div>
                  <div style="font-size:16px;font-weight:bold;text-align:left;padding-left:3px;">[<?=$diff?>] <?=$data[it_name]?></div>
               </div>
            </a>
         <? } ?>
         </div>
      </div>

      <? include 'tail.php';?>
      
   </div>
   
</body>
</html>
