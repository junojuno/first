<?
   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';
   $it_id = $_POST[it_id];
   $good_name = $_POST[good_name];
   $username = $_POST[od_name];
   $ytid = $_POST[youtube_id];
   $use_pay_method = $_POST[use_pay_method];
   $good_mny = $_POST[good_mny];
   $tno = $_POST[tno];
   $donate_num = $_POST[donate_num];
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>[위제너레이션] 후원 감사 메일</title>
</head>
<body>
   <div style='width:900px;margin:0 auto;font-family:NanumGothic'>
      <div style='margin:30px 0;'>
         <div style='text-align:right;margin-bottom:5px;'>
            <a href='http://<?=$_SERVER[HTTP_HOST]?>/'><img style='width:200px;' src="http://<?=$_SERVER[HTTP_HOST]?>/images/common/logo.png" alt="WEGENERATION"></a>
         </div>
         <div style='text-align:center;line-height:33px;background-color:rgb(228,0,0);padding: 35px 0 17px;'>
            <span style='font-size:25px;color:white;'>축하합니다. <?=$username?> 님의 <?=$donate_num?> 번째 후원입니다!</span><br/>
            <!-- <span style='font-size:15px; color:white;'>참여하신 캠페인의 이벤트 추첨일이 10일 남았습니다</span> -->
         </div>
      </div>

      <? if($ytid != '') { ?>

      <h2 style='font-size:20px;color:black;border-bottom:2px solid red;padding-bottom:5px;text-shadow: 0px 1px #BFBFBF;margin-bottom:15px;'>감사영상</h2>
      <div style='text-align:center;margin-bottom:40px;'>
         <table style='width:880px;margin:0 auto;'>
            <tr>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>링크</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' >
                  <a href='http://www.youtube.com/watch?v=<?=$ytid?>' style="cursor:pointer;">http://www.youtube.com/watch?v=<?=$ytid?></a>
               </td>
            </tr>
         </table>
      </div>
      <? } ?>

      <h2 style='font-size:20px;color:black;border-bottom:2px solid red;padding-bottom:5px;text-shadow: 0px 1px #BFBFBF;margin-bottom:15px;'>후원결과</h2>
      <div style='text-align:center;margin-bottom:40px;'>
         <table style='width:880px;margin:0 auto;'>
            <tr>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>후원 캠페인명</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' ><?=$good_name?></td>
            </tr>
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>후원 금액</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'><?=$good_mny?></th>
            </tr>
            
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>후원 수단</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'>
                  <?=$use_pay_method == "COIN" ? "코인" : $use_pay_method?>  
                  <?=$use_pay_method == "신용카드" ? "<a href=\"https://admin8.kcp.co.kr/assist/bill.BillAction.do?cmd=card_bill&tno=$tno\">[영수증 출력]</a>" : false ?>
               </td>
            </tr>
            <? if ( $use_pay_method == "100000000000" ) { ?>
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>후원 수단</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'>신용카드 <a href="https://admin8.kcp.co.kr/assist/bill.BillAction.do?cmd=card_bill&tno=<?=$tno?>">[영수증 출력]</a></td>
            </tr>
            <?	} else if ($use_pay_method == "010000000000") { ?>
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;' >후원 수단</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'>계좌이체</td>
            </tr>
            <?	} else if ($use_pay_method == "001000000000") { ?>
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;' >후원 수단</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'>가상계좌</td>
            </tr>
            <? } else if ( $use_pay_method == "000010000000" ) { ?>
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>후원 수단</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'>핸드폰</td>
            </tr>
            <? } else if ( $use_pay_method == 'COIN') { ?>
            <tr>
               <th style='text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>후원 수단</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;'>코인</td>
            </tr>
            <? } ?>
         </table>
      </div>

      <div style='text-align:right;font-size:16px; margin-bottom:60px;'>
         <strong>지금 더 많은 스타 캠페인을 만나보세요! <a style='color:red;text-decoration:underline;cursor:pointer;' href='http://<?=$_SERVER[HTTP_HOST]?>/'>더 보기</a></strong>
      </div>

      <? $deny = true; include 'tail.php'; ?>
   </div>
</body>
</html>
