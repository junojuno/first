
<?
   $_required = true;
   include $_SERVER[DOCUMENT_ROOT].'/config.php';

   $mb_id = $_POST[mb_id];
   $mb_name = $_POST[mb_name];
   $mb_contact = $_POST[mb_contact];
   $agree_sms = $_POST[agree_sms];
   $agree_email = $_POST[agree_email];
   $mb_email = strpos($mb_id, "Facebook") || strpos($mb_id,"Twitter") ? "" : $mb_id;

   $campaigns_res = sql_query("SELECT i.*, (SELECT sum(od_amount)-sum(pay_remain) FROM ".DB_ORDERS." o WHERE o.it_id = i.it_id) AS it_funded FROM ".DB_CAMPAIGNS." i
      WHERE it_isPublic = 1 AND it_isMain > 0 AND it_isEnd = 0 AND type < 100 
      ORDER BY it_isHighlighted DESC, (it_funded / it_target) DESC 
      LIMIT 0, 3");

?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title></title>
</head>
<body>
   <div style='width:900px;margin:0 auto;font-family:NanumGothic'>
      <div style='margin:30px 0;'>
         <div style='text-align:right;margin-bottom:5px;'>
            <a href='http://<?=$_SERVER[HTTP_HOST]?>/'><img style='width:200px;' src="http://<?=$_SERVER[HTTP_HOST]?>/images/common/logo.png" alt="WEGENERATION"></a>
         </div>
         <div style='text-align:center;line-height:33px;background-color:rgb(228,0,0);padding: 35px 0 17px;'>
            <span style='font-size:25px;color:white;'>반갑습니다! 가입해주셔서 감사합니다.</span><br/>
            <span style='font-size:15px; color:white;'>지금 바로 스타와의 특별한 경험을 시작해보세요.</span>
         </div>
      </div>

      <h2 style='font-size:20px;color:black;border-bottom:2px solid red;padding-bottom:5px;text-shadow: 0px 1px #BFBFBF;margin-bottom:15px;'>회원등록정보</h2>
      <div style='text-align:center;margin-bottom:40px;'>
         <table style='width:880px;margin:0 auto;'>
            <tr>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>아이디</th>
               <td colspan="3" style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' ><?=$mb_id?></td>
            </tr>
            <tr>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>휴대폰번호</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' ><?=$mb_contact?></td>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>SMS 수신동의</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' ><?=$agree_sms?></td>
            </tr>
            <tr>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>이메일</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' ><?=$mb_email?></td>
               <th style='width:144px;text-align:center;background-color:rgb(247,247,247);border-bottom: solid 1px #E9E9E9; height: 40px;'>이메일 수신동의</th>
               <td style='text-align:left;padding-left:10px;border-bottom: solid 1px #E9E9E9; height: 40px;' ><?=$agree_email?></td>
            </tr>
         </table>
         <div style='text-align:center;margin-top:25px;'>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>/my/modify" style="text-decoration:none;color:white;cursor:pointer">
               <div style='width:160px;font-size:15px;text-align:center;font-weight:bold;margin-right:5px;padding:11px 0 8px;background-color:rgb(228,0,0);display:inline-block;'>
                  회원정보 변경하기
               </div>
            </a>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>" style="text-decoration:none;color:white;cursor:pointer">
               <div style='width:160px;font-size:15px;font-weight:bold;text-align:center;margin-right:5px;padding:11px 0 8px;background-color:rgb(130,130,130);display:inline-block'>
                  위젠 바로가기
               </div>
            </a>
         </div>
      </div>
      
      <h2 style='font-size:20px;color:black;border-bottom:2px solid red;padding-bottom:5px;text-shadow: 0px 1px #BFBFBF;position:relative;margin-bottom:15px;'>캠페인 둘러보기
         <a href="http://<?=$_SERVER[HTTP_HOST]?>" style="text-decoration:none;float:right;font-weight:bold;font-size:15px;color:red;cursor:pointer">더 보기 ></a>
      </h2>
      <div style='text-align:center;margin-bottom:60px;'>
         <div>
         <? for($i =1; $data = sql_fetch_array($campaigns_res); $i++) { 
            $diff = round((strtotime($data[it_enddate]) - strtotime(date('Y-m-d'))) / (60*60*24));
            $diff = "D-".$diff; 
            $margin = $i%3 != 0 ? "margin-right:25px;" : false;
         ?>
            <a href="http://<?=$_SERVER[HTTP_HOST]?>/campaign/<?=$data[it_id]?>" style="text-decoration:none;cursor:pointer;">
               <div style="display:inline-block;width:270px;<?=$margin?>">
                  <div style="margin-bottom:10px;">
                     <img style="width:100%" src="<?="http://$_SERVER[HTTP_HOST]/data/campaign/$data[it_id]/list.jpg"?>" />
                  </div>
                  <div style="font-size:16px;font-weight:bold;text-align:left;padding-left:3px;color:black;">[<?=$diff?>] <?=$data[it_name]?></div>
               </div>
            </a>
         <? } ?>
         </div>
         
      </div>

      <? include 'tail.php'; ?>
   </div>
   
</body>
</html>
