<?php
	if (!$_required) {
		header("HTTP/1.0 404 Not Found");
		exit;
	}
	
	sec_session_start();
	
	$company = explode('/', $_SERVER['REQUEST_URI']);
	switch($company[2]) {
		case "hellojeju" :
			$company = "hellojeju";
         $company_kr = "헬로제주";
         $company_email = "hellojeju.com";
         $company_admin = $arr_btb['hellojeju'];
         $company_type = 5;
			$authority = $settings[hellojeju];
			$title = '헬로제주 관리자 페이지';
			$pages = array(
						array('members','members_log'),
						array('charge','coin_log')
					);
			break;
      case "sk" :
         $company = "sk";
         $company_kr = "SK";
         $company_email = "sk.com";
         $company_admin = $arr_btb['sk'];
         $company_type = 10;
         $authority = $settings[sk];
         $title = 'SK 관리자 페이지';
         $pages = array(
                  array('members','members_log'),
                  array('charge','coin_log')
               );
	}
	
	if (array_search($_SESSION[user_no], $authority) === false) {
		header("HTTP/1.0 404 Not Found");
		exit;
	}
	
	$current = array_pop(array_reverse(explode('.', array_pop(explode('/', $_SERVER[PHP_SELF])))));
	for ($i = 0; $i < count($pages); $i++) {
		$index = array_search($current, $pages[$i]);
		if ($index !== false) {
			$eq = $i;
			$meq = $index;
			break;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title><?=$title?></title>
<link rel='stylesheet' type='text/css' href='/css/style.css'/>
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
<script type='text/javascript' src='/js/scripts.js'></script>

<style>
h2 {padding-bottom: 20px; }
table {font:9pt NanumGothic; text-align:center; width: 100%; border: solid 1px #DFDFDF}
th {font:9pt NanumGothicBold; text-align: center; padding: 0px; background-color: #F0F0F0; }
.th_left {
	width:70px;
	font:9pt NanumGothicBold;
}
.td_right {
	text-align:left;
	padding-left:15px;
}

.button {margin: 0px 10px; outline: none; border: none; display: inline !important; padding:5px; text-align: center; background-color: black; color: white; font: 10pt NanumGothic; cursor: pointer; }
.tab-contents {cursor: pointer; }
.cell-contents {line-height: 25px; padding: 10px 20px; background-color: #F9F9F9; display: none; }
.cell-contents p {line-height: 25px; }

.ui-datepicker th, .ui-datepicker td {height: auto; }
li {border-top: solid 1px white; border-bottom: solid 1px #DFDFDF; }
a.nav {padding: 0px 5px; background-color: #EEEEEE; font: 11pt Tahoma; outline: solid 1px #E3E3E3; }
#coinLayer {display: none; width: 800px; padding: 30px; border: solid 2px #DFDFDF; border-radius: 5px; background-color: white; }

.submit {margin: 30px 0px; outline: none; border: none; width: 150px; height: 40px; text-align: center; background-color: black; color: white; font: 12pt NanumGothic; cursor: pointer; }
.submit-red {margin: 30px 0px; outline: none; border: none; width: 150px; height: 40px; text-align: center; background-color: red; color: white; font: 12pt NanumGothic; cursor: pointer; }

.button-red {margin: 0px 10px; outline: none; border: none; display: inline !important; width: 80px; height: 25px; text-align: center; background-color: red; color: white; font: 10pt NanumGothic; cursor: pointer; }
.button-small {margin: 0px 10px; outline: none; border: none; display: inline !important; padding: 0px 10px; height: 20px; text-align: center; background-color: black; color: white; font: 10pt NanumGothic; cursor: pointer; }
.procPayment {cursor: pointer; }
.help {font: 10px NanumGothic; padding-left: 3px; }
.caption {font-size: 12px; line-height: 18px; }
.btn-coin {color: blue; }
.ui-autocomplete-loading {
    background: white url('/images/ui-anim_basic_16x16.gif') right center no-repeat;
}
.unit {
	position:relative;
	margin:30px 0;
}
</style>
<script type='text/javascript'>
$(document).ready(function() {

	$.fn.qtip.defaults.position.my = 'bottom left';
	$.fn.qtip.defaults.position.at = 'top center';
	$.fn.qtip.defaults.style.classes = 'qtip-bootstrap';
	$('[title]').qtip();
	$('.help').html('[?]');

	$('input[type=text], textarea').addClass('text');
	$('.cell-contents').eq(<?=$eq?>).show();
	$('.cell-contents').eq(<?=$eq?>).children('p').eq(<?=$meq?>).css('font-family', 'NanumGothicBold');

	$('.tab-contents').click(function() {
		var cur = $('.tab-contents').index(this);
		$('.cell-contents').slideUp(200);
		if (!$('.cell-contents').eq(cur).is(':visible')) {
			$('.cell-contents').eq(cur).slideToggle(200);
		}
	});

	$('tr[data-href]').css('cursor', 'pointer').hover(
		function() {
			$(this).css('background-color', '#F9F9F9');
		},
		function() {
			$(this).css('background-color', 'white');
		}
	).click(function() {
		document.location.href = $(this).attr('data-href');
	});
});

</script>
</head>

<body>

<div style='position: fixed; left: 0px; top: 0px; width: 150px; height: 100%; background-color: #ECECEC; font: 10pt/30px NanumGothicBold'>
<ul>
	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -300px -33px'></span>
	<span class='tab-contents'>회원</span>
	<div class='cell-contents'>
	<p><a href='/btb/<?=$company?>/members'>회원 관리</a></p>
	<p><a href='/btb/<?=$company?>/members_log'>회원 로그</a></p>
	</div>
	</li>

	<li>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -330px -33px'></span>
	<span class='tab-contents'>코인</span>
	<div class='cell-contents'>
	<p><a href='/btb/<?=$company?>/charge'>코인 충전</a></p>
	<p><a href='/btb/<?=$company?>/coin_log'>코인 로그</a></p>
	</div>
	</li>
	
	<li onclick="location.href='/logout.php';" style='cursor:pointer;'>
	<span style='float: left; display: block; width: 30px; height: 30px; background: url(/images/admin/menu.png) -180px -33px'></span>
	<span>로그아웃</span>
	</li>
</ul>
</div>
<div style='position:absolute;top:15px;right:30px;'>
	<span style='font:12pt NanumGothic'><img src="/images/common/coin.png" style="width: 16px; height: 16px; margin-bottom: 2px; vertical-align: middle"> <?=$company_kr?> 총 코인 : <?=number_format(getCoin($company_admin))?>개</span>
</div>