<?php
   define(SMTP_EMAIL_ID, 'wegen@wegen.kr');
   define(SMTP_EMAIL_PW, '1generation');
   define(SMTP_EMAIL_NAME, '위제너레이션');

   include 'mailer/PHPMailerAutoload.php';

   
   $_mail = new PHPMailer;
   $_mail->isSMTP();
   $_mail->ContentType = "text/html";
   $_mail->CharSet = "utf-8";
   $_mail->Host = 'smtp.gmail.com';  // Specify main and backup server
   // 0 = off (for production use)
   // 1 = client messages
   // 2 = client and server messages
   //$_mail->SMTPDebug = 2;
   //$_mail->Debugoutput = 'html';
   $_mail->Port = 587;
   $_mail->SMTPAuth = true;                               // Enable SMTP authentication
   $_mail->Username = SMTP_EMAIL_ID;                            // SMTP username
   $_mail->Password = SMTP_EMAIL_PW;                           // SMTP password
   $_mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

   $_mail->From = SMTP_EMAIL_ID;
   $_mail->FromName = SMTP_EMAIL_NAME;

   $_mail->isHTML(true);                                  // Set email format to HTML

   function send_email($mail_layout, $email, $post_arr) {
      global $_mail;

      if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return false;

      $_mail->addAddress($email);
      $postdata = http_build_query($post_arr);
      $opts = array('http' => 
         array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
         )
      );
      $ctx = stream_context_create($opts);
      switch($mail_layout) {
         case 'donate':
            $_mail->Subject = "[위제너레이션] $post_arr[od_name]님 후원해주셔서 감사합니다!";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/donate.php', false, $ctx);
            break;
         case 'kara_donate':
            $_mail->Subject = "[위제너레이션] $post_arr[od_name]님 후원해주셔서 감사합니다!";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/kara_donate.php', false, $ctx);
            break;
         case 'kara_donate_final':
            $_mail->Subject = "[DSPZONE] 최종 확정된 좌석정보를 보내드립니다.";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/kara_donate.php', false, $ctx);
            break;
         case 'join' :
            $_mail->Subject = "[위제너레이션] $post_arr[mb_name]님 회원가입을 축하드립니다.";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/join.php', false, $ctx);
            break;
         case 'findpw':
            $_mail->Subject = "[위제너레이션] $post_arr[mb_name]님 비밀번호 설정 이메일입니다.";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/findpw.php', false, $ctx);
            break;
         case 'connect_post_upload':
            $_mail->Subject = "[위제너레이션] 참여하신 캠페인에 새로운 수혜자 TALK가 등록되었습니다.";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/upload_connect.php',false, $ctx);
            break;
         case 'success_campaign':
            $_mail->Subject = "[위제너레이션] 후원하신 \"$post_arr[it_name]\" 캠페인이 성공했습니다!";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/success_campaign.php',false, $ctx);
            break;
         case 'upload_postscript':
            $_mail->Subject = "[위제너레이션] 후원하신 \"$post_arr[it_name]\" 캠페인에 후기가 도착했습니다.";
            $mail_html = file_get_contents('http://'.$_SERVER[HTTP_HOST].'/module/mail_layout/upload_postscript.php',false, $ctx);
            break;

      }

      $_mail->msgHTML($mail_html);

      if(!$_mail->send()) {
         return false;
      } else {
         $_mail->clearAddresses();
         return true;
      }
   }
   
   if(isset($_POST[mail_type])) {
      $_mail_type = $_POST[mail_type];
      $_required = true;
      include $_SERVER[DOCUMENT_ROOT].'/config.php';
      sec_session_start(false);
   }

   if($_mail_type == 'join') {
      if($_POST[mb_no] == '') {
         print 'failed';
         exit;
      }
      $mb_no = $_POST[mb_no];
      $info = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERE mb_no = $mb_no");
      if($info[sent_mail] == 1) {
         print 'failed';
         exit;
      }
      $sns_chk = explode("@",$info[mb_id]);
      if($sns_chk[0] == 'fb' ) {
         $mb_id = "Facebook Login";
      } else if($sns_chk[0] == 'tw') {
         $mb_id = "Twitter Login";
      } else {
         $mb_id = $info[mb_id];
      }

      $mb_email = $info[mb_email] != "" ? $info[mb_email] : "";
      $mb_contact = $info[mb_contact] != '' ? $info[mb_contact] : "";
      $agree_sms = $info[mb_agreesms] == 1 ? '예' : '아니오';
      $agree_email = $info[mb_agreemail] == 1 ? '예' : '아니오';

      $postarr = array(
         'mb_id' => $mb_id,
         'mb_name' => $info[mb_name],
         'mb_email' => $mb_email,
         'mb_contact' => $mb_contact,
         'agree_sms' => $agree_sms,
         'agree_email' => $agree_email,
      );

      if($mb_email != "" && send_email($_mail_type, $mb_email, $postarr)) {
         sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $mb_no, email='$mb_email'");
         sql_query("UPDATE ".DB_MEMBERS." SET sent_mail = 1 WHERE mb_no = $mb_no");
         echo "success";
      }
   }

   if($_mail_type =='findpw') {
      $postarr = array(
         'mb_name' => $info[mb_name],
         'mb_no' => $info[mb_no],
         'password' => $password,
      );

      if($info[mb_id] && send_email($_mail_type, $info[mb_id],$postarr)) {
         sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $info[mb_no], email='$info[mb_id]'");
         alert('이메일을 발송하였습니다', '/');
      } else {
         echo "<script>alert('이메일 발송이 실패하였습니다. 다시 한번 시도해주세요');</script>";
      }
   }

   if($_mail_type =='donate') {
      if(isset($_POST[mail_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }
      
      if(isset($_POST[mail_type])) {
         $od = sql_fetch("SELECT *, a.sent_mail AS o_sent_mail FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
            LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
            WHERE a.od_id = '$_POST[od_id]'");
      } // 가상계좌일때는 이미 $od 에 fetch했음. (가상계좌는 POST가아님)
      
      $ya_arr = explode("|",$od[it_youtube_after]);
      $thank_youtube_id = $ya_arr[time()%count($ya_arr)];
      $donate_info = sql_fetch("SELECT COUNT(od_id) AS num FROM ".DB_ORDERS." WHERE mb_no = $od[mb_no] AND pay_remain =0 ");
      
      $use_pay_method = $od[od_method];
      $postarr = array(
         'od_name' => $od[od_name],
         'mb_no' => $od[mb_no],
         'youtube_id' => "".$thank_youtube_id,
         'it_id' => $od[it_id],
         'good_name' => $od[it_name],
         'use_pay_method' => $use_pay_method,
         'good_mny' => $od[od_amount],
         'tno' => $od[od_tno],
         'donate_num' => $donate_info[num],
         );

      if($od[o_sent_mail] == 0 ) {
         if(send_email('donate',$od[mb_email], $postarr)) {
            sql_query("UPDATE ".DB_ORDERS." SET sent_mail = 1 WHERE od_id = '$od[od_id]'");
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $od[mb_no], email='$od[mb_email]', var_1 = '$od[it_id]'");
            echo "success";
         }
      }
   }

   if($_mail_type == 'kara_donate') {
      if(isset($_POST[mail_type]) && $_POST[od_id] == '') {
         echo 'failed';
         return;
      }
      
      if(isset($_POST[mail_type])) {
         $od = sql_fetch("SELECT *, a.sent_mail AS o_sent_mail FROM ".DB_ORDERS." a
            LEFT JOIN ".DB_CAMPAIGNS." b ON a.it_id = b.it_id
            LEFT JOIN ".DB_MEMBERS." c ON a.mb_no = c.mb_no
            WHERE a.od_id = '$_POST[od_id]'");
      } // 가상계좌일때는 이미 $od 에 fetch했음. (가상계좌는 POST가아님)
      
      
      $use_pay_method = $od[od_method];
      if($use_pay_method == '가상계좌') $od[var_1] = $seat_info;
      $postarr = array(
         'od_name' => $od[od_name],
         'seat_info' => $od[var_1],
         'it_id' => $od[it_id]
         );

      if($od[o_sent_mail] == 0 ) {
         if(send_email('kara_donate',$od[mb_email], $postarr)) {
            sql_query("UPDATE ".DB_ORDERS." SET sent_mail = 1 WHERE od_id = '$od[od_id]'");
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $od[mb_no], email='$od[mb_email]', var_1 = '$od[it_id]'");
            echo "success";
         }
      }
      
   }

   if($_mail_type == 'connect_post_upload') {
      if(!isset($_POST[it_id]) || $_POST[it_id] == '') {
         echo 'failed';
         exit;
      }
      $it_id = $_POST[it_id];
      $it = sql_fetch("SELECT it_connectors FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
      $connector = explode("|",$it[it_connectors]);
      if(!$_SESSION[is_admin] && array_search($_SESSION[user_no],$connector) === false ) exit;
      
      $chk_sent = sql_fetch("SELECT cmt_id, sent_mail FROM ".DB_CAMPAIGN_CMTS." WHERE cmt_category = 100 AND it_id = '$it_id'
         ORDER BY cmt_time DESC LIMIT 1");
      if($chk_sent[sent_mail] != 0) {
         echo 'failed';
         exit;
      }
      
      $donate_res = sql_query("SELECT * FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
            WHERE a.it_id = '$it_id' AND a.od_amount - a.pay_remain > 0 AND b.mb_agreemail = 1 GROUP BY a.mb_no");

      $postarr = array(
         'cmt_id' => $chk_sent[cmt_id],
         'it_id' => $it_id,
      );
      while($row = sql_fetch_array($donate_res)) {
         if(send_email($_mail_type,$row[mb_email], $postarr)) {
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $row[mb_no], email='$row[mb_email]', var_1 = '$chk_sent[cmt_id]'");
         }
      }
      sql_query("UPDATE ".DB_CAMPAIGN_CMTS." SET sent_mail = 1 WHERE cmt_id = '$chk_sent[cmt_id]' ");
      echo 'success';
   }

   if($_mail_type == 'success_campaign') {
      if(!isset($_POST[it_id]) || $_POST[it_id] == '' || !$_SESSION[is_admin]) {
         echo 'failed';
         exit;
      }
      $it_id = $_POST[it_id];
      $chk = sql_fetch("SELECT it_name,sent_mail FROM ".DB_CAMPAIGNS." WHERE it_id = '$it_id'");
      if($chk[sent_mail] > 0) {
         echo 'failed';
         exit;
      }

      sql_query("UPDATE ".DB_CAMPAIGNS." SET sent_mail  = 1 WHERE it_id = '$it_id'");
      $donate_res = sql_query("SELECT * FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
            WHERE a.it_id = '$it_id' AND a.od_amount - a.pay_remain > 0 AND b.mb_agreemail = 1 GROUP BY a.mb_no");

      $postarr = array(
         'it_id' => $it_id,
         'it_name' => $chk[it_name],
      );
      while($row = sql_fetch_array($donate_res)) {
         if(send_email($_mail_type,$row[mb_email], $postarr)) {
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $row[mb_no], email='$row[mb_email]', var_1 = '$it_id'");
         }
      }
      sql_query("UPDATE ".DB_CAMPAIGNS." SET sent_mail  = 2 WHERE it_id = '$it_id'");

      echo 'success';
   }

   if($_mail_type == 'upload_postscript') {
      if(!isset($_POST[wr_id]) || $_POST[wr_id] == '' || !$_SESSION[is_admin]) {
         echo 'failed';
         exit;
      }
      $wr_id = $_POST[wr_id];
      $chk = sql_fetch("SELECT *, p.sent_mail AS p_sent_mail FROM ".DB_POSTSCRIPTS." p LEFT JOIN ".DB_CAMPAIGNS." b ON p.wr_campaign = b.it_id WHERE wr_id = '$wr_id'");
      if($chk[p_sent_mail] > 0) {
         echo 'failed';
         exit;
      }

      sql_query("UPDATE ".DB_POSTSCRIPTS." SET sent_mail  = 1 WHERE wr_id = '$wr_id'");
      $donate_res = sql_query("SELECT * FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON a.mb_no = b.mb_no 
            WHERE a.it_id = '$chk[wr_campaign]' AND a.od_amount - a.pay_remain > 0 AND b.mb_agreemail = 1 GROUP BY a.mb_no");

      $postarr = array(
         'wr_id' => $wr_id,
         'it_name' => $chk[it_name],
      );
      while($row = sql_fetch_array($donate_res)) {
         if(send_email($_mail_type,$row[mb_email], $postarr)) {
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $row[mb_no], email='$row[mb_email]', var_1 = '$wr_id'");
         }
      }
      sql_query("UPDATE ".DB_POSTSCRIPTS." SET sent_mail  = 2 WHERE wr_id = '$wr_id'");

      echo 'success';
   }

   if($_mail_type == 'kara_fan_definite') {
      $res = sql_query("SELECT a.*, b.mb_email FROM ".DB_ORDERS." a LEFT JOIN ".DB_MEMBERS." b ON b.mb_no = a.mb_no 
            WHERE a.it_id = '$SPECIAL[kara_fan]' AND a.od_time >= 20140509075000 AND a.od_time <=20140517170000 AND pay_remain = 0 AND a.sent_mail = 0 ORDER BY a.od_time ASC");
      $all_num = mysql_num_rows($res);
      while($row = sql_fetch_array($res)) {
         $seat_info = $row[var_1];
         $postarr = array(
            'od_name' => $row[od_name],
            'seat_info' => $seat_info,
            'it_id' => $SPECIAL[kara_fan],
            'final' => 'Y'
            );
         if(send_email('kara_donate_final',$row[mb_email], $postarr)) {
            $fail_count++;
            sql_query("UPDATE ".DB_ORDERS." SET sent_mail = 1 WHERE od_id = '$row[od_id]'");
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $row[mb_no], email='$row[mb_email]', var_1 = '$row[od_id]'");
         } else {
            sql_query("INSERT INTO ".DB_MAIL_LOG." SET type='$_mail_type', mb_no= $row[mb_no], email='$row[mb_email]', var_1 = '$row[od_id]', isFailed = 'true'");
            
         }
      }
      echo "총 {$all_num}개 중 ".$fail_count."개 성공";
   }
   
?>
