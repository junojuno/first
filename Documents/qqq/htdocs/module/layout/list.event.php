<?php
   $sql = "SELECT * FROM ".DB_POSTSCRIPTS." a 
      LEFT JOIN ".DB_CAMPAIGNS." b ON a.wr_campaign = b.it_id 
      LEFT JOIN ".DB_FUNDRAISERS." c ON b.it_fundraiser = c.no 
      WHERE wr_category = 2 ORDER BY wr_datetime DESC LIMIT 0, 3";
   $res = sql_query($sql);
   $count = 0;
?>

<ul>
<? while($row = sql_fetch_array($res)) { ?>
   <li <? if($count ==2) echo "class='last'";?>>
      <a href='/postscript/event/<?=$row[wr_campaign]?>'>
      <div class='event'>
         <div class='img_cont'>
            <img src="<?=$row[wr_cover]?>"/>
         </div>
         <div class='name'>
            <?=$row[fr_name]?>
         </div>
         <div class='title_1'>
            <?=$row[wr_subject]?>
         </div>
         <div class='title_2'>
            <?=$row[wr_subject2]?>
         </div>
      </div>
      </a>
   </li>
<? $count++;} ?>

</ul>
