<div class='scroll_content'>
   <ul class='list'>
		<?
		$sql = "SELECT * FROM ".DB_SPONSORS." WHERE sp_logo != '' ORDER BY sp_no DESC";
		$result = sql_query($sql);
      while($row = sql_fetch_array($result)) {
         $link = ($row[sp_homepage]) ? "<a href='$row[sp_homepage]' target='_blank'>" : false;
         $linkEnd = ($link) ? '</a>' : false;
         ?>
         <li class='sponsor'>
            <div class='img_cont'>
               <?=$link?>
               <img src='/data/partners/<?=$row[sp_logo]?>' alt='<?=$row[sp_name]?>' />
               <?=$linkEnd?>
            </div>
         </li>
         <?
      }
      ?>
   </ul>
</div>
