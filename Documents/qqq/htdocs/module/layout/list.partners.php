<div class='scroll_content'>
   <ul class='list'>
		<?
		$sql = "SELECT * FROM ".DB_PARTNERS." WHERE pt_logo != '' ORDER BY pt_no DESC";
		$result = sql_query($sql);
      while($row = sql_fetch_array($result)) {
         $link = ($row[pt_homepage]) ? "<a href='$row[pt_homepage]' target='_blank'>" : false;
         $linkEnd = ($link) ? '</a>' : false;
         ?>
         <li class='partner'>
            <div class='img_cont'>
               <?=$link?>
               <img src='/data/partners/<?=$row[pt_logo]?>' alt='<?=$row[pt_name]?>' />
               <?=$linkEnd?>
            </div>
         </li>
         <?
      }
      ?>
   </ul>
</div>
