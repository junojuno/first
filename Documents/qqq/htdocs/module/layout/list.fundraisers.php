<div class='scroll_content'>
   <ul class='list'>
		<?
		$sql = "SELECT * FROM ".DB_FUNDRAISERS." WHERE fr_logo_main != '' ORDER BY no DESC";
		$result = sql_query($sql);
      while($row = sql_fetch_array($result)) {
         $link = ($row[fr_homepage]) ? "<a href='$row[fr_homepage]' target='_blank'>" : false;
         $linkEnd = ($link) ? '</a>' : false;
         $name = mb_strlen($row[fr_name], 'utf8') > 5 ? mb_substr($row[fr_name], 0, 5, 'utf8').".." : $row[fr_name];
         ?>
         <li class='fundraiser'>
            <div class='img_cont'>
               <?=$link?>
               <img src='/data/partners/<?=$row[fr_logo_main]?>' alt='<?=$row[fr_name]?>' />
               <?=$linkEnd?>
            </div>
            <div class='name'>
               <?=$name?>
            </div>
         </li>
         <?
      }
      ?>
   </ul>
</div>
