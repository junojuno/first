
<div class='open_campaign_layout'>
   <div class='close_btn'>
      <img src='/images/close_btn.png'>
   </div>
   <div class='wrap'>
      <div class='title'>
         <?=nl2br($it[it_openCampaignTitle])?>
      </div>
      <div class='guide'>
         무료응모는 1인 1회 가능하며 이벤트 응모 티켓 한 장이 주어집니다 (100원 기부 상당)
      </div>
      <div class='info_cont'>
         <div class='info'>
         <table>
            <tr>
               <th>이름</th>
               <td><input type='text' name='open_name' value='<?=$mb_info[mb_name]?>'></td>
            </tr>
            <tr>
               <th>연락처</th>
               <td>
                  <input type='text' name='open_hp1' maxlength='4' class='numonly' value='<?=$mb_contact[0]?>' /> - 
                  <input type='text' name='open_hp2' maxlength='4' class='numonly' value='<?=$mb_contact[1]?>' /> - 
                  <input type='text' name='open_hp3' maxlength='4' class='numonly' value='<?=$mb_contact[2]?>' />
               </td>
            </tr>
            <tr>
               <th>이메일</th>
               <td><input type='text' name='open_email' value='<?=$mb_info[mb_email]?>'></td>
            </tr>
            <tr>
               <th>응원메세지</th>
               <td><textarea type='text' name='open_msg'></textarea></td>
            </tr>
         </table>
         </div>
      </div>
   </div>
   <div class='share_fb_btn'>
      <img src='/images/campaign/open_share_btn.png'>
   </div>
   <input type='hidden' name='open_it_id' value='<?=$it_id?>'>
</div>

<div class='open_campaign_finish' data-refresh='true'>
   <div class='close_btn'>
      <img src='/images/close_btn.png'>
   </div>
   <div class='wrap'>
      <div class='top_msg'>
         성공적으로 이벤트에 응모되셨습니다!<br>
         클라라와의 캠핑 응모권이 1장 발급되었습니다.
      </div>
      <div class='middle_msg'>
         <span class='red'>후원하기'로 더 많은 티켓</span>을 받으시겠어요?
      </div>
      <div class='bottom_msg'>
         (응모하기 = Ticket 1장 / 후원하기 = 1,000원에 Ticket 10장)
      </div>
      <div class='donate_btn'>
         <img src='/images/campaign/open_donate_btn.png'>
      </div>
   </div>
</div>
