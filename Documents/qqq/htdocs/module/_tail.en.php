<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
$tail_info = sql_fetch("SELECT * FROM ".DB_INFO." LIMIT 1");
?>

<!-- footerwrap -->
<div id="footerwrap">
	<div id='footer'>
		<menu>
			<a href='/provision'><li>이용약관</li></a>
			<a href='/privacy-policy'><li>개인정보 취급방침</li></a>
			<a href='/about/inquiry'><li>1:1 문의</li></a>
		</menu>
      <div class='footer_right'>
         <div class='top'>
            <div class='char'>
               위제너레이션을 페이스북에서 받아보세요!
            </div>
            <div class='snsicon'>
               <a href='http://www.facebook.com/wegeneration/' target='_blank'><img src='/images/footer_facebook.png' style='margin-right: 5px' title='위젠 페이스북' /></a>
               <a href='http://www.twitter.com/wegenkr/' target='_blank'><img src='/images/footer_twitter.png' style='margin-right: 5px' title='위젠 트위터' /></a>
            </div>
         </div>
         <div class='bottom'>
            <div class="fb-like" data-href="http://www.facebook.com/WeGeneration" data-layout="standard" data-action="like" data-show-faces="true" data-share="false" data-width="400">
            </div>
         </div>
      </div>
		<div style='clear: both'></div>
	</div>
</div>
<!-- /footerwrap -->

<!-- copyrightwrap -->
<div id="copyrightwrap">
	<div id="copyright">
			<img src="<?=$g4[abspath]?>/images/common/copyright.png" style='float: left; margin-right: 15px' />
         WeGeneration<br>
         Gukil Building #301, 60, Gangnam-daero 2-gil, Seocho-gu, Seoul, 137-898 Republic of Korea<br>
         CEO : Kidae Hong | Business Registration Number : <?=$tail_info[opt_corp_num]?><br>
         TEL :  +82-70-4164-1151 | E-mail: <?=$tail_info[opt_email]?>  <br><br>
			<strong>COPYRIGHTⓒ 2012 WEGENERATION. ALL RIGHTS RESERVED.</strong>
	</div>
</div>
<!-- /copyrightwrap -->

</body>

</html>
