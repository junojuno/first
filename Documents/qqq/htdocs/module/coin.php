<?
$coin = getCoin($_SESSION[user_no]);
$coins = $coin ? $coin : '0';
$isbtb = btbCheck($_SESSION[user_no], $settings[btb]);
if ($_SESSION[user_no]) {
?>
<script type='text/javascript'>
function checkCoinForm(form) {
	p = $.trim($('input[name=good_mny]').val());
	if (!p || parseInt(p) == 0) {
		alert('후원을 희망하는 금액을 입력해 주세요.\n1코인은 1원입니다.');
		$('input[name=good_mny]').focus();
		return false;
	}
	else if (p > <?=$coins?>) {
		alert('후원희망액이 현재 보유하고 있는 코인보다 많습니다.\n1코인은 1원입니다.');
		return false;
	}
}
</script>
	<div id='coinDonateLayer'><div style='padding: 20px; text-align: center'>
		<span class='btn_close'>×</span>

		<h3 style='font: 10pt NanumGothicBold; margin: 0px 0px 20px 0px; text-align: left'>코인으로 후원하기</h3>
		<p><img src='/images/common/coin.png' style='width: 20px; height: 20px; margin-bottom: 2px; vertical-align: middle' /> <span style='font: bold 10pt Arial; color: #E30000'><?=number_format($coins);?></span><strong>개</strong></p>
		<form name='coinDonationForm' method='post' action='/campaign/proc_coin.php' onsubmit="return checkCoinForm(this.form);">
		<? $_SESSION[token] = dechex(crc32(session_id().'thisisCOINdonateSALT')); ?>
		<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisCOINdonateSALT'))?>' />
		<input type='hidden' name='ordr_idxx' value='<?=date('ymdHis').rand(100, 999)?>' />
		<input type='hidden' name='itemid' value='<?=$it_id?>' />
		<input type='hidden' name='good_name' />
		<input type='text' name='good_mny' class='text numonly' style='text-align: right; width: 95px; height: 22px' />원
		<input type='submit' value='후원' style='cursor: pointer; height: 22px; margin-top: 10px; background-color: #E30000; outline: none; border: none; font: 9pt NanumGothic; color: white;' />

		<span id='eventbox' style='display: none; font-size: 8pt'><input type='checkbox' name='od_isEvent' checked /> 스타 이벤트 참여희망</span>
		</form>
	</div></div>

<? } ?>

<?if(!$isMobile) { ?>
<!-- <div style='position: fixed; left: 50%; top: 50%; margin-left: -589px; margin-top: -80px; width: 100px; height: 160px; background: url(/images/common/coincell_bg.png)'><div style='padding: 10px 10px 10px 0px; text-align: center'>  origin -->
<!--
<div style='position: absolute; left: 50%; top: 50%; margin-left: -589px; margin-top: 210px; width: 100px; height: 160px; background: url(/images/common/coincell_bg.png)'><div style='padding: 10px 10px 10px 0px; text-align: center'>
<p style='font: bold 9pt Arial; margin-bottom: 10px'><img src='/images/common/coin.png' style='width: 20px; height: 20px; margin-bottom: 2px; vertical-align: middle' /> My Coin</p>
<?
if ($coin !== '0') {
	if($isbtb) { ?>
		<span style='font: bold 10pt Arial; color: #E30000'><?=number_format($coin);?></span><strong>개</strong>
		<p style='margin: 10px 5px; padding:0 5px;font: 8pt/20px NanumGothic;width:73px;'>
		<?
			switch(getMbrType($_SESSION[user_no])) {
				case 5:
					print '헬로제주에서 <br/>코인을 지급하는<br/>계정입니다.';
					break;
            case 10:
               print 'SK에서 <br/>코인을 지급하는<br/>계정입니다.';
               break;
				default:
					print '회사 관리자<br/>계정입니다.';
					break;
			}
		?>
		</p>
	<? } else {
		//$data = sql_fetch("SELECT * FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' ORDER BY od_id DESC LIMIT 1 ");
		$check = sql_fetch("SELECT preferdate, isCancel FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' AND isFirst = '1' LIMIT 1");
	
      /*
		$datetime = explode('-', $data[od_time]);
		$date = mktime(0, 0, 0, $datetime[1], substr($datetime[2], 0, 2), $datetime[0]);
		$deadline = strftime( '%Y-%m-%d', strtotime( '+1 month', $date));
		$diff = round((strtotime($deadline) - strtotime(date('Y-m-d'))) / (60*60*24));
      */
	?>
	<span style='font: bold 10pt Arial; color: #E30000'><?=number_format($coins);?></span><strong>개</strong>
	<p style='margin: 5px 0px; font: 8pt/20px NanumGothic'>
      <?=$check[isCancel] == 0 ? "매월 ".$check[preferdate]."일 충전" : "정기후원이 취소되었습니다"?>
   </p>
	<a href='/my/coin'><img src='/images/common/btn_coinview.png' style='margin-left: 5px'/></a>
	<?
	}
} else {
   if($isbtb) { ?>
      <span style='font: bold 10pt Arial; color: #E30000'>0</span><strong>개</strong>
      <p style='margin: 10px 5px; padding:0 5px;font: 8pt/20px NanumGothic;width:73px;'>
      <?
         switch(getMbrType($_SESSION[user_no])) {
            case 5:
               print '헬로제주에서 <br/>코인을 지급하는<br/>계정입니다.';
               break;
            case 10:
               print 'SK에서 <br/>코인을 지급하는<br/>계정입니다.';
               break;
            default:
               print '회사 관리자<br/>계정입니다.';
               break;
         }
      ?>
      </p>
   <? } else { ?>
      <span style='font: bold 10pt Arial; color: #E30000'>0</span><strong>개</strong>
      <p style='margin: 10px 0px; font: 8pt/15px NanumGothic'>지금 정기후원을<br/>신청하세요!</p>
      <a href='/givex/'><img src='/images/common/btn_coinreg.png' style='margin-left: 5px'/></a>
   <? }
} ?>
</div></div>
-->
<? } ?>

<script type='text/javascript'>
$(document).ready(function() {
	$('.donateNow a').click(function() {
<?
if ($_SESSION[user_no]) {
	$givex = sql_fetch("SELECT COUNT(od_id) AS cnt FROM ".DB_REGULARPAYMENT." WHERE mb_no = '$_SESSION[user_no]' ");
	if ($givex[cnt] > 0 || getCoin($_SESSION[user_no]) > 0) {
?>
		$('#movieFrame').hide();
		createLayer($('#coinDonateLayer'));

		$('input[name=itemid]').val($(this).attr('itemid'));
		$('input[name=good_name]').val($(this).attr('itemname'));
		var eventnum = $(this).attr('isEvent');
		if (eventnum > 0) {
			$('input[name=od_isEvent]').val(eventnum);
			$('#eventbox').show();
		}
<?
	}
	else {
?>
		alert('정기후원을 신청하지 않으셨습니다.\n확인 버튼을 누르면 신청 페이지로 이동합니다.');
		document.location.href = '/givex/';
<?
	}
} else {
?>
//		$('.btn_login').click();
		createLayer($('#loginLayer'));
<? } ?>
	});
});
</script>
