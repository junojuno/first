<?php
   //블루스퀘어 삼성카드홀 - 뮤지컬형
   $seat_arr = array();
   
   $endIdx = 42;
   for($i =0; $i < 26; $i++) {
      if($i == 17) $endIdx = 46;
      array_push($seat_arr, array_fill(0, $endIdx, 1));
   }

   for($i =0; $i<=25;$i++) {
      if(($i >=13 && $i <=15) || ($i >=17 && $i <=19)) continue;
      $seat_arr[$i][0] =0;
   }
   for($i =0; $i<=25;$i++) {
      if(($i>=0 && $i <=3) || ($i >=24 && $i <=25)) $seat_arr[$i][1] = 0;
   }
   $seat_arr[0][2] =0; $seat_arr[1][2] = 0;
   for($i =17;$i<=22;$i++) $seat_arr[$i][12] =0;
   $seat_arr[17][13] =0; $seat_arr[18][13] =0;
   for($i = 15; $i <=16 ; $i++) {
      for($j =10 ; $j <=31 ; $j++) {
         $seat_arr[$i][$j] =0;
      }
   }
   for($i =0; $i <=7 ; $i++) {
      $seat_arr[($i*2)][31] = 0;
   }
   $seat_arr[17][31] =0;
   for($i=17;$i<=20;$i++) {
      $seat_arr[$i][32] =0;
   }
   for($i=17;$i<=23;$i++) {
      $seat_arr[$i][33] =0;
   }
   $seat_arr[0][39] =0; $seat_arr[1][39] =0;
   for($i=0;$i<=3;$i++) {
      $seat_arr[$i][40] =0;
   }
   for($i=0;$i<=12;$i++) {
      $seat_arr[$i][41] =0;
   }
   $seat_arr[16][41] =0;
   $seat_arr[24][44] =0; $seat_arr[25][44] =0;
   for($i = 20;$i<=25;$i++) {
      $seat_arr[$i][45] =0;
   }

   $seat_arr[25][9] =0; $seat_arr[25][10] =0; $seat_arr[25][11] =0;

   //홀드좌석
   $seat_arr[13][15] =0; $seat_arr[13][16] =0;
   for($i =0; $i <12 ;$i++) {
      $seat_arr[17][$i] =0;
   }
   for($i =14; $i<31;$i++) {
      $seat_arr[17][$i] =0;
   }
   $seat_arr[17][34] =0;
   
   

   $count = 0; 
   $seat_col = 0; $seat_num = 0; $flag = false;
   for($i =0; $i <= 25; $i++) {
      $tmp = count($seat_arr[$i]);
      for($j =0; $j < $tmp; $j++) {
         if($seat_arr[$i][$j] == 1) {
            $count++;
            if($count == $target_rank) {
               $seat_floor = $i<=16 ? '1' : '2';
               $seat_col = $i<=16 ? ($i+1) : ($i-13);
               $seat_num = ($j+1);
               $flag = true;
               break;
            }
         }
      }
      if($flag) break;
   }
   $seat_info = $seat_floor.'층 '.$seat_col."열 ".$seat_num."번";
?>
