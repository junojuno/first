<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
$tail_info = sql_fetch("SELECT * FROM ".DB_INFO." LIMIT 1");
?>

<!-- footerwrap -->
<div id="footerwrap">
	<div id='footer'>
		<div style='float: right'>
		<a href='http://www.facebook.com/wegeneration/' target='_blank'><img src='/images/common/icon_fb.png' style='margin-right: 5px' title='위젠 페이스북' /></a>
		<a href='http://www.twitter.com/wegenkr/' target='_blank'><img src='/images/common/icon_tw.png' style='margin-right: 5px' title='위젠 트위터' /></a>
		<a href='http://www.youtube.com/wegenkr' target='_blank'><img src='/images/common/icon_youtube.png' style='margin-right: 5px' title='위젠 유튜브 채널' /></a>
		</div>

		<menu>
			<a href='/provision'><li>이용약관</li></a>
			<a href='/privacy-policy'><li>개인정보 취급방침</li></a>
			<a href='/about/inquiry'><li>1:1 문의</li></a>
		</menu>
		<div style='clear: both'></div>
	</div>
</div>
<!-- /footerwrap -->

<!-- copyrightwrap -->
<div id="copyrightwrap">
	<div id="copyright">
			<img src="<?=$g4[abspath]?>/images/common/copyright.png" style='float: left; margin-right: 15px' />
         위제너레이션 | <?=$tail_info[opt_address]?> | 대표 : <?=$tail_info[opt_ceo]?> <br>
         사업자등록번호 : <?=$tail_info[opt_corp_num]?> | 통신판매업신고번호 : <?=$tail_info[opt_comm_num]?> | 개인정보관리책임자 : <?=$tail_info[opt_ceo]?> <br>
      대표전화번호 :  <?=$tail_info[opt_tel]?> | <? if(!$tail_info[opt_fax]) echo "팩스 : $tail_info[opt_fax] | "; ?>이메일 : <?=$tail_info[opt_email]?>  <br><br>
			<strong>COPYRIGHTⓒ 2012 WEGENERATION. ALL RIGHTS RESERVED.</strong>
	</div>
</div>
<!-- /copyrightwrap -->

</body>

</html>
