<?
if (!$_required) {
	header("HTTP/1.0 404 Not Found");
	exit;
}
?>

<img src='/images/campaign/univ/detail_1.png' />

<table style='width: 100%' cellpadding='0' cellspacing='0'>
<?
$univs = array(
	'고려대학교',
	'명지대학교',
	'상명대학교',
	'성균관대학교',
	'아주대학교',
	'연세대학교',
	'이화여자대학교',
	'한세대학교',
	'한양대학교',
	'홍익대학교'
	);
$univ_temp = $univs;
$univs_sum = array();

for ($i=0; $i < count($univs); $i++) {
	$query = "SELECT sum(od_amount) AS univ_sum
			FROM ".DB_ORDERS."
			WHERE it_id = '$it_id'
			AND od_isEvent = '$univs[$i]'
			";
	$result = sql_fetch($query);
	$univs_sum[$i] = $result[univ_sum];
}
array_multisort($univs_sum, SORT_NUMERIC, SORT_DESC, $univs);

$s_sum = 500000;

for ($j=0; $j < 20; $j++) {
	$max = 500000 * (20 - $j);
	$standard = $max * 0.8;
	if ($univs_sum[0] > $standard) {
		$s_sum = $max;
		break;
	}
}

for ($i=0; $i < count($univs); $i++) {
	$bgcolor = ($i % 2 == 0) ? '#FAFAFC' : '#EAEAEC';
	$divheight = ($i == 0) ? 150 : 75;
	$imagesize = ($i == 0) ? 90 : 64;
	$lettersize = ($i == 0) ? 13 : 10;
	$numbersize = ($i == 0) ? 13 : 11;
	$msgimagesize = ($i == 0) ? 30 : 20;
?>

<tr style='height: <?=$divheight?>px; background-color: <?=$bgcolor?>'>
	<td style='width: 60px; background: url(/images/campaign/univ/rank_<?=$i+1?>.png) no-repeat center'></td>
	<td style='width: 120px; text-align: center'><img src='/images/campaign/univ/logo_<?=array_search($univs[$i], $univ_temp)+1?>.png' style='width: <?=$imagesize?>px; height: <?=$imagesize?>px'/></td>
	<td style=''>
	<p style="font: bold <?=$lettersize?>pt '맑은 고딕'">
		<?=$univs[$i]?>
		<img class='comment' src='/images/campaign/univ/btn_message.png' style='width: <?=$msgimagesize?>px; cursor: pointer' onclick="window.open('cmt_univ.php?univ=<?=array_search($univs[$i], $univ_temp)+1?>&rank=<?=$i+1?>','univpop','resizable=no,width=600,height=800')"/>
	</p>
		<!-- graph_box -->
		<?
		if (!$univs_sum[$i]) $univs_sum[$i] = 0;
		$per = @ceil($univs_sum[$i] * 100 / $s_sum); 
		$percentage = $per >= 100 ? 100 : $per;
		?>
		<div id='g_<?=$i?>' style='position: relative; margin-top: 5px; background-color: white; border: solid 1px #C6C6C5; width: 100%; height: 18px; text-align: right; color: #E30000; font-weight: bold; font-size: 11pt'>
			<div id='p_<?=$i?>' class='progress' style='background-color: #E30000; width: <?=$percentage?>%; float: left; height: 18px'></div>
		</div>
		<!-- /graph_box -->
	</td>
	<td style='width: 100px; text-align: right'>
		<?
		$sql = "SELECT *
			FROM ".DB_ORDERS."
			WHERE it_id = '$it_id'
			AND od_isEvent = '$univs[$i]'
			AND od_amount > 0
			";
		$result = sql_query($sql);
		$total = mysql_num_rows($result);
		?>
		<span class='people'><img src='/images/campaign/univ/icon_cnt.png' style='vertical-align: middle' /><span style='font-weight: bold; font-size: 12pt'> <?=$total?></span></span><br/>
		<span style="font: bold <?=$numbersize?>pt '맑은 고딕'"><?=number_format($univs_sum[$i])?>원</span>
		</td>
	<td style='width: 80px; text-align: right'>
	<? if ($member[mb_id]) { ?>
		<a href='<?=$g4[shop_path]?>/<?=$it[it_id]?>/donate?univ=<?=array_search($univs[$i], $univ_temp)+1?>'><img src='<?=$g4[abspath]?>/images/campaign/univ/btn_donate.png' /></a>
	<? } else { ?>
		<img src='<?=$g4[abspath]?>/images/campaign/univ/btn_donate.png' style='cursor: pointer' onclick="if(g4_is_ie){$('#movieframe').hide();}document.getElementById('layer').style.display='';"/>
	<? } ?>
	</td>
</tr>

<? } ?>

</table>

<script>
$(document).ready(function() {
	$('.comment').qtip({
		content: {text: '이 학교에 응원 메세지 남기기'},
		position: {my: 'bottom left', at: 'top right'},
		style: {classes: 'qtip-tipsy qtip-shadow'}
	});
	$('.people').qtip({
		content: {text: '후원자 수'},
		position: {my: 'bottom center', at: 'top center'},
		style: {classes: 'qtip-tipsy qtip-shadow'}
	});
});
</script>