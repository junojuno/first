<?php
	if(!isset($_GET['no']) || !isset($_GET['tmp'])) {
		header("HTTP/1.0 404 Not Found");
		exit;
	} else {
		$_required = true;
		include 'config.php';
		include 'module/_head.php';
		$res = mysql_query("SELECT mb_no FROM ".DB_MEMBERS." WHERE mb_no = {$_GET['no']} AND mb_password = '{$_GET['tmp']}'");
		$res = mysql_fetch_assoc($res);
		if($res['mb_no'] == '') {
			alert('비정상적인 접근입니다.', '/');
		}
	}
	
	if(isset($_POST['send']) && $_POST['send'] == 'Y') {
		if(!isset($_POST['p']) || $_POST['p'] == '' || !isset($_POST['no'])) {
			alert('비정상적인 접근입니다.', '/');
			exit;
		}
		$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$password = $_POST['p'];
		$password = hash('sha512', $password.$random_salt);
		mysql_query("UPDATE ".DB_MEMBERS." SET
				mb_password = '{$password}',
				salt = '{$random_salt}'
				WHERE mb_no = {$_POST['no']}");
		alert('비밀번호가 변경되었습니다.', '/');
	}
?>
<script type='text/javascript' src='/js/encrypt.js'></script>
<script>
$(document).ready(function() {
	$('#setting_pw_form').submit(function() {
		if(!$('input[name=password]').val()) {
			qAlert('비밀번호를 입력해주세요');
			$('input[name=email]').focus();
			return false;
		}
		if(!$('input[name=password_confirm]').val()) {
			qAlert('비밀번호 확인을 입력해주세요');
			$('input[name=password_confirm]').focus();
			return false;
		}
		if($('input[name=password]').val() != $('input[name=password_confirm]').val()) {
			qAlert('비밀번호와 비밀번호 확인이 일치하지 않습니다');
			return false;
		}
		$('#setting_pw_form').append(
			$(document.createElement("input")).attr('name', 'p').attr('type', 'hidden')
			.val(hex_sha512($('input[name=password]').val()))
		);
	});
	$('input[name=password]').focus();
});
</script>
<div id='highlight'>&nbsp;
<div id='content' class='solid' >
	<div class='inner'>
		<h2>새 비밀번호 설정</h2>
		<form id="setting_pw_form" method="POST" action='/setting_pw/<?=$_GET['no']?>/<?=$_GET['tmp']?>'>
			<input type='hidden' name='send' value='Y'/>
			<input type='hidden' name='no' value="<?=$_GET['no']?>"/>
			<table style='width: 100%'>
				<tr>
					<th style='width: 120px;'>비밀번호</th>
					<td><input type='password' name='password' class='text'/></td>
				</tr>
				<tr>
					<th>비밀번호 확인</th>
					<td>
						<input type='password' name='password_confirm' class='text'/>
					</td>
				</tr>
				<tr>
					<th colspan='2'>
						<input type='submit' value='변경하기'/>
					</th>
			</table>
			
		</form>
		
	</div>
</div>&nbsp;
</div>
<?
include 'module/_tail.php';
?>