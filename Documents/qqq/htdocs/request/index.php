<?
$_required = true;
include '../config.php';
include '../module/_head.php';

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'request', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner request'>

	<h2 style='margin-bottom: 14px; border: none'>협력 신청</h2>

	<form name='requestForm' enctype='multipart/form-data' method='post' action='/sendmail.php'>
	<input type='hidden' name='mailtype' value='request' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>' >
	<div style='float: left; width: 48%; margin-right: 2%'>
		<h3 style='font: 12pt NanumGothicBold; border-bottom: solid 2px black'>
		모금 신청
		<p style='margin: 4px 0px; font-size: 12px; color: #A2A2A2; text-shadow: 0px 1px #E3E3E3;'>(자선단체/사회적 기업/사회적 프로젝트 및 도움이 필요한 개인)</p>
		</h3>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>단체명</th><td>
			<input type='text' name='req_name' value="<?=$member[mb_name]?>" /></td></tr>
			<tr><th>단체 소개</th><td>
			<textarea name='req_about' rows='4' style='width: 280px'></textarea></td></tr>
			<tr><th>담당자명</th><td>
			<input type='text' name='req_charge' value="<?=$member[mb_name]?>" /></td></tr>
			<tr><th>담당자 연락처</th><td>
			<input type='text' name="req_hp1" value="<?=$mb_hp[0]?>" size='3' maxlength='3' class='numonly' /> -
			<input type='text' name="req_hp2" value="<?=$mb_hp[1]?>" size='4' maxlength='4' class='numonly' /> -
			<input type='text' name="req_hp3" value="<?=$mb_hp[2]?>" size='4' maxlength='4' class='numonly' /></td></tr>
			<tr><th>담당자 이메일</th><td>
			<input type='text' name='req_mail' value='<?=$m_email?>' maxlength='50' style='width: 250px' /></td></tr>
			<tr><th>모금을 원하는<br/>캠페인 내용</th><td>
			<textarea name='req_desc' rows='8' style='width: 280px'></textarea></td></tr>
			<tr><th>목표 모금액</th><td>
			<input type='text' name='req_target' value='<?=$m_email?>' maxlength='50' style='width: 250px' class='numonly' /></td></tr>
			<tr><th>제공 가능한<br/>리워드</th><td>
			<textarea name='req_reward' rows='4' style='width: 280px'></textarea></td></tr>
			<tr><th>첨부 파일</th><td>
			<input type='file' name='attfile' style='width: 100%' /></td></tr>
		</table>

		<div style='text-align: right;  margin: 0px auto'>
		<input type='submit' value='신청하기' style='margin: 20px 0px; border: none; cursor: pointer; background: black; width: 150px; height: 30px; font: 10pt NanumGothic; color: white' onclick="return checkRequestForm();" />
		</div>

	</div>
	</form>

	<form name='talentForm' enctype='multipart/form-data' method='post' action='/sendmail.php'>
	<input type='hidden' name='mailtype' value='talent' />
	<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>' >
	<div style='float: left; width: 48%; margin-left: 2%'>
		<h3 style='font: 12pt NanumGothicBold; border-bottom: solid 2px black'>
		재능기부 신청
		<p style='margin: 4px 0px; font-size: 12px; color: #A2A2A2; text-shadow: 0px 1px #E3E3E3;'>(응원스타/멘토, 리워드 기부, 촬영 등)</p>	
		</h3>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>신청분야</th><td>
			<select name='tal_category'>
				<option value=''></option>
				<option value='응원스타'>응원스타</option>
				<option value='응원멘토'>응원멘토</option>
				<option value='리워드기부'>리워드기부</option>
				<option value='사진촬영'>사진촬영</option>
				<option value='영상촬영'>영상촬영</option>
				<option value='기타'>기타</option>
			</select></td></tr>
			<tr><th>신청자명</th><td>
			<input type='text' name='tal_name' value="<?=$member[mb_name]?>" /></td></tr>
			<tr><th>자기 소개</th><td>
			<textarea name='tal_about' rows='4' style='width: 280px'></textarea></td></tr>
			<tr><th>연락처</th><td>
			<input type='text' name="tal_hp1" value="<?=$mb_hp[0]?>" size='3' maxlength='3' /> -
			<input type='text' name="tal_hp2" value="<?=$mb_hp[1]?>" size='4' maxlength='4' /> -
			<input type='text' name="tal_hp3" value="<?=$mb_hp[2]?>" size='4' maxlength='4' /></td></tr>
			<tr><th>이메일</th><td>
			<input type='text' name='tal_mail' value='<?=$m_email?>' maxlength='50' style='width: 250px' /></td></tr>
			<tr><th>희망하는<br/>재능기부 내용</th><td>
			<textarea name='tal_desc' rows='8' style='width: 280px'></textarea></td></tr>
			<tr><th>첨부 파일</th><td>
			<input type='file' name='attfile' style='width: 100%' /></td></tr>

		</table>

		<div style='text-align: right;  margin: 0px auto'>
		<input type='submit' value='신청하기' style='margin: 20px 0px; border: none; cursor: pointer; background: black; width: 150px; height: 30px; font: 10pt NanumGothic; color: white' onclick="return checkTalentForm();" />
		</div>
	</div>
	</form>
	<div style='clear: both'></div>

</div>

</div>
</div>

<?
include '../module/_tail.php';
?>