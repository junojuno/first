<?php 
$_required = true;
include '../../config.php';
include '../../module/_head.btb.php';

$sql = "SELECT account FROM ".DB_INFO;
$info = sql_fetch($sql);
?>
<script>
$(document).ready(function() {
	$('#req_form .button').click(function() {
		
		if(confirm('요청하시겠습니까?')){
			$('#req_form').submit();
		}
	});
	$('input[name=req_coin]').keyup(function(e) {
		var key = e.keyCode || e.which;
		if(key == 13) {
			$('#req_form .button').trigger('click');
		} 
	});
});
</script>

<div style='padding: 0px 20px 100px 175px'>

	<h2>코인 충전</h2>
	
	<div class='unit'>
		<form id='req_form' action='/btb/hellojeju/do' method='POST'>
			<input type='hidden' name='section' value='charge'/>
			<input type='hidden' name='mode' value='insert'/>
			코인 <input type='text' class='text numonly' name='req_coin' style='text-align:right;width:62px;'/> 개를 충전 요청합니다. 
			<input type='button' class='button' value='요청'/>
		</form>
	</div>
	
	<div class='unit' style='text-align:center;padding:50px 10px;background-color:#DAD7FF;'>
		<p style='font:14pt NanumGothicBold;'><?=$info['account']?></p>
	</div>
	
	<div class='unit'>
		<table>
			<tr>
				<th style='width:140px;'>일자</th>
				<th>내용</th>
				<th style='width:140px;'>상태</th>
			</tr>
			<?php 
			$sql = "SELECT * FROM ".DB_COINS." WHERE mb_no = {$arr_btb['hellojeju']} AND coin_category=4 ORDER BY od_time DESC";
			$result = sql_query($sql);
			if(mysql_num_rows($result) == 0) {
				print "<tr><td colspan='3'>요청 내역이 없습니다.</td></tr>";
			} else {
				for($i =0; $row = sql_fetch_array($result);$i++) {
					$coin = number_format($row['amount']);
					?>
						<tr>
							<td><?=$row['od_time']?></td>
							<td><? print "{$coin} 코인 충전 요청";?></td>
							<td>
								<? if($row['coin_desc'] == 'REQUEST') 
									print "<span color='red'>입금확인중</span>";
								   else
								   	print "<strong>충전완료</strong>";?>
							</td>
						</tr>
					<?
				}
			}
			?>
		</table>
	</div>
</div>