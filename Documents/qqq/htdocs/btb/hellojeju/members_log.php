<?php 
$_required = true;
include '../../config.php';
include '../../module/_head.btb.php';

$sql = "SELECT a.amount, a.od_time, c.it_name, b.mb_name, b.mb_contact FROM ".DB_COINS." a, ".DB_MEMBERS." b, ".DB_CAMPAIGNS." c
		WHERE b.mb_type = 5 AND b.mb_no = a.mb_no AND a.coin_category=9 AND a.it_id = c.it_id
		ORDER BY a.od_time DESC";
$result = sql_query($sql);
$row_num = mysql_num_rows($result);
?>

<div style='padding: 0px 20px 100px 175px'>

<h2>회원 후원내역<span class='help' title='헬로제주 회원들의 후원내역을 보여드립니다'></span></h2>

<div class='unit'>

	<table>
		<tr>
			<th style='width:120px;'>일자</th>
			<th>내용</th>
		</tr>
		
		<? if($row_num == 0) {?>
		<tr>
			<td colspan='2'>회원들의 후원내역이 없습니다</td>
		</tr>
		<? } else {
			for($i =0; $row = sql_fetch_array($result); $i++) {
				$mny = number_format(str_replace('-','',$row[amount]));
			?>
			<tr>
				<td><?=$row['od_time']?></td>
				<td style='text-align:left;padding-left:10px;'>
					
					<?php 
					print "{$row['mb_name']}({$row['mb_contact']})님이 <strong>{$row['it_name']} 캠페인</strong>에 코인 {$mny}개를 후원하였습니다.";
					?>
				</td>
			</tr>
			<? } ?>
		<? }?>
	</table>
</div>

</div>