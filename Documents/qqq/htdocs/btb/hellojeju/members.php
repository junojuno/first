<?
$_required = true;
include '../../config.php';
include '../../module/_head.btb.php';

$search = isset($_POST['search']) && trim($_POST['search']) != '' ? $_POST['search'] : false;

?>
<script>
$(document).ready(function() {
	$('#add_form .button').click(function() {
		if(!$('input[name=user_name]').val()) {
			qAlert('이름을 입력해주세요');
			$('input[name=user_name]').focus();
			return false;
		}

		if(!$('input[name=od_rsv1]').val() || !$('input[name=od_rsv2]').val() || !$('input[name=od_rsv3]').val()) {
			qAlert('예약번호를 입력해주세요');
			return false;
		}
		
		if(!$('input[name=od_hp1]').val() || !$('input[name=od_hp2]').val() || !$('input[name=od_hp3]').val()) {
			qAlert('핸드폰 번호를 입력해주세요');
			return false;
		}
		
		if(!$('input[name=user_coin]').val()) {
			qAlert('코인을 입력해주세요');
			$('input[name=user_coin]').focus();
			return false;
		}

		if($('input[name=user_coin]').val() > <?=getCoin($company_admin)?>) {
			qAlert('잔여 코인이 부족합니다.');
			$('input[name=user_coin]').focus();
			return false;
		}

		//중복체크
		$.ajax({
			type : "POST",
			url : "/btb/hellojeju/dupliCheck.php",
			data : {
				id : $('input[name=od_hp1]').val() + $('input[name=od_hp2]').val() + $('input[name=od_hp3]').val() + "@hellojeju.com"
			},
			cache : false,
			success : function(m) {
				if(m == 'exist') {
					qAlert('이미 등록된 아이디(핸드폰 번호) 입니다.');
					return false;
				} else {
					if(confirm('등록하시겠습니까?')) {
						$('input[name=user_phone]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());
						$('input[name=user_rsv]').val($('input[name=od_rsv1]').val() + '-' + $('input[name=od_rsv2]').val() + '-' + $('input[name=od_rsv3]').val());
						$('#add_form').submit();
					}
				}
			}
		});
	});

	$('input[name=od_hp1]').keyup(function() {
		if($(this).val().length == 3) {
			$('input[name=od_hp2]').focus();
		} 
	});
	$('input[name=od_hp2]').keyup(function() {
		if($(this).val().length == 4) {
			$('input[name=od_hp3]').focus();
		} 
	});
	$('input[name=od_hp3]').keyup(function() {
		if($(this).val().length == 4) {
			$('input[name=user_coin]').focus();
		} 
	});

	$('input[name=search]').keyup(function() {
		if($(this).val().length > 0) {
			$('label[for=search]').hide();
		} else {
			$('label[for=search]').show();
		}
		
	});

	$('input[name=user_coin]').keyup(function(e) {
		var key = e.keyCode || e.which;
		if(key == 13) {
			$('#add_form .button').trigger('click');
		} 
	});

	$('.account').click(function(){
		location.href='members.form/'+$(this).attr('id');
	});
});
</script>
<style>
tr.account:hover{
	background-color:#f8f8f8;
	cursor:pointer;
	
}

</style>
<div style='padding: 0px 20px 100px 175px'>

<h2>회원 관리</h2>

<div class='unit'>
	<h3>계정 생성<span class='help' title='ID : 핸드폰번호@hellojeju.com | PW : 핸드폰번호'></span></h3>
	<form id='add_form' method='POST' action='/btb/hellojeju/do'>
		<input type='hidden' name='section' value='members'/>
		<input type='hidden' name='mode' value='insert'/>
		<input type='hidden' name='user_phone' />
		<input type='hidden' name='user_rsv' />
		<table style='margin:10px 0 0 0; border: solid 1px #DFDFDF; width:300px;'>
			<tr>
				<th class='th_left'>이름</th>
				<td class='td_right'><input type='text' name='user_name' class='text' style='width:125px;' /></td>
			</tr>
			<tr>
				<th class='th_left'>예약번호</th>
				<td class='td_right'>
					<input type='text' name="od_rsv1" maxlength='3' class='numonly text' style='width:32px;'  /> -
					<input type='text' name="od_rsv2" maxlength='4' class='numonly text' style='width:32px;' /> -
					<input type='text' name="od_rsv3" maxlength='4' class='numonly text' style='width:32px;' />
				</td>
			</tr>
			<tr>
				<th class='th_left'>핸드폰번호</th>
				<td class='td_right'>
					<input type='text' name="od_hp1" maxlength='3' class='numonly text' style='width:24px;' /> -
					<input type='text' name="od_hp2" maxlength='4' class='numonly text' style='width:32px;' /> -
					<input type='text' name="od_hp3" maxlength='4' class='numonly text' style='width:32px;' />
				</td>
			</tr>
			<tr>
				<th class='th_left'>코인배정</th>
				<td class='td_right'><input type='text' name='user_coin' class='numonly text' style='width:125px;' /></td>
			</tr>
			<tr>
				<td class='td_right' colspan='2' style='text-align:right;'>
					<input type='button' class='button' value='계정 생성'/>
				</td>
			</tr>
		</table>
	</form>
</div>

<div class='unit'>
	
	<form id='search_form' method='POST'>
		<input type='text' id='search' name='search' style='width:300px;height:25px;'/>
		<label for='search' style='position:absolute;left:4px;top:9px;color:#A5A5A5;cursor:text;font:12px NanumGothic;'>이름, 예약번호, 핸드폰번호로 검색</label>
		<input type='submit' class='button' value='계정 검색'>
	</form>
</div>

<?php 
	if($search) {
		$sql = "SELECT a.mb_no, a.mb_name, a.mb_4, a.mb_contact, a.mb_date, (SELECT SUM(b.amount) FROM ".DB_COINS." b WHERE a.mb_no =b.mb_no) AS coin_sum FROM ".DB_MEMBERS." a  
			WHERE a.mb_type = 5 AND (a.mb_name LIKE '%{$search}%' OR a.mb_4 LIKE '%{$search}%' OR a.mb_contact LIKE '%{$search}%') 
			ORDER BY a.mb_date DESC";
	} else {
		$sql = "SELECT a.mb_no, a.mb_name, a.mb_4, a.mb_contact, a.mb_date, (SELECT SUM(b.amount) FROM ".DB_COINS." b WHERE a.mb_no =b.mb_no) AS coin_sum 
			FROM ".DB_MEMBERS." a WHERE a.mb_type = 5 ORDER BY a.mb_date DESC";
	}
	$result = sql_query($sql);
	$row_num = mysql_num_rows($result);
?>

<div class='unit'>
	<h3>계정 목록</h3>
	<table style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
		<tr>
			<th>No.</th>
			<th>이름</th>
			<th>예약번호</th>
			<th>핸드폰번호</th>
			<th>코인현황</th>
			<th>생성일시</th>
		</tr>
	   <? if($row_num == 0) { ?>
         <tr>
            <td colspan='6' style='text-align:center;font:12px NanumGothic;'>
               <? if($search) { ?>
                  검색결과가 없습니다.
               <? } else { ?>
                  등록된 계정이 없습니다.
               <? } ?>
            </td>
         </tr>
      <? } ?>
		<? for($i =0; $row = sql_fetch_array($result); $i++) { ?>
			<tr id='<?=$row[mb_no]?>' class='account'>
				<td><?=$row_num-$i?></td>
				<td><?=$row[mb_name]?></td>
				<td><?=$row[mb_4]?></td>
				<td><?=$row[mb_contact]?></td>
				<td><?=$row[coin_sum]?></td>
				<td><?=$row[mb_date]?></td>
			</tr>
		<? }?>
	</table>
</div>

</div>

</body>