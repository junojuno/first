<?
$_required = true;
include '../../config.php';
sec_session_start();

if ($_SESSION[user_no] != $arr_btb[hellojeju] && array_search($_SESSION[user_no], $settings[admin]) === false) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

foreach ($_REQUEST as $key => $value) {
	$$key = $value;
}

$url = '';
$sections = array('members', 'charge');
if (array_search($section, $sections) === false) exit;

if($section == 'members') {
	switch($mode) {
		case 'insert':
			if($user_coin > getCoin($arr_btb[hellojeju])) {
				alert('남은 코인 수가 배정코인보다 적어 배정이 불가능합니다.');
				exit;
			}
			$onlyNum = str_replace('-','',trim($user_phone));
			$password = hash('sha512', $onlyNum);
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$password = hash('sha512', $password.$random_salt);
			$sql = "INSERT INTO ".DB_MEMBERS." SET 
				mb_id = '{$onlyNum}@hellojeju.com',
				mb_type = 5,
				mb_email ='{$onlyNum}@hellojeju.com',
				mb_contact = '{$user_phone}',
				mb_name = '{$user_name}',
				mb_4 = '{$user_rsv}',
				mb_password = '{$password}',
				salt = '{$random_salt}'
				";
			sql_query($sql);
			
			$mb_no = mysql_insert_id();
			$sql = "INSERT INTO ".DB_COINS." SET 
				mb_no = '$mb_no',
				coin_category = 10,
				coin_desc = 'FIRST CHARGE',
				amount = '$user_coin'
				";
			sql_query($sql);
			
			$sql = "INSERT INTO ".DB_COINS." SET
				mb_no = {$arr_btb['hellojeju']},
				coin_category = 14,
				coin_desc = 'FIRST CHARGE',
				od_id = {$mb_no}, 
				amount = '-{$user_coin}'
			"; // 여기선 od_id가 코인 제공한 mb_no를 저장
			sql_query($sql);
			$url = 'members';
			break;
			
		case 'modify':
			$onlyNum = str_replace('-','',trim($user_phone));
			$password = hash('sha512', $onlyNum);
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$password = hash('sha512', $password.$random_salt);
			
			$sql = "UPDATE ".DB_MEMBERS." SET
				mb_id = '{$onlyNum}@hellojeju.com',
				mb_type = 5,
				mb_email ='{$onlyNum}@hellojeju.com',
				mb_contact = '{$user_phone}',
				mb_name = '{$user_name}',
				mb_4 = '{$user_rsv}',
				mb_password = '{$password}',
				salt = '{$random_salt}'
				WHERE mb_no = {$mb_no}";
			sql_query($sql);
			$url = 'members.form/'.$mb_no;
			break;
			
		case 'coin':
			if($mode_2 == 'plus') {
				if($amount > getCoin($arr_btb[hellojeju])) {
					alert('남은 코인 수가 배정코인보다 적어 배정이 불가능합니다.');
					exit;
				}
				$sql = "INSERT INTO ".DB_COINS." SET
					mb_no = '{$mb_no}',
					coin_category = 10,
					coin_desc = 'CHARGE',
					amount = '{$amount}'
					";
				sql_query($sql);
				
				$sql = "INSERT INTO ".DB_COINS." SET
					mb_no = '{$arr_btb[hellojeju]}',
					coin_category = 16,
					coin_desc = 'CHARGE',
					od_id = {$mb_no}, 
					amount = '-{$amount}'
					";
				sql_query($sql);
			} else {
				if($amount > getCoin($mb_no)) {
					alert('회수할 코인 수가 보유코인보다 적어 회수가 불가능합니다.');
					exit;
				}
				$sql = "INSERT INTO ".DB_COINS." SET
				mb_no = '{$mb_no}',
				coin_category = 11,
				coin_desc = 'MINUS',
				amount = '-{$amount}'
				";
				sql_query($sql);
				
				$sql = "INSERT INTO ".DB_COINS." SET
				mb_no = '{$arr_btb[hellojeju]}',
				coin_category = 15,
				coin_desc = 'CHARGE',
				od_id = {$mb_no},
				amount = '{$amount}'
				";
				sql_query($sql);
			}
			$url = 'members.form/'.$mb_no;
			break;
	}
	
} else if($section == 'charge') {
	$sql = "INSERT INTO ".DB_COINS." SET
			mb_no = '{$arr_btb[hellojeju]}',
			coin_category = 4,
			coin_desc = 'REQUEST',
			amount = '{$req_coin}'";
	sql_query($sql);
	$url = 'charge';
}

header("Location: $url");
?>


