<?
$_required = true;
include '../../config.php';
include '../../module/_head.btb.php';

$search = isset($_POST['search']) && trim($_POST['search']) != '' ? $_POST['search'] : false;

?>
<script>
$(document).ready(function() {

	$('#add_form .button').click(function() {
		if(!$('input[name=user_name]').val()) {
			qAlert('이름을 입력해주세요');
			$('input[name=user_name]').focus();
			return false;
		}

		if(!$('input[name=user_cpn]').val()) {
			qAlert('사번을 입력해주세요');
         $('input[name=user_cpn]').focus();
			return false;
		}
		
		if(!$('input[name=user_coin]').val()) {
			qAlert('코인을 입력해주세요');
			$('input[name=user_coin]').focus();
			return false;
		}

		if($('input[name=user_coin]').val() > <?=getCoin($company_admin)?>) {
			qAlert('잔여 코인이 부족합니다.');
			$('input[name=user_coin]').focus();
			return false;
		}

		//중복체크
		$.ajax({
			type : "POST",
			url : "/btb/<?=$company?>/dupliCheck.php",
			data : {
				id : $('input[name=user_cpn]') + "@<?=$company_email?>"
			},
			cache : false,
			success : function(m) {
				if(m == 'exist') {
					qAlert('이미 등록된 아이디(사번) 입니다.');
					return false;
				} else {
					if(confirm('등록하시겠습니까?')) {
						$('#add_form').submit();
					}
				}
			}
		});
	});

   $("#take_off_form .button").click(function() {
      if(confirm('정말 전부 회수하시겠습니까?')) {
         $("#take_off_form").submit();
      }
   });

	$('input[name=search]').keyup(function() {
		if($(this).val().length > 0) {
			$('label[for=search]').hide();
		} else {
			$('label[for=search]').show();
		}
		
	});

	$('input[name=user_coin]').keyup(function(e) {
		var key = e.keyCode || e.which;
		if(key == 13) {
			$('#add_form .button').trigger('click');
		} 
	});

	$('.account').click(function(){
		location.href='members.form/'+$(this).attr('id');
	});
});
</script>
<style>
tr.account:hover{
	background-color:#f8f8f8;
	cursor:pointer;
	
}

</style>
<div style='padding: 0px 20px 100px 175px'>

<h2>회원 관리</h2>

<div class='unit'>
	<h3>계정 생성<span class='help' title='ID : 사번@<?=$company_email?> | PW : 사번'></span></h3>
	<form id='add_form' method='POST' action='/btb/<?=$company?>/do'>
		<input type='hidden' name='section' value='members'/>
		<input type='hidden' name='mode' value='insert'/>
		<table style='margin:10px 0 0 0; border: solid 1px #DFDFDF; width:300px;'>
			<tr>
				<th class='th_left'>이름</th>
				<td class='td_right'><input type='text' name='user_name' class='text' style='width:125px;' /></td>
			</tr>
			<tr>
				<th class='th_left'>사번</th>
				<td class='td_right'>
					<input type='text' name="user_cpn" class='numonly text' style='width:125px;'/>
				</td>
			</tr>
			<tr>
				<th class='th_left'>코인배정</th>
				<td class='td_right'><input type='text' name='user_coin' class='numonly text' style='width:125px;' value="0" /></td>
			</tr>
			<tr>
				<td class='td_right' colspan='2' style='text-align:right;'>
					<input type='button' class='button' value='계정 생성'/>
				</td>
			</tr>
		</table>
	</form>
</div>

<div class='unit'>
	
	<form id='search_form' method='POST'>
		<input type='text' id='search' name='search' style='width:300px;height:25px;'/>
		<label for='search' style='position:absolute;left:4px;top:9px;color:#A5A5A5;cursor:text;font:12px NanumGothic;'>이름, 사번, 소속 검색</label>
		<input type='submit' class='button' value='계정 검색'>
	</form>
   <form id='take_off_form' method='POST' action='/btb/<?=$company?>/do'>
      <input type='hidden' name='section' value='members'/>
      <input type='hidden' name='mode' value='coinoff'/>
      <input type='button' class='button' value='계정 코인 전부 회수' style='margin:0;margin-top:10px;'><span class='help' title='<?=$company_kr?> 계정의 코인들을 전부 관리자계정으로 회수합니다.'></span>
   </form>


</div>

<?php 
	if($search) {
		$sql = "SELECT a.mb_id, a.mb_no, a.mb_name, a.mb_date, (SELECT SUM(b.amount) FROM ".DB_COINS." b WHERE a.mb_no =b.mb_no) AS coin_sum FROM ".DB_MEMBERS." a  
			WHERE a.mb_type = {$company_type} AND (a.mb_name LIKE '%{$search}%' OR a.mb_id LIKE '%{$search}%') 
			ORDER BY a.mb_date DESC";
	} else {
		$sql = "SELECT a.mb_id, a.mb_no, a.mb_name, a.mb_date, (SELECT SUM(b.amount) FROM ".DB_COINS." b WHERE a.mb_no =b.mb_no) AS coin_sum 
			FROM ".DB_MEMBERS." a WHERE a.mb_type = {$company_type} ORDER BY a.mb_date DESC";
	}
	$result = sql_query($sql);
	$row_num = mysql_num_rows($result);
?>

<div class='unit'>
	<h3>계정 목록</h3>
	<table style='margin: 30px 0px; width: 100%; border: solid 1px #DFDFDF'>
		<tr>
			<th>No.</th>
			<th>이름</th>
			<th>사번</th>
			<th>코인현황</th>
			<th>생성일시</th>
		</tr>
	   <? if($row_num == 0) { ?>
         <tr>
            <td colspan='6' style='text-align:center;font:12px NanumGothic;'>
               <? if($search) { ?>
                  검색결과가 없습니다.
               <? } else { ?>
                  등록된 계정이 없습니다.
               <? } ?>
            </td>
         </tr>
      <? } ?>
		<? for($i =0; $row = sql_fetch_array($result); $i++) { 
         $cpn = explode('@', $row[mb_id]);?>
			<tr id='<?=$row[mb_no]?>' class='account'>
				<td><?=$row_num-$i?></td>
				<td><?=$row[mb_name]?></td>
				<td><?=$cpn[0]?></td>
				<td><?=$row[coin_sum]?></td>
				<td><?=$row[mb_date]?></td>
			</tr>
		<? }?>
	</table>
</div>

</div>

</body>