<?php
$_required = true;
include '../../config.php';
include '../../module/_head.btb.php';

if(!(isset($_GET['id']) && $_GET['id'] != '')) {
	alert('잘못된 접근입니다.');
	exit;
}

$mb_no = $_GET['id'];

$sql = "SELECT * FROM ".DB_MEMBERS." WHERE mb_no = {$mb_no} AND mb_type = {$company_type}";
$result = sql_query($sql);
if(mysql_num_rows($result) == 0) {
	alert('<?=$company_kr?> 계정이 아닙니다.');
	exit;
}

$info = sql_fetch_array($result);
$cpn = explode('@', $info[mb_id]);
$sql = "SELECT a.amount, a.od_time, a.coin_category, b.it_name FROM ".DB_COINS." a
		LEFT JOIN ".DB_CAMPAIGNS." b ON b.it_id = a.it_id
		WHERE a.mb_no={$mb_no}
		ORDER BY id DESC";
$log_result = sql_query($sql);

$sql = "SELECT SUM(amount) AS coin_sum FROM ".DB_COINS." WHERE mb_no = {$mb_no}";
$coin = sql_fetch($sql);
$coin_sum = $coin['coin_sum'];
?>

<script>
$(document).ready(function() {
	$('#modify_form .mod_btn').click(function() {
		if(!$('input[name=user_name]').val()) {
			qAlert('이름을 입력해주세요');
			$('input[name=user_name]').focus();
			return false;
		}

		if(!$('input[name=user_cpn]').val()) {
			qAlert('사번을 입력해주세요');
         $('input[name=user_cpn]').focus();
			return false;
		}
		

		//중복체크
		$.ajax({
			type : "POST",
			url : "/btb/<?=$company?>/dupliCheck.php",
			data : {
				id : $('input[name=user_cpn]').val() + "@<?=$company_email?>",
				origin_id : "<?=$info[mb_id]?>"
			},
			cache : false,
			success : function(m) {
				if(m == 'exist') {
					qAlert('이미 등록된 아이디(사번) 입니다.');
					return false;
				} else {
					if(confirm('계정 정보를 수정하시겠습니까?')) {
						$('#modify_form').submit();
					}
				}
			}
		});
	});

	$('.coin').click(function() {
		if(!$('#modify_form input[name=amount]').val()) {
			qAlert('코인을 입력하세요.');
			$('#modify_form input[name=amount]').focus();
			return false;
		}
		
		$('#coin_form input[name=amount]').val($('#modify_form input[name=amount]').val());
		if($(this).val() == '배정') {
			$('#coin_form input[name=mode_2]').val('plus');
		} else {
			$('#coin_form input[name=mode_2]').val('minus');
		}

		if(confirm('코인을 배정 또는 회수하시겠습니까?')){
			$('#coin_form').submit();
		}
	});
});
</script>
<style>
.coin {
	margin:0;
	margin-left:5px;
}
</style>

<div style='padding: 0px 20px 100px 175px'>

	<h2>회원 정보</h2>
	
	<div class='unit'>
		
		<form id='modify_form' method='POST' action='/btb/<?=$company?>/do'>
			<input type='hidden' name='section' value='members'/>
			<input type='hidden' name='mode' value='modify'/>
			<input type='hidden' name='mb_no' value='<?=$mb_no?>'/>
			
			<table style='width:295px;'>
				<tr>
					<th class='th_left'>계정ID</th>
					<td class='td_right'><?=$info[mb_id]?></td> 
				</tr>
				<tr>
					<th class='th_left'>사번<span class='help' title='사번을 바꾸시면 계정ID와 비밀번호(바꾼 사번)가 바뀝니다.'></span></th>
					<td class='td_right'>
						<input type='text' name='user_cpn' class='text' style='width:125px;' value="<?=$cpn[0]?>"/>
					</td>
				</tr>
				<tr>
					<th class='th_left'>이름</th>
					<td class='td_right'><input type='text' name='user_name' class='text' style='width:125px;' value='<?=$info[mb_name]?>'/></td>
				</tr>
				<tr>
					<th class='th_left'>코인현황</th>
					<td class='td_right' style='padding-top:5px;padding-bottom:5px;'>
						<?=number_format($coin_sum)?>개 <br/><br/>
						코인 <input type='text' class='text numonly' name='amount' style='text-align:right;width:62px;'/> 개 
						<input type='button' class='coin button' value='배정'/><input type='button' class='coin button' value='회수' style=''/>
						
					</td>
				</tr>
				<tr>
				<td class='td_right' colspan='2' style='text-align:right;'>
					<input type='button' class='mod_btn button' value='수정'/>
				</td>
			</tr>
			</table>
		</form>
	</div>
	
	
	<div class='unit'>
		<table>
			<tr>
				<th>일자</th>
				<th>Action</th>
				<th>보유 코인(개)</th>
			</tr>
			<?php 
			$totalAmount = 0;
			for($i =0; $row = sql_fetch_array($log_result);$i++) {
			?>
				<tr>
					<td style='width:135px;'><?=$row['od_time']?></td>
					<td>
					<?php 
					$coin = number_format(abs($row[amount]));
					switch ($row[coin_category]) {
						case 9:
							$desc = "코인 {$coin}개 <strong>{$row['it_name']} 캠페인</strong>에 후원";
							break;
						case 10: // 제공되는건 정기후원처리안함 , 10~11 : 관리자에 생성한 계정 로그 | 12~16 : 관리자 계정 로그
							$desc = "코인 {$coin}개 배정";
							break;
						case 11:
							$desc = "코인 {$coin}개 회수";
							break;
					}
					print $desc;
					?>
					</td>
					<td style='width:100px;'><?=number_format($coin_sum)?></td>
				</tr>
				
			<?
				$coin_sum -= intval($row[amount]);
				$totalAmount += (intval($row[amount]) > 0) ? intval($row[amount]) : 0;
			}
			?>
		</table>
	</div>
	
	<div class='unit'>
		<form id='coin_form' method='POST' action='/btb/<?=$company?>/do'>
			<input type='hidden' name='section' value='members'/>
			<input type='hidden' name='mode' value='coin'/>
			<input type='hidden' name='mode_2' />
			<input type='hidden' name='mb_no' value='<?=$mb_no?>' />
			<input type='hidden' name='amount' />
		</form>
	</div>

</div>