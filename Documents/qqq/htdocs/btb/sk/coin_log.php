<?php 
$_required = true;
include '../../config.php';
include '../../module/_head.btb.php';
?>


<div style='padding: 0px 20px 100px 175px'>

<h2>코인 로그<span class='help' title='<?=$company_kr?>님의 코인사용내역을 보여드립니다.'></span></h2>

<div class='unit'>
	<table style='width: 100%; border: solid 1px #DFDFDF'>
		<tr>
			<th style='width:130px;'>일자</th>
			<th >내용</th>
		</tr>
		<?php 
		$sql = "SELECT a.coin_category, a.amount, a.od_time, b.mb_name, b.mb_id FROM ".DB_COINS." a 
			LEFT JOIN ".DB_MEMBERS." b ON b.mb_no = a.od_id 
			LEFT JOIN ".DB_CAMPAIGNS. " c ON a.coin_category = 9 AND c.it_name = a.it_id
			WHERE a.mb_no={$company_admin} AND a.coin_category != 4 
			ORDER BY a.od_time DESC";
		$result = sql_query($sql);
		if(mysql_num_rows($result) == 0) {
			print "<tr><td colspan='2'>로그가 존재하지 않습니다.</td></tr>";
		} else {
			for($i =0; $row = sql_fetch_array($result); $i++) {
				$mny = number_format(str_replace('-','',$row[amount]));
            $cpn = explode('@', $row[mb_id]);
			?>
			<tr>
				<td><?=$row['od_time']?></td>
				<td style='padding-left:10px;text-align:left;'>
					<?php 
					switch($row['coin_category']) {
						case 9 :
							print "{$company_kr}님이 <strong>{$row['it_name']} 캠페인</strong>에 코인 <strong>{$mny}개</strong> 후원";
							break;
						case 12 :
							print "위젠에서 {$company_kr}님의 코인 <strong>{$mny}개</strong> 충전";
							break;
						case 13 :
							print "위젠에서 {$company_kr}님의 코인 <strong>{$mny}개</strong> 회수";
							break;
						case 14 :
							print "{$company_kr}님이 <strong>{$row['mb_name']}</strong>({$cpn[0]})님의 계정 생성(코인 : <strong>{$mny}개</strong>)";
							break;
						case 15 :
							print "{$company_kr}님이 <strong>{$row['mb_name']}</strong>({$cpn[0]})님의 코인 <strong>{$mny}개</strong> 회수";
							break;
						case 16 :
							print "{$company_kr}님이 <strong>{$row['mb_name']}</strong>({$cpn[0]})님의 코인 <strong>{$mny}개</strong> 충전";
							break;
					}
					?>
				</td>
			</tr>
			<?php } 
		} ?>
	</table>
</div>

</div>