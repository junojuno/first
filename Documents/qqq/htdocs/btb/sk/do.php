<?
$_required = true;
include '../../config.php';
sec_session_start();
$cpy_email = "sk.com";
$mb_type = 10;
$cpy_admin = $arr_btb['sk'];

if ($_SESSION[user_no] != $cpy_admin && array_search($_SESSION[user_no], $settings[admin]) === false) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

foreach ($_REQUEST as $key => $value) {
	$$key = $value;
}

$url = '';
$sections = array('members', 'charge');
if (array_search($section, $sections) === false) exit;

if($section == 'members') {
   
	switch($mode) {
		case 'insert':
			if($user_coin > getCoin($cpy_admin)) {
				alert('남은 코인 수가 배정코인보다 적어 배정이 불가능합니다.');
				exit;
			}
			$password = hash('sha512', $user_cpn);
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$password = hash('sha512', $password.$random_salt);
			$sql = "INSERT INTO ".DB_MEMBERS." SET 
				mb_id = '{$user_cpn}@{$cpy_email}',
				mb_type = 10,
				mb_email ='{$user_cpn}@{$cpy_email}',
				mb_name = '{$user_name}',
				mb_4 = '{$mb_4}',
				mb_password = '{$password}',
				salt = '{$random_salt}'
				";
			sql_query($sql);
			
			$mb_no = mysql_insert_id();
			$sql = "INSERT INTO ".DB_COINS." SET 
				mb_no = '$mb_no',
				coin_category = 10,
				coin_desc = 'FIRST CHARGE',
				amount = '$user_coin'
				";
			sql_query($sql);
			
			$sql = "INSERT INTO ".DB_COINS." SET
				mb_no = {$cpy_admin},
				coin_category = 14,
				coin_desc = 'FIRST CHARGE',
				od_id = {$mb_no}, 
				amount = '-{$user_coin}'
			"; // 여기선 od_id가 코인 제공한 mb_no를 저장
			sql_query($sql);
			$url = 'members';
			break;
			
		case 'modify':
			$password = hash('sha512', $user_cpn);
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			$password = hash('sha512', $password.$random_salt);
			
			$sql = "UPDATE ".DB_MEMBERS." SET
				mb_id = '{$user_cpn}@{$cpy_email}',
				mb_email ='{$user_cpn}@{$cpy_email}',
				mb_name = '{$user_name}',
				mb_4 = '{$mb_4}',
				mb_password = '{$password}',
				salt = '{$random_salt}'
				WHERE mb_no = {$mb_no}";
			sql_query($sql);
			$url = 'members.form/'.$mb_no;
			break;
			
		case 'coin':
			if($mode_2 == 'plus') {
				if($amount > getCoin($cpy_admin)) {
					alert('남은 코인 수가 배정코인보다 적어 배정이 불가능합니다.');
					exit;
				}
				$sql = "INSERT INTO ".DB_COINS." SET
					mb_no = '{$mb_no}',
					coin_category = 10,
					coin_desc = 'CHARGE',
					amount = '{$amount}'
					";
				sql_query($sql);
				
				$sql = "INSERT INTO ".DB_COINS." SET
					mb_no = '{$cpy_admin}',
					coin_category = 16,
					coin_desc = 'CHARGE',
					od_id = {$mb_no}, 
					amount = '-{$amount}'
					";
				sql_query($sql);
			} else {
				if($amount > getCoin($mb_no)) {
					alert('회수할 코인 수가 보유코인보다 적어 회수가 불가능합니다.');
					exit;
				}
				$sql = "INSERT INTO ".DB_COINS." SET
				mb_no = '{$mb_no}',
				coin_category = 11,
				coin_desc = 'MINUS',
				amount = '-{$amount}'
				";
				sql_query($sql);
				
				$sql = "INSERT INTO ".DB_COINS." SET
				mb_no = '{$cpy_admin}',
				coin_category = 15,
				coin_desc = 'CHARGE',
				od_id = {$mb_no},
				amount = '{$amount}'
				";
				sql_query($sql);
			}
			$url = 'members.form/'.$mb_no;
			break;

      case 'coinoff':
            $sql = "SELECT SUM((SELECT SUM(amount) FROM ".DB_COINS." b WHERE b.mb_no = a.mb_no)) as coin_sum FROM ".DB_MEMBERS." a WHERE a.mb_type = {$mb_type}";
            $before = sql_fetch($sql);
            $str = "[회수전] 전체 회원코인 : ".number_format($before[coin_sum])."개\n";
            $sql = "SELECT a.mb_no, (SELECT SUM(b.amount) FROM ".DB_COINS." b WHERE b.mb_no = a.mb_no) as coin_sum FROM ".DB_MEMBERS." a 
               WHERE a.mb_type = {$mb_type}";
            $mem_res = sql_query($sql);
            $f_mems = array();
            $f_admin = array();
            $fIdx1 = 0;
            $fIdx2 = 0;
            for($i =0; $row = sql_fetch_array($mem_res); $i++) {
               if($row[coin_sum] <= 0 ) {
                  continue;
               }
               $sql = "INSERT INTO ".DB_COINS." SET
               mb_no = '{$row[mb_no]}',
               coin_category = 11,
               coin_desc = 'MINUS',
               amount = '-{$row[coin_sum]}'
               ";
               $res = sql_query($sql);
               if(!$res) {
                  $f_mems[$fIdx1][mb_no] = $row[mb_no];
                  $f_mems[$fIdx1++][coin_sum] = $row[coin_sum];
                  continue;
               }
               
               $sql = "INSERT INTO ".DB_COINS." SET
               mb_no = '{$cpy_admin}',
               coin_category = 15,
               coin_desc = 'CHARGE',
               od_id = {$row[mb_no]},
               amount = '{$row[coin_sum]}'
               ";
               $res = sql_query($sql);
               if(!$res) {
                  $f_admin[$fIdx2][mb_no] = $row[mb_no];
                  $f_admin[$fIdx2++][coin_sum] = $row[coin_sum];
                  continue;
               }
            }

            while($fIdx1 != 0) {
               $tmp_mems = $f_mems;
               $f_mems = array();
               $fIdx1 = 0;
               for($i =0; $i < count($tmp_mems); $i++) {
                  $sql = "INSERT INTO ".DB_COINS." SET
                  mb_no = '{$tmp_mems[mb_no]}',
                  coin_category = 11,
                  coin_desc = 'MINUS',
                  amount = '-{$tmp_mems[coin_sum]}'
                  ";
                  $res = sql_query($sql);
                  if(!$res) {
                     $f_mems[$fIdx1][mb_no] = $tmp_mems[mb_no];
                     $f_mems[$fIdx1++][coin_sum] = $tmp_mems[coin_sum];
                     continue;
                  }
                  
                  $sql = "INSERT INTO ".DB_COINS." SET
                  mb_no = '{$cpy_admin}',
                  coin_category = 15,
                  coin_desc = 'CHARGE',
                  od_id = {$tmp_mems[mb_no]},
                  amount = '{$tmp_mems[coin_sum]}'
                  ";
                  $res = sql_query($sql);
                  if(!$res) {
                     $f_admin[$fIdx2][mb_no] = $tmp_mems[mb_no];
                     $f_admin[$fIdx2++][coin_sum] = $tmp_mems[coin_sum];
                     continue;
                  }
               }
            }

            while($fIdx2 != 0 ) {
               $tmp_admin = $f_admin;
               $f_admin = array();
               $fIdx2 = 0;
               for($i =0; $i < count($tmp_admin);$i++) {
                  $sql = "INSERT INTO ".DB_COINS." SET
                  mb_no = '{$cpy_admin}',
                  coin_category = 15,
                  coin_desc = 'CHARGE',
                  od_id = {$tmp_admin[mb_no]},
                  amount = '{$tmp_admin[coin_sum]}'
                  ";
                  $res = sql_query($sql);
                  if(!$res) {
                     $f_admin[$fIdx2][mb_no] = $tmp_admin[mb_no];
                     $f_admin[$fIdx2++][coin_sum] = $tmp_admin[coin_sum];
                     continue;
                  }
               }
            }

            $sql = "SELECT SUM((SELECT SUM(amount) FROM wegen_coin b WHERE b.mb_no = a.mb_no)) as coin_sum FROM wegen_member a WHERE a.mb_type = 10";
            $after = sql_fetch($sql);
            $str .= "[회수후] 전체 회원코인 : ".number_format($after[coin_sum])."개";
            echo "<script>alert('$str');</script>";
            $url = "members";
         break;

	}
	
} else if($section == 'charge') {
	$sql = "INSERT INTO ".DB_COINS." SET
			mb_no = '{$cpy_admin}',
			coin_category = 4,
			coin_desc = 'REQUEST',
			amount = '{$req_coin}'";
	sql_query($sql);
	$url = 'charge';
}

header("Location: $url");
?>


