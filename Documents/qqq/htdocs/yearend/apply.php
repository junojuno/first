<?
$_required = false;
$_loginrequired = true;
include '../config.php';
include '../module/_head.php';

//sk 계정 체크
$info = sql_fetch("SELECT mb_type FROM ".DB_MEMBERS." WHERE mb_no = {$_SESSION['user_no']}");
if($info['mb_type'] == 10) {
   alert('해당 계정은 영수증을 신청하실 수 없습니다.');
   exit;
}

$apply_sum = sql_fetch("SELECT SUM(od_amount) AS amount FROM ".DB_ORDERS." a WHERE a.isReceipt = 0 AND a.mb_no = {$_SESSION['user_no']} AND 
      a.it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." b WHERE b.it_isReceipt = 1 and b.it_startdate >=20130104 and it_startdate <=20131108)");
$apply_sum = $apply_sum['amount'] ? $apply_sum['amount'] : 0;

if($apply_sum == 0) {
   alert('영수증 신청할 금액이 없습니다.');
   exit;
}

$sql = "SELECT a.od_amount, a.isReceipt, a.od_time, c.it_name, c.category, c.it_isReceipt FROM ".DB_ORDERS." a, ".DB_CAMPAIGNS." c WHERE 
   a.mb_no = {$_SESSION[user_no]} AND a.it_id = c.it_id AND a.isReceipt = 0 AND
   a.it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." b WHERE b.it_startdate >=20130104 and b.it_startdate <=20131108)";
$res = sql_query($sql);


// ACTIVITY LOG
if ($_SESSION[user_no]) {
   mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'yearend_apply', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<script>
$(document).ready(function() {
   $('input[name=isReceipt]').change(function() {
      if ($(this).val() == 1) {
         $('.ppForm').show();
         $('.pcForm').hide();
      }
      else if ($(this).val() == 2) {
         $('.ppForm').hide();
         $('.pcForm').show();
      }
   });

   $('input[type=text]').addClass('text');

   $('#yearend_submit').click(function() {

      var isReceipt = checkRadioValue(document.receipt_form.isReceipt);
      if (isReceipt == 1) {
         if (!$.trim($('input[name=realname]').val())) {
            qAlert('영수증 발행을 위해 실명을 입력해 주세요.');
            $('input[name=realname]').focus();
            return false;
         }
         if (!$.trim($('input[name=serial1]').val()) || !$.trim($('input[name=serial2]').val())) {
            qAlert('영수증 발행을 위해 주민등록번호를 입력해 주세요.');
            $('input[name=serial1]').focus();
            return false;
         }
      }
      if (isReceipt == 2) {

         if (!$.trim($('input[name=permit1]').val()) || !$.trim($('input[name=permit2]').val()) || !$.trim($('input[name=permit3]').val())) {
            qAlert('영수증 발행을 위해 사업자등록번호를 입력해 주세요.');
            $('input[name=permit1]').focus();
            return false;
         }

         var ext = $('input[name=permitimage]').val().split('.').pop().toLowerCase();
         if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            qAlert('사업자등록증 이미지가 올바른 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
            return false;
         }
      }
      $('#receipt_form').submit();
   });
});
</script>

<div id='highlight'>&nbsp;
   <div id='content' class='solid'>
      <div class='inner yearend'>
         <h2 style='margin-bottom: 14px; border: none'>기부금 영수증 신청</h2>
         <div class='grayborder'>
            <h3>2013년 나의 기부금 내역</h3>
            <table style='border:1px solid #c0c0c0;font:11pt NanumGothicBold;text-align:center;'>
               <tr>
                  <td style='border:0;'>기부금 영수증 신청 금액 : <span style='color:red;'><?=number_format($apply_sum)?></span> 원</td>
               </tr>
            </table>
            <form id='receipt_form' name='receipt_form' action='./result' method="POST" enctype='multipart/form-data'>
               <table style='text-align:center;margin:30px 0;'>
                  <tr>
                     <td class='top_row'>카테고리</td>
                     <td class='top_row'>캠페인명</td>
                     <td class='top_row'>결제금액</td>
                     <td class='top_row'>기부날짜</td>
                     <td class='top_row'>영수증 신청여부</td>
                  </tr>
                  <?php

                  while($row = sql_fetch_array($res)) {
                     $date = explode(' ', $row['od_time']);
                     ?>
                     <tr>
                        <td>[<?=$row['category']?>]</td>
                        <td><?=$row['it_name']?></td>
                        <td style='text-align:right;'><?=number_format($row['od_amount'])?> 원</td>
                        <td><?=$date[0]?></td>
                        <td>미신청</td>
                     </tr>
                     <?
                  }
                  ?>
               </table>
               
               <ul class='receipt_info' style='margin-left:4px;'>
                  <li><input type='radio' name='isReceipt' value='1' id='pp' checked /><label for='pp'>개인</label></li>
                  <li><input type='radio' name='isReceipt' value='2' id='pc' /><label for='pc'>사업자</label></li>
               </ul>
               <div style='clear: both'></div>
         
               <table cellpadding='0' cellpadding='0' style='width: 326px;'>
                  <colgroup>
                     <col width='120' />
                     <col width='/' />
                  </colgroup>
                  <tr class='ppForm'><th>실명</th><td>
                  <input type='text' name='realname' maxlength='10' /></td></tr>
                  <tr class='ppForm'><th>주민등록번호</th><td>
                  <input type='text' name='serial1' maxlength='6' class='numonly' style='width: 50px; text-align: center' /> - <input type='text' name='serial2' maxlength='7' class='numonly' style='width: 50px; text-align: center' /></td></tr>
                  <tr class='pcForm' style='display: none'><th>사업자등록번호</th><td>
                  <input type='text' name='permit1' maxlength='3' class='numonly' style='width: 30px; text-align: center' /> - <input type='text' name='permit2' maxlength='2' class='numonly' style='width: 20px; text-align: center' /> - <input type='text' name='permit3' maxlength='5' class='numonly' style='width: 50px; text-align: center' /></td></tr>
                  <tr class='pcForm' style='display: none'><th>등록증 사본</th><td>
                  <input type='file' name='permitimage' style='width: 100%; text-align: center' /></td></tr>
               </table>


               <div style='width:100%;text-align:center;margin-top:37px;'>
                  <input id='yearend_submit' type='image' src='/images/yearend_apply.png'>
               </div>

               <? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
               <input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>'/>
            </form>
         </div>
      </div>
   </div>
   &nbsp;
</div>


<?
include '../module/_tail.php';
?>
