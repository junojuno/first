<?
$_required = false;
$_loginrequired = true;
include '../config.php';
include '../module/class.upload.php';
include '../module/_head.php';

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token] || !isset($_POST['isReceipt'])) {
   alert('잘못된 접근입니다.');
   exit;
}
$_SESSION[token] = '';

$isReceipt  = $_POST["isReceipt"];
$realname   = $_POST["realname"];
$receiptPno = ($isReceipt != 0) ? ($isReceipt == 1) ? $_POST["serial1"].'-'.$_POST["serial2"] : $_POST["permit1"].'-'.$_POST["permit2"].'-'.$_POST["permit3"] : false;
$serialno = $_POST["serial1"].$_POST["serial2"];
$permitno = $_POST["permit1"].$_POST["permit2"].$_POST["permit3"];

$sql = "UPDATE ".DB_ORDERS." a SET 
   a.isReceipt = '{$isReceipt}',
   a.receiptName = '{$realname}',
   a.receiptPno  = '{$receiptPno}' 
   WHERE a.mb_no = {$_SESSION[user_no]} AND 
      a.isReceipt = 0 AND 
      a.it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." c WHERE c.it_startdate >=20130104 and c.it_startdate <=20131108)
   ";
sql_query($sql);

sql_query("UPDATE ".DB_MEMBERS."
         SET mb_serial = '{$serialno}', mb_permit = '{$permitno}' 
         WHERE mb_no = '$_SESSION[user_no]'
         ");

if ($_FILES['permitimage']) {
   $handle = new upload($_FILES['permitimage']);
   if ($handle->uploaded) {
      $handle->allowed           = array('image/*');
      $handle->image_convert        = 'jpg';
      $handle->file_new_name_body      = 'permit_'.$_SESSION[user_no];
//    $handle->image_resize         = true;
//    $handle->image_ratio_crop     = true;
//    $handle->image_x           = 100;
//    $handle->image_y           = 100;
      $handle->process('../data/userupload/');

      if ($handle->processed) {
         $handle->clean();
//       mysql_query("UPDATE ".DB_MEMBERS." SET mb_icon = 'profile_$check[no].jpg' WHERE mb_no = '$check[no]' ");
      } else {
         echo "<script>alert('사업자등록이미지 업로드가 실패하였습니다. seoyoung@wegen.kr 로 문의해주세요.');</script>";
      }
   }
}

// ACTIVITY LOG
if ($_SESSION[user_no]) {
   mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'yearend_result', referer = '$_SERVER[HTTP_REFERER]' ");
}



?>


<div id='highlight'>&nbsp;
   <div id='content' class='solid'>
      <div class='inner yearend'>
         <h2 style='margin-bottom: 14px; border: none'>기부금 영수증 신청</h2>
         <div style='width:100%;text-align:center;margin-top:30px;'>
            <img src='/images/yearend_finish.png'>
         </div>
         <p style='margin:20px;font:11pt NanumGothic;'>5초후 홈으로 이동합니다..<p>
      </div>
   </div>
   &nbsp;
</div>

<script>
function moveHome() {
   location.href='http://wegen.kr/';
}
$(document).ready(function() {
   setTimeout('moveHome()',5000);
})
</script>
<?
include '../module/_tail.php';
?>


