<?
$_required = false;
$_loginrequired = true;
include '../config.php';
include '../module/_head.php';

//sk 계정 체크
$info = sql_fetch("SELECT mb_type FROM ".DB_MEMBERS." WHERE mb_no = {$_SESSION['user_no']}");
if($info['mb_type'] == 10) {
   alert('해당 계정은 영수증을 신청하실 수 없습니다.');
   exit;
}

$all_sum = sql_fetch("SELECT SUM(od_amount) AS amount FROM ".DB_ORDERS." a WHERE a.mb_no = {$_SESSION['user_no']} AND 
      a.it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." b WHERE b.it_isReceipt = 1 and b.it_startdate >=20130104 and it_startdate <=20131108)");
$apply_sum = sql_fetch("SELECT SUM(od_amount) AS amount FROM ".DB_ORDERS." a WHERE a.isReceipt = 0 AND a.mb_no = {$_SESSION['user_no']} AND 
      a.it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." b WHERE b.it_isReceipt = 1 and b.it_startdate >=20130104 and it_startdate <=20131108)");

$all_sum = $all_sum['amount'] ? $all_sum['amount'] : 0;
$apply_sum = $apply_sum['amount'] ? $apply_sum['amount'] : 0;

$sql = "SELECT a.od_amount, a.isReceipt,a.od_time, c.it_name, c.category, c.it_isReceipt FROM ".DB_ORDERS." a, ".DB_CAMPAIGNS." c WHERE 
   a.mb_no = {$_SESSION[user_no]} AND a.it_id = c.it_id AND
   a.it_id IN (SELECT it_id FROM ".DB_CAMPAIGNS." b WHERE b.it_startdate >=20130104 and b.it_startdate <=20131108)";
$res = sql_query($sql);

// ACTIVITY LOG
if ($_SESSION[user_no]) {
   mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'yearend', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<div id='highlight'>&nbsp;
   <div id='content' class='solid'>
      <div class='inner yearend'>
         <h2 style='margin-bottom: 14px; border: none'>기부금 영수증 발급</h2>
         <div class='grayborder'>
            <h3>2013년 나의 기부금 내역</h3>
            <table style='border:1px solid #c0c0c0;font:11pt NanumGothicBold;text-align:center;'>
               <tr>
                  <td style='border:0;'>총 기부액 : <span style='color:red;'><?=number_format($all_sum)?></span> 원</td>
                  <td style='border:0;'>기부금 영수증 신청 가능액 : <span style='color:red;'><?=number_format($apply_sum)?></span> 원</td>
               </tr>
            </table>
            <ul style='padding:10px;font-size:9pt;list-style:initial;line-height:19px;margin-left:18px;margin-bottom:12px;'>
               <li>연말정산용 기부금 영수증 재신청 기간: 2014년 1월 7일 ~ 2014년 1월 9일</li>
               <li>기부금 영수증은 각 자선단체에서 발급하며, 단체에 따라 재신청이 가능한 경우와 불가능한 경우가 있습니다.</li>
               <li>단체에 따라 이메일이나 우편으로 발급되며, 일주일 정도의 기간이 소요될 수 있습니다.</li>
               <li>기타 문의: 박서영 seoyoung@wegen.kr 로 부탁드립니다.</li>
            </ul>
            <table style='text-align:center;'>
               <tr>
                  <td class='top_row'>카테고리</td>
                  <td class='top_row'>캠페인명</td>
                  <td class='top_row'>결제금액</td>
                  <td class='top_row'>기부날짜</td>
                  <td class='top_row'>영수증 신청여부</td>
               </tr>
               <?php

               while($row = sql_fetch_array($res)) {
                  if($row['it_isReceipt'] == 0 || $row['type'] == 2) $receipt = '-';
                  else if($row['isReceipt'] == 0) $receipt = '미신청';
                  else $receipt = '신청';
                  $date = explode(' ', $row['od_time']);
                  ?>
                  <tr>
                     <td>[<?=$row['category']?>]</td>
                     <td><?=$row['it_name']?></td>
                     <td style='text-align:right;'><?=number_format($row['od_amount'])?> 원</td>
                     <td><?=$date[0]?></td>
                     <td><?=$receipt?></td>
                  </tr>
                  <?
               }
               ?>
            </table>
            <div style='width:100%;text-align:center;margin-top:37px;'>
               <a href='./apply'><img src='/images/yearend_apply.png'></a>
            </div>
         </div>
      </div>
   </div>
   &nbsp;
</div>

<?
include '../module/_tail.php';
?>
