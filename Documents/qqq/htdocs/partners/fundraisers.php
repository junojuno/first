<?
$_required = true;
include '../config.php';
include '../module/_head.php';
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<li>펀드레이저</li>
		<a href='./'><li>파트너</li></a>
		<a href='./sponsors'><li>스폰서</li></a>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px'>위제너레이션 펀드레이저</h2>
	<div style='position: relative; top: -24px; text-align: right; color: black; font: 12px NanumGothicBold'>펀드레이저 참여 문의 : sehyun@wegen.kr &nbsp;</div>
	<p style='font-size: 12px; color: #A2A2A2; text-shadow: 0px 1px #E3E3E3; margin-bottom: 30px'>캠페인 후원자에 다양한 이벤트로 혜택을 돌려드리는 응원스타, 응원멘토입니다.</p>

	<ul style='margin: 0px 10px'>
		<?
		$sql = "SELECT * FROM wegen_fundraiser f
				LEFT JOIN ".DB_CAMPAIGNS." c ON (f.it_id = c.it_id)
				ORDER BY no DESC";
		$result = sql_query($sql);
		for ($i=0; $row=sql_fetch_array($result); $i++) {
//			if ($i == 18) break;
		?>
		<a href='/campaign/<?=$row[it_id]?>'>
		<li title="<?=$row[it_name]?>" style='float: left; width: 120px; height: 200px; text-align: center; margin: 10px'>
		<img src='/data/partners/<?=$row[fr_logo]?>' style='width: 120px; height: 144px' />
		<p style='margin-top: 10px; font-family: NanumGothicBold'><?=$row[fr_name]?></p>
		</li>
		</a>
		<? } ?>
	</ul>
	<div style='clear: both'></div>
</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li:eq(0)').addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>