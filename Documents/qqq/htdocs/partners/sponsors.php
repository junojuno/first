<?
$_required = true;
include '../config.php';
include '../module/_head.php';
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<a href='./fundraisers'><li>펀드레이저</li></a>
		<a href='./'><li>파트너</li></a>
		<li>스폰서</li>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px'>위제너레이션 스폰서</h2>
	<div style='position: relative; top: -24px; text-align: right; color: black; font: 12px NanumGothicBold'>기업스폰서 참여 문의 : wegen@wegen.kr &nbsp;</div>
	<p style='font-size: 12px; color: #A2A2A2; text-shadow: 0px 1px #E3E3E3; margin-bottom: 30px'>캠페인의 일반회원 후원 금액과 동일한 금액을 매칭 기부하거나, 캠페인 기간 중 특정 상품의 판매 수익을 기부한 기업 회원입니다.</p>

	<? include '../module/list.sponsors.php'; ?>
	</ul>
</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li:eq(2)').addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>