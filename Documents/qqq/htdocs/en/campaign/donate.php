<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;

$_loginrequired = true;
$_required = true;
include '../../config.php';

include '../../module/_head.en.php';
$row = sql_fetch("SELECT * FROM ".DB_CAMPAIGNS_EN." WHERE it_id='$_GET[it_id]' AND type !=2");

if(!$row['it_id']) { alert("Wrong campaign id"); }
if ($row['it_isEnd'] == '1') { alert("This campaign is end"); }

$it_opt1 = explode('|', $row['it_currency']);
$totalopt = count($it_opt1)-1;

// ACTIVITY LOG
if ($_SESSION['user_no']) {
   mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'donate', param2 = '$row[it_id]', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<script type='text/javascript'>

$(document).ready(function() {
   $('input[type=text]').addClass('text');
   $('#donationForm .submit').click(function() {
      var p = checkRadioValue(document.donationForm.od_price);
      var custom = $('input[name=connect_type]').val() + '|' + $('input[name=ordr_idxx]').val();
      if (!p) {
         qAlert('Please Select amount to donate.');
         return false;
      }
      if (p == 'user_input') {
         p = $.trim($('input[name=user_input]').val());
         if (!p || parseInt(p) == 0) {
            qAlert('Please Input amount to donate.');
            return false;
         }
         else if (p*1 < $('#donationPrice #p0').val()*1 ) {
            qAlert('Amount should be equal to or over '+$('label[for=p0]').html()+' dollars');
            return false;
         }
      }
      $('input[name=amount]').val(p);

      if($('input[name=reward_req') != null && $('input[name=reward_req').val() != '') {
         custom += '|'+ $('input[name=reward_req').val();
      }
      $('input[name=custom]').val(custom);

      if (!$.trim($('input[name=name]').val())) {
         qAlert('Please Input your name.');
         $('input[name=name]').focus();
         return false;
      }

      if (!$.trim($('input[name=night_phone_b]').val())) {
         qAlert('Please Input your telephone number.');
         $('input[name=night_phone_b]').focus();
         return false;
      }

      if (!$.trim($('input[name=email]').val())) {
         qAlert('Please Input your email.');
         $('input[name=email]').focus();
         return false;
      }

      if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
         qAlert('Wrong your email format');
         $('input[name=email]').val('').focus();
         return false;
      }

      if(!$.trim($('input[name=address1]').val())) {
         qAlert('Please Input your address.');
         $('input[name=address1]').val('').focus();
         return false;
      }

      $.ajax({
         type : "POST",
         url : "/en/campaign/ajax.update.php",
         data : {
            name : $('input[name=name]').val(),
            email : $('input[name=email]').val(),
            contact : $('input[name=night_phone_b').val(),
            address1 : $('input[name=address1').val(),
            address2 : $('input[name=address2').val(),
            zip : $('input[name=zip]').val()
         },
         cache : false,
         success : function(n) {
            if(n=='success') {
               $("#donationForm").submit();
            } else {
               alert('Please Retry again.');
            }
         }
      });
   });
});
</script>


<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner donation'>

   <h2 style='margin-bottom: 14px; border: none'>Donation Page</h2>
   <!--  https://www.sandbox.paypal.com/cgi-bin/webscr-->
   <form id="donationForm" name='donationForm' action="https://www.paypal.com/cgi-bin/webscr" method="post">

   <!-- left -->
   <div id='donationLeft' style='float: left; width: 48%; margin-right: 2%'>

      <h3 style='padding-bottom:5px;'>Donation Amount</h3>
      
      <ul id='donationPrice'>
         <?
         for ($i = 0; $i < count($it_opt1); $i++) {
            $it_opt = explode(';', $it_opt1[$i]);
            $it_opt = $it_opt[0];
            if ($it_opt != 'user_input') {
         ?>
         <li><input type='radio' name='od_price' value='<?=$it_opt?>' id='p<?=$i?>' />
         <label for='p<?=$i?>'>$<?=number_format($it_opt)?></label></li>
         <? }} ?>
         <li><input type='radio' name='od_price' value='user_input' />
         Custom Amount
         <input type='text' name='user_input' onfocus="$('input[name=od_price]:last').prop('checked',true)" style='text-align: right' class='numonly' /> ($)</li>
      </ul>
      
      
      <div style='clear: both'></div>

   <?
   $rewards = explode('||', $row[it_reward]);
// if (count($rewards) > 1) :
   if ($row[it_reward_fromer]) :
   ?>
   <h3 style='margin-top: 20px'>Reward Selection</h3>

      <table cellpadding='0' cellpadding='0' style='width: 100%'>
         <colgroup>
            <col width='120' />
            <col width='/' />
         </colgroup>
         <tr><th>Request</th><td>
         <input type='text' name='reward_req' style='width: 250px' /></td></tr>
      </table>

      <p><?=nl2br($row[it_reward_fromer])?></p>
      
      <?
      // Added by 2013.08.07
      $additional_reward_image = "/data/campaign/{$_GET['it_id']}/reward.jpg";
      if(file_exists("..{$additional_reward_image}")) {
         echo "<img src='{$additional_reward_image}' style='width:auto; height:auto;'>";
      }
      ?>
   <div style='clear: both'></div>
   <? endif; ?>
   </div>
   <!-- /left -->

   <!-- right -->
   <div style='float: left; width: 48%; margin-left: 2%'>
      <h3>Personal Information</h3>
      <?
      $info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
      ?>
      <table cellpadding='0' cellpadding='0' style='width: 100%'>
         <colgroup>
            <col width='120' />
            <col width='/' />
         </colgroup>
         <tr><th>Name</th><td>
         <input type='text' name='name' value="<?=$_SESSION[username]?>" /></td></tr>
         <tr><th>Telephone Number</th><td>
         <input type='text' name="night_phone_b" value="<?=$info[mb_contact]?>" class='numonly' style='width:250px;'/></td></tr>
         <tr>
            <th>E-mail</th>
            <td>
               <input type='text' name='email' value='<?=$info[mb_email]?>' maxlength='50' style='width: 250px' />
            </td>
         </tr>
         <tr style='height: 120px'>
            <th>Address</th>
            <td>
               <div id='addr'>
                  <span style='font-size:9pt;'>Postal Code</span> <input type='text' name="zip" value="<?=$info[mb_zip1]?>" style='width:181px;'/>
                  <input type='text' name="address1" value="<?=$info[mb_addr1]?>" style='width: 250px' /><br/>
                  <input type='text' name="address2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
               </div>
            </td>
         </tr>
      </table>
      <div style='text-align: center;  margin: 0px auto'>
         <input type='button' value='Donate' class='submit' onclick="" />
      </div>
   </div>
   <!-- /right -->
   <div style='clear: both'></div>
   <input type="hidden" name="charset" value="utf-8">
   <input type="hidden" name="cmd" value="_xclick">
   <input type="hidden" name="business" value="kidaehong@wegen.kr">
   <input type="hidden" name="ordr_idxx" value='<?=date('ymdHis').rand(100, 999)?>' />
   <input type='hidden' name='custom' value=''/>
   <input type='hidden' name='item_number' value='<?=$row[it_id]?>'/>
   <input type='hidden' name='item_name' value='<?=$row[it_name]?>'/>
   <input type="hidden" name="amount" value="">
   <input type="hidden" name="quantity" value="1">
   <input type="hidden" name="no_note" value="1">
   <input type="hidden" name="currency_code" value="USD">
   <input type="hidden" name="address_override" value="1">
   <input type="hidden" name="no_shipping" value="1">
   <input type="hidden" name="return" value="http://<?=$_SERVER[HTTP_HOST]?>/en/campaign/result">
   <input type="hidden" name="rm" value="2">
   <input type='hidden' name='connect_type'  value="<? if($isMobile) print 'MOBILE'; else print 'PC';?>"/>
   </form>
   </div>
</div>&nbsp;
</div>

<?
include '../../module/_tail.php';
?>