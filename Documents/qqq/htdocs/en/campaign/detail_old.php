<?
if (!preg_match("/^[0-9]{10}$/", $_GET[it_id])) exit;
$it_id = $_GET[it_id];

$_required = true;
include '../../config.php';

$sql = "SELECT *,
         (SELECT SUM(od_amount) 
         FROM ".DB_ORDERS." o 
         WHERE o.it_id = i.it_id)
      AS it_funded
      FROM ".DB_CAMPAIGNS_EN." i
      LEFT JOIN ".DB_PARTNERS_EN." p ON (i.it_partner = p.pt_no)
      WHERE i.it_id = '$it_id' AND i.it_isPublic = 1";
$res = mysql_query($sql);
if(mysql_num_rows($res) == 1) {
   $it = sql_fetch($sql);   
} else {
   alert('Abnormal campaign id');
   exit;
}

$settings[type] = 'wegenapp:campaign';
$settings[title] = $it[it_name];
$settings[desc] = $it[it_shortdesc];
$settings[image] = 'http://wegen.kr/data/en/campaign/'.$it[it_id].'/list.jpg';
include '../../module/_head.php';

if (!$it[it_id]) alert("Incorrect Access!");

$partner = $it[pt_name] ? "<img src='/data/partners/$it[pt_logo]' style='width: 100%;' />" : 'WEGENERATION';
$partner = $it[pt_homepage] ? "<a href='$it[pt_homepage]' target='_blank' style='border: none; color: none'>$partner</a>" : $partner;

?>

<div id='highlight' class='campaign'>&nbsp;
   <div style='width: 930px; margin: 30px auto 0px'>
   <h2>
      <?=$it[it_name]?> 
   </h2>
   </div>

   <div id='content' class='solid'>
      <div class='inner'>
         <div style='clear: both'></div>
         <div style='float: left; margin-top: 30px; width: 600px'>
            <? if ($it[it_youtube]) { ?>
               <div style='background: black; min-height: 400px'><div id='movieFrame'></div></div>
               <script type='text/javascript'>
               $.getScript('https://www.youtube.com/iframe_api');
               function onYouTubeIframeAPIReady() {
                  var p = new YT.Player('movieFrame',{
                     videoId:'<?=$it[it_youtube]?>',
                     width:'600',
                     height:'400',
                     //origin:'http://wegen.kr',
                     playerVars:{showinfo: 0,wmode:'transparent'}
                  });
               }
               </script>
            <? } else { ?>
               <img src='/data/campaign/<?=$it[it_id]?>/inner.jpg' style='width: 600px; height: 400px' />
            <? } ?>

            <div style='margin-top: 20px; background-color: #F4F4E3; border: solid 2px #AFAFAF;'>
               <div style='padding: 10px'>
                  <h3 style='margin: 0px; border: none'>
                     Amount Donated
                  </h3>
                  <? drawGauge($it[it_funded],$it[it_target],true, false, 3); ?>
               </div>
            </div>

            <div id='campaignDetail' style='width: 600px; margin-top: 30px'>
               <div id='it_desc'>
               <? if(!defined("SKIPPING_DETAIL_IMAGE")) {  ?>
                  <img src='/data/en/campaign/<?=$it[it_id]?>/detail.jpg' style='width: 100%' />
               <? } ?>
               </div>
               <div id='it_postscript'>
               <?=$it[it_postscript]?>
               </div>

               <? if($it_id === '1372929550') { ?>
               <div style="margin-top:21px;">
                  <img src='/images/en/campaign/notice_donate_en.png' style='width:100%'/>
               </div>
               <? } ?>
               
            </div>
            
            <div id='comment' style='width: 600px; margin-top: 30px'>
               <h3>Comments</h3>
               <? $action = ($_SESSION[user_no]) ? 'btn_submitcmt' : 'btn_login'; ?>
               <div id='commentbox' style='margin-bottom: 20px'>
                  <form name='commentForm' style='margin:0px;font-size:8pt;'>
                     <input type='hidden' name='it_id' value='<?=$it_id?>' />
                     <input type='hidden' name='is_en' value='1'/>
                     <div style='float: left; width: 70px; height: 80px'><?=drawPortrait($_SESSION[user_no])?></div>
                     <div style='float: right; width: 70px'><img class='<?=$action?> clickable' src='/images/en/campaign/btn_submitcmt.png' /></div>
                     <textarea name='cmt' class='text' style='margin: 0px; width: 445px; height: 64px'<? print ($_SESSION[user_no]) ? false : ' readonly'; ?>></textarea>
                     <input type='radio' id='c1' name='category' value='1' checked /><label for='c1'>Victim Comment</label>
                     <input type='radio' id='c3' name='category' value='3' /><label for='c3'>Sponsor Comment</label>
                     <input type='radio' id='c2' name='category' value='2' class='starcmt' /><label for='c2' class='starcmt'>Star/Mentor Comment</label>
                     <input type='radio' id='c4' name='category' value='0' /><label for='c4'>Wegen Comment</label>
                     <div style='clear: both'></div>
                  </form>
               </div>
               <? $action = ($_SESSION[user_no]) ? 'btn_submitreply' : 'btn_login'; ?>
               <div id='replybox'>
                  <form name='replyForm' style='margin:0px'>
                     <input type='hidden' name='cmt_id' />
                     <div style='float: left; width: 70px; height: 80px'><?=drawPortrait($_SESSION[user_no])?></div>
                     <div style='float: right; width: 70px'><img class='<?=$action?> clickable' src='/images/campaign/btn_submitcmt.png' /></div>
                     <textarea name='r_cmt' class='text' style='margin: 0px; width: 445px; height: 64px'></textarea>
                     <div style='clear: both'></div>
                  </form>
               </div>

               <div id='commentList'>
                  <?
                  $sql = "SELECT *, IF(cmt_depth = '0', cmt_id, parent_id) AS orderNum
                        FROM ".DB_CAMPAIGN_CMTS." p
                        LEFT JOIN ".DB_MEMBERS." m ON (p.mb_no = m.mb_no) 
                        WHERE it_id = '$it_id'
                        ORDER BY orderNum DESC, cmt_depth ASC, cmt_time DESC";
                  $result = sql_query($sql);
                  $commentTotal = mysql_num_rows($result);

                  for ($i=0; $row=sql_fetch_array($result); $i++) {
                     $replyClass = ($row[cmt_depth] == 0) ? 'commentCell' : 'replyCell';
                     $replyWidth = ($row[cmt_depth] == 0) ? '530' : '480';
                  ?>
                  <div class='<?=$replyClass?>'>
                     <ul>
                     <li style='float: left; width: 70px'><?=drawPortrait($row[mb_no])?></li>
                     <li style='float: left; width: <?=$replyWidth?>px'>
                        <? if ($row[mb_no] == $_SESSION[user_no]) { ?><span class='btn_delete' item='<?=$row[it_id]?>' cmt='<?=$row[cmt_id]?>'>Delete</span><? } ?>
                        <? if (array_search($_SESSION[user_no], $settings[admin]) !== false && $row[cmt_depth] == '0') { ?><span class='btn_reply' item='<?=$row[it_id]?>' cmt='<?=$row[cmt_id]?>'>Reply</span><? } ?>
                        <p><strong><?=$row[mb_name]?></strong> <span style='color: gray; font: 9px Arial'><?=$row[cmt_time]?></span></p>
                        <p style='margin-top: 10px'>
                        <?
                        switch ($row[cmt_category]) {
                           case 1 :
                              print "[To Victim]"; break;
                           case 2 :
                              print "[To Star/Mentor]"; break;
                           case 3 :
                              print "[To Sponsor]"; break;
                           case 0 :
                              print ""; break;
                        }
                        ?>
                        <?=nl2br($row[cmt])?></p>
                     </li>
                     </ul>
                     <div style='clear: left'></div>
                  </div>
                  <? } ?>
                  <div style='border-top: solid 1px gray; clear: left; height: 50px'></div>
                  <!-- 코멘트 리스트 -->
               </div>
            </div>
         </div> <!-- left -->

         <!-- right -->
         <div id='campaignInfo'>

            <div class='campaignDetail'>

               <div style='height: 50px; position: relative'>
                  <img src='/images/campaign/btn_share.png' class='btn_facebook clickable' style='z-index: 10; position: absolute; top: 3px; right: 0px' />
                  <div class='fb-like' style='position: relative; top: 5px' data-href='http://wegen.kr/en/campaign/?<?=$it_id?>' data-send='false' data-layout='button_count' data-width='100' data-show-faces='false'></div>
                  <span style='width: 100px; position: relative; top: 5px'><a href="https://twitter.com/share" class="twitter-share-button" count='horizontal' data-url='http://wegen.kr/en/campaign/<?=$it_id?>' data-lang="en">Tweet</a></span>
               </div>

               <table style='border: solid 1px #dadada; width: 100%; height: 100px; text-align: center'>
                  <tr>
                     <td><?=$partner?></td>
                  </tr>
               </table>

               <?
               if ($it[it_isEnd] == 0) {
                  //coin event check
                  //$checker = sql_fetch("SELECT no FROM ".DB_FUNDRAISERS_EN." WHERE it_id = '$it[it_id]' ORDER BY no ASC LIMIT 1");
                  //$checker = ($checker[no] > 0) ? $checker[no] : '0';

                  if ($_SESSION[user_no]) {
               ?>
               <!-- <a href='/en/campaign/<?=$it[it_id]?>/donate'><img src='/images/en/campaign/btn_donate.png' style='width: 100%; margin: 20px 0px 10px' /></a> -->
               <!--<img src='/images/campaign/btn_coin.png' style='width: 100%; margin-bottom: 10px; cursor: pointer; <?=$it[it_id] == '1371778995' ? "display:none;" : false;?>' class='donate_coin' itemid='<?=$it[it_id]?>' itemname="<?=$it[it_name]?>" isEvent='<?=$checker?>' />-->
               <?
                  } else {
               ?>
               <!-- <img src='/images/en/campaign/btn_donate.png' class='btn_login' style='width: 100%; margin: 20px 0px 10px' url='/en/campaign/<?=$it_id?>/donate' /> -->
               <!--<img src='/images/campaign/btn_coin.png' style='width: 100%; margin-bottom: 10px; cursor: pointer; <?=$it[it_id] == '1371778995' ? "display:none;" : false;?>' class='donate_coin' itemid='<?=$it[it_id]?>' itemname="<?=$it[it_name]?>" isEvent='<?=$checker?>' />-->
               <?
                  }
               }
               ?>
            </div>

            <?
            $sql = "SELECT * FROM ".DB_FUNDRAISERS_EN."
                  WHERE it_id = '$it_id'
                  ";
            $result = sql_query($sql);
            $total = mysql_num_rows($result);
            if ($total > 0) {
               $starcmt = true;
            ?>
               <div class='campaignDetail' style='border: none'>
                  <div style='padding: 10px'>
                     <h3>Star/Mentor Event</h3>
                     <ul>
                        <?
                        for ($i = 0; $row = sql_fetch_array($result); $i++) {
                           $row[fr_desc] = nl2br($row[fr_desc]);
                           print "<li><img class='portrait' src='/data/en/partners/$row[fr_logo]' title='$row[fr_name]' style='margin-bottom:6px;'>$row[fr_desc]</li>";
                        }
                        ?>
                     </ul>
                  </div>
               </div>
            <? } ?>

            <div class='campaignDetail'><div style='padding: 10px'>
               <h3>Reward</h3>
               <ul>
               <? if ($it[it_reward]) { ?>
                  <li><? print nl2br($it[it_reward]); ?></li>
               <? } else { ?>
                  <li><? print 'No Reward.'; ?></li>
               <? } ?>
               </ul>
            </div></div>

            <div class='campaignDetail' style='border: none'>
               <div style='padding: 10px'>
                  <h3>Members that donated to this campaign</h3>
                  <ul id='donators'>
                  <?
                  $sql = "SELECT mb_no
                        FROM ".DB_ORDERS."
                        WHERE it_id = '$it[it_id]'
                        ORDER BY rand()";
                  $result = sql_query($sql);
                  $total = mysql_num_rows($result);
                  $limit = 0;
                  for($i= 0; $row = sql_fetch_array($result); $i++) {
                     if ($limit == 15) { break; }
                     print '<li>' and drawPortrait($row[mb_no], true) and print '</li>';
                     $limit++;
                  }
                  ?>
                  </ul>
                  <div style='clear: both'></div>
                  <div style='text-align: right;font-weight:bold;'><span style='color: #E30000;'><?=$total?></span> people donated</div>
               </div>
            </div>

            <div class='campaignDetail'><div style='padding: 10px'>
               <h3>Members that shared this campaign</h3>
               <ul id='sharers'>
               <?
               $sql = "SELECT mb_no
                     FROM ".DB_BADGES."
                     WHERE it_id = '$it[it_id]' AND category = 3
                     GROUP BY mb_no
                     ORDER BY rand()";
               $result = sql_query($sql);
               $total = mysql_num_rows($result);

               $limit = 0;
               for($i= 0; $row = sql_fetch_array($result); $i++) {
                  if ($limit == 15) { break; }
                  print '<li>' and drawPortrait($row[mb_no], true) and print '</li>';
                  $limit++;
               }
               ?>       
               </ul>
               <div style='clear: both'></div>
               <div style='text-align: right;font-weight:bold;'><span style='color: #E30000;'><?=$total?></span> people shared</div>
            </div></div>

         </div>
         <!-- /right -->
         <div style='clear: both'></div>


      </div>
   </div>
   &nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {   
   $('#content menu li').eq(<?=$eq?>).addClass('on');
   $('#share_comment').html('<?=$commentTotal?>');
<? if (!$starcmt) { ?>
   $('.starcmt').hide();
<? } ?>
});

twttr.ready(function (twttr) {
   twttr.events.bind('tweet', function(event) {
      procShare('<?=$it_id?>', 'twitter', '');
   });
});

function postToFeed() {
   var list = 'list.jpg';
   var obj = {
      method: 'feed',
      link: 'http://wegen.kr/en/campaign/<?=$it_id?>',
      picture: 'http://wegen.kr/data/en/campaign/<?=$it_id?>/'+list,
      name: '<?=$it[it_name]?>',
      caption: 'Celebrity Charity Crowdfunding, WEGENERATION',
      description: '<?=addslashes($it[it_shortdesc])?>'
   };

   function callback(response) {
      if (response) {
         procShare('<?=$it_id?>', 'facebook', response['post_id']);
      }
   }
   FB.ui(obj, callback);
}

function postToTweet() {
   window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent('http://wegen.kr/en/campaign/<?=$it_id?>') + '&text=' + encodeURIComponent('<?=$it[it_name]?>'),"twitterPop", 'width=600 height=350');
}
</script>

<?
include '../../module/_tail.php';
?>
