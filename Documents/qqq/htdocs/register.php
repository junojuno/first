<?
$_required = true;
include 'config.php';
include 'module/_head.php';
?>

<script type='text/javascript' src='/js/encrypt.js'></script>
<script type='text/javascript' src='/js/zipsearch.js'></script>
<!--
코드 변경 - <script type="text/javascript" charset="utf-8">
2013. 08.10.
진입명
-->
<script type="text/javascript" charset="utf-8">
// <script>
$(document).ready(function() {
	$('#registration').hide();
	$('input[type=text], input[type=password]').addClass('text');

	var str = '이메일 주소가 회원님의 아이디로 사용됩니다';
	$('input[name=email]').val(str).addClass('label').focusin(function() {
		if (!$(this).val() || $(this).val() == str) {
			$(this).val('').removeClass('label');
		}
	}).focusout(function() {
		if (!$(this).val()) {
			$(this).val(str).addClass('label');
		}
	});

	$('#register').click(function() {
		if ($('#agree').prop('checked') == false) {
			qAlert('이용약관과 개인정보 취급방침에 동의하셔야 회원 가입이 가능합니다.');
			return false;
		}

		$('#agreement').slideUp();
		$('#registration').show();
	});
	
	// 이메일 중복
	// 2013. 08. 10.
	// 진입명
	$('#checkDuplicatedId').click(function() {
		if ($.trim($('input[name=email]').val()) && $('input[name=email]').val() != str) {
			var id = $.trim($('input[name=email]').val());
			
			if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(id)) {
				qAlert('이메일 주소가 올바르지 않습니다.');
				$('input[name=email]').val('').focus();
				return false;
			}
			
			$.ajax({
				type: "POST",
				url: "checkDuplicatedId.php",
				data: { id: id }
			}).success(function(message, status) {
				if (status === 'success') {
					if(message === $.trim('quit')) {
						qAlert('탈퇴된 아이디므로 재가입이 불가능합니다.<br/>1:1 문의로 연락해주시기바랍니다.');
						$('#duplicated').val('failure');
					} else if (message === $.trim('exist')) {
						qAlert('이미 회원가입이 완료된 이메일 주소 입니다.');
					} else {
						alert('사용 가능한 이메일 주소 입니다.');
						$('#checkDuplicatedId').data('check',1);
					}
				} else {
					qAlert('알수 없는 에러!');
				}
			});
		}
	});

	$('#registerForm').submit(function() {

		if (!$.trim($('input[name=username]').val())) {
			qAlert('이름을 입력해 주세요.');
			$('input[name=username]').focus();
			return false;
		}
				
		if (!$.trim($('input[name=email]').val()) || $('input[name=email]').val() == str) {
			qAlert('사용하실 이메일 주소를 입력해 주세요.');
			$('input[name=email]').focus();
			return false;
		}
		
		if($('#checkDuplicatedId').data('check') == undefined) {
			qAlert('이메일 중복체크를 해주세요.');
			$('#checkDuplicatedId').focus();
			return false;
		}

		if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=email]').val()))) {
			qAlert('이메일 주소가 올바르지 않습니다.');
			$('input[name=email]').val('').focus();
			return false;
		}

		if (!$.trim($('input[name=email_confirm]').val())) {
			qAlert('이메일 확인란을 입력해 주세요.');
			$('input[name=email_confirm]').focus();
			return false;
		}

		if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=email_confirm]').val()))) {
			qAlert('이메일 확인란의 주소가 올바르지 않습니다.');
			$('input[name=email_confirm]').val('').focus();
			return false;
		}

		if ($.trim($('input[name=email]').val()) != $.trim($('input[name=email_confirm]').val())) {
			qAlert('이메일과 이메일 확인이 서로 일치하지 않습니다.');
			$('input[name=email]').val('').focus();
			$('input[name=email_confirm]').val('');
			return false;
		}

      var pw = $.trim($('input[name=passwd]').val());
      var pw_confirm = $.trim($('input[name=passwd_confirm]').val());

		if (!pw) {
			qAlert('사용하실 비밀번호를 입력해 주세요.');
			$('input[name=passwd]').focus();
			return false;
		}

		if (!pw_confirm) {
			qAlert('비밀번호 확인란을 입력해 주세요.');
			$('input[name=passwd_confirm]').focus();
			return false;
		}

      if(pw.length < 8) {
         qAlert('비밀번호는 8자리 이상이어야 합니다.');
         $('input[name=passwd]').focus();
         return false;
      }
		if ( pw != pw_confirm ) {
			qAlert('비밀번호와 비밀번호 확인이 서로 일치하지 않습니다.');
			$('input[name=passwd]').focus();
			return false;
		}

      if($('input[name=mb_hp1]').val() && $('input[name=mb_hp2]').val() && $('input[name=mb_hp3]').val()) {
         $('input[name=mb_contact]').val($('input[name=mb_hp1]').val()+'-'+$('input[name=mb_hp2]').val()+'-'+$('input[name=mb_hp3]').val());
         if(!phone_check($('input[name=mb_contact]').val())) {
            qAlert("잘못된 연락처 입니다. 한번 더 확인해주세요");
            $('input[name=mb_hp1]').focus();
            return false;
         }
      }

		if ($('input[name=profilepicture]').val()) {
			var ext = $('input[name=profilepicture]').val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				qAlert('올바른 이미지 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
				return false;
			}
		}
		
		$('#registerForm').append(
			$(document.createElement("input"))
			.attr('name','p')
			.attr('type','hidden')
			.val(hex_sha512(pw))
		);
		$('input[name=passwd], input[name=passwd_confirm]').val('');
	});
});

</script>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
	<div class='inner'>

	<h2 style='margin-bottom: 14px; border: none'>회원가입</h2>

	<div id='agreement'>
		<h3 style='border-bottom: solid 2px black; margin: 0px 0px 10px'>이용약관</h3>
		<textarea style='width: 100%; font: 10pt NanumGothic' rows='10' readonly><? include 'module/provision.txt'; ?></textarea>

		<h3 style='border-bottom: solid 2px black; margin: 20px 0px 10px'>개인정보 취급방침</h3>
		<textarea style='width: 100%; font: 10pt NanumGothic' rows='10' readonly><? include 'module/privacy.txt'; ?></textarea>

		<p style='text-align: center; margin: 20px 0px'>
		<input type='checkbox' id='agree' />
		<label for='agree'>위의 이용약관과 개인정보 취급방침을 읽었으며, 이에 동의합니다.</label>
		</p>

		<p style='text-align: center'><input type='button' id='register' value='회원가입' /></p>
	</div>

	<div id='registration'>
		<form id='registerForm' enctype='multipart/form-data' method='post' action='/doRegister.php'>
		<? $_SESSION[token] = dechex(crc32(session_id().'thisisRegSALT')); ?>
		<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisRegSALT'))?>' />
		<input type='hidden' name='url' value='<?=$_GET['url']?>'/>
		<input type="hidden" id="duplicated" name="duplicated" value="" />

		<h3 style='border-bottom: solid 2px black; margin: 0px 0px 10px'>필수정보 입력</h3>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>이름</th><td>
			<input type='text' name='username' /></td></tr>
			<tr><th>이메일</th><td>
			<input type='text' name='email' maxlength='50' style='width: 250px' />
			<input type="button" id="checkDuplicatedId" name="checkDuplicatedId" value="중복체크" /></td></tr>
			<tr><th>이메일 확인</th><td>
			<input type='text' name='email_confirm' maxlength='50' style='width: 250px' /></td></tr>
			<tr><th>비밀번호</th><td>
			<input type='password' name='passwd' /></td></tr>
			<tr><th>비밀번호 확인</th><td>
			<input type='password' name='passwd_confirm' /></td></tr>
		</table>

		<h3 style='border-bottom: solid 2px black; margin: 20px 0px 10px'>추가정보 입력</h3>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr style='height: 120px'><th>주소</th><td>

			<div class='zipcode-finder'>
				<input type='text' id="dongName" />
				<input type='button' class='zipcode-search' value='검색' />
				<div class="zipcode-search-result" data-name='mb'></div>
			</div>
			<div id='addr'>
				<input type='text' name="mb_zip1" size='3' maxlength='3' readonly /> -
				<input type='text' name="mb_zip2" size='3' maxlength='3' readonly />
				<input type='text' name="mb_addr1" style='width: 250px' readonly /><br/>
				<input type='text' name="mb_addr2" style='width: 250px' />
			</div>
         
         <tr>
            <th>핸드폰</th>
            <td>
               <input type='text' name='mb_hp1' maxlength='4' class='numonly' style='width:30px'/> - 
               <input type='text' name='mb_hp2' maxlength='4' class='numonly' style='width:30px'/> - 
               <input type='text' name='mb_hp3' maxlength='4' class='numonly' style='width:30px'/>
               <input type='hidden' name='mb_contact' class='numonly'/>
            </td>
         </tr>

			</td></tr>
			<tr><th>프로필 사진</th><td>
			<input type='file' name='profilepicture' />
			100*100 크기로 자동 조정됩니다.
			</td></tr>

         <tr>
            <th>SMS 수신동의</th>
            <td><input type="checkbox" name="agree_sms" value="1" checked></td>
         </tr>

         <tr>
            <th>메일 수신동의</th>
            <td><input type="checkbox" name="agree_mail" value="1" checked></td>
         </tr>

		</table>
		<input type="submit" value='회원가입' />
		</form>
	</div>

	</div>
</div>&nbsp;
</div>

<?
include 'module/_tail.php';
?>
