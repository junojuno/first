<?php
$_required = true;
include 'config.php';
include 'module/_head.php';
if(isset($_POST['send']) && $_POST['send'] == 'Y') {
	if($_POST['email'] == '') {
		alert('비정상적인 접근입니다.');
		exit;
	} else {
		$info = sql_fetch("SELECT * FROM ".DB_MEMBERS." WHERE mb_id='{$_POST['email']}'");
		if(!$info['mb_no']) {
			alert('존재하지 않는 이메일입니다.');
			exit;
		}
	}
	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
	$password = uniqid(mt_rand(1, mt_getrandmax()), true);
	$password = hash('sha512', $password);
	$password = hash('sha512', $password.$random_salt);
	
	mysql_query("UPDATE ".DB_MEMBERS." SET 
			mb_password = '{$password}',
			salt = '{$random_salt}'
			WHERE mb_no = {$info['mb_no']}");
   //SEND EMAIL
   $_mail_type = 'findpw';
   include WEB_ROOT.'/module/sendmail.php';
	
}
?>
<script>
$(document).ready(function() {
	$('#find_pw_form').submit(function() {
		if(!$('input[name=email]').val()) {
			qAlert('이메일을 입력해주세요');
			$('input[name=email]').focus();
			return false;
		}
		if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=email]').val()))) {
			qAlert('이메일 형식을 확인해주세요');
			$('input[name=email]').focus();
			return false;
		}
		
	});
	$('input[name=email]').focus();
});
</script>
<div id='highlight'>&nbsp;
<div id='content' class='solid' >
	<div class='inner'>
		<h2>비밀번호 찾기 (위젠 계정만 가능합니다)</h2>
		<form id="find_pw_form" method="POST">
			<input type='hidden' name='send' value='Y'/>
			<table style='width: 100%'>
				<tr>
					<th style='width: 120px;'>이메일 입력</th>
					<td><input type='text' name='email' class='text'/></td>
				</tr>
				<tr>
					<th colspan='2'>
						<input type='submit' value='이메일발송'/>
					</th>
				</tr>
			</table>
			
		</form>
		
	</div>
</div>&nbsp;
</div>
<?
include 'module/_tail.php';
?>

