<?
$_required = true;
include 'config.php';

sec_session_start();


if ($_SESSION[user_no] && $_REQUEST[url]) {
	header('Location: '.urldecode($_REQUEST[url]));
}

if($isMobile) {
	header('Location: /m/login_m.php?url='.urlencode($_GET[url] ? $_GET[url] : ''));
}

if (isset($_POST['uid'], $_POST['p'])) { 

	$email = $_POST['uid'];
	$password = $_POST['p'];

	$url = $_POST[url] ? urldecode($_POST[url]) : '/';

	$logged = login($email, $password, $mysqli);
	if ($logged === true) {
		if($url=='') {
			$url = 'http://wegen.kr';
		}
		header('Location: '.$url);
	} else {
		if($isMobile) {
			header('Location: /m/login_m.php?url='.$url.'&fail=1');
		}
		
		alert("이메일 주소 또는 비밀번호 오류입니다.\\n확인 후 재시도 바랍니다.");
		exit;
	}
} else {
	$_hidestatus = true;
	include'module/_head.php';
?>

<div id='highlight' style='min-height: 600px'>&nbsp;

	<div id='placeholder' style='width: 930px; margin: 50px auto 0px'></div>
	<script>
	$(document).ready(function() {
		$('#loginLayer').show().appendTo('#placeholder')
			.css('margin', '0px auto');
		$('.btn_close').hide();
	});
	</script>
&nbsp;</div>

<?
	include'module/_tail.php';
}
?>