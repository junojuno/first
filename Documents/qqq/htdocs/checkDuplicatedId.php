<?php
/**
 * 이름: 진입명
 * 내용: 이메일 중복 체크 Ajax 모듈
 * 날짜: 2013. 8. 10.
 */

$_required = true;
include 'config.php';

$id = strip_tags($_REQUEST['id']);

$flag = sql_fetch("SELECT mb_id, quit FROM " . DB_MEMBERS . " WHERE mb_id = '$id' LIMIT 1");
	
if ($flag) {
	if($flag[quit] == 1) {
		echo "quit";
	} else {
		echo "exist";
	}
} else {
	echo "none";
}
?>