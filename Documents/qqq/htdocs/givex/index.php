<?
$_required = true;
$settings[title] = '위제너레이션 GIVEx';
$settings[desc] = 'GIVEx - 더 능동적이고 적극적인 나눔선언. 나의 나눔으로 세상을 바꾸는 무브먼트.';
include '../config.php';
include '../module/_head.php';

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'givex', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>

	<div id='about'>

	<div style='text-align: center'>
	<img src='/images/givex/header.png' style='margin: 50px 0px 20px' />
	<h3 style='font-size: 14px; text-align: center'>위젠의 다양한 캠페인에 <span style='color: #E30000; text-shadow: 0px 1px #F8C0C0'>편리하게 후원</span>하면서 <span style='color: #E30000; text-shadow: 0px 1px #F8C0C0'>선택권도 보장</span>합니다!</h3>
	<img src='/images/givex/flow.png' style='max-width: 100%' />
	</div>

	<h4>위젠의 세 가지 기부 철학!</h4>

	<div id='movieFrame' style='float: right; margin-left: 20px'></div>
	<script type='text/javascript'>
	$.getScript('https://www.youtube.com/iframe_api');
	function onYouTubeIframeAPIReady() {
		var p = new YT.Player('movieFrame',{
			videoId:'u9zVEekD3q8',
			width:'365',
			height:'240',
			origin:'http://localhost',
			playerVars:{showinfo: 0}
		});
	}
	</script>

	<p style='margin: 0px; font-weight: bold'><span style='border-bottom: solid 1px #555555'>첫째, 능동적인 기부</span></p>
	<p>거리 모금 캠페인에 이끌려 순간적으로, 혹은 남에게 이끌려 수동적으로만 하는 기부가 아니라<br/>
	<span style='font-family: NanumGothicBold'>사회적 책임 하에 자발적으로 일어나는 기부</span>의 힘을 믿습니다.</p>

	<p style='margin: 20px 0px 0px; font-weight: bold'><span style='border-bottom: solid 1px #555555'>둘째, 즐거운 기부</span></p>
	<p>즐거운 마음으로 기꺼이 일어날 수도 있는 것이 기부임을 믿고 <span style='font-family: NanumGothicBold'>남을 위한 소비가 나 자신을 위한<br/>
	소비 이상의 행복</span>을 가져올 수 있음을 믿습니다.</p>

	<p style='margin: 20px 0px 0px; font-weight: bold'><span style='border-bottom: solid 1px #555555'>셋째, 오픈된 기부</span></p>
	<p>'오른손이 하는 일을 왼손이 알게 하라!' <span style='font-family: NanumGothicBold'>나의 나눔 이야기를 공유</span>함으로써 더 많은 사람들이<br/>
	즐거운 나눔에 동참하게 하는 것이 결코 부끄러운 것이 아니라 <span style='font-family: NanumGothicBold'>꼭 필요한 일</span>임을 믿습니다.</p>

	<div style='clear: both'></div>

	<div style='margin-top: 40px; text-align: right'>
	<h3 style='margin: 0px; padding: 0px; font: 14px NanumGothicBold; text-align: right'>"새로운 기부 문화를 만들기 위한 움직임에 <span style='color: #E30000; text-shadow: 0px 1px #F8C0C0'>함께 해주세요!</span>"</h3>
	<a href='./subscribe.php'><button style='font: 17px NanumGothicBold; color: white; background-color: #E30000; margin-top: 20px; padding: 15px 30px; border: 0px; outline: 0px; cursor: pointer'>정기후원 동참하기 &gt;</button></a>
	</div>


	<h2 style='margin-top: 30px; margin-bottom: 14px'>정기후원 Members</h2>
	<p style='font-size: 12px; color: #A2A2A2; text-shadow: 0px 1px #E3E3E3; margin-bottom: 30px'>즐겁고 능동적인 기부 문화 확산에 함께 해주신 위젠의 정기후원 멤버 여러분입니다!</p>
	<?
	$sql = "SELECT r.mb_no, mb_name, vow_percentage, vow_desc
			FROM ".DB_REGULARPAYMENT." r
			LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = r.mb_no)
			WHERE isFirst = '1'
			ORDER BY od_id DESC ";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	for ($i = 0; $data = sql_fetch_array($result); $i++) {
		$margin = (($i + 1) % 8 == 0) ? '0px 0px 20px' : '0px 4% 20px 0px';
	?>
		<div style='float: left; width: 9%; height: 120px; margin: <?=$margin?>'>
		<div style='height: 78px'>
		<?=drawPortrait($data[mb_no])?>
		</div>
		<p style='margin-top: 8px; line-height: 15px; text-align: center'><span style='font-family: NanumGothicBold'><?=$data[mb_name]?></span> 님</p>

		</div>
	<? $total--; } ?>

		<div style='clear: both'></div>

	</div>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li').eq(0).addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>