<?
$_required = true;
include '../config.php';
include '../module/class.upload.php';
require 'pp_ax_hub_lib.php';

sec_session_start();

if (!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
	exit;
}
$_SESSION[token] = '';

if (!$_SESSION[user_no]) {
	alert('로그인이 필요한 페이지입니다.');
	exit;
}
$site_cd	= $_POST["site_cd"];
$site_key	= $_POST["site_key"];
$req_tx		= $_POST["req_tx"];
$tran_cd	= $_POST["tran_cd"];
$cust_ip	= getenv("REMOTE_ADDR"); // 요청 IP

$pay_method	= $_POST["pay_method"]; // 결제 방법
$ordr_idxx	= $_POST["ordr_idxx"]; // 쇼핑몰 주문번호
if (!preg_match("/^[0-9]{15}$/", $ordr_idxx)) exit;

$good_mny	= $_POST["od_price"]; // 결제 총금액
if (!preg_match("/^[0-9]+$/", $good_mny)) exit;

$buyr_name	= $_POST["buyr_name"]; // 주문자명
$buyr_tel1	= $_POST["buyr_tel1"]; // 주문자 전화번호
$buyr_tel2	= $_POST["buyr_tel2"]; // 주문자 핸드폰 번호
$buyr_mail	= $_POST["buyr_mail"]; // 주문자 E-mail 주소

$isReceipt  = $_POST["isReceipt"];
$realname   = $_POST["realname"];
$receiptPno = ($isReceipt != 0) ? ($isReceipt == 1) ? $_POST["serial1"].'-'.$_POST["serial2"] : $_POST["permit1"].'-'.$_POST["permit2"].'-'.$_POST["permit3"] : false;
$serialno = $_POST["serial1"].$_POST["serial2"];
$permitno = $_POST["permit1"].$_POST["permit2"].$_POST["permit3"];

$kcpgroup_id	= $_POST["kcpgroup_id"]; // 그룹 아이디

$res_cd		= ""; // 결과 코드
$res_msg	= ""; // 결과 메시지
$card_cd	= ""; // 카드 코드
$batch_key	= ""; // 배치 인증키

$giving = $_POST['giving'];


// 인스턴스 생성 및 초기화
$c_PayPlus = new C_PP_CLI;

// 인증 요청
$c_PayPlus->mf_set_encx_data($_POST["enc_data"] , $_POST["enc_info"]);

if ($tran_cd != "") {
	$c_PayPlus->mf_do_tx($trace_no,  $g_conf_home_dir, $site_cd, $site_key, $tran_cd, "", $g_conf_gw_url, $g_conf_gw_port, "payplus_cli_slib",	$ordr_idxx, $cust_ip, $g_conf_log_level, 0,	$g_conf_mode);
}
else {
	$c_PayPlus->m_res_cd  = "9562";
	$c_PayPlus->m_res_msg = "연동 오류";
}

$res_cd = $c_PayPlus->m_res_cd;
$res_msg = $c_PayPlus->m_res_msg;
$res_msg = iconv("euc-kr", "utf-8", $res_msg);

// 이미지 먼저 처리
if ($_FILES['permitimage']) {
	$handle = new upload($_FILES['permitimage']);
	if ($handle->uploaded) {
		$handle->allowed				= array('image/*');
		$handle->image_convert			= 'jpg';
		$handle->file_new_name_body		= 'permit_'.$_SESSION[user_no];
		$handle->process('../data/userupload/');

		if ($handle->processed) {
			$handle->clean();
			mysql_query("UPDATE ".DB_MEMBERS." SET mb_permit_img = 'profile_$_SESSION[user_no].jpg' WHERE mb_no = '$_SESSION[user_no]' ");

		} else {
			echo 'error : ' . $handle->error;
		}
	}
}

// 회원정보 업데이트
$serialno = $serialno ? ", mb_serial   = '$serialno' " : false;
$permitno = $permitno ? ", mb_permit   = '$permitno' " : false;

mysql_query("UPDATE ".DB_MEMBERS."
	SET mb_email	= '$buyr_mail',
		mb_contact	= '$buyr_tel1',
		mb_zip1		= '$_POST[od_zip1]',
		mb_zip2		= '$_POST[od_zip2]',
		mb_addr1	= '$_POST[od_addr1]',
		mb_addr2	= '$_POST[od_addr2]'
		$serialno
		$permitno
	WHERE mb_no = '$_SESSION[user_no]'
");

// 인증 결과 처리
if($res_cd == "0000") {
	$card_cd   = $c_PayPlus->mf_get_res_data("card_cd");
	$batch_key = $c_PayPlus->mf_get_res_data("batch_key");
   if($giving === "Y") {
      sql_query("UPDATE ".DB_REGULARPAYMENT." SET 
         od_escrow2 = '$batch_key',
         od_escrow1 = '$card_cd',
         isReceipt = '$isReceipt',
         receiptName = '$realname',
         receiptPno = '$receiptPno',
         isCancel = 0,
         preferdate = $_POST[preferdate]
         WHERE mb_no = $_SESSION[user_no] and isFirst = 1");
      alert('정기결제정보가 변경되었습니다.', '/');
   } else { 
      ?>
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">

      <head>
      <title>위제너레이션</title>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
      <link rel='stylesheet' type='text/css' href='/css/style.css' />
      <link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
      </head>

      <body onload="document.pay_info.submit()">

      <script type='text/javascript'>
      function noRefresh() {
         if ((event.keyCode == 78) && (event.ctrlKey == true)) {
            event.keyCode = 0;
            return false;
         }
         if(event.keyCode == 116) {
            event.keyCode = 0;
            return false;
         }
      }
      document.onkeydown = noRefresh;
      </script>

      <div class='layer'>
         <div id='pluginLayer' class='centering' style='display: block; margin-left: -200px; margin-top: -150px'>
         <img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
         <p>결제 진행 중입니다.</p>
         창을 닫거나 새로고침하지 마십시오.
         </div>
      </div>

      <form name='pay_info' method='post' action='./pp_cli_hub.php'>
         <input type='hidden' name="ordr_idxx"	value="<?=$ordr_idxx      ?>" />
         <input type="hidden" name="req_tx"		value="pay" />
         <input type="hidden" name="pay_method"	value="CARD" />
         <input type="hidden" name="card_pay_method" value="Batch" />
         <input type="hidden" name="currency"	value="410" />
         <input type='hidden' name="bt_group_id"	value="<?=$kcpgroup_id    ?>">
         <input type='hidden' name="bt_batch_key"	value="<?=$batch_key      ?>">
         <input type="hidden" name="good_mny"	value="<?=$good_mny       ?>" />
         <input type='hidden' name="buyr_name"	value="<?=$buyr_name      ?>" />
         <input type='hidden' name="buyr_tel1"	value="<?=$buyr_tel1      ?>" />
         <input type='hidden' name="buyr_tel2"	value="<?=$buyr_tel2      ?>" />
         <input type='hidden' name="buyr_mail"	value="<?=$buyr_mail      ?>" />
         <input type='hidden' name="vow_percentage"	value="<?=$_POST[percentage]?>" />
         <input type='hidden' name="vow_desc"	value="<?=$_POST[vow]?>" />
         <input type='hidden' name="preferdate"	value="<?=$_POST[preferdate]?>" />
         <input type='hidden' name="coin_method"	value="<?=$_POST[coin_method]?>" />
         <input type='hidden' name="buyr_mail"	value="<?=$buyr_mail      ?>" />
         <input type='hidden' name='isReceipt' value="<?=$isReceipt?>">
         <input type='hidden' name='realname' value="<?=$realname?>">
         <input type='hidden' name='receiptPno' value="<?=$receiptPno?>">
         <input type='hidden' name='givecode'    value='<?=$_POST[givecode]?>' />
         <? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
         <input type='hidden' name='token'		value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
      <? /*
         <input type='hidden' name="card_cd"		value="<?=$card_cd        ?>">
         <input type='hidden' name="res_cd"		value="<?=$res_cd         ?>">
         <input type='hidden' name="res_msg"		value="<?=$res_msg        ?>">
      */ ?>

      </form>

      </body>
      </html>
      <?
   }
}
else {
	alert("에러 코드 $res_cd - $res_msg 사이트 관리자에게 문의 바랍니다.", '/');
}
?>
