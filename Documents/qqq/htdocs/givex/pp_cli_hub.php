<?
$_required = true;
include '../config.php';

sec_session_start();

require "./pp_cli_hub_lib.php";

if(!isset($_POST['token']) || $_POST['token'] != $_SESSION[token]) {
//	print 'Nice try, kid.';
	exit;
}
$_SESSION[token] = '';

$pay_method = $_POST[ "pay_method" ];  // 결제 방법
$ordr_idxx  = $_POST[ "ordr_idxx"  ];  // 주문 번호
$good_name  = $_POST[ "good_name"  ];  // 상품 정보
$good_mny   = $_POST[ "good_mny"   ];  // 결제 금액
$buyr_name  = $_POST[ "buyr_name"  ];  // 주문자 이름
$buyr_mail  = $_POST[ "buyr_mail"  ];  // 주문자 E-Mail
$buyr_tel1  = $_POST[ "buyr_tel1"  ];  // 주문자 전화번호
$buyr_tel2  = $_POST[ "buyr_tel2"  ];  // 주문자 휴대폰번호
$req_tx     = $_POST[ "req_tx"     ];  // 요청 종류
$currency   = $_POST[ "currency"   ];  // 화폐단위 (WON/USD)

$group_id   = $_POST[ "bt_group_id"  ];
$batch_key  = $_POST[ "bt_batch_key" ]; 

$isReceipt  = $_POST["isReceipt"];
$realname   = $_POST["realname"];
$receiptPno = $_POST["receiptPno"];

$soc_no     = $_POST[ "soc_no"  ];     // 주민등록번호
$tran_cd    = ""; // 트랜잭션 코드
$bSucc      = ""; // DB 작업 성공 여부

$res_cd     = ""; // 결과코드
$res_msg    = ""; // 결과메시지
$tno        = ""; // 거래번호

$card_pay_method = $_POST[ "card_pay_method" ];  // 카드 결제 방법
$card_cd         = "";                                                       // 카드 코드
$card_no         = "";                                                       // 카드 번호
$card_name       = "";                                                       // 카드명
$app_time        = "";                                                       // 승인시간
$app_no          = "";                                                       // 승인번호
$noinf           = "";                                                       // 무이자여부
$quota			 = "";														 // 할부개월
$cust_ip = getenv( "REMOTE_ADDR" );

/* =   02. 인스턴스 생성 및 초기화                                              = */

$c_PayPlus  = new C_PAYPLUS_CLI;
$c_PayPlus->mf_clear();


if ( $req_tx == "pay" ) {
	$tran_cd = "00100000";
    $common_data_set = "";
    $common_data_set .= $c_PayPlus->mf_set_data_us("amount", $good_mny);
    $common_data_set .= $c_PayPlus->mf_set_data_us("currency", $currency);

	if (!soc_no == "") {
		$common_data_set .= $c_PayPlus->mf_set_data_us("soc_no",  $soc_no );
	}
	$common_data_set .= $c_PayPlus->mf_set_data_us("cust_ip",  $cust_ip );
	$common_data_set .= $c_PayPlus->mf_set_data_us("escw_mod", "N"      );

	$c_PayPlus->mf_add_payx_data( "common", $common_data_set );

			// 주문 정보
			$c_PayPlus->mf_set_ordr_data( "ordr_idxx", $ordr_idxx );
//			$c_PayPlus->mf_set_ordr_data( "good_name", good_name );
			$c_PayPlus->mf_set_ordr_data( "good_mny",  $good_mny  );
//			$c_PayPlus->mf_set_ordr_data( "buyr_name", buyr_name );
//			$c_PayPlus->mf_set_ordr_data( "buyr_tel1", buyr_tel1 );
//			$c_PayPlus->mf_set_ordr_data( "buyr_tel2", buyr_tel2 );
//			$c_PayPlus->mf_set_ordr_data( "buyr_mail", buyr_mail );

	if ( $pay_method == "CARD" ) {
		$card_data_set;

		$card_data_set .= $c_PayPlus->mf_set_data_us( "card_mny", $good_mny );        // 결제 금액

		if ( $card_pay_method == "Batch" ) {
			$card_data_set .= $c_PayPlus->mf_set_data_us( "card_tx_type",   "11511000" );
			$card_data_set .= $c_PayPlus->mf_set_data_us( "quota", "00"); // 히든폼값을 없애고 강제지정
			$card_data_set .= $c_PayPlus->mf_set_data_us( "bt_group_id",    $group_id );
			$card_data_set .= $c_PayPlus->mf_set_data_us( "bt_batch_key",   $batch_key );
		}
		$c_PayPlus->mf_add_payx_data( "card", $card_data_set );
	}
}
// 취소/매입 요청
else if ( $req_tx == "mod" ) {
	$mod_type = $_POST[ "mod_type" ];

	$tran_cd = "00200000";

	$c_PayPlus->mf_set_modx_data( "tno",      $_POST[ "tno" ]      );      // KCP 원거래 거래번호
	$c_PayPlus->mf_set_modx_data( "mod_type", $mod_type            );      // 원거래 변경 요청 종류
	$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip             );      // 변경 요청자 IP
	$c_PayPlus->mf_set_modx_data( "mod_desc", $_POST[ "mod_desc" ] );      // 변경 사유
}

if ( $tran_cd != "" ) {
	$c_PayPlus->mf_do_tx( $trace_no, $g_conf_home_dir, $g_conf_batch_cd, $g_conf_batch_key, $tran_cd, "",
		$g_conf_gw_url, $g_conf_gw_port, "payplus_cli_slib", $ordr_idxx,
		$cust_ip, "3" , 0, 0, $g_conf_key_dir, $g_conf_log_dir); // 응답 전문 처리

	$res_cd  = $c_PayPlus->m_res_cd;  // 결과 코드
	$res_msg = $c_PayPlus->m_res_msg; // 결과 메시지
}
else {
	$c_PayPlus->m_res_cd  = "9562";
	$c_PayPlus->m_res_msg = "연동 오류|Payplus Plugin이 설치되지 않았거나 tran_cd값이 설정되지 않았습니다.";
}

    /* =   04. 승인 결과 처리                                                       = */

$givecode = strtoupper($_POST['token']);

if ( $req_tx == "pay" ) {
	if ( $res_cd == "0000" ) {
		$tno       = $c_PayPlus->mf_get_res_data( "tno"       ); // KCP 거래 고유 번호

		if ( $pay_method == "CARD" ) {
			$card_cd   = $c_PayPlus->mf_get_res_data( "card_cd"   ); // 카드사 코드
			$card_no   = $c_PayPlus->mf_get_res_data( "card_no"   ); // 카드 번호
			$card_name = $c_PayPlus->mf_get_res_data( "card_name" ); // 카드 종류
			$app_time  = $c_PayPlus->mf_get_res_data( "app_time"  ); // 승인 시간
			$app_no    = $c_PayPlus->mf_get_res_data( "app_no"    ); // 승인 번호
			$noinf     = $c_PayPlus->mf_get_res_data( "noinf"     ); // 무이자 여부 ( 'Y' : 무이자 )
			$quota     = $c_PayPlus->mf_get_res_data( "quota"     ); // 할부 개월 수

         $data = sql_fetch("SELECT isCancel FROM ".DB_REGULARPAYMENT." WHERE mb_no = $_SESSION[user_no] and isFirst = 1 and isCancel = 1");
         if($data[isCancel]) {
            $result = mysql_query("UPDATE ".DB_REGULARPAYMENT."
                     SET
                     od_id		= '$ordr_idxx',
                     mb_no		= '$_SESSION[user_no]',
                     od_name		= '$buyr_name',
                     od_hp		= '$buyr_tel1',
                     od_zip1		= '$_POST[od_zip1]',
                     od_zip2		= '$_POST[od_zip2]',
                     od_addr1	= '$_POST[od_addr1]',
                     od_addr2	= '$_POST[od_addr2]',
                     od_amount	= '$good_mny',
                     od_ip		= '$cust_ip',
                     od_method	= '신용카드',
                     od_escrow1	= '$card_cd',
                     od_escrow2	= '$batch_key',
                     od_groupid  = '$group_id',
                     od_tno = '$tno',
                     preferdate  = '$_POST[preferdate]',
                     coin_method = '$_POST[coin_method]',
                     vow_percentage  = '$_POST[vow_percentage]',
                     vow_desc    = '$_POST[vow_desc]',
                     isReceipt   = '$isReceipt',
                     receiptName = '$realname',
                     receiptPno  = '$receiptPno',
                     isFirst		= '1',
                     isCancel = 0,
                     givecode    = '$givecode' WHERE mb_no = $_SESSION[user_no] and isFirst = 1
            ");
         } else {
            $result = mysql_query("INSERT INTO ".DB_REGULARPAYMENT."
                     SET
                     od_id		= '$ordr_idxx',
                     mb_no		= '$_SESSION[user_no]',
                     od_name		= '$buyr_name',
                     od_hp		= '$buyr_tel1',
                     od_zip1		= '$_POST[od_zip1]',
                     od_zip2		= '$_POST[od_zip2]',
                     od_addr1	= '$_POST[od_addr1]',
                     od_addr2	= '$_POST[od_addr2]',
                     od_amount	= '$good_mny',
                     od_ip		= '$cust_ip',
                     od_method	= '신용카드',
                     od_escrow1	= '$card_cd',
                     od_escrow2	= '$batch_key',
                     od_groupid  = '$group_id',
                     od_tno = '$tno',
                     preferdate  = '$_POST[preferdate]',
                     coin_method = '$_POST[coin_method]',
                     vow_percentage  = '$_POST[vow_percentage]',
                     vow_desc    = '$_POST[vow_desc]',
                     isReceipt   = '$isReceipt',
                     receiptName = '$realname',
                     receiptPno  = '$receiptPno',
                     isFirst		= '1',
                     givecode    = '$givecode'
            ");
         }

			if ($result) {
				$result = mysql_query("INSERT INTO ".DB_COINS."
						SET
						mb_no		= '$_SESSION[user_no]',
						coin_category = '1',
						coin_desc	= 'FIRST CHARGE',
						od_id		= '$ordr_idxx',
						amount	= '$good_mny'
				");
			}
		}

		// 회원정보 업데이트는 pp_ax_hub에서 처리

		// 뱃지 발급 루틴
		$procBadges = array();

		if ($result) {
			//정기후원자 뱃지
			array_push($procBadges, '90');

			$c = sql_fetch("
					SELECT COUNT(od_id) AS total
					FROM ".DB_REGULARPAYMENT);
			if ($c[total] <= 300) {
				array_push($procBadges, '91');
			}

			for ($i = 0; $i < count($procBadges); $i++) {
				$check = sql_fetch("
						SELECT mb_no
						FROM ".DB_BADGES."
						WHERE mb_no = '$_SESSION[user_no]'
						AND category = '$procBadges[$i]'
						");
				if (!$check[mb_no]) {
					$badge = "INSERT ".DB_BADGES."
								SET mb_no			= '$_SESSION[user_no]',
									category		= '$procBadges[$i]',
									od_id			= '$ordr_idxx',
									it_id			= 'givex'
							";
					sql_query($badge);
				}
				unset($check);
			}
		
		}


		// givecode 루틴
		if ($_POST[givecode]) {
			$givecode_proc = (strlen($_POST[givecode]) == 8) ? strtoupper($_POST[givecode]) : '00000000';
			$cck = sql_fetch("SELECT od_id, givecode_used FROM ".DB_REGULARPAYMENT."
					WHERE givecode = '$givecode_proc'
					");

			if ($cck[givecode_used] == '0') {
				$givecode_used = '1';
				$result = mysql_query("UPDATE ".DB_REGULARPAYMENT."
									SET givecode_used = '1'
									WHERE od_id = '$cck[od_id]' ");

				if ($result) {
					$result = mysql_query("INSERT INTO ".DB_COINS."
							SET
							mb_no		= '$_SESSION[user_no]',
							coin_category = '3',
							coin_desc	= 'GIVE CODE',
							od_id		= '$ordr_idxx',
							amount	= '2000'
					");
				}
			}
		}


		$bSucc = "";             // DB 작업 실패일 경우 "false" 로 세팅

		if ( $req_tx == "pay" ) {
			if( $res_cd == "0000" ) {	
				if ( $bSucc == "false" ) {
					$c_PayPlus->mf_clear();

					$tran_cd = "00200000";

					$c_PayPlus->mf_set_modx_data( "tno",      $tno                         );  // KCP 원거래 거래번호
					$c_PayPlus->mf_set_modx_data( "mod_type", "STSC"                       );  // 원거래 변경 요청 종류
					$c_PayPlus->mf_set_modx_data( "mod_ip",   $cust_ip                     );  // 변경 요청자 IP
					$c_PayPlus->mf_set_modx_data( "mod_desc", "결과 처리 오류 - 자동 취소" );  // 변경 사유

					$c_PayPlus->mf_do_tx( $tno,  $g_conf_home_dir, $g_conf_site_cd,
						  $g_conf_site_key,  $tran_cd,    "",
						  $g_conf_gw_url,  $g_conf_gw_port,  "payplus_cli_slib",
						  $ordr_idxx, $cust_ip, "3" ,
						  0, 0, $g_conf_key_dir, $g_conf_log_dir);

					$res_cd  = $c_PayPlus->m_res_cd;
					$res_msg = $c_PayPlus->m_res_msg;
				}
			}
		}
	}
}
echo $mod_type;
$res_msg = iconv("euc-kr", "utf-8", $res_msg);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>위제너레이션</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='/css/style.css' />
<link rel='shortcut icon' type='image/png' href='/images/favicon.png' />
</head>

<body onload="document.pay_info.submit()">

<script type='text/javascript'>
function noRefresh() {
	if ((event.keyCode == 78) && (event.ctrlKey == true)) {
		event.keyCode = 0;
		return false;
	}
	if(event.keyCode == 116) {
		event.keyCode = 0;
		return false;
	}
}
document.onkeydown = noRefresh;
</script>

<div class='layer'>
	<div id='pluginLayer' class='centering' style='display: block; margin-left: -200px; margin-top: -150px'>
	<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
	<p>결제 진행 중입니다.</p>
	창을 닫거나 새로고침하지 마십시오.
	</div>
</div>

<form name="pay_info" method="post" action="./result.php">
	<input type="hidden" name="req_tx"     value="<?=$req_tx     ?>">  <!-- 요청 구분 -->
	<input type="hidden" name="pay_method" value="<?=$pay_method ?>">  <!-- 사용한 결제 수단 -->
	<input type="hidden" name="bSucc"      value="<?=$bSucc      ?>">  <!-- 쇼핑몰 DB 처리 성공 여부 -->

	<input type="hidden" name="res_cd"     value="<?=$res_cd     ?>">  <!-- 결과 코드 -->
	<input type="hidden" name="res_msg"    value="<?=$res_msg    ?>">  <!-- 결과 메세지 -->
	<input type="hidden" name="ordr_idxx"  value="<?=$ordr_idxx  ?>">  <!-- 주문번호 -->
	<input type="hidden" name="tno"        value="<?=$tno        ?>">  <!-- KCP 거래번호 -->
	<input type="hidden" name="good_mny"   value="<?=$good_mny   ?>">  <!-- 결제금액 -->
	<input type="hidden" name="good_name"  value="<?=$good_name  ?>">  <!-- 상품명 -->
	<input type="hidden" name="buyr_name"  value="<?=$buyr_name  ?>">  <!-- 주문자명 -->
	<? /*
	<input type="hidden" name="buyr_tel1"  value="<?=$buyr_tel1  ?>">  <!-- 주문자 전화번호 -->
	<input type="hidden" name="buyr_tel2"  value="<?=$buyr_tel2  ?>">  <!-- 주문자 휴대폰번호 -->
	<input type="hidden" name="buyr_mail"  value="<?=$buyr_mail  ?>">  <!-- 주문자 E-mail -->
	*/ ?>
	<input type="hidden" name="card_cd"    value="<?=$card_cd    ?>">  <!-- 카드코드 -->
	<input type="hidden" name="card_no"    value="<?=$card_no    ?>">  <!-- 카드번호 -->
	<input type="hidden" name="card_name"  value="<?=$card_name  ?>">  <!-- 카드명 -->
	<input type="hidden" name="app_time"   value="<?=$app_time   ?>">  <!-- 승인시간 -->
	<input type="hidden" name="app_no"     value="<?=$app_no     ?>">  <!-- 승인번호 -->
	<input type="hidden" name="quota"      value="<?=$quota      ?>">  <!-- 할부개월 -->
	<input type="hidden" name="noinf"      value="<?=$noinf      ?>">  <!-- 무이자여부 -->
	<input type="hidden" name="givecode"   value="<?=$givecode   ?>">
	<input type="hidden" name="givecode_used"   value="<?=$givecode_used   ?>">

</form>

</body>
</html>
