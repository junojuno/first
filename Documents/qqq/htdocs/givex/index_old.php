<?
$_required = true;
$settings[title] = '위제너레이션 GIVEx';
$settings[desc] = 'GIVEx - 더 능동적이고 적극적인 나눔선언. 나의 나눔으로 세상을 바꾸는 무브먼트.';
include '../config.php';
include '../module/_head.php';
?>

<div id='content' class='givex'>
	<div style='background-color: black'><div style='padding: 20px; color: #CFCFCF'>
		<div style='text-align: center; border-bottom: solid 1px gray; margin-bottom: 30px'>
		<img src='/images/givex/header.png' alt='WE GIVE x' style='margin-bottom: 30px' />
		</div>

		<div id='movieFrame' style='float: left; margin-right: 20px'></div>
		<script type='text/javascript'>
		$.getScript('https://www.youtube.com/iframe_api');
		function onYouTubeIframeAPIReady() {
			var p = new YT.Player('movieFrame',{
				videoId:'u9zVEekD3q8',
				width:'320',
				height:'240',
				origin:'http://localhost',
				playerVars:{showinfo: 0}
			});
		}
		</script>

		<h3>GIVEx는 자발적인 정기후원을 통해 세 가지 기부 철학을 추구합니다.</h3>

		<p>첫째, 능동적인 기부!</p>
		기부는 우연한 기회나 수동적인 강요를 넘어 사회적 책임 하에 능동적으로 이루어질 수 있습니다.

		<p>둘째, 즐거운 기부!</p>
		불쌍하고 슬픈 마음으로 이루어지는 것만이 기부는 아닙니다. 나눔은 충분히 즐거운 경험이 될 수 있습니다.

		<p>셋째, 오픈된 기부!</p>
		'오른손이 하는 일을 왼손이 알게 하라!' 공유로 더 많은 사람들이 즐거운 나눔에 동참할 수 있음을 믿습니다.
		<div style='clear: both; text-align: right; color: white; text-decoration: underline; height: 40px; line-height: 40px; padding-right: 20px'>더 보기 &gt;</div>

	</div></div>

	<div style='position: relative; text-align: right; margin: 10px 0px 20px; font-family: NanumGothicBold; padding-right: 90px'>GIVEx 응원하기 &nbsp;
	<div class='fb-like' style='position: relative; top: 5px' data-href='http://wegen.kr/givex' data-send='false' data-layout='button_count' data-width='100' data-show-faces='false'></div>
	<span style='position: absolute; top: 5px; left: 850px'><a href="https://twitter.com/share" class="twitter-share-button" count='horizontal' data-url='http://wegen.kr/givex' data-lang="ko">Tweet</a></span>
	</div>

	<h2>GIVEx 선서</h2>

	<div style='position: relative; margin-top: 30px; height: 220px; background: black url(/images/givex/blackboard.png) no-repeat center'>
	<form name='subscribeForm' method='post' action='./subscribe.php'>
	<input type='text' name='vow' style='position: absolute; top: 85px; left: 400px; width: 400px; height: 30px; background: transparent; color: #CFCD99; border: none; font: 13pt NanumGothic' maxlength='50'>
	<input type='image' src='/images/givex/btn_subscribe.png' style='cursor: pointer; position: absolute; bottom: 20px; right: 30px' />
	</form>
	</div>

	<div style='margin-top: 30px'>
	<?
	$sql = "SELECT r.mb_no, mb_name, vow_percentage, vow_desc
			FROM ".DB_REGULARPAYMENT." r
			LEFT JOIN ".DB_MEMBERS." m ON (m.mb_no = r.mb_no)
			WHERE isFirst = '1'
			ORDER BY od_id DESC ";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	for ($i = 0; $data = sql_fetch_array($result); $i++) {
		$chars = array('.', '!', '^');
		if (array_search($data[vow_desc][strlen($data[vow_desc])-1], $chars) === false) {
			$data[vow_desc] .= '.';
		}
	?>
		<div class='vows' style='margin: 20px 0px; outline: solid 1px gray; background: url(/images/givex/logo_givex.png) no-repeat 98% center'><div style='padding: 20px; line-height: 20px; '>
			<div style='float: left; width: 30px; font: 12pt/40px Arial; text-align: center'><?=$total?></div>
			<?=drawPortrait($data[mb_no])?>
			<strong><?=$data[mb_name]?></strong> 님은 수입의 <span style='font-weight: bold; color: #E30000'><?=$data[vow_percentage]?>%</span>를 꾸준히 나눌 것을 선서합니다.<br/>
			나의 나눔으로 <span style='font-weight: bold; color: #E30000'><?=$data[vow_desc]?></span>
			<div style='clear: both'></div>
		</div></div>
	<? $total--; } ?>
	</div>
</div>

<script type='text/javascript'>
$(document).ready(function() {
	addLabel('input[name=vow]', '당신이 만들고 싶은 변화를 입력해주세요!');
});
</script>
<?
include '../module/_tail.php';
?>