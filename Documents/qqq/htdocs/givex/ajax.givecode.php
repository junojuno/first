<?
if (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$_required = true;
$_path = '../';
include '../config.php';

//sec_session_start();

if (!$_POST[givecode]) {
	exit;
}
$givecode = (strlen($_POST[givecode]) == 8) ? strtoupper($_POST[givecode]) : '00000000';

$result = sql_fetch("SELECT givecode_used FROM ".DB_REGULARPAYMENT."
		WHERE givecode = '$givecode'
		");
if (!$result) {
	print "0009"; // invalid code
	exit;
}
if ($result[givecode_used] == '0') {
	print "0000";
}
else if ($result[givecode_used] == '1') {
	print "0001"; // already used
}
else {
	print "0009"; // unknown err
}
?>