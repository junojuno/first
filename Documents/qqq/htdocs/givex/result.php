<?
$_required = true;
include '../config.php';
//if (!$_POST) header("Location: /");
include '../module/_head.php';

$req_tx           = $_POST[ "req_tx"       ];      // 요청 구분(승인/취소)
$pay_method       = $_POST[ "pay_method"   ];      // 사용 결제 수단
$bSucc            = $_POST[ "bSucc"        ];      // 업체 DB 정상처리 완료 여부

$res_cd           = $_POST[ "res_cd"       ];      // 결과 코드
$res_msg          = $_POST[ "res_msg"      ];      // 결과 메시지
$res_msg_bsucc    = "" ;

$ordr_idxx        = $_POST[ "ordr_idxx"    ];      // 주문번호
$tno              = $_POST[ "tno"          ];      // KCP 거래번호
$good_mny         = $_POST[ "good_mny"     ];      // 결제 금액
$good_name        = $_POST[ "good_name"    ];      // 상품명
$buyr_name        = $_POST[ "buyr_name"    ];      // 구매자명
$buyr_tel1        = $_POST[ "buyr_tel1"    ];      // 구매자 전화번호
$buyr_tel2        = $_POST[ "buyr_tel2"    ];      // 구매자 휴대폰번호
$buyr_mail        = $_POST[ "buyr_mail"    ];      // 구매자 E-Mail

// 신용카드
$card_cd          = $_POST[ "card_cd"      ];      // 카드 코드
$card_no          = $_POST[ "card_no"      ];      // 카드 번호
$card_name        = $_POST[ "card_name"    ];      // 카드명
$app_time         = $_POST[ "app_time"     ];      // 승인시간 (공통)
$app_no           = $_POST[ "app_no"       ];      // 승인번호
$quota            = $_POST[ "quota"        ];      // 할부개월
$noinf            = $_POST[ "noinf"        ];      // 무이자여부

$coinsCharged = ($_POST[givecode_used] == 1) ? 2000 : 0;
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
	<div class='inner donation'>

	<h2 style='border: none'>정기후원 신청 결과</h2>

		<table cellpadding='0' cellpadding='0' style='width: 500px; margin: 30px auto'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>결과</th><td><?=$res_msg?> (<?=$res_cd?>)</td></tr>

<?
if ($req_tx == "pay") {
	if($bSucc == "false") {
		$res_msg_bsucc = ($res_cd == "0000") ? '결제는 정상적으로 이루어졌으나 결제 결과를 처리하는 중 오류가 발생하여 자동으로 취소 요청 되었습니다.<br/>고객센터로 문의하여 확인하시기 바랍니다.' : '결제는 정상적으로 이루어졌으나 결제 결과를 처리하는 중 오류가 발생하여 자동으로 취소 요청을 했지만, <strong>취소 요청에 실패했습니다.</strong><br/>반드시 고객센터로 문의해주시기 바랍니다.';
//		print "<tr><td colspan='2'>$res_msg_bsucc</td></tr>";
		print "<div style='text-align: center'>$res_msg_bsucc</div>";
	}
	else {
		if ($res_cd == "0000") {
			$app_time = str_split($app_time, 2);
			$app_time = $app_time[0].$app_time[1].'-'.$app_time[2].'-'.$app_time[3].' '.$app_time[4].':'.$app_time[5].':'.$app_time[6];
?>
			<tr><th>정기후원 번호</th><td><?=$ordr_idxx?></td></tr>
			<tr><th>정기후원 금액</th><td><?=number_format($good_mny);?>원</td></tr>
<?
			if ($pay_method == "CARD") {
?>
			<tr><th>후원 수단</th><td>신용카드 <a href="javascript:receiptView('<?=$tno?>')">[영수증 출력]</a></td></tr>
			<tr><th>승인시간</th><td><?=$app_time?></td></tr>
<?
			}
		}
	}
}
?>
		</table>
<? if ($bSucc != 'false' && $res_cd == '0000') {

	// ACTIVITY LOG
	if ($_SESSION[user_no]) {
		mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'givex_done', referer = '$_SERVER[HTTP_REFERER]' ");
	}

?>
	<div style='font: 13px/30px NanumGothicBold; color: black; text-align: center; background-color: #F4F4E3; margin: 20px 0px'>
	<img src='/images/common/coin.png' style='vertical-align: middle' />
	회원님의 아이디에 코인 <span style='color: #E30000; font: bold 10pt Arial'><?=number_format($good_mny + $coinsCharged)?></span>개가 충전되었습니다.</span>
	</div>
<? } ?>

		<h3 style='margin-bottom: 20px'>공유하기</h3>
		나의 기부 서약 사실을 SNS를 통해 공유할 수 있습니다. 즐거운 나눔의 경험을 주위 친구들과 공유해 보세요!

		<div style='text-align: center; margin: 30px 0px'>
		<img src='/images/givex/share_title.png' />
		<p style='text-align: center; font-size: 15px'>(금액은 공유되지 않습니다)</p>
		</div>

		<div style='text-align: center; margin-bottom: 20px'>
		<img class='btn_facebook clickable' src='/images/campaign/btn_share_fb.png' />
		<img class='btn_twitter clickable' src='/images/campaign/btn_share_tw.png' />
		</div>

      <!--
		<h3 style='margin: 20px 0px'>친구에게 초대장 보내기</h3>
		친구에게 초대장을 보내면서 2,000개의 기부코인을 무료로 선물하실 수 있습니다. 즐거운 나눔에 친구도 초대해보세요!

		<div style='text-align: center; margin: 20px 0px'>
		<img src='/images/givex/invite_title.png' />
		</div>

		<div style='text-align: center'>
		<img class='btn_invite_fb clickable' src='/images/givex/btn_invite_facebook.png' />
		<img class='btn_invite_email clickable' src='/images/givex/btn_invite_email.png' />
		</div>

		<div id='invite_email' style='width: 500px; background-color: white; display: none'>
		<span class='btn_close'>×</span>

		<form name='codeForm' method='post' action='/sendmail.php'>
		<input type='hidden' name='mailtype' value='code' />
		<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
		<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>' >

		<table cellpadding='0' cellpadding='0' style='width: 100%; margin-top: 10px'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>보내는 이</th><td>
			<input type='text' name='username' value='<?=$_SESSION[username]?>' /></td></tr>
			<tr><th>보내는 이메일</th><td>
			<?
			$info = sql_fetch("SELECT mb_email FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
			?>
			<input type='text' name='email' maxlength='50' value='<?=$info[mb_email]?>' style='width: 250px' /></td></tr>
			<tr><th>초대받을<br/>친구의 이메일</th><td>
			<input type='text' name='email_confirm' maxlength='50' style='width: 250px' /></td></tr>
			<tr><th>제목</th><td>
			<input type='text' name='subject' style='width: 90%' value='예시) 착하게 살고 싶은 너에게 강추한다 친구야!' /></td></tr>
			<tr><th>내용</th><td>
			<textarea name='content' style='width: 90%' class='text' rows='15'>
위제너레이션은 스타와 함께 하는 온라인 기부 사이트로, 하나의 사이트에서 다양한 자선단체 모금 캠페인을 한 눈에 보고 원하는 캠페인을 골라 후원할 수 있습니다. 또한 각 캠페인에 연예인, 멘토 등 다양한 유명인사가 참여하여 기부자와의 저녁식사, 봉사활동, 파티 등의 다양한 이벤트를 개최합니다.

GIVEx 무브먼트는 정기후원을 통해 이러한 즐거운 나눔 문화에 꾸준히 함께 참여할 것을 선언하는 것입니다.
매달 정기후원을 신청한 날짜에 내 계정으로 코인이 적립되어 원하는 캠페인에 쉽게 배분하는 방식입니다. 

즐거운 기부, 능동적인 기부, 열린 기부를 표방하는 GIVEx에 함께해주세요! 위제너레이션의 무브먼트에 당신을 초대합니다.

http://wegen.kr/givex

정기후원에 참여할 때 아래의 GIVE 코드를 입력하면 2,000개의 기부코인을 추가로 충전해 드립니다.			
GIVE 코드: <?=$_POST[givecode];?></textarea></td></tr>
		</table>

		<div style='text-align: center'>
		<input type='submit' value='초대장 발송' class='submit' onclick="return checkCodeForm(this.form);" />
		</div>
		</form>
		</div>
      -->

	</div>
</div>&nbsp;
</div>

<script>
$(document).ready(function() {
	$('input[type=text]').addClass('text');
	$('.btn_invite_email').click(function() {
		createLayer($('#invite_email'));
	});
	$('.btn_invite_fb').click(postToSend);
});

function postToSend() {
	var obj = {
		method: 'send',
		link: 'http://wegen.kr/givex',
		picture: 'http://wegen.kr/images/common/logo_facebook.jpg',
		name: '<?=$_SESSION[username]?>님이 당신을 GIVEx 캠페인에 초대하셨습니다.',
		caption: '스타와 함께 하는 즐거운 기부, 위젠',
		description: 'GIVE 코드 <?=$_POST[givecode]?> 를 정기후원 신청 페이지에서 입력하시면 2,000코인을 추가로 충전해 드립니다.'
	};

	FB.ui(obj);
}

function postToFeed() {
	var obj = {
		method: 'feed',
		link: 'http://wegen.kr/givex',
		picture: 'http://wegen.kr/images/common/logo_facebook.jpg',
		name: '<?=$_SESSION[username]?>님이 GIVEx 캠페인에 동참하셨습니다.',
		caption: '스타와 함께 하는 즐거운 기부, 위젠',
		description: 'GIVEx는 자발적이고 보다 성숙한 나눔을 위한 위젠의 무브먼트입니다.'
	};

	function callback(response) {
		if (response) {
			procShare('<?=$it_id?>', 'facebook', response['post_id']);
		}
	}
	FB.ui(obj, callback);
}

function postToTweet() {
	window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent('http://wegen.kr/campaign/<?=$it_id?>') + '&text=' + encodeURIComponent("위제너레이션의 GIVEx 무브먼트에 동참합니다."),"twitterPop", 'width=600 height=350');
}

function receiptView(tno) {
	receiptWin = "http://admin.kcp.co.kr/Modules/Sale/Card/ADSA_CARD_BILL_Receipt.jsp?c_trade_no=" + tno;
	window.open(receiptWin , "" , "width=420, height=670");
}
</script>

<?
include '../module/_tail.php';
?>
