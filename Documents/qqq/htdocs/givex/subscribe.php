<?
$_loginrequired = true;
$_required = true;
include '../config.php';
include '../module/_head.php';

if($btb = btbCheck($_SESSION[user_no], $settings[btb])) {
	print "<script>
		alert('본 계정으로는 정기후원을 신청하실 수 없습니다.');
		history.go(-1);
		</script>";
	exit;
}

$giving = false;
if ($stmt = $mysqli->prepare("SELECT od_id FROM wegen_regularpayment WHERE mb_no = ? and isFirst = 1 and isCancel = 0 LIMIT 1")) {
	$stmt->bind_param('i', $_SESSION['user_no']);
	$stmt->execute();
	$stmt->store_result();

	if($stmt->num_rows == 1) {
      $giving = true;
	}
}

$vow = ($_POST[vow] && $_POST[vow] != '당신이 만들고 싶은 변화를 입력해주세요!') ? $_POST[vow] : false;


// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '2', mb_no = '$_SESSION[user_no]', param1 = 'givex', referer = '$_SERVER[HTTP_REFERER]' ");
}

?>

<script type='text/javascript' src='<?=$g_conf_js_url?>'></script>
<script type='text/javascript' src='/js/zipsearch.js'></script>
<script type='text/javascript'>
createLayer($('#pluginLayer'));
StartSmartUpdate();

$(document).ready(function() {
	$('input[type=text]').addClass('text');
	addLabel('input[name=vow]', '회원님이 나눔으로 꿈꾸는 미래를 적어주세요 :)');

<? if($giving) { ?>
   $('input[name=giving]').val('Y');
   alert('이미 정기후원중이십니다.\n새로 신청하시면 업데이트된 정보로 정기결제가 이루어집니다.');
<? } ?>

	$('input[name=givecode]').focusout(function() {
		$.ajax({
			type: "POST",
			url: "/givex/ajax.givecode.php",
			data: {
				'givecode' : $(this).val()
			},
			cache: false,
			success: function(result) {
				if (result == '0000') {
					$('#codeStatus').html("<span style='color: green'>사용 가능</span>");
				}
				else if (result == '0001') {
					$('#codeStatus').html("<span style='color: red'>이미 사용한 코드입니다</span>");
				}
				else if (result == '0009') {
					$('#codeStatus').html("<span style='color: red'>코드가 올바르지 않습니다</span>");
				}
			}
		});
	});

	$('input[name=od_price]').focusout(function() {
		p = $.trim($(this).val());
		if (p < 10000) {
			qAlert('정기후원은 최소 10,000원 이상부터 가능합니다.');
			$(this).val('');
			return false;
		}
	});

	$('input[name=isReceipt]').change(function() {
		if ($(this).val() == 1) {
			$('.ppForm').show();
			$('.pcForm').hide();
		}
		else if ($(this).val() == 2) {
			$('.ppForm').hide();
			$('.pcForm').show();
		}
		else {
			$('.ppForm, .pcForm').hide();
		}
	});
});

function kcp_checkPlugin() {

	if (navigator.userAgent.indexOf('MSIE') > 0 || (navigator.userAgent.indexOf('Trident/7.0') > 0)) {
		if ( document.Payplus.object != null ) {
			$('.layer').remove();
		}
	}
	else {
		var inst = 0;
		for (var i = 0; i < navigator.plugins.length; i++) {
			if (navigator.plugins[i].name == 'KCP') {
				inst = 1;
				break;
			}		
		}

		if (inst == 1) {
			$('.layer').remove();
		}
		else {
			document.location.href=GetInstallFile();
		}
	}
}
setTimeout('kcp_checkPlugin()', 1000);

function kcp_runPlugin(form) {
	var RetVal = false;
<?/*
	if (!$.trim($('input[name=percentage]').val())) {
		qAlert('후원 서약을 입력해 주세요.');
		$('input[name=percentage]').focus();
		return false;
	}

	if (!$.trim($('input[name=vow]').val()) || $('input[name=vow]').val() == '회원님이 나눔으로 꿈꾸는 미래를 적어주세요 :)') {
		qAlert('후원 서약을 입력해 주세요.');
		return false;
	}
*/?>

	p = $.trim($('input[name=od_price]').val());
	if (!p || parseInt(p) == 0) {
		qAlert('정기후원을 희망하는 금액을 입력해 주세요.');
		$('input[name=od_price]').focus();
		return false;
	}
	else if (p < 10000) {
		qAlert('정기후원은 최소 10,000원 이상부터 가능합니다.');
		return false;
	}

	var d = checkRadioValue(document.donationForm.preferdate);
	if (!d) {
		qAlert('자동결제 희망일을 선택해 주세요.');
		return false;
	}

   if('<?=$_debug?>' == '1') {
      var groupid = (d == '10') ? 'BA0011000348' : 'BA0011000348';
   } else {
      var groupid = (d == '10') ? 'E03261000729' : 'E03261000730';
   }

	$('input[name=kcpgroup_id]').val(groupid);

	if (!$.trim($('input[name=buyr_name]').val())) {
		qAlert('후원자 이름을 입력해 주세요.');
		$('input[name=buyr_name]').focus();
		return false;
	}

	if (!$.trim($('input[name=od_hp1]').val()) || !$.trim($('input[name=od_hp2]').val()) || !$('input[name=od_hp3]').val()) {
		qAlert('후원자 연락처를 입력해 주세요.');
		return false;
	}
	$('input[name=buyr_tel1]').val($('input[name=od_hp1]').val() + '-' + $('input[name=od_hp2]').val() + '-' + $('input[name=od_hp3]').val());

	if (!$.trim($('input[name=buyr_mail]').val())) {
		qAlert('후원자 이메일 주소를 입력해 주세요.');
		$('input[name=buyr_mail]').focus();
		return false;
	}

	if(!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($.trim($('input[name=buyr_mail]').val()))) {
		qAlert('이메일 주소가 올바르지 않습니다.');
		$('input[name=buyr_mail]').val('').focus();
		return false;
	}

	if (!$.trim($('input[name=od_zip1]').val())) {
		qAlert('우편번호 검색창을 통해 주소를 입력해 주세요.');
		return false;
	}

	if (!$.trim($('input[name=od_addr2]').val()) || $('input[name=od_addr2]').val() == '상세주소 입력') {
		qAlert('상세주소를 입력해 주세요.');
		return false;
	}

	var isReceipt = checkRadioValue(document.donationForm.isReceipt);
	if (isReceipt == 1) {
		if (!$.trim($('input[name=realname]').val())) {
			qAlert('영수증 발행을 위해 실명을 입력해 주세요.');
			return false;
		}
		if (!$.trim($('input[name=serial1]').val()) || !$.trim($('input[name=serial2]').val())) {
			qAlert('영수증 발행을 위해 주민등록번호를 입력해 주세요.');
			return false;
		}
	}
	if (isReceipt == 2) {

		if (!$.trim($('input[name=permit1]').val()) || !$.trim($('input[name=permit2]').val()) || !$.trim($('input[name=permit3]').val())) {
			qAlert('영수증 발행을 위해 사업자등록번호를 입력해 주세요.');
			return false;
		}

		var ext = $('input[name=permitimage]').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
			qAlert('사업자등록증 이미지가 올바른 포맷이 아닙니다.<br/>GIF, PNG, JPG 파일만 업로드 가능합니다.');
			return false;
		}
	}

<? if(!$giving) { ?>
	if (!confirm('정기후원 신청과 동시에 첫번째 결제가 진행되며, 다음 자동결제 예정일은 <?=date('m') == 12 ? 1 : date('m')+1;?>월 ' + d + '일입니다.\n\n이대로 결제를 진행하시려면 확인 버튼을 눌러 주세요.')) {
		return false;
	}
<? } ?>

	if (MakePayMessage(form) == true) {
		RetVal = true;
	}
	else {
		// res_cd= 오류코드, res_msg=오류메시지
		res_cd  = document.donationForm.res_cd.value;
		res_msg = document.donationForm.res_msg.value;
	}
	return RetVal ;
	
}
</script>

<div id='pluginLayer'>
<img src='/images/common/loadingCircle.gif' style='width: 150px; height: 200px' />
<p>결제 플러그인 설치 여부를 확인하는 중입니다.</p>
브라우저 상단에 노란 알림표시줄이 나타날 경우<br/>
<span style='font-weight: bold; color: white'>"ActiveX 컨트롤 설치"</span>를 선택해 주십시오.
</div>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner donation'>

	<h2 style='margin-bottom: 14px; border: none'>정기후원 신청하기</h2>

	<p style='margin-bottom: 30px; font-size: 10pt'>
   정기후원을 신청하시면 매달 희망일에 자동으로 후원금 결제가 이루어지며, 해당 액수만큼 코인이 충전됩니다.<br/>
   충전된 코인으로 원하는 기부처를 직접 선택하여 기부를 하실 수 있습니다.<br/>
   </p>

	<form name='donationForm' enctype='multipart/form-data' method='post' action='/givex/pp_ax_hub.php'>

	<!-- left -->
	<div id='donationLeft' style='float: left; width: 48%; margin-right: 2%'>

		<h3>정기후원 희망액</h3>
<?/*
		<div style='padding: 20px; font-weight: bold'>
		나는 내 수입의 <input type='text' name='percentage' style='width: 30px; text-align: right' class='numonly' />%를 나눔에 사용할 것을 약속합니다.<br/>
		나의 나눔으로,
		<input type='text' name='vow' value='<?=$vow?>' style='width: 250px; height: 22px' /><br/>
		<p style='color: silver; margin-bottom: 20px'>ex) 나의 나눔으로 세상을 더 아름답게 만들 수 있기를 바랍니다.<br/>
		<span style='color: white'>ex) </span>나의 나눔으로 세상을 희망이 가득한 곳으로 만들고 싶습니다.<br/>
		<span style='color: white'>ex) </span>나의 나눔으로 세상을 서로에게 손 내밀 수 있는 곳 되기를 바랍니다.</p>

		<div style='text-align: right'><strong>정기후원금액</strong> <input type='text' name='od_price' style='text-align: right' class='numonly' /> 원</div>
		</div>

*/?>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>후원금액</th><td>
			<input type='text' name='od_price' style='text-align: right' class='numonly' /> 원
			</td></tr>
		</table>
		<p>정기후원 액수는 최소 10,000원부터 자유롭게 설정 가능합니다.</p>

		<h3 style='margin-top: 20px'>후원금 납입 방식</h3>
		<ul id='donationMethod'>
			<li><input type='radio' name='od_method' value='100000000000' id='oc' checked /><label for='oc'>신용카드</label></li>
			<li><input type='radio' name='od_method' id='oa' disabled /><label for='oa' style='color: #CCCCCC'>자동이체</label></li>
			<li><input type='radio' name='od_method' id='oh' disabled /><label for='oh' style='color: #CCCCCC'>핸드폰</label></li>
		</ul>
		<div style='clear: both'></div>
		<p>현재 신용카드를 이용한 정기결제만 가능합니다.<br/>
		자동이체 및 핸드폰을 이용한 정기 소액결제 서비스를 준비 중입니다.
		</p>

		<h3 style='margin-top: 20px'>결제 희망일</h3>
		<ul id='donationMethod'>
			<li><input type='radio' name='preferdate' value='10' id='d1' /><label for='d1'>10일</label></li>
			<li><input type='radio' name='preferdate' value='25' id='d2' /><label for='d2'>25일</label></li>
		</ul>
		<div style='clear: both'></div>
      <!--
		<p>회원님의 편의에 따라 후원금 결제일을 매달 10일과 25일 중 지정하실 수 있습니다.</p>
		<h3 style='margin-top: 20px'>잔여 코인 사용 방식</h3>
		<ul id='donationPrice'>
			<li><input type='radio' name='coin_method' value='1' id='ca' checked /><label for='ca'>잔여 코인을 자동으로 위젠 추천 캠페인에 기부</label></li>
			<li><input type='radio' name='coin_method' value='2' id='cb' /><label for='cb'>잔여 코인을 진행중인 모든 캠페인에 고르게 기부</label></li>
		</ul>
      -->

<?/*
		<h3 style='margin-top: 20px'>GIVE 코드<span style='font-size: 8pt; text-shadow: none; padding-top: 5px; color: #C0C0C0'> (초대받았을 경우에만 작성하시면 됩니다)</span></h3>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>코드 입력</th><td>
			<input type='text' name='givecode' maxlength='8' style='width: 100px; height: 22px' /> <span id='codeStatus'></span>
			</td></tr>
		</table>
		<p>친구로부터 받은 GIVE 코드가 있다면 입력해주세요.</p>
		<div style='clear: both'></div>
*/?>
	</div>
	<!-- /left -->

	<!-- right -->
	<div style='float: left; width: 48%; margin-left: 2%'>
		<h3>개인정보 입력</h3>
		<?
		$info = sql_fetch("SELECT mb_email, mb_contact, mb_zip1, mb_zip2, mb_addr1, mb_addr2 FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
		$contact = explode('-', $info[mb_contact]);
		?>
		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr><th>이름</th><td>
			<input type='text' name='buyr_name' value="<?=$_SESSION[username]?>" /></td></tr>
			<tr><th>전화번호</th><td>
			<input type='text' name="od_hp1" value="<?=$contact[0]?>" size='3' maxlength='3' class='numonly' /> -
			<input type='text' name="od_hp2" value="<?=$contact[1]?>" size='4' maxlength='4' class='numonly' /> -
			<input type='text' name="od_hp3" value="<?=$contact[2]?>" size='4' maxlength='4' class='numonly' /></td></tr>
			<tr><th>이메일</th><td>
			<input type='text' name='buyr_mail' value='<?=$info[mb_email]?>' maxlength='50' style='width: 250px' /></td></tr>
			<tr style='height: 120px'><th>주소</th><td>

			<div class='zipcode-finder'>
				<input type='text' id="dongName" />
				<input type='button' class='zipcode-search' value='검색' />
				<div class="zipcode-search-result"></div>
			</div>
			<div id='addr'>
				<input type='text' name="od_zip1" value="<?=$info[mb_zip1]?>" size='3' maxlength='3' readonly /> -
				<input type='text' name="od_zip2" value="<?=$info[mb_zip2]?>" size='3' maxlength='3' readonly />
				<input type='text' name="od_addr1" value="<?=$info[mb_addr1]?>" style='width: 250px' readonly /><br/>
				<input type='text' name="od_addr2" value="<?=$info[mb_addr2]?>" style='width: 250px' />
			</div>

			</td></tr>
		</table>
		<div style='text-align: center; display: none'>
		<input type='checkbox' name='isBracelet' /> GIVEx 캠페인 팔찌를 신청합니다(무료)
		<p>GIVEx 무브먼트에 참여해주신 첫 300분께는 한정판 Innovator 팔찌를 드립니다.</p>
		</div>

		<h3 style='margin-top: 20px'>기부금 영수증 신청</h3>
		<ul id='donationReceipt'>
			<li><input type='radio' name='isReceipt' value='0' id='pn' /><label for='pn'>미신청</label></li>
			<li><input type='radio' name='isReceipt' value='1' id='pp' checked /><label for='pp'>개인</label></li>
			<li><input type='radio' name='isReceipt' value='2' id='pc' /><label for='pc'>사업자</label></li>
		</ul>
		<div style='clear: both'></div>

		<table cellpadding='0' cellpadding='0' style='width: 100%'>
			<colgroup>
				<col width='120' />
				<col width='/' />
			</colgroup>
			<tr class='ppForm' style=''><th>실명</th><td>
			<input type='text' name='realname' maxlength='10' /></td></tr>
			<tr class='ppForm' style=''><th>주민등록번호</th><td>
			<input type='text' name='serial1' maxlength='6' class='numonly' style='width: 50px; text-align: center' /> - <input type='text' name='serial2' maxlength='7' class='numonly' style='width: 50px; text-align: center' /></td></tr>
			<tr class='pcForm' style='display: none'><th>사업자등록번호</th><td>
			<input type='text' name='permit1' maxlength='3' class='numonly' style='width: 30px; text-align: center' /> - <input type='text' name='permit2' maxlength='2' class='numonly' style='width: 20px; text-align: center' /> - <input type='text' name='permit3' maxlength='5' class='numonly' style='width: 50px; text-align: center' /></td></tr>
			<tr class='pcForm' style='display: none'><th>등록증 사본</th><td>
			<input type='file' name='permitimage' style='width: 100%; text-align: center' /></td></tr>
		</table>

		<p>영수증 발행이 가능한 캠페인에 코인으로 기부하신 경우 해당 금액에 대해 자동으로 영수증을 발행해 드립니다. (소득공제 혜택 있음)</p>
		<p>영수증 발행은 해당 캠페인의 지원 단체에 요청되며, 기부금 영수증은 캠페인이 완료된 후 해당 단체를 통해 일괄 발송됩니다.<br/>
		입력하신 회원님의 실명 정보와 주민등록번호는 영수증 발행 이의외 목적으로는 사용되지 않으며, 입력하신 정보가 정확하지 않을 경우 영수증 발급이 어려울 수 있습니다.</p>


		<div style='text-align: center;  margin: 0px auto'>
		<p>입력하신 개인정보는 캠페인 진행 상황 및 이벤트 당첨 알림, 리워드 발송 등의 목적으로만<br/>
		사용되며, <a href='/privacy-policy'><strong>개인정보 취급방침</strong></a>에 의거해 보호됩니다.</p>
<!--		<input type='checkbox' name='saveInfo' checked />
		위 정보를 앞으로 후원시에도 자동으로 사용하겠습니다.
-->

		<input type='submit' value='정기후원 신청하기' class='submit' onclick="return kcp_runPlugin(this.form);" />

		</div>
	</div>
	<!-- /right -->
	<div style='clear: both'></div>
   <input type='hidden' name='giving' value = 'N'>
	<input type='hidden' name='ordr_idxx'		value='<?=date('ymdHis').rand(100, 999)?>' />
	<input type='hidden' name='buyr_tel1'		value='' />
	<input type='hidden' name='buyr_tel2'		value='' />
	<input type='hidden' name='kcpgroup_id'		value='E03261000729' />
	<input type='hidden' name='batch_soc'		value='Y' />
	<!-- PLUGIN 설정 (변경 불가) -->
	<input type='hidden' name='req_tx'			value='pay' />
	<input type='hidden' name='site_cd'			value="<?=$g_conf_batch_cd?>" />
	<input type='hidden' name='site_key'		value="<?=$g_conf_batch_key?>" />
	<input type='hidden' name='site_name'		value="<?=$g_conf_site_name?>" />
	<input type='hidden' name='module_type'		value='01' />
	<input type='hidden' name='res_cd'			value='' />
	<input type='hidden' name='res_msg'			value='' />
	<input type='hidden' name='trace_no'		value='' />
	<input type='hidden' name='enc_info'		value='' />
	<input type='hidden' name='enc_data'		value='' />
	<input type='hidden' name='tran_cd'			value='' />
	<input type='hidden' name='pay_method'		value='AUTH:CARD' />
	<input type='hidden' name='site_logo'		value='' />
	<input type='hidden' name='skin_indx'		value='1' />
	<input type='hidden' name='card_cert_type'	value='BATCH' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token'			value='<?=dechex(crc32(session_id().'thisisSALT'))?>' />
   '
	</form>

	</div>
</div>&nbsp;
</div>

<?
include '../module/_tail.php';
?>
