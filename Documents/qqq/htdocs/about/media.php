<?
$_required = true;
include '../config.php';
include '../module/_head.php';

$view = preg_match("/^[0-9]+$/", $_GET[view]) ? $_GET[view] : false;
$page = preg_match("/^[0-9]+$/", $_GET[page]) ? $_GET[page] : 1;
$perpage = 10;
$limitFrom = ($page - 1) * $perpage;
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<a href='/about'><li>위젠이란?</li></a>
		<li>언론소개</li>
		<a href='/about/guide'><li>위젠 가이드</li></a>
		<a href='/about/inquiry'><li>1:1 문의</li></a>
		<a href='/about/hall-of-fame'><li>명예의전당</li></a>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px; margin-bottom: 14px; border: none'>언론소개</h2>

	<?
	if ($view) {
		$data = sql_fetch("SELECT * FROM ".DB_MEDIA." WHERE wr_id = '$view' ");
	?>

		<table cellpadding='0' cellpadding='0' style='width: 100%; margin: 30px auto; border-top: solid 2px gray; border-bottom: solid 1px gray'>
			<colgroup>
				<col width='/' />
				<col width='100' />
			</colgroup>
			<tr>
			<td><?=$data[wr_subject]?></td>
			<td><?=array_pop(array_reverse(explode(' ', $data[wr_datetime])))?></td>
			</tr>
			<tr><td colspan='2' style='padding: 30px 0px; text-align: justify'>
			<?=$data[wr_content]?>
			</td></tr>
		</table>
		<div style='text-align: right'><a href='../media?page=<?=$page?>'><button class='button_common'>목록</button></a></div>

	<? } else { ?>
		<table cellpadding='0' cellpadding='0' style='width: 100%; margin: 30px auto; border-top: solid 2px gray; border-bottom: solid 1px gray'>
			<colgroup>
				<col width='100' />
				<col width='/' />
				<col width='100' />
			</colgroup>
			<tr style='text-align: center; font-family: NanumGothicBold; color: #E30000; text-shadow: 0px 1px #F8C0C0'>
			<td>번호</td>
			<td>제목</td>
			<td>작성일</td>
			</tr>
		<?
		$sql = "SELECT COUNT(wr_id) AS cnt FROM ".DB_MEDIA;
		$result = sql_fetch($sql);
		$total = $result[cnt];
		$pageTotal = ceil($total / $perpage);

		$total -= $limitFrom;

		$sql = "SELECT * FROM ".DB_MEDIA."
				ORDER BY wr_id DESC
				LIMIT $limitFrom, $perpage";
		$result = sql_query($sql);

		for ($i = 0; $row = sql_fetch_array($result); $i++) {
		?>
			<tr style='text-align: center'>
			<td><?=$total?></td>
			<td style='text-align: left'><a href='/about/media/<?=$row[wr_id]?>'><?=$row[wr_subject]?></a></td>
			<td style='font: 8pt Arial'><?=array_pop(array_reverse(explode(' ', $row[wr_datetime])))?></td>
			</tr>
		<?
			$total--;
		}
		?>
		</table>

		<div style='text-align: center; word-spacing: 5px'>
			<?
			$loopStarts = ($page <= 5) ? 1 : $page-5;
			$loopEnds = ($loopStarts + 9 > $pageTotal) ? $pageTotal : $loopStarts + 9;
			$prev = $page - 1;
			$next = $page + 1;

			if ($page > 6) print "<a href='./media?page=1'><img src='/images/btn_pagging_start.gif' style='vertical-align: middle' /></a> ";
			if ($page != 1) print "<a href='./media?page=$prev'><img src='/images/btn_pagging_prev.gif' style='vertical-align: middle; margin-right: 10px' /></a>";

			for ($i = $loopStarts; $i <= $loopEnds; $i++) {
				print ($page == $i) ? " <span style='font-weight: bold'>$i</span> " : " <a href='./media?page=$i'>$i</a> ";
			}

			if ($page < $pageTotal - 4) print "<a href='./media?page=$next'><img src='/images/btn_pagging_next.gif' style='vertical-align: middle; margin-left: 10px' /></a> ";
			if ($page < 11) print "<a href='./media?page=$pageTotal'><img src='/images/btn_pagging_end.gif' style='vertical-align: middle' /></a> ";

//			if ($pageTotal > 10 && $loopEnds != $pageTotal) print "... <a href='./media?page=$pageTotal'>$pageTotal</a>";
			?>
		</div>

	<? } ?>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu.submenu li').eq(1).addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>