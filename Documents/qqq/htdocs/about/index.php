<?
$_required = true;
include '../config.php';
include '../module/_head.php';

// ACTIVITY LOG
if ($_SESSION[user_no]) {
	mysql_query("INSERT INTO ".DB_ACTIVITY." SET action = '1', mb_no = '$_SESSION[user_no]', param1 = 'about', referer = '$_SERVER[HTTP_REFERER]' ");
}
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<li>위젠이란?</li>
		<a href='./media'><li>언론소개</li></a>
		<a href='./guide'><li>위젠 가이드</li></a>
		<a href='./inquiry'><li>1:1 문의</li></a>
		<a href='./hall-of-fame'><li>명예의전당</li></a>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px; margin-bottom: 14px'>위젠이란?</h2>

	<div id='about'>
	<h3>즐겁고, 투명하고, 부담 없는 기부를 통해<br/>
	사회에 보다 지속적인 변화를 만들어가는 신개념 소셜 기부 플랫폼입니다.</h3>

	<h4>기업, 유명인사, 대중이 모여 만드는 더 지속적인 변화</h4>

	<img src='/images/about/flow.png' style='float: left; margin-right: 20px; margin-bottom: 20px' />
	<p>내가 혼자 내는 목소리는 멀리 퍼지기가 힘들지만 우리가 모여 한 목소리를 낼 때 언론이 이를 반영하게 되고 궁극적으로 우리와 공감하고 목표에 동조하는 기업과 유명인사가 나타납니다.
	유명인사의 소신 있는 발언 등은 다시 우리 세대의 의지 발현에 큰 동기가 되고 기업은 사회적 공헌활동에 더 힘쓸 것이며 목표를 적극적으로 알림으로써 더 많은 수의 우리를 결집하게 합니다.</p>
	<p>사회를 발전시키고 문화를 형성하는 소통을 위해선 구성원들이 자신의 가능성을 열어야 합니다.</p>

	<p><strong>- 우리 자녀들이 살 좋은 세상, 나의 행복한 노후는 그것을 원하는 여러분의 의지로 만들어집니다.</strong><br/>
	관심 있는 캠페인에 기부하고 힘을 보태주세요.</p>

	<p><strong>- 언론 노출이 잦은 스타들은 더 이상 혼자서 사회적 책임에 대한 부담을 짊어질 필요가 없습니다. 스타들의 영향력을 활용해서 그 변화를 모두와 나누려는 노력이 필요합니다.</strong><br/>
	기부에 참여하는 분들 중 추첨을 통해, 같은 캠페인을 지원하는 유명인사와의 만남의 기회, 작품/소장품 등을 드립니다.</p>

	<p style='clear: both'><strong>- 기업의 성장은 매분 매초 변하는 소비자의 입맛만 따라가기보다, 그들과 같이 나은 미래를 꿈꿀 때 이루어집니다.</strong><br/>
	일반 기부자들의 목표 금액만큼을 매칭, 기부하여 기업철학과 사회적 가치를 함께 나눕니다.<br/>
	기업 자체의 사회적 책임 활동(CSR)보다 더 큰 파급력을 가지고, 기업과 사회의 동반 성장에 더욱 효과적입니다.</p>

	<h4>SNS로 더 즐겁고 투명한 소통</h4>

	<p>내가 기부한 돈이 어디에 쓰이는지 모른다는 것은, 그 다음 행동을 취할 동기가 없어지는 것과 같습니다.<br/>
	여러분이 쓴 에너지가 어디로 흘러가서 어떤 운동을 발생시켰는 지 알려주는 것, 위젠이 소통의 생태계를 구축해가는 방식입니다.</p>
	<p style='margin-left: 30px'>
	- 위젠은 100% 기부만을 원하시는 후원자분들께 후원금액의 100%가 명시된 수혜자에게 전달되는 <strong>100% 기부 옵션</strong>을 제공합니다.<br/>
	- 위젠은 기부와 더불어 <strong>유명인사 이벤트 추첨에 응모</strong> 역시 원하시는 후원자분들께는 금융수수료, 이벤트비(저녁식사비 등), 운영비를 포함하여<br/>
	20%를 제한 나머지 금액을 명시된 수혜자에게 전해드립니다.<br/>
	- 위젠은 캠페인 종료 후에도 재무보고를 통해 후원금 사용처를 활동 내용과 함께 캠페인 페이지에 지속적으로 기재합니다.</p>

	<h4>책임은 관심으로, 기부는 사회적 가치 소비로</h4>

	<p>위젠은 여러분 개인의 선택과 결과뿐만 아니라, 그 과정에서 행하는 자유와 책임의 영향력을 사회에 널리 퍼뜨리고자 합니다.<br/>
	일하면서, 여가를 즐기면서, 사회의 한 구성원으로서, 지구의 한 생명체로서, 우리는 더 나은 미래의 모습을 상상하거나 직접 실현해보고 싶어합니다.<br/>
	그 때, 어디에 계시든지, 위젠에 접속하세요. 자유와 보람의 폭이 더 커집니다.</p>
	<p style='margin-left: 30px'>
	- 어떤 기업, 비영리단체, 유명인사가 어떤 사회변화를 위해 같이 일하는지 둘러보세요.<br/>
	- 여러분 각자가 생각하는 우리 사회에 필요한 변화를 지지 그리고 소비하세요.<br/>
	- 주변 사람들에게 여러분의 의지를 알리고, 같은 마음을 가진 새로운 친구들과 어울리세요.<br/></p>

	<p style='margin-top: 20px'>돈은 잘 꺼내보이지 않지만 쓰기는 쉬운 편입니다. 그런데 의지는 표현해보이긴 쉬운데 쓰기는 참 어렵습니다.<br/>
	위젠은 돈을 더 현명하게 쓰고 의지는 더 쉽게 쓰는 방법이 뭘까 생각했습니다.</p>
	<p style='margin-left: 30px'>
	- <strong>천 원, 대중교통 한 번 타는 값입니다. 삼천 원, 커피 한 잔 마시는 값입니다.<br/>
	위젠에 오셔서 마음에 드는 사회변화 캠페인에 여러분의 의지를 보태는 비용입니다.</strong><br/>
	- 동영상과 사진을 곁들인 스토리를 읽고 원하는 변화에 기부하세요.<br/>
	- 캠페인이 종료된 후에는 여러분의 기부를 통해 생겨난 새로운 이야기들을 보내드립니다.</p>

	<h3 style='text-align: center; margin-top: 30px'>혼자는 어렵지만, 함께라면 할 수 있습니다.<br/>
	세상을 바꾸는 꿈을 꾸는 청춘들의 커뮤니티, 위제너레이션입니다.<br/>
	<img src='/images/common/logo.png' style='width: 200px; height: 25px; margin-top: 30px' /></h3>

	</div>
</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li').eq(0).addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>