<?
$_required = true;
include '../config.php';
include '../module/_head.php';
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<a href='./'><li>위젠이란?</li></a>
		<a href='./media'><li>언론소개</li></a>
		<a href='./guide'><li>위젠 가이드</li></a>
		<li>1:1 문의</li>
		<a href='./hall-of-fame'><li>명예의전당</li></a>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px; margin-bottom: 14px'>1:1 문의</h2>


	<form name='inquiryForm' method='post' action='/sendmail.php'>
	<input type='hidden' name='mailtype' value='inquiry' />
	<? $_SESSION[token] = dechex(crc32(session_id().'thisisSALT')); ?>
	<input type='hidden' name='token' value='<?=dechex(crc32(session_id().'thisisSALT'))?>' >
	<table cellpadding='0' cellpadding='0' style='width: 100%'>
		<colgroup>
			<col width='120' />
			<col width='/' />
		</colgroup>
		<?
		$info = sql_fetch("SELECT mb_email FROM ".DB_MEMBERS." WHERE mb_no = '$_SESSION[user_no]' ");
		?>
		<tr><th>문의자명</th><td>
		<input type='text' name='inq_name' value="<?=$_SESSION['username']?>" /></td></tr>
		<tr><th>이메일</th><td>
		<input type='text' name='inq_mail' value='<?=$info[mb_email]?>' maxlength='50' style='width: 250px' /></td></tr>
		<tr><th>제목</th><td>
		<input type='text' name='inq_subject' style='width: 100%' /></td></tr>
		<tr><th>문의 내용</th><td>
		<textarea name='inq_desc' rows='8' style='width: 100%'></textarea></td></tr>
	</table>

	<div style='text-align: right;  margin: 0px auto'>
	<input type='submit' value='문의하기' style='margin: 20px 0px; border: none; cursor: pointer; background: black; width: 150px; height: 30px; font: 10pt NanumGothic; color: white' onclick="return checkInquiryForm();" />
	</div>
	</form>


</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu.submenu li').eq(3).addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>