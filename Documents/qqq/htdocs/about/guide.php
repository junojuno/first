<?
$_required = true;
include '../config.php';
include '../module/_head.php';
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<a href='./'><li>위젠이란?</li></a>
		<a href='./media'><li>언론소개</li></a>
		<li>위젠 가이드</li>
		<a href='./inquiry'><li>1:1 문의</li></a>
		<a href='./hall-of-fame'><li>명예의전당</li></a>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px; margin-bottom: 14px; border: none'>위젠 가이드</h2>

	<div style='float: left; width: 125px'>
		<menu class='leftmenu'>
			<li>* 위제너레이션</li>
			<li>* 계정설정</li>
			<li>* 후원하기</li>
			<li>* 이벤트/리워드</li>
			<li>* 정기후원</li>
			<li>* 뱃지/레벨</li>
			<li>* 캠페인신청</li>
			<li>* 기타</li>
		</menu>
	</div>

	<div style='border-left: solid 2px #EBEBEB; min-height: 300px; margin-left: 150px'>
	<div style='padding: 0px 20px'>
		<ul class='guide'>
		</ul>
	</div>
	</div>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu.submenu li').eq(2).addClass('on');
	$('menu.leftmenu li').eq(0).addClass('on');

	$('menu.leftmenu li').click(function() {
		$('menu.leftmenu li').removeClass('on');
		$('menu.leftmenu li').eq($(this).index()).addClass('on');
		$.ajax({
			url: 'ajax.guide.php?c=' + $(this).index(),
			cache: false
		}).done(function(html) {
			$('.guide').empty().append(html);
			$('#content ul li:first').css('borderTop', 'solid 2px black');
			$('#content ul li').click(function() {
				$('.guideAnswer').hide();
				$('.guideAnswer', this).fadeIn(250);
			});
		});
	});
	$('menu.leftmenu li').eq(0).click();
});
</script>

<?
include '../module/_tail.php';
?>