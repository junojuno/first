<?
$_required = true;
include '../config.php';
include '../module/_head.php';
?>

<div id='highlight'>&nbsp;
<div id='content' class='solid'>
<div class='inner'>
	<menu class='submenu'>
		<a href='./'><li>위젠이란?</li></a>
		<a href='./media'><li>언론소개</li></a>
		<a href='./guide'><li>위젠 가이드</li></a>
		<a href='./inquiry'><li>1:1 문의</li></a>
		<li>명예의전당</li>
	</menu>
	<div style='clear: both'></div>

	<h2 style='margin-top: 30px; margin-bottom: 14px'>명예의 전당</h2>
	<p style='font-size: 12px; color: #A2A2A2; text-shadow: 0px 1px #E3E3E3; margin-bottom: 30px'>위젠 레벨은 기부액과 기부 횟수를 함께 고려하여 부여되는 명예로운 레벨입니다.<br/>
	예를 들어 총 5개 이상 캠페인에 5만원 이상 기부해주신 경우 레벨 5가, 10개 이상 캠페인에 총 10만원 이상 기부해주신 경우 레벨 10이 부여되며,<br/>같은 방식으로 레벨 100까지 5단위로 올라가게 됩니다.</p>

	<div id='hall-of-fame'>
	<?
	$sql = "SELECT mb_no, COUNT(DISTINCT it_id) AS count, SUM(od_amount) AS amount
			FROM ".DB_ORDERS."
			WHERE od_amount > 0
			GROUP BY mb_no";
	$result = sql_query($sql);
	$total = mysql_num_rows($result);

	for ($i = 0; $row = sql_fetch_array($result); $i++) {
		$arr[$i] = array($row[mb_no], getWegenLevel($row[mb_no], true, $row[count], $row[amount]));
	}

	function sortLvl($a, $b) {
		return $b[1] - $a[1];
	}
	usort($arr, 'sortLvl');

	$isFirst = 0;
	for ($i = 0; $i < count($arr); $i++) {
		if ($arr[$i][1] != $isFirst) {
			$isFirst = $arr[$i][1];
	?>
	<div style='clear: both'></div>

	<h3 class='hMiddle' style='border: none'>WEGEN LEVEL <span style='color: #E30000; font-weight: bold; font-family: Arial; text-shadow: none'><?=$isFirst?></span></h3>
	<ul>
	<?
		}
		print "<li>";
		drawPortrait($arr[$i][0], true);
		print "</li>\n";
	}
	?>
	</ul>
	<div style='clear: both'></div>
	</div>

</div>
</div>
&nbsp;
</div>

<script type='text/javascript'>
$(document).ready(function() {
	$('#content menu li').eq(4).addClass('on');
});
</script>

<?
include '../module/_tail.php';
?>